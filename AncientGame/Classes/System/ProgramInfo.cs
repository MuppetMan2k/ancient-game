﻿namespace AncientGame
{
    static class ProgramInfo
    {
        /* -------- Properties -------- */
        public static string CompanyName { get { return "Jasmine Software"; } }
        public static string ProgramName { get { return "Ancient Game"; } }
    }
}
