using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace AncientGame
{
    class AncientGame : Game
    {
        /* -------- Private Fields -------- */
        // Components
        private InputComponent input;
        private GraphicsComponent graphics;
        private AudioComponent audio;
        private FlowStateComponent flow;
        private UIComponent ui;
        // Graphics objects
        private GraphicsDeviceManager deviceManager;

        /* -------- Constructors -------- */
        public AncientGame()
        {
            deviceManager = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsFixedTimeStep = false;
        }

        /* -------- Protected Methods --------- */
        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Debug.Log("{0} started at {1} on {2}", ProgramInfo.ProgramName, DateTime.Now.ToShortTimeString(), DateTime.Now.ToShortDateString());

            RandomUtilities.Initialize();
            SettingsManager.Initialize();

            ApplyGraphicsSettings();

            input = new InputComponent(this);
            Components.Add(input);
            graphics = new GraphicsComponent(this);
            Components.Add(graphics);
            audio = new AudioComponent(this);
            Components.Add(audio);
            ui = new UIComponent(this, graphics, input);
            Components.Add(ui);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            graphics.SetGraphicsObjects(deviceManager);
            AssetLists.LoadContent(Content);
            Debug.LoadContent(Content);
            CalibrationManager.LoadContent(Content);
            Localization.LoadContent(Content);
            
            // use this.Content to load your game content here

            flow = new FlowStateComponent(this, input, graphics, ui, Services);
            Components.Add(flow);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            Benchmarking.StartBenchmark("UpdateLoop");
            base.Update(gameTime);
            Benchmarking.EndBenchmark("UpdateLoop");

            if (input.KeyIsPressing(Keys.Escape))
            {
                OnExit();
                Exit();
            }
            
            Debug.Update(new GameTimeWrapper(gameTime), input);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            Benchmarking.StartBenchmark("DrawLoop");
            base.Draw(gameTime);
            Benchmarking.EndBenchmark("DrawLoop");

#if DEBUG
            graphics.SpriteBatch.Begin();
            Debug.SBDraw(graphics);
            graphics.SpriteBatch.End();
#endif
        }

        /* -------- Private Methods --------- */
        private void ApplyGraphicsSettings()
        {
            deviceManager.PreferredBackBufferWidth = SettingsManager.Settings.Resolution.Width;
            deviceManager.PreferredBackBufferHeight = SettingsManager.Settings.Resolution.Height;
            deviceManager.IsFullScreen = !SettingsManager.Settings.Windowed;
            deviceManager.ApplyChanges();
        }

        private void OnExit()
        {
            SettingsManager.OnGameExit();
        }
    }
}
