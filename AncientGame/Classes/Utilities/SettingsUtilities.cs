﻿namespace AncientGame
{
    static class SettingsUtilities
    {
        /* -------- Public Methods -------- */
        public static bool GetWindowedFromSettingsText(string text)
        {
            bool windowed;
            if (bool.TryParse(text, out windowed))
                return windowed;

            return false;
        }

        public static float GetAudioVolumeFromSettingsText(string text)
        {
            float volume;
            if (float.TryParse(text, out volume))
                return Mathf.Clamp01(volume);

            return 1f;
        }

        public static float GetFloatFromSettingsText(string text)
        {
            float f;
            if (float.TryParse(text, out f))
                return f;

            return 0f;
        }
    }
}
