﻿namespace AncientGame
{
    static class GameUtilities
    {
        /* -------- Public Methods -------- */
        public static bool LandTerritoryControlLevelIsOwnership(eLandTerritoryControlLevel level)
        {
            switch (level)
            {
                case eLandTerritoryControlLevel.NO_CONTROL:
                    return false;
                case eLandTerritoryControlLevel.OCCUPATION:
                    return false;
                case eLandTerritoryControlLevel.MILITARY_OWNERSHIP:
                    return true;
                case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                    return true;
                case eLandTerritoryControlLevel.HOMELAND_OWNERSHIP:
                    return true;
                default:
                    Debug.LogUnrecognizedValueWarning(level);
                    return false;
            }
        }

        public static bool DiplomaticContractTypeIsClientMastery(eDiplomaticContractType type)
        {
            if (type == eDiplomaticContractType.CULTURAL_CLIENT_MASTER || type == eDiplomaticContractType.MILITARY_CLIENT_MASTER || type == eDiplomaticContractType.SATRAP_MASTER)
                return true;

            return false;
        }
        public static bool DiplomaticContractTypeIsColonyMastery(eDiplomaticContractType type)
        {
            if (type == eDiplomaticContractType.SETTLEMENT_COLONY_MASTER || type == eDiplomaticContractType.TRADE_COLONY_MASTER)
                return true;

            return false;
        }
        public static bool DiplomaticContractTypeIsColonySubjection(eDiplomaticContractType type)
        {
            if (type == eDiplomaticContractType.SETTLEMENT_COLONY_SUBJECT || type == eDiplomaticContractType.TRADE_COLONY_SUBJECT)
                return true;

            return false;
        }
    }
}
