﻿using System;

namespace AncientGame
{
    static class DataUtilities
    {
        /* -------- Public Methods -------- */
        // Enum parsing methods
        public static eTerrainType ParseTerrainType(string text)
        {
            eTerrainType terrainType;

            if (Enum.TryParse(text, true, out terrainType))
                return terrainType;

            Debug.LogWarning("Tried to parse text {0} to eTerrainType but failed.", text);
            return eTerrainType.LOW_GRASS_LAND;
        }
        public static eClimateType ParseClimateType(string text)
        {
            eClimateType climateType;

            if (Enum.TryParse(text, true, out climateType))
                return climateType;

            Debug.LogWarning("Tried to parse text {0} to eClimateType but failed.", text);
            return eClimateType.WARM_AND_WET;
        }
        public static eResourceTier ParseResourceTier(string text)
        {
            eResourceTier resourceTier;

            if (Enum.TryParse(text, true, out resourceTier))
                return resourceTier;

            Debug.LogWarning("Tried to parse text {0} to eResourceTier but failed.", text);
            return eResourceTier.PRIMARY;
        }
        public static eResourceType ParseResourceType(string text)
        {
            eResourceType resourceType;

            if (Enum.TryParse(text, true, out resourceType))
                return resourceType;

            Debug.LogWarning("Tried to parse text {0} to eResourceType but failed.", text);
            return eResourceType.GENERIC;
        }
        public static eLandTerritoryControlLevel ParseLandTerritoryControlLevel(string text)
        {
            eLandTerritoryControlLevel controlLevel;

            if (Enum.TryParse(text, out controlLevel))
                return controlLevel;

            Debug.LogWarning("Tried to parse text {0} to eLandTerritoryControlLevel but failed", text);
            return eLandTerritoryControlLevel.NO_CONTROL;
        }
        public static eGovernmentGroup ParseGovernmentGroup(string text)
        {
            eGovernmentGroup governmentGroup;

            if (Enum.TryParse(text, out governmentGroup))
                return governmentGroup;

            Debug.LogWarning("Tried to parse text {0} to eGovernmentGroup but failed", text);
            return eGovernmentGroup.MONARCHY;
        }
        public static eBuildingDamageLevel ParseBuildingDamageLevel(string text)
        {
            eBuildingDamageLevel buildingDamageLevel;

            if (Enum.TryParse(text, out buildingDamageLevel))
                return buildingDamageLevel;

            Debug.LogWarning("Tried to parse text {0} to eBuildingDamageLevel but failed", text);
            return eBuildingDamageLevel.NO_DAMAGE;
        }
        public static eStatConstituentType ParseStatConstituentType(string text)
        {
            eStatConstituentType statConstituentType;

            if (Enum.TryParse(text, out statConstituentType))
                return statConstituentType;

            Debug.LogWarning("Tried to parse text {0} to eStatConstituentType but failed", text);
            return eStatConstituentType.LOCAL_TRADITIONS_POS;
        }
        public static eCultureInfluenceType ParseCultureInfluenceType(string text)
        {
            eCultureInfluenceType cultureInfluenceType;

            if (Enum.TryParse(text, out cultureInfluenceType))
                return cultureInfluenceType;

            Debug.LogWarning("Tried to parse text {0} to eCultureInfluenceType but failed", text);
            return eCultureInfluenceType.LOCAL_TRADITIONS;
        }
        public static eLanguageInfluenceType ParseLanguageInfluenceType(string text)
        {
            eLanguageInfluenceType languageInfluenceType;

            if (Enum.TryParse(text, out languageInfluenceType))
                return languageInfluenceType;

            Debug.LogWarning("Tried to parse text {0} to eLanguageInfluenceType but failed", text);
            return eLanguageInfluenceType.LOCAL_TRADITIONS;
        }
        public static eReligionInfluenceType ParseReligionInfluenceType(string text)
        {
            eReligionInfluenceType religionInfluenceType;

            if (Enum.TryParse(text, out religionInfluenceType))
                return religionInfluenceType;

            Debug.LogWarning("Tried to parse text {0} to eReligionInfluenceType but failed", text);
            return eReligionInfluenceType.LOCAL_TRADITIONS;
        }
        public static eBuildingSlotType ParseBuildingSlotType(string text)
        {
            eBuildingSlotType buildingSlotType;

            if (Enum.TryParse(text, out buildingSlotType))
                return buildingSlotType;

            Debug.LogWarning("Tried to parse text {0} to eBuildingSlotType but failed", text);
            return eBuildingSlotType.STANDARD_SETTLEMENT;
        }
        public static eDiplomaticContractType ParseDiplomaticContractType(string text)
        {
            eDiplomaticContractType diplomaticContractType;

            if (Enum.TryParse(text, out diplomaticContractType))
                return diplomaticContractType;

            Debug.LogWarning("Tried to parse text {0} to eDiplomaticContractType but failed", text);
            return eDiplomaticContractType.WAR;
        }
        public static eFactionSize ParseFactionSize(string text)
        {
            eFactionSize factionsize;

            if (Enum.TryParse(text, out factionsize))
                return factionsize;

            Debug.LogWarning("Tried to parse text {0} to eFactionSize but failed", text);
            return eFactionSize.STATE;
        }
        public static eFactionSpecialRating ParseFactionSpecialRating(string text)
        {
            eFactionSpecialRating factionSpecialRating;

            if (Enum.TryParse(text, out factionSpecialRating))
                return factionSpecialRating;

            Debug.LogWarning("Tried to parse text {0} to eFactionSpecialRating but failed", text);
            return eFactionSpecialRating.NONE;
        }
        public static eCharacterGender ParseCharacterGender(string text)
        {
            eCharacterGender characterGender;

            if (Enum.TryParse(text, out characterGender))
                return characterGender;

            Debug.LogWarning("Tried to parse text {0} to eCharacterGender but failed", text);
            return eCharacterGender.MALE;
        }
        public static eCharacterGenderCondition ParseCharacterGenderCondition(string text)
        {
            eCharacterGenderCondition characterGenderCondition;

            if (Enum.TryParse(text, out characterGenderCondition))
                return characterGenderCondition;

            Debug.LogWarning("Tried to parse text {0} to eCharacterGenderCondition but failed", text);
            return eCharacterGenderCondition.CAN_BE_EITHER;
        }
        public static eAppointmentType ParseAppointmentType(string text)
        {
            eAppointmentType appointmentType;

            if (Enum.TryParse(text, out appointmentType))
                return appointmentType;

            Debug.LogWarning("Tried to parse text {0} to eAppointmentType but failed", text);
            return eAppointmentType.OTHER;
        }
        public static eFollowerType ParseFollowerType(string text)
        {
            eFollowerType followerType;

            if (Enum.TryParse(text, out followerType))
                return followerType;

            Debug.LogWarning("Tried to parse text {0} to eFollowerType but failed", text);
            return eFollowerType.DEBUG_FOLLOWER;
        }
        public static eLandForceStance ParseLandForceStance(string text)
        {
            eLandForceStance landForceStance;

            if (Enum.TryParse(text, out landForceStance))
                return landForceStance;

            Debug.LogWarning("Tried to parse text {0} to eLandForceStance but failed", text);
            return eLandForceStance.NORMAL;
        }
        public static eSeaForceStance ParseSeaForceStance(string text)
        {
            eSeaForceStance seaForceStance;

            if (Enum.TryParse(text, out seaForceStance))
                return seaForceStance;

            Debug.LogWarning("Tried to parse text {0} to eSeaForceStance but failed", text);
            return eSeaForceStance.NORMAL;
        }
        public static eWealthSourceType ParseWealthSourceType(string text)
        {
            eWealthSourceType wealthSourceType;

            if (Enum.TryParse(text, out wealthSourceType))
                return wealthSourceType;

            Debug.LogWarning("Tried to parse text {0} to eWealthSourceType but failed", text);
            return eWealthSourceType.PRIVATE_WEALTH;
        }
        public static eIncomeEfficiencySource ParseIncomeEfficiencySource(string text)
        {
            eIncomeEfficiencySource incomeEfficiencySource;

            if (Enum.TryParse(text, out incomeEfficiencySource))
                return incomeEfficiencySource;

            Debug.LogWarning("Tried to parse text {0} to eIncomeEfficiencySource but failed", text);
            return eIncomeEfficiencySource.CORRUPTION_NEG;
        }
        public static eModificationType ParseModificationType(string text)
        {
            eModificationType modificationType;

            if (Enum.TryParse(text, out modificationType))
                return modificationType;

            Debug.LogWarning("Tried to parse text {0} to eModificationType but failed", text);
            return eModificationType.ADDITION;
        }
        public static eModifierType ParseModifierType(string text)
        {
            eModifierType modifierType;

            if (Enum.TryParse(text, out modifierType))
                return modifierType;

            Debug.LogWarning("Tried to parse text {0} to eModifierType but failed", text);
            return eModifierType.STATS;
        }
        public static eStatType ParseStatType(string text)
        {
            eStatType statType;

            if (Enum.TryParse(text, out statType))
                return statType;

            Debug.LogWarning("Tried to parse text {0} to eStatType but failed", text);
            return eStatType.PLEB_HAPPINESS;
        }
        public static eWealthSourceTaxationType ParseWealthSourceTaxationType(string text)
        {
            eWealthSourceTaxationType wealthSourceTaxationType;

            if (Enum.TryParse(text, out wealthSourceTaxationType))
                return wealthSourceTaxationType;

            Debug.LogWarning("Tried to parse text {0} to eWealthSourceTaxationType but failed", text);
            return eWealthSourceTaxationType.PRIVATE_WEALTH;
        }
        public static eLandTerritoryControlLevelRequirement ParseLandTerritoryControlLevelRequirement(string text)
        {
            eLandTerritoryControlLevelRequirement landTerritoryControlLevelRequirement;

            if (Enum.TryParse(text, out landTerritoryControlLevelRequirement))
                return landTerritoryControlLevelRequirement;

            Debug.LogWarning("Tried to parse text {0} to eLandTerritoryControlLevelRequirement but failed", text);
            return eLandTerritoryControlLevelRequirement.NO_REQUIREMENT;
        }
        public static eResourceTradeType ParseResourceTradeType(string text)
        {
            eResourceTradeType resourceTradeType;

            if (Enum.TryParse(text, out resourceTradeType))
                return resourceTradeType;

            Debug.LogWarning("Tried to parse text {0} to eResourceTradeType but failed", text);
            return eResourceTradeType.GENERIC_IMPORTS;
        }
        public static eModifierConditionType ParseModifierConditionType(string text)
        {
            eModifierConditionType modifierCaveatType;

            if (Enum.TryParse(text, out modifierCaveatType))
                return modifierCaveatType;

            Debug.LogWarning("Tried to parse text {0} to eModifierConditionType but failed", text);
            return eModifierConditionType.NONE;
        }
        public static eLandUnitType ParseLandUnitType(string text)
        {
            eLandUnitType landUnitType;

            if (Enum.TryParse(text, out landUnitType))
                return landUnitType;

            Debug.LogWarning("Tried to parse text {0} to eLandUnitType but failed", text);
            return eLandUnitType.SPECIAL_INFANTRY;
        }
        public static eLandUnitWeight ParseLandUnitWeight(string text)
        {
            eLandUnitWeight landUnitWeight;

            if (Enum.TryParse(text, out landUnitWeight))
                return landUnitWeight;

            Debug.LogWarning("Tried to parse text {0} to eLandUnitWeight but failed", text);
            return eLandUnitWeight.MEDIUM;
        }
        public static eSeaUnitType ParseSeaUnitType(string text)
        {
            eSeaUnitType navalUnitType;

            if (Enum.TryParse(text, out navalUnitType))
                return navalUnitType;

            Debug.LogWarning("Tried to parse text {0} to eNavalUnitType but failed", text);
            return eSeaUnitType.SPECIAL_SHIP;
        }
        public static eSeaUnitSize ParseSeaUnitSize(string text)
        {
            eSeaUnitSize navalUnitSize;

            if (Enum.TryParse(text, out navalUnitSize))
                return navalUnitSize;

            Debug.LogWarning("Tried to parse text {0} to eNavalUnitSize but failed", text);
            return eSeaUnitSize.MEDIUM;
        }
        public static eLandUnitImprovementType ParseLandUnitImprovementType(string text)
        {
            eLandUnitImprovementType improvementType;

            if (Enum.TryParse(text, out improvementType))
                return improvementType;

            Debug.LogWarning("Tried to parse text {0} to eLandUnitImprovementType but failed", text);
            return eLandUnitImprovementType.ARMOR;
        }
        public static eSeaUnitImprovementType ParseSeaUnitImprovementType(string text)
        {
            eSeaUnitImprovementType improvementType;

            if (Enum.TryParse(text, out improvementType))
                return improvementType;

            Debug.LogWarning("Tried to parse text {0} to eSeaUnitImprovementType but failed", text);
            return eSeaUnitImprovementType.ARMOR;
        }
    }
}
