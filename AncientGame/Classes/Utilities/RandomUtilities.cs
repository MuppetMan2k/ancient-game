﻿using System;

namespace AncientGame
{
    static class RandomUtilities
    {
        /* -------- Private Fields -------- */
        private static Random rand;
        private static Random determRand;

        /* -------- Public Methods -------- */
        public static void Initialize()
        {
            rand = new Random(CreateSeed());
        }

        // Random methods
        public static float RandomNumber(float min, float max)
        {
            return (min + (max - min) * (float)rand.NextDouble());
        }
        public static int RandomNumber(int min, int max)
        {
            return rand.Next(min, max + 1);
        }
        public static bool ProbabilitySuccess(float probability)
        {
            if (probability >= 1f)
                return true;

            float randomNumber = RandomNumber(0f, 1f);
            return (randomNumber < probability);
        }
        public static float GetValueWithError(float average, float error)
        {
            float minValue = average * (1f - error);
            float maxValue = average * (1f + error);

            return RandomNumber(minValue, maxValue);
        }
        public static float RandomDistributedNumber(float min, float max, float average, int power)
        {
            float val = RandomNumber(min, max);

            if (val == average)
                return val;

            if (val > average)
            {
                float lerpFactor = Mathf.GetLerpFactor(val, average, max);
                lerpFactor = Mathf.Pow(lerpFactor, power);
                return Mathf.Lerp(average, max, lerpFactor);
            }
            else
            {
                float lerpFactor = Mathf.GetLerpFactor(val, average, min);
                lerpFactor = Mathf.Pow(lerpFactor, power);
                return Mathf.Lerp(average, min, lerpFactor);
            }
        }

        // Deterministic random methods
        public static float DetermRandomNumber(int seed, float min, float max)
        {
            determRand = new Random(seed);
            return (min + (max - min) * (float)determRand.NextDouble());
        }
        public static int DetermRandomNumber(int seed, int min, int max)
        {
            determRand = new Random(seed);
            return determRand.Next(min, max + 1);
        }
        public static bool DetermProbabilitySuccess(int seed, float probability)
        {
            if (probability >= 1f)
                return true;

            determRand = new Random(seed);
            float randomNumber = DetermRandomNumber(seed, 0f, 1f);
            return (randomNumber < probability);
        }
        public static float GetDetermValueWithError(int seed, float average, float error)
        {
            determRand = new Random(seed);

            float minValue = average * (1f - error);
            float maxValue = average * (1f + error);

            return DetermRandomNumber(seed, minValue, maxValue);
        }
        public static float DetermRandomDistributedNumber(int seed, float min, float max, float average, int power)
        {
            float val = DetermRandomNumber(seed, min, max);

            if (val == average)
                return val;

            if (val > average)
            {
                float lerpFactor = Mathf.GetLerpFactor(val, average, max);
                lerpFactor = Mathf.Pow(lerpFactor, power);
                return Mathf.Lerp(average, max, lerpFactor);
            }
            else
            {
                float lerpFactor = Mathf.GetLerpFactor(val, average, min);
                lerpFactor = Mathf.Pow(lerpFactor, power);
                return Mathf.Lerp(average, min, lerpFactor);
            }
        }

        /* -------- Private Methods --------- */
        private static int CreateSeed()
        {
            DateTime now = DateTime.UtcNow;

            int seed = now.Hour * 3600000 + now.Minute * 60000 + now.Second * 1000 + now.Millisecond;

            return seed;
        }
    }
}
