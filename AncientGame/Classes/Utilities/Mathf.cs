﻿using System;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    static class Mathf
    {
        /* -------- Properties -------- */
        public static float Pi { get { return MathHelper.Pi; } }

        /* -------- Public Methods -------- */
        public static float ToDegrees(float rads)
        {
            return MathHelper.ToDegrees(rads);
        }
        public static float ToRadians(float degs)
        {
            return MathHelper.ToRadians(degs);
        }

        public static float Sin(float degs)
        {
            return (float)Math.Sin((double)ToRadians(degs));
        }

        public static float Asin(float val)
        {
            float rads = (float)Math.Asin((float)val);
            return ToDegrees(rads);
        }

        public static float Cos(float degs)
        {
            return (float)Math.Cos((double)ToRadians(degs));
        }

        public static float Acos(float val)
        {
            float rads = (float)Math.Acos((float)val);
            return ToDegrees(rads);
        }

        public static float Tan(float degs)
        {
            return (float)Math.Tan((double)ToRadians(degs));
        }

        public static float Atan2Rads(float yDiff, float xDiff)
        {
            float rads = (float)Math.Atan2((double)yDiff, (double)xDiff);
            return rads;
        }
        public static float Atan2(float yDiff, float xDiff)
        {
            float rads = Atan2Rads(yDiff, xDiff);
            return ToDegrees(rads);
        }

        public static float Abs(float val)
        {
            if (val < 0f)
                return -val;

            return val;
        }
        public static int Abs(int val)
        {
            if (val < 0)
                return -val;

            return val;
        }

        public static int Sign(float val)
        {
            if (val == 0f)
                return 0;

            if (val > 0f)
                return 1;

            return -1;
        }
        public static int Sign(int val)
        {
            if (val == 0)
                return 0;

            if (val > 0)
                return 1;

            return -1;
        }

        public static float SignF(float val)
        {
            if (val == 0f)
                return 0f;

            if (val > 0f)
                return 1f;

            return -1f;
        }
        public static float SignF(int val)
        {
            if (val == 0)
                return 0f;

            if (val > 0)
                return 1f;

            return -1f;
        }

        public static float Pow(float val, float pow)
        {
            return (float)Math.Pow((double)val, (double)pow);
        }
        public static float Pow(float val, int pow)
        {
            float result = 1f;

            if (pow > 0)
                for (int i = 0; i < pow; i++)
                    result *= val;

            if (pow < 0)
                for (int i = 0; i < pow; i++)
                    result /= val;

            return result;
        }

        public static float Lerp(float val1, float val2, float factor)
        {
            return MathHelper.Lerp(val1, val2, factor);
        }
        public static float GetLerpFactor(float val, float val1, float val2)
        {
            return ((val - val1) / (val2 - val1));
        }

        public static float Clamp(float val, float min, float max)
        {
            if (val < min)
                return min;

            if (val > max)
                return max;

            return val;
        }
        public static int Clamp(int val, int min, int max)
        {
            if (val < min)
                return min;

            if (val > max)
                return max;

            return val;
        }

        public static float ClampUpper(float val, float max)
        {
            if (val > max)
                return max;

            return val;
        }
        public static int ClampUpper(int val, int max)
        {
            if (val > max)
                return max;

            return val;
        }

        public static float ClampLower(float val, float min)
        {
            if (val < min)
                return min;

            return val;
        }
        public static int ClampLower(int val, int min)
        {
            if (val < min)
                return min;

            return val;
        }

        public static float Clamp01(float val)
        {
            return Clamp(val, 0f, 1f);
        }

        public static int Round(float val)
        {
            float remainder = val % 1f;

            if (remainder >= 0.5f)
                return (int)val + 1;

            return (int)val;
        }
        public static int Round(double d)
        {
            return Round((float)d);
        }

        public static float Min(params float[] vals)
        {
            float min = vals[0];

            for (int i = 1; i < vals.Length; i++)
                if (vals[i] < min)
                    min = vals[i];

            return min;
        }
        public static int Min(params int[] vals)
        {
            int min = vals[0];

            for (int i = 1; i < vals.Length; i++)
                if (vals[i] < min)
                    min = vals[i];

            return min;
        }

        public static float Max(params float[] vals)
        {
            float max = vals[0];

            for (int i = 1; i < vals.Length; i++)
                if (vals[i] > max)
                    max = vals[i];

            return max;
        }
        public static int Max(params int[] vals)
        {
            int max = vals[0];

            for (int i = 1; i < vals.Length; i++)
                if (vals[i] > max)
                    max = vals[i];

            return max;
        }

        public static int Floor(float val)
        {
            return (int)Math.Floor((double)val);
        }
        public static int Ceiling(float val)
        {
            return (int)Math.Ceiling((double)val);
        }
    }
}
