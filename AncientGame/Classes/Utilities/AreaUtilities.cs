﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    static class AreaUtilities
    {
        /* -------- Public Methods -------- */
        public static List<int> CalculateAreaIndices(Vector2[] positions)
        {
            int numIndices = (positions.Length - 2) * 3;
            List<int> indices = new List<int>();

            List<Vector2> remainingPositionsToFill = positions.ToList();

            bool continueProcess = true;
            bool error = false;

            while (continueProcess && !error)
            {
                List<eRotationDirection> trainRotations = GetAreaTrainRotationDirections(remainingPositionsToFill);
                if (!AreaContainsConcavePositions(trainRotations))
                {
                    indices.AddRange(GetIndicesForConvexArea(remainingPositionsToFill, positions.ToList()));
                    continueProcess = false;
                }
                else
                {
                    indices.AddRange(CutConcaveTriangleOffEnclosedArea(remainingPositionsToFill, trainRotations, positions.ToList(), ref error));
                }
            }

            return indices;
        }

        public static bool PositionIsWithinArea(Area area, Vector2 position)
        {
            if (!area.BoundingRectangle.ContainsPosition(position))
                return false;

            for (int i = 0; i < area.Indices.Count; i += 3)
            {
                int ind1 = area.Indices[i];
                int ind2 = area.Indices[i + 1];
                int ind3 = area.Indices[i + 2];

                Vector2 triPos1 = area.MSPositions[ind1];
                Vector2 triPos2 = area.MSPositions[ind2];
                Vector2 triPos3 = area.MSPositions[ind3];

                if (VectorUtilities.GetPositionWithinTriangleState(triPos1, triPos2, triPos3, position) != ePositionWithinTriangleState.OUTSIDE)
                    return true;
            }

            return false;
        }

        public static float CalculateAreaEnclosedArea(Area area)
        {
            float containedArea = 0f;

            for (int i = 0; i < area.Indices.Count; i += 3)
            {
                int ind1 = area.Indices[i];
                int ind2 = area.Indices[i + 1];
                int ind3 = area.Indices[i + 2];

                Vector2 triPos1 = area.MSPositions[ind1];
                Vector2 triPos2 = area.MSPositions[ind2];
                Vector2 triPos3 = area.MSPositions[ind3];

                containedArea += CalculateTriangleEnclosedArea(triPos1, triPos2, triPos3);
            }

            return containedArea;
        }

        /* -------- Private Methods --------- */
        private static List<eRotationDirection> GetAreaTrainRotationDirections(List<Vector2> positions)
        {
            List<eRotationDirection> trainRotations = new List<eRotationDirection>();
            int positionNum = positions.Count;

            for (int i = 0; i < positions.Count; i++)
            {
                Vector2 position = positions[i];
                Vector2 prevPosition;
                if (i == 0)
                    prevPosition = positions.Last();
                else
                    prevPosition = positions[i - 1];
                Vector2 nextPosition;
                if (i == positionNum - 1)
                    nextPosition = positions[0];
                else
                    nextPosition = positions[i + 1];

                trainRotations.Add(VectorUtilities.GetVectorTrainRotationDirection(prevPosition, position, nextPosition));
            }

            return trainRotations;
        }

        private static bool AreaContainsConcavePositions(List<eRotationDirection> trainRotations)
        {
            if (trainRotations.Count <= 3)
                return false;

            return trainRotations.Contains(eRotationDirection.CLOCKWISE);
        }

        private static List<int> GetIndicesForConvexArea(List<Vector2> positions, List<Vector2> indexReference)
        {
            List<int> indices = new List<int>();

            Vector2 position1 = positions[0];
            int position1Index = indexReference.IndexOf(position1);
            for (int i = 1; i < positions.Count - 1; i++)
            {
                Vector2 position2 = positions[i];
                Vector2 position3 = positions[i + 1];
                int position2Index = indexReference.IndexOf(position2);
                int position3Index = indexReference.IndexOf(position3);

                indices.Add(position1Index);
                indices.Add(position2Index);
                indices.Add(position3Index);
            }

            return indices;
        }

        private static List<int> CutConcaveTriangleOffEnclosedArea(List<Vector2> positions, List<eRotationDirection> trainRotations, List<Vector2> indexReference, ref bool error)
        {
            List<int> indices = new List<int>();

            int positionNum = positions.Count;

            for (int i = 0; i < positions.Count; i++)
            {
                Vector2 position = positions[i];
                eRotationDirection trainRot = trainRotations[i];

                if (trainRot == eRotationDirection.ANTICLOCKWISE || trainRot == eRotationDirection.NO_ROTATION)
                    continue;

                eRotationDirection prevTrainRot = GeneralUtilities.GetFromCircularList<eRotationDirection>(trainRotations, i - 1);
                if (prevTrainRot == eRotationDirection.ANTICLOCKWISE || prevTrainRot == eRotationDirection.NO_ROTATION)
                {
                    Vector2 prevPosition = GeneralUtilities.GetFromCircularList<Vector2>(positions, i - 1);
                    Vector2 prev2Position = GeneralUtilities.GetFromCircularList<Vector2>(positions, i - 2);

                    if (ConcaveTriangleIsValidForCutoff(prev2Position, prevPosition, position, positions))
                    {
                        indices.Add(indexReference.IndexOf(prev2Position));
                        indices.Add(indexReference.IndexOf(prevPosition));
                        indices.Add(indexReference.IndexOf(position));

                        positions.Remove(prevPosition);

                        return indices;
                    }
                }

                eRotationDirection nextTrainRot = GeneralUtilities.GetFromCircularList<eRotationDirection>(trainRotations, i + 1);
                if (nextTrainRot == eRotationDirection.ANTICLOCKWISE || nextTrainRot == eRotationDirection.NO_ROTATION)
                {
                    Vector2 nextPosition = GeneralUtilities.GetFromCircularList<Vector2>(positions, i + 1);
                    Vector2 next2Position = GeneralUtilities.GetFromCircularList<Vector2>(positions, i + 2);

                    if (ConcaveTriangleIsValidForCutoff(position, nextPosition, next2Position, positions))
                    {
                        indices.Add(indexReference.IndexOf(position));
                        indices.Add(indexReference.IndexOf(nextPosition));
                        indices.Add(indexReference.IndexOf(next2Position));

                        positions.Remove(nextPosition);

                        return indices;
                    }
                }
            }

            error = true;
            return indices;
        }

        private static bool ConcaveTriangleIsValidForCutoff(Vector2 triPos1, Vector2 triPos2, Vector2 triPos3, List<Vector2> positions)
        {
            foreach (Vector2 position in positions)
            {
                if (position.Equals(triPos1) || position.Equals(triPos2) || position.Equals(triPos3))
                    continue;

                ePositionWithinTriangleState positionWithinTriangleState = VectorUtilities.GetPositionWithinTriangleState(triPos1, triPos2, triPos3, position);

                if (positionWithinTriangleState != ePositionWithinTriangleState.OUTSIDE)
                    return false;
            }

            return true;
        }

        private static float CalculateTriangleEnclosedArea(Vector2 triPos1, Vector2 triPos2, Vector2 triPos3)
        {
            Vector3 edgeVec1 = new Vector3(triPos2 - triPos1, 0f);
            Vector3 edgeVec2 = new Vector3(triPos3 - triPos1, 0f);

            return 0.5f * Vector3.Cross(edgeVec1, edgeVec2).Length();
        }
    }
}
