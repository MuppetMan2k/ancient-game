﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eRotationDirection
    {
        CLOCKWISE,
        ANTICLOCKWISE,
        NO_ROTATION
    }

    enum ePositionWithinTriangleState
    {
        INSIDE,
        ON_VERTEX,
        ON_EDGE,
        OUTSIDE
    }

    static class VectorUtilities
    {
        /* -------- Public Methods -------- */
        public static eRotationDirection GetVectorTrainRotationDirection(Vector2 prevPosition, Vector2 position, Vector2 nextPosition)
        {
            Vector2 vec1 = position - prevPosition;
            Vector2 vec2 = nextPosition - position;

            return GetVectorTrainRotationDirection(vec1, vec2);
        }
        public static eRotationDirection GetVectorTrainRotationDirection(Vector2 vec1, Vector2 vec2)
        {
            Vector3 vec31 = new Vector3(vec1, 0f);
            Vector3 vec32 = new Vector3(vec2, 0f);

            Vector3 cross = Vector3.Cross(vec31, vec32);

            if (cross.Z == 0f)
                return eRotationDirection.NO_ROTATION;

            if (cross.Z > 0f)
                return eRotationDirection.ANTICLOCKWISE;
            else
                return eRotationDirection.CLOCKWISE;
        }

        public static ePositionWithinTriangleState GetPositionWithinTriangleState(Vector2 triPos1, Vector2 triPos2, Vector2 triPos3, Vector2 testPos)
        {
            eRotationDirection rotation1 = GetVectorTrainRotationDirection(triPos3, triPos1, testPos);
            eRotationDirection rotation2 = GetVectorTrainRotationDirection(triPos1, triPos2, testPos);
            eRotationDirection rotation3 = GetVectorTrainRotationDirection(triPos2, triPos3, testPos);

            int numNoRotations = 0;
            if (rotation1 == eRotationDirection.NO_ROTATION)
                numNoRotations++;
            if (rotation2 == eRotationDirection.NO_ROTATION)
                numNoRotations++;
            if (rotation3 == eRotationDirection.NO_ROTATION)
                numNoRotations++;

            if (numNoRotations == 2)
                return ePositionWithinTriangleState.ON_VERTEX;

            int numEqualities = 0;
            if (rotation1 == rotation2)
                numEqualities++;
            if (rotation2 == rotation3)
                numEqualities++;
            if (rotation3 == rotation1)
                numEqualities++;

            if (numEqualities == 3)
                return ePositionWithinTriangleState.INSIDE;

            if (numNoRotations == 1 && numEqualities == 1)
                return ePositionWithinTriangleState.ON_EDGE;

            return ePositionWithinTriangleState.OUTSIDE;
        }

        public static Vector2 GetPositionBisectingAngle(Vector2 prevPos, Vector2 pos, Vector2 nextPos, eRotationDirection rotationDirection, float thickness)
        {
            Vector2 aVec = pos - prevPos;
            Vector2 bVec = nextPos - pos;

            float angle = GetAngleBetweenVectors(-aVec, bVec);
            float trainAngle = 180f - angle;
            float bisectAngle = angle / 2f;

            float distance = thickness / Mathf.Cos(trainAngle / 2f);

            eRotationDirection trainRotation = GetVectorTrainRotationDirection(aVec, bVec);
            float rotateAngle = (rotationDirection == trainRotation) ? bisectAngle : trainAngle + bisectAngle;
            if (rotationDirection == eRotationDirection.CLOCKWISE)
                rotateAngle = -rotateAngle;

            Vector2 vecToPos = bVec;
            vecToPos.Normalize();
            vecToPos *= distance;
            vecToPos = RotateVector(vecToPos, rotateAngle);
            Vector2 posBisectingAngle = pos + vecToPos;

            return posBisectingAngle;
        }

        public static float GetAngleBetweenVectors(Vector2 vec1, Vector2 vec2)
        {
            vec1.Normalize();
            vec2.Normalize();

            float cosAngle = Vector2.Dot(vec1, vec2);
            float angle = Mathf.Acos(cosAngle);

            return angle;
        }

        public static Vector2 CreateVector(float angle, float length)
        {
            Vector3 vec3 = new Vector3(length, 0f, 0f);

            float rads = Mathf.ToRadians(angle);
            Matrix rotation = Matrix.CreateRotationZ(rads);

            vec3 = Vector3.Transform(vec3, rotation);

            return new Vector2(vec3.X, vec3.Y);
        }

        public static Vector2 RotateVector(Vector2 vector, float angle)
        {
            Vector3 vec3 = new Vector3(vector, 0f);

            float rads = Mathf.ToRadians(angle);
            Matrix rotation = Matrix.CreateRotationZ(rads);

            vec3 = Vector3.Transform(vec3, rotation);

            return new Vector2(vec3.X, vec3.Y);
        }

        public static Vector3 RotateVectorAboutX(Vector3 vector, float angle)
        {
            Matrix transform = Matrix.CreateRotationX(Mathf.ToRadians(angle));
            return Vector3.Transform(vector, transform);
        }
        public static Vector3 RotateVectorAboutY(Vector3 vector, float angle)
        {
            Matrix transform = Matrix.CreateRotationY(Mathf.ToRadians(angle));
            return Vector3.Transform(vector, transform);
        }
        public static Vector3 RotateVectorAboutZ(Vector3 vector, float angle)
        {
            Matrix transform = Matrix.CreateRotationZ(Mathf.ToRadians(angle));
            return Vector3.Transform(vector, transform);
        }
        public static Vector3 RotateVectorAboutAxis(Vector3 vector, Vector3 axis, float angle)
        {
            Matrix transform = Matrix.CreateFromAxisAngle(axis, Mathf.ToRadians(angle));
            return Vector3.Transform(vector, transform);
        }

        public static float GetPointDistanceFromLine(Vector2 linePos1, Vector2 linePos2, Vector2 point, out float lineLengthFactor)
        {
            Vector3 lineVec3 = new Vector3(linePos2.X - linePos1.X, linePos2.Y - linePos1.Y, 0f);
            Vector3 pointVec3 = new Vector3(point.X - linePos1.X, point.Y - linePos1.Y, 0f);
            Vector2 perpLineVec = new Vector2(lineVec3.Y, -lineVec3.X);

            lineLengthFactor = (perpLineVec.Y * (point.X - linePos1.X) - perpLineVec.X * (point.Y - linePos1.Y)) / (perpLineVec.Y * lineVec3.X - perpLineVec.X * lineVec3.Y);

            return (Vector3.Cross(lineVec3, pointVec3).Length() / lineVec3.Length());
        }
    }
}
