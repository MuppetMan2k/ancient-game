﻿using System.Collections.Generic;

namespace AncientGame
{
    static class StatueMovementOptionUtility
    {
        /* -------- Public Methods -------- */
        public static StatueMovementOptionSet CalculateStatueMovementOptions(Statue s, GameManagerContainer gameManagers)
        {
            StatueMovementOptionSet optionSet = new StatueMovementOptionSet();

            StatueMovementCalculationState movementState = GetStatueMovementCalculationState(s);
            if (movementState == null)
                return null;

            if (movementState.LandTerritory != null)
                optionSet.MovementOptions.AddRange(GetStatuesMovementOptionsFromLandTerritory(movementState, s, gameManagers));
            if (movementState.SeaTerritory != null)
                optionSet.MovementOptions.AddRange(GetStatuesMovementOptionsFromSeaTerritory(movementState, s, gameManagers));
            if (movementState.Settlement != null)
                optionSet.MovementOptions.AddRange(GetStatuesMovementOptionsFromSettlement(movementState, s, gameManagers));

            return optionSet;
        }

        /* -------- Private Methods --------- */
        private static StatueMovementCalculationState GetStatueMovementCalculationState(Statue s)
        {
            StatueMovementCalculationState movementState = null;

            if (s.Position.IsInLandTerritory)
                movementState = new StatueMovementCalculationState(s.Position.LandTerritory, null, null, s.MovementPoints, s.AdditionalMovementPoints);
            if (s.Position.IsInSeaTerritory)
                movementState = new StatueMovementCalculationState(null, s.Position.SeaTerritory, null, s.MovementPoints, s.AdditionalMovementPoints);
            if (s.Position.IsInSettlement)
                movementState = new StatueMovementCalculationState(null, null, s.Position.Settlement, s.MovementPoints, s.AdditionalMovementPoints);

            return movementState;
        }

        private static List<StatueMovementOption> GetStatuesMovementOptionsFromLandTerritory(StatueMovementCalculationState state, Statue s, GameManagerContainer gameManagers)
        {
            List<StatueMovementOption> options = new List<StatueMovementOption>();

            int totalMovementPoints = s.MovementPoints + s.AdditionalMovementPoints;

            Faction statueFaction = s.OwningFaction;
            LandTerritory landTerritory = state.LandTerritory;
            Faction landTerritoryOwningFaction = landTerritory.Variables.OwningFaction;
            Faction landTerritoryOccupyingFaction = landTerritory.Variables.OccupyingFaction;

            int cost;
            bool canAfford;

            // Remain
            options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.REMAIN_LAND, landTerritory, 0, true));

            // To settlement
            cost = landTerritory.GetTerritoryToSettlementMovementPointCost();
            canAfford = (totalMovementPoints >= cost);

            if (statueFaction == landTerritoryOwningFaction && landTerritoryOccupyingFaction == null)
                options.Add(StatueMovementOption.CreateForSettlement(eStatueMovementType.GARRISON_LAND, landTerritory.Settlement, cost, canAfford));
            else if (statueFaction == landTerritoryOwningFaction && landTerritoryOccupyingFaction != null)
                options.Add(StatueMovementOption.CreateForSettlement(eStatueMovementType.REPEL_OCCUPIERS, landTerritory.Settlement, cost, canAfford));
            else if (statueFaction != landTerritoryOwningFaction)
            {
                if (landTerritoryOccupyingFaction != null && landTerritoryOccupyingFaction == statueFaction)
                    options.Add(StatueMovementOption.CreateForSettlement(eStatueMovementType.GARRISON_LAND, landTerritory.Settlement, cost, canAfford));
                else if (landTerritoryOccupyingFaction == null && statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(landTerritoryOwningFaction))
                    options.Add(StatueMovementOption.CreateForSettlement(eStatueMovementType.BESIEGE_LAND, landTerritory.Settlement, cost, canAfford));
                else if (landTerritoryOccupyingFaction != null && (statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(landTerritoryOwningFaction) || statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(landTerritoryOccupyingFaction)))
                    options.Add(StatueMovementOption.CreateForSettlement(eStatueMovementType.BESIEGE_LAND, landTerritory.Settlement, cost, canAfford));
            }

            // To adjacent land territory
            foreach (AdjacentLandTerritory alt in landTerritory.AdjacentTerritories)
            {
                if (alt.LandTerritory.IsSettlementOnly)
                    continue;

                cost = alt.GetMovementPointCost(landTerritory, gameManagers);
                canAfford = (totalMovementPoints >= cost);

                Faction aLandTerritoryOwningFaction = alt.LandTerritory.Variables.OwningFaction;
                Faction aLandTerritoryOccupyingFaction = alt.LandTerritory.Variables.OccupyingFaction;

                if (statueFaction == aLandTerritoryOwningFaction && aLandTerritoryOccupyingFaction == null)
                    options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.FREE_MOVEMENT_LAND, alt.LandTerritory, cost, canAfford));
                else if (statueFaction == aLandTerritoryOwningFaction && aLandTerritoryOccupyingFaction != null)
                    options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.ATTACK_LAND, alt.LandTerritory, cost, canAfford));
                else if (statueFaction != aLandTerritoryOwningFaction)
                {
                    if (aLandTerritoryOccupyingFaction == null && statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(aLandTerritoryOwningFaction))
                        options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.ATTACK_LAND, alt.LandTerritory, cost, canAfford));
                    else if (aLandTerritoryOccupyingFaction == null && statueFaction.Variables.DiplomaticContractSet.HasMilitaryAccessContractWithFaction(aLandTerritoryOwningFaction))
                        options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.FREE_MOVEMENT_LAND, alt.LandTerritory, cost, canAfford));
                    else if (aLandTerritoryOccupyingFaction == null && !statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(aLandTerritoryOwningFaction))
                        options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.TRESPASS_LAND, alt.LandTerritory, cost, canAfford));
                    else if (aLandTerritoryOccupyingFaction != null)
                    {
                        if (statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(aLandTerritoryOwningFaction) || statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(aLandTerritoryOccupyingFaction))
                            options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.ATTACK_LAND, alt.LandTerritory, cost, canAfford));
                        else
                            options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.TRESPASS_LAND, alt.LandTerritory, cost, canAfford));
                    }
                }
            }

            return options;
        }
        private static List<StatueMovementOption> GetStatuesMovementOptionsFromSeaTerritory(StatueMovementCalculationState state, Statue s, GameManagerContainer gameManagers)
        {
            List<StatueMovementOption> options = new List<StatueMovementOption>();

            int totalMovementPoints = s.MovementPoints + s.AdditionalMovementPoints;

            Faction statueFaction = s.OwningFaction;
            SeaTerritory seaTerritory = state.SeaTerritory;

            int cost;
            bool canAfford;

            // Remain
            options.Add(StatueMovementOption.CreateForSeaTerritory(eStatueMovementType.REMAIN_SEA, seaTerritory, 0, true));

            // To adjacent sea territory
            foreach (AdjacentSeaTerritory ast in seaTerritory.AdjacentSeaTerritories)
            {
                cost = ast.GetMovementPointCost(seaTerritory, gameManagers);
                canAfford = (totalMovementPoints >= cost);

                options.Add(StatueMovementOption.CreateForSeaTerritory(eStatueMovementType.FREE_MOVEMENT_SEA, ast.SeaTerritory, cost, canAfford));
            }

            // To adjacent land territory
            CoastalSeaTerritory coastalSeaTerritory = seaTerritory as CoastalSeaTerritory;
            if (s.CanTraverseLand() && coastalSeaTerritory != null)
            {
                foreach (AdjacentLandTerritory alt in coastalSeaTerritory.AdjacentLandTerritories)
                {
                    if (alt.LandTerritory.IsSettlementOnly)
                        continue;

                    cost = alt.GetMovementPointCost(coastalSeaTerritory, gameManagers);
                    canAfford = (totalMovementPoints >= cost);

                    Faction landTerritoryOwningFaction = alt.LandTerritory.Variables.OwningFaction;
                    Faction landTerritoryOccupyingFaction = alt.LandTerritory.Variables.OccupyingFaction;

                    if (statueFaction == landTerritoryOwningFaction && landTerritoryOccupyingFaction == null)
                        options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.FREE_MOVEMENT_LAND, alt.LandTerritory, cost, canAfford));
                    else if (statueFaction == landTerritoryOwningFaction && landTerritoryOccupyingFaction != null)
                        options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.ATTACK_LAND, alt.LandTerritory, cost, canAfford));
                    else if (statueFaction != landTerritoryOwningFaction)
                    {
                        if (landTerritoryOccupyingFaction == null && statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(landTerritoryOwningFaction))
                            options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.ATTACK_LAND, alt.LandTerritory, cost, canAfford));
                        else if (landTerritoryOccupyingFaction == null && statueFaction.Variables.DiplomaticContractSet.HasMilitaryAccessContractWithFaction(landTerritoryOwningFaction))
                            options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.FREE_MOVEMENT_LAND, alt.LandTerritory, cost, canAfford));
                        else if (landTerritoryOccupyingFaction == null && !statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(landTerritoryOwningFaction))
                            options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.TRESPASS_LAND, alt.LandTerritory, cost, canAfford));
                        else if (landTerritoryOccupyingFaction != null)
                        {
                            if (statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(landTerritoryOwningFaction) || statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(landTerritoryOccupyingFaction))
                                options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.ATTACK_LAND, alt.LandTerritory, cost, canAfford));
                            else
                                options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.TRESPASS_LAND, alt.LandTerritory, cost, canAfford));
                        }
                    }
                }
            }

            // To adjacent settlement
            if (coastalSeaTerritory != null)
            {
                foreach (AdjacentLandTerritory alt in coastalSeaTerritory.AdjacentLandTerritories)
                {
                    if (alt.LandTerritory.PortAdjacentSeaTerritory == null || alt.LandTerritory.PortAdjacentSeaTerritory.SeaTerritory != seaTerritory)
                        continue;

                    cost = alt.GetMovementPointCostToSettlement(coastalSeaTerritory, gameManagers);
                    canAfford = (totalMovementPoints >= cost);

                    Faction landTerritoryOwningFaction = alt.LandTerritory.Variables.OwningFaction;
                    Faction landTerritoryOccupyingFaction = alt.LandTerritory.Variables.OccupyingFaction;

                    if (statueFaction == landTerritoryOwningFaction && landTerritoryOccupyingFaction == null)
                        options.Add(StatueMovementOption.CreateForSettlement(eStatueMovementType.GARRISON_SEA, alt.LandTerritory.Settlement, cost, canAfford));
                    else if (landTerritoryOccupyingFaction != null && landTerritoryOccupyingFaction == statueFaction)
                        options.Add(StatueMovementOption.CreateForSettlement(eStatueMovementType.GARRISON_SEA, alt.LandTerritory.Settlement, cost, canAfford));
                    else if (s.CanTraverseLand())
                    {
                        if (statueFaction == landTerritoryOwningFaction && landTerritoryOccupyingFaction != null)
                            options.Add(StatueMovementOption.CreateForSettlement(eStatueMovementType.REPEL_OCCUPIERS, alt.LandTerritory.Settlement, cost, canAfford));
                        else if (statueFaction != landTerritoryOwningFaction && landTerritoryOccupyingFaction == null && statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(landTerritoryOwningFaction))
                            options.Add(StatueMovementOption.CreateForSettlement(eStatueMovementType.BESIEGE_LAND, alt.LandTerritory.Settlement, cost, canAfford));
                        else if (statueFaction != landTerritoryOwningFaction && landTerritoryOccupyingFaction != null && (statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(landTerritoryOwningFaction) || statueFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(landTerritoryOccupyingFaction)))
                            options.Add(StatueMovementOption.CreateForSettlement(eStatueMovementType.BESIEGE_LAND, alt.LandTerritory.Settlement, cost, canAfford));
                    }
                }
            }

            return options;
        }
        private static List<StatueMovementOption> GetStatuesMovementOptionsFromSettlement(StatueMovementCalculationState state, Statue s, GameManagerContainer gameManagers)
        {
            List<StatueMovementOption> options = new List<StatueMovementOption>();

            int totalMovementPoints = s.MovementPoints + s.AdditionalMovementPoints;

            LandTerritory landTerritory = state.Settlement.ParentLandTerritory;

            int cost;
            bool canAfford;

            // Remain
            options.Add(StatueMovementOption.CreateForSettlement(eStatueMovementType.REMAIN_LAND, state.Settlement, 0, true));

            // To land territory
            if (s.CanTraverseLand())
            {
                cost = landTerritory.GetTerritoryToSettlementMovementPointCost();
                canAfford = (totalMovementPoints >= cost);

                options.Add(StatueMovementOption.CreateForLandTerritory(eStatueMovementType.FREE_MOVEMENT_LAND, landTerritory, cost, canAfford));
            }

            // To sea territory
            if (s.CanTraverseWater() && landTerritory.PortAdjacentSeaTerritory != null)
            {
                cost = landTerritory.PortAdjacentSeaTerritory.GetMovementPointCost(landTerritory, gameManagers);
                canAfford = (totalMovementPoints >= cost);
                SeaTerritory adjacentSeaTerritory = landTerritory.PortAdjacentSeaTerritory.SeaTerritory;

                options.Add(StatueMovementOption.CreateForSeaTerritory(eStatueMovementType.FREE_MOVEMENT_SEA, adjacentSeaTerritory, cost, canAfford));
            }

            return options;
        }
    }
}
