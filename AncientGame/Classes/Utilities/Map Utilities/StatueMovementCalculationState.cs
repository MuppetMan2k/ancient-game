﻿namespace AncientGame
{
    class StatueMovementCalculationState
    {
        /* -------- Properties -------- */
        public LandTerritory LandTerritory { get; private set; }
        public SeaTerritory SeaTerritory { get; private set; }
        public Settlement Settlement { get; private set; }
        public int RemainingMovementPoints { get; private set; }
        public int AdditionalMovementPoints { get; private set; }

        /* -------- Constructors -------- */
        public StatueMovementCalculationState(LandTerritory inLandTerritory, SeaTerritory inSeaTerritory, Settlement inSettlement, int inRemainingMovementPoints, int inAdditionalMovementPoints)
        {
            LandTerritory = inLandTerritory;
            SeaTerritory = inSeaTerritory;
            Settlement = inSettlement;
            RemainingMovementPoints = inRemainingMovementPoints;
            AdditionalMovementPoints = inAdditionalMovementPoints;
        }
    }
}
