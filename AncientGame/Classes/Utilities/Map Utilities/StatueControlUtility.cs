﻿namespace AncientGame
{
    static class StatueControlUtility
    {
        /* -------- Public Methods -------- */
        public static bool LandForcesAreSmallEnoughToMerge(LandForce landForce1, LandForce landForce2)
        {
            Army army1 = landForce1 as Army;
            Army army2 = landForce2 as Army;

            // Land forces do not have a maximum size
            if (army1 == null && army2 == null)
                return true;

            int maxUnitNum;
            if (army1 != null)
                maxUnitNum = army1.GetMaxUnitNum();
            else
                maxUnitNum = army2.GetMaxUnitNum();

            // Ensure total units will be <= the max unit number
            return ((landForce1.Units.Count + landForce2.Units.Count) <= maxUnitNum);
        }
        public static bool SeaForcesAreSmallEnoughToMerge(SeaForce seaForce1, SeaForce seaForce2)
        {
            Navy navy1 = seaForce1 as Navy;
            Navy navy2 = seaForce2 as Navy;

            // Sea forces do not have a maximum size
            if (navy1 == null && navy2 == null)
                return true;

            int maxUnitNum;
            if (navy1 != null)
                maxUnitNum = navy1.GetMaxUnitNum();
            else
                maxUnitNum = navy2.GetMaxUnitNum();

            // Ensure total units will be <= the max unit number
            return ((seaForce1.Units.Count + seaForce2.Units.Count) <= maxUnitNum);
        }
    }
}
