﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eUITextShade
    {
        LIGHT,
        DARK
    }
    
    enum eUIAttitude
    {
        POSITIVE,
        NEUTRAL,
        NEGATIVE
    }

    static class UIUtilities
    {
        /* -------- Private Fields -------- */
        private const float BACKGROUND_LUMINANCE_FOR_DARK_TEXT_SHADE = 0.6f;

        /* -------- Public Methods -------- */
        public static Point GetUIRectangleTopLeft(Point position, eUIAnchor anchor, Rectangle uiRect, UIEntity parent, GraphicsComponent g)
        {
            Point anchorPoint = GetUIAnchorPoint(anchor, parent, g);

            switch (anchor)
            {
                case eUIAnchor.TOP_LEFT:
                    return new Point(anchorPoint.X + position.X, anchorPoint.Y + position.Y);
                case eUIAnchor.TOP_CENTER:
                    return new Point(anchorPoint.X + position.X - uiRect.Width / 2, anchorPoint.Y + position.Y);
                case eUIAnchor.TOP_RIGHT:
                    return new Point(anchorPoint.X - position.X - uiRect.Width, anchorPoint.Y + position.Y);
                case eUIAnchor.CENTER_LEFT:
                    return new Point(anchorPoint.X + position.X, anchorPoint.Y + position.Y - uiRect.Height / 2);
                case eUIAnchor.CENTER_CENTER:
                    return new Point(anchorPoint.X + position.X - uiRect.Width / 2, anchorPoint.Y + position.Y - uiRect.Height / 2);
                case eUIAnchor.CENTER_RIGHT:
                    return new Point(anchorPoint.X - position.X - uiRect.Width, anchorPoint.Y + position.Y - uiRect.Height / 2);
                case eUIAnchor.BOTTOM_LEFT:
                    return new Point(anchorPoint.X + position.X, anchorPoint.Y - position.Y - uiRect.Height);
                case eUIAnchor.BOTTOM_CENTER:
                    return new Point(anchorPoint.X + position.X - uiRect.Width / 2, anchorPoint.Y - position.Y - uiRect.Height);
                case eUIAnchor.BOTTOM_RIGHT:
                    return new Point(anchorPoint.X - position.X - uiRect.Width, anchorPoint.Y - position.Y - uiRect.Height);
                default:
                    Debug.LogUnrecognizedValueWarning(anchor);
                    return new Point(anchorPoint.X + position.X, anchorPoint.Y + position.Y);
            }
        }

        public static Point GetUIAnchorPoint(eUIAnchor anchor, UIEntity parent, GraphicsComponent g)
        {
            int parentTop = (parent == null) ? 0 : parent.Rectangle.Top;
            int parentBottom = (parent == null) ? g.ScreenHeight : parent.Rectangle.Bottom;
            int parentCenterY = (parent == null) ? g.HalfScreenHeight : parent.Rectangle.Center.Y;
            int parentLeft = (parent == null) ? 0 : parent.Rectangle.Left;
            int parentRight = (parent == null) ? g.ScreenWidth : parent.Rectangle.Right;
            int parentCenterX = (parent == null) ? g.HalfScreenWidth : parent.Rectangle.Center.X;

            switch (anchor)
            {
                case eUIAnchor.TOP_LEFT:
                    return new Point(parentLeft, parentTop);
                case eUIAnchor.TOP_CENTER:
                    return new Point(parentCenterX, parentTop);
                case eUIAnchor.TOP_RIGHT:
                    return new Point(parentRight, parentTop);
                case eUIAnchor.CENTER_LEFT:
                    return new Point(parentLeft, parentCenterY);
                case eUIAnchor.CENTER_CENTER:
                    return new Point(parentCenterX, parentCenterY);
                case eUIAnchor.CENTER_RIGHT:
                    return new Point(parentRight, parentCenterY);
                case eUIAnchor.BOTTOM_LEFT:
                    return new Point(parentLeft, parentBottom);
                case eUIAnchor.BOTTOM_CENTER:
                    return new Point(parentCenterX, parentBottom);
                case eUIAnchor.BOTTOM_RIGHT:
                    return new Point(parentRight, parentBottom);
                default:
                    Debug.LogUnrecognizedValueWarning(anchor);
                    return new Point(parentLeft, parentTop);
            }
        }

        public static bool MouseIsInRectangle(Rectangle rect, InputComponent i)
        {
            Point mousePoint = i.GetMousePosition();

            if (mousePoint.X < rect.Left)
                return false;

            if (mousePoint.X > rect.Right)
                return false;

            if (mousePoint.Y < rect.Top)
                return false;

            if (mousePoint.Y > rect.Bottom)
                return false;

            return true;
        }

        public static eUITextShade GetTextShade(Color backgroundColor)
        {
            float r = (float)backgroundColor.R / 255f;
            float g = (float)backgroundColor.G / 255f;
            float b = (float)backgroundColor.B / 255f;
            float luminance = 0.2126f * r + 0.7152f * g + 0.0722f * b;

            if (luminance >= BACKGROUND_LUMINANCE_FOR_DARK_TEXT_SHADE)
                return eUITextShade.DARK;

            return eUITextShade.LIGHT;
        }

        public static void SBWrapTextureAcrossRectangle(GraphicsComponent g, Texture2D tex, Rectangle rect)
        {
            int numWholeXWraps = rect.Width / tex.Width;
            int numWholeYWraps = rect.Height / tex.Height;

            for (int x = 0; x < numWholeXWraps; x++)
            {
                for (int y = 0; y < numWholeYWraps; y++)
                {
                    Rectangle texRect = new Rectangle(rect.X + x * tex.Width, rect.Y + y * tex.Height, tex.Width, tex.Height);
                    g.SpriteBatch.Draw(tex, texRect, Color.White);
                }
            }

            int xPixelsRemaining = rect.Width - numWholeXWraps * tex.Width;
            int yPixelsRemaining = rect.Height - numWholeYWraps * tex.Height;

            // Down right hand edge
            if (xPixelsRemaining > 0)
            {
                for (int i = 0; i < numWholeYWraps; i++)
                {
                    Rectangle partialRect = new Rectangle(rect.Right - xPixelsRemaining, rect.Y + i * tex.Height, xPixelsRemaining, tex.Height);
                    Rectangle sourceRect = new Rectangle(0, 0, xPixelsRemaining, tex.Height);
                    g.SpriteBatch.Draw(tex, partialRect, sourceRect, Color.White);
                }
            }

            // Across lower edge
            if (yPixelsRemaining > 0)
            {
                for (int i = 0; i < numWholeXWraps; i++)
                {
                    Rectangle partialRect = new Rectangle(rect.X + i * tex.Width, rect.Bottom - yPixelsRemaining, tex.Width, yPixelsRemaining);
                    Rectangle sourceRect = new Rectangle(0, 0, tex.Width, yPixelsRemaining);
                    g.SpriteBatch.Draw(tex, partialRect, sourceRect, Color.White);
                }
            }

            // Bottom right corner
            if (xPixelsRemaining > 0 && yPixelsRemaining > 0)
            {
                Rectangle partialRect = new Rectangle(rect.Right - xPixelsRemaining, rect.Bottom - yPixelsRemaining, xPixelsRemaining, yPixelsRemaining);
                Rectangle sourceRect = new Rectangle(0, 0, xPixelsRemaining, yPixelsRemaining);
                g.SpriteBatch.Draw(tex, partialRect, sourceRect, Color.White);
            }
        }

        public static string GetFloatDisplayString(float val)
        {
            if (FloatIsApproxZero(val) && val != 0f)
                val = 0.01f * Mathf.SignF(val);

            return val.ToString("0.00");
        }
        public static string GetBiDirectionalFloatDisplayString(float val)
        {
            string valString = GetFloatDisplayString(val);

            if (val > 0f)
                valString = String.Format(Localization.Localize("text_positive"), valString);

            return valString;
        }
        public static string GetBiDirectionalPercentageFloatDisplayString(float val)
        {
            return String.Format(Localization.Localize("text_percentage"), GetBiDirectionalFloatDisplayString(val));
        }
        public static string GetPercentageFloatDisplayString(float val)
        {
            return String.Format(Localization.Localize("text_percentage"), GetFloatDisplayString(val));
        }
        public static bool FloatIsApproxZero(float val)
        {
            int roundedInt = Mathf.Round(val * 100f);
            return (roundedInt == 0);
        }
        public static string GetIntDisplayString(int val)
        {
            return val.ToString("0");
        }
        public static string GetLargeIntDisplayString(int val)
        {
            if (val > 1000000)
            {
                float numMil = (float)val / 1000000f;
                string milString = numMil.ToString("0.00");
                return String.Format(Localization.Localize("text_million"), milString);
            }

            if (val < 1000)
            {
                string oneThousString = String.Format(Localization.Localize("text_thousand"), "1");
                return String.Format(Localization.Localize("text_less_than"), oneThousString);
            }

            int numThous = Mathf.Ceiling((float)val / 1000f);
            return String.Format(Localization.Localize("text_thousand"), numThous.ToString("0"));
        }

        public static eUIAttitude ReverseUIAttitude(eUIAttitude attitude)
        {
            switch (attitude)
            {
                case eUIAttitude.POSITIVE:
                    return eUIAttitude.NEGATIVE;
                case eUIAttitude.NEGATIVE:
                    return eUIAttitude.POSITIVE;
                case eUIAttitude.NEUTRAL:
                    return eUIAttitude.NEUTRAL;
                default:
                    Debug.LogUnrecognizedValueWarning(attitude);
                    return eUIAttitude.NEUTRAL;
            }
        }
    }
}
