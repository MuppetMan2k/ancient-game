﻿using System;
using System.Collections.Generic;

namespace AncientGame
{
    class GeneralUtilities
    {
        /* -------- Public Methods -------- */
        public static T GetFromCircularList<T>(List<T> list, int index)
        {
            index = index % list.Count;
            if (index < 0)
                index += list.Count;

            return list[index];
        }
        public static T GetFromCircularArray<T>(T[] array, int index)
        {
            index = index % array.Length;
            if (index < 0)
                index += array.Length;

            return array[index];
        }
        public static List<T> ShallowCloneList<T>(List<T> listToClone)
        {
            List<T> newList = new List<T>();

            foreach (T t in listToClone)
                newList.Add(t);

            return newList;
        }

        public static T[] GetEnumValues<T>()
        {
            return (T[])Enum.GetValues(typeof(T));
        }
    }
}
