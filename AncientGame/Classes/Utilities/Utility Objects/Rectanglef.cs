﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    struct Rectanglef
    {
        /* -------- Properties -------- */
        public Vector2 Center { get { return new Vector2(x + width / 2f, y + height / 2f); } }
        public float Top { get { return y + height; } }
        public float Bottom { get { return y; } }
        public float Left { get { return x; } }
        public float Right { get { return x + width; } }

        /* -------- Public Fields -------- */
        public float x;
        public float y;
        public float width;
        public float height;

        /* -------- Constructors -------- */
        public Rectanglef(float inX, float inY, float inWidth, float inHeight)
        {
            x = inX;
            y = inY;
            width = inWidth;
            height = inHeight;
        }

        /* -------- Public Methods -------- */
        public bool ContainsPosition(Vector2 position)
        {
            if (position.X < Left)
                return false;
            if (position.X > Right)
                return false;
            if (position.Y > Top)
                return false;
            if (position.Y < Bottom)
                return false;

            return true;
        }
        public Rectanglef Unify(Rectanglef rectangle)
        {
            float newTop = Mathf.Max(Top, rectangle.Top);
            float newBottom = Mathf.Min(Bottom, rectangle.Bottom);
            float newLeft = Mathf.Min(Left, rectangle.Left);
            float newRight = Mathf.Max(Right, rectangle.Right);

            return new Rectanglef(newLeft, newBottom, newRight - newLeft, newTop - newBottom);
        }

        /* -------- Static Methods -------- */
        public static Rectanglef CreateFromPoints(Vector2[] points)
        {
            float left = points[0].X;
            float right = points[0].X;
            float top = points[0].Y;
            float bottom = points[0].Y;

            for (int i = 1; i < points.Length; i++)
            {
                Vector2 point = points[i];

                if (point.X < left)
                    left = point.X;
                if (point.X > right)
                    right = point.X;
                if (point.Y > top)
                    top = point.Y;
                if (point.Y < bottom)
                    bottom = point.Y;
            }

            return new Rectanglef(left, bottom, right - left, top - bottom);
        }
        public static Rectanglef CreateFromPoints(List<Vector2> points)
        {
            return CreateFromPoints(points.ToArray());
        }
    }
}
