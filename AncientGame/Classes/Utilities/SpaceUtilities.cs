﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    static class SpaceUtilities
    {
        /* -------- Public Methods -------- */
        public static Point ConvertSSToPS(Vector3 ssVec, GraphicsComponent g)
        {
            Point psPoint = new Point();
            psPoint.X = Mathf.Round(g.ScreenWidth * (ssVec.X + 1f) / 2f);
            psPoint.Y = Mathf.Round(g.ScreenHeight * (1f - ssVec.Y) / 2f);

            return psPoint;
        }
        public static Vector3 ConvertPSToSS(Point psPoint, GraphicsComponent g)
        {
            Vector3 ssVec = new Vector3();
            ssVec.X = (2f * (float)psPoint.X / (float)g.ScreenWidth) - 1f;
            ssVec.Y = 1f - (2f * (float)psPoint.Y / (float)g.ScreenHeight);

            return ssVec;
        }

        public static Vector2 ConvertPSToPSVec(Point psPoint)
        {
            Vector2 psVec = new Vector2((float)psPoint.X, (float)psPoint.Y);
            return psVec;
        }
        public static Point ConvertPSVecToPS(Vector2 psVec)
        {
            Point psPoint = new Point(Mathf.Round(psVec.X), Mathf.Round(psVec.Y));
            return psPoint;
        }

        public static Vector3 ConvertMSToGS(Vector2 msVec)
        {
            return new Vector3(msVec, 0f);
        }
        public static Vector2 ConvertGSToMS(Vector3 gsVec)
        {
            return new Vector2(gsVec.X, gsVec.Y);
        }

        public static bool TryConvertPSToMS(Point psPoint, GraphicsComponent g, CameraManager cm, out Vector2 msVec)
        {
            return TryConvertSSToMS(ConvertPSToSS(psPoint, g), cm, out msVec);
        }
        public static bool TryConvertSSToMS(Vector3 ssVec, CameraManager cm, out Vector2 msVec)
        {
            Matrix inverseTransform = Matrix.Invert(cm.Camera.ViewMatrix * cm.Camera.ProjectionMatrix);
            Vector3 mouseGSPos = Vector3.Transform(ssVec, inverseTransform);

            Vector3 cameraToMouseGSPos = mouseGSPos - cm.Camera.Position;

            if (cameraToMouseGSPos.Z >= 0f)
            {
                msVec = new Vector2(0f);
                return false;
            }

            Vector3 mapGSPos = mouseGSPos - (mouseGSPos.Z / cameraToMouseGSPos.Z) * cameraToMouseGSPos;
            msVec = ConvertGSToMS(mapGSPos);

            return true;
        }

        public static Point ConvertGSToPS(Vector3 gsPos, Camera c, GraphicsComponent g)
        {
            Matrix transform = c.ViewMatrix * c.ProjectionMatrix;

            Vector4 gsPosVec4 = new Vector4(gsPos, 1f);
            Vector4 ssPosVec4 = Vector4.Transform(gsPosVec4, transform);
            Vector3 ssPos = new Vector3(ssPosVec4.X / ssPosVec4.W, ssPosVec4.Y / ssPosVec4.W, ssPosVec4.Z / ssPosVec4.W);

            return ConvertSSToPS(ssPos, g);
        }
        public static Point ConvertMSToPS(Vector2 msPos, Camera c, GraphicsComponent g)
        {
            Vector3 gsPos = ConvertMSToGS(msPos);
            return ConvertGSToPS(gsPos, c, g);
        }
    }
}
