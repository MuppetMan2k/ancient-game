﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    static class TextUtilities
    {
        /* -------- Public Methods -------- */
        public static List<TextWord> SplitTextSegmentsIntoWords(List<ColoredTextSegment> textSegments, SpriteFont font)
        {
            List<TextWord> words = new List<TextWord>();

            foreach (ColoredTextSegment cts in textSegments)
            {
                string[] splitWords = cts.Text.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string w in splitWords)
                {
                    words.Add(new TextWord(w, cts.Color, font));
                }
            }

            return words;
        }

        public static List<TextLine> FormWordsIntoLines(List<TextWord> words, int maxWidth, int maxLines, SpriteFont font)
        {
            List<TextLine> lines = new List<TextLine>();
            int spaceSize = Mathf.Round(font.MeasureString(" ").X);

            TextLine line = new TextLine();
            foreach (TextWord w in words)
            {
                int lineLengthWithWord = line.GetLengthWithNewWord(w, spaceSize);

                if (lineLengthWithWord <= maxWidth || maxWidth == -1)
                {
                    // Have space on the line for the word, add it and move on
                    line.AddWord(w, spaceSize);
                    continue;
                }

                // No space for word

                if (line.Words.Count == 0)
                {
                    // Single word longer than max width. Let it overrun and continue
                    line.AddWord(w, spaceSize);
                    continue;
                }

                lines.Add(line);

                if (lines.Count >= maxLines)
                {
                    // Hit max line limit
                    return lines;
                }

                line = new TextLine();
                line.AddWord(w, spaceSize);
            }

            lines.Add(line);
            return lines;
        }

        public static bool TextAlignmentIsEquivalentToUIAnchor(eTextAlignment alignment, eUIAnchor anchor)
        {
            switch (alignment)
            {
                case eTextAlignment.LEFT:
                    return (anchor == eUIAnchor.TOP_LEFT || anchor == eUIAnchor.CENTER_LEFT || anchor == eUIAnchor.BOTTOM_LEFT);
                case eTextAlignment.CENTER:
                    return (anchor == eUIAnchor.TOP_CENTER || anchor == eUIAnchor.CENTER_CENTER || anchor == eUIAnchor.BOTTOM_CENTER);
                case eTextAlignment.RIGHT:
                    return (anchor == eUIAnchor.TOP_RIGHT || anchor == eUIAnchor.CENTER_RIGHT || anchor == eUIAnchor.BOTTOM_RIGHT);
                default:
                    Debug.LogUnrecognizedValueWarning(alignment);
                    return false;
            }
        }

        public static string ProcessNameLabelText(string text)
        {
            string processedText = "";

            foreach (char c in text)
            {
                if (char.IsLetter(c))
                    processedText += char.ToUpper(c);

                if (char.IsWhiteSpace(c))
                    processedText += c;
            }

            return processedText;
        }
    }
}
