﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    static class AssetLoader
    {
        /* -------- Private Fields -------- */
        // Constants
        private const string LOCALIZATION_PATH = "XML/Localization";
        private const string DEFAULT_TEXTURE_PATH = "Assets/Default Assets/MagentaChecker";
        private const string DEFAULT_SPRITE_FONT_PATH = "Assets/Default Assets/Arial12";
        private const string DEFAULT_SOUND_EFFECT_PATH = "Assets/Default Assets/SadTrombone";
        private const string RESOURCE_DATABASE_PATH = "XML/Data/Single Databases/ResourceDatabase";
        private const string CULTURE_DATABASE_PATH = "XML/Data/Single Databases/CultureDatabase";
        private const string LANGUAGE_DATABASE_PATH = "XML/Data/Single Databases/LanguageDatabase";
        private const string RELIGION_DATABASE_PATH = "XML/Data/Single Databases/ReligionDatabase";
        private const string INSTITUTION_DATABASE_PATH = "XML/Data/Single Databases/InstitutionDatabase";
        private const string LAW_DATBASE_PATH = "XML/Data/Single Databases/LawDatabase";
        private const string TAX_DATABASE_PATH = "XML/Data/Single Databases/TaxDatabase";
        private const string GOVERNMENT_TRAIT_DATABASE_PATH = "XML/Data/Single Databases/GovernmentTraitDatabase";
        private const string APPOINTMENT_DATABASE_PATH = "XML/Data/Single Databases/AppointmentDatabase";
        private const string MILITARY_ERA_DATABASE_PATH = "XML/Data/Single Databases/MilitaryEraDatabase";
        private const string GRAPHICS_CALIBRATION_DATA_PATH = "XML/Data/Calibration Datas/GraphicsCalibrationData";
        private const string CAMERA_CALIBRATION_DATA_PATH = "XML/Data/Calibration Datas/CameraCalibrationData";
        private const string LIGHTING_CALIBRATION_DATA_PATH = "XML/Data/Calibration Datas/LightingCalibrationData";
        private const string FACTION_CALIBRATION_DATA_PATH = "XML/Data/Calibration Datas/FactionCalibrationData";
        private const string CHARACTER_CALIBRATION_DATA_PATH = "XML/Data/Calibration Datas/CharacterCalibrationData";
        private const string UI_CALIBRATION_DATA_PATH = "XML/Data/Calibration Datas/UICalibrationData";
        private const string CAMPAIGN_CALIBRATION_DATA_PATH = "XML/Data/Calibration Datas/CampaignCalibrationData";
        private const string TRADE_CALIBRATION_DATA_PATH = "XML/Data/Calibration Datas/TradeCalibrationData";

        /* -------- Public Methods -------- */
        // Localization
        public static LocalizationListItem[] LoadGeneralLocalization(string language, ContentManager contentManager)
        {
            string path = LOCALIZATION_PATH + "/" + language + "/GeneralLocalization";
            try
            {
                LocalizationListItem[] localization = contentManager.Load<LocalizationListItem[]>(path);
                return localization;
            }
            catch (Exception e)
            {
                Debug.LogWarning("Tried to load general localization for language {0} but caught exception {1}", language, e.Message);
                return null;
            }
        }
        public static LocalizationListItem[] LoadLocalization(string id, string language, ContentManager contentManager)
        {
            string path = AssetLists.GetLocalizationPath(id);
            path = LOCALIZATION_PATH + "/" + language + "/" + path;

            try
            {
                LocalizationListItem[] localization = contentManager.Load<LocalizationListItem[]>(path);
                return localization;
            }
            catch (Exception e)
            {
                Debug.LogWarning("Tried to load localization {0} for language {1} but caught exception {2}", id, language, e.Message);
                return null;
            }
        }
        // Assets
        public static Texture2D LoadTexture(string id, ContentManager contentManager)
        {
            string path = AssetLists.GetTexturePath(id);

            if (string.IsNullOrEmpty(path))
            {
                Debug.LogWarning(string.Format("Failed to get content path for texture ID {0}", id));
                return GetDefaultTexture(contentManager);
            }

            Texture2D tex;

            try
            {
                tex = contentManager.Load<Texture2D>(path);
            }
            catch (Exception e)
            {
                Debug.LogWarning(string.Format("Tried to load texture ID {0} from path {1} but got exception {2}", id, path, e));
                tex = GetDefaultTexture(contentManager);
            }

            return tex;
        }
        public static SpriteFont LoadSpriteFont(string id, ContentManager contentManager)
        {
            string path = AssetLists.GetSpriteFontPath(id);

            if (string.IsNullOrEmpty(path))
            {
                Debug.LogWarning(string.Format("Failed to get content path for sprite font ID {0}", id));
                return GetDefaultSpriteFont(contentManager);
            }

            SpriteFont sf;

            try
            {
                sf = contentManager.Load<SpriteFont>(path);
            }
            catch (Exception e)
            {
                Debug.LogWarning(string.Format("Tried to load sprite font ID {0} from path {1} but got exception {2}", id, path, e));
                sf = GetDefaultSpriteFont(contentManager);
            }

            return sf;
        }
        public static SoundEffect LoadSoundEffect(string id, ContentManager contentManager)
        {
            string path = AssetLists.GetSoundEffectPath(id);

            if (string.IsNullOrEmpty(path))
            {
                Debug.LogWarning(string.Format("Failed to get content path for sound effect ID {0}", id));
                return GetDefaultSoundEffect(contentManager);
            }

            SoundEffect se;

            try
            {
                se = contentManager.Load<SoundEffect>(path);
            }
            catch (Exception e)
            {
                Debug.LogWarning(string.Format("Tried to load sound effect ID {0} from path {1} but got exception {2}", id, path, e));
                se = GetDefaultSoundEffect(contentManager);
            }

            return se;
        }
        public static Effect LoadEffect(string id, ContentManager contentManager)
        {
            string path = AssetLists.GetEffectPath(id);
            return contentManager.Load<Effect>(path);
        }
        public static NameLabelAtlas LoadNameLabelAtlas(string id, ContentManager contentManager)
        {
            string path = AssetLists.GetNameLabelAtlasPath(id);
            NameLabelAtlasData data = contentManager.Load<NameLabelAtlasData>(path);
            return new NameLabelAtlas(data, contentManager);
        }
        // Data
        public static MapData LoadMapData(string id, ContentManager contentManager)
        {
            string path = AssetLists.GetMapDataPath(id);
            return contentManager.Load<MapData>(path);
        }
        public static FactionDatabase LoadFactionDatabase(string id, ContentManager contentManager)
        {
            string path = AssetLists.GetFactionDatabasePath(id);
            return contentManager.Load<FactionDatabase>(path);
        }
        public static UnitDatabase LoadUnitDatabase(string id, ContentManager contentManager)
        {
            string path = AssetLists.GetUnitDatabasePath(id);
            return contentManager.Load<UnitDatabase>(path);
        }
        public static BuildingDatabase LoadBuildingDatabase(string id, ContentManager contentManager)
        {
            string path = AssetLists.GetBuildingDatabasePath(id);
            return contentManager.Load<BuildingDatabase>(path);
        }
        public static ResourceDatabase LoadResourceDatabase(ContentManager contentManager)
        {
            return contentManager.Load<ResourceDatabase>(RESOURCE_DATABASE_PATH);
        }
        public static CultureDatabase LoadCultureDatabase(ContentManager contentManager)
        {
            return contentManager.Load<CultureDatabase>(CULTURE_DATABASE_PATH);
        }
        public static LanguageDatabase LoadLanguageDatabase(ContentManager contentManager)
        {
            return contentManager.Load<LanguageDatabase>(LANGUAGE_DATABASE_PATH);
        }
        public static ReligionDatabase LoadReligionDatabase(ContentManager contentManager)
        {
            return contentManager.Load<ReligionDatabase>(RELIGION_DATABASE_PATH);
        }
        public static InstitutionDatabase LoadInstitutionDatabase(ContentManager contentManager)
        {
            return contentManager.Load<InstitutionDatabase>(INSTITUTION_DATABASE_PATH);
        }
        public static LawDatabase LoadLawDatabase(ContentManager contentManager)
        {
            return contentManager.Load<LawDatabase>(LAW_DATBASE_PATH);
        }
        public static TaxDatabase LoadTaxDatabase(ContentManager contentManager)
        {
            return contentManager.Load<TaxDatabase>(TAX_DATABASE_PATH);
        }
        public static GovernmentTraitDatabase LoadGovernmentTraitDatabase(ContentManager contentManager)
        {
            return contentManager.Load<GovernmentTraitDatabase>(GOVERNMENT_TRAIT_DATABASE_PATH);
        }
        public static AppointmentDatabase LoadAppointmentDatabase(ContentManager contentManager)
        {
            return contentManager.Load<AppointmentDatabase>(APPOINTMENT_DATABASE_PATH);
        }
        public static MilitaryEraDatabase LoadMilitaryEraDatabase(ContentManager contentManager)
        {
            string path = MILITARY_ERA_DATABASE_PATH;
            return contentManager.Load<MilitaryEraDatabase>(path);
        }
        public static CampaignStartStateData LoadCampaignStartStateData(string id, ContentManager contentManager)
        {
            string path = AssetLists.GetCampaignStartStateDataPath(id);
            return contentManager.Load<CampaignStartStateData>(path);
        }
        public static CampaignGameData LoadCampaignGameData(string id, ContentManager contentManager)
        {
            string path = AssetLists.GetCampaignGameDataPath(id);
            return contentManager.Load<CampaignGameData>(path);
        }
        // Calibration
        public static GraphicsCalibrationData LoadGraphicsCalibrationData(ContentManager contentManager)
        {
            return contentManager.Load<GraphicsCalibrationData>(GRAPHICS_CALIBRATION_DATA_PATH);
        }
        public static CameraCalibrationData LoadCameraCalibrationData(ContentManager contentManager)
        {
            return contentManager.Load<CameraCalibrationData>(CAMERA_CALIBRATION_DATA_PATH);
        }
        public static LightingCalibrationData LoadLightingCalibrationData(ContentManager contentManager)
        {
            return contentManager.Load<LightingCalibrationData>(LIGHTING_CALIBRATION_DATA_PATH);
        }
        public static FactionCalibrationData LoadFactionCalibrationData(ContentManager contentManager)
        {
            return contentManager.Load<FactionCalibrationData>(FACTION_CALIBRATION_DATA_PATH);
        }
        public static CharacterCalibrationData LoadCharacterCalibrationData(ContentManager contentManager)
        {
            return contentManager.Load<CharacterCalibrationData>(CHARACTER_CALIBRATION_DATA_PATH);
        }
        public static UICalibrationData LoadUICalibrationData(ContentManager contentManager)
        {
            return contentManager.Load<UICalibrationData>(UI_CALIBRATION_DATA_PATH);
        }
        public static CampaignCalibrationData LoadCampaignCalibrationData(ContentManager contentManager)
        {
            return contentManager.Load<CampaignCalibrationData>(CAMPAIGN_CALIBRATION_DATA_PATH);
        }
        public static TradeCalibrationData LoadTradeCalibrationData(ContentManager contentManager)
        {
            return contentManager.Load<TradeCalibrationData>(TRADE_CALIBRATION_DATA_PATH);
        }

        /* -------- Private Methods --------- */
        private static Texture2D GetDefaultTexture(ContentManager contentManager)
        {
            return (contentManager.Load<Texture2D>(DEFAULT_TEXTURE_PATH));
        }
        private static SpriteFont GetDefaultSpriteFont(ContentManager contentManager)
        {
            return (contentManager.Load<SpriteFont>(DEFAULT_SPRITE_FONT_PATH));
        }
        private static SoundEffect GetDefaultSoundEffect(ContentManager contentManager)
        {
            return (contentManager.Load<SoundEffect>(DEFAULT_SOUND_EFFECT_PATH));
        }
    }
}
