﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    static class AssetLists
    {
        /* -------- Private Fields -------- */
        // Texture asset lists
        private static AssetListItem[] basicTextureAssetList;
        private static AssetListItem[] uiTextureAssetList;
        private static AssetListItem[] mapTextureAssetList;
        private static AssetListItem[] statueTextureAssetList;
        private static AssetListItem[] buildingTextureAssetList;
        private static AssetListItem[] unitTextureAssetList;

        private static AssetListItem[] spriteFontAssetList;

        private static AssetListItem[] effectAssetList;

        private static AssetListItem[] soundEffectAssetList;

        private static AssetListItem[] nameLabelAtlasAssetList;

        private static AssetListItem[] localizationAssetList;

        private static AssetListItem[] mapDataAssetList;

        private static AssetListItem[] factionDatabaseAssetList;

        private static AssetListItem[] unitDatabaseAssetList;

        private static AssetListItem[] buildingDatabaseAssetList;

        private static AssetListItem[] campaignStartStateDataAssetList;

        private static AssetListItem[] campaignGameDataAssetList;

        /* -------- Public Methods -------- */
        public static void LoadContent(ContentManager contentManager)
        {
            basicTextureAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Texture Lists/BasicTextureAssetList");
            uiTextureAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Texture Lists/UITextureAssetList");
            mapTextureAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Texture Lists/MapTextureAssetList");
            statueTextureAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Texture Lists/StatueTextureAssetList");
            buildingTextureAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Texture Lists/BuildingTextureAssetList");
            unitTextureAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Texture Lists/UnitTextureAssetList");

            spriteFontAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/SpriteFontAssetList");

            effectAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/EffectAssetList");

            soundEffectAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/SoundEffectAssetList");

            nameLabelAtlasAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/NameLabelAtlasAssetList");

            localizationAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/LocalizationAssetList");

            mapDataAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Database Lists/MapDataAssetList");

            factionDatabaseAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Database Lists/FactionDatabaseAssetList");

            unitDatabaseAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Database Lists/UnitDatabaseAssetList");

            buildingDatabaseAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Database Lists/BuildingDatabaseAssetList");

            campaignStartStateDataAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Database Lists/CampaignStartStateDataAssetList");

            campaignGameDataAssetList = contentManager.Load<AssetListItem[]>("XML/Asset Lists/Database Lists/CampaignGameDataAssetList");
        }

        // Get asset path
        public static string GetTexturePath(string id)
        {
            foreach (AssetListItem listItem in basicTextureAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            foreach (AssetListItem listItem in uiTextureAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            foreach (AssetListItem listItem in mapTextureAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            foreach (AssetListItem listItem in statueTextureAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            foreach (AssetListItem listItem in buildingTextureAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            foreach (AssetListItem listItem in unitTextureAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            // Search further texture content lists here...

            return null;
        }
        public static string GetSpriteFontPath(string id)
        {
            foreach (AssetListItem listItem in spriteFontAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            return null;
        }
        public static string GetEffectPath(string id)
        {
            foreach (AssetListItem listItem in effectAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            return null;
        }
        public static string GetSoundEffectPath(string id)
        {
            foreach (AssetListItem listItem in soundEffectAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            return null;
        }
        public static string GetNameLabelAtlasPath(string id)
        {
            foreach (AssetListItem listItem in nameLabelAtlasAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            return null;
        }
        public static string GetLocalizationPath(string id)
        {
            foreach (AssetListItem listItem in localizationAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            return null;
        }
        public static string GetMapDataPath(string id)
        {
            foreach (AssetListItem listItem in mapDataAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            return null;
        }
        public static string GetFactionDatabasePath(string id)
        {
            foreach (AssetListItem listItem in factionDatabaseAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            return null;
        }
        public static string GetUnitDatabasePath(string id)
        {
            foreach (AssetListItem listItem in unitDatabaseAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            return null;
        }
        public static string GetBuildingDatabasePath(string id)
        {
            foreach (AssetListItem listItem in buildingDatabaseAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            return null;
        }
        public static string GetCampaignStartStateDataPath(string id)
        {
            foreach (AssetListItem listItem in campaignStartStateDataAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            return null;
        }
        public static string GetCampaignGameDataPath(string id)
        {
            foreach (AssetListItem listItem in campaignGameDataAssetList)
                if (listItem.id == id)
                    return listItem.contentPath;

            return null;
        }
    }
}
