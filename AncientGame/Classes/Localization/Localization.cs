﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    static class Localization
    {
        /* -------- Properties -------- */
        public static string DefaultLanguage { get { return "en-us"; } }

        /* -------- Private Fields -------- */
        private static Dictionary<string, string> generalLocalizationDict = new Dictionary<string,string>();
        private static Dictionary<string, string> specificLocalizationDict = new Dictionary<string, string>();

        /* -------- Public Methods -------- */
        public static void LoadContent(ContentManager content)
        {
            string language = SettingsManager.Settings.Language;
            LocalizationListItem[] localization = AssetLoader.LoadGeneralLocalization(language, content);

            if (localization == null)
                localization = AssetLoader.LoadGeneralLocalization(DefaultLanguage, content);

            SetGeneralLocalizationDictionary(localization);
        }

        public static string Localize(string id)
        {
            if (generalLocalizationDict.ContainsKey(id))
                return generalLocalizationDict[id];

            if (specificLocalizationDict.ContainsKey(id))
                return specificLocalizationDict[id];

#if DEBUG
            return ("?:" + id);
#else
            return "";
#endif
        }

        public static void AddToSpecificLocalizationDictionary(LocalizationListItem[] localization)
        {
            foreach (LocalizationListItem lli in localization)
            {
                if (!specificLocalizationDict.ContainsKey(lli.id))
                    specificLocalizationDict.Add(lli.id, lli.text);
            }
        }

        public static void ClearSpecificLocalizationDictionary()
        {
            specificLocalizationDict.Clear();
        }

        /* -------- Private Methods --------- */
        private static void SetGeneralLocalizationDictionary(LocalizationListItem[] localization)
        {
            if (localization == null)
            {
                Debug.LogWarning("Tried to set a null localization dictionary");
                return;
            }

            foreach (LocalizationListItem lli in localization)
            {
                if (generalLocalizationDict.ContainsKey(lli.id))
                    continue;

                generalLocalizationDict.Add(lli.id, lli.text);
            }
        }
    }
}
