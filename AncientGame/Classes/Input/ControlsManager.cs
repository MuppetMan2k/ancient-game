﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eControls
    {
        // Camera
        CAMERA_FORWARD,
        CAMERA_BACKWARD,
        CAMERA_LEFT,
        CAMERA_RIGHT,
        CAMERA_SWING_LEFT,
        CAMERA_SWING_RIGHT,
        CAMERA_ZOOM_IN,
        CAMERA_ZOOM_OUT
    }

    class ControlsManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<eControls, Keys> controlMap;

        /* -------- Constructors -------- */
        public ControlsManager()
        {
            controlMap = new Dictionary<eControls, Keys>();

            SetDefaultControlMap();
        }

        /* -------- Public Methods -------- */
        public Keys GetControlMappedKey(eControls control)
        {
            return controlMap[control];
        }

        /* -------- Private Methods --------- */
        private void SetDefaultControlMap()
        {
            // Camera
            controlMap.Add(eControls.CAMERA_FORWARD, Keys.W);
            controlMap.Add(eControls.CAMERA_BACKWARD, Keys.S);
            controlMap.Add(eControls.CAMERA_LEFT, Keys.A);
            controlMap.Add(eControls.CAMERA_RIGHT, Keys.D);
            controlMap.Add(eControls.CAMERA_SWING_LEFT, Keys.Q);
            controlMap.Add(eControls.CAMERA_SWING_RIGHT, Keys.E);
            controlMap.Add(eControls.CAMERA_ZOOM_IN, Keys.PageUp);
            controlMap.Add(eControls.CAMERA_ZOOM_OUT, Keys.PageDown);
        }
    }
}
