﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SystemInformation = System.Windows.Forms.SystemInformation;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eMouseButton
    {
        LEFT_BUTTON,
        RIGHT_BUTTON,
        MIDDLE_BUTTON,
        X1_BUTTON,
        X2_BUTTON
    }

    class InputComponent : GameComponent
    {
        /* -------- Properties -------- */
        public ControlsManager Controls { get; private set; }

        /* -------- Private Fields -------- */
        private KeyboardState prevKeyboardState;
        private KeyboardState currKeyboardState;
        private MouseState prevMouseState;
        private MouseState currMouseState;

        /* -------- Constructors -------- */
        public InputComponent(Game game)
            : base(game)
        {
            UpdateOrder = 1;

            Controls = new ControlsManager();
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTime gameTime)
        {
            prevKeyboardState = currKeyboardState;
            currKeyboardState = Keyboard.GetState();

            prevMouseState = currMouseState;
            currMouseState = Mouse.GetState();
            
            base.Update(gameTime);
        }

        public bool KeyIsDown(Keys key)
        {
            return (currKeyboardState.IsKeyDown(key));
        }
        public bool KeyIsUp(Keys key)
        {
            return (currKeyboardState.IsKeyUp(key));
        }
        public bool KeyIsPressing(Keys key)
        {
            return (currKeyboardState.IsKeyDown(key) && prevKeyboardState.IsKeyUp(key));
        }
        public bool KeyIsReleasing(Keys key)
        {
            return (currKeyboardState.IsKeyUp(key) && prevKeyboardState.IsKeyDown(key));
        }

        public bool ControlIsDown(eControls control)
        {
            Keys key = Controls.GetControlMappedKey(control);
            return KeyIsDown(key);
        }
        public bool ControlIsUp(eControls control)
        {
            Keys key = Controls.GetControlMappedKey(control);
            return KeyIsUp(key);
        }
        public bool ControlIsPressing(eControls control)
        {
            Keys key = Controls.GetControlMappedKey(control);
            return KeyIsPressing(key);
        }
        public bool ControlIsReleasing(eControls control)
        {
            Keys key = Controls.GetControlMappedKey(control);
            return KeyIsReleasing(key);
        }

        public bool MouseButtonIsDown(eMouseButton button)
        {
            return (MouseStateButtonIsDown(currMouseState, button));
        }
        public bool MouseButtonIsUp(eMouseButton button)
        {
            return (MouseStateButtonIsUp(currMouseState, button));
        }
        public bool MouseButtonIsPressing(eMouseButton button)
        {
            return (MouseStateButtonIsDown(currMouseState, button) && MouseStateButtonIsUp(prevMouseState, button));
        }
        public bool MouseButtonIsReleasing(eMouseButton button)
        {
            return (MouseStateButtonIsUp(currMouseState, button) && MouseStateButtonIsDown(prevMouseState, button));
        }

        public Point GetMousePosition()
        {
            return (new Point(currMouseState.X, currMouseState.Y));
        }

        public int GetMouseScrollChange()
        {
            return (currMouseState.ScrollWheelValue - prevMouseState.ScrollWheelValue);
        }

        /* -------- Private Methods --------- */
        private static bool MouseStateButtonIsDown(MouseState mouseState, eMouseButton button)
        {
            bool buttonsSwapped = SystemInformation.MouseButtonsSwapped;

            switch (button)
            {
                case eMouseButton.LEFT_BUTTON:
                    if (buttonsSwapped)
                        return (mouseState.RightButton == ButtonState.Pressed);
                    else
                        return (mouseState.LeftButton == ButtonState.Pressed);
                case eMouseButton.RIGHT_BUTTON:
                    if (buttonsSwapped)
                        return (mouseState.LeftButton == ButtonState.Pressed);
                    else
                        return (mouseState.RightButton == ButtonState.Pressed);
                case eMouseButton.MIDDLE_BUTTON:
                    return (mouseState.MiddleButton == ButtonState.Pressed);
                case eMouseButton.X1_BUTTON:
                    return (mouseState.XButton1 == ButtonState.Pressed);
                case eMouseButton.X2_BUTTON:
                    return (mouseState.XButton2 == ButtonState.Pressed);
                default:
                    return false;
            }
        }
        private static bool MouseStateButtonIsUp(MouseState mouseState, eMouseButton button)
        {
            bool buttonsSwapped = SystemInformation.MouseButtonsSwapped;

            switch (button)
            {
                case eMouseButton.LEFT_BUTTON:
                    if (buttonsSwapped)
                        return (mouseState.RightButton == ButtonState.Released);
                    else
                        return (mouseState.LeftButton == ButtonState.Released);
                case eMouseButton.RIGHT_BUTTON:
                    if (buttonsSwapped)
                        return (mouseState.LeftButton == ButtonState.Released);
                    else
                        return (mouseState.RightButton == ButtonState.Released);
                case eMouseButton.MIDDLE_BUTTON:
                    return (mouseState.MiddleButton == ButtonState.Released);
                case eMouseButton.X1_BUTTON:
                    return (mouseState.XButton1 == ButtonState.Released);
                case eMouseButton.X2_BUTTON:
                    return (mouseState.XButton2 == ButtonState.Released);
                default:
                    return true;
            }
        }
    }
}
