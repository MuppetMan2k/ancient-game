﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CharacterCalibrationManager
    {
        /* -------- Properties -------- */
        public CharacterCalibrationData Data { get; private set; }

        /* -------- Constructors -------- */
        public CharacterCalibrationManager(ContentManager contentManager)
        {
            Data = AssetLoader.LoadCharacterCalibrationData(contentManager);
        }
    }
}
