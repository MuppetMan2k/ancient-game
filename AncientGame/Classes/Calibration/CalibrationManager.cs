﻿using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    static class CalibrationManager
    {
        /* -------- Properties -------- */
        public static GraphicsCalibrationManager GraphicsCalibration { get; private set; }
        public static CameraCalibrationManager CameraCalibration { get; private set; }
        public static LightingCalibrationManager LightingCalibration { get; private set; }
        public static FactionCalibrationManager FactionCalibration { get; private set; }
        public static CharacterCalibrationManager CharacterCalibration { get; private set; }
        public static UICalibrationManager UICalibration { get; private set; }
        public static CampaignCalibrationManager CampaignCalibration { get; private set; }
        public static TradeCalibrationManager TradeCalibration { get; private set; }

        /* -------- Public Methods -------- */
        public static void LoadContent(ContentManager contentManager)
        {
            GraphicsCalibration = new GraphicsCalibrationManager(contentManager);
            CameraCalibration = new CameraCalibrationManager(contentManager);
            LightingCalibration = new LightingCalibrationManager(contentManager);
            FactionCalibration = new FactionCalibrationManager(contentManager);
            CharacterCalibration = new CharacterCalibrationManager(contentManager);
            UICalibration = new UICalibrationManager(contentManager);
            CampaignCalibration = new CampaignCalibrationManager(contentManager);
            TradeCalibration = new TradeCalibrationManager(contentManager);
        }
    }
}
