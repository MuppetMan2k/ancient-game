﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CameraCalibrationManager
    {
        /* -------- Properties -------- */
        public CameraCalibrationData Data { get; private set; }

        /* -------- Constructors -------- */
        public CameraCalibrationManager(ContentManager contentManager)
        {
            Data = AssetLoader.LoadCameraCalibrationData(contentManager);
        }
    }
}
