﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class UICalibrationManager
    {
        /* -------- Properties -------- */
        public UICalibrationData Data { get; private set; }

        /* -------- Constructors -------- */
        public UICalibrationManager(ContentManager contentManager)
        {
            Data = AssetLoader.LoadUICalibrationData(contentManager);
        }
    }
}
