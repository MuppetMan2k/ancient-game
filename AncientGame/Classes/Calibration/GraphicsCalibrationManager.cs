﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class GraphicsCalibrationManager
    {
        /* -------- Properties -------- */
        public GraphicsCalibrationData Data { get; private set; }

        /* -------- Constructors -------- */
        public GraphicsCalibrationManager(ContentManager contentManager)
        {
            Data = AssetLoader.LoadGraphicsCalibrationData(contentManager);
        }
    }
}
