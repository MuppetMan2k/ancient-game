﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class TradeCalibrationManager
    {
        /* -------- Properties -------- */
        public TradeCalibrationData Data { get; private set; }

        /* -------- Constructors -------- */
        public TradeCalibrationManager(ContentManager contentManager)
        {
            Data = AssetLoader.LoadTradeCalibrationData(contentManager);
        }
    }
}
