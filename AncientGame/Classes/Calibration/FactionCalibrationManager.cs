﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class FactionCalibrationManager
    {
        /* -------- Properties -------- */
        public FactionCalibrationData Data { get; private set; }

        /* -------- Constructors -------- */
        public FactionCalibrationManager(ContentManager content)
        {
            Data = AssetLoader.LoadFactionCalibrationData(content);
        }
    }
}
