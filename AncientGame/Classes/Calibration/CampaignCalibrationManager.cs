﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CampaignCalibrationManager
    {
        /* -------- Properties -------- */
        public CampaignCalibrationData Data { get; private set; }

        /* -------- Constructors -------- */
        public CampaignCalibrationManager(ContentManager contentManager)
        {
            Data = AssetLoader.LoadCampaignCalibrationData(contentManager);
        }
    }
}
