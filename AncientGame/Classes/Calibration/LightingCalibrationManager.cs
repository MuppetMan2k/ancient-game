﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LightingCalibrationManager
    {
        /* -------- Properties -------- */
        public LightingCalibrationData Data { get; private set; }

        /* -------- Constructors -------- */
        public LightingCalibrationManager(ContentManager contentManager)
        {
            Data = AssetLoader.LoadLightingCalibrationData(contentManager);
        }
    }
}
