﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class GameTimeWrapper
    {
        /* -------- Properties -------- */
        public float ElapsedSeconds { get { return (float)gameTime.ElapsedGameTime.TotalSeconds; } }

        /* -------- Private Fields -------- */
        private GameTime gameTime;

        /* -------- Constructors -------- */
        public GameTimeWrapper(GameTime inGameTime)
        {
            gameTime = inGameTime;
        }
    }
}
