﻿namespace AncientGame
{
    class FlowStateChangeRequest
    {
        /* -------- Properties -------- */
        public eFlowState NewFlowState { get; private set; }
        public GameInfo GameInfo { get; private set; }

        /* -------- Constructors -------- */
        public FlowStateChangeRequest(eFlowState inNewFlowState, GameInfo inGameInfo)
        {
            NewFlowState = inNewFlowState;
            GameInfo = inGameInfo;
        }

        /* -------- Static Methods -------- */
        public static FlowStateChangeRequest CreateMenuFlowStateChangeRequest()
        {
            return new FlowStateChangeRequest(eFlowState.MENU, null);
        }
        public static FlowStateChangeRequest CreateGameFlowStateChangeRequest(GameInfo inGameInfo)
        {
            return new FlowStateChangeRequest(eFlowState.GAME, inGameInfo);
        }
    }
}
