﻿using System;
using System.Collections.Generic;

namespace AncientGame
{
    class GameFlowState : FlowState
    {
        /* -------- Private Fields -------- */
        private GameInfo gameInfo;
        private GameManagerContainer managers;
        private GameUIEntityCreator uiCreator;

        /* -------- Constructors -------- */
        public GameFlowState(FlowStateComponent inFlowStateComponent, IServiceProvider inServiceProvider, GraphicsComponent inGraphicsComponent, UIComponent inUIComponent, GameInfo inGameInfo)
            : base(inFlowStateComponent, inServiceProvider)
        {
            State = eFlowState.GAME;
            gameInfo = inGameInfo;
            managers = new GameManagerContainer(inGraphicsComponent, inUIComponent, content);
            uiCreator = new GameUIEntityCreator(inUIComponent);

            inUIComponent.OnNewFlowStateSet(UIStyle.DebugStyle, content); // DEBUG UI style
        }

        /* -------- Public Methods -------- */
        public override void Update(GraphicsComponent g, InputComponent i, UIComponent ui, GameTimeWrapper gtw)
        {
            managers.Update(g, i, ui, gtw, content);
        }

        public override void Draw(GraphicsComponent g)
        {
            g.SetRasterizerState();
            managers.Draw(g, content);
        }

        /* -------- Private Methods --------- */
        protected override void OnExitLoadingScreen()
        {
            base.OnExitLoadingScreen();

            uiCreator.SetUpNonFactionUI(content);

            uiCreator.SetUpFactionUI(managers, content);
        }

        protected override bool UsesLoadingScreen()
        {
            return true;
        }
        protected override Queue<LoadingProcess> GetLoadingProcesses()
        {
            Queue<LoadingProcess> loadingProcesses = new Queue<LoadingProcess>();

            if (gameInfo.Type == eGameType.NEW_CAMPAIGN)
                AddLoadingProcessesForNewCampaign(loadingProcesses);
            if (gameInfo.Type == eGameType.LOADED_CAMPAIGN)
                AddLoadingProcessesForLoadedCampaign(loadingProcesses);

            return loadingProcesses;
        }
        private void AddLoadingProcessesForNewCampaign(Queue<LoadingProcess> loadingProcesses)
        {
            loadingProcesses.Enqueue(new LoadAppointmentDatabaseProcess());
            loadingProcesses.Enqueue(new LoadResourceDatabaseProcess());
            loadingProcesses.Enqueue(new LoadCultureDatabaseProcess());
            loadingProcesses.Enqueue(new LoadLanguageDatabaseProcess());
            loadingProcesses.Enqueue(new LoadReligionDatabaseProcess());
            loadingProcesses.Enqueue(new LoadInstitutionDatabaseProcess());
            loadingProcesses.Enqueue(new LoadLawDatabaseProcess());
            loadingProcesses.Enqueue(new LoadTaxDatabaseProcess());
            loadingProcesses.Enqueue(new LoadGovernmentTraitDatabaseProcess());
            loadingProcesses.Enqueue(new LoadMilitaryEraDatabaseProcess());
            loadingProcesses.Enqueue(new LoadMapDataProcess(gameInfo.CampaignInfo.CampaignGameData.mapDataID));
            loadingProcesses.Enqueue(new LoadFactionDatabaseProcess(gameInfo.CampaignInfo.CampaignGameData.factionDatabaseID, gameInfo.CampaignInfo.PlayerInfo));
            loadingProcesses.Enqueue(new LoadUnitDatabaseProcess(gameInfo.CampaignInfo.CampaignGameData.unitDatabaseID));
            loadingProcesses.Enqueue(new LoadBuildingDatabaseProcess(gameInfo.CampaignInfo.CampaignGameData.buildingDatabaseID));
            loadingProcesses.Enqueue(new LoadCampaignStartStateDataProcess(gameInfo.CampaignInfo.CampaignGameData.campaignStartStateDataID));
            loadingProcesses.Enqueue(new LoadOtherSpecificLocalizationsProcess());
            loadingProcesses.Enqueue(new RecalculateForNewCampaignProcess());
        }
        private void AddLoadingProcessesForLoadedCampaign(Queue<LoadingProcess> loadingProcesses)
        {
            // TODO
        }
        protected override void ProcessLoadingProcess(LoadingProcess loadingProcess)
        {
            loadingProcess.Process(loadingProcesses, managers, gameInfo, content);
        }

        private void OnNewFactionTurnSet()
        {
            uiCreator.RemoveFactionUI();
            uiCreator.SetUpFactionUI(managers, content);
        }
    }
}
