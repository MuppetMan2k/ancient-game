﻿using System;
using System.Collections.Generic;

namespace AncientGame
{
    class MenuFlowState : FlowState
    {
        /* -------- Private Fields -------- */
        private MenuUIEntityCreator uiCreator;

        /* -------- Constructors -------- */
        public MenuFlowState(FlowStateComponent inFlowStateComponent, IServiceProvider inServiceProvider, UIComponent inUIComponent)
            : base(inFlowStateComponent, inServiceProvider)
        {
            State = eFlowState.MENU;

            inUIComponent.OnNewFlowStateSet(UIStyle.DebugStyle, content); // DEBUG UI style

            uiCreator = new MenuUIEntityCreator(inUIComponent);
            uiCreator.CreateDebugMenuUI(this, content); // DEBUG
        }

        /* -------- Public Methods -------- */
        public override void Update(GraphicsComponent g, InputComponent i, UIComponent ui, GameTimeWrapper gtw)
        {

        }

        public override void Draw(GraphicsComponent g)
        {
            g.GraphicsDevice.Clear(Microsoft.Xna.Framework.Color.IndianRed);
        }

        // DEBUG
        public void DebugGoToGameFlowState()
        {
            DataTypes.CampaignGameData campaignGameData = AssetLoader.LoadCampaignGameData("debug-campaign-game-data", content);
            PlayerInfo playerInfo = new PlayerInfo();
            playerInfo.PlayerFactionIDs.Add("roma");
            CampaignGameInfo campaignGameInfo = new CampaignGameInfo(campaignGameData, playerInfo);

            GameInfo gameInfo = GameInfo.CreateNewCampaignGameInfo(campaignGameInfo);
            FlowStateChangeRequest changeRequest = new FlowStateChangeRequest(eFlowState.GAME, gameInfo);

            flowStateComponent.RequestFlowStateChange(changeRequest);
        }

        /* -------- Private Methods --------- */
        protected override bool UsesLoadingScreen()
        {
            return false;
        }
        protected override Queue<LoadingProcess> GetLoadingProcesses()
        {
            return new Queue<LoadingProcess>();
        }
        protected override void ProcessLoadingProcess(LoadingProcess loadingProcess)
        {
            loadingProcess.Process(loadingProcesses, null, null, content);
        }
    }
}
