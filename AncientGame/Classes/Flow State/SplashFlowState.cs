﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eSplashState
    {
        FADE_UP,
        HOLD,
        FADE_DOWN,
        FADE_BETWEEN_BACKGROUNDS,
        FADE_FINAL_BACKGROUND,
    }

    class SplashFlowState : FlowState
    {
        /* -------- Private Fields -------- */
        private List<SplashScreen> splashScreens;
        private eSplashState splashState;
        private float timer;
        private int splashScreenIndex;
        private Color finalBackgroundColor;
        // Assets
        private Texture2D pixelTex;
        private Texture2D logoTex;
        // Constants
        private const float FADE_UP_TIME = 1f;
        private const float HOLD_TIME = 2f;
        private const float FADE_DOWN_TIME = 1f;
        private const float FADE_BACKGROUND_TIME = 1f;

        /* -------- Constructors -------- */
        public SplashFlowState(FlowStateComponent inFlowStateComponent, IServiceProvider inServiceProvider)
            : base(inFlowStateComponent, inServiceProvider)
        {
            State = eFlowState.SPLASH;

            CreateSplashScreens();

            splashState = eSplashState.FADE_UP;
            timer = 0f;
            splashScreenIndex = 0;
            finalBackgroundColor = Color.Black;

            pixelTex = AssetLoader.LoadTexture("white-pixel", content);
            SetNewLogoTexture();
        }

        /* -------- Public Methods -------- */
        public override void Update(GraphicsComponent g, InputComponent i, UIComponent ui, GameTimeWrapper gtw)
        {
#if DEBUG
            GoToNextFlowState();
#else
            timer += gtw.ElapsedSeconds;

            switch(splashState)
            {
                case eSplashState.FADE_UP:
                    Update_FadeUp();
                    break;
                case eSplashState.HOLD:
                    Update_Hold();
                    break;
                case eSplashState.FADE_DOWN:
                    Update_FadeDown();
                    break;
                case eSplashState.FADE_BETWEEN_BACKGROUNDS:
                    Update_FadeBetweenBackgrounds();
                    break;
                case eSplashState.FADE_FINAL_BACKGROUND:
                    Update_FadeFinalBackground();
                    break;
            }
#endif
        }

        public override void Draw(GraphicsComponent g)
        {
            g.SpriteBatch.Begin();
            SBDraw(g);
            g.SpriteBatch.End();
        }

        /* -------- Private Methods --------- */
        protected override bool UsesLoadingScreen()
        {
            return false;
        }
        protected override Queue<LoadingProcess> GetLoadingProcesses()
        {
            return new Queue<LoadingProcess>();
        }
        protected override void ProcessLoadingProcess(LoadingProcess loadingProcess)
        {
            loadingProcess.Process(loadingProcesses, null, null, content);
        }

        private void CreateSplashScreens()
        {
            splashScreens = new List<SplashScreen>();

            SplashScreen jasmineSoftwareSplash = new SplashScreen();
            jasmineSoftwareSplash.TextureId = "jasmine-software-logo";
            jasmineSoftwareSplash.BackgroundColor = Color.White;
            splashScreens.Add(jasmineSoftwareSplash);
        }

        private void Update_FadeUp()
        {
            if (timer < FADE_UP_TIME)
                return;

            timer = 0f;
            splashState = eSplashState.HOLD;
        }
        private void Update_Hold()
        {
            if (timer < HOLD_TIME)
                return;

            timer = 0f;
            splashState = eSplashState.FADE_DOWN;
        }
        private void Update_FadeDown()
        {
            if (timer < FADE_DOWN_TIME)
                return;

            timer = 0f;
            OnFadeDownFinished();
        }
        private void Update_FadeBetweenBackgrounds()
        {
            if (timer < FADE_BACKGROUND_TIME)
                return;

            timer = 0f;
            splashScreenIndex++;
            splashState = eSplashState.FADE_UP;
            SetNewLogoTexture();
        }
        private void Update_FadeFinalBackground()
        {
            if (timer < FADE_BACKGROUND_TIME)
                return;

            GoToNextFlowState();
        }

        private void SBDraw(GraphicsComponent g)
        {
            switch (splashState)
            {
                case eSplashState.FADE_UP:
                    SBDraw_FadeUp(g);
                    break;
                case eSplashState.HOLD:
                    SBDraw_Hold(g);
                    break;
                case eSplashState.FADE_DOWN:
                    SBDraw_FadeDown(g);
                    break;
                case eSplashState.FADE_BETWEEN_BACKGROUNDS:
                    SBDraw_FadeBetweenBackgrounds(g);
                    break;
                case eSplashState.FADE_FINAL_BACKGROUND:
                    SBDraw_FadeFinalBackground(g);
                    break;
            }
        }
        private void SBDraw_FadeUp(GraphicsComponent g)
        {
            Color backgroundColor = GetCurrentSplashScreen().BackgroundColor;
            string textureId = GetCurrentSplashScreen().TextureId;
            float textureOpacity = Mathf.Clamp01(timer / FADE_UP_TIME);

            SBDrawBackgroundAndTexture(g, backgroundColor, textureOpacity);
        }
        private void SBDraw_Hold(GraphicsComponent g)
        {
            Color backgroundColor = GetCurrentSplashScreen().BackgroundColor;
            string textureId = GetCurrentSplashScreen().TextureId;

            SBDrawBackgroundAndTexture(g, backgroundColor, 1f);
        }
        private void SBDraw_FadeDown(GraphicsComponent g)
        {
            Color backgroundColor = GetCurrentSplashScreen().BackgroundColor;
            string textureId = GetCurrentSplashScreen().TextureId;
            float textureOpacity = 1f - Mathf.Clamp01(timer / FADE_DOWN_TIME);

            SBDrawBackgroundAndTexture(g, backgroundColor, textureOpacity);
        }
        private void SBDraw_FadeBetweenBackgrounds(GraphicsComponent g)
        {
            float fadeFactor = Mathf.Clamp01(timer / FADE_BACKGROUND_TIME);
            Color backgroundColor1 = GetCurrentSplashScreen().BackgroundColor;
            Color backgroundColor2 = GetNextSplashScreen().BackgroundColor;
            Color fadeColor = Color.Lerp(backgroundColor1, backgroundColor2, fadeFactor);

            SBDrawBackgroundAndTexture(g, fadeColor, 0f);
        }
        private void SBDraw_FadeFinalBackground(GraphicsComponent g)
        {
            float fadeFactor = Mathf.Clamp01(timer / FADE_BACKGROUND_TIME);
            Color backgroundColor1 = GetCurrentSplashScreen().BackgroundColor;
            Color fadeColor = Color.Lerp(backgroundColor1, finalBackgroundColor, fadeFactor);

            SBDrawBackgroundAndTexture(g, fadeColor, 0f);
        }
        private void SBDrawBackgroundAndTexture(GraphicsComponent g, Color backgroundColor, float textureOpacity)
        {
            g.SpriteBatch.Draw(pixelTex, g.FullScreenRectangle, backgroundColor);

            if (logoTex == null)
                return;

            Rectangle rect = GetTextureRectangle(g, logoTex);
            g.SpriteBatch.Draw(logoTex, rect, Color.White * textureOpacity);
        }
        private Rectangle GetTextureRectangle(GraphicsComponent g, Texture2D tex)
        {
            int x = g.HalfScreenWidth - tex.Width / 2;
            int y = g.HalfScreenHeight - tex.Height / 2;

            return new Rectangle(x, y, tex.Width, tex.Height);
        }

        private void OnFadeDownFinished()
        {
            if (!MoreSplashScreensToShow())
            {
                // No more splash screens to show, go to final fade or next game flow state
                if (ShowFinalBackgroundFade())
                    splashState = eSplashState.FADE_FINAL_BACKGROUND;
                else
                    GoToNextFlowState();

                return;
            }

            // More splash screens
            if (ShowBetweenBackgroundFade())
                splashState = eSplashState.FADE_BETWEEN_BACKGROUNDS;
            else
            {
                splashScreenIndex++;
                splashState = eSplashState.FADE_UP;
                SetNewLogoTexture();
            }
        }

        private bool MoreSplashScreensToShow()
        {
            return (splashScreens.Count - 1 > splashScreenIndex);
        }

        private SplashScreen GetCurrentSplashScreen()
        {
            return splashScreens[splashScreenIndex];
        }
        private SplashScreen GetNextSplashScreen()
        {
            return splashScreens[splashScreenIndex + 1];
        }

        private bool ShowFinalBackgroundFade()
        {
            return (!GetCurrentSplashScreen().BackgroundColor.Equals(finalBackgroundColor));
        }
        private bool ShowBetweenBackgroundFade()
        {
            Color currentColor = GetCurrentSplashScreen().BackgroundColor;
            Color nextColor = GetNextSplashScreen().BackgroundColor;

            return (!currentColor.Equals(nextColor));
        }

        private void SetNewLogoTexture()
        {
            string textureId = GetCurrentSplashScreen().TextureId;
            logoTex = AssetLoader.LoadTexture(textureId, content);
        }

        private void GoToNextFlowState()
        {
            FlowStateChangeRequest changeRequest = FlowStateChangeRequest.CreateMenuFlowStateChangeRequest();
            flowStateComponent.RequestFlowStateChange(changeRequest);
        }
    }
}
