﻿using System;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class FlowStateComponent : DrawableGameComponent
    {
        /* -------- Private Fields -------- */
        private FlowState flowState;
        private FlowStateChangeRequest changeRequest;
        private IServiceProvider serviceProvider;
        // Sister components
        private InputComponent inputComponent;
        private GraphicsComponent graphicsComponent;
        private UIComponent uiComponent;

        /* -------- Constructors -------- */
        public FlowStateComponent(Game inGame, InputComponent inInputComponent, GraphicsComponent inGraphicsComponent, UIComponent inUIComponent, IServiceProvider inServiceProvider)
            : base(inGame)
        {
            inputComponent = inInputComponent;
            graphicsComponent = inGraphicsComponent;
            uiComponent = inUIComponent;

            changeRequest = null;
            serviceProvider = inServiceProvider;
            flowState = new SplashFlowState(this, serviceProvider);

            UpdateOrder = 2;
            DrawOrder = 2;
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTime gameTime)
        {
            GameTimeWrapper gtw = new GameTimeWrapper(gameTime);

            if (flowState.InLoadingScreen)
                flowState.UpdateLoadingScreen(gtw);
            else
                flowState.Update(graphicsComponent, inputComponent, uiComponent, gtw);

            if (changeRequest != null)
                PerformFlowStateChange();

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if (flowState.InLoadingScreen)
                flowState.DrawLoadingScreen(graphicsComponent);
            else
                flowState.Draw(graphicsComponent);

            base.Draw(gameTime);
        }

        public void RequestFlowStateChange(FlowStateChangeRequest inChangeRequest)
        {
            if (changeRequest != null)
                return;

            changeRequest = inChangeRequest;
        }

        /* -------- Private Methods --------- */
        private void PerformFlowStateChange()
        {
            switch (changeRequest.NewFlowState)
            {
                case eFlowState.SPLASH:
                    {
                        if (flowState.State == eFlowState.SPLASH)
                            break;

                        flowState.UnloadContent();
                        flowState = new SplashFlowState(this, serviceProvider);
                        break;
                    }
                case eFlowState.MENU:
                    {
                        if (flowState.State == eFlowState.MENU)
                            break;

                        flowState.UnloadContent();
                        flowState = new MenuFlowState(this, serviceProvider, uiComponent);
                        break;
                    }
                case eFlowState.GAME:
                    {
                        flowState.UnloadContent();
                        flowState = new GameFlowState(this, serviceProvider, graphicsComponent, uiComponent, changeRequest.GameInfo);
                        break;
                    }
                default:
                    {
                        Debug.LogUnrecognizedValueWarning(changeRequest.NewFlowState);
                        break;
                    }
            }

            changeRequest = null;
        }
    }
}
