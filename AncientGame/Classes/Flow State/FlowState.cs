﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eFlowState
    {
        SPLASH,
        MENU,
        GAME
    }

    abstract class FlowState
    {
        /* -------- Properties -------- */
        public eFlowState State { get; protected set; }
        public bool InLoadingScreen { get; private set; }

        /* -------- Private Fields -------- */
        protected FlowStateComponent flowStateComponent;
        protected ContentManager content;
        // Loading
        protected Queue<LoadingProcess> loadingProcesses;
        private bool performedLoadInitialization;
        // Constants
        private const float MAX_TIME_PROCESSING_LOADING_PROCESSES = 0.0f;

        /* -------- Constructors -------- */
        public FlowState(FlowStateComponent inFlowStateComponent, IServiceProvider inServiceProvider)
        {
            flowStateComponent = inFlowStateComponent;
            content = new ContentManager(inServiceProvider, "Content");

            InLoadingScreen = UsesLoadingScreen();
        }

        /* -------- Public Methods -------- */
        public virtual void UnloadContent()
        {
            content.Unload();
            content.Dispose();
        }

        public abstract void Update(GraphicsComponent g, InputComponent i, UIComponent ui, GameTimeWrapper gtw);

        public void UpdateLoadingScreen(GameTimeWrapper gtw)
        {
            if (!performedLoadInitialization)
            {
                loadingProcesses = GetLoadingProcesses();
                performedLoadInitialization = true;
            }

            if (loadingProcesses == null || loadingProcesses.Count == 0)
            {
                OnFinishAllLoadingProcesses();
                return;
            }

            DateTime startTimeStamp = DateTime.UtcNow;
            bool shouldContinue = true;
            while (shouldContinue)
            {
                LoadingProcess loadingProcess = loadingProcesses.Peek();
                ProcessLoadingProcess(loadingProcess);
                if (!loadingProcess.ContinueProcessing)
                    loadingProcesses.Dequeue();

                DateTime timeStamp = DateTime.UtcNow;
                float timeProcessing = (float)(timeStamp - startTimeStamp).TotalSeconds;
                if (timeProcessing > MAX_TIME_PROCESSING_LOADING_PROCESSES)
                    shouldContinue = false;

                if (!LoadingProcessesAreRemaining())
                {
                    OnFinishAllLoadingProcesses();
                    shouldContinue = false;
                }
            }
        }

        public abstract void Draw(GraphicsComponent g);

        public virtual void DrawLoadingScreen(GraphicsComponent g)
        {
            g.GraphicsDevice.Clear(Color.MediumPurple);
        }

        /* -------- Private Methods --------- */
        protected abstract bool UsesLoadingScreen();
        protected abstract Queue<LoadingProcess> GetLoadingProcesses();
        protected abstract void ProcessLoadingProcess(LoadingProcess loadingProcess);

        protected virtual void OnExitLoadingScreen()
        {
        }

        private void OnFinishAllLoadingProcesses()
        {
            loadingProcesses = null;
            InLoadingScreen = false;

            OnExitLoadingScreen();
        }
        private bool LoadingProcessesAreRemaining()
        {
            return (loadingProcesses.Count > 0);
        }
    }
}
