﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    static class Debug
    {
#if DEBUG
        // -------- Properties --------
        public static bool DrawDebugOutput { get; set; }

        /* -------- Private Fields -------- */
        private static List<DebugMessage> debugMessages = new List<DebugMessage>(MAX_NUM_DEBUG_MESSAGES);
        // Assets
        private static Texture2D pixelTex;
        private static SpriteFont messageFont;
        // Constants
        private const int NUM_DEBUG_MESSAGES_TO_DRAW = 10;
        private const int MAX_NUM_DEBUG_MESSAGES = 100;
        private const int TEXT_HEIGHT = 14;
        private const int PIXELS_BEWTWEEN_DEBUG_MESSAGES = 5;
        private const float DEBUG_ALPHA = 0.8f;
        private const bool OPEN_DEBUG_ON_WARNING = true;
#endif

        /* -------- Public Methods -------- */
        public static void LoadContent(ContentManager content)
        {
#if DEBUG
            pixelTex = AssetLoader.LoadTexture("white-pixel", content);
            messageFont = AssetLoader.LoadSpriteFont("arial-12", content);

            FramerateCounter.LoadContent(content);
#endif
        }

        public static void Update(GameTimeWrapper gtw, InputComponent input)
        {
#if DEBUG
            if (input.KeyIsPressing(Keys.Home))
                DrawDebugOutput = !DrawDebugOutput;
            if (DrawDebugOutput && input.KeyIsPressing(Keys.End))
                debugMessages.Clear();

            FramerateCounter.Update(gtw);
            Benchmarking.Update(input);
#endif
        }

        public static void SBDraw(GraphicsComponent g)
        {
            FramerateCounter.SBDraw(g);

            if (!DrawDebugOutput)
                return;

            Rectangle windowRect = new Rectangle(0, 0, g.ScreenWidth, GetDebugWindowHeight());
            g.SpriteBatch.Draw(pixelTex, windowRect, Color.Black * DEBUG_ALPHA);

            if (debugMessages.Count == 0)
                return;

            int x = PIXELS_BEWTWEEN_DEBUG_MESSAGES;
            int y = PIXELS_BEWTWEEN_DEBUG_MESSAGES;
            for (int i = GetFirstDrawnMessageIndex(); i <= GetLastDrawnMessageIndex(); i++)
            {
                DebugMessage message = debugMessages[i];
                Point psPoint = new Point(x, y);
                Vector2 psVec = SpaceUtilities.ConvertPSToPSVec(psPoint);
                Color tint = GetDebugMessageColor(message.Type) * DEBUG_ALPHA;

                g.SpriteBatch.DrawString(messageFont, message.Message, psVec, tint);

                y += TEXT_HEIGHT + PIXELS_BEWTWEEN_DEBUG_MESSAGES;
            }
        }

        public static void Log(Object message)
        {
#if DEBUG
            CreateDebugMessage(eDebugMessageType.INFO, message.ToString());
#else
            return;
#endif
        }
        public static void Log(string message, params string[] args)
        {
#if DEBUG
            CreateDebugMessage(eDebugMessageType.INFO, string.Format(message, args));
#else
            return;
#endif
        }
        public static void LogWarning(Object message)
        {
#if DEBUG
            CreateDebugMessage(eDebugMessageType.WARNING, message.ToString());

            if (OPEN_DEBUG_ON_WARNING)
                DrawDebugOutput = true;
#else
            return;
#endif
        }
        public static void LogWarning(string message, params string[] args)
        {
#if DEBUG
            CreateDebugMessage(eDebugMessageType.WARNING, string.Format(message, args));

            if (OPEN_DEBUG_ON_WARNING)
                DrawDebugOutput = true;
#else
            return;
#endif
        }
        public static void LogUnrecognizedValueWarning<T>(T val)
        {
            LogWarning("Unrecognized value {0} for type {1}", val.ToString(), typeof(T).ToString());
        }
        public static void LogError(Object message)
        {
#if DEBUG
            CreateDebugMessage(eDebugMessageType.ERROR, message.ToString());

            if (OPEN_DEBUG_ON_WARNING)
                DrawDebugOutput = true;
#else
            return;
#endif
        }
        public static void LogError(string message, params string[] args)
        {
#if DEBUG
            CreateDebugMessage(eDebugMessageType.ERROR, string.Format(message, args));

            if (OPEN_DEBUG_ON_WARNING)
                DrawDebugOutput = true;
#else
            return;
#endif
        }

#if DEBUG
        /* -------- Private Methods --------- */
        private static void CreateDebugMessage(eDebugMessageType type, string message)
        {
            if (DebugMessagesAreFull())
                TrimDebugMessages();

            DebugMessage newDebugMessage = new DebugMessage();
            newDebugMessage.Type = type;
            newDebugMessage.Message = message;

            debugMessages.Add(newDebugMessage);
        }

        private static void TrimDebugMessages()
        {
            int numToTrim = debugMessages.Count - NUM_DEBUG_MESSAGES_TO_DRAW;
            debugMessages.RemoveRange(0, numToTrim);
        }

        private static bool DebugMessagesAreFull()
        {
            return (debugMessages.Count == MAX_NUM_DEBUG_MESSAGES);
        }

        private static int GetDebugWindowHeight()
        {
            return (NUM_DEBUG_MESSAGES_TO_DRAW * TEXT_HEIGHT + (NUM_DEBUG_MESSAGES_TO_DRAW + 1) * PIXELS_BEWTWEEN_DEBUG_MESSAGES);
        }

        private static int GetFirstDrawnMessageIndex()
        {
            return Mathf.ClampLower(GetLastDrawnMessageIndex() - NUM_DEBUG_MESSAGES_TO_DRAW + 1, 0);
        }
        private static int GetLastDrawnMessageIndex()
        {
            return (debugMessages.Count - 1);
        }

        private static Color GetDebugMessageColor(eDebugMessageType type)
        {
            switch (type)
            {
                case eDebugMessageType.INFO:
                    return Color.White;
                case eDebugMessageType.WARNING:
                    return Color.Yellow;
                case eDebugMessageType.ERROR:
                    return Color.Red;
                default:
                    LogUnrecognizedValueWarning(type);
                    return Color.White;
            }
        }
#endif
    }
}
