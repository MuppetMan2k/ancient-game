﻿using System;

namespace AncientGame
{
    class BenchmarkResult
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }

        /* -------- Private Fields -------- */
        private DateTime startStamp;
        private DateTime endStamp;

        /* -------- Constructors -------- */
        public BenchmarkResult(string inID)
        {
            ID = inID;
            startStamp = DateTime.UtcNow;
        }

        /* -------- Public Methods -------- */
        public void End()
        {
            endStamp = DateTime.UtcNow;
        }

        public int GetDuration()
        {
            return Mathf.Round((endStamp - startStamp).TotalMilliseconds);
        }
    }
}
