﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    static class FramerateCounter
    {
#if DEBUG
        /* -------- Private Fields -------- */
        private static float[] frameTimes = new float[FRAME_NUMBER];
        private static int frameTimeIndex = 0;
        private static int frameNumberCounter = 0;
        // Assets
        private static Texture2D pixelTex;
        private static SpriteFont spriteFont;
        // Constants
        private const int FRAME_NUMBER = 10;
        private const int COUNTER_RECTANGLE_WIDTH = 90;
        private const int COUNTER_RECTANGLE_HEIGHT = 26;
        private const float COUNTER_ALPHA = 0.5f;

        /* -------- Public Methods -------- */
        public static void LoadContent(ContentManager content)
        {
            pixelTex = AssetLoader.LoadTexture("white-pixel", content);
            spriteFont = AssetLoader.LoadSpriteFont("arial-12", content);
        }

        public static void Update(GameTimeWrapper gtw)
        {
            frameTimes[frameTimeIndex++] = gtw.ElapsedSeconds;
            if (frameTimeIndex >= FRAME_NUMBER)
                frameTimeIndex = 0;
        }

        public static void SBDraw(GraphicsComponent g)
        {
            if (frameNumberCounter <= FRAME_NUMBER)
            {
                frameNumberCounter++;
                return;
            }

            float averageFrameTime = 0f;
            for (int i = 0; i < FRAME_NUMBER; i++)
                averageFrameTime += frameTimes[i];
            averageFrameTime /= (float)FRAME_NUMBER;
            float frameRate = 1f / averageFrameTime;

            g.SpriteBatch.Draw(pixelTex, new Rectangle(g.ScreenWidth - COUNTER_RECTANGLE_WIDTH, g.ScreenHeight - COUNTER_RECTANGLE_HEIGHT, COUNTER_RECTANGLE_WIDTH, COUNTER_RECTANGLE_HEIGHT), Color.Black * COUNTER_ALPHA);
            string text = "FPS: " + Mathf.Round(frameRate).ToString();
            g.SpriteBatch.DrawString(spriteFont, text, new Vector2(g.ScreenWidth - COUNTER_RECTANGLE_WIDTH + 5, g.ScreenHeight - COUNTER_RECTANGLE_HEIGHT + 5), Color.White * COUNTER_ALPHA);
        }
#endif
    }
}
