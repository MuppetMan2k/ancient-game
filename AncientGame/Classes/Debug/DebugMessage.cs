﻿namespace AncientGame
{
    // -------- Enumerations --------
    enum eDebugMessageType
    {
        INFO,
        WARNING,
        ERROR
    }

    class DebugMessage
    {
        // -------- Properties --------
        public eDebugMessageType Type { get; set; }
        public string Message { get; set; }
    }
}
