﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace AncientGame
{
    static class Benchmarking
    {
#if DEBUG
        /* -------- Private Fields -------- */
        private static List<BenchmarkResult> benchmarkResults = new List<BenchmarkResult>();
        private static bool runningBenchmark = false;
        // Constants
        private const int MAX_RESULTS_PER_LINE = 6;

        /* -------- Public Methods -------- */
        public static void Update(InputComponent i)
        {
            if (runningBenchmark)
            {
                // Display benchmark results
                Debug.Log("Benchmark finished, displaying results:");
                string output = "";
                int resultsThisLine = 0;
                foreach (BenchmarkResult br in benchmarkResults)
                {
                    output += br.ID + " - " + br.GetDuration().ToString() + "ms; ";
                    resultsThisLine++;
                    if (resultsThisLine >= MAX_RESULTS_PER_LINE)
                    {
                        Debug.Log(output);
                        output = "";
                        resultsThisLine = 0;
                    }
                }
                if (!String.IsNullOrEmpty(output))
                    Debug.Log(output);

                benchmarkResults.Clear();
                runningBenchmark = false;
                return;
            }

            if (i.KeyIsPressing(Keys.B))
                runningBenchmark = true;
        }
#endif

        public static void StartBenchmark(string id)
        {
#if DEBUG
            if (!runningBenchmark)
                return;

            benchmarkResults.Add(new BenchmarkResult(id));
#endif
        }
        public static void EndBenchmark(string id)
        {
#if DEBUG
            if (!runningBenchmark)
                return;

            BenchmarkResult result = benchmarkResults.Find(x => x.ID == id);
            if (result != null)
                result.End();
#endif
        }
    }
}
