﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    struct SplashScreen
    {
        /* -------- Public Fields -------- */
        public string TextureId;
        public Color BackgroundColor;
    }
}
