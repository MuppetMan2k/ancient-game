﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AncientGame
{
    class Settings
    {
        /* -------- Properties -------- */
        public Resolution Resolution { get; set; }
        public bool Windowed { get; set; }
        public float MasterVolume { get; set; }
        public float UISoundEffectsVolume { get; set; }
        public float GameSoundEffectVolume { get; set; }
        public float MusicVolume { get; set; }
        public float CameraMovementSpeed { get; private set; }
        public float CameraRotationSpeed { get; private set; }
        public float CameraZoomSpeed { get; private set; }
        public string Language { get; private set; }
        // Add further settings properties here...

        /* -------- Private Fields -------- */
        // Const params
        private const string RESOLUTION_IDENTIFIER = "Resolution";
        private const string WINDOWED_IDENTIFIER = "Windowed";
        private const string MASTER_VOLUME_IDENTIFIER = "MasterVolume";
        private const string UI_SOUND_VOLUME_IDENTIFIER = "UISoundVolume";
        private const string GAME_SOUND_VOLUME_IDENTIFIER = "GameSoundVolume";
        private const string MUSIC_VOLUME_IDENTIFIER = "MusicVolume";
        private const string CAMERA_MOVEMENT_SPEED_IDENTIFIER = "CameraMovementSpeed";
        private const string CAMERA_ROTATION_SPEED_IDENTIFIER = "CameraRotationSpeed";
        private const string CAMERA_ZOOM_SPEED_IDENTIFIER = "CameraZoomSpeed";
        private const string LANGUAGE_IDENTIFIER = "Language";
        // Add further identifier strings here...

        /* -------- Public Methods -------- */
        public string GetFileText()
        {
            string text = "";

            text += (RESOLUTION_IDENTIFIER + ":" + Resolution.CreateSettingsText());
            text += Environment.NewLine;
            text += (WINDOWED_IDENTIFIER + ":" + Windowed.ToString());
            text += Environment.NewLine;
            text += (MASTER_VOLUME_IDENTIFIER + ":" + MasterVolume.ToString("0.0"));
            text += Environment.NewLine;
            text += (UI_SOUND_VOLUME_IDENTIFIER + ":" + UISoundEffectsVolume.ToString("0.0"));
            text += Environment.NewLine;
            text += (GAME_SOUND_VOLUME_IDENTIFIER + ":" + GameSoundEffectVolume.ToString("0.0"));
            text += Environment.NewLine;
            text += (MUSIC_VOLUME_IDENTIFIER + ":" + MusicVolume.ToString("0.0"));
            text += Environment.NewLine;
            text += (CAMERA_MOVEMENT_SPEED_IDENTIFIER + ":" + CameraMovementSpeed.ToString("0.00"));
            text += Environment.NewLine;
            text += (CAMERA_ROTATION_SPEED_IDENTIFIER + ":" + CameraRotationSpeed.ToString("0.00"));
            text += Environment.NewLine;
            text += (CAMERA_ZOOM_SPEED_IDENTIFIER + ":" + CameraZoomSpeed.ToString("0.00"));
            text += Environment.NewLine;
            text += (LANGUAGE_IDENTIFIER + ":" + Language);
            // Add further properties here...

            return text;
        }

        public static Settings CreateSettingsFromLines(List<string> fileLines)
        {
            Settings settings = CreateDefaultSettings();

            foreach (string line in fileLines)
            {
                if (!line.Contains(':'))
                    continue;

                string[] splitParts = line.Split(new Char[] { ':' }, 2, StringSplitOptions.RemoveEmptyEntries);

                if (splitParts.Length < 2)
                    continue;

                string identifier = splitParts[0];
                string data = splitParts[1];

                switch (identifier)
                {
                    case RESOLUTION_IDENTIFIER:
                        settings.Resolution = Resolution.CreateResolutionFromSettingsText(data);
                        break;
                    case WINDOWED_IDENTIFIER:
                        settings.Windowed = SettingsUtilities.GetWindowedFromSettingsText(data);
                        break;
                    case MASTER_VOLUME_IDENTIFIER:
                        settings.MasterVolume = SettingsUtilities.GetAudioVolumeFromSettingsText(data);
                        break;
                    case UI_SOUND_VOLUME_IDENTIFIER:
                        settings.UISoundEffectsVolume = SettingsUtilities.GetAudioVolumeFromSettingsText(data);
                        break;
                    case GAME_SOUND_VOLUME_IDENTIFIER:
                        settings.GameSoundEffectVolume = SettingsUtilities.GetAudioVolumeFromSettingsText(data);
                        break;
                    case MUSIC_VOLUME_IDENTIFIER:
                        settings.MusicVolume = SettingsUtilities.GetAudioVolumeFromSettingsText(data);
                        break;
                    case CAMERA_MOVEMENT_SPEED_IDENTIFIER:
                        settings.CameraMovementSpeed = SettingsUtilities.GetFloatFromSettingsText(data);
                        break;
                    case CAMERA_ROTATION_SPEED_IDENTIFIER:
                        settings.CameraRotationSpeed = SettingsUtilities.GetFloatFromSettingsText(data);
                        break;
                    case CAMERA_ZOOM_SPEED_IDENTIFIER:
                        settings.CameraZoomSpeed = SettingsUtilities.GetFloatFromSettingsText(data);
                        break;
                    case LANGUAGE_IDENTIFIER:
                        settings.Language = data;
                        break;
                    // Add further property cases here...
                    default:
                        break;
                }
            }

            return settings;
        }

        public static Settings CreateDefaultSettings()
        {
            Settings settings = new Settings();

            settings.Resolution = GraphicsComponent.GetDefaultResolution();
            settings.Windowed = false;
            settings.MasterVolume = 1f;
            settings.UISoundEffectsVolume = 1f;
            settings.GameSoundEffectVolume = 1f;
            settings.MusicVolume = 1f;
            settings.CameraMovementSpeed = 1.0f;
            settings.CameraRotationSpeed = 90.0f;
            settings.CameraZoomSpeed = 1.0f;
            settings.Language = "en-us";
            // Add further properties here...

            return settings;
        }
    }
}
