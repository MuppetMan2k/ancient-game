﻿using System.Collections.Generic;

namespace AncientGame
{
    static class SettingsManager
    {
        /* -------- Properties -------- */
        public static Settings Settings { get; private set; }

        /* -------- Private Fields -------- */
        // Const params
        private static string SETTINGS_FILE_NAME = "Settings.cfg";

        /* -------- Public Methods -------- */
        public static void Initialize()
        {
            LoadSettings();
        }

        public static void OnGameExit()
        {
            SaveSettings();
        }

        /* -------- Private Methods --------- */
        private static void LoadSettings()
        {
            if (!FileManager.FileExistsInStore(eFileType.SETTINGS, SETTINGS_FILE_NAME))
            {
                Settings = Settings.CreateDefaultSettings();
                return;
            }

            string path = FileManager.GetFilePath(eFileType.SETTINGS, SETTINGS_FILE_NAME);
            List<string> settingsLines = FileManager.GetTextFileLines(path);
            Settings = Settings.CreateSettingsFromLines(settingsLines);
        }

        private static void SaveSettings()
        {
            if (!FileManager.ReadyToSaveFile(eFileType.SETTINGS))
            {
                Debug.LogWarning("Unable to save settings file!");
                return;
            }

            string fileText = Settings.GetFileText();
            string path = FileManager.GetFilePath(eFileType.SETTINGS, SETTINGS_FILE_NAME);

            if (!FileManager.SaveTextFile(path, fileText))
            {
                Debug.LogWarning("Unable to save text file at {0}", path);
            }
        }
    }
}
