﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace AncientGame
{
    // -------- Enumerations --------
    enum eFileType
    {
        SETTINGS
    }

    static class FileManager
    {
        // -------- Private Fields --------
        private static string fileStoreRootPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        // Const params
        private const string SETTINGS_FOLDER_NAME = "Settings";

        // -------- Public Methods --------
        public static string GetFileStorePath()
        {
            string fileStorePath = System.IO.Path.Combine(fileStoreRootPath, ProgramInfo.CompanyName, ProgramInfo.ProgramName);

            return fileStorePath;
        }
        public static string GetFileTypeStorePath(eFileType fileType)
        {
            string folder = null;
            switch (fileType)
            {
                case eFileType.SETTINGS:
                    folder = SETTINGS_FOLDER_NAME;
                    break;
                default:
                    break;
            }

            if (string.IsNullOrEmpty(folder))
                return null;

            string fileTypeStorePath = System.IO.Path.Combine(GetFileStorePath(), folder);
            return fileTypeStorePath;
        }
        public static string GetFilePath(eFileType fileType, string fileName)
        {
            string filePath = System.IO.Path.Combine(GetFileTypeStorePath(fileType), fileName);
            return filePath;
        }
        public static bool FileExistsInStore(eFileType type, string fileName)
        {
            string path = GetFilePath(type, fileName);

            return File.Exists(path);
        }
        public static bool ReadyToSaveFile(eFileType type)
        {
            if (!EnsureFileStoreExists())
                return false;

            if (!EnsureFileTypeStoreExists(type))
                return false;

            return true;
        }

        public static List<string> GetTextFileLines(string path)
        {
            List<string> lines = new List<string>();

            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    while (!sr.EndOfStream)
                    {
                        String line = sr.ReadLine();
                        lines.Add(line);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning("Tried to open text file: {0}, but caught exception: {1}", path, e.Message);
            }

            return lines;
        }

        public static bool SaveTextFile(string path, string text)
        {
            try
            {
                File.WriteAllText(path, text, Encoding.UTF8);
                return true;
            }
            catch (Exception e)
            {
                Debug.LogWarning("Tried to save text file at {0} but caught exception: {1}", path, e.Message);
                return false;
            }
        }

        /* -------- Private Methods --------- */
        private static bool EnsureFileStoreExists()
        {
            string path = GetFileStorePath();
            if (Directory.Exists(path))
                return true;

            return TryCreateDirectory(path);
        }
        private static bool EnsureFileTypeStoreExists(eFileType fileType)
        {
            string path = GetFileTypeStorePath(fileType);
            if (Directory.Exists(path))
                return true;

            return TryCreateDirectory(path);
        }

        private static bool TryCreateDirectory(string path)
        {
            try
            {
                DirectoryInfo info = Directory.CreateDirectory(path);
                return info.Exists;
            }
            catch (Exception e)
            {
                Debug.LogWarning("Tried to create directory at {0} but caught exception: {1}", path, e.Message);
                return false;
            }
        }
    }
}
