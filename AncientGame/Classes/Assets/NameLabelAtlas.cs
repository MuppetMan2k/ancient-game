﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class NameLabelAtlas
    {
        /* -------- Properties -------- */
        public NameLabelAtlasData Data { get; private set; }
        public Texture2D AtlasTexture { get; private set; }

        /* -------- Constructors -------- */
        public NameLabelAtlas(NameLabelAtlasData data, ContentManager content)
        {
            Data = data;
            AtlasTexture = AssetLoader.LoadTexture(data.atlasTextureID, content);
        }

        /* -------- Public Methods -------- */
        public Point GetCharPoint(char c)
        {
            switch (c)
            {
                case 'A':
                    return Data.aPosition;
                case 'B':
                    return Data.bPosition;
                case 'C':
                    return Data.cPosition;
                case 'D':
                    return Data.dPosition;
                case 'E':
                    return Data.ePosition;
                case 'F':
                    return Data.fPosition;
                case 'G':
                    return Data.gPosition;
                case 'H':
                    return Data.hPosition;
                case 'I':
                    return Data.iPosition;
                case 'J':
                    return Data.jPosition;
                case 'K':
                    return Data.kPosition;
                case 'L':
                    return Data.lPosition;
                case 'M':
                    return Data.mPosition;
                case 'N':
                    return Data.nPosition;
                case 'O':
                    return Data.oPosition;
                case 'P':
                    return Data.pPosition;
                case 'Q':
                    return Data.qPosition;
                case 'R':
                    return Data.rPosition;
                case 'S':
                    return Data.sPosition;
                case 'T':
                    return Data.tPosition;
                case 'U':
                    return Data.uPosition;
                case 'V':
                    return Data.vPosition;
                case 'W':
                    return Data.wPosition;
                case 'X':
                    return Data.xPosition;
                case 'Y':
                    return Data.yPosition;
                case 'Z':
                    return Data.zPosition;
                default:
                    return new Point(0, 0);
            }
        }
        public int GetCharWidth(char c)
        {
            if (char.IsWhiteSpace(c))
                return Data.spaceSize;

            switch (c)
            {
                case 'A':
                    return Data.aWidth;
                case 'B':
                    return Data.bWidth;
                case 'C':
                    return Data.cWidth;
                case 'D':
                    return Data.dWidth;
                case 'E':
                    return Data.eWidth;
                case 'F':
                    return Data.fWidth;
                case 'G':
                    return Data.gWidth;
                case 'H':
                    return Data.hWidth;
                case 'I':
                    return Data.iWidth;
                case 'J':
                    return Data.jWidth;
                case 'K':
                    return Data.kWidth;
                case 'L':
                    return Data.lWidth;
                case 'M':
                    return Data.mWidth;
                case 'N':
                    return Data.nWidth;
                case 'O':
                    return Data.oWidth;
                case 'P':
                    return Data.pWidth;
                case 'Q':
                    return Data.qWidth;
                case 'R':
                    return Data.rWidth;
                case 'S':
                    return Data.sWidth;
                case 'T':
                    return Data.tWidth;
                case 'U':
                    return Data.uWidth;
                case 'V':
                    return Data.vWidth;
                case 'W':
                    return Data.wWidth;
                case 'X':
                    return Data.xWidth;
                case 'Y':
                    return Data.yWidth;
                case 'Z':
                    return Data.zWidth;
                default:
                    return 0;
            }
        }
        
        public Vector2 GetCharTopLeftTextureCoordinate(char c)
        {
            Point point = GetCharPoint(c);
            return new Vector2((float)point.X / (float)AtlasTexture.Width, (float)point.Y / (float)AtlasTexture.Height);
        }
        public Vector2 GetCharTopRightTextureCoordinate(char c)
        {
            Point point = GetCharPoint(c);
            point.X += GetCharWidth(c);
            return new Vector2((float)point.X / (float)AtlasTexture.Width, (float)point.Y / (float)AtlasTexture.Height);
        }
        public Vector2 GetCharBottomLeftTextureCoordinate(char c)
        {
            Point point = GetCharPoint(c);
            point.Y += Data.height;
            return new Vector2((float)point.X / (float)AtlasTexture.Width, (float)point.Y / (float)AtlasTexture.Height);
        }
        public Vector2 GetCharBottomRightTextureCoordinate(char c)
        {
            Point point = GetCharPoint(c);
            point.X += GetCharWidth(c);
            point.Y += Data.height;
            return new Vector2((float)point.X / (float)AtlasTexture.Width, (float)point.Y / (float)AtlasTexture.Height);
        }
    }
}
