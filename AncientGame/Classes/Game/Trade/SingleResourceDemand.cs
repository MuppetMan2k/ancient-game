﻿namespace AncientGame
{
    class SingleResourceDemand
    {
        /* -------- Properties -------- */
        public Resource Resource { get; private set; }
        public float Quantity { get; private set; }

        /* -------- Constructors -------- */
        public SingleResourceDemand(Resource inResource, float inQuantity)
        {
            Resource = inResource;
            Quantity = inQuantity;
        }
    }
}
