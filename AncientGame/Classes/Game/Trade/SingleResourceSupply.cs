﻿namespace AncientGame
{
    class SingleResourceSupply
    {
        /* -------- Properties -------- */
        public Resource Resource { get; private set; }
        public float Quantity { get; private set; }

        /* -------- Constructors -------- */
        public SingleResourceSupply(Resource inResource, float inQuantity)
        {
            Resource = inResource;
            Quantity = inQuantity;
        }
    }
}
