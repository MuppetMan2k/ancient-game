﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class TradeSet
    {
        /* -------- Properties -------- */
        public List<TradePath> ImportTradePaths { get; private set; }
        public List<TradePath> ExportTradePaths { get; private set; }
        public List<TradePath> ThroughportTradePaths { get; private set; }
        // Temporary variables
        public List<SingleResourceSupply> SupplySet { get; private set; }
        public List<SingleResourceDemand> DemandSet { get; private set; }

        /* -------- Constructors -------- */
        public TradeSet()
        {
            ImportTradePaths = new List<TradePath>();
            ExportTradePaths = new List<TradePath>();
            ThroughportTradePaths = new List<TradePath>();

            SupplySet = new List<SingleResourceSupply>();
            DemandSet = new List<SingleResourceDemand>();
        }

        /* -------- Public Methods -------- */
        public void Reset()
        {
            ImportTradePaths.Clear();
            ExportTradePaths.Clear();
            ThroughportTradePaths.Clear();
            SupplySet.Clear();
            DemandSet.Clear();
        }
        public void ClearTemporaryVariables()
        {
            SupplySet.Clear();
            DemandSet.Clear();
        }

        public bool TryAddImportTradePath(TradePath newTradePath)
        {
            TradePath tradePathToReplace = null;

            foreach (TradePath tp in ImportTradePaths)
            {
                if (tp.ExportLandTerritory != newTradePath.ExportLandTerritory)
                    continue;

                if (newTradePath.MovementPointCost >= tp.MovementPointCost)
                    return false;

                tradePathToReplace = tp;
            }

            if (tradePathToReplace != null)
                ImportTradePaths.Remove(tradePathToReplace);
            ImportTradePaths.Add(newTradePath);
            return true;
        }
        public void OnFinishedAddingImportTradePaths()
        {
            foreach (TradePath tp in ImportTradePaths)
            {
                tp.ExportLandTerritory.Variables.TradeSet.ExportTradePaths.Add(tp);
                foreach (LandTerritory lt in tp.ThroughportLandTerritories)
                    lt.Variables.TradeSet.ThroughportTradePaths.Add(tp);
            }
        }

        public List<TradeRequest> GetImportTradeRequests(Resource resource)
        {
            List<TradeRequest> importTradeRequests = new List<TradeRequest>();

            foreach (TradePath tp in ImportTradePaths)
                foreach (TradeRequest tr in tp.TradeRequests)
                    if (tr.Resource == resource)
                        importTradeRequests.Add(tr);

            return importTradeRequests;
        }
        public List<TradeRequest> GetExportTradeRequests(Resource resource)
        {
            List<TradeRequest> exportTradeRequests = new List<TradeRequest>();

            foreach (TradePath tp in ExportTradePaths)
                foreach (TradeRequest tr in tp.TradeRequests)
                    if (tr.Resource == resource)
                        exportTradeRequests.Add(tr);

            return exportTradeRequests;
        }
        public void RemoveImportTradeRequests(Resource resource)
        {
            foreach (TradePath tp in ImportTradePaths)
                tp.TradeRequests.RemoveAll(x => x.Resource == resource);
        }
        public void RemoveExportTradeRequests(Resource resource)
        {
            foreach (TradePath tp in ExportTradePaths)
                tp.TradeRequests.RemoveAll(x => x.Resource == resource);
        }
        public float GetImportedResourceQuantity(Resource r)
        {
            float quantity = 0f;

            foreach (TradePath tp in ImportTradePaths)
                quantity += tp.GetTradedResourceQuantity(r);

            return quantity;
        }
        public float GetExportedResourceQuantity(Resource r)
        {
            float quantity = 0f;

            foreach (TradePath tp in ExportTradePaths)
                quantity += tp.GetTradedResourceQuantity(r);

            return quantity;
        }

        /* -------- Static Methods -------- */
        public static TradeSet CreateFromData(TradeSetData data, GameManagerContainer gameManagers)
        {
            TradeSet ts = new TradeSet();

            foreach (TradePathData tpd in data.importTradePathDatas)
                ts.ImportTradePaths.Add(TradePath.CreateFromData(tpd, gameManagers));
            foreach (TradePathData tpd in data.exportTradePathDatas)
                ts.ExportTradePaths.Add(TradePath.CreateFromData(tpd, gameManagers));
            foreach (TradePathData tpd in data.throughportTradePathDatas)
                ts.ThroughportTradePaths.Add(TradePath.CreateFromData(tpd, gameManagers));

            return ts;
        }
    }
}
