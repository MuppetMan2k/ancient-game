﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eResourceTradeType
    {
        AGRICULTURE_IMPORTS,
        AGRICULTURE_THROUGHPORTS,
        FOOD_IMPORTS,
        FOOD_THROUGHPORTS,
        LIVESTOCK_IMPORTS,
        LIVESTOCK_THROUGHPORTS,
        MINERAL_IMPORTS,
        MINERAL_THROUGHPORTS,
        ANIMAL_IMPORTS,
        ANIMAL_THROUGHPORTS,
        LUXURY_IMPORTS,
        LUXURY_THROUGHPORTS,
        WEAPONRY_IMPORTS,
        WEAPONRY_THROUGHPORTS,
        GENERIC_IMPORTS,
        GENERIC_THROUGHPORTS,
        // Blanket types
        ALL_IMPORTS,
        ALL_THROUGHPORTS
    }

    class TradePath
    {
        /* -------- Properties -------- */
        public LandTerritory ImportLandTerritory { get; private set; }
        public LandTerritory ExportLandTerritory { get; private set; }
        public List<LandTerritory> ThroughportLandTerritories { get; private set; }
        public int MovementPointCost { get; private set; }
        public List<SingleResourcePresence> TradedResources { get; private set; }
        // Temporary variables
        public List<TradeRequest> TradeRequests { get; private set; }

        /* -------- Constructors -------- */
        public TradePath()
        {
            ThroughportLandTerritories = new List<LandTerritory>();
            TradedResources = new List<SingleResourcePresence>();
            TradeRequests = new List<TradeRequest>();
        }
        public TradePath(LandTerritory inImportLandTerritory, LandTerritory inExportLandTerritory, List<LandTerritory> inThroughportLandTerritories, int inMovementPointCost)
        {
            ImportLandTerritory = inImportLandTerritory;
            ExportLandTerritory = inExportLandTerritory;
            ThroughportLandTerritories = inThroughportLandTerritories;
            MovementPointCost = inMovementPointCost;

            TradedResources = new List<SingleResourcePresence>();
            TradeRequests = new List<TradeRequest>();
        }

        /* -------- Public Methods -------- */
        public void ClearTemporaryVariables()
        {
            TradeRequests.Clear();
        }

        public ModifierSet GetImportModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            set.Merge(ImportLandTerritory.GetModifierSet(gameManagers));

            return set;
        }
        public ModifierSet GetThroughportModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            foreach (LandTerritory lt in ThroughportLandTerritories)
                set.Merge(lt.GetModifierSet(gameManagers));

            return set;
        }

        public TradePath CreateExtended(LandTerritory extendedExportLandTerritory, int extendedMovementPointCost)
        {
            TradePath tp = new TradePath();

            tp.ImportLandTerritory = ImportLandTerritory;
            tp.ExportLandTerritory = extendedExportLandTerritory;
            foreach (LandTerritory tlt in ThroughportLandTerritories)
                tp.ThroughportLandTerritories.Add(tlt);
            tp.ThroughportLandTerritories.Add(ExportLandTerritory);
            tp.MovementPointCost = MovementPointCost + extendedMovementPointCost;

            return tp;
        }

        public float GetTradedResourceQuantity(Resource r)
        {
            SingleResourcePresence srp = TradedResources.Find(x => x.Resource == r);

            if (srp == null)
                return 0f;

            return srp.Quantity;
        }
        public float GetTradedResourcesQuantity()
        {
            float quantity = 0f;
            foreach (SingleResourcePresence srp in TradedResources)
                quantity += srp.Quantity;
            return quantity;
        }
        public float GetTradedResourcesValue()
        {
            float value = 0f;
            foreach (SingleResourcePresence srp in TradedResources)
                value += srp.Quantity * srp.Resource.Value;
            return value;
        }

        public float GetTradeEfficiency(eResourceType resourceType, GameManagerContainer gameManagers)
        {
            float efficiency = CalibrationManager.TradeCalibration.Data.baseTradePathEfficiency;

            efficiency = GetImportModifierSet(gameManagers).Modify_ResourceImportTradeEfficiency(efficiency, resourceType);
            efficiency = GetThroughportModifierSet(gameManagers).Modify_ResourceThroughportTradeEfficiency(efficiency, resourceType);

            efficiency = Mathf.Clamp(efficiency, 0f, 100f);
            return efficiency;
        }

        /* -------- Static Methods -------- */
        public static TradePath CreateFromData(TradePathData data, GameManagerContainer gameManagers)
        {
            TradePath tradePath = new TradePath();

            tradePath.ImportLandTerritory = gameManagers.MapManager.LandTerritoryManager.GetLandTerritory(data.importLandTerritoryID);
            tradePath.ExportLandTerritory = gameManagers.MapManager.LandTerritoryManager.GetLandTerritory(data.exportLandTerritoryID);
            foreach (string ltid in data.throughportLandTerritoryIDs)
                tradePath.ThroughportLandTerritories.Add(gameManagers.MapManager.LandTerritoryManager.GetLandTerritory(ltid));
            tradePath.MovementPointCost = data.movementPointCost;
            foreach (SingleResourcePresenceData srpd in data.tradedResourceDatas)
                tradePath.TradedResources.Add(SingleResourcePresence.CreateFromData(srpd, gameManagers.ResourceManager));

            return tradePath;
        }

        public static string GetResourceTradeTypeName(eResourceTradeType resourceTradeType)
        {
            switch (resourceTradeType)
            {
                case eResourceTradeType.AGRICULTURE_IMPORTS:
                    return Localization.Localize("text_wealth-source_agriculture-import");
                case eResourceTradeType.AGRICULTURE_THROUGHPORTS:
                    return Localization.Localize("text_wealth-source_agriculture-throughport");
                case eResourceTradeType.FOOD_IMPORTS:
                    return Localization.Localize("text_wealth-source_food-import");
                case eResourceTradeType.FOOD_THROUGHPORTS:
                    return Localization.Localize("text_wealth-source_food-throughport");
                case eResourceTradeType.LIVESTOCK_IMPORTS:
                    return Localization.Localize("text_wealth-source_livestock-import");
                case eResourceTradeType.LIVESTOCK_THROUGHPORTS:
                    return Localization.Localize("text_wealth-source_livestock-throughport");
                case eResourceTradeType.MINERAL_IMPORTS:
                    return Localization.Localize("text_wealth-source_mineral-import");
                case eResourceTradeType.MINERAL_THROUGHPORTS:
                    return Localization.Localize("text_wealth-source_mineral-throughport");
                case eResourceTradeType.ANIMAL_IMPORTS:
                    return Localization.Localize("text_wealth-source_animal-import");
                case eResourceTradeType.ANIMAL_THROUGHPORTS:
                    return Localization.Localize("text_wealth-source_animal-throughport");
                case eResourceTradeType.LUXURY_IMPORTS:
                    return Localization.Localize("text_wealth-source_luxury-import");
                case eResourceTradeType.LUXURY_THROUGHPORTS:
                    return Localization.Localize("text_wealth-source_luxury-throughport");
                case eResourceTradeType.WEAPONRY_IMPORTS:
                    return Localization.Localize("text_wealth-source_weaponry-import");
                case eResourceTradeType.WEAPONRY_THROUGHPORTS:
                    return Localization.Localize("text_wealth-source_weaponry-throughport");
                case eResourceTradeType.GENERIC_IMPORTS:
                    return Localization.Localize("text_wealth-source_generic-import");
                case eResourceTradeType.GENERIC_THROUGHPORTS:
                    return Localization.Localize("text_wealth-source_generic-throughport");
                case eResourceTradeType.ALL_IMPORTS:
                    return Localization.Localize("text_wealth-source_all-imports");
                case eResourceTradeType.ALL_THROUGHPORTS:
                    return Localization.Localize("text_wealth-source_all-throughports");
                default:
                    Debug.LogUnrecognizedValueWarning(resourceTradeType);
                    return "";
            }
        }
        public static eResourceTradeType GetResourceTradeTypeForResourceType_Import(eResourceType resourceType)
        {
            switch (resourceType)
            {
                case eResourceType.AGRICULTURE:
                    return eResourceTradeType.AGRICULTURE_IMPORTS;
                case eResourceType.FOOD:
                    return eResourceTradeType.FOOD_IMPORTS;
                case eResourceType.LIVESTOCK:
                    return eResourceTradeType.LIVESTOCK_IMPORTS;
                case eResourceType.MINERAL:
                    return eResourceTradeType.MINERAL_IMPORTS;
                case eResourceType.ANIMAL:
                    return eResourceTradeType.ANIMAL_IMPORTS;
                case eResourceType.LUXURY:
                    return eResourceTradeType.LUXURY_IMPORTS;
                case eResourceType.WEAPONRY:
                    return eResourceTradeType.WEAPONRY_IMPORTS;
                case eResourceType.GENERIC:
                    return eResourceTradeType.GENERIC_IMPORTS;
                default:
                    Debug.LogUnrecognizedValueWarning(resourceType);
                    return eResourceTradeType.GENERIC_IMPORTS;
            }
        }
        public static eResourceTradeType GetResourceTradeTypeForResourceType_Throughport(eResourceType resourceType)
        {
            switch (resourceType)
            {
                case eResourceType.AGRICULTURE:
                    return eResourceTradeType.AGRICULTURE_THROUGHPORTS;
                case eResourceType.FOOD:
                    return eResourceTradeType.FOOD_THROUGHPORTS;
                case eResourceType.LIVESTOCK:
                    return eResourceTradeType.LIVESTOCK_THROUGHPORTS;
                case eResourceType.MINERAL:
                    return eResourceTradeType.MINERAL_THROUGHPORTS;
                case eResourceType.ANIMAL:
                    return eResourceTradeType.ANIMAL_THROUGHPORTS;
                case eResourceType.LUXURY:
                    return eResourceTradeType.LUXURY_THROUGHPORTS;
                case eResourceType.WEAPONRY:
                    return eResourceTradeType.WEAPONRY_THROUGHPORTS;
                case eResourceType.GENERIC:
                    return eResourceTradeType.GENERIC_THROUGHPORTS;
                default:
                    Debug.LogUnrecognizedValueWarning(resourceType);
                    return eResourceTradeType.GENERIC_THROUGHPORTS;
            }
        }
    }
}
