﻿namespace AncientGame
{
    class TradeRequest
    {
        /* -------- Properties -------- */
        public TradePath ParentTradePath { get; private set; }
        public Resource Resource { get; private set; }
        public float Supply { get; private set; }
        public float Demand { get; private set; }
        public float TotalSupplyOfOtherTradeRequests { get; private set; }
        public float TotalDemandOfOtherTradeRequests { get; private set; }

        /* -------- Constructors -------- */
        public TradeRequest(TradePath inParentTradePath, Resource inResource, float inSupply, float inDemand, float inTotalSupplyOfOtherTradeRequests, float inTotalDemandOfOtherTradeRequests)
        {
            ParentTradePath = inParentTradePath;
            Resource = inResource;
            Supply = inSupply;
            Demand = inDemand;
            TotalSupplyOfOtherTradeRequests = inTotalSupplyOfOtherTradeRequests;
            TotalDemandOfOtherTradeRequests = inTotalDemandOfOtherTradeRequests;
        }
    }
}
