﻿using System.Collections.Generic;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eTradeCalculationStage
    {
        TRADE_PATH_CALCULATION,
        PRIMARY_RESOURCE_TRADE_CALCULATION,
        SECONDARY_RESOURCE_TRADE_CALCULATION
    }

    enum eTradePathCalculationStage
    {
        CALCULATE_TRADE_PATHS,
        SET_TRADE_PATHS_TO_TRADE_ROUTES
    }

    enum eResourceTradeCalculationStage
    {
        CALCULATE_SUPPLY_AND_DEMAND,
        MAKE_TRADE_REQUESTS,
        REMOVE_IMPORT_EXPORT_DUPLICATES,
        SEND_RESOURCES,
        CLEAN_UP
    }

    class TradeManager
    {
        /* -------- Properties -------- */
        public bool Active { get; private set; }
        public eTradeCalculationStage Stage { get; private set; }
        public eTradePathCalculationStage TradePathStage { get; private set; }
        public eResourceTradeCalculationStage ResourceStage { get; private set; }

        /* -------- Private Fields -------- */
        private int landTerritoryIndex;
        // Constants
        private const int NUM_LAND_TERRITORIES_TO_CALCULATE_PATH_PER_FRAME = 1;
        private const int NUM_LAND_TERRITORIES_TO_SET_TRADE_PATHS_TO_TRADE_ROUTES_PER_FRAME = 1;
        private const int NUM_LAND_TERRITORIES_TO_CALCULATE_SUPPLY_AND_DEMAND_PER_FRAME = 1;
        private const int NUM_LAND_TERRITORIES_TO_MAKE_TRADE_REQUESTS_PER_FRAME = 1;
        private const int NUM_LAND_TERRITORIES_TO_REMOVE_IMPORT_EXPORT_DUPLICATES_PER_FRAME = 1;
        private const int NUM_LAND_TERRITORIES_TO_SEND_RESOURCES_PER_FRAME = 1;
        private const int NUM_LAND_TERRITORIES_TO_CLEAN_UP_PER_FRAME = 1;

        /* -------- Constructors -------- */
        public TradeManager()
        {
            landTerritoryIndex = 0;
        }

        /* -------- Public Methods -------- */
        public void Update(GameManagerContainer gameManagers)
        {
            if (!Active)
                return;

            switch (Stage)
            {
                case eTradeCalculationStage.TRADE_PATH_CALCULATION:
                    Update_TradePathCalculation(gameManagers);
                    break;
                case eTradeCalculationStage.PRIMARY_RESOURCE_TRADE_CALCULATION:
                    Update_PrimaryResourceTradeCalculation(gameManagers);
                    break;
                case eTradeCalculationStage.SECONDARY_RESOURCE_TRADE_CALCULATION:
                    Update_SecondaryResourceTradeCalculation(gameManagers);
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(Stage);
                    break;
            }
        }

        public void StartTradeCalculation(eTradeCalculationStage inStage, GameManagerContainer gameManagers)
        {
            if (Active)
                return;

            Active = true;
            Stage = inStage;
            landTerritoryIndex = 0;

            switch (Stage)
            {
                case eTradeCalculationStage.TRADE_PATH_CALCULATION:
                    OnStart_TradePathCalculation(gameManagers);
                    break;
                case eTradeCalculationStage.PRIMARY_RESOURCE_TRADE_CALCULATION:
                    OnStart_PrimaryResourceTradeCalculation();
                    break;
                case eTradeCalculationStage.SECONDARY_RESOURCE_TRADE_CALCULATION:
                    OnStart_SecondaryResourceTradeCalculation();
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(Stage);
                    break;
            }
        }

        /* -------- Private Methods --------- */
        private void OnStart_TradePathCalculation(GameManagerContainer gameManagers)
        {
            TradePathStage = eTradePathCalculationStage.CALCULATE_TRADE_PATHS;

            gameManagers.MapManager.LandTradeRouteManager.UpdateLandTradeRoutesBlockaded(gameManagers.FactionManager);
            gameManagers.MapManager.LandTradeRouteManager.ClearLandTradeRouteTradePaths();
            gameManagers.MapManager.SeaTradeRouteManager.UpdateSeaTradeRoutesBlockaded(gameManagers.FactionManager);
            gameManagers.MapManager.SeaTradeRouteManager.ClearSeaTradeRouteTradePaths();
        }
        private void OnStart_PrimaryResourceTradeCalculation()
        {
            ResourceStage = eResourceTradeCalculationStage.CALCULATE_SUPPLY_AND_DEMAND;
        }
        private void OnStart_SecondaryResourceTradeCalculation()
        {
            ResourceStage = eResourceTradeCalculationStage.CALCULATE_SUPPLY_AND_DEMAND;
        }

        private void Update_TradePathCalculation(GameManagerContainer gameManagers)
        {
            switch (TradePathStage)
            {
                case eTradePathCalculationStage.CALCULATE_TRADE_PATHS:
                    Update_TradePathCalculation_CalculateTradePaths(gameManagers);
                    break;
                case eTradePathCalculationStage.SET_TRADE_PATHS_TO_TRADE_ROUTES:
                    Update_TradePathCalculation_SetTradePathsToTradeRoutes(gameManagers);
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(TradePathStage);
                    break;
            }
        }
        private void Update_TradePathCalculation_CalculateTradePaths(GameManagerContainer gameManagers)
        {
            List<LandTerritory> landTerritories = gameManagers.MapManager.LandTerritoryManager.GetAllLandTerritories();

            for (int i = 0; i < NUM_LAND_TERRITORIES_TO_CALCULATE_PATH_PER_FRAME; i++)
            {
                LandTerritory lt = landTerritories[landTerritoryIndex];
                CalculateTradePaths(lt, gameManagers);
                landTerritoryIndex++;
                if (landTerritoryIndex >= landTerritories.Count)
                {
                    TradePathStage = eTradePathCalculationStage.SET_TRADE_PATHS_TO_TRADE_ROUTES;
                    landTerritoryIndex = 0;
                    return;
                }
            }
        }
        private void Update_TradePathCalculation_SetTradePathsToTradeRoutes(GameManagerContainer gameManagers)
        {
            List<LandTerritory> landTerritories = gameManagers.MapManager.LandTerritoryManager.GetAllLandTerritories();
            
            for (int i = 0; i < NUM_LAND_TERRITORIES_TO_SET_TRADE_PATHS_TO_TRADE_ROUTES_PER_FRAME; i++)
            {
                LandTerritory lt = landTerritories[landTerritoryIndex];
                SetTradePathsToTradeRoutes(lt);
                landTerritoryIndex++;
                if (landTerritoryIndex >= landTerritories.Count)
                {
                    Active = false;
                    return;
                }
            }
        }
        private void Update_PrimaryResourceTradeCalculation(GameManagerContainer gameManagers)
        {
            Update_ResourceTradeCalculation(eResourceTier.PRIMARY, gameManagers);
        }
        private void Update_SecondaryResourceTradeCalculation(GameManagerContainer gameManagers)
        {
            Update_ResourceTradeCalculation(eResourceTier.SECONDARY, gameManagers);
        }
        private void Update_ResourceTradeCalculation(eResourceTier tier, GameManagerContainer gameManagers)
        {
            switch (ResourceStage)
            {
                case eResourceTradeCalculationStage.CALCULATE_SUPPLY_AND_DEMAND:
                    Update_ResourceTradeCalculation_CalculateSupplyAndDemand(tier, gameManagers);
                    break;
                case eResourceTradeCalculationStage.MAKE_TRADE_REQUESTS:
                    Update_ResourceTradeCalculation_MakeTradeRequests(tier, gameManagers);
                    break;
                case eResourceTradeCalculationStage.REMOVE_IMPORT_EXPORT_DUPLICATES:
                    Update_ResourceTradeCalculation_RemoveImportExportRequestDuplicates(gameManagers);
                    break;
                case eResourceTradeCalculationStage.SEND_RESOURCES:
                    Update_ResourceTradeCalculation_SendResources(tier, gameManagers);
                    break;
                case eResourceTradeCalculationStage.CLEAN_UP:
                    Update_ResourceTradeCalculation_CleanUp(tier, gameManagers);
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(ResourceStage);
                    break;
            }
        }
        private void Update_ResourceTradeCalculation_CalculateSupplyAndDemand(eResourceTier tier, GameManagerContainer gameManagers)
        {
            List<LandTerritory> landTerritories = gameManagers.MapManager.LandTerritoryManager.GetAllLandTerritories();

            for (int i = 0; i < NUM_LAND_TERRITORIES_TO_CALCULATE_SUPPLY_AND_DEMAND_PER_FRAME; i++)
            {
                LandTerritory lt = landTerritories[landTerritoryIndex];
                CalculateSupplyAndDemand(tier, lt, gameManagers.ResourceManager);
                landTerritoryIndex++;
                if (landTerritoryIndex >= landTerritories.Count)
                {
                    landTerritoryIndex = 0;
                    ResourceStage = eResourceTradeCalculationStage.MAKE_TRADE_REQUESTS;
                    return;
                }
            }
        }
        private void Update_ResourceTradeCalculation_MakeTradeRequests(eResourceTier tier, GameManagerContainer gameManagers)
        {
            List<LandTerritory> landTerritories = gameManagers.MapManager.LandTerritoryManager.GetAllLandTerritories();

            for (int i = 0; i < NUM_LAND_TERRITORIES_TO_MAKE_TRADE_REQUESTS_PER_FRAME; i++)
            {
                LandTerritory lt = landTerritories[landTerritoryIndex];
                MakeTradeRequests(tier, lt);
                landTerritoryIndex++;
                if (landTerritoryIndex >= landTerritories.Count)
                {
                    landTerritoryIndex = 0;
                    ResourceStage = eResourceTradeCalculationStage.REMOVE_IMPORT_EXPORT_DUPLICATES;
                    return;
                }
            }
        }
        private void Update_ResourceTradeCalculation_RemoveImportExportRequestDuplicates(GameManagerContainer gameManagers)
        {
            List<LandTerritory> landTerritories = gameManagers.MapManager.LandTerritoryManager.GetAllLandTerritories();

            for (int i = 0; i < NUM_LAND_TERRITORIES_TO_REMOVE_IMPORT_EXPORT_DUPLICATES_PER_FRAME; i++)
            {
                LandTerritory lt = landTerritories[landTerritoryIndex];
                RemoveImportExportRequestDuplicates(lt.Variables.TradeSet, gameManagers.ResourceManager);
                landTerritoryIndex++;
                if (landTerritoryIndex >= landTerritories.Count)
                {
                    landTerritoryIndex = 0;
                    ResourceStage = eResourceTradeCalculationStage.SEND_RESOURCES;
                    return;
                }
            }
        }
        private void Update_ResourceTradeCalculation_SendResources(eResourceTier tier, GameManagerContainer gameManagers)
        {
            List<LandTerritory> landTerritories = gameManagers.MapManager.LandTerritoryManager.GetAllLandTerritories();

            for (int i = 0; i < NUM_LAND_TERRITORIES_TO_SEND_RESOURCES_PER_FRAME; i++)
            {
                LandTerritory lt = landTerritories[landTerritoryIndex];
                SendResources(tier, lt, gameManagers);
                landTerritoryIndex++;
                if (landTerritoryIndex >= landTerritories.Count)
                {
                    landTerritoryIndex = 0;
                    ResourceStage = eResourceTradeCalculationStage.CLEAN_UP;
                    return;
                }
            }
        }
        private void Update_ResourceTradeCalculation_CleanUp(eResourceTier tier, GameManagerContainer gameManagers)
        {
            List<LandTerritory> landTerritories = gameManagers.MapManager.LandTerritoryManager.GetAllLandTerritories();

            for (int i = 0; i < NUM_LAND_TERRITORIES_TO_CLEAN_UP_PER_FRAME; i++)
            {
                LandTerritory lt = landTerritories[landTerritoryIndex];
                lt.Variables.TradeSet.ClearTemporaryVariables();
                foreach (TradePath tp in lt.Variables.TradeSet.ImportTradePaths)
                    tp.ClearTemporaryVariables();
                landTerritoryIndex++;
                if (landTerritoryIndex >= landTerritories.Count)
                {
                    Active = false;
                    return;
                }
            }
        }

        // Trade path calculation
        private void CalculateTradePaths(LandTerritory lt, GameManagerContainer gameManagers)
        {
            lt.Variables.TradeSet.Reset();

            if (lt.IsBlockedFromTrade(gameManagers.FactionManager))
                return;

            int remainingMovementPoints = lt.GetTraderMovementPoints(gameManagers);
            FindImportTradePathsRecursive(lt, lt, null, remainingMovementPoints, gameManagers.FactionManager);

            // Remove paths ending with factions that can't be traded with
            Faction owningFaction = lt.Variables.OwningFaction;
            lt.Variables.TradeSet.ImportTradePaths.RemoveAll(x => !owningFaction.CanTradeWithFaction(x.ExportLandTerritory.Variables.OwningFaction));

            lt.Variables.TradeSet.OnFinishedAddingImportTradePaths();
        }
        private void FindImportTradePathsRecursive(LandTerritory originLandTerritory, LandTerritory kernelLandTerritory, TradePath pathToKernel, int remainingMovementPoints, FactionManager fm)
        {
            foreach (TradeRoute tr in kernelLandTerritory.GetAllTradeRoutes())
            {
                if (tr.IsBlockaded)
                    continue;

                int cost = tr.GetMovementPointCost();
                if (cost > remainingMovementPoints)
                    continue;

                LandTerritory destination = tr.GetOppositeLandTerritory(kernelLandTerritory);
                if (destination == originLandTerritory)
                    continue;
                // Cannot trade through another faction if at war
                if (originLandTerritory.Variables.OwningFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(destination.Variables.OwningFaction))
                    continue;
                // Cannot trade with or through an occupied territory
                if (destination.IsBlockedFromTrade(fm))
                    continue;

                TradePath pathToDestination;
                if (pathToKernel == null)
                    pathToDestination = new TradePath(originLandTerritory, destination, new List<LandTerritory>(), cost);
                else
                    pathToDestination = pathToKernel.CreateExtended(destination, cost);

                if (originLandTerritory.Variables.TradeSet.TryAddImportTradePath(pathToDestination))
                    FindImportTradePathsRecursive(originLandTerritory, destination, pathToDestination, remainingMovementPoints - cost, fm);
            }
        }
        private void SetTradePathsToTradeRoutes(LandTerritory lt)
        {
            foreach (TradePath tp in lt.Variables.TradeSet.ImportTradePaths)
            {
                LandTerritory tradeRouteStartLT = tp.ExportLandTerritory;
                LandTerritory tradeRouteEndLT = (tp.ThroughportLandTerritories.Count > 0) ? tp.ThroughportLandTerritories[0] : tp.ImportLandTerritory;
                tradeRouteStartLT.SetTradePathToTradeRoute(tp, tradeRouteEndLT);

                for (int i = 0; i < tp.ThroughportLandTerritories.Count; i++)
                {
                    tradeRouteStartLT = tp.ThroughportLandTerritories[i];
                    tradeRouteEndLT = (i == tp.ThroughportLandTerritories.Count - 1) ? tp.ImportLandTerritory : tp.ThroughportLandTerritories[i + 1];
                    tradeRouteStartLT.SetTradePathToTradeRoute(tp, tradeRouteEndLT);
                }
            }
        }

        // Trade resource calculation
        private void CalculateSupplyAndDemand(eResourceTier tier, LandTerritory lt, ResourceManager rm)
        {
            foreach (KeyValuePair<string, Resource> kvp in rm.GetAllResources())
            {
                Resource r = kvp.Value;
                if (r.Tier != tier)
                    continue;

                float supply = lt.Variables.ResourceProduction.GetResourceQuantity(r);
                lt.Variables.TradeSet.SupplySet.Add(new SingleResourceSupply(r, supply));

                float demand = CalibrationManager.TradeCalibration.Data.baseResourceDemand;
                demand += CalibrationManager.TradeCalibration.Data.resourceDemandPer1kPopulation * ((float)lt.Variables.Population.Population / 1000f);
                // TODO - maybe improve demand calculations to represent better (e.g. cold climates demand more cold-helping resources)

                if (tier == eResourceTier.PRIMARY)
                {
                    // TODO - increase demand for buildings which produce secondary resources from the given primary resource
                }

                lt.Variables.TradeSet.DemandSet.Add(new SingleResourceDemand(r, demand));
            }
        }
        private void MakeTradeRequests(eResourceTier tier, LandTerritory lt)
        {
            TradeSet tradeSet = lt.Variables.TradeSet;

            for (int i = 0; i < tradeSet.SupplySet.Count; i++)
            {
                SingleResourceSupply supply = tradeSet.SupplySet[i];
                Resource r = supply.Resource;
                if (r.Tier != tier)
                    continue;
                SingleResourceDemand demand = tradeSet.DemandSet[i];
                float netDemand = demand.Quantity - supply.Quantity;

                List<TradePath> tradePathsToMakeRequestsThrough = new List<TradePath>();
                float totalSupplyOfExporters = 0f;
                float totalDemandOfExporters = 0f;

                foreach (TradePath tp in tradeSet.ImportTradePaths)
                {
                    LandTerritory exporter = tp.ExportLandTerritory;
                    float exporterSupply = exporter.Variables.TradeSet.SupplySet[i].Quantity;
                    float exporterDemand = exporter.Variables.TradeSet.DemandSet[i].Quantity;
                    float exporterNetDemand = exporterDemand - exporterSupply;

                    // Don't request trade from a land territory with no supply of its own
                    if (exporterSupply == 0f)
                        continue;

                    // Don't request trade from a land territory with a higher net demand
                    if (exporterNetDemand >= netDemand)
                        continue;

                    tradePathsToMakeRequestsThrough.Add(tp);
                    totalSupplyOfExporters += exporterSupply;
                    totalDemandOfExporters += exporterDemand;
                }

                foreach (TradePath tp in tradePathsToMakeRequestsThrough)
                {
                    LandTerritory exporter = tp.ExportLandTerritory;
                    float exporterSupply = exporter.Variables.TradeSet.SupplySet[i].Quantity;
                    float exporterDemand = exporter.Variables.TradeSet.DemandSet[i].Quantity;

                    float totalSupplyOfOtherExporters = totalSupplyOfExporters - exporterSupply;
                    float totalDemandOfOtherExporters = totalDemandOfExporters - exporterDemand;

                    TradeRequest request = new TradeRequest(tp, r, supply.Quantity, demand.Quantity, totalSupplyOfOtherExporters, totalDemandOfOtherExporters);
                    tp.TradeRequests.Add(request);
                }
            }
        }
        private void RemoveImportExportRequestDuplicates(TradeSet tradeSet, ResourceManager rm)
        {
            for (int i = 0; i < tradeSet.SupplySet.Count; i++)
            {
                SingleResourceSupply supply = tradeSet.SupplySet[i];
                SingleResourceDemand demand = tradeSet.DemandSet[i];
                Resource r = supply.Resource;

                List<TradeRequest> importTradeRequests = tradeSet.GetImportTradeRequests(r);
                List<TradeRequest> exportTradeRequests = tradeSet.GetExportTradeRequests(r);

                if (importTradeRequests.Count == 0 || exportTradeRequests.Count == 0)
                    continue;

                // Calculate net demands
                float importNetDemand = demand.Quantity - supply.Quantity;
                float exportAverageNetDemand = 0f;
                foreach (TradeRequest tr in exportTradeRequests)
                    exportAverageNetDemand += tr.Demand - tr.Supply;
                exportAverageNetDemand /= (float)exportTradeRequests.Count;

                // Decide whether to clear import or export requests
                if (importNetDemand >= exportAverageNetDemand)
                    tradeSet.RemoveExportTradeRequests(r);
                else
                    tradeSet.RemoveImportTradeRequests(r);
            }
        }
        private void SendResources(eResourceTier tier, LandTerritory lt, GameManagerContainer gameManagers)
        {
            TradeSet tradeSet = lt.Variables.TradeSet;

            for (int i = 0; i < tradeSet.SupplySet.Count; i++)
            {
                SingleResourceSupply supply = tradeSet.SupplySet[i];
                if (supply.Quantity == 0f)
                    continue;
                Resource r = supply.Resource;
                if (r.Tier != tier)
                    continue;
                SingleResourceDemand demand = tradeSet.DemandSet[i];

                List<TradeRequest> requests = tradeSet.GetExportTradeRequests(r);
                if (requests.Count == 0)
                    continue;

                // Calculate immediate network totals
                float importersTotalSupply = 0f;
                float importersTotalDemand = 0f;
                foreach (TradeRequest tr in requests)
                {
                    importersTotalSupply += tr.Supply;
                    importersTotalDemand += tr.Demand;
                }
                float immediateNetworkTotalSupply = importersTotalSupply + supply.Quantity;
                float immediateNetworkTotalDemand = importersTotalDemand + demand.Quantity;

                // Calculate amount to export
                float optimalResourceQuantity = demand.Quantity * immediateNetworkTotalSupply / immediateNetworkTotalDemand;
                float amountToExport = supply.Quantity - optimalResourceQuantity;

                if (amountToExport <= 0f)
                    continue;

                // Calculate export dominance factor for each importer sub-network
                List<float> exportDominanceFactors = new List<float>();
                foreach (TradeRequest tr in requests)
                {
                    exportDominanceFactors.Add(CalculateExportDominanceFactor(supply.Quantity, demand.Quantity, tr.Supply, tr.Demand, tr.TotalSupplyOfOtherTradeRequests, tr.TotalDemandOfOtherTradeRequests));
                }

                // Calculate weighted demands
                List<float> weightedDemands = new List<float>();
                float totalWeightedDemand = 0f;
                for (int req = 0; req < requests.Count; req++)
                {
                    float weightedDemand = requests[req].Demand * exportDominanceFactors[req];
                    weightedDemands.Add(weightedDemand);
                    totalWeightedDemand += weightedDemand;
                }

                if (totalWeightedDemand <= 0f)
                    continue;

                // Calculate weighted demand fractions
                List<float> weightedDemandFractions = new List<float>();
                foreach (float weightedDemand in weightedDemands)
                    weightedDemandFractions.Add(weightedDemand / totalWeightedDemand);

                // Send trade down paths
                for (int req = 0; req < requests.Count; req++)
                {
                    TradeRequest request = requests[req];
                    TradePath tradePath = request.ParentTradePath;
                    float exportQuantity = weightedDemandFractions[req] * amountToExport * (tradePath.GetTradeEfficiency(r.Type, gameManagers) / 100f);

                    if (exportQuantity <= 0f)
                        continue;

                    tradePath.TradedResources.Add(new SingleResourcePresence(r, exportQuantity));
                }
            }
        }
        private float CalculateExportDominanceFactor(float exporterSupply, float exporterDemand, float importerSupply, float importerDemand, float otherExportersSupply, float otherExportersDemand)
        {
            float totalSupply = exporterSupply + importerSupply + otherExportersSupply;
            float totalDemand = exporterDemand + importerDemand + otherExportersDemand;

            float exporterOptimalQuantity = exporterDemand * totalSupply / totalDemand;
            float otherExportersOptimalQuantity = otherExportersDemand * totalSupply / totalDemand;

            float optimalExportedQuantity = exporterSupply - exporterOptimalQuantity;
            float optimalOtherExportedQuantity = otherExportersSupply - otherExportersOptimalQuantity;

            if (optimalExportedQuantity <= 0f)
                return 0f;
            else if (optimalOtherExportedQuantity <= 0f)
                return 1f;
            else
            {
                float exportDominanceFactor = optimalExportedQuantity / (optimalExportedQuantity + optimalOtherExportedQuantity);
                return Mathf.Clamp01(exportDominanceFactor);
            }
        }
    }
}
