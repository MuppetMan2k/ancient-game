﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eCameraControlMode
    {
        FREE_MOVEMENT
    }

    class Camera
    {
        /* -------- Properties -------- */
        public Vector3 Position { get; private set; }
        public Vector3 LookTarget { get; private set; }
        public Vector3 UpDirection { get; private set; }
        public float ZoomFactor { get; private set; }
        public Matrix ViewMatrix { get; private set; }
        public Matrix ProjectionMatrix { get; private set; }
        public eCameraControlMode ControlMode { get; private set; }
        public float PerspectiveVerticalFieldAngle { get; private set; }
        public float DistanceFromTarget { get; private set; }

        /* -------- Private Fields -------- */
        private GraphicsComponent graphicsComponent;
        private float rotationAngle;
        private float riseAngle;
        private float zoomFactorRate;
        // Constants
        private const float NEAR_CLIPPING_PLANE_DISTANCE = 1f;
        private const float FAR_CLIPPING_PLANE_DISTANCE = 1000f;
        private const float BASE_MOVEMENT_SPEED = 20f;
        private const float MAX_ZOOM_FACTOR_RATE = 0.05f;
        private const float ZOOM_FACTOR_RATE_DECAY_SPEED = 0.5f;

        /* -------- Constructors -------- */
        public Camera(GraphicsComponent inGraphicsComponent, Vector3 inLookTarget, float inRotationAngle, float inZoomFactor, float inPerspectiveVerticalFieldAngle)
        {
            graphicsComponent = inGraphicsComponent;
            LookTarget = inLookTarget;
            rotationAngle = inRotationAngle;
            ZoomFactor = inZoomFactor;
            zoomFactorRate = 0f;
            ControlMode = eCameraControlMode.FREE_MOVEMENT;
            PerspectiveVerticalFieldAngle = inPerspectiveVerticalFieldAngle;
        }

        /* -------- Public Methods -------- */
        public void Update(InputComponent i, UIComponent ui, GameTimeWrapper gtw, ViewManager vm)
        {
            switch (ControlMode)
            {
                case eCameraControlMode.FREE_MOVEMENT:
                    UpdateFreeMovement(i, ui, gtw, vm);
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(ControlMode);
                    break;
            }

            UpdateZoomDependentVariables();
            UpdatePosition();
            UpdateMatrices();
        }

        public void SnapPosition(Vector2 msLookTarget)
        {
            LookTarget = SpaceUtilities.ConvertMSToGS(msLookTarget);
        }

        /* -------- Private Methods --------- */
        private void UpdateZoomDependentVariables()
        {
            float riseAngleMinZoom = CalibrationManager.CameraCalibration.Data.minZoomFactorForRiseAngle;
            float riseAngleMaxZoom = CalibrationManager.CameraCalibration.Data.maxZoomFactorForRiseAngle;
            float riseAngleLerp = Mathf.Clamp01(Mathf.GetLerpFactor(ZoomFactor, riseAngleMinZoom, riseAngleMaxZoom));
            float riseAngleAtMinZoom = CalibrationManager.CameraCalibration.Data.riseAngleAtMinZoomFactor;
            float riseAngleAtMaxZoom = CalibrationManager.CameraCalibration.Data.riseAngleAtMaxZoomFactor;
            riseAngle = Mathf.Lerp(riseAngleAtMinZoom, riseAngleAtMaxZoom, riseAngleLerp);

            float distanceMinZoom = CalibrationManager.CameraCalibration.Data.minZoomFactorForDistance;
            float distanceMaxZoom = CalibrationManager.CameraCalibration.Data.maxZoomFactorForDistance;
            float distanceLerp = Mathf.Clamp01(Mathf.GetLerpFactor(ZoomFactor, distanceMinZoom, distanceMaxZoom));
            float distanceAtMinZoom = CalibrationManager.CameraCalibration.Data.distanceAtMinZoomFactor;
            float distanceAtMaxZoom = CalibrationManager.CameraCalibration.Data.distanceAtMaxZoomFactor;
            DistanceFromTarget = Mathf.Lerp(distanceAtMinZoom, distanceAtMaxZoom, distanceLerp);
        }
        private void UpdatePosition()
        {
            Vector3 vecTargetToPosition = new Vector3(1f, 0f, 0f);
            vecTargetToPosition = VectorUtilities.RotateVectorAboutZ(vecTargetToPosition, rotationAngle);
            Vector3 leftVec = VectorUtilities.RotateVectorAboutZ(vecTargetToPosition, 90f);
            vecTargetToPosition = VectorUtilities.RotateVectorAboutAxis(vecTargetToPosition, leftVec, -riseAngle);
            UpDirection = VectorUtilities.RotateVectorAboutAxis(vecTargetToPosition, leftVec, -90f);
            UpDirection.Normalize();
            vecTargetToPosition *= DistanceFromTarget;

            Position = LookTarget + vecTargetToPosition;
        }

        private void UpdateMatrices()
        {
            ViewMatrix = Matrix.CreateLookAt(Position, LookTarget, UpDirection);
            ProjectionMatrix = Matrix.CreatePerspectiveFieldOfView(Mathf.ToRadians(PerspectiveVerticalFieldAngle), graphicsComponent.AspectRatio, NEAR_CLIPPING_PLANE_DISTANCE, FAR_CLIPPING_PLANE_DISTANCE);
        }

        private void UpdateFreeMovement(InputComponent i, UIComponent ui, GameTimeWrapper gtw, ViewManager vm)
        {
            float movementSpeed = SettingsManager.Settings.CameraMovementSpeed * DistanceFromTarget;
            float rotationSpeed = SettingsManager.Settings.CameraRotationSpeed;
            float zoomSpeed = SettingsManager.Settings.CameraZoomSpeed;

            Vector2 forwardDirection = VectorUtilities.CreateVector(rotationAngle + 180f, 1f);
            Vector2 rightDirection = VectorUtilities.CreateVector(rotationAngle + 90f, 1f);

            // Movement
            if (KeyControlIsActive())
            {
                if (i.ControlIsDown(eControls.CAMERA_FORWARD))
                {
                    Vector2 movement = forwardDirection * movementSpeed * gtw.ElapsedSeconds;
                    MoveLookTarget(movement);
                }
                if (i.ControlIsDown(eControls.CAMERA_BACKWARD))
                {
                    Vector2 movement = -forwardDirection * movementSpeed * gtw.ElapsedSeconds;
                    MoveLookTarget(movement);
                }
                if (i.ControlIsDown(eControls.CAMERA_RIGHT))
                {
                    Vector2 movement = rightDirection * movementSpeed * gtw.ElapsedSeconds;
                    MoveLookTarget(movement);
                }
                if (i.ControlIsDown(eControls.CAMERA_LEFT))
                {
                    Vector2 movement = -rightDirection * movementSpeed * gtw.ElapsedSeconds;
                    MoveLookTarget(movement);
                }

                if (i.ControlIsDown(eControls.CAMERA_SWING_RIGHT))
                    rotationAngle += rotationSpeed * gtw.ElapsedSeconds;
                if (i.ControlIsDown(eControls.CAMERA_SWING_LEFT))
                    rotationAngle -= rotationSpeed * gtw.ElapsedSeconds;
            }

            // Zoom
            if ((KeyControlIsActive() && i.ControlIsDown(eControls.CAMERA_ZOOM_IN)) || (MouseWheelControlIsActive(ui) && i.GetMouseScrollChange() > 0))
            {
                float zoomRateChange = zoomSpeed * gtw.ElapsedSeconds;
                zoomFactorRate = Mathf.ClampUpper(zoomFactorRate + zoomRateChange, MAX_ZOOM_FACTOR_RATE);
            }
            if ((KeyControlIsActive() && i.ControlIsDown(eControls.CAMERA_ZOOM_OUT)) || (MouseWheelControlIsActive(ui) && i.GetMouseScrollChange() < 0))
            {
                float zoomRateChange = -zoomSpeed * gtw.ElapsedSeconds;
                zoomFactorRate = Mathf.ClampLower(zoomFactorRate + zoomRateChange, -MAX_ZOOM_FACTOR_RATE);
            }
            ZoomFactor = Mathf.Clamp01(ZoomFactor + zoomFactorRate);
            vm.ViewMode.OnZoomFactorChanged(ZoomFactor);
            if (zoomFactorRate > 0f)
            {
                float zoomFactorRateChange = -ZOOM_FACTOR_RATE_DECAY_SPEED * gtw.ElapsedSeconds;
                zoomFactorRate = Mathf.ClampLower(zoomFactorRate + zoomFactorRateChange, 0f);
            }
            if (zoomFactorRate < 0f)
            {
                float zoomFactorRateChange = ZOOM_FACTOR_RATE_DECAY_SPEED * gtw.ElapsedSeconds;
                zoomFactorRate = Mathf.ClampUpper(zoomFactorRate + zoomFactorRateChange, 0f);
            }
        }

        private bool KeyControlIsActive()
        {
            // TODO - return false if menu is open or using keyboard for something else

            return true;
        }
        private bool MouseWheelControlIsActive(UIComponent ui)
        {
            if (ui.HasDetectedMouseOverUI())
                return false;

            // TODO - return false if menu is open

            return true;
        }

        private void MoveLookTarget(Vector2 movement)
        {
            Vector2 msLookTarget = SpaceUtilities.ConvertGSToMS(LookTarget);
            msLookTarget += movement;
            LookTarget = SpaceUtilities.ConvertMSToGS(msLookTarget);
        }
    }
}
