﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class CameraManager
    {
        /* -------- Properties -------- */
        public Camera Camera { get; private set; }

        /* -------- Constructors -------- */
        public CameraManager(GraphicsComponent inGraphicsComponent)
        {
            Camera = new Camera(inGraphicsComponent, new Vector3(), 270f, 0f, CalibrationManager.CameraCalibration.Data.verticalFieldOfViewAngle);
        }

        /* -------- Public Methods -------- */
        public void Update(InputComponent i, UIComponent ui, GameTimeWrapper gtw, ViewManager vm)
        {
            Camera.Update(i, ui, gtw, vm);
        }
    }
}
