﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class IncomeEfficiencySet
    {
        /* -------- Properties -------- */
        public List<IncomeEfficiency> Efficiencies { get; private set; }
        public float TotalEfficiencyPercentage { get; private set; }

        /* -------- Constructors -------- */
        public IncomeEfficiencySet()
        {
            Efficiencies = new List<IncomeEfficiency>();
        }

        /* -------- Public Methods -------- */
        public void RecalculateEfficiencies(LandTerritory lt, GameManagerContainer gameManagers)
        {
            Efficiencies.Clear();

            foreach (eIncomeEfficiencySource ies in GeneralUtilities.GetEnumValues<eIncomeEfficiencySource>())
            {
                float value = 0f;
                ModifierTypeSet typeSet = ModifierTypeSet.Create_IncomeEfficiency(ies);
                value = lt.GetModifierSet(gameManagers).Modify(value, typeSet);

                if (value > 0f)
                    Efficiencies.Add(new IncomeEfficiency(ies, value));
            }

            TotalEfficiencyPercentage = 100f;
            foreach (IncomeEfficiency sie in Efficiencies)
                if (IncomeEfficiency.GetBias(sie.Source) == eStatConstituentBias.POSITIVE)
                    TotalEfficiencyPercentage += sie.Value;
                else
                    TotalEfficiencyPercentage -= sie.Value;

            TotalEfficiencyPercentage = Mathf.Clamp(TotalEfficiencyPercentage, 0f, 100f);
        }

        /* -------- Static Methods -------- */
        public static IncomeEfficiencySet CreateFromData(IncomeEfficiencySetData data)
        {
            IncomeEfficiencySet ies = new IncomeEfficiencySet();

            foreach (SingleIncomeEfficiencyData sied in data.singleIncomeEfficiencyDatas)
                ies.Efficiencies.Add(IncomeEfficiency.CreateFromData(sied));

            ies.TotalEfficiencyPercentage = 100f;
            foreach (IncomeEfficiency sie in ies.Efficiencies)
                ies.TotalEfficiencyPercentage += sie.Value;
            ies.TotalEfficiencyPercentage = Mathf.Clamp(ies.TotalEfficiencyPercentage, 0f, 100f);

            return ies;
        }
    }
}
