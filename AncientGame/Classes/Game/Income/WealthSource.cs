﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eWealthSourceType
    {
        PRIVATE_WEALTH,
        // Resources
        RESOURCE_AGRICULTURE_PRODUCTION,
        RESOURCE_AGRICULTURE_THROUGHPORT,
        RESOURCE_AGRICULTURE_IMPORT,
        RESOURCE_FOOD_PRODUCTION,
        RESOURCE_FOOD_THROUGHPORT,
        RESOURCE_FOOD_IMPORT,
        RESOURCE_LIVESTOCK_PRODUCTION,
        RESOURCE_LIVESTOCK_THROUGHPORT,
        RESOURCE_LIVESTOCK_IMPORT,
        RESOURCE_MINERAL_PRODUCTION,
        RESOURCE_MINERAL_THROUGHPORT,
        RESOURCE_MINERAL_IMPORT,
        RESOURCE_ANIMAL_PRODUCTION,
        RESOURCE_ANIMAL_THROUGHPORT,
        RESOURCE_ANIMAL_IMPORT,
        RESOURCE_LUXURY_PRODUCTION,
        RESOURCE_LUXURY_THROUGHPORT,
        RESOURCE_LUXURY_IMPORT,
        RESOURCE_WEAPONRY_PRODUCTION,
        RESOURCE_WEAPONRY_THROUGHPORT,
        RESOURCE_WEAPONRY_IMPORT,
        RESOURCE_GENERIC_PRODUCTION,
        RESOURCE_GENERIC_THROUGHPORT,
        RESOURCE_GENERIC_IMPORT
    }

    enum eWealthSourceTaxationType
    {
        PRIVATE_WEALTH,
        // Resources
        RESOURCE_AGRICULTURE_PRODUCTION,
        RESOURCE_AGRICULTURE_THROUGHPORT,
        RESOURCE_AGRICULTURE_IMPORT,
        RESOURCE_FOOD_PRODUCTION,
        RESOURCE_FOOD_THROUGHPORT,
        RESOURCE_FOOD_IMPORT,
        RESOURCE_LIVESTOCK_PRODUCTION,
        RESOURCE_LIVESTOCK_THROUGHPORT,
        RESOURCE_LIVESTOCK_IMPORT,
        RESOURCE_MINERAL_PRODUCTION,
        RESOURCE_MINERAL_THROUGHPORT,
        RESOURCE_MINERAL_IMPORT,
        RESOURCE_ANIMAL_PRODUCTION,
        RESOURCE_ANIMAL_THROUGHPORT,
        RESOURCE_ANIMAL_IMPORT,
        RESOURCE_LUXURY_PRODUCTION,
        RESOURCE_LUXURY_THROUGHPORT,
        RESOURCE_LUXURY_IMPORT,
        RESOURCE_WEAPONRY_PRODUCTION,
        RESOURCE_WEAPONRY_THROUGHPORT,
        RESOURCE_WEAPONRY_IMPORT,
        RESOURCE_GENERIC_PRODUCTION,
        RESOURCE_GENERIC_THROUGHPORT,
        RESOURCE_GENERIC_IMPORT,
        // Resource blanket taxes
        RESOURCE_ALL_PRODUCTION,
        RESOURCE_ALL_THROUGHPORT,
        RESOURCE_ALL_IMPORT
    }

    class WealthSource
    {
        /* -------- Properties -------- */
        public eWealthSourceType Type { get; private set; }
        public float Value { get; private set; }
        public float TaxedValue { get; private set; }
        public float UntaxedValue { get { return Value - TaxedValue; } }

        /* -------- Constructors -------- */
        public WealthSource(eWealthSourceType inType, float inValue, float inTaxedValue)
        {
            Type = inType;
            Value = inValue;
            TaxedValue = inTaxedValue;
        }

        /* -------- Public Methods -------- */
        public string GetIconTextureID()
        {
            switch (Type)
            {
                case eWealthSourceType.PRIVATE_WEALTH:
                    return "wealth-icon";
                case eWealthSourceType.RESOURCE_AGRICULTURE_PRODUCTION:
                    return "agriculture-production-icon";
                case eWealthSourceType.RESOURCE_AGRICULTURE_THROUGHPORT:
                    return "agriculture-throughport-icon";
                case eWealthSourceType.RESOURCE_AGRICULTURE_IMPORT:
                    return "agriculture-import-icon";
                case eWealthSourceType.RESOURCE_FOOD_PRODUCTION:
                    return "food-production-icon";
                case eWealthSourceType.RESOURCE_FOOD_THROUGHPORT:
                    return "food-throughport-icon";
                case eWealthSourceType.RESOURCE_FOOD_IMPORT:
                    return "food-import-icon";
                case eWealthSourceType.RESOURCE_LIVESTOCK_PRODUCTION:
                    return "livestock-production-icon";
                case eWealthSourceType.RESOURCE_LIVESTOCK_THROUGHPORT:
                    return "livestock-throughport-icon";
                case eWealthSourceType.RESOURCE_LIVESTOCK_IMPORT:
                    return "livestock-import-icon";
                case eWealthSourceType.RESOURCE_MINERAL_PRODUCTION:
                    return "mineral-production-icon";
                case eWealthSourceType.RESOURCE_MINERAL_THROUGHPORT:
                    return "mineral-throughport-icon";
                case eWealthSourceType.RESOURCE_MINERAL_IMPORT:
                    return "mineral-import-icon";
                case eWealthSourceType.RESOURCE_ANIMAL_PRODUCTION:
                    return "animal-production-icon";
                case eWealthSourceType.RESOURCE_ANIMAL_THROUGHPORT:
                    return "animal-throughport-icon";
                case eWealthSourceType.RESOURCE_ANIMAL_IMPORT:
                    return "animal-import-icon";
                case eWealthSourceType.RESOURCE_LUXURY_PRODUCTION:
                    return "luxury-production-icon";
                case eWealthSourceType.RESOURCE_LUXURY_THROUGHPORT:
                    return "luxury-throughport-icon";
                case eWealthSourceType.RESOURCE_LUXURY_IMPORT:
                    return "luxury-import-icon";
                case eWealthSourceType.RESOURCE_WEAPONRY_PRODUCTION:
                    return "weaponry-production-icon";
                case eWealthSourceType.RESOURCE_WEAPONRY_THROUGHPORT:
                    return "weaponry-throughport-icon";
                case eWealthSourceType.RESOURCE_WEAPONRY_IMPORT:
                    return "weaponry-import-icon";
                case eWealthSourceType.RESOURCE_GENERIC_PRODUCTION:
                    return "generic-production-icon";
                case eWealthSourceType.RESOURCE_GENERIC_THROUGHPORT:
                    return "generic-throughport-icon";
                case eWealthSourceType.RESOURCE_GENERIC_IMPORT:
                    return "generic-import-icon";
                default:
                    Debug.LogUnrecognizedValueWarning(Type);
                    return "";
            }
        }

        public void SetValue(float inValue, float inTaxedValue)
        {
            Value = inValue;
            TaxedValue = inTaxedValue;
        }

        /* -------- Static Methods -------- */
        public static WealthSource CreateFromData(WealthSourceData data)
        {
            eWealthSourceType type = DataUtilities.ParseWealthSourceType(data.wealthSourceType);
            float value = data.value;
            float taxedValue = data.taxedValue;

            return new WealthSource(type, value, taxedValue);
        }

        public static eWealthSourceType GetResourceWealthSourceTypeForResourceType_Production(eResourceType type)
        {
            switch (type)
            {
                case eResourceType.AGRICULTURE:
                    return eWealthSourceType.RESOURCE_AGRICULTURE_PRODUCTION;
                case eResourceType.FOOD:
                    return eWealthSourceType.RESOURCE_FOOD_PRODUCTION;
                case eResourceType.LIVESTOCK:
                    return eWealthSourceType.RESOURCE_LIVESTOCK_PRODUCTION;
                case eResourceType.MINERAL:
                    return eWealthSourceType.RESOURCE_MINERAL_PRODUCTION;
                case eResourceType.ANIMAL:
                    return eWealthSourceType.RESOURCE_ANIMAL_PRODUCTION;
                case eResourceType.LUXURY:
                    return eWealthSourceType.RESOURCE_LUXURY_PRODUCTION;
                case eResourceType.WEAPONRY:
                    return eWealthSourceType.RESOURCE_WEAPONRY_PRODUCTION;
                case eResourceType.GENERIC:
                    return eWealthSourceType.RESOURCE_GENERIC_PRODUCTION;
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return eWealthSourceType.RESOURCE_GENERIC_PRODUCTION;
            }
        }
        public static eWealthSourceType GetResourceWealthSourceTypeForResourceType_Import(eResourceType type)
        {
            switch (type)
            {
                case eResourceType.AGRICULTURE:
                    return eWealthSourceType.RESOURCE_AGRICULTURE_IMPORT;
                case eResourceType.FOOD:
                    return eWealthSourceType.RESOURCE_FOOD_IMPORT;
                case eResourceType.LIVESTOCK:
                    return eWealthSourceType.RESOURCE_LIVESTOCK_IMPORT;
                case eResourceType.MINERAL:
                    return eWealthSourceType.RESOURCE_MINERAL_IMPORT;
                case eResourceType.ANIMAL:
                    return eWealthSourceType.RESOURCE_ANIMAL_IMPORT;
                case eResourceType.LUXURY:
                    return eWealthSourceType.RESOURCE_LUXURY_IMPORT;
                case eResourceType.WEAPONRY:
                    return eWealthSourceType.RESOURCE_WEAPONRY_IMPORT;
                case eResourceType.GENERIC:
                    return eWealthSourceType.RESOURCE_GENERIC_IMPORT;
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return eWealthSourceType.RESOURCE_GENERIC_IMPORT;
            }
        }
        public static eWealthSourceType GetResourceWealthSourceTypeForResourceType_Throughport(eResourceType type)
        {
            switch (type)
            {
                case eResourceType.AGRICULTURE:
                    return eWealthSourceType.RESOURCE_AGRICULTURE_THROUGHPORT;
                case eResourceType.FOOD:
                    return eWealthSourceType.RESOURCE_FOOD_THROUGHPORT;
                case eResourceType.LIVESTOCK:
                    return eWealthSourceType.RESOURCE_LIVESTOCK_THROUGHPORT;
                case eResourceType.MINERAL:
                    return eWealthSourceType.RESOURCE_MINERAL_THROUGHPORT;
                case eResourceType.ANIMAL:
                    return eWealthSourceType.RESOURCE_ANIMAL_THROUGHPORT;
                case eResourceType.LUXURY:
                    return eWealthSourceType.RESOURCE_LUXURY_THROUGHPORT;
                case eResourceType.WEAPONRY:
                    return eWealthSourceType.RESOURCE_WEAPONRY_THROUGHPORT;
                case eResourceType.GENERIC:
                    return eWealthSourceType.RESOURCE_GENERIC_THROUGHPORT;
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return eWealthSourceType.RESOURCE_GENERIC_THROUGHPORT;
            }
        }

        public static eWealthSourceTaxationType GetResourceWealthSourceTaxationTypeForResourceType_Production(eResourceType type)
        {
            switch (type)
            {
                case eResourceType.AGRICULTURE:
                    return eWealthSourceTaxationType.RESOURCE_AGRICULTURE_PRODUCTION;
                case eResourceType.FOOD:
                    return eWealthSourceTaxationType.RESOURCE_FOOD_PRODUCTION;
                case eResourceType.LIVESTOCK:
                    return eWealthSourceTaxationType.RESOURCE_LIVESTOCK_PRODUCTION;
                case eResourceType.MINERAL:
                    return eWealthSourceTaxationType.RESOURCE_MINERAL_PRODUCTION;
                case eResourceType.ANIMAL:
                    return eWealthSourceTaxationType.RESOURCE_ANIMAL_PRODUCTION;
                case eResourceType.LUXURY:
                    return eWealthSourceTaxationType.RESOURCE_LUXURY_PRODUCTION;
                case eResourceType.WEAPONRY:
                    return eWealthSourceTaxationType.RESOURCE_WEAPONRY_PRODUCTION;
                case eResourceType.GENERIC:
                    return eWealthSourceTaxationType.RESOURCE_GENERIC_PRODUCTION;
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return eWealthSourceTaxationType.RESOURCE_GENERIC_PRODUCTION;
            }
        }
        public static eWealthSourceTaxationType GetResourceWealthSourceTaxationTypeForResourceType_Import(eResourceType type)
        {
            switch (type)
            {
                case eResourceType.AGRICULTURE:
                    return eWealthSourceTaxationType.RESOURCE_AGRICULTURE_IMPORT;
                case eResourceType.FOOD:
                    return eWealthSourceTaxationType.RESOURCE_FOOD_IMPORT;
                case eResourceType.LIVESTOCK:
                    return eWealthSourceTaxationType.RESOURCE_LIVESTOCK_IMPORT;
                case eResourceType.MINERAL:
                    return eWealthSourceTaxationType.RESOURCE_MINERAL_IMPORT;
                case eResourceType.ANIMAL:
                    return eWealthSourceTaxationType.RESOURCE_ANIMAL_IMPORT;
                case eResourceType.LUXURY:
                    return eWealthSourceTaxationType.RESOURCE_LUXURY_IMPORT;
                case eResourceType.WEAPONRY:
                    return eWealthSourceTaxationType.RESOURCE_WEAPONRY_IMPORT;
                case eResourceType.GENERIC:
                    return eWealthSourceTaxationType.RESOURCE_GENERIC_IMPORT;
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return eWealthSourceTaxationType.RESOURCE_GENERIC_IMPORT;
            }
        }
        public static eWealthSourceTaxationType GetResourceWealthSourceTaxationTypeForResourceType_Throughport(eResourceType type)
        {
            switch (type)
            {
                case eResourceType.AGRICULTURE:
                    return eWealthSourceTaxationType.RESOURCE_AGRICULTURE_THROUGHPORT;
                case eResourceType.FOOD:
                    return eWealthSourceTaxationType.RESOURCE_FOOD_THROUGHPORT;
                case eResourceType.LIVESTOCK:
                    return eWealthSourceTaxationType.RESOURCE_LIVESTOCK_THROUGHPORT;
                case eResourceType.MINERAL:
                    return eWealthSourceTaxationType.RESOURCE_MINERAL_THROUGHPORT;
                case eResourceType.ANIMAL:
                    return eWealthSourceTaxationType.RESOURCE_ANIMAL_THROUGHPORT;
                case eResourceType.LUXURY:
                    return eWealthSourceTaxationType.RESOURCE_LUXURY_THROUGHPORT;
                case eResourceType.WEAPONRY:
                    return eWealthSourceTaxationType.RESOURCE_WEAPONRY_THROUGHPORT;
                case eResourceType.GENERIC:
                    return eWealthSourceTaxationType.RESOURCE_GENERIC_THROUGHPORT;
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return eWealthSourceTaxationType.RESOURCE_GENERIC_THROUGHPORT;
            }
        }

        public static string GetWealthSourceName(eWealthSourceType type)
        {
            switch (type)
            {
                case eWealthSourceType.PRIVATE_WEALTH:
                    return Localization.Localize("text_wealth-source_private-wealth");
                case eWealthSourceType.RESOURCE_AGRICULTURE_PRODUCTION:
                    return Localization.Localize("text_wealth-source_agriculture-production");
                case eWealthSourceType.RESOURCE_AGRICULTURE_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_agriculture-throughport");
                case eWealthSourceType.RESOURCE_AGRICULTURE_IMPORT:
                    return Localization.Localize("text_wealth-source_agriculture-import");
                case eWealthSourceType.RESOURCE_FOOD_PRODUCTION:
                    return Localization.Localize("text_wealth-source_food-production");
                case eWealthSourceType.RESOURCE_FOOD_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_food-throughport");
                case eWealthSourceType.RESOURCE_FOOD_IMPORT:
                    return Localization.Localize("text_wealth-source_food-import");
                case eWealthSourceType.RESOURCE_LIVESTOCK_PRODUCTION:
                    return Localization.Localize("text_wealth-source_livestock-production");
                case eWealthSourceType.RESOURCE_LIVESTOCK_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_livestock-throughport");
                case eWealthSourceType.RESOURCE_LIVESTOCK_IMPORT:
                    return Localization.Localize("text_wealth-source_livestock-import");
                case eWealthSourceType.RESOURCE_MINERAL_PRODUCTION:
                    return Localization.Localize("text_wealth-source_mineral-production");
                case eWealthSourceType.RESOURCE_MINERAL_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_mineral-throughport");
                case eWealthSourceType.RESOURCE_MINERAL_IMPORT:
                    return Localization.Localize("text_wealth-source_mineral-import");
                case eWealthSourceType.RESOURCE_ANIMAL_PRODUCTION:
                    return Localization.Localize("text_wealth-source_animal-production");
                case eWealthSourceType.RESOURCE_ANIMAL_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_animal-throughport");
                case eWealthSourceType.RESOURCE_ANIMAL_IMPORT:
                    return Localization.Localize("text_wealth-source_animal-import");
                case eWealthSourceType.RESOURCE_LUXURY_PRODUCTION:
                    return Localization.Localize("text_wealth-source_luxury-production");
                case eWealthSourceType.RESOURCE_LUXURY_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_luxury-throughport");
                case eWealthSourceType.RESOURCE_LUXURY_IMPORT:
                    return Localization.Localize("text_wealth-source_luxury-import");
                case eWealthSourceType.RESOURCE_WEAPONRY_PRODUCTION:
                    return Localization.Localize("text_wealth-source_weaponry-production");
                case eWealthSourceType.RESOURCE_WEAPONRY_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_weaponry-throughport");
                case eWealthSourceType.RESOURCE_WEAPONRY_IMPORT:
                    return Localization.Localize("text_wealth-source_weaponry-import");
                case eWealthSourceType.RESOURCE_GENERIC_PRODUCTION:
                    return Localization.Localize("text_wealth-source_generic-production");
                case eWealthSourceType.RESOURCE_GENERIC_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_generic-throughport");
                case eWealthSourceType.RESOURCE_GENERIC_IMPORT:
                    return Localization.Localize("text_wealth-source_generic-import");
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return "";
            }
        }
        public static string GetWealthSourceTaxationName(eWealthSourceTaxationType type)
        {
            switch (type)
            {
                case eWealthSourceTaxationType.PRIVATE_WEALTH:
                    return Localization.Localize("text_wealth-source_private-wealth");
                case eWealthSourceTaxationType.RESOURCE_AGRICULTURE_PRODUCTION:
                    return Localization.Localize("text_wealth-source_agriculture-production");
                case eWealthSourceTaxationType.RESOURCE_AGRICULTURE_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_agriculture-throughport");
                case eWealthSourceTaxationType.RESOURCE_AGRICULTURE_IMPORT:
                    return Localization.Localize("text_wealth-source_agriculture-import");
                case eWealthSourceTaxationType.RESOURCE_FOOD_PRODUCTION:
                    return Localization.Localize("text_wealth-source_food-production");
                case eWealthSourceTaxationType.RESOURCE_FOOD_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_food-throughport");
                case eWealthSourceTaxationType.RESOURCE_FOOD_IMPORT:
                    return Localization.Localize("text_wealth-source_food-import");
                case eWealthSourceTaxationType.RESOURCE_LIVESTOCK_PRODUCTION:
                    return Localization.Localize("text_wealth-source_livestock-production");
                case eWealthSourceTaxationType.RESOURCE_LIVESTOCK_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_livestock-throughport");
                case eWealthSourceTaxationType.RESOURCE_LIVESTOCK_IMPORT:
                    return Localization.Localize("text_wealth-source_livestock-import");
                case eWealthSourceTaxationType.RESOURCE_MINERAL_PRODUCTION:
                    return Localization.Localize("text_wealth-source_mineral-production");
                case eWealthSourceTaxationType.RESOURCE_MINERAL_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_mineral-throughport");
                case eWealthSourceTaxationType.RESOURCE_MINERAL_IMPORT:
                    return Localization.Localize("text_wealth-source_mineral-import");
                case eWealthSourceTaxationType.RESOURCE_ANIMAL_PRODUCTION:
                    return Localization.Localize("text_wealth-source_animal-production");
                case eWealthSourceTaxationType.RESOURCE_ANIMAL_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_animal-throughport");
                case eWealthSourceTaxationType.RESOURCE_ANIMAL_IMPORT:
                    return Localization.Localize("text_wealth-source_animal-import");
                case eWealthSourceTaxationType.RESOURCE_LUXURY_PRODUCTION:
                    return Localization.Localize("text_wealth-source_luxury-production");
                case eWealthSourceTaxationType.RESOURCE_LUXURY_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_luxury-throughport");
                case eWealthSourceTaxationType.RESOURCE_LUXURY_IMPORT:
                    return Localization.Localize("text_wealth-source_luxury-import");
                case eWealthSourceTaxationType.RESOURCE_WEAPONRY_PRODUCTION:
                    return Localization.Localize("text_wealth-source_weaponry-production");
                case eWealthSourceTaxationType.RESOURCE_WEAPONRY_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_weaponry-throughport");
                case eWealthSourceTaxationType.RESOURCE_WEAPONRY_IMPORT:
                    return Localization.Localize("text_wealth-source_weaponry-import");
                case eWealthSourceTaxationType.RESOURCE_GENERIC_PRODUCTION:
                    return Localization.Localize("text_wealth-source_generic-production");
                case eWealthSourceTaxationType.RESOURCE_GENERIC_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_generic-throughport");
                case eWealthSourceTaxationType.RESOURCE_GENERIC_IMPORT:
                    return Localization.Localize("text_wealth-source_generic-import");
                case eWealthSourceTaxationType.RESOURCE_ALL_PRODUCTION:
                    return Localization.Localize("text_wealth-source_all-productions");
                case eWealthSourceTaxationType.RESOURCE_ALL_THROUGHPORT:
                    return Localization.Localize("text_wealth-source_all-throughports");
                case eWealthSourceTaxationType.RESOURCE_ALL_IMPORT:
                    return Localization.Localize("text_wealth-source_all-imports");
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return "";
            }
        }
    }
}
