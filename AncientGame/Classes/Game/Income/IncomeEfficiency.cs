﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eIncomeEfficiencySource
    {
        CORRUPTION_NEG
    }

    class IncomeEfficiency
    {
        /* -------- Properties -------- */
        public eIncomeEfficiencySource Source { get; private set; }
        public float Value { get; private set; }

        /* -------- Constructors -------- */
        public IncomeEfficiency(eIncomeEfficiencySource inSource, float inValue)
        {
            Source = inSource;
            Value = inValue;
        }

        /* -------- Static Methods -------- */
        public static IncomeEfficiency CreateFromData(SingleIncomeEfficiencyData data)
        {
            eIncomeEfficiencySource source = DataUtilities.ParseIncomeEfficiencySource(data.incomeEfficiencySource);
            float value = data.value;

            return new IncomeEfficiency(source, value);
        }

        public static string GetSourceName(eIncomeEfficiencySource source)
        {
            switch (source)
            {
                case eIncomeEfficiencySource.CORRUPTION_NEG:
                    return Localization.Localize("text_income-efficiency_corruption-neg");
                default:
                    Debug.LogUnrecognizedValueWarning(source);
                    return "";
            }
        }

        /* -------- Static Methods -------- */
        public static eUIAttitude GetUIAttitude(eIncomeEfficiencySource source)
        {
            string sourceAsString = source.ToString();

            if (sourceAsString.EndsWith("_POS"))
                return eUIAttitude.POSITIVE;

            if (sourceAsString.EndsWith("_NEG"))
                return eUIAttitude.NEGATIVE;

            return eUIAttitude.NEUTRAL;
        }
        public static eStatConstituentBias GetBias(eIncomeEfficiencySource source)
        {
            if (GetUIAttitude(source) == eUIAttitude.NEGATIVE)
                return eStatConstituentBias.NEGATIVE;
            else
                return eStatConstituentBias.POSITIVE;
        }
    }
}
