﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class IncomeSet
    {
        /* -------- Properties -------- */
        public List<WealthSource> WealthSources { get; private set; }
        public IncomeEfficiencySet EfficiencySet { get; private set; }
        public float TotalTaxedWealth { get; private set; }
        public int TotalIncome { get; private set; }

        /* -------- Constructors -------- */
        public IncomeSet()
        {
            WealthSources = new List<WealthSource>();
        }

        /* -------- Public Methods -------- */
        public void RecalculateWealthSources(LandTerritory lt, GameManagerContainer gameManagers)
        {
            WealthSources.Clear();

            // Private wealth
            float privateWealth = lt.Variables.PrivateWealth.PrivateWealth;
            if (privateWealth > 0f)
            {
                float taxPercentage = 0f;
                ModifierTypeSet modifierType = ModifierTypeSet.Create_WealthSourceTaxation(eWealthSourceTaxationType.PRIVATE_WEALTH);
                taxPercentage = Mathf.Clamp(lt.GetModifierSet(gameManagers).Modify(taxPercentage, modifierType), 0f, 100f);
                float taxedPrivateWealth = privateWealth * taxPercentage / 100f;
                AddWealthSource(new WealthSource(eWealthSourceType.PRIVATE_WEALTH, privateWealth, taxedPrivateWealth));
            }

            // Resource production
            foreach (SingleResourcePresence srp in lt.Variables.ResourceProduction.ResourceProductions)
            {
                float taxPercentage = 0f;
                taxPercentage = Mathf.Clamp(lt.GetModifierSet(gameManagers).Modify_ResourceProductionWealthSourceTaxation(taxPercentage, srp.Resource.Type), 0f, 100f);
                float resourceValue = srp.Quantity * srp.Resource.Value;
                float taxedResource = resourceValue * taxPercentage / 100f;
                eWealthSourceType wealthSourceType = WealthSource.GetResourceWealthSourceTypeForResourceType_Production(srp.Resource.Type);
                AddWealthSource(new WealthSource(wealthSourceType, resourceValue, taxedResource));
            }

            // Resource throughports
            foreach (TradePath tp in lt.Variables.TradeSet.ThroughportTradePaths)
            {
                foreach (SingleResourcePresence srp in tp.TradedResources)
                {
                    float taxPercentage = 0f;
                    taxPercentage = Mathf.Clamp(lt.GetModifierSet(gameManagers).Modify_ResourceThroughportWealthSourceTaxation(taxPercentage, srp.Resource.Type), 0f, 100f);
                    float resourceValue = srp.Quantity * srp.Resource.Value;
                    float taxedResource = resourceValue * taxPercentage / 100f;
                    eWealthSourceType wealthSourceType = WealthSource.GetResourceWealthSourceTypeForResourceType_Throughport(srp.Resource.Type);
                    AddWealthSource(new WealthSource(wealthSourceType, resourceValue, taxedResource));
                }
            }

            // Resource imports
            foreach (TradePath tp in lt.Variables.TradeSet.ImportTradePaths)
            {
                foreach (SingleResourcePresence srp in tp.TradedResources)
                {
                    float taxPercentage = 0f;
                    taxPercentage = Mathf.Clamp(lt.GetModifierSet(gameManagers).Modify_ResourceImportWealthSourceTaxation(taxPercentage, srp.Resource.Type), 0f, 100f);
                    float resourceValue = srp.Quantity * srp.Resource.Value;
                    float taxedResource = resourceValue * taxPercentage / 100f;
                    eWealthSourceType wealthSourceType = WealthSource.GetResourceWealthSourceTypeForResourceType_Import(srp.Resource.Type);
                    AddWealthSource(new WealthSource(wealthSourceType, resourceValue, taxedResource));
                }
            }

            // TODO - add further wealth sources

            TotalTaxedWealth = 0f;
            foreach (WealthSource ws in WealthSources)
                TotalTaxedWealth += ws.TaxedValue;
        }
        public void RecalculateEfficiencySet(LandTerritory lt, GameManagerContainer gameManagers)
        {
            EfficiencySet.RecalculateEfficiencies(lt, gameManagers);

            TotalIncome = Mathf.Round(TotalTaxedWealth * EfficiencySet.TotalEfficiencyPercentage / 100f);
        }

        /* -------- Private Methods --------- */
        private void AddWealthSource(WealthSource ws)
        {
            WealthSource preexistingWealthSource = WealthSources.Find(x => x.Type == ws.Type);

            if (preexistingWealthSource == null)
            {
                WealthSources.Add(ws);
                return;
            }

            preexistingWealthSource.SetValue(preexistingWealthSource.Value + ws.Value, preexistingWealthSource.TaxedValue + ws.TaxedValue);
        }

        /* -------- Static Methods -------- */
        public static IncomeSet CreateFromData(IncomeSetData data)
        {
            IncomeSet incomeSet = new IncomeSet();

            foreach (WealthSourceData wsd in data.wealthSourceDatas)
                incomeSet.WealthSources.Add(WealthSource.CreateFromData(wsd));

            incomeSet.TotalTaxedWealth = 0f;
            foreach (WealthSource ws in incomeSet.WealthSources)
                incomeSet.TotalTaxedWealth += ws.TaxedValue;

            incomeSet.EfficiencySet = IncomeEfficiencySet.CreateFromData(data.incomeEfficiencySetData);

            incomeSet.TotalIncome = Mathf.Round(incomeSet.TotalTaxedWealth * incomeSet.EfficiencySet.TotalEfficiencyPercentage / 100f);

            return incomeSet;
        }
    }
}
