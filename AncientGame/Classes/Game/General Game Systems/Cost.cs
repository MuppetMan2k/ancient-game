﻿using DataTypes;

namespace AncientGame
{
    class Cost
    {
        /* -------- Properties -------- */
        public int MoneyCost { get; private set; }
        public int AdminPowerCost { get; private set; }
        public int ManpowerCost { get; private set; }

        /* -------- Public Methods -------- */
        public bool CanAfford(LandTerritory lt)
        {
            Faction f = lt.Variables.OwningFaction;

            if (MoneyCost > f.Variables.Treasury.Treasury)
                return false;

            if (AdminPowerCost > f.Variables.AdminPower.AdminPower)
                return false;

            if (ManpowerCost > lt.Variables.Manpower.Manpower)
                return false;

            return true;
        }

        public Cost Multiply(float val)
        {
            Cost cost = new Cost();

            cost.MoneyCost = Mathf.ClampLower(Mathf.Round(val * (float)MoneyCost), 0);
            cost.AdminPowerCost = Mathf.ClampLower(Mathf.Round(val * (float)AdminPowerCost), 0);
            cost.ManpowerCost = Mathf.ClampLower(Mathf.Round(val * (float)ManpowerCost), 0);

            return cost;
        }

        public void SetMoneyCost(int val)
        {
            MoneyCost = Mathf.ClampLower(val, 0);
        }
        public void SetAdminPowerCost(int val)
        {
            AdminPowerCost = Mathf.ClampLower(val, 0);
        }
        public void SetManpowerCost(int val)
        {
            ManpowerCost = Mathf.ClampLower(val, 0);
        }

        /* -------- Static Methods -------- */
        public static Cost CreateFromData(CostData data)
        {
            Cost cost = new Cost();

            cost.MoneyCost = data.moneyCost;
            cost.AdminPowerCost = data.adminPowerCost;
            cost.ManpowerCost = data.manpowerCost;

            return cost;
        }

        /* -------- Static Instances -------- */
        public static Cost zero = new Cost
        {
            MoneyCost = 0,
            AdminPowerCost = 0,
            ManpowerCost = 0
        };
    }
}
