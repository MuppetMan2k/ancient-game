﻿namespace AncientGame
{
    class CultureManagerContainer
    {
        /* -------- Properties -------- */
        public CultureManager CultureManager { get; private set; }
        public LanguageManager LanguageManager { get; private set; }
        public ReligionManager ReligionManager { get; private set; }

        /* -------- Constructors -------- */
        public CultureManagerContainer()
        {
            CultureManager = new CultureManager();
            LanguageManager = new LanguageManager();
            ReligionManager = new ReligionManager();
        }
    }
}
