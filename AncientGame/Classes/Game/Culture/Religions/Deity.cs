﻿using DataTypes;

namespace AncientGame
{
    class Deity
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }

        /* -------- Static Methods -------- */
        public static Deity CreateFromData(DeityData data)
        {
            Deity deity = new Deity();

            deity.ID = data.id;

            return deity;
        }
    }
}
