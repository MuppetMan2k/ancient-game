﻿using System.Collections.Generic;

namespace AncientGame
{
    class ReligionManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<string, Religion> religions;

        /* -------- Constructors -------- */
        public ReligionManager()
        {
            religions = new Dictionary<string, Religion>();
        }

        /* -------- Public Methods -------- */
        public void AddReligion(Religion religion)
        {
            religions.Add(religion.ID, religion);
        }
        public Religion GetReligion(string id)
        {
            return religions[id];
        }
    }
}
