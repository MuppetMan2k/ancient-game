﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class Religion
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public List<Deity> Deities { get; private set; }
        public Culture AssociatedCulture { get; private set; }
        public ModifierSet ModifierSet { get; private set; }
        public List<AppointmentData> AppointmentDatas { get; private set; }

        /* -------- Constructors -------- */
        public Religion()
        {
            Deities = new List<Deity>();
            AppointmentDatas = new List<AppointmentData>();
        }

        /* -------- Public Methods -------- */
        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            ModifierSet.TestIsSatisfied(objectSet);
            set.Merge(ModifierSet);

            // TODO - Add deity modifiers. Do they just have modifiers? Or is there some sort of deity favor system?

            return set;
        }

        /* -------- Creation Methods -------- */
        public static Religion CreateFromData(ReligionData data, GameManagerContainer gameManagers)
        {
            Religion religion = new Religion();

            religion.ID = data.id;
            foreach (DeityData deityData in data.deityDatas)
            {
                Deity deity = Deity.CreateFromData(deityData);
                religion.Deities.Add(deity);
            }
            religion.ModifierSet = ModifierSet.CreateFromData(data.modifierDatas, gameManagers);

            return religion;
        }
        public void SetCrossReferences(ReligionData data, GameManagerContainer gameManagers)
        {
            AssociatedCulture = gameManagers.CultureManagers.CultureManager.GetCulture(data.associatedCultureID);
            foreach (string appointmentID in data.appointmentIDs)
                AppointmentDatas.Add(gameManagers.AppointmentManager.GetAppointmentData(appointmentID));
        }
    }
}
