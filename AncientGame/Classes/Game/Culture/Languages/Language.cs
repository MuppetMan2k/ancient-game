﻿using DataTypes;

namespace AncientGame
{
    class Language
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public Culture AssociatedCulture { get; private set; }
        public ModifierSet ModifierSet { get; private set; }

        /* -------- Creation Methods -------- */
        public static Language CreateFromData(LanguageData data, GameManagerContainer gameManagers)
        {
            Language language = new Language();

            language.ID = data.id;
            language.ModifierSet = ModifierSet.CreateFromData(data.modifierDatas, gameManagers);

            return language;
        }
        public void SetCrossReferences(LanguageData data, CultureManager cultureManager)
        {
            AssociatedCulture = cultureManager.GetCulture(data.associatedCultureID);
        }
    }
}
