﻿using System.Collections.Generic;

namespace AncientGame
{
    class LanguageManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<string, Language> languages;

        /* -------- Constructors -------- */
        public LanguageManager()
        {
            languages = new Dictionary<string, Language>();
        }

        /* -------- Public Methods -------- */
        public void AddLanguage(Language language)
        {
            languages.Add(language.ID, language);
        }
        public Language GetLanguage(string id)
        {
            return languages[id];
        }
    }
}
