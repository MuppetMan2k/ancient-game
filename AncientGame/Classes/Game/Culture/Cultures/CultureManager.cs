﻿using System.Collections.Generic;

namespace AncientGame
{
    class CultureManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<string, Culture> cultures;
        private Dictionary<string, CultureGroup> cultureGroups;
        private Dictionary<string, CultureFamily> cultureFamilies;

        /* -------- Constructors -------- */
        public CultureManager()
        {
            cultures = new Dictionary<string, Culture>();
            cultureGroups = new Dictionary<string, CultureGroup>();
            cultureFamilies = new Dictionary<string, CultureFamily>();
        }

        /* -------- Public Methods -------- */
        public void AddCulture(Culture culture)
        {
            cultures.Add(culture.ID, culture);
        }
        public Culture GetCulture(string id)
        {
            return cultures[id];
        }
        public void AddCultureGroup(CultureGroup cultureGroup)
        {
            cultureGroups.Add(cultureGroup.ID, cultureGroup);
        }
        public CultureGroup GetCultureGroup(string id)
        {
            return cultureGroups[id];
        }
        public void AddCultureFamily(CultureFamily cultureFamily)
        {
            cultureFamilies.Add(cultureFamily.ID, cultureFamily);
        }
        public CultureFamily GetCultureFamily(string id)
        {
            return cultureFamilies[id];
        }
    }
}
