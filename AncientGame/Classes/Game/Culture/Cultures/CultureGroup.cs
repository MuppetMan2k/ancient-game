﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class CultureGroup
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public CultureFamily CultureFamily { get; private set; }
        public ModifierSet ModifierSet { get; private set; }
        public List<AppointmentData> AppointmentDatas { get; private set; }

        /* -------- Constructors -------- */
        public CultureGroup()
        {
            AppointmentDatas = new List<AppointmentData>();
        }

        /* -------- Public Methods -------- */
        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            ModifierSet.TestIsSatisfied(objectSet);
            set.Merge(ModifierSet);

            set.Merge(CultureFamily.ModifierSet);

            return set;
        }

        /* -------- Creation Methods -------- */
        public static CultureGroup CreateFromData(CultureGroupData data, GameManagerContainer gameManagers)
        {
            CultureGroup cultureGroup = new CultureGroup();

            cultureGroup.ID = data.id;
            cultureGroup.ModifierSet = ModifierSet.CreateFromData(data.modifierDatas, gameManagers);

            return cultureGroup;
        }
        public void SetCrossReferences(CultureGroupData data, GameManagerContainer gameManagers)
        {
            CultureFamily = gameManagers.CultureManagers.CultureManager.GetCultureFamily(data.cultureFamilyID);

            foreach (string appointmentID in data.appointmentIDs)
                AppointmentDatas.Add(gameManagers.AppointmentManager.GetAppointmentData(appointmentID));
        }
    }
}
