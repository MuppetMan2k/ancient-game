﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class CultureFamily
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public ModifierSet ModifierSet { get; private set; }
        public List<AppointmentData> AppointmentDatas { get; private set; }

        /* -------- Constructors -------- */
        public CultureFamily()
        {
            AppointmentDatas = new List<AppointmentData>();
        }

        /* -------- Creation Methods -------- */
        public static CultureFamily CreateFromData(CultureFamilyData data, GameManagerContainer gameManagers)
        {
            CultureFamily cultureFamily = new CultureFamily();

            cultureFamily.ID = data.id;
            cultureFamily.ModifierSet = ModifierSet.CreateFromData(data.modifierDatas, gameManagers);

            return cultureFamily;
        }
        public void SetCrossReferences(CultureFamilyData data, AppointmentManager appointmentManager)
        {
            foreach (string appointmentID in data.appointmentIDs)
                AppointmentDatas.Add(appointmentManager.GetAppointmentData(appointmentID));
        }
    }
}
