﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class Culture
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public CultureGroup CultureGroup { get; private set; }
        public ModifierSet ModifierSet { get; private set; }
        public List<AppointmentData> AppointmentDatas { get; private set; }
        public StatueStyle StatueStyle { get; private set; }

        /* -------- Constructors -------- */
        public Culture()
        {
            AppointmentDatas = new List<AppointmentData>();
        }

        /* -------- Public Methods -------- */
        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            ModifierSet.TestIsSatisfied(objectSet);
            set.Merge(ModifierSet);

            set.Merge(CultureGroup.GetModifierSet(gameManagers));

            return set;
        }

        /* -------- Creation Methods -------- */
        public static Culture CreateFromData(CultureData data, GameManagerContainer gameManagers)
        {
            Culture culture = new Culture();

            culture.ID = data.id;
            culture.StatueStyle = StatueStyle.CreateFromData(data.statueStyleData);
            culture.ModifierSet = ModifierSet.CreateFromData(data.modifierDatas, gameManagers);

            return culture;
        }
        public void SetCrossReferences(CultureData data, GameManagerContainer gameManagers)
        {
            CultureGroup = gameManagers.CultureManagers.CultureManager.GetCultureGroup(data.cultureGroupID);

            foreach (string appointmentID in data.appointmentIDs)
                AppointmentDatas.Add(gameManagers.AppointmentManager.GetAppointmentData(appointmentID));
        }
    }
}
