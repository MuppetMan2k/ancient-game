﻿using System.Collections.Generic;

namespace AncientGame
{
    class LawManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<string, Law> laws;

        /* -------- Constructors -------- */
        public LawManager()
        {
            laws = new Dictionary<string, Law>();
        }

        /* -------- Public Methods -------- */
        public void AddLaw(Law law)
        {
            laws.Add(law.ID, law);
        }
        public Law GetLaw(string id)
        {
            return laws[id];
        }
    }
}
