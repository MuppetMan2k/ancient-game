﻿using DataTypes;

namespace AncientGame
{
    class Law
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public string FactionGroup { get; private set; }
        public ModifierSet ModifierSet { get; private set; }

        /* -------- Static Methods -------- */
        public static Law CreateFromData(LawData data, GameManagerContainer gameManagers)
        {
            Law law = new Law();

            law.ID = data.id;
            law.FactionGroup = data.factionGroup;
            law.ModifierSet = ModifierSet.CreateFromData(data.modifierDatas, gameManagers);

            return law;
        }
    }
}
