﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class LawSet
    {
        /* -------- Properties -------- */
        public List<Law> Laws { get; private set; }

        /* -------- Constructors -------- */
        public LawSet()
        {
            Laws = new List<Law>();
        }

        /* -------- Public Methods ------- */
        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            foreach (Law l in Laws)
                set.Merge(l.ModifierSet);
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static LawSet CreateFromData(LawSetData data, LawManager lawManager)
        {
            LawSet lawSet = new LawSet();

            foreach (string lawID in data.lawIDs)
            {
                Law law = lawManager.GetLaw(lawID);
                lawSet.Laws.Add(law);
            }

            return lawSet;
        }
    }
}
