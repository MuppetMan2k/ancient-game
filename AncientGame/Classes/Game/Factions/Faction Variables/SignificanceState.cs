﻿namespace AncientGame
{
    class SignificanceState
    {
        /* -------- Properties -------- */
        public int Significance { get; private set; }

        /* -------- Static Methods -------- */
        public static SignificanceState CreateFromData(int significanceData)
        {
            SignificanceState state = new SignificanceState();

            state.Significance = significanceData;

            return state;
        }
    }
}
