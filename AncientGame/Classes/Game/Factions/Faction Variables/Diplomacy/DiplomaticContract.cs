﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eDiplomaticContractType
    {
        WAR,
        TRADE,
        MILITARY_ACCESS,
        ALLIANCE,
        MILITARY_CLIENT_MASTER,
        MILITARY_CLIENT_SUBJECT,
        CULTURAL_CLIENT_MASTER,
        CULTURAL_CLIENT_SUBJECT,
        SATRAP_MASTER,
        SATRAP_SUBJECT,
        TRADE_COLONY_MASTER,
        TRADE_COLONY_SUBJECT,
        SETTLEMENT_COLONY_MASTER,
        SETTLEMENT_COLONY_SUBJECT,
        TRIBUTE,
        CASUS_BELLI
    }

    class DiplomaticContract
    {
        /* -------- Properties -------- */
        public Faction TargetFaction { get; private set; }
        public eDiplomaticContractType Type { get; private set; }
        public int TurnsRemaining { get; private set; }
        // Specific properties
        public int TributeValue { get; private set; }

        /* -------- Static Methods -------- */
        public static DiplomaticContract CreateFromData(DiplomaticContractData data, FactionManager fm)
        {
            DiplomaticContract contract = new DiplomaticContract();

            contract.TargetFaction = fm.GetFaction(data.targetFactionID);
            contract.Type = DataUtilities.ParseDiplomaticContractType(data.contractType);
            contract.TurnsRemaining = data.turnsRemaining;
            // Specific properties
            contract.TributeValue = data.tributeValue;

            return contract;
        }
    }
}
