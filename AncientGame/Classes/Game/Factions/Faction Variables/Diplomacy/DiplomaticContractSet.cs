﻿using System.Collections.Generic;

namespace AncientGame
{
    class DiplomaticContractSet
    {
        /* -------- Properties -------- */
        public List<DiplomaticContract> DiplomaticContracts { get; private set; }

        /* -------- Constructors -------- */
        public DiplomaticContractSet()
        {
            DiplomaticContracts = new List<DiplomaticContract>();
        }

        /* -------- Public Methods -------- */
        public List<DiplomaticContract> GetClientMasteryContracts()
        {
            return DiplomaticContracts.FindAll(x => GameUtilities.DiplomaticContractTypeIsClientMastery(x.Type));
        }
        public List<DiplomaticContract> GetColonyMasteryContracts()
        {
            return DiplomaticContracts.FindAll(x => GameUtilities.DiplomaticContractTypeIsColonyMastery(x.Type));
        }
        public DiplomaticContract GetColonySubjectContract()
        {
            return DiplomaticContracts.Find(x => GameUtilities.DiplomaticContractTypeIsColonySubjection(x.Type));
        }
        public bool HasWarContractWithFaction(Faction faction)
        {
            foreach (DiplomaticContract dc in DiplomaticContracts)
                if (dc.TargetFaction == faction && dc.Type == eDiplomaticContractType.WAR)
                    return true;

            return false;
        }
        public bool HasMilitaryAccessContractWithFaction(Faction faction)
        {
            foreach (DiplomaticContract dc in DiplomaticContracts)
                if (dc.TargetFaction == faction && dc.Type == eDiplomaticContractType.MILITARY_ACCESS)
                    return true;

            return false;
        }
        public bool HasTradeAgreementContractWithFaction(Faction faction)
        {
            foreach (DiplomaticContract dc in DiplomaticContracts)
                if (dc.TargetFaction == faction && dc.Type == eDiplomaticContractType.TRADE)
                    return true;

            return false;
        }
    }
}
