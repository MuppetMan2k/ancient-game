﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eLandTerritoryControlLevel
    {
        NO_CONTROL,
        OCCUPATION,
        MILITARY_OWNERSHIP,
        ANNEXED_OWNERSHIP,
        HOMELAND_OWNERSHIP
    }

    class LandTerritoryControl
    {
        /* -------- Properties -------- */
        public LandTerritory LandTerritory { get; private set; }
        public eLandTerritoryControlLevel CurrControlLevel { get; private set; }
        public eLandTerritoryControlLevel PrevControlLevel { get; private set; }
        public int TurnsOfNoControl { get; private set; }
        public eLandTerritoryControlLevel DevelopingControlLevel { get; private set; }
        public int DevelopmentRemainingTurns { get; private set; }

        /* -------- Private Methods --------- */
        private static bool ControlLevelIsOwnership(eLandTerritoryControlLevel controlLevel)
        {
            return (controlLevel == eLandTerritoryControlLevel.MILITARY_OWNERSHIP || controlLevel == eLandTerritoryControlLevel.ANNEXED_OWNERSHIP || controlLevel == eLandTerritoryControlLevel.HOMELAND_OWNERSHIP);
        }

        /* -------- Static Methods -------- */
        public static LandTerritoryControl CreateFromData(Faction parentFaction, LandTerritoryControlData data, LandTerritoryManager landTerritoryManager)
        {
            LandTerritoryControl ltc = new LandTerritoryControl();

            ltc.LandTerritory = landTerritoryManager.GetLandTerritory(data.landTerritoryID);
            ltc.CurrControlLevel = DataUtilities.ParseLandTerritoryControlLevel(data.currControlLevel);
            ltc.PrevControlLevel = DataUtilities.ParseLandTerritoryControlLevel(data.prevControlLevel);
            ltc.TurnsOfNoControl = data.turnsOfNoControl;
            ltc.DevelopingControlLevel = DataUtilities.ParseLandTerritoryControlLevel(data.developingControlLevel);
            ltc.DevelopmentRemainingTurns = data.developmentTurnsRemaining;

            if (ControlLevelIsOwnership(ltc.CurrControlLevel))
                ltc.LandTerritory.Variables.SetOwningFaction(parentFaction);

            if (ltc.CurrControlLevel == eLandTerritoryControlLevel.OCCUPATION)
                ltc.LandTerritory.Variables.SetOccupyingFaction(parentFaction);

            return ltc;
        }
    }
}
