﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class GovernmentTrait
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public ModifierSet ModifierSet { get; private set; }
        public List<AppointmentData> AppointmentDatas { get; private set; }

        /* -------- Constructors -------- */
        public GovernmentTrait()
        {
            AppointmentDatas = new List<AppointmentData>();
        }

        /* -------- Static Methods -------- */
        public static GovernmentTrait CreateFromData(GovernmentTraitData data, GameManagerContainer gameManagers)
        {
            GovernmentTrait governmentTrait = new GovernmentTrait();

            governmentTrait.ID = data.id;
            governmentTrait.ModifierSet = ModifierSet.CreateFromData(data.modifierDatas, gameManagers);
            foreach (string appointmentID in data.appointmentIDs)
                governmentTrait.AppointmentDatas.Add(gameManagers.AppointmentManager.GetAppointmentData(appointmentID));

            return governmentTrait;
        }
    }
}
