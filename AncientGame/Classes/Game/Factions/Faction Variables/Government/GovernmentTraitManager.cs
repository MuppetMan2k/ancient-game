﻿using System.Collections.Generic;

namespace AncientGame
{
    class GovernmentTraitManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<string, GovernmentTrait> governmentTraits;

        /* -------- Constructors -------- */
        public GovernmentTraitManager()
        {
            governmentTraits = new Dictionary<string, GovernmentTrait>();
        }

        /* -------- Public Methods -------- */
        public void AddGovernmentTrait(GovernmentTrait governmentTrait)
        {
            governmentTraits.Add(governmentTrait.ID, governmentTrait);
        }
        public GovernmentTrait GetGovernmentTrait(string id)
        {
            return governmentTraits[id];
        }
    }
}
