﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eGovernmentGroup
    {
        MONARCHY,
        REPUBLIC,
        CONFEDERATION
    }

    class Government
    {
        /* -------- Properties -------- */
        public eGovernmentGroup Group { get; private set; }
        public List<GovernmentTrait> Traits { get; private set; }

        /* -------- Constructors -------- */
        public Government()
        {
            Traits = new List<GovernmentTrait>();
        }

        /* -------- Public Methods -------- */
        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            switch (Group)
            {
                case eGovernmentGroup.MONARCHY:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.governmentGroupModifierDatas_Monarchy)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eGovernmentGroup.REPUBLIC:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.governmentGroupModifierDatas_Republic)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eGovernmentGroup.CONFEDERATION:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.governmentGroupModifierDatas_Confederation)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(Group);
                    break;
            }
            foreach (GovernmentTrait gt in Traits)
                set.Merge(gt.ModifierSet);
            set.TestIsSatisfied(objectSet);

            return set;
        }

        public string GetGroupName()
        {
            switch (Group)
            {
                case eGovernmentGroup.MONARCHY:
                    return Localization.Localize("text_government-group_monarchy_name");
                case eGovernmentGroup.REPUBLIC:
                    return Localization.Localize("text_government-group_republic_name");
                case eGovernmentGroup.CONFEDERATION:
                    return Localization.Localize("text_government-group_confederation_name");
                default:
                    Debug.LogUnrecognizedValueWarning(Group);
                    return "";
            }
        }

        /* -------- Static Methods -------- */
        public static Government CreateFromData(GovernmentData data, GovernmentTraitManager governmentTraitManager)
        {
            Government government = new Government();

            government.Group = DataUtilities.ParseGovernmentGroup(data.governmentGroup);
            foreach (string governmentTraitID in data.governmentTraitIDs)
            {
                GovernmentTrait trait = governmentTraitManager.GetGovernmentTrait(governmentTraitID);
                government.Traits.Add(trait);
            }

            return government;
        }
    }
}
