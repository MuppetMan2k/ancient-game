﻿using DataTypes;

namespace AncientGame
{
    class LandProvinceStake
    {
        /* -------- Properties -------- */
        public LandProvince LandProvince { get; private set; }
        public InstitutionSet Institutions { get; private set; }
        public TaxSet Taxes { get; private set; }

        /* -------- Constructors -------- */
        public LandProvinceStake()
        {
            Institutions = new InstitutionSet();
            Taxes = new TaxSet();
        }

        /* -------- Public Methods -------- */
        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            set.Merge(Institutions.GetModifierSet(gameManagers));
            set.Merge(Taxes.GetModifierSet(gameManagers));

            return set;
        }

        /* -------- Static Methods -------- */
        public static LandProvinceStake CreateFromData(LandProvinceStakeData data, GameManagerContainer gameManagers)
        {
            LandProvinceStake landProvinceStake = new LandProvinceStake();

            landProvinceStake.LandProvince = gameManagers.MapManager.LandProvinceManager.GetLandProvince(data.landProvinceID);
            int institutionNum = Mathf.Min(landProvinceStake.Institutions.InstitutionSlots.Count, data.institutionStateDatas.Length);
            for (int i = 0; i < institutionNum; i++)
            {
                InstitutionStateData institutionStateData = data.institutionStateDatas[i];
                Institution institution = Institution.CreateFromData(institutionStateData, gameManagers);
                landProvinceStake.Institutions.InstitutionSlots[i].SetInstitution(institution);
            }
            foreach (string taxID in data.taxIDs)
            {
                Tax tax = gameManagers.LandProvinceStakeManagers.TaxManager.GetTax(taxID);
                landProvinceStake.Taxes.Taxes.Add(tax);
            }

            return landProvinceStake;
        }
    }
}
