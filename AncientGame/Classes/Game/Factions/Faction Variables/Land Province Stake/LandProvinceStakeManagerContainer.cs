﻿namespace AncientGame
{
    class LandProvinceStakeManagerContainer
    {
        /* -------- Properties -------- */
        public InstitutionManager InstitutionManager { get; private set; }
        public TaxManager TaxManager { get; private set; }

        /* -------- Constructors -------- */
        public LandProvinceStakeManagerContainer()
        {
            InstitutionManager = new InstitutionManager();
            TaxManager = new TaxManager();
        }
    }
}
