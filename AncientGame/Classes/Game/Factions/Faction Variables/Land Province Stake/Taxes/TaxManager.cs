﻿using System.Collections.Generic;

namespace AncientGame
{
    class TaxManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<string, Tax> taxes;

        /* -------- Constructors -------- */
        public TaxManager()
        {
            taxes = new Dictionary<string, Tax>();
        }

        /* -------- Public Methods -------- */
        public void AddTax(Tax tax)
        {
            taxes.Add(tax.ID, tax);
        }
        public Tax GetTax(string id)
        {
            return taxes[id];
        }
    }
}
