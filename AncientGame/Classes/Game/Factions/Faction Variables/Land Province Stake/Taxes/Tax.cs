﻿using DataTypes;

namespace AncientGame
{
    class Tax
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public Culture Culture { get; private set; }
        public ModifierSet ModifierSet { get; private set; }

        /* -------- Public Methods -------- */
        public string GetName()
        {
            string locID = "text_tax_" + ID + "_name";
            return Localization.Localize(locID);
        }

        /* -------- Static Methods -------- */
        public static Tax CreateFromData(TaxData data, GameManagerContainer gameManagers)
        {
            Tax tax = new Tax();

            tax.ID = data.id;
            tax.Culture = gameManagers.CultureManagers.CultureManager.GetCulture(data.cultureID);
            tax.ModifierSet = ModifierSet.CreateFromData(data.modifierDatas, gameManagers);

            return tax;
        }
    }
}
