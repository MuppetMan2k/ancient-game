﻿using System.Collections.Generic;

namespace AncientGame
{
    class TaxSet
    {
        /* -------- Properties -------- */
        public List<Tax> Taxes { get; private set; }

        /* -------- Public Methods -------- */
        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            foreach (Tax t in Taxes)
                set.Merge(t.ModifierSet);
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Constructors -------- */
        public TaxSet()
        {
            Taxes = new List<Tax>();
        }
    }
}
