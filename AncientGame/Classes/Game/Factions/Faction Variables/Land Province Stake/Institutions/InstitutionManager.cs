﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class InstitutionManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<string, InstitutionData> institutionDatas;

        /* -------- Constructors -------- */
        public InstitutionManager()
        {
            institutionDatas = new Dictionary<string, InstitutionData>();
        }

        /* -------- Public Methods -------- */
        public void AddInstitutionData(InstitutionData institutionData)
        {
            institutionDatas.Add(institutionData.id, institutionData);
        }
        public InstitutionData GetInstitutionData(string id)
        {
            return institutionDatas[id];
        }
    }
}
