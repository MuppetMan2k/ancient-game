﻿namespace AncientGame
{
    class InstitutionSlot
    {
        /* -------- Properties -------- */
        public Institution Institution { get; private set; }

        /* -------- Public Methods -------- */
        public void SetInstitution(Institution inInstitution)
        {
            Institution = inInstitution;
        }
    }
}
