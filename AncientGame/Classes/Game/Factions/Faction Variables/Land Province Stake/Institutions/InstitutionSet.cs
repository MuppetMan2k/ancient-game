﻿using System.Collections.Generic;

namespace AncientGame
{
    class InstitutionSet
    {
        /* -------- Properties -------- */
        public List<InstitutionSlot> InstitutionSlots { get; private set; }

        /* -------- Constructors -------- */
        public InstitutionSet()
        {
            InstitutionSlots = new List<InstitutionSlot>();

            // TODO - create institution slots
        }

        /* -------- Public Methods -------- */
        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            foreach (InstitutionSlot s in InstitutionSlots)
                if (s.Institution != null && s.Institution.TurnsToCompletion == 0)
                    set.Merge(s.Institution.ModifierSet);
            set.TestIsSatisfied(objectSet);

            return set;
        }
    }
}
