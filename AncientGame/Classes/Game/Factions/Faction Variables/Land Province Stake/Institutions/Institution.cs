﻿using DataTypes;

namespace AncientGame
{
    class Institution
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public string FactionGroup { get; private set; }
        public int TurnsToCompletion { get; private set; }
        public ModifierSet ModifierSet { get; private set; }

        /* -------- Static Methods -------- */
        public static Institution CreateFromData(InstitutionStateData data, GameManagerContainer gameManagers)
        {
            InstitutionData institutionData = gameManagers.LandProvinceStakeManagers.InstitutionManager.GetInstitutionData(data.institutionID);

            Institution institution = new Institution();

            institution.ID = data.institutionID;
            institution.FactionGroup = institutionData.factionGroup;
            institution.TurnsToCompletion = data.turnsToCompletion;
            institution.ModifierSet = ModifierSet.CreateFromData(institutionData.modifierDatas, gameManagers);

            return institution;
        }
    }
}
