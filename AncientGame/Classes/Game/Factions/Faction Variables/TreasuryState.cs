﻿namespace AncientGame
{
    class TreasuryState
    {
        /* -------- Properties -------- */
        public int Treasury { get; private set; }
        public int TreasuryChange { get; private set; }

        /* -------- Public Methods -------- */
        public void ChargeTreasury(int amount)
        {
            Treasury = Mathf.ClampLower(Treasury - amount, 0);
        }

        /* -------- Static Methods -------- */
        public static TreasuryState CreateFromData(int treasuryData, int treasuryChangeData)
        {
            TreasuryState state = new TreasuryState();

            state.Treasury = Mathf.ClampLower(treasuryData, 0);
            state.TreasuryChange = treasuryChangeData;

            return state;
        }
    }
}
