﻿namespace AncientGame
{
    class AdminPowerState
    {
        /* -------- Properties -------- */
        public int AdminPower { get; private set; }
        public int MaxAdminPower { get; private set; }
        public int AdminPowerChange { get; private set; }

        /* -------- Public Methods -------- */
        public void ChargeAdminPower(int amount)
        {
            AdminPower = Mathf.ClampLower(AdminPower - amount, 0);
        }

        /* -------- Static Methods -------- */
        public static AdminPowerState CreateFromData(int adminPowerData, int maxAdminPowerData, int adminPowerChangeData)
        {
            AdminPowerState state = new AdminPowerState();

            state.AdminPower = Mathf.ClampLower(adminPowerData, 0);
            state.MaxAdminPower = Mathf.ClampLower(maxAdminPowerData, 0);
            state.AdminPowerChange = Mathf.ClampLower(adminPowerChangeData, 0);

            return state;
        }
    }
}
