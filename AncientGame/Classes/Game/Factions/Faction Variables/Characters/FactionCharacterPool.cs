﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class FactionCharacterPool
    {
        /* -------- Properties -------- */
        public List<Character> Characters { get; private set; }

        /* -------- Constructors -------- */
        public FactionCharacterPool()
        {
            Characters = new List<Character>();
        }

        /* -------- Static Methods -------- */
        public static FactionCharacterPool CreateFromData(FactionCharacterPoolData data, GameManagerContainer gameManagers)
        {
            FactionCharacterPool pool = new FactionCharacterPool();

            foreach (CharacterData cd in data.characterDatas)
            {
                Character character = Character.CreateFromData(cd, gameManagers);
                pool.Characters.Add(character);
            }

            return pool;
        }
    }
}
