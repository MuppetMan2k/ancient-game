﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class AppointmentSet
    {
        /* -------- Properties -------- */
        public List<Appointment> Appointments { get; private set; }
        public Appointment FactionLeaderAppointment { get; private set; }
        public Appointment FactionHeirAppointment { get; private set; }

        /* -------- Constructors -------- */
        public AppointmentSet()
        {
            Appointments = new List<Appointment>();
        }

        /* -------- Static Methods -------- */
        public static AppointmentSet CreateFromData(AppointmentSetData data, AppointmentManager appointmentManager, FactionCharacterPool factionCharacterPool)
        {
            AppointmentSet set = new AppointmentSet();

            foreach (AppointmentStateData asd in data.appointmentStateDatas)
            {
                AppointmentData appointmentData = appointmentManager.GetAppointmentData(asd.appointmentID);
                Appointment appointment = Appointment.CreateFromData(appointmentData);
                appointment.SetState(asd, factionCharacterPool);

                set.Appointments.Add(appointment);

                if (appointment.Type == eAppointmentType.FACTION_LEADER)
                    set.FactionLeaderAppointment = appointment;
                if (appointment.Type == eAppointmentType.FACTION_HEIR)
                    set.FactionHeirAppointment = appointment;
            }

            return set;
        }
    }
}
