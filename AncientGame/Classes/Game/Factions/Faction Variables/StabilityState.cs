﻿using DataTypes;

namespace AncientGame
{
    class StabilityState
    {
        /* -------- Properties -------- */
        public int Stability { get; private set; }

        /* -------- Private Fields -------- */
        // Constants
        private const int MAX_VALUE = 10;
        private const int MIN_VALUE = -10;

        /* -------- Public Methods -------- */
        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (Stability > 0)
                foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionPositiveStabilityModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, (float)Stability));
            else if (Stability < 0)
                foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionNegativeStabilityModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, (float)Stability));
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static StabilityState CreateFromData(int data)
        {
            StabilityState stability = new StabilityState();

            stability.Stability = Mathf.Clamp(data, MIN_VALUE, MAX_VALUE);

            return stability;
        }
    }
}
