﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eFactionSize
    {
        TRIBAL_STATE,
        CITY_STATE,
        SMALL_STATE,
        STATE,
        PROVINCIAL_STATE,
        MULTI_PROVINCIAL_STATE
    }

    enum eFactionSpecialRating
    {
        EMPIRE,
        KINGDOM,
        LEAGUE,
        CONFEDERATION,
        REPUBLIC,
        HEGEMON,
        COLONIES,
        HORDE,
        NONE
    }

    class FactionVariables
    {
        /* -------- Properties -------- */
        public Faction ParentFaction { get; private set; }
        public bool IsAlive { get; private set; }
        public eFactionSize Size { get; private set; }
        public eFactionSpecialRating SpecialRating { get; private set; }
        public LandTerritory CapitolLandTerritory { get; private set; }
        public string EmblemID { get; private set; }
        public Color PrimaryColor { get; private set; }
        public Color SecondaryColor { get; private set; }
        public Culture Culture { get; private set; }
        public Language Language { get; private set; }
        public Religion Religion { get; private set; }
        public Government Government { get; private set; }
        public LawSet LawSet { get; private set; }
        public List<LandTerritoryControl> LandTerritoryControls { get; private set; }
        public List<LandProvinceStake> LandProvinceStakes { get; private set; }
        public StabilityState Stability { get; private set; }
        public TreasuryState Treasury { get; private set; }
        public AdminPowerState AdminPower { get; private set; }
        public SignificanceState Significance { get; private set; }
        public DiplomaticContractSet DiplomaticContractSet { get; private set; }
        public FactionCharacterPool CharacterPool { get; private set; }
        public AppointmentSet AppointmentSet { get; private set; }
        public StatueSet StatueSet { get; private set; }
        public MilitaryEra MilitaryEra { get; private set; }
        // Assets
        public Texture2D StandardColorTex { get; private set; }
        public Texture2D StandardTintTex { get; private set; }
        public Texture2D EmblemTintTex { get; private set; }
        public Texture2D EmblemColorTex { get; private set; }

        /* -------- Constructors -------- */
        public FactionVariables(Faction inParentFaction)
        {
            ParentFaction = inParentFaction;

            LandTerritoryControls = new List<LandTerritoryControl>();
            LandProvinceStakes = new List<LandProvinceStake>();

            DiplomaticContractSet = new DiplomaticContractSet();
        }

        /* -------- Public Methods -------- */
        public void Update(GraphicsComponent g, CameraManager cm)
        {
            StatueSet.Update(g, cm);
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            set.Merge(GetSizeModifierSet(gameManagers));
            set.Merge(GetSpecialRatingModifierSet(gameManagers));
            set.Merge(Culture.GetModifierSet(gameManagers));

            Language.ModifierSet.TestIsSatisfied(objectSet);
            set.Merge(Language.ModifierSet);

            set.Merge(Religion.GetModifierSet(gameManagers));
            set.Merge(Government.GetModifierSet(gameManagers));
            set.Merge(LawSet.GetModifierSet(gameManagers));
            set.Merge(Stability.GetModifierSet(gameManagers));

            return set;
        }
        public ModifierSet GetSizeModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            switch (Size)
            {
                case eFactionSize.TRIBAL_STATE:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSizeModifierDatas_TribalState)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSize.CITY_STATE:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSizeModifierDatas_CityState)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSize.SMALL_STATE:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSizeModifierDatas_SmallState)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSize.STATE:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSizeModifierDatas_State)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSize.PROVINCIAL_STATE:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSizeModifierDatas_ProvincialState)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSize.MULTI_PROVINCIAL_STATE:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSizeModifierDatas_MultiProvincialState)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(Size);
                    break;
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }
        public ModifierSet GetSpecialRatingModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            switch (SpecialRating)
            {
                case eFactionSpecialRating.COLONIES:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSpecialRatingModifierDatas_Colonies)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSpecialRating.CONFEDERATION:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSpecialRatingModifierDatas_Confederation)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSpecialRating.EMPIRE:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSpecialRatingModifierDatas_Empire)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSpecialRating.HEGEMON:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSpecialRatingModifierDatas_Hegemon)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSpecialRating.HORDE:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSpecialRatingModifierDatas_Horde)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSpecialRating.KINGDOM:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSpecialRatingModifierDatas_Kingdom)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSpecialRating.LEAGUE:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSpecialRatingModifierDatas_League)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSpecialRating.REPUBLIC:
                    foreach (ModifierData md in CalibrationManager.FactionCalibration.Data.factionSpecialRatingModifierDatas_Republic)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eFactionSpecialRating.NONE:
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(Size);
                    break;
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        public LandTerritoryControl GetLandTerritoryControl(LandTerritory lt)
        {
            foreach (LandTerritoryControl ltc in LandTerritoryControls)
                if (ltc.LandTerritory == lt)
                    return ltc;

            return null;
        }
        public LandProvinceStake GetLandProvinceStake(LandProvince lp)
        {
            foreach (LandProvinceStake lps in LandProvinceStakes)
                if (lps.LandProvince == lp)
                    return lps;

            return null;
        }

        // Recalculation methods
        public void RecalculateSizeAndSpecialRating(FactionManager fm)
        {
            Size = fm.CalculateFactionSize(ParentFaction);
            SpecialRating = fm.CalculateFactionSpecialRating(ParentFaction);
        }

        /* -------- Private Methods --------- */
        private void LoadStandardAndEmblemGraphics(ContentManager content)
        {
            StandardColorTex = AssetLoader.LoadTexture(ParentFaction.GetStandardTextureID() + "-color", content);
            StandardTintTex = AssetLoader.LoadTexture(ParentFaction.GetStandardTextureID() + "-tint", content);
            EmblemTintTex = AssetLoader.LoadTexture(EmblemID + "-tint", content);
            EmblemColorTex = AssetLoader.LoadTexture(EmblemID + "-color", content);
        }

        /* -------- Static Methods -------- */
        public static FactionVariables CreateFromData(Faction parentFaction, FactionStateData data, GameManagerContainer gameManagers, ContentManager content)
        {
            FactionVariables variables = new FactionVariables(parentFaction);

            variables.Size = DataUtilities.ParseFactionSize(data.size);
            variables.SpecialRating = DataUtilities.ParseFactionSpecialRating(data.specialRating);
            variables.EmblemID = data.emblemID;
            variables.CapitolLandTerritory = String.IsNullOrEmpty(data.capitolLandTerritoryID) ? null : gameManagers.MapManager.LandTerritoryManager.GetLandTerritory(data.capitolLandTerritoryID);
            variables.PrimaryColor = data.primaryColor;
            variables.SecondaryColor = data.secondaryColor;
            variables.Culture = gameManagers.CultureManagers.CultureManager.GetCulture(data.cultureID);
            variables.Language = gameManagers.CultureManagers.LanguageManager.GetLanguage(data.languageID);
            variables.Religion = gameManagers.CultureManagers.ReligionManager.GetReligion(data.religionID);
            variables.Government = Government.CreateFromData(data.governmentData, gameManagers.GovernmentTraitManager);
            variables.LawSet = LawSet.CreateFromData(data.lawSetData, gameManagers.LawManager);
            foreach (LandTerritoryControlData ltcd in data.landTerritoryControlDatas)
            {
                LandTerritoryControl ltc = LandTerritoryControl.CreateFromData(parentFaction, ltcd, gameManagers.MapManager.LandTerritoryManager);
                variables.LandTerritoryControls.Add(ltc);
            }
            foreach (LandProvinceStakeData lpsd in data.landProvinceStakeDatas)
            {
                LandProvinceStake lps = LandProvinceStake.CreateFromData(lpsd, gameManagers);
                variables.LandProvinceStakes.Add(lps);
            }
            variables.Stability = StabilityState.CreateFromData(data.stability);
            variables.Treasury = TreasuryState.CreateFromData(data.treasury, data.treasuryChange);
            variables.AdminPower = AdminPowerState.CreateFromData(data.adminPower, data.maxAdminPower, data.adminPowerChange);
            variables.Significance = SignificanceState.CreateFromData(data.significance);
            foreach (DiplomaticContractData dcd in data.diplomaticContractDatas)
            {
                DiplomaticContract contract = DiplomaticContract.CreateFromData(dcd, gameManagers.FactionManager);
                variables.DiplomaticContractSet.DiplomaticContracts.Add(contract);
            }
            variables.CharacterPool = FactionCharacterPool.CreateFromData(data.factionCharacterPoolData, gameManagers);
            variables.AppointmentSet = AppointmentSet.CreateFromData(data.appointmentSetData, gameManagers.AppointmentManager, variables.CharacterPool);
            variables.MilitaryEra = String.IsNullOrEmpty(data.militaryEraID) ? null : gameManagers.MilitaryEraManager.GetMilitaryEra(data.militaryEraID);
            StatueStyle statueStyle = (variables.MilitaryEra == null) ? variables.Culture.StatueStyle : variables.MilitaryEra.StatueStyle;
            variables.StatueSet = StatueSet.CreateFromData(data.statueSetData, parentFaction, statueStyle, gameManagers, content);

            // TODO - set IsAlive from loaded data (has any territories or armies)
            variables.IsAlive = true; // DEBUG

            variables.LoadStandardAndEmblemGraphics(content);

            return variables;
        }
    }
}
