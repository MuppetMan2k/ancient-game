﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class FactionManager
    {
        /* -------- Private Fields -------- */
        private List<Faction> factions;

        /* -------- Constructors -------- */
        public FactionManager()
        {
            factions = new List<Faction>();
        }

        /* -------- Public Methods -------- */
        public void Update(GraphicsComponent g, CameraManager cm)
        {
            foreach (Faction f in factions)
                f.Update(g, cm);
        }

        public void DrawStatues(StatueGraphicsManager gm, GraphicsComponent g, CameraManager cm)
        {
            foreach (Faction f in factions)
            {
                if (!f.Variables.IsAlive)
                    continue;

                foreach (Statue s in f.Variables.StatueSet.Statues)
                    gm.DrawStatue(s, cm, g);
            }
        }

        public void AddFaction(Faction faction)
        {
            factions.Add(faction);
        }
        public Faction GetFaction(string id)
        {
            foreach (Faction f in factions)
                if (f.ID == id)
                    return f;

            return null;
        }
        public List<Faction> GetAllFactions()
        {
            return factions;
        }

        public Faction GetFirstAlivePlayerFaction()
        {
            foreach (Faction faction in factions)
                if (faction.Variables.IsAlive && faction.IsPlayerControlled)
                    return faction;

            return null;
        }
        public Faction GetFirstAliveNonPlayerFaction()
        {
            foreach (Faction faction in factions)
                if (faction.Variables.IsAlive && !faction.IsPlayerControlled)
                    return faction;

            return null;
        }

        public Faction GetNextAlivePlayerFaction(Faction currFaction)
        {
            int currIndex = factions.IndexOf(currFaction);

            for (int i = currIndex + 1; i < factions.Count; i++)
            {
                Faction faction = factions[i];
                if (faction.Variables.IsAlive && faction.IsPlayerControlled)
                    return faction;
            }

            return null;
        }
        public Faction GetNextAliveNonPlayerFaction(Faction currFaction)
        {
            int currIndex = factions.IndexOf(currFaction);

            for (int i = currIndex + 1; i < factions.Count; i++)
            {
                Faction faction = factions[i];
                if (faction.Variables.IsAlive && !faction.IsPlayerControlled)
                    return faction;
            }

            return null;
        }
        
        public eFactionSize CalculateFactionSize(Faction faction)
        {
            int numLandTerritories = faction.CalulateNumberLandTerritoriesOwned(true, true);
            int numLandProvinces = faction.CalculateNumberLandProvincesStaked(true, true);

            if (FactionIsMultiProvincialState(numLandTerritories, numLandProvinces))
                return eFactionSize.MULTI_PROVINCIAL_STATE;

            if (FactionIsProvincialState(numLandTerritories, numLandProvinces))
                return eFactionSize.PROVINCIAL_STATE;

            if (FactionIsState(numLandTerritories, numLandProvinces))
                return eFactionSize.STATE;

            if (FactionIsCityState(faction, numLandTerritories, numLandProvinces))
                return eFactionSize.CITY_STATE;

            if (FactionIsTribalState(faction, numLandTerritories, numLandProvinces))
                return eFactionSize.TRIBAL_STATE;

            if (FactionIsSmallState(numLandTerritories, numLandProvinces))
                return eFactionSize.SMALL_STATE;

            return eFactionSize.SMALL_STATE;
        }
        public bool FactionIsTribalState(Faction faction, int numLandTerritories, int numLandProvinces)
        {
            if (!CalibrationManager.FactionCalibration.Data.tribalStatePossibleCultureFamilyIDs.Contains(faction.Variables.Culture.CultureGroup.CultureFamily.ID))
                return false;

            int minLandTerritories = CalibrationManager.FactionCalibration.Data.cityStateMinLandTerritories;
            if (minLandTerritories != -1 && numLandTerritories < minLandTerritories)
                return false;

            int maxLandTerritories = CalibrationManager.FactionCalibration.Data.cityStateMaxLandTerritories;
            if (maxLandTerritories != -1 && numLandTerritories > maxLandTerritories)
                return false;

            int minLandProvinces = CalibrationManager.FactionCalibration.Data.cityStateMinLandProvinces;
            if (minLandProvinces != -1 && numLandProvinces < minLandProvinces)
                return false;

            int maxLandProvinces = CalibrationManager.FactionCalibration.Data.cityStateMaxLandProvinces;
            if (maxLandProvinces != -1 && numLandProvinces < maxLandProvinces)
                return false;

            return true;
        }
        public bool FactionIsCityState(Faction faction, int numLandTerritories, int numLandProvinces)
        {
            if (!CalibrationManager.FactionCalibration.Data.cityStatePossibleCultureFamilyIDs.Contains(faction.Variables.Culture.CultureGroup.CultureFamily.ID))
                return false;

            int minLandTerritories = CalibrationManager.FactionCalibration.Data.cityStateMinLandTerritories;
            if (minLandTerritories != -1 && numLandTerritories < minLandTerritories)
                return false;

            int maxLandTerritories = CalibrationManager.FactionCalibration.Data.cityStateMaxLandTerritories;
            if (maxLandTerritories != -1 && numLandTerritories > maxLandTerritories)
                return false;

            int minLandProvinces = CalibrationManager.FactionCalibration.Data.cityStateMinLandProvinces;
            if (minLandProvinces != -1 && numLandProvinces < minLandProvinces)
                return false;

            int maxLandProvinces = CalibrationManager.FactionCalibration.Data.cityStateMaxLandProvinces;
            if (maxLandProvinces != -1 && numLandProvinces < maxLandProvinces)
                return false;

            return true;
        }
        public bool FactionIsSmallState(int numLandTerritories, int numLandProvinces)
        {
            int minLandTerritories = CalibrationManager.FactionCalibration.Data.smallStateMinLandTerritories;
            if (minLandTerritories != -1 && numLandTerritories < minLandTerritories)
                return false;

            int maxLandTerritories = CalibrationManager.FactionCalibration.Data.smallStateMaxLandTerritories;
            if (maxLandTerritories != -1 && numLandTerritories > maxLandTerritories)
                return false;

            int minLandProvinces = CalibrationManager.FactionCalibration.Data.smallStateMinLandProvinces;
            if (minLandProvinces != -1 && numLandProvinces < minLandProvinces)
                return false;

            int maxLandProvinces = CalibrationManager.FactionCalibration.Data.smallStateMaxLandProvinces;
            if (maxLandProvinces != -1 && numLandProvinces < maxLandProvinces)
                return false;

            return true;
        }
        public bool FactionIsState(int numLandTerritories, int numLandProvinces)
        {
            int minLandTerritories = CalibrationManager.FactionCalibration.Data.stateMinLandTerritories;
            if (minLandTerritories != -1 && numLandTerritories < minLandTerritories)
                return false;

            int maxLandTerritories = CalibrationManager.FactionCalibration.Data.stateMaxLandTerritories;
            if (maxLandTerritories != -1 && numLandTerritories > maxLandTerritories)
                return false;

            int minLandProvinces = CalibrationManager.FactionCalibration.Data.stateMinLandProvinces;
            if (minLandProvinces != -1 && numLandProvinces < minLandProvinces)
                return false;

            int maxLandProvinces = CalibrationManager.FactionCalibration.Data.stateMaxLandProvinces;
            if (maxLandProvinces != -1 && numLandProvinces < maxLandProvinces)
                return false;

            return true;
        }
        public bool FactionIsProvincialState(int numLandTerritories, int numLandProvinces)
        {
            int minLandTerritories = CalibrationManager.FactionCalibration.Data.provincialStateMinLandTerritories;
            if (minLandTerritories != -1 && numLandTerritories < minLandTerritories)
                return false;

            int maxLandTerritories = CalibrationManager.FactionCalibration.Data.provincialStateMaxLandTerritories;
            if (maxLandTerritories != -1 && numLandTerritories > maxLandTerritories)
                return false;

            int minLandProvinces = CalibrationManager.FactionCalibration.Data.provincialStateMinLandProvinces;
            if (minLandProvinces != -1 && numLandProvinces < minLandProvinces)
                return false;

            int maxLandProvinces = CalibrationManager.FactionCalibration.Data.provincialStateMaxLandProvinces;
            if (maxLandProvinces != -1 && numLandProvinces < maxLandProvinces)
                return false;

            return true;
        }
        public bool FactionIsMultiProvincialState(int numLandTerritories, int numLandProvinces)
        {
            int minLandTerritories = CalibrationManager.FactionCalibration.Data.multiProvincialStateMinLandTerritories;
            if (minLandTerritories != -1 && numLandTerritories < minLandTerritories)
                return false;

            int maxLandTerritories = CalibrationManager.FactionCalibration.Data.multiProvincialStateMaxLandTerritories;
            if (maxLandTerritories != -1 && numLandTerritories > maxLandTerritories)
                return false;

            int minLandProvinces = CalibrationManager.FactionCalibration.Data.multiProvincialStateMinLandProvinces;
            if (minLandProvinces != -1 && numLandProvinces < minLandProvinces)
                return false;

            int maxLandProvinces = CalibrationManager.FactionCalibration.Data.multiProvincialStateMaxLandProvinces;
            if (maxLandProvinces != -1 && numLandProvinces < maxLandProvinces)
                return false;

            return true;
        }
        public eFactionSpecialRating CalculateFactionSpecialRating(Faction faction)
        {
            if (CalibrationManager.FactionCalibration.Data.hegemonPossibleSizes.Contains(faction.Variables.Size.ToString()))
            {
                int minCityStateClients = CalibrationManager.FactionCalibration.Data.hegemonMinCityStateClients;
                int numCityStateClients = faction.Variables.DiplomaticContractSet.GetClientMasteryContracts().FindAll(x => x.TargetFaction.Variables.Size == eFactionSize.CITY_STATE).Count;
                if (minCityStateClients == -1 || numCityStateClients >= minCityStateClients)
                    return eFactionSpecialRating.HEGEMON;
            }

            if (CalibrationManager.FactionCalibration.Data.republicPossibleSizes.Contains(faction.Variables.Size.ToString()) && faction.Variables.Government.Group == eGovernmentGroup.REPUBLIC)
                return eFactionSpecialRating.REPUBLIC;

            if (CalibrationManager.FactionCalibration.Data.kingdomPossibleSizes.Contains(faction.Variables.Size.ToString()) && faction.Variables.Government.Group == eGovernmentGroup.MONARCHY)
                return eFactionSpecialRating.KINGDOM;

            if (CalibrationManager.FactionCalibration.Data.empirePossibleSizes.Contains(faction.Variables.Size.ToString()) && faction.Variables.Government.Group == eGovernmentGroup.MONARCHY)
                return eFactionSpecialRating.EMPIRE;

            if (CalibrationManager.FactionCalibration.Data.coloniesPossibleSizes.Contains(faction.Variables.Size.ToString())
                && faction.Variables.DiplomaticContractSet.GetColonySubjectContract() != null)
                return eFactionSpecialRating.COLONIES;

            if (CalibrationManager.FactionCalibration.Data.confederationPossibleSizes.Contains(faction.Variables.Size.ToString())
                && faction.Variables.Government.Group == eGovernmentGroup.CONFEDERATION)
                return eFactionSpecialRating.CONFEDERATION;

            if (CalibrationManager.FactionCalibration.Data.leaguePossibleSizes.Contains(faction.Variables.Size.ToString()) && faction.Variables.Government.Group == eGovernmentGroup.CONFEDERATION)
                return eFactionSpecialRating.LEAGUE;

            if (CalibrationManager.FactionCalibration.Data.hordePossibleSizes.Contains(faction.Variables.Size.ToString()) && CalibrationManager.FactionCalibration.Data.hordePossibleCultureFamilyIDs.Contains(faction.Variables.Culture.CultureGroup.CultureFamily.ID))
                return eFactionSpecialRating.HORDE;

            return eFactionSpecialRating.NONE;
        }

        public Statue GetPSMouseOverStatue(Point psPos, ViewManager vm)
        {
            foreach (Faction f in factions)
            {
                foreach (Statue s in f.Variables.StatueSet.Statues)
                {
                    if (!vm.ViewMode.StatueIsSelectable(s))
                        continue;

                    if (s.Position.IsInSettlement)
                        continue;

                    if (s.Rectangle.Contains(psPos))
                        return s;
                }
            }

            return null;
        }

        public List<Force> GetForcesInLandTerritory(LandTerritory lt)
        {
            List<Force> forces = new List<Force>();

            foreach (Faction f in factions)
            {
                foreach (Statue s in f.Variables.StatueSet.Statues)
                {
                    if (s is Force && s.Position.IsInLandTerritory && s.Position.LandTerritory == lt)
                        forces.Add(s as Force);
                }
            }

            return forces;
        }
        public List<Force> GetForcesInSeaTerritory(SeaTerritory st)
        {
            List<Force> forces = new List<Force>();

            foreach (Faction f in factions)
            {
                foreach (Statue s in f.Variables.StatueSet.Statues)
                {
                    if (s is Force && s.Position.IsInSeaTerritory && s.Position.SeaTerritory == st)
                        forces.Add(s as Force);
                }
            }

            return forces;
        }
    }
}
