﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class Faction
    {
        /* -------- Properties -------- */
        // Data
        public string ID { get; private set; }
        // Variables
        public FactionVariables Variables { get; private set; }
        // Properties
        public FactionController Controller { get; private set; }
        public bool IsPlayerControlled { get { return (Controller is PlayerFactionController); } }

        /* -------- Constructors -------- */
        public Faction()
        {
            Variables = new FactionVariables(this);
        }

        /* -------- Public Methods -------- */
        public void Update(GraphicsComponent g, CameraManager cm)
        {
            Variables.Update(g, cm);
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            set.Merge(Variables.GetModifierSet(gameManagers));

            return set;
        }

        public int CalulateNumberLandTerritoriesOwned(bool includeClients, bool includeColonies)
        {
            int landTerritoriesOwned = 0;

            foreach (LandTerritoryControl ltc in Variables.LandTerritoryControls)
                if (GameUtilities.LandTerritoryControlLevelIsOwnership(ltc.CurrControlLevel))
                    landTerritoriesOwned++;

            if (includeClients)
            {
                foreach (DiplomaticContract dc in Variables.DiplomaticContractSet.GetClientMasteryContracts())
                    landTerritoriesOwned += dc.TargetFaction.CalulateNumberLandTerritoriesOwned(false, false);
            }

            if (includeColonies)
            {
                foreach (DiplomaticContract dc in Variables.DiplomaticContractSet.GetColonyMasteryContracts())
                    landTerritoriesOwned += dc.TargetFaction.CalulateNumberLandTerritoriesOwned(false, false);
            }

            return landTerritoriesOwned;
        }
        public int CalculateNumberLandProvincesStaked(bool includeClients, bool includeColonies)
        {
            List<string> landProvinceStakeIDs = new List<string>();

            foreach (LandProvinceStake lps in Variables.LandProvinceStakes)
                landProvinceStakeIDs.Add(lps.LandProvince.ID);

            if (includeClients)
            {
                foreach (DiplomaticContract dc in Variables.DiplomaticContractSet.GetClientMasteryContracts())
                    foreach (LandProvinceStake lps in dc.TargetFaction.Variables.LandProvinceStakes)
                        if (!landProvinceStakeIDs.Contains(lps.LandProvince.ID))
                            landProvinceStakeIDs.Add(lps.LandProvince.ID);
            }

            if (includeColonies)
            {
                foreach (DiplomaticContract dc in Variables.DiplomaticContractSet.GetColonyMasteryContracts())
                    foreach (LandProvinceStake lps in dc.TargetFaction.Variables.LandProvinceStakes)
                        if (!landProvinceStakeIDs.Contains(lps.LandProvince.ID))
                            landProvinceStakeIDs.Add(lps.LandProvince.ID);
            }

            return landProvinceStakeIDs.Count;
        }

        public string GetStandardTextureID()
        {
            // TODO
            return "debug-standard";
        }

        public StatueStyle GetStatueStyle()
        {
            if (Variables.MilitaryEra != null)
                return Variables.MilitaryEra.StatueStyle;

            return Variables.Culture.StatueStyle;
        }

        public LandTerritoryControl GetLandTerritoryControl(LandTerritory lt)
        {
            foreach (LandTerritoryControl ltc in Variables.LandTerritoryControls)
            {
                if (ltc.LandTerritory == lt)
                    return ltc;
            }

            return null;
        }
        public eLandTerritoryControlLevel GetLandTerritoryControlLevel(LandTerritory lt)
        {
            foreach (LandTerritoryControl ltc in Variables.LandTerritoryControls)
            {
                if (ltc.LandTerritory == lt)
                    return ltc.CurrControlLevel;
            }

            return eLandTerritoryControlLevel.NO_CONTROL;
        }
        public eLandTerritoryControlLevel GetLandTerritoryControlUpgradedToFromOccupation(LandTerritory lt)
        {
            // TODO

            LandTerritoryControl control = GetLandTerritoryControl(lt);

            if (control == null)
                return eLandTerritoryControlLevel.MILITARY_OWNERSHIP;
            else
                return eLandTerritoryControlLevel.MILITARY_OWNERSHIP;
        }

        public bool CanTradeWithFaction(Faction f)
        {
            if (this == f)
                return true;

            return Variables.DiplomaticContractSet.HasTradeAgreementContractWithFaction(f);
        }

        public string GetDisplayName()
        {
            if (Variables.SpecialRating == eFactionSpecialRating.NONE)
            {
                string locID = "text_faction_" + ID + "_name";
                return Localization.Localize(locID);
            }

            string specialRatingTitle = GetSpecialRatingTitle();
            string adjectiveID = "text_faction_" + ID + "_adjective";
            string adjective = Localization.Localize(adjectiveID);
            string displayName = string.Format(specialRatingTitle, adjective);
            return displayName;
        }
        public string GetNameAdjective()
        {
            string adjectiveID = "text_faction_" + ID + "_adjective";
            return Localization.Localize(adjectiveID);
        }
        public string GetSpecialRatingTitle()
        {
            switch (Variables.SpecialRating)
            {
                case eFactionSpecialRating.HEGEMON:
                    return Localization.Localize("text_faction-special-rating_hegemon_title");
                case eFactionSpecialRating.REPUBLIC:
                    return Localization.Localize("text_faction-special-rating_republic_title");
                case eFactionSpecialRating.KINGDOM:
                    return Localization.Localize("text_faction-special-rating_kingdom_title");
                case eFactionSpecialRating.EMPIRE:
                    return Localization.Localize("text_faction-special-rating_empire_title");
                case eFactionSpecialRating.COLONIES:
                    return Localization.Localize("text_faction-special-rating_colonies_title");
                case eFactionSpecialRating.CONFEDERATION:
                    return Localization.Localize("text_faction-special-rating_confederation_title");
                case eFactionSpecialRating.LEAGUE:
                    return Localization.Localize("text_faction-special-rating_league_title");
                case eFactionSpecialRating.HORDE:
                    return Localization.Localize("text_faction-special-rating_horde_title");
                default:
                    Debug.LogUnrecognizedValueWarning(Variables.SpecialRating);
                    return "{0}";
            }
        }
        public string GetSpecialRatingName()
        {
            switch (Variables.SpecialRating)
            {
                case eFactionSpecialRating.HEGEMON:
                    return Localization.Localize("text_faction-special-rating_hegemon_name");
                case eFactionSpecialRating.REPUBLIC:
                    return Localization.Localize("text_faction-special-rating_republic_name");
                case eFactionSpecialRating.KINGDOM:
                    return Localization.Localize("text_faction-special-rating_kingdom_name");
                case eFactionSpecialRating.EMPIRE:
                    return Localization.Localize("text_faction-special-rating_empire_name");
                case eFactionSpecialRating.COLONIES:
                    return Localization.Localize("text_faction-special-rating_colonies_name");
                case eFactionSpecialRating.CONFEDERATION:
                    return Localization.Localize("text_faction-special-rating_confederation_name");
                case eFactionSpecialRating.LEAGUE:
                    return Localization.Localize("text_faction-special-rating_league_name");
                case eFactionSpecialRating.HORDE:
                    return Localization.Localize("text_faction-special-rating_horde_name");
                case eFactionSpecialRating.NONE:
                    return Localization.Localize("text_faction-special-rating_none_name");
                default:
                    Debug.LogUnrecognizedValueWarning(Variables.SpecialRating);
                    return "{0}";
            }
        }
        public string GetSizeName()
        {
            switch (Variables.Size)
            {
                case eFactionSize.CITY_STATE:
                    return Localization.Localize("text_faction-size_city-state");
                case eFactionSize.TRIBAL_STATE:
                    return Localization.Localize("text_faction-size_tribal-state");
                case eFactionSize.SMALL_STATE:
                    return Localization.Localize("text_faction-size_small-state");
                case eFactionSize.STATE:
                    return Localization.Localize("text_faction-size_state");
                case eFactionSize.PROVINCIAL_STATE:
                    return Localization.Localize("text_faction-size_provincial-state");
                case eFactionSize.MULTI_PROVINCIAL_STATE:
                    return Localization.Localize("text_faction-size_multi-provincial-state");
                default:
                    Debug.LogUnrecognizedValueWarning(Variables.Size);
                    return "";
            }
        }

        public bool TryChargeCost(Cost cost)
        {
            if (cost.MoneyCost > Variables.Treasury.Treasury || cost.AdminPowerCost > Variables.AdminPower.AdminPower)
                return false;

            Variables.Treasury.ChargeTreasury(cost.MoneyCost);
            Variables.AdminPower.ChargeAdminPower(cost.AdminPowerCost);
            return true;
        }

        /* -------- Creation Methods -------- */
        public static Faction CreateFromData(FactionData data, PlayerInfo playerInfo)
        {
            Faction faction = new Faction();

            faction.ID = data.id;
            if (playerInfo.PlayerFactionIDs.Contains(faction.ID))
                faction.Controller = new PlayerFactionController();
            else
                faction.Controller = new AIFactionController();
            
            return faction;
        }
        public void SetState(FactionStateData data, GameManagerContainer gameManagers, ContentManager content)
        {
            Variables = FactionVariables.CreateFromData(this, data, gameManagers, content);
        }
    }
}
