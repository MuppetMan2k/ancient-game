﻿using System.Collections.Generic;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eBattleType
    {
        LAND_BATTLE,
        NAVAL_BATTLE,
        SIEGE_BATTLE
    }

    class Battle
    {
        /* -------- Properties -------- */
        public eBattleType Type { get; private set; }
        public BattleLocation Location { get; private set; }
        public Force PrimaryAttacker { get; private set; }
        public List<Force> Attackers { get; private set; }
        public Force PrimaryDefender { get; private set; }
        public List<Force> Defenders { get; private set; }

        /* -------- Constructors -------- */
        public Battle(eBattleType inType, BattleLocation inLocation, Force inPrimaryAttacker, List<Force> inDefenders)
        {
            Type = inType;
            Location = inLocation;
            Attackers = new List<Force>();
            PrimaryAttacker = inPrimaryAttacker;
            Attackers.Add(inPrimaryAttacker);
            Defenders = inDefenders;
            ChoosePrimaryDefender();
        }

        /* -------- Private Methods --------- */
        private void ChoosePrimaryDefender()
        {
            PrimaryDefender = Defenders[0];

            for (int i = 1; i < Defenders.Count; i++)
                if (Defenders[i].ForceRating > PrimaryDefender.ForceRating)
                    PrimaryDefender = Defenders[i];
        }
    }
}
