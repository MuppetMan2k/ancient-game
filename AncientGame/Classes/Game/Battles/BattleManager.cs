﻿using System.Collections.Generic;

namespace AncientGame
{
    class BattleManager
    {
        /* -------- Private Fields -------- */
        private List<Battle> battles;

        /* -------- Constructors -------- */
        public BattleManager()
        {
            battles = new List<Battle>();
        }

        /* -------- Public Methods -------- */
        public bool ForceIsInvolvedInBattle(Force force)
        {
            if (force == null)
                return false;

            foreach (Battle b in battles)
            {
                if (b.Attackers.Contains(force))
                    return true;

                if (b.Defenders.Contains(force))
                    return true;
            }

            return false;
        }

        public void AddForceToBattle(Force attacker, List<Force> defenders, BattleLocation location)
        {
            foreach (Battle b in battles)
            {
                if (b.Location.IsEquivalent(location))
                {
                    // Battle already exists there, add attacker as reinforcement
                    b.Attackers.Add(attacker);
                    return;
                }
            }

            // Battle doesn't exist yet, so create it
            Battle newBattle = new Battle(GetBattleTypeFromLocation(location), location, attacker, defenders);
            battles.Add(newBattle);
        }

        /* -------- Private Methods --------- */
        private eBattleType GetBattleTypeFromLocation(BattleLocation location)
        {
            if (location.LandTerritory != null)
                return eBattleType.LAND_BATTLE;

            if (location.SeaTerritory != null)
                return eBattleType.NAVAL_BATTLE;

            if (location.Settlement != null)
                return eBattleType.SIEGE_BATTLE;

            Debug.LogWarning("Cannot resolve battle type from battle location: {0}", location.ToString());
            return eBattleType.LAND_BATTLE;
        }
    }
}
