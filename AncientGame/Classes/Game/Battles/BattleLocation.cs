﻿namespace AncientGame
{
    class BattleLocation
    {
        /* -------- Properties -------- */
        public LandTerritory LandTerritory { get; private set; }
        public SeaTerritory SeaTerritory { get; private set; }
        public Settlement Settlement { get; private set; }

        /* -------- Constructors -------- */
        public BattleLocation(LandTerritory inLandTerritory, SeaTerritory inSeaTerritory, Settlement inSettlement)
        {
            LandTerritory = inLandTerritory;
            SeaTerritory = inSeaTerritory;
            Settlement = inSettlement;
        }

        /* -------- Public Methods -------- */
        public bool IsEquivalent(BattleLocation location)
        {
            if (!Equals(LandTerritory, location.LandTerritory))
                return false;

            if (!Equals(SeaTerritory, location.SeaTerritory))
                return false;

            if (!Equals(Settlement, location.Settlement))
                return false;

            return true;
        }

        /* -------- Static Methods -------- */
        public static BattleLocation CreateFromLandTerritory(LandTerritory inLandTerritory)
        {
            return new BattleLocation(inLandTerritory, null, null);
        }
        public static BattleLocation CreateFromSeaTerritory(SeaTerritory inSeaTerritory)
        {
            return new BattleLocation(null, inSeaTerritory, null);
        }
        public static BattleLocation CreateFromSettlement(Settlement inSettlement)
        {
            return new BattleLocation(null, null, inSettlement);
        }
    }
}
