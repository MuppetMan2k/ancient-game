﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    abstract class ViewMode
    {
        /* -------- Private Fields -------- */
        protected eViewLevel viewLevel = eViewLevel.PROVINCE_LEVEL;
        protected Vector2 seaTerrainBumpMap1Offset;
        protected Vector2 seaTerrainBumpMap2Offset;
        protected float riverBumpMapYOffset;

        /* -------- Constructors -------- */
        public ViewMode()
        {
            seaTerrainBumpMap1Offset = new Vector2(0f, 0f);
            seaTerrainBumpMap2Offset = new Vector2(0f, 0f);
            riverBumpMapYOffset = 0f;
        }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw)
        {
            UpdateSeaTerrainOffsets(gtw);
            UpdateRiverOffset(gtw);
        }

        public void OnZoomFactorChanged(float newZoomFactor)
        {
            if (newZoomFactor >= CalibrationManager.CameraCalibration.Data.zoomFactorForTerritoryLevel)
            {
                viewLevel = eViewLevel.TERRITORY_LEVEL;
                return;
            }

            if (newZoomFactor >= CalibrationManager.CameraCalibration.Data.zoomFactorForRegionLevel)
            {
                viewLevel = eViewLevel.REGION_LEVEL;
                return;
            }

            viewLevel = eViewLevel.PROVINCE_LEVEL;
        }

        public abstract GraphicsStyle GetLandTerrainMetaStyle();
        public abstract SeaTerrainMetaStyle GetSeaTerrainMetaStyle();

        public abstract GraphicsStyle GetLakeMetaStyle();

        public abstract RiverMetaStyle GetRiverMetaStyle();

        public abstract GraphicsStyle GetSmallIslandMetaStyle();

        public abstract GraphicsStyle GetSettlementMetaStyle();
        public abstract GraphicsStyle GetSettlementDrawStyle(LandTerritory lt);

        public abstract GraphicsStyle GetRoadMetaStyle();
        public abstract GraphicsStyle GetRoadDrawStyle(LandTerritory lt);

        public abstract GraphicsStyle GetSeaTradeRouteMetaStyle();
        public abstract GraphicsStyle GetSeaTradeRouteDrawStyle(SeaTradeRoute str);

        public abstract GraphicsStyle GetTradeVehicleMetaStyle();
        public abstract GraphicsStyle GetTradeVehicleDrawStyle(TradeRoute tr);

        public abstract OverlayMetaStyle GetOverlayMetaStyle();
        public abstract GraphicsStyle GetLandTerritoryOverlayTypeStyle();
        public abstract OverlayDrawStyle GetLandTerritoryOverlayDrawStyle(LandTerritory lt);
        public abstract GraphicsStyle GetLandRegionOverlayTypeStyle();
        public abstract OverlayDrawStyle GetLandRegionOverlayDrawStyle(LandRegion lr);
        public abstract GraphicsStyle GetLandProvinceOverlayTypeStyle();
        public abstract OverlayDrawStyle GetLandProvinceOverlayDrawStyle(LandProvince lp);
        public abstract GraphicsStyle GetSeaTerritoryOverlayTypeStyle();
        public abstract OverlayDrawStyle GetSeaTerritoryOverlayDrawStyle(SeaTerritory st);
        public abstract GraphicsStyle GetSeaRegionOverlayTypeStyle();
        public abstract OverlayDrawStyle GetSeaRegionOverlayDrawStyle(SeaRegion sr);

        public abstract GraphicsStyle GetBorderMetaStyle();
        public abstract GraphicsStyle GetLandTerritoryBorderTypeStyle();
        public abstract BorderDrawStyle GetLandTerritoryBorderDrawStyle(LandTerritory lt);
        public abstract GraphicsStyle GetLandRegionBorderTypeStyle();
        public abstract BorderDrawStyle GetLandRegionBorderDrawStyle(LandRegion lr);
        public abstract GraphicsStyle GetLandProvinceBorderTypeStyle();
        public abstract BorderDrawStyle GetLandProvinceBorderDrawStyle(LandProvince lp);
        public abstract GraphicsStyle GetSeaTerritoryBorderTypeStyle();
        public abstract BorderDrawStyle GetSeaTerritoryBorderDrawStyle(SeaTerritory st);
        public abstract GraphicsStyle GetSeaRegionBorderTypeStyle();
        public abstract BorderDrawStyle GetSeaRegionBorderDrawStyle(SeaRegion sr);

        public abstract GraphicsStyle GetNameLabelMetaStyle();
        public abstract GraphicsStyle GetLandRegionNameLabelTypeStyle();
        public abstract GraphicsStyle GetLandRegionNameLabelDrawStyle(LandRegion lr);
        public abstract GraphicsStyle GetLandProvinceNameLabelTypeStyle();
        public abstract GraphicsStyle GetLandProvinceNameLabelDrawStyle(LandProvince lp);
        public abstract GraphicsStyle GetSeaRegionNameLabelTypeStyle();
        public abstract GraphicsStyle GetSeaRegionNameLabelDrawStyle(SeaRegion sr);

        public abstract GraphicsStyle GetStatueMetaStyle();
        public abstract GraphicsStyle GetStatueDrawStyle(Statue statue);

        public abstract GraphicsStyle GetSettlementLabelMetaStyle();
        public abstract GraphicsStyle GetSettlementLabelDrawStyle(LandTerritory lt);

        public abstract bool LandTerritoriesAreSelectable();
        public abstract bool LandRegionsAreSelectable();
        public abstract bool LandProvincesAreSelectable();
        public abstract bool SeaTerritoriesAreSelectable();
        public abstract bool SeaRegionsAreSelectable();
        public abstract bool SmallIslandsAreSelectable();
        public abstract bool LakesAreSelectable();
        public abstract bool RiversAreSelectable();
        public abstract bool LandTradeRoutesAreSelectable();
        public abstract bool SeaTradeRoutesAreSelectable();
        public abstract bool StatuesAreSelectable();
        public abstract bool StatueIsSelectable(Statue s);
        public abstract bool SettlementsAreSelectable();

        /* -------- Private Methods --------- */
        private void UpdateSeaTerrainOffsets(GameTimeWrapper gtw)
        {
            seaTerrainBumpMap1Offset += CalibrationManager.GraphicsCalibration.Data.seaTerrainBumpMap1Velocity * gtw.ElapsedSeconds;
            seaTerrainBumpMap2Offset += CalibrationManager.GraphicsCalibration.Data.seaTerrainBumpMap2Velocity * gtw.ElapsedSeconds;
        }
        private void UpdateRiverOffset(GameTimeWrapper gtw)
        {
            riverBumpMapYOffset -= CalibrationManager.GraphicsCalibration.Data.riverBumpMapSpeed * gtw.ElapsedSeconds;
        }
    }
}
