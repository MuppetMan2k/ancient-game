﻿using System;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class FactionOwnershipViewMode : ViewMode
    {
        /* -------- Public Methods -------- */
        public override GraphicsStyle GetLandTerrainMetaStyle()
        {
            return new GraphicsStyle(true);
        }
        public override SeaTerrainMetaStyle GetSeaTerrainMetaStyle()
        {
            string textureID = CalibrationManager.GraphicsCalibration.Data.seaTerrainTextureID;
            float textureScale = CalibrationManager.GraphicsCalibration.Data.seaTerrainTextureScale;
            string bumpMap1ID = CalibrationManager.GraphicsCalibration.Data.seaTerrainBumpMap1ID;
            float bumpMap1Scale = CalibrationManager.GraphicsCalibration.Data.seaTerrainBumpMap1Scale;
            string bumpMap2ID = CalibrationManager.GraphicsCalibration.Data.seaTerrainBumpMap2ID;
            float bumpMap2Scale = CalibrationManager.GraphicsCalibration.Data.seaTerrainBumpMap2Scale;

            return new SeaTerrainMetaStyle(true, seaTerrainBumpMap1Offset, seaTerrainBumpMap2Offset);
        }

        public override GraphicsStyle GetLakeMetaStyle()
        {
            return new GraphicsStyle(true);
        }

        public override RiverMetaStyle GetRiverMetaStyle()
        {
            return new RiverMetaStyle(true, riverBumpMapYOffset);
        }

        public override GraphicsStyle GetSmallIslandMetaStyle()
        {
            return new GraphicsStyle(true);
        }

        public override GraphicsStyle GetSettlementMetaStyle()
        {
            return GraphicsStyle.none;
        }
        public override GraphicsStyle GetSettlementDrawStyle(LandTerritory lt)
        {
            return GraphicsStyle.none;
        }

        public override GraphicsStyle GetRoadMetaStyle()
        {
            return GraphicsStyle.none;
        }
        public override GraphicsStyle GetRoadDrawStyle(LandTerritory lt)
        {
            return GraphicsStyle.none;
        }

        public override GraphicsStyle GetSeaTradeRouteMetaStyle()
        {
            return GraphicsStyle.none;
        }
        public override GraphicsStyle GetSeaTradeRouteDrawStyle(SeaTradeRoute str)
        {
            return GraphicsStyle.none;
        }

        public override GraphicsStyle GetTradeVehicleMetaStyle()
        {
            return GraphicsStyle.none;
        }
        public override GraphicsStyle GetTradeVehicleDrawStyle(TradeRoute tr)
        {
            return GraphicsStyle.none;
        }

        public override OverlayMetaStyle GetOverlayMetaStyle()
        {
            float overlayAlpha = CalibrationManager.GraphicsCalibration.Data.factionOwnershipOverlayAlpha;
            return new OverlayMetaStyle(true, "DoubleColorBands", overlayAlpha);
        }
        public override GraphicsStyle GetLandTerritoryOverlayTypeStyle()
        {
            return new GraphicsStyle(true);
        }
        public override OverlayDrawStyle GetLandTerritoryOverlayDrawStyle(LandTerritory lt)
        {
            // TODO - test if land territory is visible/in fog of war

            Color color1 = lt.Variables.OwningFaction.Variables.PrimaryColor;
            Color color2;
            if (lt.Variables.OccupyingFaction != null)
                color2 = lt.Variables.OccupyingFaction.Variables.PrimaryColor;
            else
                color2 = lt.Variables.OwningFaction.Variables.PrimaryColor;

            return new OverlayDrawStyle(true, color1, color2);
        }
        public override GraphicsStyle GetLandRegionOverlayTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override OverlayDrawStyle GetLandRegionOverlayDrawStyle(LandRegion lr)
        {
            return OverlayDrawStyle.none;
        }
        public override GraphicsStyle GetLandProvinceOverlayTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override OverlayDrawStyle GetLandProvinceOverlayDrawStyle(LandProvince lp)
        {
            return OverlayDrawStyle.none;
        }
        public override GraphicsStyle GetSeaTerritoryOverlayTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override OverlayDrawStyle GetSeaTerritoryOverlayDrawStyle(SeaTerritory st)
        {
            return OverlayDrawStyle.none;
        }
        public override GraphicsStyle GetSeaRegionOverlayTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override OverlayDrawStyle GetSeaRegionOverlayDrawStyle(SeaRegion sr)
        {
            return OverlayDrawStyle.none;
        }

        public override GraphicsStyle GetBorderMetaStyle()
        {
            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetLandTerritoryBorderTypeStyle()
        {
            return new GraphicsStyle(true);
        }
        public override BorderDrawStyle GetLandTerritoryBorderDrawStyle(LandTerritory lt)
        {
            // TODO - test if land territory is visible/in fog of war

            float thickness = CalibrationManager.GraphicsCalibration.Data.landTerritoryBorderThickness;
            Color color = lt.Variables.OwningFaction.Variables.PrimaryColor * CalibrationManager.GraphicsCalibration.Data.landTerritoryBorderAlpha;
            return new SingleBorderDrawStyle(true, thickness, color);
        }
        public override GraphicsStyle GetLandRegionBorderTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override BorderDrawStyle GetLandRegionBorderDrawStyle(LandRegion lr)
        {
            return SingleBorderDrawStyle.none;
        }
        public override GraphicsStyle GetLandProvinceBorderTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override BorderDrawStyle GetLandProvinceBorderDrawStyle(LandProvince lp)
        {
            return SingleBorderDrawStyle.none;
        }
        public override GraphicsStyle GetSeaTerritoryBorderTypeStyle()
        {
            return new GraphicsStyle(true);
        }
        public override BorderDrawStyle GetSeaTerritoryBorderDrawStyle(SeaTerritory st)
        {
            float thickness = CalibrationManager.GraphicsCalibration.Data.seaTerritoryBorderThickness;
            Color color = CalibrationManager.GraphicsCalibration.Data.seaTerritoryBorderColor * CalibrationManager.GraphicsCalibration.Data.seaTerritoryBorderAlpha;
            return new SingleBorderDrawStyle(true, thickness, color);
        }
        public override GraphicsStyle GetSeaRegionBorderTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override BorderDrawStyle GetSeaRegionBorderDrawStyle(SeaRegion sr)
        {
            return SingleBorderDrawStyle.none;
        }

        public override GraphicsStyle GetNameLabelMetaStyle()
        {
            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetLandRegionNameLabelTypeStyle()
        {
            if (viewLevel != eViewLevel.REGION_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetLandRegionNameLabelDrawStyle(LandRegion lr)
        {
            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetLandProvinceNameLabelTypeStyle()
        {
            if (viewLevel != eViewLevel.PROVINCE_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetLandProvinceNameLabelDrawStyle(LandProvince lp)
        {
            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetSeaRegionNameLabelTypeStyle()
        {
            if (viewLevel != eViewLevel.REGION_LEVEL && viewLevel != eViewLevel.PROVINCE_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetSeaRegionNameLabelDrawStyle(SeaRegion sr)
        {
            return new GraphicsStyle(true);
        }

        public override GraphicsStyle GetStatueMetaStyle()
        {
            return GraphicsStyle.none;
        }
        public override GraphicsStyle GetStatueDrawStyle(Statue statue)
        {
            return GraphicsStyle.none;
        }

        public override GraphicsStyle GetSettlementLabelMetaStyle()
        {
            if (viewLevel != eViewLevel.TERRITORY_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetSettlementLabelDrawStyle(LandTerritory lt)
        {
            return GraphicsStyle.none;
        }

        public override bool LandTerritoriesAreSelectable()
        {
            return true;
        }
        public override bool LandRegionsAreSelectable()
        {
            return false;
        }
        public override bool LandProvincesAreSelectable()
        {
            return false;
        }
        public override bool SeaTerritoriesAreSelectable()
        {
            return true;
        }
        public override bool SeaRegionsAreSelectable()
        {
            return false;
        }
        public override bool SmallIslandsAreSelectable()
        {
            return true;
        }
        public override bool LakesAreSelectable()
        {
            return true;
        }
        public override bool RiversAreSelectable()
        {
            return true;
        }
        public override bool LandTradeRoutesAreSelectable()
        {
            return false;
        }
        public override bool SeaTradeRoutesAreSelectable()
        {
            return false;
        }
        public override bool StatuesAreSelectable()
        {
            return false;
        }
        public override bool StatueIsSelectable(Statue s)
        {
            return false;
        }
        public override bool SettlementsAreSelectable()
        {
            return false;
        }
    }
}
