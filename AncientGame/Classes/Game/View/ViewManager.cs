﻿namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eViewLevel
    {
        TERRITORY_LEVEL,
        REGION_LEVEL,
        PROVINCE_LEVEL
    }

    class ViewManager
    {
        /* -------- Properties -------- */
        public ViewMode ViewMode { get; private set; }
        
        /* -------- Constructors -------- */
        public ViewManager()
        {
            ViewMode = new TerrainViewMode();
        }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw)
        {
            ViewMode.Update(gtw);
        }
    }
}
