﻿using System;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class TerrainViewMode : ViewMode
    {
        /* -------- Public Methods -------- */
        public override GraphicsStyle GetLandTerrainMetaStyle()
        {
            return new GraphicsStyle(true);
        }
        public override SeaTerrainMetaStyle GetSeaTerrainMetaStyle()
        {
            return new SeaTerrainMetaStyle(true, seaTerrainBumpMap1Offset, seaTerrainBumpMap2Offset);
        }

        public override GraphicsStyle GetLakeMetaStyle()
        {
            return new GraphicsStyle(true);
        }

        public override RiverMetaStyle GetRiverMetaStyle()
        {
            return new RiverMetaStyle(true, riverBumpMapYOffset);
        }

        public override GraphicsStyle GetSmallIslandMetaStyle()
        {
            return new GraphicsStyle(true);
        }

        public override GraphicsStyle GetSettlementMetaStyle()
        {
            if (viewLevel != eViewLevel.TERRITORY_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetSettlementDrawStyle(LandTerritory lt)
        {
            return new GraphicsStyle(true);
        }

        public override GraphicsStyle GetRoadMetaStyle()
        {
            if (viewLevel != eViewLevel.TERRITORY_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetRoadDrawStyle(LandTerritory lt)
        {
            return new GraphicsStyle(true);
        }

        public override GraphicsStyle GetSeaTradeRouteMetaStyle()
        {
            if (viewLevel != eViewLevel.TERRITORY_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetSeaTradeRouteDrawStyle(SeaTradeRoute str)
        {
            return new GraphicsStyle(true);
        }

        public override GraphicsStyle GetTradeVehicleMetaStyle()
        {
            if (viewLevel != eViewLevel.TERRITORY_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetTradeVehicleDrawStyle(TradeRoute tr)
        {
            return new GraphicsStyle(true);
        }

        public override OverlayMetaStyle GetOverlayMetaStyle()
        {
            return OverlayMetaStyle.none;
        }
        public override GraphicsStyle GetLandTerritoryOverlayTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override OverlayDrawStyle GetLandTerritoryOverlayDrawStyle(LandTerritory lt)
        {
            return OverlayDrawStyle.none;
        }
        public override GraphicsStyle GetLandRegionOverlayTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override OverlayDrawStyle GetLandRegionOverlayDrawStyle(LandRegion lr)
        {
            return OverlayDrawStyle.none;
        }
        public override GraphicsStyle GetLandProvinceOverlayTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override OverlayDrawStyle GetLandProvinceOverlayDrawStyle(LandProvince lp)
        {
            return OverlayDrawStyle.none;
        }
        public override GraphicsStyle GetSeaTerritoryOverlayTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override OverlayDrawStyle GetSeaTerritoryOverlayDrawStyle(SeaTerritory st)
        {
            return OverlayDrawStyle.none;
        }
        public override GraphicsStyle GetSeaRegionOverlayTypeStyle()
        {
            return GraphicsStyle.none;
        }
        public override OverlayDrawStyle GetSeaRegionOverlayDrawStyle(SeaRegion sr)
        {
            return OverlayDrawStyle.none;
        }

        public override GraphicsStyle GetBorderMetaStyle()
        {
            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetLandTerritoryBorderTypeStyle()
        {
            if (viewLevel != eViewLevel.TERRITORY_LEVEL)
                return GraphicsStyle.none;

            // TODO - test if land territory is visible/in fog of war

            return new GraphicsStyle(true);
        }
        public override BorderDrawStyle GetLandTerritoryBorderDrawStyle(LandTerritory lt)
        {
            if (viewLevel != eViewLevel.TERRITORY_LEVEL)
                return SingleBorderDrawStyle.none;

            // TODO - test if land territory is visible/in fog of war

            float outerThickness = CalibrationManager.GraphicsCalibration.Data.landTerritoryBorderOuterThickness;
            float innerThickness = CalibrationManager.GraphicsCalibration.Data.landTerritoryBorderInnerThickness;
            Color outerColor = lt.Variables.OwningFaction.Variables.PrimaryColor * CalibrationManager.GraphicsCalibration.Data.landTerritoryBorderAlpha;
            Color innerColor = lt.Variables.OwningFaction.Variables.SecondaryColor;
            return new DoubleBorderDrawStyle(true, outerThickness, innerThickness, outerColor, innerColor);
        }
        public override GraphicsStyle GetLandRegionBorderTypeStyle()
        {
            if (viewLevel != eViewLevel.REGION_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override BorderDrawStyle GetLandRegionBorderDrawStyle(LandRegion lr)
        {
            float thickness = CalibrationManager.GraphicsCalibration.Data.landRegionBorderThickness;
            Color color = CalibrationManager.GraphicsCalibration.Data.landRegionBorderColor * CalibrationManager.GraphicsCalibration.Data.landRegionBorderAlpha;
            return new SingleBorderDrawStyle(true, thickness, color);
        }
        public override GraphicsStyle GetLandProvinceBorderTypeStyle()
        {
            if (viewLevel != eViewLevel.PROVINCE_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override BorderDrawStyle GetLandProvinceBorderDrawStyle(LandProvince lp)
        {
            float thickness = CalibrationManager.GraphicsCalibration.Data.landProvinceBorderThickness;
            Color color = CalibrationManager.GraphicsCalibration.Data.landProvinceBorderColor * CalibrationManager.GraphicsCalibration.Data.landProvinceBorderAlpha;
            return new SingleBorderDrawStyle(true, thickness, color);
        }
        public override GraphicsStyle GetSeaTerritoryBorderTypeStyle()
        {
            return new GraphicsStyle(true);
        }
        public override BorderDrawStyle GetSeaTerritoryBorderDrawStyle(SeaTerritory st)
        {
            if (viewLevel != eViewLevel.TERRITORY_LEVEL)
                return SingleBorderDrawStyle.none;

            float thickness = CalibrationManager.GraphicsCalibration.Data.seaTerritoryBorderThickness;
            Color color = CalibrationManager.GraphicsCalibration.Data.seaTerritoryBorderColor * CalibrationManager.GraphicsCalibration.Data.seaTerritoryBorderAlpha;
            return new SingleBorderDrawStyle(true, thickness, color);
        }
        public override GraphicsStyle GetSeaRegionBorderTypeStyle()
        {
            return new GraphicsStyle(true);
        }
        public override BorderDrawStyle GetSeaRegionBorderDrawStyle(SeaRegion sr)
        {
            if (viewLevel != eViewLevel.REGION_LEVEL && viewLevel != eViewLevel.PROVINCE_LEVEL)
                return SingleBorderDrawStyle.none;

            float thickness = CalibrationManager.GraphicsCalibration.Data.seaRegionBorderThickness;
            Color color = CalibrationManager.GraphicsCalibration.Data.seaRegionBorderColor * CalibrationManager.GraphicsCalibration.Data.seaRegionBorderAlpha;
            return new SingleBorderDrawStyle(true, thickness, color);
        }

        public override GraphicsStyle GetNameLabelMetaStyle()
        {
            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetLandRegionNameLabelTypeStyle()
        {
            if (viewLevel != eViewLevel.REGION_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetLandRegionNameLabelDrawStyle(LandRegion lr)
        {
            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetLandProvinceNameLabelTypeStyle()
        {
            if (viewLevel != eViewLevel.PROVINCE_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetLandProvinceNameLabelDrawStyle(LandProvince lp)
        {
            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetSeaRegionNameLabelTypeStyle()
        {
            if (viewLevel != eViewLevel.REGION_LEVEL && viewLevel != eViewLevel.PROVINCE_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetSeaRegionNameLabelDrawStyle(SeaRegion sr)
        {
            return new GraphicsStyle(true);
        }

        public override GraphicsStyle GetStatueMetaStyle()
        {
            if (viewLevel != eViewLevel.TERRITORY_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetStatueDrawStyle(Statue statue)
        {
            return new GraphicsStyle(true);
        }

        public override GraphicsStyle GetSettlementLabelMetaStyle()
        {
            if (viewLevel != eViewLevel.TERRITORY_LEVEL)
                return GraphicsStyle.none;

            return new GraphicsStyle(true);
        }
        public override GraphicsStyle GetSettlementLabelDrawStyle(LandTerritory lt)
        {
            return new GraphicsStyle(true);
        }

        public override bool LandTerritoriesAreSelectable()
        {
            return (viewLevel == eViewLevel.TERRITORY_LEVEL);
        }
        public override bool LandRegionsAreSelectable()
        {
            return (viewLevel == eViewLevel.REGION_LEVEL);
        }
        public override bool LandProvincesAreSelectable()
        {
            return (viewLevel == eViewLevel.PROVINCE_LEVEL);
        }
        public override bool SeaTerritoriesAreSelectable()
        {
            return (viewLevel == eViewLevel.TERRITORY_LEVEL);
        }
        public override bool SeaRegionsAreSelectable()
        {
            return (viewLevel == eViewLevel.REGION_LEVEL || viewLevel == eViewLevel.PROVINCE_LEVEL);
        }
        public override bool SmallIslandsAreSelectable()
        {
            return true;
        }
        public override bool LakesAreSelectable()
        {
            return true;
        }
        public override bool RiversAreSelectable()
        {
            return true;
        }
        public override bool LandTradeRoutesAreSelectable()
        {
            return (viewLevel == eViewLevel.TERRITORY_LEVEL);
        }
        public override bool SeaTradeRoutesAreSelectable()
        {
            return (viewLevel == eViewLevel.TERRITORY_LEVEL);
        }
        public override bool StatuesAreSelectable()
        {
            return (viewLevel == eViewLevel.TERRITORY_LEVEL);
        }
        public override bool StatueIsSelectable(Statue s)
        {
            return true;
        }
        public override bool SettlementsAreSelectable()
        {
            return (viewLevel == eViewLevel.TERRITORY_LEVEL);
        }
    }
}
