﻿using DataTypes;

namespace AncientGame
{
    abstract class Unit
    {
        /* -------- Properties -------- */
        public string TextureID { get { return unitData.texID; } }
        // Generic unit variables
        public int NumMen { get; private set; }
        public int ExperienceLevel { get; private set; }
        public int ExperiencePoints { get; private set; }

        /* -------- Private Fields -------- */
        protected UnitData unitData;
        // Constants
        protected const int MAX_EXPERIENCE_LEVEL = 9;

        /* -------- Constructors -------- */
        public Unit(UnitData inUnitData)
        {
            unitData = inUnitData;
        }
        
        /* -------- Public Methods -------- */
        public static int Sort(Unit unit1, Unit unit2)
        {
            LandUnit lu1 = unit1 as LandUnit;
            LandUnit lu2 = unit2 as LandUnit;
            SeaUnit su1 = unit1 as SeaUnit;
            SeaUnit su2 = unit2 as SeaUnit;

            // Sea units before land units
            if (su1 != null && lu2 != null)
                return 1;
            if (lu1 != null && su2 != null)
                return -1;

            // Same type
            if (lu1 != null && lu2 != null)
                return LandUnit.Sort(lu1, lu2);
            if (su1 != null && su2 != null)
                return SeaUnit.Sort(su1, su2);

            // Unknown
            return 0;
        }

        public static int GetMaxExperienceLevel()
        {
            return MAX_EXPERIENCE_LEVEL;
        }

        /* -------- Creation Methods -------- */
        public static Unit CreateFromData(UnitStateData data, GameManagerContainer gameManagers)
        {
            LandUnitStateData lusd = data as LandUnitStateData;
            if (lusd != null)
                return LandUnit.CreateFromData(lusd, gameManagers);

            SeaUnitStateData susd = data as SeaUnitStateData;
            if (susd != null)
                return SeaUnit.CreateFromData(susd, gameManagers);

            Debug.LogWarning("Tried to create a unit from state data but is was neither a land nor sea unit");
            return null;
        }
        public void SetState(UnitStateData data)
        {
            NumMen = data.numMen;
            ExperienceLevel = data.experienceLevel;
            ExperiencePoints = data.experiencePoints;
        }
    }
}
