﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eLandUnitImprovementType
    {
        ARMOR,
        SHIELDS,
        MELEE_WEAPONS,
        RANGED_WEAPONS,
        HORSES,
        CAMELS,
        ELEPHANTS,
        ENGINES
    }

    class LandUnitImprovement
    {
        /* -------- Properties -------- */
        public eLandUnitImprovementType Type { get; private set; }
        public int Level { get; private set; }

        /* -------- Private Fields -------- */
        // Constants
        private const int MAX_LEVEL = 3;

        /* -------- Public Methods -------- */
        public static int Sort(LandUnitImprovement lui1, LandUnitImprovement lui2)
        {
            int num1 = (int)lui1.Type;
            int num2 = (int)lui2.Type;

            return Mathf.Sign(num1 - num2);
        }

        /* -------- Creation Methods -------- */
        public static LandUnitImprovement CreateFromData(LandUnitImprovementData data)
        {
            LandUnitImprovement lui = new LandUnitImprovement();

            lui.Type = DataUtilities.ParseLandUnitImprovementType(data.type);
            lui.Level = Mathf.ClampUpper(data.level, MAX_LEVEL);

            return lui;
        }
    }
}
