﻿using System.Collections.Generic;
using UnitData = DataTypes.UnitData;

namespace AncientGame
{
    class UnitManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<string, UnitData> unitDatas;

        /* -------- Constructors -------- */
        public UnitManager()
        {
            unitDatas = new Dictionary<string, UnitData>();
        }

        /* -------- Public Methods -------- */
        public void AddUnit(UnitData unitData)
        {
            unitDatas.Add(unitData.id, unitData);
        }
        public UnitData GetUnitData(string id)
        {
            return unitDatas[id];
        }
    }
}
