﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eLandUnitType
    {
        MELEE_CAVALRY,
        SHOCK_CAVALRY,
        ARCHER_CAVALRY,
        JAVELIN_CAVALRY,
        MELEE_CAMELS,
        SHOCK_CAMELS,
        ARCHER_CAMELS,
        JAVELIN_CAMELS,
        ELEPHANTS,
        MELEE_INFANTRY,
        SPEAR_INFANTRY,
        PIKE_INFANTRY,
        ARCHER_INFANTRY,
        JAVELIN_INFANTRY,
        SLINGER_INFANTRY,
        SPECIAL_INFANTRY,
        SIEGE_ENGINE
    }

    enum eLandUnitWeight
    {
        SUPER_LIGHT,
        LIGHT,
        MEDIUM,
        HEAVY,
        SUPER_HEAVY
    }
    
    class LandUnit : Unit
    {
        /* -------- Properties -------- */
        // Data
        public LandUnitData LandUnitData { get { return unitData as LandUnitData; } }
        public eLandUnitType Type { get; private set; }
        public eLandUnitWeight Weight { get; private set; }
        // Land unit variables
        public List<LandUnitImprovement> Improvements { get; private set; }

        /* -------- Constructors -------- */
        public LandUnit(LandUnitData inLandUnitData)
            : base(inLandUnitData)
        {
            Type = DataUtilities.ParseLandUnitType(inLandUnitData.type);
            Weight = DataUtilities.ParseLandUnitWeight(inLandUnitData.weight);

            Improvements = new List<LandUnitImprovement>();
        }

        /* -------- Public Methods -------- */
        public static int Sort(LandUnit lu1, LandUnit lu2)
        {
            // Type
            int type1 = (int)lu1.Type;
            int type2 = (int)lu2.Type;
            if (type1 != type2)
                return Mathf.Sign(type1 - type2);

            // Weight
            int weight1 = (int)lu1.Weight;
            int weight2 = (int)lu2.Weight;
            if (weight1 != weight2)
                return Mathf.Sign(weight2 - weight1);

            // ID
            int idComparison = string.Compare(lu1.LandUnitData.id, lu2.LandUnitData.id, true);
            if (idComparison != 0)
                return idComparison;

            // Experience
            if (lu1.ExperienceLevel != lu2.ExperienceLevel)
                return Mathf.Sign(lu2.ExperienceLevel - lu1.ExperienceLevel);

            return 0;
        }

        /* -------- Private Methods -------- */
        private void SortImprovements()
        {
            Improvements.Sort(LandUnitImprovement.Sort);
        }

        /* -------- Creation Methods -------- */
        public static LandUnit CreateFromData(LandUnitStateData data, GameManagerContainer gameManagers)
        {
            LandUnitData lud = gameManagers.UnitManager.GetUnitData(data.unitID) as LandUnitData;

            if (lud == null)
                return null;

            LandUnit lu = new LandUnit(lud);
            lu.SetState(data);

            return lu;
        }
        public void SetState(LandUnitStateData data)
        {
            Improvements.Clear();
            foreach (LandUnitImprovementData luid in data.landUnitImprovementDatas)
                Improvements.Add(LandUnitImprovement.CreateFromData(luid));
            SortImprovements();

            base.SetState(data);
        }
    }
}
