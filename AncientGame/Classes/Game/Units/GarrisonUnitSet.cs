﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class GarrisonUnitSet
    {
        /* -------- Properties -------- */
        public List<Unit> Units { get; private set; }

        /* -------- Constructors -------- */
        public GarrisonUnitSet()
        {
            Units = new List<Unit>();
        }

        /* -------- Private Methods -------- */
        private void SortUnits()
        {
            Units.Sort(Unit.Sort);
        }

        /* -------- Creation Methods -------- */
        public static GarrisonUnitSet CreateFromData(UnitStateData[] unitStateDatas, GameManagerContainer gameManagers)
        {
            GarrisonUnitSet garrisonUnitSet = new GarrisonUnitSet();

            foreach (UnitStateData usd in unitStateDatas)
            {
                Unit unit = Unit.CreateFromData(usd, gameManagers);

                if (unit == null)
                    continue;

                garrisonUnitSet.Units.Add(unit);
            }
            garrisonUnitSet.SortUnits();

            return garrisonUnitSet;
        }
    }
}
