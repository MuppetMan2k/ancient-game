﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eSeaUnitImprovementType
    {
        ARMOR,
        SHIELDS,
        MELEE_WEAPONS,
        RANGED_WEAPONS,
        OARS,
        SAILS,
        HULLS,
        ENGINES
    }

    class SeaUnitImprovement
    {
        /* -------- Properties -------- */
        public eSeaUnitImprovementType Type { get; private set; }
        public int Level { get; private set; }

        /* -------- Private Fields -------- */
        // Constants
        private const int MAX_LEVEL = 3;

        /* -------- Public Methods -------- */
        public static int Sort(SeaUnitImprovement sui1, SeaUnitImprovement sui2)
        {
            int num1 = (int)sui1.Type;
            int num2 = (int)sui2.Type;

            return Mathf.Sign(num2 - num1);
        }

        /* -------- Creation Methods -------- */
        public static SeaUnitImprovement CreateFromData(SeaUnitImprovementData data)
        {
            SeaUnitImprovement sui = new SeaUnitImprovement();

            sui.Type = DataUtilities.ParseSeaUnitImprovementType(data.type);
            sui.Level = Mathf.ClampUpper(data.level, MAX_LEVEL);

            return sui;
        }
    }
}
