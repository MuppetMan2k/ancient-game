﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eSeaUnitType
    {
        SKIRMISH_SHIP,
        ASSAULT_SHIP,
        COMMAND_SHIP,
        SIEGE_SHIP,
        SPECIAL_SHIP
    }

    enum eSeaUnitSize
    {
        TINY,
        SMALL,
        MEDIUM,
        LARGE,
        HUGE
    }

    class SeaUnit : Unit
    {
        /* -------- Properties -------- */
        // Data
        public SeaUnitData SeaUnitData { get { return unitData as SeaUnitData; } }
        public eSeaUnitType Type { get; private set; }
        public eSeaUnitSize Size { get; private set; }
        // Sea unit variables
        public int HullHP { get; private set; }
        public int RiggingHP { get; private set; }
        public List<SeaUnitImprovement> Improvements { get; private set; }

        /* -------- Constructors -------- */
        public SeaUnit(SeaUnitData inSeaUnitData)
            : base(inSeaUnitData)
        {
            Type = DataUtilities.ParseSeaUnitType(inSeaUnitData.type);
            Size = DataUtilities.ParseSeaUnitSize(inSeaUnitData.size);

            Improvements = new List<SeaUnitImprovement>();
        }

        /* -------- Public Methods -------- */
        public static int Sort(SeaUnit su1, SeaUnit su2)
        {
            // Type
            int type1 = (int)su1.Type;
            int type2 = (int)su2.Type;
            if (type1 != type2)
                return Mathf.Sign(type1 - type2);

            // Size
            int size1 = (int)su1.Size;
            int size2 = (int)su2.Size;
            if (size1 != size2)
                return Mathf.Sign(size2 - size1);

            // ID
            int idComparison = string.Compare(su1.SeaUnitData.id, su2.SeaUnitData.id, true);
            if (idComparison != 0)
                return idComparison;

            // Experience
            if (su1.ExperienceLevel != su2.ExperienceLevel)
                return Mathf.Sign(su2.ExperienceLevel - su1.ExperienceLevel);

            return 0;
        }

        /* -------- Private Methods -------- */
        private void SortImprovements()
        {
            Improvements.Sort(SeaUnitImprovement.Sort);
        }

        /* -------- Creation Methods -------- */
        public static SeaUnit CreateFromData(SeaUnitStateData data, GameManagerContainer gameManagers)
        {
            SeaUnitData sud = gameManagers.UnitManager.GetUnitData(data.unitID) as SeaUnitData;

            if (sud == null)
                return null;

            SeaUnit su = new SeaUnit(sud);
            su.SetState(data);

            return su;
        }
        public void SetState(SeaUnitStateData data)
        {
            HullHP = data.hullHP;
            RiggingHP = data.riggingHP;
            Improvements.Clear();
            foreach (SeaUnitImprovementData suid in data.seaUnitImprovementDatas)
                Improvements.Add(SeaUnitImprovement.CreateFromData(suid));
            SortImprovements();

            base.SetState(data);
        }
    }
}
