﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eResourceTier
    {
        PRIMARY,
        SECONDARY
    }

    enum eResourceType
    {
        AGRICULTURE,
        FOOD,
        LIVESTOCK,
        MINERAL,
        ANIMAL,
        LUXURY,
        WEAPONRY,
        GENERIC
    }

    class Resource
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public string LocID { get; private set; }
        public string IconTextureID { get; private set; }
        public eResourceTier Tier { get; private set; }
        public eResourceType Type { get; private set; }
        public float BaseValue { get; private set; }
        public float Value { get; private set; }

        /* -------- Constructors -------- */
        public Resource()
        {
            Value = 0.0f;
        }

        /* -------- Public Methods -------- */
        public string GetName()
        {
            return Localization.Localize(LocID);
        }
        public void SetValue(float newValue)
        {
            Value = newValue;
        }

        /* -------- Static Methods -------- */
        public static Resource CreateFromData(ResourceData data)
        {
            Resource resource = new Resource();

            resource.ID = data.id;
            resource.LocID = data.locID;
            resource.IconTextureID = data.iconTextureID;
            resource.Tier = DataUtilities.ParseResourceTier(data.resourceTier);
            resource.Type = DataUtilities.ParseResourceType(data.resourceType);
            resource.BaseValue = data.baseValue;

            return resource;
        }

        public static string GetTypeName(eResourceType type)
        {
            switch (type)
            {
                case eResourceType.AGRICULTURE:
                    return Localization.Localize("text_resource-type_agriculture");
                case eResourceType.FOOD:
                    return Localization.Localize("text_resource-type_food");
                case eResourceType.LIVESTOCK:
                    return Localization.Localize("text_resource-type_livestock");
                case eResourceType.MINERAL:
                    return Localization.Localize("text_resource-type_mineral");
                case eResourceType.ANIMAL:
                    return Localization.Localize("text_resource-type_animal");
                case eResourceType.LUXURY:
                    return Localization.Localize("text_resource-type_luxury");
                case eResourceType.WEAPONRY:
                    return Localization.Localize("text_resource-type_weaponry");
                case eResourceType.GENERIC:
                    return Localization.Localize("text_resource-type_generic");
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return "";
            }
        }
    }
}
