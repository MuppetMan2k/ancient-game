﻿using DataTypes;

namespace AncientGame
{
    class ResourceMarketState
    {
        /* -------- Properties -------- */
        public Resource Resource { get; private set; }
        public float TotalSupply { get; set; }
        public float TotalDemand { get; set; }

        /* -------- Constructors -------- */
        public ResourceMarketState(Resource inResource, float inTotalSupply, float inTotalDemand)
        {
            Resource = inResource;
            TotalSupply = inTotalSupply;
            TotalDemand = inTotalDemand;
        }

        /* -------- Static Methods -------- */
        public static ResourceMarketState CreateFromData(ResourceMarketStateData data, Resource resource)
        {
            resource.SetValue(data.value);
            return new ResourceMarketState(resource, data.totalSupply, data.totalDemand);
        }
    }
}
