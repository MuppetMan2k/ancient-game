﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class ResourceManager
    {
        /* -------- Properties -------- */
        public MarketManager MarketManager { get; private set; }

        public List<eResourceType> ResourceTypesAffectedByFertility { get; private set; }
        public List<eResourceType> ResourceTypesAffectedByHarvest { get; private set; }
        public List<eResourceType> ResourceTypesThatCountAsFood { get; private set; }

        /* -------- Private Fields -------- */
        private Dictionary<string, Resource> resources;

        /* -------- Constructors -------- */
        public ResourceManager()
        {
            MarketManager = new MarketManager();

            resources = new Dictionary<string, Resource>();
            ResourceTypesAffectedByFertility = new List<eResourceType>();
            ResourceTypesAffectedByHarvest = new List<eResourceType>();
            ResourceTypesThatCountAsFood = new List<eResourceType>();
        }

        /* -------- Public Methods -------- */
        public void AddResource(Resource resource)
        {
            resources.Add(resource.ID, resource);
        }
        public Resource GetResource(string id)
        {
            if (!resources.ContainsKey(id))
                return null;

            return resources[id];
        }
        public Dictionary<string, Resource> GetAllResources()
        {
            return resources;
        }
        public List<eResourceType> GetResourceTypesThatCountAsFood()
        {
            return ResourceTypesThatCountAsFood;
        }

        public eStatConstituentType GetConstituentTypeForResourceType(eResourceType type)
        {
            switch (type)
            {
                case eResourceType.AGRICULTURE:
                    return eStatConstituentType.RESOURCE_AGRICULTURE_POS;
                case eResourceType.ANIMAL:
                    return eStatConstituentType.RESOURCE_ANIMAL_POS;
                case eResourceType.FOOD:
                    return eStatConstituentType.RESOURCE_FOOD_POS;
                case eResourceType.GENERIC:
                    return eStatConstituentType.RESOURCE_GENERIC_POS;
                case eResourceType.LIVESTOCK:
                    return eStatConstituentType.RESOURCE_LIVESTOCK_POS;
                case eResourceType.LUXURY:
                    return eStatConstituentType.RESOURCE_LUXURY_POS;
                case eResourceType.MINERAL:
                    return eStatConstituentType.RESOURCE_MINERAL_POS;
                case eResourceType.WEAPONRY:
                    return eStatConstituentType.RESOURCE_WEAPONRY_POS;
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return eStatConstituentType.LOCAL_TRADITIONS_POS;
            }
        }

        public ResourceProduction CreateLandTerritoryBaseResourceProduction(LandTerritory lt, ResourceProductionData data)
        {
            ResourceProduction production = ResourceProduction.CreateFromData(data, this);

            float multiplier = CalibrationManager.CampaignCalibration.Data.baseResourceProductionMultiplier + lt.TotalArea * CalibrationManager.CampaignCalibration.Data.baseResourceProductionMultiplierPerArea;
            multiplier /= CalibrationManager.CampaignCalibration.Data.turnsPerYear;

            int numFertilityPointsBelow10 = 10 - lt.ParentLandRegion.FertilityLevel;
            float fertilityMultiplier = CalibrationManager.CampaignCalibration.Data.baseResourceProductionBaseFertilityMultiplier;
            fertilityMultiplier *= Mathf.Pow(CalibrationManager.CampaignCalibration.Data.baseResourceProductionReducedFertilityMultiplier, numFertilityPointsBelow10);

            foreach (SingleResourcePresence srp in production.ResourceProductions)
            {
                float newQuantity = srp.Quantity;
                newQuantity *= multiplier;

                if (ResourceTypesAffectedByFertility.Contains(srp.Resource.Type))
                    newQuantity *= fertilityMultiplier;

                srp.SetQuantity(newQuantity);
            }

            return production;
        }
    }
}
