﻿using System.Collections.Generic;

namespace AncientGame
{
    class MarketManager
    {
        /* -------- Private Fields -------- */
        private List<ResourceMarketState> resourceMarketStates;

        /* -------- Constructors -------- */
        public MarketManager()
        {
            resourceMarketStates = new List<ResourceMarketState>();
        }

        /* -------- Public Methods -------- */
        public void AddResourceMarketState(ResourceMarketState rms)
        {
            resourceMarketStates.Add(rms);
        }
        public void EnsureAllResourceMarketStatesExist(GameManagerContainer gameManagers)
        {
            foreach (KeyValuePair<string, Resource> kvp in gameManagers.ResourceManager.GetAllResources())
            {
                Resource r = kvp.Value;

                if (resourceMarketStates.Find(x => x.Resource == r) != null)
                    continue;

                // Create a new resource market state with default values
                r.SetValue(r.BaseValue);
                ResourceMarketState rms = new ResourceMarketState(r, 0f, 0f);
                resourceMarketStates.Add(rms);
            }
        }

        public void RecalculateMarketVariables(GameManagerContainer gameManagers)
        {
            foreach (ResourceMarketState rms in resourceMarketStates)
            {
                // Recalculate total supply
                rms.TotalSupply = 0f;
                foreach (LandTerritory lt in gameManagers.MapManager.LandTerritoryManager.GetAllLandTerritories())
                {
                    SingleResourceSupply srs = lt.Variables.TradeSet.SupplySet.Find(x => x.Resource == rms.Resource);

                    if (srs == null)
                        continue;

                    rms.TotalSupply += srs.Quantity;
                }

                // Recalculate total demand
                rms.TotalDemand = 0f;
                foreach (LandTerritory lt in gameManagers.MapManager.LandTerritoryManager.GetAllLandTerritories())
                {
                    SingleResourceDemand srd = lt.Variables.TradeSet.DemandSet.Find(x => x.Resource == rms.Resource);

                    if (srd == null)
                        continue;

                    rms.TotalDemand += srd.Quantity;
                }
            }
        }

        public void RecalculateResourceValues(GameManagerContainer gameManagers)
        {
            RecalculateMarketVariables(gameManagers);

            // TODO - Recalculate resource values based on total supply and demand, and random fluctuations etc.
        }
    }
}
