﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class ResourceProduction
    {
        /* -------- Properties -------- */
        public List<SingleResourcePresence> ResourceProductions { get; private set; }

        /* -------- Constructors -------- */
        public ResourceProduction()
        {
            ResourceProductions = new List<SingleResourcePresence>();
        }

        /* -------- Public Methods -------- */
        public float GetResourceQuantity(Resource resource)
        {
            foreach (SingleResourcePresence srp in ResourceProductions)
                if (srp.Resource == resource)
                    return srp.Quantity;

            return 0f;
        }

        /* -------- Static Methods -------- */
        public static ResourceProduction CreateFromData(ResourceProductionData data, ResourceManager resourceManager)
        {
            ResourceProduction resourceProduction = new ResourceProduction();

            foreach (SingleResourcePresenceData singleData in data.singleResourceDatas)
            {
                SingleResourcePresence singleResourceProduction = SingleResourcePresence.CreateFromData(singleData, resourceManager);

                if (singleResourceProduction != null)
                    resourceProduction.ResourceProductions.Add(singleResourceProduction);
            }

            return resourceProduction;
        }
    }
}
