﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class ResourcePresence
    {
        /* -------- Properties -------- */
        public List<SingleResourcePresence> ResourcePresences { get; private set; }

        /* -------- Constructors -------- */
        public ResourcePresence()
        {
            ResourcePresences = new List<SingleResourcePresence>();
        }

        /* -------- Public Methods -------- */
        public float GetResourceQuantity(Resource resource)
        {
            foreach (SingleResourcePresence srp in ResourcePresences)
                if (srp.Resource == resource)
                    return srp.Quantity;

            return 0f;
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            int numDifferentFoodResources = 0;

            // Food
            foreach (eResourceType rt in gameManagers.ResourceManager.GetResourceTypesThatCountAsFood())
            {
                List<SingleResourcePresence> resourcePresences = ResourcePresences.FindAll(x => x.Resource.Type == rt);

                if (resourcePresences.Count == 0)
                    continue;

                numDifferentFoodResources += resourcePresences.Count;

                float total = 0f;
                foreach (SingleResourcePresence srp in resourcePresences)
                    total += srp.Quantity;

                if (total <= 0f)
                    continue;

                total *= CalibrationManager.CampaignCalibration.Data.foodStatPerUnitFoodResource;

                eStatConstituentType scType = gameManagers.ResourceManager.GetConstituentTypeForResourceType(rt);
                Modifier mod = new Modifier(ModifierTypeSet.Create_Stats(eStatType.FOOD, scType), eModificationType.ADDITION, ModifierCondition.None, total);
                set.Modifiers.Add(mod);
            }

            // Varied diet
            int minFoodTypesForVariedDiet = CalibrationManager.CampaignCalibration.Data.minDifferentFoodTypesForVariedDiet;
            if (numDifferentFoodResources > minFoodTypesForVariedDiet)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.variedDietModifierDatas)
                {
                    MultipliedModifier mod = MultipliedModifier.CreateFromData(md, gameManagers, (float)(numDifferentFoodResources - minFoodTypesForVariedDiet));
                    set.Modifiers.Add(mod);
                }
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static ResourcePresence CreateFromData(ResourcePresenceData data, ResourceManager resourceManager)
        {
            ResourcePresence resourcePresence = new ResourcePresence();

            foreach (SingleResourcePresenceData singleData in data.singleResourceDatas)
            {
                SingleResourcePresence singleResourcePresence = SingleResourcePresence.CreateFromData(singleData, resourceManager);

                if (singleResourcePresence != null)
                    resourcePresence.ResourcePresences.Add(singleResourcePresence);
            }

            return resourcePresence;
        }
    }
}
