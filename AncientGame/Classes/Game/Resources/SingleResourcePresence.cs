﻿using DataTypes;

namespace AncientGame
{
    class SingleResourcePresence
    {
        /* -------- Properties -------- */
        public Resource Resource { get; private set; }
        public float Quantity { get; private set; }

        /* -------- Constructors -------- */
        public SingleResourcePresence(Resource inResource, float inQuantity)
        {
            Resource = inResource;
            Quantity = inQuantity;
        }

        /* -------- Public Methods -------- */
        public void SetQuantity(float inQuantity)
        {
            Quantity = inQuantity;
        }

        /* -------- Static Methods -------- */
        public static SingleResourcePresence CreateFromData(SingleResourcePresenceData data, ResourceManager resourceManager)
        {
            Resource resource = resourceManager.GetResource(data.resourceID);

            SingleResourcePresence singleResourcePresence = new SingleResourcePresence(resource, data.level);

            return singleResourcePresence;
        }
    }
}
