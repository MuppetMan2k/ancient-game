﻿using System.Collections.Generic;

namespace AncientGame
{
    class PlayerInfo
    {
        /* -------- Properties -------- */
        public List<string> PlayerFactionIDs { get; private set; }

        /* -------- Constructors -------- */
        public PlayerInfo()
        {
            PlayerFactionIDs = new List<string>();
        }
    }
}
