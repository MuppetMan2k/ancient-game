﻿using System.IO;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eGameType
    {
        NEW_CAMPAIGN,
        LOADED_CAMPAIGN
    }

    class GameInfo
    {
        /* -------- Properties -------- */
        public eGameType Type { get; private set; }
        public FileInfo SaveFileInfo { get; private set; }
        public CampaignGameInfo CampaignInfo { get; private set; }

        /* -------- Constructors -------- */
        public GameInfo(eGameType inType, FileInfo inSaveFileInfo, CampaignGameInfo inCampaignInfo)
        {
            Type = inType;
            SaveFileInfo = inSaveFileInfo;
            CampaignInfo = inCampaignInfo;
        }

        /* -------- Static Methods -------- */
        public static GameInfo CreateNewCampaignGameInfo(CampaignGameInfo inCampaignInfo)
        {
            return new GameInfo(eGameType.NEW_CAMPAIGN, null, inCampaignInfo);
        }
        public static GameInfo CreateLoadedCampaignGameInfo(FileInfo inSaveFileInfo)
        {
            return new GameInfo(eGameType.LOADED_CAMPAIGN, inSaveFileInfo, null);
        }
    }
}
