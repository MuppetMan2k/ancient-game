﻿using DataTypes;

namespace AncientGame
{
    class CampaignGameInfo
    {
        /* -------- Properties -------- */
        public CampaignGameData CampaignGameData { get; private set; }
        public PlayerInfo PlayerInfo { get; private set; }

        /* -------- Constructors -------- */
        public CampaignGameInfo(CampaignGameData inCampaignGameData, PlayerInfo inPlayerInfo)
        {
            CampaignGameData = inCampaignGameData;
            PlayerInfo = inPlayerInfo;
        }
    }
}
