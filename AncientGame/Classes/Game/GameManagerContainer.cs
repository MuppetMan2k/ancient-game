﻿using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class GameManagerContainer
    {
        /* -------- Properties -------- */
        public MapManager MapManager { get; private set; }
        public FactionManager FactionManager { get; private set; }
        public ResourceManager ResourceManager { get; private set; }
        public TradeManager TradeManager { get; private set; }
        public TurnManager TurnManager { get; private set; }
        public BuildingManager BuildingManager { get; private set; }
        public UnitManager UnitManager { get; private set; }
        public CultureManagerContainer CultureManagers { get; private set; }
        public MilitaryEraManager MilitaryEraManager { get; private set; }
        public LawManager LawManager { get; private set; }
        public GovernmentTraitManager GovernmentTraitManager { get; private set; }
        public AppointmentManager AppointmentManager { get; private set; }
        public CharacterManager CharacterManager { get; private set; }
        public LandProvinceStakeManagerContainer LandProvinceStakeManagers { get; private set; }
        public BattleManager BattleManager { get; private set; }
        
        public CameraManager CameraManager { get; private set; }
        public ViewManager ViewManager { get; private set; }
        public GameMouseOverManager MouseOverManager { get; private set; }
        public StatueDragManager StatueDragManager { get; private set; }
        public WindowManager WindowManager { get; private set; }

        /* -------- Constructors -------- */
        public GameManagerContainer(GraphicsComponent g, UIComponent ui, ContentManager content)
        {
            MapManager = new MapManager(content);
            FactionManager = new FactionManager();
            ResourceManager = new ResourceManager();
            TradeManager = new TradeManager();
            TurnManager = new TurnManager();
            BuildingManager = new BuildingManager();
            UnitManager = new UnitManager();
            CultureManagers = new CultureManagerContainer();
            MilitaryEraManager = new MilitaryEraManager();
            LawManager = new LawManager();
            GovernmentTraitManager = new GovernmentTraitManager();
            AppointmentManager = new AppointmentManager();
            CharacterManager = new CharacterManager();
            LandProvinceStakeManagers = new LandProvinceStakeManagerContainer();
            BattleManager = new BattleManager();

            CameraManager = new CameraManager(g);
            ViewManager = new ViewManager();
            MouseOverManager = new GameMouseOverManager();
            StatueDragManager = new StatueDragManager();
            WindowManager = new WindowManager(g, ui, this, content);
        }

        /* -------- Public Methods -------- */
        public void Update(GraphicsComponent g, InputComponent i, UIComponent ui, GameTimeWrapper gtw, ContentManager content)
        {
            MapManager.Update(gtw);
            ViewManager.Update(gtw);
            CameraManager.Update(i, ui, gtw, ViewManager);
            MouseOverManager.Update(g, i, ui, this, CameraManager, ViewManager, content);
            StatueDragManager.Update(g, i, ui, this);
            FactionManager.Update(g, CameraManager);
            TradeManager.Update(this);
        }

        public void Draw(GraphicsComponent g, ContentManager content)
        {
            MapManager.Draw(g, FactionManager, StatueDragManager, CameraManager, ViewManager, content);
        }
    }
}
