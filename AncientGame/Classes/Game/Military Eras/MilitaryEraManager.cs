﻿using System.Collections.Generic;

namespace AncientGame
{
    class MilitaryEraManager
    {
        /* -------- Private Fields -------- */
        private List<MilitaryEra> militaryEras;

        /* -------- Constructors -------- */
        public MilitaryEraManager()
        {
            militaryEras = new List<MilitaryEra>();
        }

        /* -------- Public Methods -------- */
        public void AddMilitaryEra(MilitaryEra militaryEra)
        {
            militaryEras.Add(militaryEra);
        }
        public MilitaryEra GetMilitaryEra(string id)
        {
            return militaryEras.Find(x => x.ID == id);
        }
    }
}
