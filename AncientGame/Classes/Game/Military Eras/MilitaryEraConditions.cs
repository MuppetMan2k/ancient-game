﻿using DataTypes;

namespace AncientGame
{
    class MilitaryEraConditions
    {
        /* -------- Properties -------- */
        public int MinYear { get; private set; }

        /* -------- Static Methods -------- */
        public static MilitaryEraConditions CreateFromData(MilitaryEraConditionsData data)
        {
            MilitaryEraConditions conditions = new MilitaryEraConditions();

            conditions.MinYear = data.minYear;

            return conditions;
        }
    }
}
