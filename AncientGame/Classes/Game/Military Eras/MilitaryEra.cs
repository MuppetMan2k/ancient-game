﻿using DataTypes;

namespace AncientGame
{
    class MilitaryEra
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public MilitaryEraConditions Conditions { get; private set; }
        public Culture AssociatedCulture { get; private set; }
        public StatueStyle StatueStyle { get; private set; }

        /* -------- Static Methods -------- */
        public static MilitaryEra CreateFromData(MilitaryEraData data, CultureManager cultureManager)
        {
            MilitaryEra era = new MilitaryEra();

            era.ID = data.id;
            era.Conditions = MilitaryEraConditions.CreateFromData(data.conditionsData);
            era.AssociatedCulture = cultureManager.GetCulture(data.associatedCultureID);
            era.StatueStyle = StatueStyle.CreateFromData(data.statueStyleData);

            return era;
        }
    }
}
