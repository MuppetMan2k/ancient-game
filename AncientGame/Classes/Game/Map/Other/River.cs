﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DataTypes;

namespace AncientGame
{
    class River
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public List<Line> Lines { get; private set; }
        // Graphics
        public List<VertexRiver> Vertices { get; private set; }
        public List<int> Indices { get; private set; }

        /* -------- Private Fields -------- */
        // Constants
        private const float SELECTION_AREA_DISTANCE = 0.8f;

        /* -------- Constructors -------- */
        public River()
        {
            Lines = new List<Line>();

            Vertices = new List<VertexRiver>();
            Indices = new List<int>();
        }

        /* -------- Public Methods -------- */
        public bool MouseIsOver(Vector2 mouseMSPos)
        {
            foreach (Line l in Lines)
                if (l.MouseIsOver(mouseMSPos, SELECTION_AREA_DISTANCE))
                    return true;

            return false;
        }

        public string GetNameString()
        {
            string titleID = "text_river_title";
            string title = Localization.Localize(titleID);
            string nameID = "text_river_" + ID + "_name";
            string name = Localization.Localize(nameID);

            return string.Format(title, name);
        }

        /* -------- Private Methods --------- */
        // Vertex and index creation methods
        private void CalculateVerticesAndIndices()
        {
            float thickness = CalibrationManager.GraphicsCalibration.Data.riverWidth;

            foreach (Line l in Lines)
            {
                List<VertexRiver> lineVerts = new List<VertexRiver>();
                List<int> lineIndices = new List<int>();

                // Vertices
                float cumulativeLength = 0f;
                Vector2 startMSPos = l.MSPositions[0];
                Vector2 startDir = l.MSPositions[1] - startMSPos;
                startDir.Normalize();
                Vector2 startRightPos = startMSPos + VectorUtilities.RotateVector(startDir, -90f) * (CalibrationManager.GraphicsCalibration.Data.riverWidth / 2f);
                Vector2 startLeftPos = startMSPos + VectorUtilities.RotateVector(startDir, 90f) * (CalibrationManager.GraphicsCalibration.Data.riverWidth / 2f);
                lineVerts.Add(CreateBodyVertex(startRightPos, cumulativeLength, true));
                lineVerts.Add(CreateBodyVertex(startLeftPos, cumulativeLength, false));

                for (int i = 1; i < l.MSPositions.Length - 1; i++)
                {
                    Vector2 prevPos = l.MSPositions[i - 1];
                    Vector2 pos = l.MSPositions[i];
                    Vector2 nextPos = l.MSPositions[i + 1];

                    cumulativeLength += (pos - prevPos).Length();

                    Vector2 rightMSPos = VectorUtilities.GetPositionBisectingAngle(prevPos, pos, nextPos, eRotationDirection.CLOCKWISE, thickness / 2f);
                    Vector2 leftMSPos = VectorUtilities.GetPositionBisectingAngle(prevPos, pos, nextPos, eRotationDirection.ANTICLOCKWISE, thickness / 2f);

                    lineVerts.Add(CreateBodyVertex(rightMSPos, cumulativeLength, true));
                    lineVerts.Add(CreateBodyVertex(leftMSPos, cumulativeLength, false));
                }

                Vector2 secondLastMSPos = l.MSPositions[l.MSPositions.Length - 2];
                Vector2 lastMSPos = l.MSPositions[l.MSPositions.Length - 1];
                cumulativeLength += (lastMSPos - secondLastMSPos).Length();
                lineVerts.Add(CreateEndPointVertex(lastMSPos, cumulativeLength));

                // Indices
                for (int i = 0; i < l.MSPositions.Length - 2; i++)
                {
                    int rightInd = 2 * i;
                    int leftInd = rightInd + 1;
                    int nextRightInd = leftInd + 1;
                    int nextLeftInd = nextRightInd + 1;

                    lineIndices.Add(rightInd);
                    lineIndices.Add(leftInd);
                    lineIndices.Add(nextRightInd);

                    lineIndices.Add(nextRightInd);
                    lineIndices.Add(nextLeftInd);
                    lineIndices.Add(leftInd);
                }

                int endRightInd = (l.MSPositions.Length - 2) * 2;
                int endLeftInd = endRightInd + 1;
                int endInd = endLeftInd + 1;

                lineIndices.Add(endRightInd);
                lineIndices.Add(endLeftInd);
                lineIndices.Add(endInd);

                AddLineIndicesAndVertices(lineIndices, lineVerts);
            }
        }
        private VertexRiver CreateStartVertex(Vector2 msPos)
        {
            Vector3 gsPos = SpaceUtilities.ConvertMSToGS(msPos);

            Vector2 coord = new Vector2(CalibrationManager.GraphicsCalibration.Data.riverWidth / 2f, 0f);
            Vector2 texCoord = coord * CalibrationManager.GraphicsCalibration.Data.riverTextureScale;
            Vector2 bumpMapCoord = coord * CalibrationManager.GraphicsCalibration.Data.riverBumpMapScale;

            return new VertexRiver(gsPos, texCoord, bumpMapCoord);
        }
        private VertexRiver CreateStartRoundVertex(Vector2 msPos, float angle)
        {
            Vector3 gsPos = SpaceUtilities.ConvertMSToGS(msPos);

            float width = CalibrationManager.GraphicsCalibration.Data.riverWidth;
            Vector2 coord = new Vector2(width / 2f, 0f);
            coord += VectorUtilities.CreateVector(angle, width / 2f);
            Vector2 texCoord = coord * CalibrationManager.GraphicsCalibration.Data.riverTextureScale;
            Vector2 bumpMapCoord = coord * CalibrationManager.GraphicsCalibration.Data.riverBumpMapScale;

            return new VertexRiver(gsPos, texCoord, bumpMapCoord);
        }
        private VertexRiver CreateBodyVertex(Vector2 msPos, float lengthAlongBody, bool right)
        {
            Vector3 gsPos = SpaceUtilities.ConvertMSToGS(msPos);

            Vector2 coord = new Vector2(right ? CalibrationManager.GraphicsCalibration.Data.riverWidth : 0f, -lengthAlongBody);
            Vector2 texCoord = coord * CalibrationManager.GraphicsCalibration.Data.riverTextureScale;
            Vector2 bumpMapCoord = coord * CalibrationManager.GraphicsCalibration.Data.riverBumpMapScale;

            return new VertexRiver(gsPos, texCoord, bumpMapCoord);
        }
        private VertexRiver CreateEndPointVertex(Vector2 msPos, float totalLength)
        {
            Vector3 gsPos = SpaceUtilities.ConvertMSToGS(msPos);

            Vector2 coord = new Vector2(CalibrationManager.GraphicsCalibration.Data.riverWidth / 2f, -totalLength);
            Vector2 texCoord = coord * CalibrationManager.GraphicsCalibration.Data.riverTextureScale;
            Vector2 bumpMapCoord = coord * CalibrationManager.GraphicsCalibration.Data.riverBumpMapScale;

            return new VertexRiver(gsPos, texCoord, bumpMapCoord);
        }
        private void AddLineIndicesAndVertices(List<int> lineIndices, List<VertexRiver> lineVerts)
        {
            int vertexNum = Vertices.Count;

            foreach (int i in lineIndices)
                Indices.Add(i + vertexNum);

            Vertices.AddRange(lineVerts);
        }

        /* -------- Static Methods -------- */
        public static River CreateFromData(RiverData data)
        {
            River river = new River();

            river.ID = data.id;
            foreach (LineData ld in data.lineDatas)
            {
                Line line = Line.CreateFromData(ld);
                river.Lines.Add(line);
            }
            river.CalculateVerticesAndIndices();

            return river;
        }
    }
}
