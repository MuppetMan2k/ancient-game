﻿using Microsoft.Xna.Framework;
using DataTypes;

namespace AncientGame
{
    class Settlement
    {
        /* -------- Properties -------- */
        public LandTerritory ParentLandTerritory { get; private set; }
        public Vector2 MSPosition { get; private set; }
        public bool IsPort { get; private set; }

        /* -------- Constructors -------- */
        public Settlement(LandTerritory inParentLandTerritory, Vector2 inMSPosition, bool inIsPort)
        {
            ParentLandTerritory = inParentLandTerritory;
            MSPosition = inMSPosition;
            IsPort = inIsPort;
        }

        /* -------- Static Methods -------- */
        public static Settlement CreateFromData(SettlementData data, LandTerritory inParentLandTerritory)
        {
            Settlement settlement = new Settlement(inParentLandTerritory, data.position, data.isPort);

            return settlement;
        }
    }
}
