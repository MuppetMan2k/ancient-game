﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class RiverManager
    {
        /* -------- Private Fields -------- */
        private List<River> rivers;

        /* -------- Constructors -------- */
        public RiverManager()
        {
            rivers = new List<River>();
        }

        /* -------- Public Methods -------- */
        public void DrawRivers(RiverGraphicsManager riverGraphicsManager, RiverMetaStyle ms)
        {
            foreach (River r in rivers)
                riverGraphicsManager.DrawRiver(r, ms);
        }

        public void AddRiver(River river)
        {
            rivers.Add(river);
        }
        public River GetMouseOverRiver(Vector2 mouseMSPos)
        {
            return rivers.Find(x => x.MouseIsOver(mouseMSPos));
        }
    }
}
