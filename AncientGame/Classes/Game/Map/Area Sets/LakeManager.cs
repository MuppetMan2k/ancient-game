﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class LakeManager
    {
        /* -------- Private Fields -------- */
        private List<Lake> lakes;

        /* -------- Constructors -------- */
        public LakeManager()
        {
            lakes = new List<Lake>();
        }

        /* -------- Public Methods -------- */
        public void DrawLakes(SeaTerrainGraphicsManager seaTerrainGraphicsManager)
        {
            foreach (Lake l in lakes)
                seaTerrainGraphicsManager.DrawLake(l);
        }

        public void AddLake(Lake lake)
        {
            lakes.Add(lake);
        }
        public Lake GetMouseOverLake(Vector2 mouseMSPos)
        {
            return lakes.Find(x => x.MouseIsOver(mouseMSPos));
        }
    }
}
