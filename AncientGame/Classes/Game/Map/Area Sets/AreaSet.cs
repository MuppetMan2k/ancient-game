﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    abstract class AreaSet
    {
        /* -------- Properties -------- */
        public string ID { get; protected set; }
        public List<Area> Areas { get; protected set; }
        public Rectanglef BoundingRectangle { get; private set; }
        public float TotalArea { get; private set; }

        /* -------- Constructors -------- */
        public AreaSet()
        {
            Areas = new List<Area>();
        }

        /* -------- Public Methods -------- */
        public bool MouseIsOver(Vector2 mouseMSPos)
        {
            foreach (Area a in Areas)
                if (AreaUtilities.PositionIsWithinArea(a, mouseMSPos))
                    return true;

            return false;
        }

        public void CalculateBoundingRectangle()
        {
            if (Areas.Count == 0)
                return;

            BoundingRectangle = Areas[0].BoundingRectangle;

            for (int i = 1; i < Areas.Count; i++)
            {
                Area area = Areas[i];
                BoundingRectangle = BoundingRectangle.Unify(area.BoundingRectangle);
            }
        }
        public void CalculateTotalArea()
        {
            TotalArea = 0f;
            foreach (Area a in Areas)
                TotalArea += a.EnclosedArea;
        }
    }
}
