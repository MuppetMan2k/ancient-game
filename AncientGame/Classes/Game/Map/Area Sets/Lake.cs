﻿using DataTypes;

namespace AncientGame
{
    class Lake : AreaSet
    {
        /* -------- Public Methods -------- */
        public string GetNameString()
        {
            string titleID = "text_lake_title";
            string title = Localization.Localize(titleID);
            string nameID = "text_lake_" + ID + "_name";
            string name = Localization.Localize(nameID);

            return string.Format(title, name);
        }

        /* -------- Static Methods -------- */
        public static Lake CreateFromData(LakeData data)
        {
            Lake lake = new Lake();

            lake.ID = data.id;
            foreach (AreaData ad in data.areaDatas)
            {
                Area area = Area.CreateFromData(ad);
                lake.Areas.Add(area);
            }
            lake.CalculateBoundingRectangle();

            return lake;
        }
    }
}
