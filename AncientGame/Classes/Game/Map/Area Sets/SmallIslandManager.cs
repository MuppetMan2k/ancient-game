﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class SmallIslandManager
    {
        /* -------- Private Fields -------- */
        private List<SmallIsland> smallIslands;

        /* -------- Constructors -------- */
        public SmallIslandManager()
        {
            smallIslands = new List<SmallIsland>();
        }

        /* -------- Public Methods -------- */
        public void DrawSmallIslands(SmallIslandGraphicsManager smallIslandGraphicsManager)
        {
            foreach (SmallIsland si in smallIslands)
                smallIslandGraphicsManager.DrawSmallIsland(si);
        }

        public void AddSmallIsland(SmallIsland smallIsland)
        {
            smallIslands.Add(smallIsland);
        }
        public SmallIsland GetMouseOverSmallIsland(Vector2 mouseMSPos)
        {
            return smallIslands.Find(x => x.MouseIsOver(mouseMSPos));
        }
    }
}
