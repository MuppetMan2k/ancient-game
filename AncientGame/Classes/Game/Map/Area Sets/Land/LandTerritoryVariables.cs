﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eStatType
    {
        PLEB_HAPPINESS,
        NOBILITY_HAPPINESS,
        ORDER,
        HEALTH,
        POPULATION_GROWTH,
        FOOD,
        FOOD_STORES_CHANGE,
        PRIVATE_WEALTH_CHANGE,
        MANPOWER_CHANGE,
        WATER,
        WARMTH
    }

    class LandTerritoryVariables
    {
        /* -------- Properties -------- */
        public LandTerritory ParentLandTerritory { get; private set; }
        public Faction OwningFaction { get; private set; }
        public Faction OccupyingFaction { get; private set; }
        public BuildingSet SettlementBuildingSet { get; private set; }
        public BuildingSet TerritoryBuildingSet { get; private set; }
        public ResourceProduction ResourceProduction { get; private set; }
        public ResourcePresence ResourcePresence { get; private set; }
        public TradeSet TradeSet { get; private set; }
        public IncomeSet IncomeSet { get; private set; }
        public GarrisonUnitSet GarrisonUnitSet { get; private set; }
        public int SupportablePopulation { get; private set; }
        // Stats
        public HappinessState Happiness { get; private set; }
        public OrderState Order { get; private set; }
        public HealthState Health { get; private set; }
        public PopulationState Population { get; private set; }
        public FoodState Food { get; private set; }
        public FoodStoresState FoodStores { get; private set; }
        public PrivateWealthState PrivateWealth { get; private set; }
        public ManpowerState Manpower { get; private set; }
        public WeatherState Weather { get; private set; }
        public float MovementPointCost { get; set; }

        /* -------- Constructors -------- */
        public LandTerritoryVariables(LandTerritory inParentLandTerritory)
        {
            ParentLandTerritory = inParentLandTerritory;
        }

        /* -------- Public Methods -------- */
        public void SetOwningFaction(Faction faction)
        {
            OwningFaction = faction;
        }
        public void SetOccupyingFaction(Faction faction)
        {
            OccupyingFaction = faction;
        }

        public void SetState(LandTerritoryStateData data, GameManagerContainer gameManagers)
        {
            SettlementBuildingSet = BuildingSet.CreateFromData(data.settlementBuildingSetData, eBuildingSetType.SETTLEMENT, gameManagers);
            TerritoryBuildingSet = BuildingSet.CreateFromData(data.territoryBuildingSetData, eBuildingSetType.TERRITORY, gameManagers);
            ResourceProduction = ResourceProduction.CreateFromData(data.resourceProductionData, gameManagers.ResourceManager);
            ResourcePresence = ResourcePresence.CreateFromData(data.resourcePresenceData, gameManagers.ResourceManager);
            TradeSet = TradeSet.CreateFromData(data.tradeSetData, gameManagers);
            IncomeSet = IncomeSet.CreateFromData(data.incomeSetData);
            GarrisonUnitSet = GarrisonUnitSet.CreateFromData(data.garrisonUnitStateDatas, gameManagers);
            SupportablePopulation = data.supportablePopulation;

            Happiness = HappinessState.CreateFromData(data.happinessStateData);
            Order = OrderState.CreateFromData(data.orderStateData);
            Health = HealthState.CreateFromData(data.healthStateData);
            Population = PopulationState.CreateFromData(data.populationStateData);
            Food = FoodState.CreateFromData(data.foodStateData);
            FoodStores = FoodStoresState.CreateFromData(data.foodStoresStateData);
            PrivateWealth = PrivateWealthState.CreateFromData(data.privateWealthStateData);
            Manpower = ManpowerState.CreateFromData(data.manpowerStateData);
            Weather = WeatherState.CreateFromData(data.weatherStateData);
            MovementPointCost = data.movementPointCost;
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            if (ParentLandTerritory.RequiresWaterStat())
                set.Merge(Weather.GetWaterModifierSet(gameManagers));
            if (ParentLandTerritory.RequiresWarmthStat())
                set.Merge(Weather.GetWarmthModifierSet(gameManagers));
            set.Merge(Food.GetModifierSet(gameManagers));
            set.Merge(FoodStores.GetModifierSet(gameManagers));
            set.Merge(Health.GetModifierSet(gameManagers));
            set.Merge(Happiness.GetModifierSet(gameManagers));
            set.Merge(Order.GetModifierSet(gameManagers));
            set.Merge(Population.GetModifierSet(gameManagers, ParentLandTerritory));
            set.Merge(PrivateWealth.GetModifierSet(gameManagers));
            set.Merge(Manpower.GetModifierSet(gameManagers));
            set.Merge(ResourcePresence.GetModifierSet(gameManagers));
            set.Merge(SettlementBuildingSet.GetModifierSet(gameManagers));
            set.Merge(TerritoryBuildingSet.GetModifierSet(gameManagers));

            return set;
        }

        // Recalculation methods
        public void RecalculateResourceProduction(eResourceTier tier, GameManagerContainer gameManagers)
        {
            if (tier == eResourceTier.PRIMARY)
                ResourceProduction.ResourceProductions.Clear();

            foreach (KeyValuePair<string, Resource> kvp in gameManagers.ResourceManager.GetAllResources())
            {
                Resource r = kvp.Value;
                if (r.Tier != tier)
                    continue;

                float quantity = ParentLandTerritory.BaseResourceProduction.GetResourceQuantity(r);

                ParentLandTerritory.GetModifierSet(gameManagers).Modify_ResourceQuantity(quantity, r);

                if (quantity <= 0)
                    continue;

                ResourceProduction.ResourceProductions.Add(new SingleResourcePresence(r, quantity));
            }
        }
        public void RecalculateResourcePresence(ResourceManager rm)
        {
            ResourcePresence.ResourcePresences.Clear();

            foreach (KeyValuePair<string, Resource> kvp in rm.GetAllResources())
            {
                Resource r = kvp.Value;

                float quantity = ResourceProduction.GetResourceQuantity(r);
                float importQuantity = TradeSet.GetImportedResourceQuantity(r);
                quantity += importQuantity;
                if (importQuantity == 0f)
                {
                    float exportQuantity = TradeSet.GetExportedResourceQuantity(r);
                    quantity -= exportQuantity;
                }

                if (quantity > 0f)
                    ResourcePresence.ResourcePresences.Add(new SingleResourcePresence(r, quantity));
            }
        }
        public void RecalculateStats(GameManagerContainer gameManagers)
        {
            RecalculateWeather(gameManagers);
            RecalculateFood(gameManagers);
            RecalculateFoodStoresChangeAndMax(gameManagers);
            RecalculateHealth(gameManagers);
            RecalculateHappiness(gameManagers);
            RecalculateOrder(gameManagers);
            RecalculatePopulationGrowth(gameManagers);
            RecalculatePrivateWealthChange(gameManagers);
            RecalculateManpowerChangeAndMax(gameManagers);
        }
        public void RecalculateIncomeSet(GameManagerContainer gameManagers)
        {
            IncomeSet.RecalculateWealthSources(ParentLandTerritory, gameManagers);
            IncomeSet.RecalculateEfficiencySet(ParentLandTerritory, gameManagers);
        }
        public void RecalculateBuildingSets(GameManagerContainer gameManagers)
        {
            int settlementBuildingSlotNum = ParentLandTerritory.GetSettlementBuildingSlotNum(gameManagers);
            SettlementBuildingSet.CheckForBuildingSlotAdditions(settlementBuildingSlotNum);
            SettlementBuildingSet.CheckForBuildingSlotRemovals(settlementBuildingSlotNum);
            int territoryBuildingSlotNum = ParentLandTerritory.GetTerritoryBuildingSlotNum(gameManagers);
            TerritoryBuildingSet.CheckForBuildingSlotAdditions(territoryBuildingSlotNum);
            TerritoryBuildingSet.CheckForBuildingSlotRemovals(territoryBuildingSlotNum);
        }
        public void RecalculateSupportablePopulation(GameManagerContainer gameManagers)
        {
            float supportablePopulationF = (float)CalibrationManager.CampaignCalibration.Data.landTerritoryBaseSupportablePopulation;
            supportablePopulationF = GetModifierSet(gameManagers).Modify(supportablePopulationF, eModifierType.SUPPORTABLE_POPULATION);

            SupportablePopulation = Mathf.Max(Mathf.Round(supportablePopulationF), CalibrationManager.CampaignCalibration.Data.landTerritoryBaseSupportablePopulation);
        }

        /* -------- Private Methods --------- */
        // Recalculation methods
        private void RecalculateWeather(GameManagerContainer gameManagers)
        {
            if (ParentLandTerritory.RequiresWaterStat())
                RecalculateWater(gameManagers);

            if (ParentLandTerritory.RequiresWarmthStat())
                RecalculateWarmth(gameManagers);
        }
        private void RecalculateWater(GameManagerContainer gameManagers)
        {
            Weather.WaterConstituents.StatConstituents.Clear();

            foreach (eStatConstituentType constituentType in GeneralUtilities.GetEnumValues<eStatConstituentType>())
            {
                float val = 0f;

                ModifierTypeSet modifierType = ModifierTypeSet.Create_Stats(eStatType.WATER, constituentType);
                val = ParentLandTerritory.GetModifierSet(gameManagers).Modify(val, modifierType);

                if (val != 0f)
                    Weather.WaterConstituents.AddStatConstituent(new StatConstituent(constituentType, val));
            }

            Weather.RecalculateWater();
        }
        private void RecalculateWarmth(GameManagerContainer gameManagers)
        {
            Weather.WarmthConstituents.StatConstituents.Clear();

            foreach (eStatConstituentType constituentType in GeneralUtilities.GetEnumValues<eStatConstituentType>())
            {
                float val = 0f;

                ModifierTypeSet modifierType = ModifierTypeSet.Create_Stats(eStatType.WARMTH, constituentType);
                val = ParentLandTerritory.GetModifierSet(gameManagers).Modify(val, modifierType);

                if (val != 0f)
                    Weather.WarmthConstituents.AddStatConstituent(new StatConstituent(constituentType, val));
            }

            Weather.RecalculateWarmth();
        }
        private void RecalculateFood(GameManagerContainer gameManagers)
        {
            Food.FoodConstituents.StatConstituents.Clear();

            foreach (eStatConstituentType constituentType in GeneralUtilities.GetEnumValues<eStatConstituentType>())
            {
                float val = 0f;

                ModifierTypeSet modifierType = ModifierTypeSet.Create_Stats(eStatType.FOOD, constituentType);
                val = ParentLandTerritory.GetModifierSet(gameManagers).Modify(val, modifierType);

                if (val != 0f)
                    Food.FoodConstituents.AddStatConstituent(new StatConstituent(constituentType, val));
            }

            Food.RecalculateFood();
        }
        private void RecalculateFoodStoresChangeAndMax(GameManagerContainer gameManagers)
        {
            FoodStores.FoodStoresChangeConstituents.StatConstituents.Clear();

            foreach (eStatConstituentType constituentType in GeneralUtilities.GetEnumValues<eStatConstituentType>())
            {
                float val = 0f;

                ModifierTypeSet modifierType = ModifierTypeSet.Create_Stats(eStatType.FOOD_STORES_CHANGE, constituentType);
                val = ParentLandTerritory.GetModifierSet(gameManagers).Modify(val, modifierType);

                if (val != 0f)
                    FoodStores.FoodStoresChangeConstituents.AddStatConstituent(new StatConstituent(constituentType, val));
            }

            FoodStores.RecalculateFoodStoresChange();
            FoodStores.RecalculateMaxFoodStores(ParentLandTerritory, gameManagers);
        }
        private void RecalculateHealth(GameManagerContainer gameManagers)
        {
            Health.HealthConstituents.StatConstituents.Clear();

            foreach (eStatConstituentType constituentType in GeneralUtilities.GetEnumValues<eStatConstituentType>())
            {
                float val = 0f;

                ModifierTypeSet modifierType = ModifierTypeSet.Create_Stats(eStatType.HEALTH, constituentType);
                val = ParentLandTerritory.GetModifierSet(gameManagers).Modify(val, modifierType);

                if (val != 0f)
                    Health.HealthConstituents.AddStatConstituent(new StatConstituent(constituentType, val));
            }

            Health.RecalculateHealth();
        }
        private void RecalculateHappiness(GameManagerContainer gameManagers)
        {
            // Plebs
            Happiness.PlebHappinessConstituents.StatConstituents.Clear();

            foreach (eStatConstituentType constituentType in GeneralUtilities.GetEnumValues<eStatConstituentType>())
            {
                float val = 0f;

                ModifierTypeSet modifierType = ModifierTypeSet.Create_Stats(eStatType.PLEB_HAPPINESS, constituentType);
                val = ParentLandTerritory.GetModifierSet(gameManagers).Modify(val, modifierType);

                if (val != 0f)
                    Happiness.PlebHappinessConstituents.AddStatConstituent(new StatConstituent(constituentType, val));
            }

            Happiness.RecalculatePlebHappiness();

            // Nobility
            Happiness.NobilityHappinessConstituents.StatConstituents.Clear();

            foreach (eStatConstituentType constituentType in GeneralUtilities.GetEnumValues<eStatConstituentType>())
            {
                float val = 0f;

                ModifierTypeSet modifierType = ModifierTypeSet.Create_Stats(eStatType.NOBILITY_HAPPINESS, constituentType);
                val = ParentLandTerritory.GetModifierSet(gameManagers).Modify(val, modifierType);

                if (val != 0f)
                    Happiness.NobilityHappinessConstituents.AddStatConstituent(new StatConstituent(constituentType, val));
            }

            Happiness.RecalculateNobilityHappiness();
        }
        private void RecalculateOrder(GameManagerContainer gameManagers)
        {
            Order.OrderConstituents.StatConstituents.Clear();

            foreach (eStatConstituentType constituentType in GeneralUtilities.GetEnumValues<eStatConstituentType>())
            {
                float val = 0f;

                ModifierTypeSet modifierType = ModifierTypeSet.Create_Stats(eStatType.ORDER, constituentType);
                val = ParentLandTerritory.GetModifierSet(gameManagers).Modify(val, modifierType);

                if (val != 0f)
                    Order.OrderConstituents.AddStatConstituent(new StatConstituent(constituentType, val));
            }

            Order.RecalculateOrder();
        }
        private void RecalculatePopulationGrowth(GameManagerContainer gameManagers)
        {
            Population.PopulationGrowthConstituents.StatConstituents.Clear();

            foreach (eStatConstituentType constituentType in GeneralUtilities.GetEnumValues<eStatConstituentType>())
            {
                float val = 0f;

                ModifierTypeSet modifierType = ModifierTypeSet.Create_Stats(eStatType.POPULATION_GROWTH, constituentType);
                val = ParentLandTerritory.GetModifierSet(gameManagers).Modify(val, modifierType);

                if (val != 0f)
                    Population.PopulationGrowthConstituents.AddStatConstituent(new StatConstituent(constituentType, val));
            }

            Population.RecalculatePopulationGrowth();
        }
        private void RecalculatePrivateWealthChange(GameManagerContainer gameManagers)
        {
            PrivateWealth.PrivateWealthChangeConstituents.StatConstituents.Clear();

            foreach (eStatConstituentType constituentType in GeneralUtilities.GetEnumValues<eStatConstituentType>())
            {
                float val = 0f;

                ModifierTypeSet modifierType = ModifierTypeSet.Create_Stats(eStatType.PRIVATE_WEALTH_CHANGE, constituentType);
                val = ParentLandTerritory.GetModifierSet(gameManagers).Modify(val, modifierType);

                if (val != 0f)
                    PrivateWealth.PrivateWealthChangeConstituents.AddStatConstituent(new StatConstituent(constituentType, val));
            }

            PrivateWealth.RecalculatePrivateWealthChange();
        }
        private void RecalculateManpowerChangeAndMax(GameManagerContainer gameManagers)
        {
            Manpower.ManpowerChangeConstituents.StatConstituents.Clear();

            foreach (eStatConstituentType constituentType in GeneralUtilities.GetEnumValues<eStatConstituentType>())
            {
                float val = 0f;

                ModifierTypeSet modifierType = ModifierTypeSet.Create_Stats(eStatType.MANPOWER_CHANGE, constituentType);
                val = ParentLandTerritory.GetModifierSet(gameManagers).Modify(val, modifierType);

                if (val != 0f)
                    Manpower.ManpowerChangeConstituents.AddStatConstituent(new StatConstituent(constituentType, val));
            }

            Manpower.RecalculateManpowerChange();
            Manpower.RecalculateMaxManpower(ParentLandTerritory, gameManagers);
        }
    }
}
