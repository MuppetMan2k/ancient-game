﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class LandRegion : AreaSet
    {
        /* -------- Properties -------- */
        // Data
        public NameLabel NameLabel { get; private set; }
        public List<LandTerritory> ChildLandTerritories { get; private set; }
        public LandProvince ParentLandProvince { get; private set; }
        public int FertilityLevel { get; private set; }
        // Variables
        public LandRegionVariables Variables { get; private set; }

        /* -------- Constructors -------- */
        public LandRegion()
        {
            ChildLandTerritories = new List<LandTerritory>();
            Variables = new LandRegionVariables();
        }

        /* -------- Public Methods -------- */
        public string GetNameString()
        {
            string id = "text_land-region_" + ID + "_name";
            return Localization.Localize(id);
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            set.Merge(GetFertilityModifierSet(gameManagers));

            return set;
        }
        public ModifierSet GetFertilityModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            float fertilityMultiplier = CalibrationManager.CampaignCalibration.Data.baseResourceProductionBaseFertilityMultiplier;
            fertilityMultiplier *= Mathf.Pow(CalibrationManager.CampaignCalibration.Data.baseResourceProductionReducedFertilityMultiplier, 10 - FertilityLevel);
            if (fertilityMultiplier != 1f)
            {
                foreach (eResourceType rt in gameManagers.ResourceManager.ResourceTypesAffectedByHarvest)
                {
                    Modifier mod = new Modifier(ModifierTypeSet.Create_ResourceTypeProduction(rt), eModificationType.MULTIPLICATION, ModifierCondition.None, fertilityMultiplier);
                    // TODO - apply "primary resource tier only" caveat?
                    set.Modifiers.Add(mod);
                }
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Creation Methods -------- */
        public static LandRegion CreateFromData(LandRegionData data)
        {
            LandRegion landRegion = new LandRegion();

            landRegion.ID = data.id;
            foreach (AreaData ad in data.areaDatas)
            {
                Area area = Area.CreateFromData(ad);
                landRegion.Areas.Add(area);
            }
            landRegion.CalculateBoundingRectangle();
            landRegion.NameLabel = NameLabel.CreateFromData(data.nameLabelData, landRegion.GetNameString());
            landRegion.FertilityLevel = data.fertilityLevel;

            return landRegion;
        }
        public void SetCrossReferences(LandRegionData data, MapManager mapManager)
        {
            foreach (string id in data.landTerritoryIDs)
            {
                LandTerritory landTerritory = mapManager.LandTerritoryManager.GetLandTerritory(id);
                ChildLandTerritories.Add(landTerritory);
                landTerritory.SetParentLandRegion(this);
            }
        }
        public void SetParentLandProvince(LandProvince inParentLandProvince)
        {
            ParentLandProvince = inParentLandProvince;
        }
    }
}
