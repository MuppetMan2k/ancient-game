﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eTerrainType
    {
        LOW_GRASS_LAND,
        HILLY_GRASS_LAND,
        STEPPES,
        FOREST,
        DESERT,
        MOUNTAIN,
        MARSH
    }

    class LandTerritory : Territory
    {
        /* -------- Properties -------- */
        // Data
        public Settlement Settlement { get; private set; }
        public List<AdjacentLandTerritory> AdjacentTerritories { get; private set; }
        public AdjacentSeaTerritory PortAdjacentSeaTerritory { get; private set; }
        public ResourceProduction BaseResourceProduction { get; private set; }
        public LandRegion ParentLandRegion { get; private set; }
        public LandProvince ParentLandProvince { get; private set; }
        public eTerrainType TerrainType { get; private set; }
        public bool IsSettlementOnly { get; private set; }
        public List<Road> Roads { get; private set; }
        public List<LandTradeRoute> LandTradeRoutes { get; private set; }
        public List<SeaTradeRoute> SeaTradeRoutes { get; private set; }
        // Variables
        public LandTerritoryVariables Variables { get; private set; }

        /* -------- Private Fields -------- */
        // Constants
        private const float SELECTION_AREA_DISTANCE = 1.4f;

        /* -------- Constructors -------- */
        public LandTerritory()
        {
            AdjacentTerritories = new List<AdjacentLandTerritory>();
            Variables = new LandTerritoryVariables(this);
            Roads = new List<Road>();
            LandTradeRoutes = new List<LandTradeRoute>();
            SeaTradeRoutes = new List<SeaTradeRoute>();
        }

        /* -------- Public Methods -------- */
        public string GetNameString()
        {
            string locID = "text_land-territory_" + ID + "_name";
            return Localization.Localize(locID);
        }
        public string GetTerritoryNameString()
        {
            string territoryString = Localization.Localize("text_land-territory_territory");
            return String.Format(territoryString, GetNameString());
        }

        public bool MouseIsOverSettlement(Vector2 mouseMSPos)
        {
            if (!BoundingRectangle.ContainsPosition(mouseMSPos))
                return false;

            float separationSquared = (mouseMSPos - Settlement.MSPosition).LengthSquared();
            return (separationSquared < SELECTION_AREA_DISTANCE * SELECTION_AREA_DISTANCE);
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            set.Merge(Variables.GetModifierSet(gameManagers));
            set.Merge(GetTerrainTypeModifierSet(gameManagers));
            set.Merge(ParentLandRegion.GetModifierSet(gameManagers));
            set.Merge(ParentLandProvince.GetModifierSet(gameManagers));
            LandProvinceStake lps = Variables.OwningFaction.Variables.GetLandProvinceStake(ParentLandProvince);
            set.Merge(lps.GetModifierSet(gameManagers));
            set.Merge(GetCapitolModifierSet(gameManagers));
            set.Merge(GetOwnershipModifierSet(gameManagers));

            return set;
        }
        public ModifierSet GetTerrainTypeModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            ModifierData[] modifierDatas;

            switch (TerrainType)
            {
                case eTerrainType.LOW_GRASS_LAND:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landTerritoryTerrainModifierDatas_LowGrassLand;
                    break;
                case eTerrainType.HILLY_GRASS_LAND:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landTerritoryTerrainModifierDatas_HillyGrassLand;
                    break;
                case eTerrainType.MOUNTAIN:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landTerritoryTerrainModifierDatas_Mountain;
                    break;
                case eTerrainType.STEPPES:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landTerritoryTerrainModifierDatas_Steppes;
                    break;
                case eTerrainType.FOREST:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landTerritoryTerrainModifierDatas_Forest;
                    break;
                case eTerrainType.DESERT:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landTerritoryTerrainModifierDatas_Desert;
                    break;
                case eTerrainType.MARSH:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landTerritoryTerrainModifierDatas_Marsh;
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(TerrainType);
                    modifierDatas = null;
                    break;
            }

            if (modifierDatas != null)
                foreach (ModifierData md in modifierDatas)
                    set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
            set.TestIsSatisfied(objectSet);

            return set;
        }
        public ModifierSet GetCapitolModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (Variables.OwningFaction.Variables.CapitolLandTerritory == this)
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.landTerritoryCapitolModifierDatas)
                    set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
            else
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.landTerritoryNonCapitolModifierDatas)
                    set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
            set.TestIsSatisfied(objectSet);

            return set;
        }
        public ModifierSet GetOwnershipModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            eLandTerritoryControlLevel controlLevel = Variables.OwningFaction.GetLandTerritoryControlLevel(this);

            switch (controlLevel)
            {
                case eLandTerritoryControlLevel.MILITARY_OWNERSHIP:
                    foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.landTerritoryOwnershipModifierDatas_MilitaryOwnership)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                    foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.landTerritoryOwnershipModifierDatas_AnnexedOwnership)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                case eLandTerritoryControlLevel.HOMELAND_OWNERSHIP:
                    foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.landTerritoryOwnershipModifierDatas_HomelandOwnership)
                        set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
                    break;
                default:
                    Debug.LogWarning("Expected an ownership control level!");
                    break;
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        public void RecalculateMovementPointCost(GameManagerContainer gameManagers)
        {
            Variables.MovementPointCost = TotalArea * CalibrationManager.CampaignCalibration.Data.landTerritoryMovementPointsPerArea;
            Variables.MovementPointCost = GetModifierSet(gameManagers).Modify(Variables.MovementPointCost, eModifierType.LAND_TERRITORY_MOVEMENT_POINT_COST);
        }
        public int GetTerritoryToSettlementMovementPointCost()
        {
            return Mathf.Round(Variables.MovementPointCost * CalibrationManager.CampaignCalibration.Data.landTerritoryToSettlementMovementPointCostMultiplier);
        }

        public List<TradeRoute> GetAllTradeRoutes()
        {
            List<TradeRoute> tradeRoutes = new List<TradeRoute>();
            tradeRoutes.AddRange(LandTradeRoutes);
            tradeRoutes.AddRange(SeaTradeRoutes);

            return tradeRoutes;
        }
        public int GetTraderMovementPoints(GameManagerContainer gameManagers)
        {
            float movementPoints = (float)CalibrationManager.TradeCalibration.Data.baseTraderMovementPoints;
            movementPoints = GetModifierSet(gameManagers).Modify(movementPoints, eModifierType.TRADER_MOVEMENT_POINTS);

            return Mathf.Round(movementPoints);
        }
        public bool IsBlockedFromTrade(FactionManager fm)
        {
            // Occupation
            if (Variables.OccupyingFaction != null)
                return true;

            // Under Siege
            if (IsUnderSiege(fm))
                return true;

            return false;
        }
        public bool SetTradePathToTradeRoute(TradePath tp, LandTerritory endLandTerritory)
        {
            foreach (TradeRoute tr in GetAllTradeRoutes())
            {
                LandTerritory oppositeLandTerritory = tr.GetOppositeLandTerritory(this);

                if (oppositeLandTerritory == endLandTerritory)
                {
                    tr.SetTradePath(tp, endLandTerritory);
                    return true;
                }
            }

            return false;
        }

        public int GetSettlementBuildingSlotNum(GameManagerContainer gameManagers)
        {
            // Central building, walls and port
            int numSpecialSlots = Settlement.IsPort ? 3 : 2;

            float numStandardSlotsF = (float)CalibrationManager.CampaignCalibration.Data.baseNumSettlementStandardBuildingSlots;
            numStandardSlotsF = GetModifierSet(gameManagers).Modify(numStandardSlotsF, eModifierType.SETTLEMENT_BUILDING_SLOT_NUMBER);

            return (numSpecialSlots + Mathf.ClampLower(Mathf.Round(numStandardSlotsF), 1));
        }
        public int GetTerritoryBuildingSlotNum(GameManagerContainer gameManagers)
        {
            if (IsSettlementOnly)
                return 0;

            // Roads
            int numSpecialSlots = 1;

            float numSlotsF = CalibrationManager.CampaignCalibration.Data.numTerritoryBuildingSlotsPerArea * TotalArea;
            numSlotsF = GetModifierSet(gameManagers).Modify(numSlotsF, eModifierType.TERRITORY_BUILDING_SLOT_NUMBER);

            return (numSpecialSlots + Mathf.ClampLower(Mathf.Round(numSlotsF), 1));
        }

        public bool RequiresWaterStat()
        {
            string[] waterClimateTypes = CalibrationManager.CampaignCalibration.Data.climateTypesRequiringWater;
            foreach (string wct in waterClimateTypes)
            {
                eClimateType climateType = DataUtilities.ParseClimateType(wct);
                if (climateType == ParentLandProvince.ClimateType)
                    return true;
            }

            return false;
        }
        public bool RequiresWarmthStat()
        {
            string[] warmthClimateTypes = CalibrationManager.CampaignCalibration.Data.climateTypesRequiringWarmth;
            foreach (string wct in warmthClimateTypes)
            {
                eClimateType climateType = DataUtilities.ParseClimateType(wct);
                if (climateType == ParentLandProvince.ClimateType)
                    return true;
            }

            return false;
        }

        public bool TryChargeCost(Cost cost)
        {
            if (cost.ManpowerCost > Variables.Manpower.Manpower)
                return false;

            Faction f = Variables.OwningFaction;
            if (!f.TryChargeCost(cost))
                return false;

            Variables.Manpower.ChargeManpower(cost.ManpowerCost);
            return true;
        }

        public List<Statue> GetGarrisonedStatues()
        {
            return Variables.OwningFaction.Variables.StatueSet.Statues.FindAll(x => x.Position.IsInSettlement && x.Position.Settlement == Settlement);
        }
        public bool IsUnderSiege(FactionManager fm)
        {
            foreach (Faction f in fm.GetAllFactions())
            {
                foreach (Statue s in f.Variables.StatueSet.Statues)
                {
                    if (!s.Position.IsInLandTerritory)
                        continue;
                    if (s.Position.LandTerritory != this)
                        continue;

                    LandForce lf = s as LandForce;
                    if (lf == null)
                        continue;

                    if (lf.Stance == eLandForceStance.LAYING_SIEGE)
                        return true;
                }
            }

            return false;
        }

        /* -------- Creation Methods -------- */
        public static LandTerritory CreateFromData(LandTerritoryData data)
        {
            LandTerritory landTerritory = new LandTerritory();

            landTerritory.ID = data.id;
            foreach (AreaData ad in data.areaDatas)
            {
                Area area = Area.CreateFromData(ad);
                landTerritory.Areas.Add(area);
            }
            landTerritory.CalculateTotalArea();
            landTerritory.CalculateBoundingRectangle();
            landTerritory.Settlement = Settlement.CreateFromData(data.settlementData, landTerritory);
            landTerritory.TerrainType = DataUtilities.ParseTerrainType(data.terrainType);
            landTerritory.IsSettlementOnly = data.isSettlementOnly;

            return landTerritory;
        }
        public void SetCrossReferences(LandTerritoryData data, GameManagerContainer gameManagers)
        {
            foreach (AdjacentLandTerritoryData altd in data.adjacentLandTerritoryDatas)
            {
                LandTerritory landTerritory = gameManagers.MapManager.LandTerritoryManager.GetLandTerritory(altd.territoryID);
                AdjacentLandTerritory adjacentLandTerritory = new AdjacentLandTerritory(landTerritory, altd.crossesRiver);
                AdjacentTerritories.Add(adjacentLandTerritory);
            }
            if (Settlement.IsPort)
            {
                SeaTerritory seaTerritory = gameManagers.MapManager.SeaTerritoryManager.GetSeaTerritory(data.adjacentCoastalSeaTerritoryID);
                PortAdjacentSeaTerritory = new AdjacentSeaTerritory(seaTerritory);
            }
            BaseResourceProduction = gameManagers.ResourceManager.CreateLandTerritoryBaseResourceProduction(this, data.resourceProductionData);
        }
        public void SetParentLandRegion(LandRegion inParentLandRegion)
        {
            ParentLandRegion = inParentLandRegion;
        }
        public void SetParentLandProvince(LandProvince inParentLandProvince)
        {
            ParentLandProvince = inParentLandProvince;
        }
    }
}
