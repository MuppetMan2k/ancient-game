﻿using DataTypes;

namespace AncientGame
{
    class LandRegionVariables
    {
        /* -------- Properties -------- */
        public CultureState CultureState { get; private set; }
        public LanguageState LanguageState { get; private set; }
        public ReligionState ReligionState { get; private set; }

        /* -------- Public Methods -------- */
        public void SetState(LandRegionStateData data, CultureManagerContainer cultureManagers)
        {
            CultureState = CultureState.CreateFromData(data.cultureStateData, cultureManagers.CultureManager);
            LanguageState = LanguageState.CreateFromData(data.languageStateData, cultureManagers.LanguageManager);
            ReligionState = ReligionState.CreateFromData(data.religionStateData, cultureManagers.ReligionManager);
        }
    }
}
