﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class LandRegionManager
    {
        /* -------- Private Fields -------- */
        private List<LandRegion> landRegions;

        /* -------- Constructors -------- */
        public LandRegionManager()
        {
            landRegions = new List<LandRegion>();
        }

        /* -------- Public Methods -------- */
        public void DrawOverlays(OverlayGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetLandRegionOverlayTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (LandRegion lr in landRegions)
                gm.DrawLandRegionOverlay(lr, vm);
        }
        public void DrawBorders(BorderGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetLandRegionBorderTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (LandRegion lr in landRegions)
                gm.DrawLandRegionBorder(lr, vm);
        }
        public void DrawNameLabels(GraphicsComponent g, NameLabelGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetLandRegionNameLabelTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (LandRegion lr in landRegions)
                gm.DrawLandRegionNameLabel(lr, vm);
        }

        public void AddLandRegion(LandRegion landRegion)
        {
            landRegions.Add(landRegion);
        }

        public LandRegion GetLandRegion(string id)
        {
            foreach (LandRegion lr in landRegions)
                if (lr.ID == id)
                    return lr;

            return null;
        }
        public LandRegion GetMouseOverLandRegion(Vector2 mouseMSPos)
        {
            return landRegions.Find(x => x.MouseIsOver(mouseMSPos));
        }
    }
}
