﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class LandTerritoryManager
    {
        /* -------- Private Fields -------- */
        private List<LandTerritory> landTerritories;

        /* -------- Constructors -------- */
        public LandTerritoryManager()
        {
            landTerritories = new List<LandTerritory>();
        }

        /* -------- Public Methods -------- */
        public void DrawOverlays(OverlayGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetLandTerritoryOverlayTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (LandTerritory lt in landTerritories)
                gm.DrawLandTerritoryOverlay(lt, vm);
        }
        public void DrawBorders(BorderGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetLandTerritoryBorderTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (LandTerritory lt in landTerritories)
                gm.DrawLandTerritoryBorder(lt, vm);
        }
        public void DrawSettlements(SettlementGraphicsManager gm, ViewManager vm)
        {
            foreach (LandTerritory lt in landTerritories)
                gm.DrawSettlement(lt, vm);
        }
        public void DrawRoads(RoadGraphicsManager gm, ViewManager vm)
        {
            foreach (LandTerritory lt in landTerritories)
                foreach (Road r in lt.Roads)
                    gm.DrawRoad(r, lt, vm);
        }
        public void DrawSettlementLabels(SettlementLabelGraphicsManager gm, GraphicsComponent g, CameraManager cm, ViewManager vm)
        {
            foreach (LandTerritory lt in landTerritories)
                gm.DrawSettlementLabel(lt, g, cm, vm);
        }
        public void DrawLandTerritoryTerrain(LandTerrainGraphicsManager gm)
        {
            foreach (LandTerritory lt in landTerritories)
                gm.DrawLandTerritoryTerrain(lt);
        }

        public void AddLandTerritory(LandTerritory landTerritory)
        {
            landTerritories.Add(landTerritory);
        }

        public LandTerritory GetLandTerritory(string id)
        {
            foreach (LandTerritory lt in landTerritories)
                if (lt.ID == id)
                    return lt;

            return null;
        }
        public List<LandTerritory> GetAllLandTerritories()
        {
            return landTerritories;
        }

        public LandTerritory GetMouseOverLandTerritory(Vector2 mouseMSPos)
        {
            return landTerritories.Find(x => x.MouseIsOver(mouseMSPos));
        }
        public Settlement GetMouseOverSettlement(Vector2 mouseMSPos)
        {
            LandTerritory landTerritory = landTerritories.Find(x => x.MouseIsOverSettlement(mouseMSPos));

            return (landTerritory == null) ? null : landTerritory.Settlement;
        }
    }
}
