﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class LandProvinceManager
    {
        /* -------- Private Fields -------- */
        private List<LandProvince> landProvinces;

        /* -------- Constructors -------- */
        public LandProvinceManager()
        {
            landProvinces = new List<LandProvince>();
        }

        /* -------- Public Methods -------- */
        public void DrawOverlays(OverlayGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetLandRegionOverlayTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (LandProvince lp in landProvinces)
                gm.DrawLandProvinceOverlay(lp, vm);
        }
        public void DrawBorders(BorderGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetLandProvinceBorderTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (LandProvince lp in landProvinces)
                gm.DrawLandProvinceBorder(lp, vm);
        }
        public void DrawNameLabels(GraphicsComponent g, NameLabelGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetLandProvinceNameLabelTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (LandProvince lp in landProvinces)
                gm.DrawLandProvinceNameLabel(lp, vm);
        }

        public void AddLandProvince(LandProvince landProvince)
        {
            landProvinces.Add(landProvince);
        }
        public LandProvince GetLandProvince(string id)
        {
            foreach (LandProvince lp in landProvinces)
                if (lp.ID == id)
                    return lp;

            return null;
        }
        public LandProvince GetMouseOverLandProvince(Vector2 mouseMSPos)
        {
            return landProvinces.Find(x => x.MouseIsOver(mouseMSPos));
        }
    }
}
