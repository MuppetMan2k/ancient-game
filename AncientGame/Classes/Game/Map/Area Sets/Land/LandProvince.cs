﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eClimateType
    {
        FROZEN,
        COLD_AND_WET,
        COLD_AND_DRY,
        COOL_AND_WET,
        COOL_AND_DRY,
        WARM_AND_WET,
        WARM_AND_DRY,
        HOT_AND_WET,
        HOT_AND_DRY,
        BAKED
    }

    class LandProvince : AreaSet
    {
        /* -------- Properties -------- */
        // Data
        public NameLabel NameLabel { get; private set; }
        public List<LandRegion> ChildLandRegions { get; private set; }
        public List<LandTerritory> ChildLandTerritories { get; private set; }
        public eClimateType ClimateType { get; private set; }
        // Variables
        public HarvestState HarvestState { get; private set; }

        /* -------- Constructors -------- */
        public LandProvince()
        {
            ChildLandRegions = new List<LandRegion>();
            ChildLandTerritories = new List<LandTerritory>();
        }

        /* -------- Public Methods -------- */
        public string GetNameString()
        {
            string id = "text_land-province_" + ID + "_name";
            return Localization.Localize(id);
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            set.Merge(GetClimateModifierSet(gameManagers));
            set.Merge(HarvestState.GetModifierSet(gameManagers));

            return set;
        }
        public ModifierSet GetClimateModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            ModifierData[] modifierDatas;
            switch (ClimateType)
            {
                case eClimateType.FROZEN:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landProvinceClimateModifierDatas_Frozen;
                    break;
                case eClimateType.COLD_AND_WET:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landProvinceClimateModifierDatas_ColdAndWet;
                    break;
                case eClimateType.COLD_AND_DRY:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landProvinceClimateModifierDatas_ColdAndDry;
                    break;
                case eClimateType.COOL_AND_WET:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landProvinceClimateModifierDatas_CoolAndWet;
                    break;
                case eClimateType.COOL_AND_DRY:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landProvinceClimateModifierDatas_CoolAndDry;
                    break;
                case eClimateType.WARM_AND_WET:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landProvinceClimateModifierDatas_WarmAndWet;
                    break;
                case eClimateType.WARM_AND_DRY:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landProvinceClimateModifierDatas_WarmAndDry;
                    break;
                case eClimateType.HOT_AND_WET:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landProvinceClimateModifierDatas_HotAndWet;
                    break;
                case eClimateType.HOT_AND_DRY:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landProvinceClimateModifierDatas_HotAndDry;
                    break;
                case eClimateType.BAKED:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.landProvinceClimateModifierDatas_Baked;
                    break;
                default:
                    modifierDatas = null;
                    Debug.LogUnrecognizedValueWarning(ClimateType);
                    break;
            }
            if (modifierDatas != null)
                foreach (ModifierData md in modifierDatas)
                    set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Creation Methods -------- */
        public static LandProvince CreateFromData(LandProvinceData data)
        {
            LandProvince landProvince = new LandProvince();

            landProvince.ID = data.id;
            foreach (AreaData ad in data.areaDatas)
            {
                Area area = Area.CreateFromData(ad);
                landProvince.Areas.Add(area);
            }
            landProvince.CalculateBoundingRectangle();
            landProvince.NameLabel = NameLabel.CreateFromData(data.nameLabelData, landProvince.GetNameString());
            landProvince.ClimateType = DataUtilities.ParseClimateType(data.climateType);

            return landProvince;
        }
        public void SetCrossReferences(LandProvinceData data, MapManager mapManager)
        {
            foreach (string id in data.landRegionIDs)
            {
                LandRegion landRegion = mapManager.LandRegionManager.GetLandRegion(id);
                ChildLandRegions.Add(landRegion);
                landRegion.SetParentLandProvince(this);

                foreach (LandTerritory lt in landRegion.ChildLandTerritories)
                {
                    ChildLandTerritories.Add(lt);
                    lt.SetParentLandProvince(this);
                }
            }
        }
        public void SetState(LandProvinceStateData data)
        {
            HarvestState = HarvestState.CreateFromData(data.harvestLevel);
        }
    }
}
