﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class SeaRegionManager
    {
        /* -------- Private Fields -------- */
        private List<SeaRegion> seaRegions;

        /* -------- Constructors -------- */
        public SeaRegionManager()
        {
            seaRegions = new List<SeaRegion>();
        }

        /* -------- Public Methods -------- */
        public void DrawOverlays(OverlayGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetSeaRegionOverlayTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (SeaRegion sr in seaRegions)
                gm.DrawSeaRegionOverlay(sr, vm);
        }
        public void DrawBorders(BorderGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetSeaRegionBorderTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (SeaRegion sr in seaRegions)
                gm.DrawSeaRegionBorder(sr, vm);
        }
        public void DrawNameLabels(GraphicsComponent g, NameLabelGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetSeaRegionNameLabelTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (SeaRegion sr in seaRegions)
                gm.DrawSeaRegionNameLabel(sr, vm);
        }

        public void AddSeaRegion(SeaRegion seaRegion)
        {
            seaRegions.Add(seaRegion);
        }

        public SeaRegion GetSeaRegion(string id)
        {
            return seaRegions.Find(x => x.ID == id);
        }
        public SeaRegion GetMouseOverSeaRegion(Vector2 mouseMSPos)
        {
            return seaRegions.Find(x => x.MouseIsOver(mouseMSPos));
        }
    }
}
