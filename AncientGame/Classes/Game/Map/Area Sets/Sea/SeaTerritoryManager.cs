﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class SeaTerritoryManager
    {
        /* -------- Private Fields -------- */
        private List<SeaTerritory> seaTerritories;

        /* -------- Constructors -------- */
        public SeaTerritoryManager()
        {
            seaTerritories = new List<SeaTerritory>();
        }

        /* -------- Public Methods -------- */
        public void DrawOverlays(OverlayGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetSeaTerritoryOverlayTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (SeaTerritory st in seaTerritories)
                gm.DrawSeaTerritoryOverlay(st, vm);
        }
        public void DrawBorders(BorderGraphicsManager gm, ViewManager vm)
        {
            GraphicsStyle typeStyle = vm.ViewMode.GetSeaTerritoryBorderTypeStyle();
            if (!typeStyle.ShouldDraw)
                return;

            foreach (SeaTerritory st in seaTerritories)
                gm.DrawSeaTerritoryBorder(st, vm);
        }
        public void DrawSeaTerrain(SeaTerrainGraphicsManager seaTerrainGraphicsManager)
        {
            foreach (SeaTerritory st in seaTerritories)
                seaTerrainGraphicsManager.DrawSeaTerritoryTerrain(st);
        }

        public void AddSeaTerritory(SeaTerritory seaTerritory)
        {
            seaTerritories.Add(seaTerritory);
        }

        public SeaTerritory GetSeaTerritory(string id)
        {
            foreach (SeaTerritory st in seaTerritories)
                if (st.ID == id)
                    return st;

            return null;
        }
        public List<SeaTerritory> GetAllSeaTerritories()
        {
            return seaTerritories;
        }
        public SeaTerritory GetMouseOverSeaTerritory(Vector2 mouseMSPos)
        {
            return seaTerritories.Find(x => x.MouseIsOver(mouseMSPos));
        }
    }
}
