﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class SeaRegion : AreaSet
    {
        /* -------- Properties -------- */
        // Data
        public NameLabel NameLabel { get; private set; }
        public List<SeaTerritory> ChildSeaTerritories { get; private set; }

        /* -------- Constructors -------- */
        public SeaRegion()
        {
            ChildSeaTerritories = new List<SeaTerritory>();
        }

        /* -------- Public Methods -------- */
        public string GetNameString()
        {
            string id = "text_sea-region_" + ID + "_name";
            return Localization.Localize(id);
        }

        /* -------- Creation Methods -------- */
        public static SeaRegion CreateFromData(SeaRegionData data)
        {
            SeaRegion seaRegion = new SeaRegion();

            seaRegion.ID = data.id;
            foreach (AreaData ad in data.areaDatas)
            {
                Area area = Area.CreateFromData(ad);
                seaRegion.Areas.Add(area);
            }
            seaRegion.CalculateBoundingRectangle();
            seaRegion.NameLabel = NameLabel.CreateFromData(data.nameLabelData, seaRegion.GetNameString());

            return seaRegion;
        }
        public void SetCrossReferences(SeaRegionData data, MapManager mapManager)
        {
            foreach (string id in data.seaTerritoryIDs)
            {
                SeaTerritory seaTerritory = mapManager.SeaTerritoryManager.GetSeaTerritory(id);
                ChildSeaTerritories.Add(seaTerritory);
                seaTerritory.SetSeaRegion(this);
            }
        }
    }
}
