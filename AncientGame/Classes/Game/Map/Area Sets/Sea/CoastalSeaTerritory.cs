﻿using System.Collections.Generic;

namespace AncientGame
{
    class CoastalSeaTerritory : SeaTerritory
    {
        /* -------- Properties -------- */
        // Data
        public List<AdjacentLandTerritory> AdjacentLandTerritories { get; private set; }

        /* -------- Constructors -------- */
        public CoastalSeaTerritory()
        {
            AdjacentLandTerritories = new List<AdjacentLandTerritory>();
        }
    }
}
