﻿using DataTypes;

namespace AncientGame
{
    class SeaTerritoryVariables
    {
        /* -------- Properties -------- */
        public float MovementPointCost { get; set; }

        /* -------- Public Methods -------- */
        public void SetState(SeaTerritoryStateData data, FactionManager fm)
        {
            MovementPointCost = data.movementPointCost;
        }
    }
}
