﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class SeaTerritory : Territory
    {
        /* -------- Properties -------- */
        // Data
        public List<AdjacentSeaTerritory> AdjacentSeaTerritories { get; private set; }
        public SeaRegion ParentSeaRegion { get; private set; }
        public bool IsDeep { get; private set; }
        // Variables
        public SeaTerritoryVariables Variables { get; private set; }

        /* -------- Constructors -------- */
        public SeaTerritory()
        {
            AdjacentSeaTerritories = new List<AdjacentSeaTerritory>();
            Variables = new SeaTerritoryVariables();
        }

        /* -------- Public Methods -------- */
        public void RecalculateMovementPointCost(GameManagerContainer gameManagers)
        {
            Variables.MovementPointCost = TotalArea * CalibrationManager.CampaignCalibration.Data.seaTerritoryMovementPointsPerArea;
            Variables.MovementPointCost = GetModifierSet(gameManagers).Modify(Variables.MovementPointCost, eModifierType.SEA_TERRITORY_MOVEMENT_POINT_COST);
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (IsDeep)
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.seaTerritoryDeepModifierDatas)
                    set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));

            if (this is CoastalSeaTerritory)
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.seaTerritoryCoastalModifierDatas)
                    set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
            set.TestIsSatisfied(objectSet);

            set.Merge(gameManagers.TurnManager.Date.GetModifierSet(gameManagers));

            return set;
        }

        /* -------- Creation Methods -------- */
        public static SeaTerritory CreateFromData(SeaTerritoryData data)
        {
            SeaTerritory seaTerritory = (data.isCoastal) ? new CoastalSeaTerritory() : new SeaTerritory();
            
            seaTerritory.ID = data.id;
            foreach (AreaData ad in data.areaDatas)
            {
                Area area = Area.CreateFromData(ad);
                seaTerritory.Areas.Add(area);
            }
            seaTerritory.CalculateTotalArea();
            seaTerritory.CalculateBoundingRectangle();
            seaTerritory.IsDeep = data.isDeep;

            return seaTerritory;
        }
        public void SetCrossReferences(SeaTerritoryData data, GameManagerContainer gameManagers)
        {
            if (data.isCoastal)
            {
                foreach (string id in data.adjacentLandTerritoryIDs)
                {
                    LandTerritory landTerritory = gameManagers.MapManager.LandTerritoryManager.GetLandTerritory(id);
                    AdjacentLandTerritory adjacentLandTerritory = new AdjacentLandTerritory(landTerritory, false);
                    (this as CoastalSeaTerritory).AdjacentLandTerritories.Add(adjacentLandTerritory);
                }
            }
            foreach (string id in data.adjacentSeaTerritoryIDs)
            {
                SeaTerritory seaTerritory = gameManagers.MapManager.SeaTerritoryManager.GetSeaTerritory(id);
                AdjacentSeaTerritory adjacentSeaTerritory = new AdjacentSeaTerritory(seaTerritory);
                AdjacentSeaTerritories.Add(adjacentSeaTerritory);
            }
        }
        public void SetSeaRegion(SeaRegion inSeaRegion)
        {
            ParentSeaRegion = inSeaRegion;
        }
    }
}
