﻿using DataTypes;

namespace AncientGame
{
    class SmallIsland : AreaSet
    {
        /* -------- Public Methods -------- */
        public string GetNameString()
        {
            string nameID = "text_small-island_" + ID + "_name";
            string name = Localization.Localize(nameID);

            return name;
        }

        /* -------- Static Methods -------- */
        public static SmallIsland CreateFromData(SmallIslandData data)
        {
            SmallIsland smallIsland = new SmallIsland();

            smallIsland.ID = data.id;
            foreach (AreaData ad in data.areaDatas)
            {
                Area area = Area.CreateFromData(ad);
                smallIsland.Areas.Add(area);
            }
            smallIsland.CalculateBoundingRectangle();

            return smallIsland;
        }
    }
}
