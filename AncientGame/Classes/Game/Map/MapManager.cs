﻿using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class MapManager
    {
        /* -------- Properties -------- */
        public LandTerritoryManager LandTerritoryManager { get; private set; }
        public LandRegionManager LandRegionManager { get; private set; }
        public LandProvinceManager LandProvinceManager { get; private set; }
        public SeaTerritoryManager SeaTerritoryManager { get; private set; }
        public SeaRegionManager SeaRegionManager { get; private set; }
        public LakeManager LakeManager { get; private set; }
        public SmallIslandManager SmallIslandManager { get; private set; }
        public RiverManager RiverManager { get; private set; }
        public LandTradeRouteManager LandTradeRouteManager { get; private set; }
        public SeaTradeRouteManager SeaTradeRouteManager { get; private set; }

        /* -------- Private Fields -------- */
        // Graphics managers
        private LightingManager lightingManager;
        private OverlayGraphicsManager overlayGraphicsManager;
        private BorderGraphicsManager borderGraphicsManager;
        private NameLabelGraphicsManager nameLabelGraphicsManager;
        private LandTerrainGraphicsManager landTerrainGraphicsManager;
        private SeaTerrainGraphicsManager seaTerrainGraphicsManager;
        private SettlementGraphicsManager settlementGraphicsManager;
        private RiverGraphicsManager riverGraphicsManager;
        private SmallIslandGraphicsManager smallIslandGraphicsManager;
        private RoadGraphicsManager roadGraphicsManager;
        private SeaTradeRouteGraphicsManager seaTradeRouteGraphicsManager;
        private TradeVehicleGraphicsManager tradeVehicleGraphicsManager;
        private SettlementLabelGraphicsManager settlementLabelGraphicsManager;
        private StatueGraphicsManager statueGraphicsManager;
        private StatueDragGraphicsManager statueDragGraphicsManager;

        /* -------- Constructors -------- */
        public MapManager(ContentManager content)
        {
            LandTerritoryManager = new LandTerritoryManager();
            LandRegionManager = new LandRegionManager();
            LandProvinceManager = new LandProvinceManager();
            SeaTerritoryManager = new SeaTerritoryManager();
            SeaRegionManager = new SeaRegionManager();
            LakeManager = new LakeManager();
            SmallIslandManager = new SmallIslandManager();
            RiverManager = new RiverManager();
            LandTradeRouteManager = new LandTradeRouteManager();
            SeaTradeRouteManager = new SeaTradeRouteManager();

            lightingManager = new LightingManager();
            overlayGraphicsManager = new OverlayGraphicsManager(content);
            borderGraphicsManager = new BorderGraphicsManager(content);
            nameLabelGraphicsManager = new NameLabelGraphicsManager(content);
            landTerrainGraphicsManager = new LandTerrainGraphicsManager(content);
            seaTerrainGraphicsManager = new SeaTerrainGraphicsManager(content);
            settlementGraphicsManager = new SettlementGraphicsManager(content);
            riverGraphicsManager = new RiverGraphicsManager(content);
            smallIslandGraphicsManager = new SmallIslandGraphicsManager(content);
            roadGraphicsManager = new RoadGraphicsManager(content);
            seaTradeRouteGraphicsManager = new SeaTradeRouteGraphicsManager(content);
            tradeVehicleGraphicsManager = new TradeVehicleGraphicsManager(content);
            settlementLabelGraphicsManager = new SettlementLabelGraphicsManager(content);
            statueGraphicsManager = new StatueGraphicsManager();
            statueDragGraphicsManager = new StatueDragGraphicsManager(content);
        }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw)
        {
            LandTradeRouteManager.Update(gtw);
            SeaTradeRouteManager.Update(gtw);
        }

        public void Draw(GraphicsComponent g, FactionManager fm, StatueDragManager sdm, CameraManager c, ViewManager vm, ContentManager content)
        {
            Benchmarking.StartBenchmark("DrawMap");
            GraphicsStyle landTerrainMetaStyle = vm.ViewMode.GetLandTerrainMetaStyle();
            if (landTerrainMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawTerrain");
                landTerrainGraphicsManager.BeginDrawing();
                LandTerritoryManager.DrawLandTerritoryTerrain(landTerrainGraphicsManager);
                landTerrainGraphicsManager.EndDrawing(g, c, lightingManager);
                Benchmarking.EndBenchmark("DrawTerrain");
            }
            
            GraphicsStyle settlementMetaStyle = vm.ViewMode.GetSettlementMetaStyle();
            if (settlementMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawSettlements");
                settlementGraphicsManager.BeginDrawing();
                LandTerritoryManager.DrawSettlements(settlementGraphicsManager, vm);
                settlementGraphicsManager.EndDrawing(g, c, lightingManager);
                Benchmarking.EndBenchmark("DrawSettlements");
            }
            
            GraphicsStyle roadMetaStyle = vm.ViewMode.GetRoadMetaStyle();
            if (roadMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawRoads");
                roadGraphicsManager.BeginDrawing();
                LandTerritoryManager.DrawRoads(roadGraphicsManager, vm);
                roadGraphicsManager.EndDrawing(g, c, lightingManager);
                Benchmarking.EndBenchmark("DrawRoads");
            }
            
            OverlayMetaStyle overlayMetaStyle = vm.ViewMode.GetOverlayMetaStyle();
            if (overlayMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawLandOverlay");
                overlayGraphicsManager.BeginDrawing();
                LandTerritoryManager.DrawOverlays(overlayGraphicsManager, vm);
                LandRegionManager.DrawOverlays(overlayGraphicsManager, vm);
                LandProvinceManager.DrawOverlays(overlayGraphicsManager, vm);
                overlayGraphicsManager.EndDrawing(g, c, lightingManager, overlayMetaStyle);
                Benchmarking.EndBenchmark("DrawLandOverlay");
            }
            
            SeaTerrainMetaStyle seaTerrainMetaStyle = vm.ViewMode.GetSeaTerrainMetaStyle();
            if (seaTerrainMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawSeaTerrain");
                seaTerrainGraphicsManager.BeginDrawing(seaTerrainMetaStyle);
                SeaTerritoryManager.DrawSeaTerrain(seaTerrainGraphicsManager);
                LakeManager.DrawLakes(seaTerrainGraphicsManager);
                seaTerrainGraphicsManager.EndDrawing(g, c, lightingManager, content);
                Benchmarking.EndBenchmark("DrawSeaTerrain");
            }
            
            GraphicsStyle seaTradeRouteMetaStyle = vm.ViewMode.GetSeaTradeRouteMetaStyle();
            if (seaTradeRouteMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawSeaTradeRoutes");
                seaTradeRouteGraphicsManager.BeginDrawing();
                SeaTradeRouteManager.DrawSeaTradeRoutes(seaTradeRouteGraphicsManager, vm);
                seaTradeRouteGraphicsManager.EndDrawing(g, c, lightingManager);
                Benchmarking.EndBenchmark("DrawSeaTradeRoutes");
            }

            GraphicsStyle tradeVehicleMetaStyle = vm.ViewMode.GetTradeVehicleMetaStyle();
            if (tradeVehicleMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawTradeVehicles");
                tradeVehicleGraphicsManager.BeginDrawing();
                LandTradeRouteManager.DrawTradeVehicles(tradeVehicleGraphicsManager, vm);
                SeaTradeRouteManager.DrawTradeVehicles(tradeVehicleGraphicsManager, vm);
                tradeVehicleGraphicsManager.EndDrawing(g, c, lightingManager);
                Benchmarking.EndBenchmark("DrawTradeVehicles");
            }
            
            if (overlayMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawSeaOverlay");
                overlayGraphicsManager.BeginDrawing();
                SeaTerritoryManager.DrawOverlays(overlayGraphicsManager, vm);
                SeaRegionManager.DrawOverlays(overlayGraphicsManager, vm);
                overlayGraphicsManager.EndDrawing(g, c, lightingManager, overlayMetaStyle);
                Benchmarking.EndBenchmark("DrawSeaOverlay");
            }

            if (sdm.DraggingStatue != null)
            {
                Benchmarking.StartBenchmark("DrawDraggingStatue");
                statueDragGraphicsManager.BeginDrawing();
                sdm.DrawStatueDraggingOverlay(statueDragGraphicsManager);
                statueDragGraphicsManager.EndDrawing(g, c, lightingManager);
                Benchmarking.EndBenchmark("DrawDraggingStatue");
            }
            
            GraphicsStyle borderMetaStyle = vm.ViewMode.GetBorderMetaStyle();
            if (borderMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawBorders");
                borderGraphicsManager.BeginDrawing();
                LandTerritoryManager.DrawBorders(borderGraphicsManager, vm);
                LandRegionManager.DrawBorders(borderGraphicsManager, vm);
                LandProvinceManager.DrawBorders(borderGraphicsManager, vm);
                SeaTerritoryManager.DrawBorders(borderGraphicsManager, vm);
                SeaRegionManager.DrawBorders(borderGraphicsManager, vm);
                borderGraphicsManager.EndDrawing(g, c);
                Benchmarking.EndBenchmark("DrawBorders");
            }
            
            RiverMetaStyle riverMetaStyle = vm.ViewMode.GetRiverMetaStyle();
            if (riverMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawRivers");
                riverGraphicsManager.BeginDrawing();
                RiverManager.DrawRivers(riverGraphicsManager, riverMetaStyle);
                riverGraphicsManager.EndDrawing(g, c, lightingManager);
                Benchmarking.EndBenchmark("DrawRivers");
            }

            GraphicsStyle smallIslandMetaStyle = vm.ViewMode.GetSmallIslandMetaStyle();
            if (smallIslandMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawSmallIslands");
                smallIslandGraphicsManager.BeginDrawing();
                SmallIslandManager.DrawSmallIslands(smallIslandGraphicsManager);
                smallIslandGraphicsManager.EndDrawing(g, c, lightingManager);
                Benchmarking.EndBenchmark("DrawSmallIslands");
            }
            
            GraphicsStyle nameLabelMetaStyle = vm.ViewMode.GetNameLabelMetaStyle();
            if (nameLabelMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawNameLabels");
                nameLabelGraphicsManager.BeginDrawing(content);
                LandRegionManager.DrawNameLabels(g, nameLabelGraphicsManager, vm);
                LandProvinceManager.DrawNameLabels(g, nameLabelGraphicsManager, vm);
                SeaRegionManager.DrawNameLabels(g, nameLabelGraphicsManager, vm);
                nameLabelGraphicsManager.EndDrawing(g, c);
                Benchmarking.EndBenchmark("DrawNameLabels");
            }
            
            GraphicsStyle statueMetaStyle = vm.ViewMode.GetStatueMetaStyle();
            if (statueMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawStatues");
                statueGraphicsManager.BeginDrawing();
                fm.DrawStatues(statueGraphicsManager, g, c);
                statueGraphicsManager.EndDrawing(g);
                Benchmarking.EndBenchmark("DrawStatues");
            }

            GraphicsStyle settlementLabelMetaStyle = vm.ViewMode.GetSettlementLabelMetaStyle();
            if (settlementLabelMetaStyle.ShouldDraw)
            {
                Benchmarking.StartBenchmark("DrawSettlementLabels");
                settlementLabelGraphicsManager.BeginDrawing(g);
                LandTerritoryManager.DrawSettlementLabels(settlementLabelGraphicsManager, g, c, vm);
                settlementLabelGraphicsManager.EndDrawing(g);
                Benchmarking.EndBenchmark("DrawSettlementLabels");
            }
            
            Benchmarking.EndBenchmark("DrawMap");
        }

        public void OnDateLoaded(Date date)
        {
            lightingManager.OnDateLoaded(date);
        }
    }
}
