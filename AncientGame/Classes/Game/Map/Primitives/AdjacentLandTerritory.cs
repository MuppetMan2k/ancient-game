﻿using DataTypes;

namespace AncientGame
{
    class AdjacentLandTerritory
    {
        /* -------- Properties -------- */
        public LandTerritory LandTerritory { get; private set; }
        public bool CrossesRiver { get; private set; }

        /* -------- Constructors -------- */
        public AdjacentLandTerritory(LandTerritory inLandTerritory, bool inCrossesRiver)
        {
            LandTerritory = inLandTerritory;
            CrossesRiver = inCrossesRiver;
        }

        /* -------- Public Methods -------- */
        public int GetMovementPointCost(LandTerritory inOriginLandTerritory, GameManagerContainer gameManagers)
        {
            float cost = (inOriginLandTerritory.Variables.MovementPointCost + LandTerritory.Variables.MovementPointCost) * 0.5f;
            cost = GetModifierSet(gameManagers).Modify(cost, eModifierType.ADJACENT_LAND_TERRITORY_MOVEMENT_POINT_COST);

            return Mathf.ClampLower(Mathf.Round(cost), 1);
        }
        public int GetMovementPointCost(CoastalSeaTerritory inOriginCoastalSeaTerritory, GameManagerContainer gameManagers)
        {
            float cost = (inOriginCoastalSeaTerritory.Variables.MovementPointCost + LandTerritory.Variables.MovementPointCost) * 0.5f;

            cost *= CalibrationManager.CampaignCalibration.Data.seaTerritoryToLandTerritoryMovementPointCostMultiplier;
            cost = GetModifierSet(gameManagers).Modify(cost, eModifierType.ADJACENT_LAND_TERRITORY_MOVEMENT_POINT_COST);

            return Mathf.ClampLower(Mathf.Round(cost), 1);
        }
        public int GetMovementPointCostToSettlement(CoastalSeaTerritory inOriginCoastalSeaTerritory, GameManagerContainer gameManagers)
        {
            float cost = inOriginCoastalSeaTerritory.Variables.MovementPointCost * CalibrationManager.CampaignCalibration.Data.seaTerritoryToSettlementMovementPointCostMultiplier;
            cost = GetModifierSet(gameManagers).Modify(cost, eModifierType.ADJACENT_LAND_TERRITORY_MOVEMENT_POINT_COST);

            return Mathf.ClampLower(Mathf.Round(cost), 1);
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (CrossesRiver)
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.adjacentLandTerritoryCrossesRiverModifierDatas)
                    set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
            set.TestIsSatisfied(objectSet);

            set.Merge(gameManagers.TurnManager.Date.GetModifierSet(gameManagers));

            return set;
        }
    }
}
