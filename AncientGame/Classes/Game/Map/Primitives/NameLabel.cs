﻿using Microsoft.Xna.Framework;
using DataTypes;

namespace AncientGame
{
    class NameLabel
    {
        /* -------- Properties -------- */
        public Vector2 StartMSPosition { get; private set; }
        public Vector2 EndMSPosition { get; private set; }
        public string Text { get; private set; }

        /* -------- Constructors -------- */
        public NameLabel(Vector2 inStartMSPosition, Vector2 inEndMSPosition, string inText)
        {
            StartMSPosition = inStartMSPosition;
            EndMSPosition = inEndMSPosition;
            Text = inText;
        }

        /* -------- Static Methods -------- */
        public static NameLabel CreateFromData(NameLabelData data, string inText)
        {
            string processedText = TextUtilities.ProcessNameLabelText(inText);
            return new NameLabel(data.startPosition, data.endPosition, processedText);
        }
    }
}
