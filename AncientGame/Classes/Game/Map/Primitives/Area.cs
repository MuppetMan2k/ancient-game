﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DataTypes;

namespace AncientGame
{
    class Area
    {
        /* -------- Properties -------- */
        public Vector2[] MSPositions { get; private set; }
        public List<int> Indices { get; private set; }
        public Rectanglef BoundingRectangle { get; private set; }
        public float EnclosedArea { get; private set; }

        /* -------- Constructors -------- */
        public Area(Vector2[] inMSPositions)
        {
            MSPositions = inMSPositions;
        }

        /* -------- Static Methods -------- */
        public static Area CreateFromData(AreaData data)
        {
            Area area = new Area(data.positions);

            area.Indices = AreaUtilities.CalculateAreaIndices(area.MSPositions);
            area.BoundingRectangle = Rectanglef.CreateFromPoints(area.MSPositions);
            area.EnclosedArea = AreaUtilities.CalculateAreaEnclosedArea(area);

            return area;
        }
    }
}
