﻿namespace AncientGame
{
    class AdjacentSeaTerritory
    {
        /* -------- Properties -------- */
        public SeaTerritory SeaTerritory { get; private set; }

        /* -------- Constructors -------- */
        public AdjacentSeaTerritory(SeaTerritory inSeaTerritory)
        {
            SeaTerritory = inSeaTerritory;
        }

        /* -------- Public Methods -------- */
        public int GetMovementPointCost(LandTerritory inOriginLandTerritory, GameManagerContainer gameManagers)
        {
            float cost = SeaTerritory.Variables.MovementPointCost * CalibrationManager.CampaignCalibration.Data.settlementToSeaTerritoryMovementPointCostMultiplier;
            cost = GetModifierSet(gameManagers).Modify(cost, eModifierType.ADJACENT_SEA_TERRITORY_MOVEMENT_POINT_COST);

            return Mathf.ClampLower(Mathf.Round(cost), 1);
        }
        public int GetMovementPointCost(SeaTerritory inOriginSeaTerritory, GameManagerContainer gameManagers)
        {
            float cost = (inOriginSeaTerritory.Variables.MovementPointCost + SeaTerritory.Variables.MovementPointCost) * 0.5f;
            cost = GetModifierSet(gameManagers).Modify(cost, eModifierType.ADJACENT_SEA_TERRITORY_MOVEMENT_POINT_COST);

            return Mathf.ClampLower(Mathf.Round(cost), 1);
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            set.Merge(gameManagers.TurnManager.Date.GetModifierSet(gameManagers));

            return set;
        }
    }
}
