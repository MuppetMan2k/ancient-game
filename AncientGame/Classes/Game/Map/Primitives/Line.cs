﻿using Microsoft.Xna.Framework;
using DataTypes;

namespace AncientGame
{
    class Line
    {
        /* -------- Properties -------- */
        public Vector2[] MSPositions { get; private set; }
        public Vector2[] MSDirections { get; private set; }
        public float[] MSDistances { get; private set; }
        public Rectanglef BoundingRectangle { get; private set; }

        /* -------- Constructors -------- */
        public Line(Vector2[] inMSPositions)
        {
            MSPositions = inMSPositions;

            // Directions and distances
            if (MSPositions.Length >= 2)
            {
                MSDirections = new Vector2[MSPositions.Length - 1];
                MSDistances = new float[MSPositions.Length - 1];

                for (int i = 0; i < MSPositions.Length - 1; i++)
                {
                    Vector2 thisPosition = MSPositions[i];
                    Vector2 nextPosition = MSPositions[i + 1];

                    MSDistances[i] = (nextPosition - thisPosition).Length();
                    MSDirections[i] = (nextPosition - thisPosition) / (MSDistances[i] > 0f ? MSDistances[i] : 1f);
                }
            }

            BoundingRectangle = Rectanglef.CreateFromPoints(MSPositions);
        }

        /* -------- Public Methods -------- */
        public bool MouseIsOver(Vector2 mouseMSPos, float selectionAreaDistance)
        {
            if (!BoundingRectangle.ContainsPosition(mouseMSPos))
                return false;

            // Test nodes
            foreach (Vector2 nodePos in MSPositions)
            {
                float separation = (mouseMSPos - nodePos).LengthSquared();
                if (separation <= selectionAreaDistance * selectionAreaDistance)
                    return true;
            }

            // Test sub-lines
            for (int i = 0; i < MSPositions.Length - 1; i++)
            {
                Vector2 thisPos = MSPositions[i];
                Vector2 nextPos = MSPositions[i + 1];

                float lengthAlongLineFactor;
                float distanceFromLine = VectorUtilities.GetPointDistanceFromLine(thisPos, nextPos, mouseMSPos, out lengthAlongLineFactor);

                if (distanceFromLine > selectionAreaDistance)
                    continue;
                if (lengthAlongLineFactor < 0f)
                    continue;
                if (lengthAlongLineFactor > 1f)
                    continue;

                return true;
            }

            return false;
        }

        public bool IsDegenerate()
        {
            return (MSPositions.Length < 2);
        }
        public float TotalLength()
        {
            float totalLength = 0f;
            foreach (float distance in MSDistances)
                totalLength += distance;

            return totalLength;
        }
        public void GetLineSectionIdxAndTravelFraction(float totalDistanceFraction, bool forwardDirection, out int sectionIdx, out float travelFraction)
        {
            float lengthToCut = TotalLength() * totalDistanceFraction;
            float thisCumulativeLength = 0f;
            if (forwardDirection)
            {
                for (int i = 0; i < MSDistances.Length; i++)
                {
                    float nextCumulativeLength = thisCumulativeLength + MSDistances[i];

                    if (thisCumulativeLength <= lengthToCut && nextCumulativeLength >= lengthToCut)
                    {
                        travelFraction = Mathf.GetLerpFactor(lengthToCut, thisCumulativeLength, nextCumulativeLength);
                        sectionIdx = i;
                        return;
                    }

                    thisCumulativeLength = nextCumulativeLength;
                }
            }
            else
            {
                for (int i = MSDistances.Length - 1; i >= 0; i--)
                {
                    float nextCumulativeLength = thisCumulativeLength + MSDistances[i];

                    if (thisCumulativeLength <= lengthToCut && nextCumulativeLength >= lengthToCut)
                    {
                        travelFraction = Mathf.GetLerpFactor(lengthToCut, thisCumulativeLength, nextCumulativeLength);
                        sectionIdx = i;
                        return;
                    }

                    thisCumulativeLength = nextCumulativeLength;
                }
            }

            sectionIdx = 0;
            travelFraction = 0f;
        }

        /* -------- Static Methods -------- */
        public static Line CreateFromData(LineData data)
        {
            Line line = new Line(data.positions);

            return line;
        }
        public static Line CreateFromOpposingDatas(LineData data1, LineData data2)
        {
            Vector2[] positions = new Vector2[data1.positions.Length + data2.positions.Length - 1];

            for (int i = 0; i < data1.positions.Length; i++)
                positions[i] = data1.positions[i];

            for (int i = 0; i < data2.positions.Length - 1; i++)
                positions[positions.Length - 1 - i] = data2.positions[i];

            Line line = new Line(positions);

            return line;
        }
    }
}
