﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    abstract class TradeRoute
    {
        /* -------- Properties -------- */
        public LandTerritory LandTerritory1 { get; protected set; }
        public LandTerritory LandTerritory2 { get; protected set; }
        public Line Line { get; protected set; }
        public List<TradePath> TradePathsLT1To2 { get; private set; }
        public List<TradePath> TradePathsLT2To1 { get; private set; }
        public TradeVehicleSet VehicleSet { get; private set; }
        public bool IsBlockaded { get; protected set; }

        /* -------- Constructors -------- */
        public TradeRoute()
        {
            TradePathsLT1To2 = new List<TradePath>();
            TradePathsLT2To1 = new List<TradePath>();
            VehicleSet = new TradeVehicleSet();
        }

        /* -------- Private Fields -------- */
        // Constants
        private const float SELECTION_AREA_DISTANCE = 0.6f;

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw)
        {
            VehicleSet.Update(gtw, Line);
        }

        public bool MouseIsOver(Vector2 mouseMSPos)
        {
            if (!Line.BoundingRectangle.ContainsPosition(mouseMSPos))
                return false;

            return Line.MouseIsOver(mouseMSPos, SELECTION_AREA_DISTANCE);
        }

        public abstract void UpdateIsBlockaded(FactionManager fm);

        public abstract int GetMovementPointCost();

        public LandTerritory GetOppositeLandTerritory(LandTerritory lt)
        {
            if (LandTerritory1 == lt)
                return LandTerritory2;

            return LandTerritory1;
        }

        public void ClearTradePaths()
        {
            TradePathsLT1To2.Clear();
            TradePathsLT2To1.Clear();
        }
        public void SetTradePath(TradePath tp, LandTerritory endLandTerritory)
        {
            if (LandTerritory1 == endLandTerritory)
            {
                TradePathsLT2To1.Add(tp);
                return;
            }

            if (LandTerritory2 == endLandTerritory)
            {
                TradePathsLT1To2.Add(tp);
                return;
            }
        }
        public void RecalculateTraffic(bool spawnVehicles)
        {
            VehicleSet.RecalculateLT1ToLT2Traffic(TradePathsLT1To2, spawnVehicles, Line);
            VehicleSet.RecalculateLT2ToLT1Traffic(TradePathsLT2To1, spawnVehicles, Line);
        }
    }
}
