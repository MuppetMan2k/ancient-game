﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DataTypes;

namespace AncientGame
{
    class Road
    {
        /* -------- Properties -------- */
        public Line Line { get; private set; }
        public List<VertexRoad> Vertices { get; private set; }
        public List<int> Indices { get; private set; }

        /* -------- Constructors -------- */
        public Road()
        {
            Vertices = new List<VertexRoad>();
            Indices = new List<int>();
        }

        /* -------- Private Methods --------- */
        private void CalculateVerticesAndIndices()
        {
            float width = CalibrationManager.GraphicsCalibration.Data.roadWidth;

            // Vertices
            Vector2 firstPos = Line.MSPositions[0];
            Vector2 secondPos = Line.MSPositions[1];
            Vector2 startDir = secondPos - firstPos;
            startDir.Normalize();
            Vector2 firstRightMSPos = firstPos + VectorUtilities.RotateVector(startDir, -90f) * width / 2f;
            Vector2 firstLeftMSPos = firstPos + VectorUtilities.RotateVector(startDir, 90f) * width / 2f;

            Vertices.Add(CreateVertex(firstRightMSPos, 0f, true));
            Vertices.Add(CreateVertex(firstLeftMSPos, 0f, false));

            float cumulativeLength = 0f;
            for (int i = 1; i < Line.MSPositions.Length - 1; i++)
            {
                Vector2 prevPos = Line.MSPositions[i - 1];
                Vector2 pos = Line.MSPositions[i];
                Vector2 nextPos = Line.MSPositions[i + 1];

                cumulativeLength += (pos - prevPos).Length();

                Vector2 rightMSPos = VectorUtilities.GetPositionBisectingAngle(prevPos, pos, nextPos, eRotationDirection.CLOCKWISE, width / 2f);
                Vector2 leftMSPos = VectorUtilities.GetPositionBisectingAngle(prevPos, pos, nextPos, eRotationDirection.ANTICLOCKWISE, width / 2f);

                Vertices.Add(CreateVertex(rightMSPos, cumulativeLength, true));
                Vertices.Add(CreateVertex(leftMSPos, cumulativeLength, false));
            }

            Vector2 lastPos = Line.MSPositions[Line.MSPositions.Length - 1];
            Vector2 secondLastPos = Line.MSPositions[Line.MSPositions.Length - 2];
            Vector2 endDir = lastPos - secondLastPos;
            endDir.Normalize();
            Vector2 lastRightMSPos = lastPos + VectorUtilities.RotateVector(endDir, -90f) * width / 2f;
            Vector2 lastLeftMSPos = lastPos + VectorUtilities.RotateVector(endDir, 90f) * width / 2f;

            cumulativeLength += (lastPos - secondLastPos).Length();

            Vertices.Add(CreateVertex(lastRightMSPos, cumulativeLength, true));
            Vertices.Add(CreateVertex(lastLeftMSPos, cumulativeLength, false));

            // Indices
            for (int i = 0; i < Line.MSPositions.Length - 1; i++)
            {
                int rightInd = 2 * i;
                int leftInd = rightInd + 1;
                int nextRightInd = leftInd + 1;
                int nextLeftInd = nextRightInd + 1;

                Indices.Add(leftInd);
                Indices.Add(rightInd);
                Indices.Add(nextRightInd);

                Indices.Add(nextRightInd);
                Indices.Add(nextLeftInd);
                Indices.Add(leftInd);
            }
        }
        private VertexRoad CreateVertex(Vector2 msPos, float lengthAlongRoad, bool right)
        {
            float width = CalibrationManager.GraphicsCalibration.Data.roadWidth;
            Vector2 texCoord = new Vector2(right ? 1f : 0f, lengthAlongRoad / width);
            Vector3 gsPos = SpaceUtilities.ConvertMSToGS(msPos);

            return new VertexRoad(gsPos, texCoord, 0);
        }

        /* -------- Static Methods -------- */
        public static Road CreateFromData(LandTradeRouteData data)
        {
            Road road = new Road();

            road.Line = Line.CreateFromData(data.lineData);
            road.CalculateVerticesAndIndices();

            return road;
        }
    }
}
