﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class LandTradeRouteManager
    {
        /* -------- Private Fields -------- */
        private List<LandTradeRoute> landTradeRoutes;

        /* -------- Constructors -------- */
        public LandTradeRouteManager()
        {
            landTradeRoutes = new List<LandTradeRoute>();
        }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw)
        {
            foreach (LandTradeRoute ltr in landTradeRoutes)
                ltr.Update(gtw);
        }

        public void DrawTradeVehicles(TradeVehicleGraphicsManager gm, ViewManager vm)
        {
            foreach (LandTradeRoute ltr in landTradeRoutes)
                gm.DrawTradeVehicles(ltr, vm);
        }

        public List<LandTradeRoute> GetAllLandTradeRoutes()
        {
            return landTradeRoutes;
        }
        public void AddLandTradeRoute(LandTradeRoute landTradeRoute)
        {
            landTradeRoutes.Add(landTradeRoute);
        }
        public List<LandTradeRoute> GetMouseOverLandTradeRoutes(Vector2 msMousePos)
        {
            return landTradeRoutes.FindAll(x => x.MouseIsOver(msMousePos));
        }
        public void UpdateLandTradeRoutesBlockaded(FactionManager fm)
        {
            foreach (LandTradeRoute ltr in landTradeRoutes)
                ltr.UpdateIsBlockaded(fm);
        }
        public void ClearLandTradeRouteTradePaths()
        {
            foreach (LandTradeRoute ltr in landTradeRoutes)
                ltr.ClearTradePaths();
        }
    }
}
