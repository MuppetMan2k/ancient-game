﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eTradeVehicleMode
    {
        FADING_IN,
        TRAVELLING,
        FADING_OUT
    }
    enum eTradeVehicleDirection
    {
        FORWARD,
        BACKWARD
    }

    class TradeVehicle
    {
        /* -------- Properties -------- */
        public TradeVehicleSet Set { get; private set; }
        public eTradeVehicleMode Mode { get; private set; }
        public eTradeVehicleDirection Direction { get; private set; }
        public Vector2 MSPosition { get; private set; }
        public Vector2 MSDirection { get; private set; }
        public float Alpha { get; private set; }

        /* -------- Private Fields -------- */
        private float speed;
        private Vector2 startNode;
        private Vector2 endNode;
        private float nodeDistance;
        private int lineSectionIdx;
        private float nodeTravelFraction;
        private float fadeTimer;

        /* -------- Constructors -------- */
        public TradeVehicle(TradeVehicleSet inSet, eTradeVehicleDirection inDirection, Line line)
        {
            Set = inSet;
            Mode = eTradeVehicleMode.FADING_IN;
            Direction = inDirection;

            MSPosition = (Direction == eTradeVehicleDirection.FORWARD) ? line.MSPositions[0] : line.MSPositions[line.MSPositions.Length - 1];
            MSDirection = (Direction == eTradeVehicleDirection.FORWARD) ? line.MSDirections[0] : -line.MSDirections[line.MSDirections.Length - 1];
            Alpha = 0f;

            speed = RandomUtilities.GetValueWithError(CalibrationManager.GraphicsCalibration.Data.tradeVehicleSpeed, CalibrationManager.GraphicsCalibration.Data.tradeVehicleSpeedError);
            startNode = MSPosition;
            endNode = (Direction == eTradeVehicleDirection.FORWARD) ? line.MSPositions[1] : line.MSPositions[line.MSPositions.Length - 2];
            nodeDistance = (Direction == eTradeVehicleDirection.FORWARD) ? line.MSDistances[0] : line.MSDistances[line.MSDistances.Length - 1];
            lineSectionIdx = (Direction == eTradeVehicleDirection.FORWARD) ? 0 : line.MSDistances.Length - 1;
            nodeTravelFraction = 0f;
        }
        public TradeVehicle(TradeVehicleSet inSet, eTradeVehicleDirection inDirection, int inLineSectionIdx, float inNodeTravelFraction, Line line)
        {
            Set = inSet;
            Mode = eTradeVehicleMode.TRAVELLING;
            Direction = inDirection;

            speed = RandomUtilities.GetValueWithError(CalibrationManager.GraphicsCalibration.Data.tradeVehicleSpeed, CalibrationManager.GraphicsCalibration.Data.tradeVehicleSpeedError);
            lineSectionIdx = inLineSectionIdx;
            nodeTravelFraction = inNodeTravelFraction;
            startNode = (Direction == eTradeVehicleDirection.FORWARD) ? line.MSPositions[lineSectionIdx] : line.MSPositions[lineSectionIdx + 1];
            endNode = (Direction == eTradeVehicleDirection.FORWARD) ? line.MSPositions[lineSectionIdx + 1] : line.MSPositions[lineSectionIdx];
            nodeDistance = line.MSDistances[lineSectionIdx];

            MSPosition = Vector2.Lerp(startNode, endNode, nodeTravelFraction);
            MSDirection = (Direction == eTradeVehicleDirection.FORWARD) ? line.MSDirections[lineSectionIdx] : -line.MSDirections[lineSectionIdx];
            Alpha = 1f;
        }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw, Line line)
        {
            switch (Mode)
            {
                case eTradeVehicleMode.FADING_IN:
                    Update_FadeIn(gtw);
                    break;
                case eTradeVehicleMode.TRAVELLING:
                    Update_Travel(gtw, line);
                    break;
                case eTradeVehicleMode.FADING_OUT:
                    Update_FadeOut(gtw);
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(Mode);
                    break;
            }
        }

        public void GetVerticesAndIndices(float length, float width, int textureIndex, out List<VertexTradeVehicle> vertices, out List<int> indices)
        {
            vertices = new List<VertexTradeVehicle>();
            indices = new List<int>();

            Vector2 msLeft = VectorUtilities.RotateVector(MSDirection, 90f);

            Vector2 msVertPos;
            // Front-left
            msVertPos = MSPosition + 0.5f * length * MSDirection + 0.5f * width * msLeft;
            vertices.Add(new VertexTradeVehicle(SpaceUtilities.ConvertMSToGS(msVertPos), new Vector2(1f, 0f), textureIndex, Alpha));
            // Front-right
            msVertPos = MSPosition + 0.5f * length * MSDirection - 0.5f * width * msLeft;
            vertices.Add(new VertexTradeVehicle(SpaceUtilities.ConvertMSToGS(msVertPos), new Vector2(1f, 1f), textureIndex, Alpha));
            // Back-left
            msVertPos = MSPosition - 0.5f * length * MSDirection + 0.5f * width * msLeft;
            vertices.Add(new VertexTradeVehicle(SpaceUtilities.ConvertMSToGS(msVertPos), new Vector2(0f, 0f), textureIndex, Alpha));
            // Back-right
            msVertPos = MSPosition - 0.5f * length * MSDirection - 0.5f * width * msLeft;
            vertices.Add(new VertexTradeVehicle(SpaceUtilities.ConvertMSToGS(msVertPos), new Vector2(0f, 1f), textureIndex, Alpha));

            indices.Add(0);
            indices.Add(2);
            indices.Add(3);

            indices.Add(3);
            indices.Add(1);
            indices.Add(0);
        }

        /* -------- Private Methods -------- */
        private void Update_FadeIn(GameTimeWrapper gtw)
        {
            fadeTimer += gtw.ElapsedSeconds;
            Alpha = Mathf.Clamp01(fadeTimer / CalibrationManager.GraphicsCalibration.Data.tradeVehicleFadeTime);

            if (fadeTimer >= CalibrationManager.GraphicsCalibration.Data.tradeVehicleFadeTime)
            {
                Mode = eTradeVehicleMode.TRAVELLING;
                fadeTimer = 0f;
            }
        }
        private void Update_Travel(GameTimeWrapper gtw, Line line)
        {
            float distanceToMove = gtw.ElapsedSeconds * speed;
            nodeTravelFraction += distanceToMove / nodeDistance;

            if (nodeTravelFraction >= 1f)
            {
                if (Direction == eTradeVehicleDirection.FORWARD)
                {
                    if (lineSectionIdx == line.MSDistances.Length - 1)
                    {
                        nodeTravelFraction = 1f;
                        Mode = eTradeVehicleMode.FADING_OUT;
                    }
                    else
                    {
                        nodeTravelFraction = 0f;
                        lineSectionIdx++;
                        startNode = line.MSPositions[lineSectionIdx];
                        endNode = line.MSPositions[lineSectionIdx + 1];
                        nodeDistance = line.MSDistances[lineSectionIdx];
                        MSDirection = line.MSDirections[lineSectionIdx];
                    }
                }
                else
                {
                    if (lineSectionIdx == 0)
                    {
                        nodeTravelFraction = 1f;
                        Mode = eTradeVehicleMode.FADING_OUT;
                    }
                    else
                    {
                        nodeTravelFraction = 0f;
                        lineSectionIdx--;
                        startNode = line.MSPositions[lineSectionIdx + 1];
                        endNode = line.MSPositions[lineSectionIdx];
                        nodeDistance = line.MSDistances[lineSectionIdx];
                        MSDirection = -line.MSDirections[lineSectionIdx];
                    }
                }
            }

            MSPosition = Vector2.Lerp(startNode, endNode, nodeTravelFraction);
        }
        private void Update_FadeOut(GameTimeWrapper gtw)
        {
            fadeTimer += gtw.ElapsedSeconds;
            Alpha = Mathf.Clamp01(1f - fadeTimer / CalibrationManager.GraphicsCalibration.Data.tradeVehicleFadeTime);

            if (fadeTimer >= CalibrationManager.GraphicsCalibration.Data.tradeVehicleFadeTime)
            {
                Set.RemoveTradeVehicle(this);
            }
        }
    }
}
