﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class LandTradeRoute : TradeRoute
    {
        /* -------- Public Methods -------- */
        public override void UpdateIsBlockaded(FactionManager fm)
        {
            List<Force> forces = fm.GetForcesInLandTerritory(LandTerritory1);
            forces.AddRange(fm.GetForcesInLandTerritory(LandTerritory2));

            foreach (Force f in forces)
            {
                LandForce lf = f as LandForce;
                if (lf == null)
                    continue;

                if (lf.Stance == eLandForceStance.LAYING_SIEGE)
                {
                    IsBlockaded = true;
                    return;
                }
            }

            IsBlockaded = false;
        }

        public override int GetMovementPointCost()
        {
            float cost = (LandTerritory1.Variables.MovementPointCost + LandTerritory2.Variables.MovementPointCost) * 0.5f;

            return Mathf.ClampLower(Mathf.Round(cost), 1);
        }

        /* -------- Creation Methods -------- */
        public static LandTradeRoute CreateFromDatas(LandTradeRouteData data1, LandTradeRouteData data2)
        {
            LandTradeRoute landTradeRoute = new LandTradeRoute();

            landTradeRoute.Line = Line.CreateFromOpposingDatas(data1.lineData, data2.lineData);

            return landTradeRoute;
        }
        public void SetCrossReferences(LandTradeRouteData data1, MapManager mapManager)
        {
            LandTerritory1 = mapManager.LandTerritoryManager.GetLandTerritory(data1.originLandTerritoryID);
            LandTerritory2 = mapManager.LandTerritoryManager.GetLandTerritory(data1.destinationLandTerritoryID);
            LandTerritory1.LandTradeRoutes.Add(this);
            LandTerritory2.LandTradeRoutes.Add(this);
        }
    }
}
