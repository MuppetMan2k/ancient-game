﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class SeaTradeRouteManager
    {
        /* -------- Private Fields -------- */
        private List<SeaTradeRoute> seaTradeRoutes;

        /* -------- Constructors -------- */
        public SeaTradeRouteManager()
        {
            seaTradeRoutes = new List<SeaTradeRoute>();
        }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw)
        {
            foreach (SeaTradeRoute str in seaTradeRoutes)
                str.Update(gtw);
        }

        public void DrawSeaTradeRoutes(SeaTradeRouteGraphicsManager gm, ViewManager vm)
        {
            foreach (SeaTradeRoute str in seaTradeRoutes)
                gm.DrawSeaTradeRoute(str, vm);
        }
        public void DrawTradeVehicles(TradeVehicleGraphicsManager gm, ViewManager vm)
        {
            foreach (SeaTradeRoute str in seaTradeRoutes)
                gm.DrawTradeVehicles(str, vm);
        }

        public List<SeaTradeRoute> GetAllSeaTradeRoutes()
        {
            return seaTradeRoutes;
        }
        public void AddSeaTradeRoute(SeaTradeRoute seaTradeRoute)
        {
            seaTradeRoutes.Add(seaTradeRoute);
        }
        public List<SeaTradeRoute> GetMouseOverSeaTradeRoutes(Vector2 msMousePos)
        {
            return seaTradeRoutes.FindAll(x => x.MouseIsOver(msMousePos));
        }
        public void UpdateSeaTradeRoutesBlockaded(FactionManager fm)
        {
            foreach (SeaTradeRoute str in seaTradeRoutes)
                str.UpdateIsBlockaded(fm);
        }
        public void ClearSeaTradeRouteTradePaths()
        {
            foreach (SeaTradeRoute str in seaTradeRoutes)
                str.ClearTradePaths();
        }
    }
}
