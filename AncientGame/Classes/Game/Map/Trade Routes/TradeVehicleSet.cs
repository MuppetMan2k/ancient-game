﻿using System.Collections.Generic;

namespace AncientGame
{
    class TradeVehicleSet
    {
        /* -------- Properties -------- */
        public List<TradeVehicle> TradeVehicles { get; private set; }

        /* -------- Private Fields -------- */
        private List<TradeVehicle> tradeVehicleRemovalList;
        private bool lt1ToLT2SpawnsActivated;
        private float lt1ToLT2Timer;
        private float lt1ToLT2SpawnTime;
        private float lt1ToLT2ThisSpawnTime;
        private bool lt2ToLT1SpawnsActivated;
        private float lt2ToLT1Timer;
        private float lt2ToLT1SpawnTime;
        private float lt2ToLT1ThisSpawnTime;

        /* -------- Constructors -------- */
        public TradeVehicleSet()
        {
            TradeVehicles = new List<TradeVehicle>();
            tradeVehicleRemovalList = new List<TradeVehicle>();
        }

        /* -------- Public Methods -------- */
        public void Update(GameTimeWrapper gtw, Line line)
        {
            foreach (TradeVehicle tv in TradeVehicles)
                tv.Update(gtw, line);

            foreach (TradeVehicle tv in tradeVehicleRemovalList)
                if (!TradeVehicles.Remove(tv))
                    Debug.LogWarning(string.Format("Tried to remove trade vehicle {0} but couldn't find it!", tv));
            tradeVehicleRemovalList.Clear();

            if (lt1ToLT2SpawnsActivated)
            {
                lt1ToLT2Timer += gtw.ElapsedSeconds;
                if (lt1ToLT2Timer >= lt1ToLT2ThisSpawnTime)
                {
                    TradeVehicles.Add(new TradeVehicle(this, eTradeVehicleDirection.FORWARD, line));
                    lt1ToLT2Timer = 0f;
                    lt1ToLT2ThisSpawnTime = RandomUtilities.GetValueWithError(lt1ToLT2SpawnTime, CalibrationManager.GraphicsCalibration.Data.tradeVehicleSpawnTimeError);
                }
            }
            if (lt2ToLT1SpawnsActivated)
            {
                lt2ToLT1Timer += gtw.ElapsedSeconds;
                if (lt2ToLT1Timer >= lt2ToLT1ThisSpawnTime)
                {
                    TradeVehicles.Add(new TradeVehicle(this, eTradeVehicleDirection.BACKWARD, line));
                    lt2ToLT1Timer = 0f;
                    lt2ToLT1ThisSpawnTime = RandomUtilities.GetValueWithError(lt2ToLT1SpawnTime, CalibrationManager.GraphicsCalibration.Data.tradeVehicleSpawnTimeError);
                }
            }
        }

        public void RecalculateLT1ToLT2Traffic(List<TradePath> tradePaths, bool spawnVehicles, Line line)
        {
            if (line.IsDegenerate())
                return;

            float traffic = GetTradePathsTotalQuantity(tradePaths);
            lt1ToLT2SpawnsActivated = (traffic > 0f);
            if (!lt1ToLT2SpawnsActivated)
                return;
            
            lt1ToLT2Timer = 0f;
            float maxTraffic = CalibrationManager.GraphicsCalibration.Data.tradeVehicleMaxTraffic;
            lt1ToLT2SpawnTime = CalibrationManager.GraphicsCalibration.Data.tradeVehicleMinSpawnTime * maxTraffic / Mathf.ClampUpper(traffic, maxTraffic);
            lt1ToLT2SpawnTime = Mathf.ClampUpper(lt1ToLT2SpawnTime, CalibrationManager.GraphicsCalibration.Data.tradeVehicleMaxSpawnTime);
            lt1ToLT2ThisSpawnTime = RandomUtilities.GetValueWithError(lt1ToLT2SpawnTime, CalibrationManager.GraphicsCalibration.Data.tradeVehicleSpawnTimeError);

            if (spawnVehicles)
                SpawnVehicles(eTradeVehicleDirection.FORWARD, line);
        }
        public void RecalculateLT2ToLT1Traffic(List<TradePath> tradePaths, bool spawnVehicles, Line line)
        {
            if (line.IsDegenerate())
                return;

            float traffic = GetTradePathsTotalQuantity(tradePaths);
            lt2ToLT1SpawnsActivated = (traffic > 0f);
            if (!lt2ToLT1SpawnsActivated)
                return;

            lt2ToLT1Timer = 0f;
            float maxTraffic = CalibrationManager.GraphicsCalibration.Data.tradeVehicleMaxTraffic;
            lt2ToLT1SpawnTime = CalibrationManager.GraphicsCalibration.Data.tradeVehicleMinSpawnTime * maxTraffic / Mathf.ClampUpper(traffic, maxTraffic);
            lt2ToLT1SpawnTime = Mathf.ClampUpper(lt2ToLT1SpawnTime, CalibrationManager.GraphicsCalibration.Data.tradeVehicleMaxSpawnTime);
            lt2ToLT1ThisSpawnTime = RandomUtilities.GetValueWithError(lt2ToLT1SpawnTime, CalibrationManager.GraphicsCalibration.Data.tradeVehicleSpawnTimeError);

            if (spawnVehicles)
                SpawnVehicles(eTradeVehicleDirection.BACKWARD, line);
        }

        public void RemoveTradeVehicle(TradeVehicle tv)
        {
            tradeVehicleRemovalList.Add(tv);
        }

        /* -------- Private Methods -------- */
        private float GetTradePathsTotalQuantity(List<TradePath> tradePaths)
        {
            float totalQuantity = 0f;

            foreach (TradePath tp in tradePaths)
                totalQuantity += tp.GetTradedResourcesQuantity();

            return totalQuantity;
        }
        private void SpawnVehicles(eTradeVehicleDirection direction, Line line)
        {
            float spawnTime = (direction == eTradeVehicleDirection.FORWARD) ? lt1ToLT2SpawnTime : lt2ToLT1SpawnTime;
            float timeToDriveLength = line.TotalLength() / CalibrationManager.GraphicsCalibration.Data.tradeVehicleSpeed;

            int numTradeVehicles = Mathf.Ceiling(timeToDriveLength / spawnTime);
            for (int i = 1; i <= numTradeVehicles; i++)
            {
                float totalLengthFraction = i / (numTradeVehicles + 1f);
                int sectionIdx;
                float travelFraction;
                line.GetLineSectionIdxAndTravelFraction(totalLengthFraction, direction == eTradeVehicleDirection.FORWARD, out sectionIdx, out travelFraction);

                TradeVehicles.Add(new TradeVehicle(this, direction, sectionIdx, travelFraction, line));
            }
        }
    }
}
