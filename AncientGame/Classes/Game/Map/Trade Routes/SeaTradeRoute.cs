﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DataTypes;

namespace AncientGame
{
    class SeaTradeRoute : TradeRoute
    {
        /* -------- Properties -------- */
        public List<SeaTerritory> SeaTerritoriesPassedThrough { get; private set; }
        public List<VertexPositionColor> Vertices { get; private set; }
        public List<int> Indices { get; private set; }

        /* -------- Constructors -------- */
        public SeaTradeRoute()
        {
            SeaTerritoriesPassedThrough = new List<SeaTerritory>();
            Vertices = new List<VertexPositionColor>();
            Indices = new List<int>();
        }

        /* -------- Public Methods -------- */
        public override void UpdateIsBlockaded(FactionManager fm)
        {
            foreach (SeaTerritory st in SeaTerritoriesPassedThrough)
            {
                List<Force> forces = fm.GetForcesInSeaTerritory(st);
                foreach (Force f in forces)
                {
                    SeaForce sf = f as SeaForce;
                    if (sf == null)
                        continue;

                    if (sf.Stance == eSeaForceStance.BLOCKADING)
                    {
                        IsBlockaded = true;
                        return;
                    }
                }
            }

            IsBlockaded = false;
        }

        public override int GetMovementPointCost()
        {
            int seaTerritoryNum = SeaTerritoriesPassedThrough.Count;
            float cost = (SeaTerritoriesPassedThrough[0].Variables.MovementPointCost + SeaTerritoriesPassedThrough[seaTerritoryNum - 1].Variables.MovementPointCost) * 0.5f;
            for (int i = 1; i < seaTerritoryNum - 1; i++)
                cost += SeaTerritoriesPassedThrough[i].Variables.MovementPointCost;

            return Mathf.ClampLower(Mathf.Round(cost), 1);
        }

        /* -------- Private Methods --------- */
        private void CalculateVerticesAndIndices()
        {
            float width = CalibrationManager.GraphicsCalibration.Data.seaTradeRouteWidth;
            Color color = CalibrationManager.GraphicsCalibration.Data.seaTradeRouteColor * CalibrationManager.GraphicsCalibration.Data.seaTradeRouteAlpha;

            // Vertices
            Vector2 firstPos = Line.MSPositions[0];
            Vector2 secondPos = Line.MSPositions[1];
            Vector2 startDir = secondPos - firstPos;
            startDir.Normalize();
            Vector2 firstRightMSPos = firstPos + VectorUtilities.RotateVector(startDir, -90f) * width / 2f;
            Vector2 firstLeftMSPos = firstPos + VectorUtilities.RotateVector(startDir, 90f) * width / 2f;

            Vertices.Add(new VertexPositionColor(SpaceUtilities.ConvertMSToGS(firstRightMSPos), color));
            Vertices.Add(new VertexPositionColor(SpaceUtilities.ConvertMSToGS(firstLeftMSPos), color));

            for (int i = 1; i < Line.MSPositions.Length - 1; i++)
            {
                Vector2 prevPos = Line.MSPositions[i - 1];
                Vector2 pos = Line.MSPositions[i];
                Vector2 nextPos = Line.MSPositions[i + 1];

                Vector2 rightMSPos = VectorUtilities.GetPositionBisectingAngle(prevPos, pos, nextPos, eRotationDirection.CLOCKWISE, width / 2f);
                Vector2 leftMSPos = VectorUtilities.GetPositionBisectingAngle(prevPos, pos, nextPos, eRotationDirection.ANTICLOCKWISE, width / 2f);

                Vertices.Add(new VertexPositionColor(SpaceUtilities.ConvertMSToGS(rightMSPos), color));
                Vertices.Add(new VertexPositionColor(SpaceUtilities.ConvertMSToGS(leftMSPos), color));
            }

            Vector2 lastPos = Line.MSPositions[Line.MSPositions.Length - 1];
            Vector2 secondLastPos = Line.MSPositions[Line.MSPositions.Length - 2];
            Vector2 endDir = lastPos - secondLastPos;
            endDir.Normalize();
            Vector2 lastRightMSPos = lastPos + VectorUtilities.RotateVector(endDir, -90f) * width / 2f;
            Vector2 lastLeftMSPos = lastPos + VectorUtilities.RotateVector(endDir, 90f) * width / 2f;

            Vertices.Add(new VertexPositionColor(SpaceUtilities.ConvertMSToGS(lastRightMSPos), color));
            Vertices.Add(new VertexPositionColor(SpaceUtilities.ConvertMSToGS(lastLeftMSPos), color));

            // Indices
            for (int i = 0; i < Line.MSPositions.Length - 1; i++)
            {
                int rightInd = 2 * i;
                int leftInd = rightInd + 1;
                int nextRightInd = leftInd + 1;
                int nextLeftInd = nextRightInd + 1;

                Indices.Add(leftInd);
                Indices.Add(rightInd);
                Indices.Add(nextRightInd);

                Indices.Add(nextRightInd);
                Indices.Add(nextLeftInd);
                Indices.Add(leftInd);
            }
        }

        /* -------- Creation Methods -------- */
        public static SeaTradeRoute CreateFromData(SeaTradeRouteData data)
        {
            SeaTradeRoute seaTradeRoute = new SeaTradeRoute();

            seaTradeRoute.Line = Line.CreateFromData(data.lineData);
            seaTradeRoute.CalculateVerticesAndIndices();
            
            return seaTradeRoute;
        }
        public void SetCrossReferences(SeaTradeRouteData data, MapManager mapManager)
        {
            LandTerritory1 = mapManager.LandTerritoryManager.GetLandTerritory(data.landTerritory1ID);
            LandTerritory2 = mapManager.LandTerritoryManager.GetLandTerritory(data.landTerritory2ID);
            LandTerritory1.SeaTradeRoutes.Add(this);
            LandTerritory2.SeaTradeRoutes.Add(this);
            foreach (string id in data.seaTerritoryPassedThroughIDs)
            {
                SeaTerritory seaTerritory = mapManager.SeaTerritoryManager.GetSeaTerritory(id);
                SeaTerritoriesPassedThrough.Add(seaTerritory);
            }
        }
    }
}
