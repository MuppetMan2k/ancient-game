﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eFollowerType
    {
        DEBUG_FOLLOWER
    }

    class Follower : Character
    {
        /* -------- Properties -------- */
        public eFollowerType Type { get; private set; }

        /* -------- Static Methods -------- */
        public static Follower CreateFromData(FollowerData data, GameManagerContainer gameManagers)
        {
            Character character = Character.CreateFromData(data, gameManagers);
            Follower follower = character as Follower;

            follower.Type = DataUtilities.ParseFollowerType(data.followerType);

            return follower;
        }
    }
}
