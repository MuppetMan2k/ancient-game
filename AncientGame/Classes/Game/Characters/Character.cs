﻿using System.Collections.Generic;
using System.Linq;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eCharacterGender
    {
        MALE,
        FEMALE
    }

    class Character
    {
        /* -------- Properties -------- */
        public int ID { get; private set; }
        public string Name { get; private set; }
        public eCharacterGender Gender { get; private set; }
        public int Age { get; private set; }
        public List<Follower> Followers { get; private set; }
        public CharacterTraitSet Traits { get; private set; }
        public List<string> CompletedAppointmentIDs { get; private set; }
        public CharacterDetailSet Details { get; private set; }

        /* -------- Constructors -------- */
        public Character()
        {
            Followers = new List<Follower>();
            CompletedAppointmentIDs = new List<string>();
        }

        /* -------- Static Methods -------- */
        public static Character CreateFromData(CharacterData data, GameManagerContainer gameManagers)
        {
            Character character = new Character();

            character.ID = data.id;
            character.Name = data.name;
            character.Gender = DataUtilities.ParseCharacterGender(data.gender);
            character.Age = data.age;
            foreach (FollowerData fd in data.followerDatas)
            {
                Follower follower = Follower.CreateFromData(fd, gameManagers);
                character.Followers.Add(follower);
            }
            character.Traits = new CharacterTraitSet();
            foreach (CharacterTraitData ctd in data.characterTraitDatas)
            {
                CharacterTrait trait = CharacterTrait.CreateFromData(ctd);
                character.Traits.Traits.Add(trait);
            }
            character.CompletedAppointmentIDs = data.completedAppointmentIDs.ToList();
            character.Details = CharacterDetailSet.CreateFromData(data.characterDetailSetData, character, gameManagers.CultureManagers);

            gameManagers.CharacterManager.OnCharacterLoaded(character);

            return character;
        }
    }
}
