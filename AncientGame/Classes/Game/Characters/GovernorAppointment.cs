﻿using DataTypes;

namespace AncientGame
{
    class GovernorAppointment : Appointment
    {
        /* -------- Properties -------- */
        public LandProvince LandProvince { get; private set; }

        /* -------- Creation Methods -------- */
        public static GovernorAppointment CreateFromData(GovernorAppointmentData data, LandProvinceManager landProvinceManager)
        {
            Appointment appointment = CreateFromData(data);
            GovernorAppointment governorAppointment = appointment as GovernorAppointment;

            governorAppointment.LandProvince = landProvinceManager.GetLandProvince(data.landProvinceID);

            return governorAppointment;
        }
    }
}
