﻿using DataTypes;

namespace AncientGame
{
    class CharacterDetailSet
    {
        /* -------- Properties -------- */
        public Religion Religion { get; private set; }
        public Culture Culture { get; private set; }
        public Language Language { get; private set; }
        public CharacterStatSet BaseStats { get; private set; }
        public CharacterStatSet AggregateStats { get; private set; }

        /* -------- Public Methods -------- */
        public void CalculateAggregateState(Character parentCharacter)
        {
            // TODO
        }

        /* -------- Static Methods -------- */
        public static CharacterDetailSet CreateFromData(CharacterDetailSetData data, Character parentCharacter, CultureManagerContainer cultureManagers)
        {
            CharacterDetailSet details = new CharacterDetailSet();

            details.Religion = cultureManagers.ReligionManager.GetReligion(data.religionID);
            details.Culture = cultureManagers.CultureManager.GetCulture(data.cultureID);
            details.Language = cultureManagers.LanguageManager.GetLanguage(data.languageID);
            details.BaseStats = CharacterStatSet.CreateFromData(data.baseStats, parentCharacter);
            details.CalculateAggregateState(parentCharacter);

            return details;
        }
    }
}
