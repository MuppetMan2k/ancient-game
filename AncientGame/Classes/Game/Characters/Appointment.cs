﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eAppointmentType
    {
        FACTION_LEADER,
        FACTION_HEIR,
        GOVERNOR,
        OTHER
    }

    class Appointment
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }
        public eAppointmentType Type { get; private set; }
        public AppointmentConditions Conditions { get; private set; }
        public AppointmentCreationConditions CreationConditions { get; private set; }
        public Character AppointedCharacter { get; private set; }

        /* -------- Creation Methods -------- */
        public static Appointment CreateFromData(AppointmentData data)
        {
            Appointment appointment = new Appointment();

            appointment.ID = data.id;
            appointment.Type = DataUtilities.ParseAppointmentType(data.appointmentType);
            appointment.Conditions = AppointmentConditions.CreateFromData(data.appointmentConditionsData);
            appointment.CreationConditions = AppointmentCreationConditions.CreateFromData(data.appointmentCreationConditionsData);

            return appointment;
        }
        public void SetState(AppointmentStateData stateData, FactionCharacterPool factionCharacterPool)
        {
            if (stateData.appointedCharacterID == -1)
            {
                AppointedCharacter = null;
                return;
            }

            AppointedCharacter = factionCharacterPool.Characters.Find(x => x.ID == stateData.appointedCharacterID);
        }
    }
}
