﻿using System.Collections.Generic;

namespace AncientGame
{
    class CharacterTraitSet
    {
        /* -------- Properties -------- */
        public List<CharacterTrait> Traits { get; private set; }

        /* -------- Constructors -------- */
        public CharacterTraitSet()
        {
            Traits = new List<CharacterTrait>();
        }
    }
}
