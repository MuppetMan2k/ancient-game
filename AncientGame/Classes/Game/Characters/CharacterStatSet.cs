﻿using DataTypes;

namespace AncientGame
{
    class CharacterStatSet
    {
        /* -------- Properties -------- */
        public Character ParentCharacter { get; private set; }
        public float Command { get; private set; }
        public float Administration { get; private set; }
        public float Popularity { get; private set; }
        public float Ambition { get; private set; }
        public float Piousness { get; private set; }
        public float Leadership { get; private set; }
        public float Progressiveness { get; private set; }
        public float Honor { get; private set; }
        
        /* -------- Private Fields -------- */
        // Constants
        private const float COMMAND_MAX = 10f;
        private const float COMMAND_MIN = 0f;
        private const float ADMINISTRATION_MAX = 10f;
        private const float ADMINISTRATION_MIN = 0f;
        private const float POPULARITY_MAX = 10f;
        private const float POPULARITY_MIN = -10f;
        private const float AMBITION_MAX = 10f;
        private const float AMBITION_MIN = 0f;
        private const float PIOUSNESS_MAX = 10f;
        private const float PIOUSNESS_MIN = 0f;
        private const float LEADERSHIP_MAX = 10f;
        private const float LEADERSHIP_MIN = 0f;
        private const float PROGRESSIVENESS_MAX = 10f;
        private const float PROGRESSIVENESS_MIN = -10f;
        private const float HONOR_MAX = 10f;
        private const float HONOR_MIN = -10f;
        private const float RELATIONS_MAX = 10f;
        private const float RELATIONS_MIN = -10f;

        /* -------- Constructors -------- */
        public CharacterStatSet(Character inParentCharacter)
        {
            ParentCharacter = inParentCharacter;
        }

        /* -------- Public Methods -------- */
        public float GetCharacterRelations(Character targetCharacter)
        {
            int hash1 = ParentCharacter.ID.GetHashCode();
            int hash2 = targetCharacter.ID.GetHashCode();
            int seed = hash1 + hash2;

            float average = CalibrationManager.CharacterCalibration.Data.characterRelationsAverage;
            int power = CalibrationManager.CharacterCalibration.Data.characterRelationsPower;

            return RandomUtilities.DetermRandomDistributedNumber(seed, RELATIONS_MIN, RELATIONS_MAX, average, power);
        }

        /* -------- Static Methods -------- */
        public static CharacterStatSet CreateFromData(CharacterStatSetData data, Character parentCharacter)
        {
            CharacterStatSet stats = new CharacterStatSet(parentCharacter);

            stats.Command = Mathf.Clamp(data.command, COMMAND_MIN, COMMAND_MAX);
            stats.Administration = Mathf.Clamp(data.administration, ADMINISTRATION_MIN, ADMINISTRATION_MAX);
            stats.Popularity = Mathf.Clamp(data.popularity, POPULARITY_MIN, POPULARITY_MAX);
            stats.Ambition = Mathf.Clamp(data.ambition, AMBITION_MIN, AMBITION_MAX);
            stats.Piousness = Mathf.Clamp(data.piousness, PIOUSNESS_MIN, PIOUSNESS_MAX);
            stats.Leadership = Mathf.Clamp(data.leadership, LEADERSHIP_MIN, LEADERSHIP_MAX);
            stats.Progressiveness = Mathf.Clamp(data.progressiveness, PROGRESSIVENESS_MIN, PROGRESSIVENESS_MAX);
            stats.Honor = Mathf.Clamp(data.honor, HONOR_MIN, HONOR_MAX);

            return stats;
        }
    }
}
