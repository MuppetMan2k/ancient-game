﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class AppointmentManager
    {
        /* -------- Private Fields -------- */
        private Dictionary<string, AppointmentData> appointmentDataDict;

        /* -------- Constructors -------- */
        public AppointmentManager()
        {
            appointmentDataDict = new Dictionary<string, AppointmentData>();
        }

        /* -------- Public Methods -------- */
        public void AddAppointmentData(AppointmentData appointmentData)
        {
            appointmentDataDict.Add(appointmentData.id, appointmentData);
        }

        public AppointmentData GetAppointmentData(string id)
        {
            return appointmentDataDict[id];
        }
    }
}
