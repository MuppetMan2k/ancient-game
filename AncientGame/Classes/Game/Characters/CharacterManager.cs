﻿namespace AncientGame
{
    class CharacterManager
    {
        /* -------- Private Fields -------- */
        private int highestCharacterID;

        /* -------- Constructors -------- */
        public CharacterManager()
        {
            highestCharacterID = 0;
        }

        /* -------- Public Methods -------- */
        public void OnCharacterLoaded(Character c)
        {
            highestCharacterID = Mathf.Max(highestCharacterID, c.ID);
        }
    }
}
