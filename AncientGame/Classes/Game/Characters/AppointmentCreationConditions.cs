﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class AppointmentCreationConditions
    {
        /* -------- Properties -------- */
        public List<eGovernmentGroup> PossibleGovernmentGroups { get; private set; }

        /* -------- Constructors -------- */
        public AppointmentCreationConditions()
        {
            PossibleGovernmentGroups = new List<eGovernmentGroup>();
        }

        /* -------- Static Methods -------- */
        public static AppointmentCreationConditions CreateFromData(AppointmentCreationConditionsData data)
        {
            AppointmentCreationConditions creationConditions = new AppointmentCreationConditions();

            foreach (string pgg in data.possibleGovernmentGroups)
            {
                eGovernmentGroup governmentGroup = DataUtilities.ParseGovernmentGroup(pgg);
                creationConditions.PossibleGovernmentGroups.Add(governmentGroup);
            }

            return creationConditions;
        }
    }
}
