﻿using System.Collections.Generic;
using System.Linq;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eCharacterGenderCondition
    {
        MUST_BE_MALE,
        MUST_BE_FEMALE,
        CAN_BE_EITHER
    }

    class AppointmentConditions
    {
        /* -------- Properties -------- */
        public int MinAge { get; private set; }
        public int MaxAge { get; private set; }
        public eCharacterGenderCondition GenderCondition { get; private set; }
        public List<string> PrerequisiteAppointmentIDs { get; private set; }

        /* -------- Static Methods -------- */
        public static AppointmentConditions CreateFromData(AppointmentConditionsData data)
        {
            AppointmentConditions conditions = new AppointmentConditions();

            conditions.MinAge = data.minAge;
            conditions.MaxAge = data.maxAge;
            conditions.GenderCondition = DataUtilities.ParseCharacterGenderCondition(data.genderCondition);
            conditions.PrerequisiteAppointmentIDs = data.prerequisiteAppointmentIDs.ToList();

            return conditions;
        }
    }
}
