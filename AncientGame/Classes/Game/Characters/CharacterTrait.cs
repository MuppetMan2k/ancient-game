﻿using DataTypes;

namespace AncientGame
{
    class CharacterTrait
    {
        /* -------- Properties -------- */
        public string ID { get; private set; }

        /* -------- Static Methods -------- */
        public static CharacterTrait CreateFromData(CharacterTraitData data)
        {
            CharacterTrait trait = new CharacterTrait();

            trait.ID = data.id;

            return trait;
        }
    }
}
