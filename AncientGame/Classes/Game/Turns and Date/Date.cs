﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eSeason
    {
        SPRING,
        SUMMER,
        AUTUMN,
        WINTER
    }

    class Date
    {
        /* -------- Properties -------- */
        public int Year { get; private set; }
        public int YearlyTurnNumber { get; private set; }

        /* -------- Constructors -------- */
        public Date(int inYear, int inYearlyTurnNumber)
        {
            Year = inYear;
            YearlyTurnNumber = inYearlyTurnNumber;
        }

        /* -------- Public Methods -------- */
        public eSeason GetSeason()
        {
            float yearFraction = (float)YearlyTurnNumber / (float)CalibrationManager.CampaignCalibration.Data.turnsPerYear;

            if (yearFraction > 0.75f)
                return eSeason.WINTER;

            if (yearFraction > 0.5f)
                return eSeason.AUTUMN;

            if (yearFraction > 0.25f)
                return eSeason.SUMMER;

            return eSeason.SPRING;
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            eSeason season = GetSeason();
            ModifierData[] modifierDatas = null;
            switch (season)
            {
                case eSeason.SPRING:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.seasonModifierDatas_Spring;
                    break;
                case eSeason.SUMMER:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.seasonModifierDatas_Summer;
                    break;
                case eSeason.AUTUMN:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.seasonModifierDatas_Autumn;
                    break;
                case eSeason.WINTER:
                    modifierDatas = CalibrationManager.CampaignCalibration.Data.seasonModifierDatas_Winter;
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(season);
                    break;
            }

            if (modifierDatas != null)
                foreach (ModifierData md in modifierDatas)
                    set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));
            set.TestIsSatisfied(objectSet);

            return set;
        }

        public void Iterate()
        {
            if (YearlyTurnNumber >= CalibrationManager.CampaignCalibration.Data.turnsPerYear)
            {
                Year++;
                YearlyTurnNumber = 1;
                return;
            }

            YearlyTurnNumber++;
        }

        /* -------- Static Methods -------- */
        public static Date CreateFromData(DateData data)
        {
            return new Date(data.year, data.yearlyTurnNumber);
        }
    }
}
