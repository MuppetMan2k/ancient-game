﻿namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eTurnMode
    {
        RECALCULATION,
        CONTROL,
        ACCUMULATION
    }

    class TurnManager
    {
        /* -------- Properties -------- */
        public eTurnMode TurnMode { get; private set; }
        public Faction FactionTurn { get; private set; }
        public Date Date { get; private set; }
        public int TurnNumber { get; private set; }

        /* -------- Public Methods -------- */
        public void SetNewCampaignFactionTurn(FactionManager factionManager)
        {
            TurnNumber = 1;
            FactionTurn = GetFirstFactionTurn(factionManager);
            TurnMode = eTurnMode.CONTROL;
        }

        public void OnFinishFactionControl()
        {
            TurnMode = eTurnMode.ACCUMULATION;
        }

        public void SetDate(Date inDate)
        {
            Date = inDate;
        }

        /* -------- Private Methods --------- */
        private void OnFinishFactionTurn(FactionManager factionManager, DelegateUtilities.VoidDelegate onNewFactionSetHandler)
        {
            Faction newFactionTurn = GetNextFactionTurn(factionManager);
            if (newFactionTurn == null)
            {
                // Finished going through the factions, iterate the date and start back at the top
                newFactionTurn = GetFirstFactionTurn(factionManager);
                TurnNumber++;
                Date.Iterate();
            }

            FactionTurn = newFactionTurn;
            TurnMode = eTurnMode.RECALCULATION;
            onNewFactionSetHandler();
        }

        private Faction GetFirstFactionTurn(FactionManager factionManager)
        {
            Faction firstFaction = factionManager.GetFirstAlivePlayerFaction();

            if (firstFaction != null)
                return firstFaction;

            return factionManager.GetFirstAliveNonPlayerFaction();
        }
        private Faction GetNextFactionTurn(FactionManager factionManager)
        {
            if (FactionTurn.IsPlayerControlled)
            {
                Faction nextFaction = factionManager.GetNextAlivePlayerFaction(FactionTurn);

                if (nextFaction == null)
                    nextFaction = factionManager.GetFirstAliveNonPlayerFaction();

                return nextFaction;
            }
            
            return factionManager.GetNextAliveNonPlayerFaction(FactionTurn);
        }
    }
}
