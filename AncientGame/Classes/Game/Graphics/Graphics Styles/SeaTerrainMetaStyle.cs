﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class SeaTerrainMetaStyle : GraphicsStyle
    {
        /* -------- Properties -------- */
        public Vector2 BumpMap1Offset { get; private set; }
        public Vector2 BumpMap2Offset { get; private set; }

        /* -------- Constructors -------- */
        public SeaTerrainMetaStyle(bool inShouldDraw, Vector2 inBumpMap1Offset, Vector2 inBumpMap2Offset)
            : base(inShouldDraw)
        {
            BumpMap1Offset = inBumpMap1Offset;
            BumpMap2Offset = inBumpMap2Offset;
        }
    }
}
