﻿namespace AncientGame
{
    abstract class BorderDrawStyle : GraphicsStyle
    {
        /* -------- Constructors -------- */
        public BorderDrawStyle(bool inShouldDraw)
            : base(inShouldDraw)
        {
        }
    }
}
