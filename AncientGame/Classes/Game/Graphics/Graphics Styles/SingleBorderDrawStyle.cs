﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class SingleBorderDrawStyle : BorderDrawStyle
    {
        /* -------- Properties -------- */
        public float Thickness { get; private set; }
        public Color Color { get; private set; }

        /* -------- Constructors -------- */
        public SingleBorderDrawStyle(bool inShouldDraw, float inThickness, Color inColor)
            : base(inShouldDraw)
        {
            Thickness = inThickness;
            Color = inColor;
        }

        /* -------- Static Instances -------- */
        public new static SingleBorderDrawStyle none = new SingleBorderDrawStyle(false, 0f, Color.Transparent);
    }
}
