﻿namespace AncientGame
{
    class RiverMetaStyle : GraphicsStyle
    {
        /* -------- Properties -------- */
        public float BumpMapYOffset { get; private set; }

        /* -------- Constructors -------- */
        public RiverMetaStyle(bool inShouldDraw, float inBumpMapYOffset)
            : base(inShouldDraw)
        {
            BumpMapYOffset = inBumpMapYOffset;
        }
    }
}
