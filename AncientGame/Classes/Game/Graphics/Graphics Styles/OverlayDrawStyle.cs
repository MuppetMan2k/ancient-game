﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class OverlayDrawStyle : GraphicsStyle
    {
        /* -------- Properties -------- */
        public Color Color1;
        public Color Color2;

        /* -------- Constructors -------- */
        public OverlayDrawStyle(bool inShouldDraw, Color inColor1, Color inColor2)
            : base(inShouldDraw)
        {
            Color1 = inColor1;
            Color2 = inColor2;
        }

        /* -------- Static Instances -------- */
        public new static OverlayDrawStyle none = new OverlayDrawStyle(false, Color.Transparent, Color.Transparent);
    }
}
