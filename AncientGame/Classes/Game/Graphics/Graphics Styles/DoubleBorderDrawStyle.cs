﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class DoubleBorderDrawStyle : BorderDrawStyle
    {
        /* -------- Properties -------- */
        public float OuterThickness { get; private set; }
        public float InnerThickness { get; private set; }
        public Color OuterColor { get; private set; }
        public Color InnerColor { get; private set; }

        /* -------- Constructors -------- */
        public DoubleBorderDrawStyle(bool inShouldDraw, float inOuterThickness, float inInnerThickness, Color inOuterColor, Color inInnerColor)
            : base(inShouldDraw)
        {
            OuterThickness = inOuterThickness;
            InnerThickness = inInnerThickness;
            OuterColor = inOuterColor;
            InnerColor = inInnerColor;
        }

        /* -------- Static Instances -------- */
        public new static DoubleBorderDrawStyle none = new DoubleBorderDrawStyle(false, 0f, 0f, Color.Transparent, Color.Transparent);
    }
}
