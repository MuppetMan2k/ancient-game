﻿namespace AncientGame
{
    class OverlayMetaStyle : GraphicsStyle
    {
        /* -------- Properties -------- */
        public string TechniqueID { get; private set; }
        public float OverlayAlpha { get; private set; }

        /* -------- Constructors -------- */
        public OverlayMetaStyle(bool inShouldDraw, string inTechniqueID, float inOverlayAlpha)
            : base(inShouldDraw)
        {
            TechniqueID = inTechniqueID;
            OverlayAlpha = inOverlayAlpha;
        }

        /* -------- Static Instances -------- */
        public new static OverlayMetaStyle none = new OverlayMetaStyle(false, null, 0f);
    }
}
