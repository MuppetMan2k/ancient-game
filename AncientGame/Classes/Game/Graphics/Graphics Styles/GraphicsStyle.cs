﻿namespace AncientGame
{
    class GraphicsStyle
    {
        /* -------- Properties -------- */
        public bool ShouldDraw { get; private set; }

        /* -------- Constructors -------- */
        public GraphicsStyle(bool inShouldDraw)
        {
            ShouldDraw = inShouldDraw;
        }

        /* -------- Static Instances -------- */
        public static GraphicsStyle none = new GraphicsStyle(false);
    }
}
