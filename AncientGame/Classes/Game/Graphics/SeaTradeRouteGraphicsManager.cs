﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class SeaTradeRouteGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexPositionColor> vertices;
        private List<int> indices;
        private string techniqueID;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;

        /* -------- Constructors -------- */
        public SeaTradeRouteGraphicsManager(ContentManager content)
        {
            vertices = new List<VertexPositionColor>();
            indices = new List<int>();

            techniqueID = "Standard";

            effect = AssetLoader.LoadEffect(CalibrationManager.GraphicsCalibration.Data.seaTradeRouteEffectID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing()
        {
            vertices.Clear();
            indices.Clear();
        }

        public void DrawSeaTradeRoute(SeaTradeRoute str, ViewManager vm)
        {
            GraphicsStyle style = vm.ViewMode.GetSeaTradeRouteDrawStyle(str);

            if (!style.ShouldDraw)
                return;

            AddIndicesAndVertices(str.Indices, str.Vertices);
        }

        public void EndDrawing(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            if (vertices.Count == 0 || indices.Count == 0)
                return;

            SetBuffers(g);
            RenderBuffers(g, cm, lm);
        }

        /* -------- Private Methods --------- */
        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexPositionColor> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexPositionColor.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexPositionColor>(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[techniqueID];
            effect.Parameters["xView"].SetValue(cm.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(cm.Camera.ProjectionMatrix);
            effect.Parameters["xLightColor"].SetValue(lm.LightColor.ToVector3());
            effect.Parameters["xAmbientLightIntensity"].SetValue(lm.AmbientLightIntensity);
            effect.Parameters["xDirectLightIntensity"].SetValue(lm.DirectLightIntensity);
            effect.Parameters["xDirectLightDirection"].SetValue(lm.DirectLightDirection);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
    }
}
