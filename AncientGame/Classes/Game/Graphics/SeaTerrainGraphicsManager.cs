﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class SeaTerrainGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexSeaTerrain> vertices;
        private List<int> indices;
        private string techniqueID;
        private SeaTerrainMetaStyle metaStyle;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;
        private Texture2D texture1;
        private Texture2D texture2;
        private Texture2D texture3;
        private Texture2D bumpMap1;
        private Texture2D bumpMap2;

        /* -------- Constructors -------- */
        public SeaTerrainGraphicsManager(ContentManager content)
        {
            vertices = new List<VertexSeaTerrain>();
            indices = new List<int>();

            techniqueID = "Standard";

            effect = AssetLoader.LoadEffect(CalibrationManager.GraphicsCalibration.Data.seaTerrainEffectID, content);
            texture1 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.seaTerrainTextureID, content);
            texture2 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.seaTerrainCoastalTextureID, content);
            texture3 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.seaTerrainDeepTextureID, content);
            bumpMap1 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.seaTerrainBumpMap1ID, content);
            bumpMap2 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.seaTerrainBumpMap2ID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing(SeaTerrainMetaStyle ms)
        {
            metaStyle = ms;

            vertices.Clear();
            indices.Clear();
        }

        public void DrawSeaTerritoryTerrain(SeaTerritory st)
        {
            int textureIndex;
            if (st is CoastalSeaTerritory)
                textureIndex = 2;
            else if (st.IsDeep)
                textureIndex = 3;
            else
                textureIndex = 1;

            foreach (Area a in st.Areas)
            {
                List<VertexSeaTerrain> areaVerts = new List<VertexSeaTerrain>();

                foreach (Vector2 msPos in a.MSPositions)
                {
                    Vector3 gsPos = SpaceUtilities.ConvertMSToGS(msPos);
                    Vector2 texCoord = GetTextureCoord(msPos);
                    Vector2 bumpMap1Coord = GetBumpMap1Coord(msPos, metaStyle);
                    Vector2 bumpMap2Coord = GetBumpMap2Coord(msPos, metaStyle);
                    VertexSeaTerrain vert = new VertexSeaTerrain(gsPos, texCoord, bumpMap1Coord, bumpMap2Coord, textureIndex);

                    areaVerts.Add(vert);
                }

                AddIndicesAndVertices(a.Indices, areaVerts);
            }
        }
        public void DrawLake(Lake l)
        {
            int textureIndex = 2;

            foreach (Area a in l.Areas)
            {
                List<VertexSeaTerrain> areaVerts = new List<VertexSeaTerrain>();

                foreach (Vector2 msPos in a.MSPositions)
                {
                    Vector3 gsPos = SpaceUtilities.ConvertMSToGS(msPos);
                    Vector2 texCoord = GetTextureCoord(msPos);
                    Vector2 bumpMap1Coord = GetBumpMap1Coord(msPos, metaStyle);
                    Vector2 bumpMap2Coord = GetBumpMap2Coord(msPos, metaStyle);
                    VertexSeaTerrain vert = new VertexSeaTerrain(gsPos, texCoord, bumpMap1Coord, bumpMap2Coord, textureIndex);

                    areaVerts.Add(vert);
                }

                AddIndicesAndVertices(a.Indices, areaVerts);
            }
        }

        public void EndDrawing(GraphicsComponent g, CameraManager cm, LightingManager lm, ContentManager content)
        {
            if (vertices.Count == 0 || indices.Count == 0)
                return;

            SetBuffers(g);
            RenderBuffers(g, cm, lm);
        }

        /* -------- Private Methods --------- */
        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexSeaTerrain> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }

        private Vector2 GetTextureCoord(Vector2 msPos)
        {
            float scale = CalibrationManager.GraphicsCalibration.Data.seaTerrainTextureScale;
            return scale * msPos;
        }
        private Vector2 GetBumpMap1Coord(Vector2 msPos, SeaTerrainMetaStyle style)
        {
            float scale = CalibrationManager.GraphicsCalibration.Data.seaTerrainBumpMap1Scale;
            return scale * (msPos + style.BumpMap1Offset);
        }
        private Vector2 GetBumpMap2Coord(Vector2 msPos, SeaTerrainMetaStyle style)
        {
            float scale = CalibrationManager.GraphicsCalibration.Data.seaTerrainBumpMap2Scale;
            return scale * (msPos + style.BumpMap2Offset);
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexSeaTerrain.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexSeaTerrain>(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData<int>(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[techniqueID];
            effect.Parameters["xView"].SetValue(cm.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(cm.Camera.ProjectionMatrix);
            effect.Parameters["xCameraPosition"].SetValue(cm.Camera.Position);
            effect.Parameters["xLightColor"].SetValue(lm.LightColor.ToVector3());
            effect.Parameters["xAmbientLightIntensity"].SetValue(lm.AmbientLightIntensity);
            effect.Parameters["xDirectLightIntensity"].SetValue(lm.DirectLightIntensity);
            effect.Parameters["xDirectLightDirection"].SetValue(lm.DirectLightDirection);
            effect.Parameters["xSpecularPower"].SetValue(CalibrationManager.GraphicsCalibration.Data.seaTerrainSpecularPower);
            effect.Parameters["xSpecularIntensity"].SetValue(CalibrationManager.GraphicsCalibration.Data.seaTerrainSpecularIntensity);
            effect.Parameters["xTexture1"].SetValue(texture1);
            effect.Parameters["xTexture2"].SetValue(texture2);
            effect.Parameters["xTexture3"].SetValue(texture3);
            effect.Parameters["xBumpMap1"].SetValue(bumpMap1);
            effect.Parameters["xBumpMap2"].SetValue(bumpMap2);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
    }
}
