﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class SettlementGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexSettlement> vertices;
        private List<int> indices;
        private string techniqueID;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;
        private Texture2D texture1;
        // Constants
        private const int NUM_OUTER_VERTS = 10;

        /* -------- Constructors -------- */
        public SettlementGraphicsManager(ContentManager content)
        {
            vertices = new List<VertexSettlement>();
            indices = new List<int>();

            techniqueID = "Standard";

            string effectID = CalibrationManager.GraphicsCalibration.Data.settlementEffectID;
            effect = AssetLoader.LoadEffect(effectID, content);
            texture1 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.settlementTexture1ID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing()
        {
            vertices.Clear();
            indices.Clear();
        }

        public void DrawSettlement(LandTerritory lt, ViewManager vm)
        {
            GraphicsStyle style = vm.ViewMode.GetSettlementDrawStyle(lt);

            if (!style.ShouldDraw)
                return;

            List<VertexSettlement> settlementVerts = new List<VertexSettlement>();
            List<int> settlementIndices = new List<int>();

            float size = GetSettlementSize(lt);
            int textureIndex = GetSettlementTextureIndex(lt);

            Vector3 centerGSPos = SpaceUtilities.ConvertMSToGS(lt.Settlement.MSPosition);
            VertexSettlement centerVert = new VertexSettlement(centerGSPos, new Vector2(0f, 0f), textureIndex);
            settlementVerts.Add(centerVert);

            for (int i = 0; i < NUM_OUTER_VERTS; i++)
            {
                float angle = 360f * (float)i / (float)NUM_OUTER_VERTS;
                Vector2 vecFromCenter = VectorUtilities.CreateVector(angle, size);
                Vector2 msPos = lt.Settlement.MSPosition + vecFromCenter;
                Vector3 gsPos = SpaceUtilities.ConvertMSToGS(msPos);
                Vector2 texCoord = new Vector2(vecFromCenter.X, -vecFromCenter.Y) * CalibrationManager.GraphicsCalibration.Data.settlementTextureScale;
                VertexSettlement vert = new VertexSettlement(gsPos, texCoord, textureIndex);
                settlementVerts.Add(vert);

                settlementIndices.Add(0);
                settlementIndices.Add(i + 1);
                if (i == NUM_OUTER_VERTS - 1)
                    settlementIndices.Add(1);
                else
                    settlementIndices.Add(i + 2);
            }

            AddIndicesAndVertices(settlementIndices, settlementVerts);
        }

        public void EndDrawing(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            if (vertices.Count == 0 || indices.Count == 0)
                return;

            SetBuffers(g);
            RenderBuffers(g, cm, lm);
        }

        /* -------- Private Methods --------- */
        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexSettlement> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }

        private float GetSettlementSize(LandTerritory lt)
        {
            // TODO
            return 1.2f;
        }
        private int GetSettlementTextureIndex(LandTerritory lt)
        {
            // TODO
            return 1;
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexSettlement.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexSettlement>(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData<int>(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[techniqueID];
            effect.Parameters["xView"].SetValue(cm.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(cm.Camera.ProjectionMatrix);
            effect.Parameters["xLightColor"].SetValue(lm.LightColor.ToVector3());
            effect.Parameters["xAmbientLightIntensity"].SetValue(lm.AmbientLightIntensity);
            effect.Parameters["xDirectLightIntensity"].SetValue(lm.DirectLightIntensity);
            effect.Parameters["xDirectLightDirection"].SetValue(lm.DirectLightDirection);
            effect.Parameters["xTexture1"].SetValue(texture1);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
    }
}
