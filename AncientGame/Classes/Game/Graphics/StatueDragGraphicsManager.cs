﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class StatueDragGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexPositionColor> vertices;
        private List<int> indices;
        private string techniqueID;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;
        // Constants
        private const float SETTLEMENT_OVERLAY_SIZE = 2f;
        private const int SETTLEMENT_OVERLAY_NUM_VERTICES = 12;

        /* -------- Constructors -------- */
        public StatueDragGraphicsManager(ContentManager content)
        {
            techniqueID = "Standard";

            effect = AssetLoader.LoadEffect(CalibrationManager.GraphicsCalibration.Data.statueDragEffectID, content);

            vertices = new List<VertexPositionColor>();
            indices = new List<int>();
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing()
        {
            vertices.Clear();
            indices.Clear();
        }

        public void DrawStatueMovementOption(StatueMovementOption smo, StatueDragMouseOver currMouseOver)
        {
            if (smo.LandTerritory != null)
            {
                Color color;
                if (smo.CanAffordCost)
                {
                    if (currMouseOver != null && currMouseOver.LandTerritory != null && currMouseOver.LandTerritory.Equals(smo.LandTerritory))
                        color = CalibrationManager.GraphicsCalibration.Data.statueDragSelectOverlayColor * CalibrationManager.GraphicsCalibration.Data.statueDragOverlayAlpha;
                    else
                        color = CalibrationManager.GraphicsCalibration.Data.statueDragCanMoveOverlayColor * CalibrationManager.GraphicsCalibration.Data.statueDragOverlayAlpha;
                }
                else
                    color = CalibrationManager.GraphicsCalibration.Data.statueDragCannotMoveOverlayColor * CalibrationManager.GraphicsCalibration.Data.statueDragOverlayAlpha;

                DrawLandTerritoryOverlay(smo.LandTerritory, color);
                return;
            }

            if (smo.SeaTerritory != null)
            {
                Color color;
                if (smo.CanAffordCost)
                {
                    if (currMouseOver != null && currMouseOver.SeaTerritory != null && currMouseOver.SeaTerritory.Equals(smo.SeaTerritory))
                        color = CalibrationManager.GraphicsCalibration.Data.statueDragSelectOverlayColor * CalibrationManager.GraphicsCalibration.Data.statueDragOverlayAlpha;
                    else
                        color = CalibrationManager.GraphicsCalibration.Data.statueDragCanMoveOverlayColor * CalibrationManager.GraphicsCalibration.Data.statueDragOverlayAlpha;
                }
                else
                    color = CalibrationManager.GraphicsCalibration.Data.statueDragCannotMoveOverlayColor * CalibrationManager.GraphicsCalibration.Data.statueDragOverlayAlpha;

                DrawSeaTerritoryOverlay(smo.SeaTerritory, color);
                return;
            }

            if (smo.Settlement != null)
            {
                Color color;
                if (smo.CanAffordCost)
                {
                    if (currMouseOver != null && currMouseOver.Settlement != null && currMouseOver.Settlement.Equals(smo.Settlement))
                        color = CalibrationManager.GraphicsCalibration.Data.statueDragSelectOverlayColor * CalibrationManager.GraphicsCalibration.Data.statueDragOverlayAlpha;
                    else
                        color = CalibrationManager.GraphicsCalibration.Data.statueDragCanMoveOverlayColor * CalibrationManager.GraphicsCalibration.Data.statueDragOverlayAlpha;
                }
                else
                    color = CalibrationManager.GraphicsCalibration.Data.statueDragCannotMoveOverlayColor * CalibrationManager.GraphicsCalibration.Data.statueDragOverlayAlpha;

                DrawSettlementOverlay(smo.Settlement, color);
                return;
            }
        }

        public void EndDrawing(GraphicsComponent g, CameraManager c, LightingManager lm)
        {
            if (vertices.Count == 0 || indices.Count == 0)
                return;

            SetBuffers(g);
            RenderBuffers(g, c, lm);
        }

        /* -------- Private Methods --------- */
        private void DrawLandTerritoryOverlay(LandTerritory lt, Color color)
        {
            foreach (Area a in lt.Areas)
            {
                List<VertexPositionColor> verts = new List<VertexPositionColor>();

                foreach (Vector2 msPos in a.MSPositions)
                {
                    VertexPositionColor vert = new VertexPositionColor(SpaceUtilities.ConvertMSToGS(msPos), color);
                    verts.Add(vert);
                }

                AddIndicesAndVertices(a.Indices, verts);
            }
        }
        private void DrawSeaTerritoryOverlay(SeaTerritory st, Color color)
        {
            foreach (Area a in st.Areas)
            {
                List<VertexPositionColor> verts = new List<VertexPositionColor>();

                foreach (Vector2 msPos in a.MSPositions)
                {
                    VertexPositionColor vert = new VertexPositionColor(SpaceUtilities.ConvertMSToGS(msPos), color);
                    verts.Add(vert);
                }

                AddIndicesAndVertices(a.Indices, verts);
            }
        }
        private void DrawSettlementOverlay(Settlement s, Color color)
        {
            List<VertexPositionColor> settlementVerts = new List<VertexPositionColor>();
            List<int> settlementIndices = new List<int>();

            VertexPositionColor centerVert = new VertexPositionColor(SpaceUtilities.ConvertMSToGS(s.MSPosition), color);
            settlementVerts.Add(centerVert);

            for (int i = 0; i < SETTLEMENT_OVERLAY_NUM_VERTICES; i++)
            {
                float angle = 360f * (float)i / (float)SETTLEMENT_OVERLAY_NUM_VERTICES;
                Vector2 msPos = s.MSPosition + VectorUtilities.CreateVector(angle, SETTLEMENT_OVERLAY_SIZE);
                VertexPositionColor vert = new VertexPositionColor(SpaceUtilities.ConvertMSToGS(msPos), color);
                settlementVerts.Add(vert);
            }

            for (int i = 1; i <= SETTLEMENT_OVERLAY_NUM_VERTICES; i++)
            {
                settlementIndices.Add(0);
                settlementIndices.Add(i);
                if (i == SETTLEMENT_OVERLAY_NUM_VERTICES)
                    settlementIndices.Add(1);
                else
                    settlementIndices.Add(i + 1);
            }

            AddIndicesAndVertices(settlementIndices, settlementVerts);
        }

        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexPositionColor> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexPositionColor.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexPositionColor>(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData<int>(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager c, LightingManager lm)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[techniqueID];
            effect.Parameters["xView"].SetValue(c.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(c.Camera.ProjectionMatrix);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
    }
}
