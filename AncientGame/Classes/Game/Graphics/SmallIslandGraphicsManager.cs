﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class SmallIslandGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexSmallIsland> vertices;
        private List<int> indices;
        private string techniqueID;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;
        private Texture2D texture;
        private Texture2D bumpMap;

        /* -------- Constructors -------- */
        public SmallIslandGraphicsManager(ContentManager content)
        {
            vertices = new List<VertexSmallIsland>();
            indices = new List<int>();

            techniqueID = "Standard";

            effect = AssetLoader.LoadEffect(CalibrationManager.GraphicsCalibration.Data.smallIslandEffectID, content);
            texture = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.smallIslandTextureID, content);
            bumpMap = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.smallIslandBumpMapID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing()
        {
            vertices.Clear();
            indices.Clear();
        }

        public void DrawSmallIsland(SmallIsland si)
        {
            foreach (Area a in si.Areas)
            {
                List<VertexSmallIsland> areaVerts = new List<VertexSmallIsland>();

                foreach (Vector2 msPos in a.MSPositions)
                {
                    Vector3 gsPos = SpaceUtilities.ConvertMSToGS(msPos);
                    Vector2 texCoord = GetTextureCoord(msPos);
                    Vector2 bumpMapCoord = GetBumpMapCoord(msPos);
                    VertexSmallIsland vert = new VertexSmallIsland(gsPos, texCoord, bumpMapCoord);
                    areaVerts.Add(vert);
                }

                AddIndicesAndVertices(a.Indices, areaVerts);
            }
        }

        public void EndDrawing(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            if (vertices.Count == 0 || indices.Count == 0)
                return;

            SetBuffers(g);
            RenderBuffers(g, cm, lm);
        }

        /* -------- Private Methods --------- */
        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexSmallIsland> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }

        private Vector2 GetTextureCoord(Vector2 msPos)
        {
            return msPos * CalibrationManager.GraphicsCalibration.Data.smallIslandTextureScale;
        }
        private Vector2 GetBumpMapCoord(Vector2 msPos)
        {
            return msPos * CalibrationManager.GraphicsCalibration.Data.smallIslandBumpMapScale;
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexSmallIsland.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexSmallIsland>(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData<int>(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[techniqueID];
            effect.Parameters["xView"].SetValue(cm.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(cm.Camera.ProjectionMatrix);
            effect.Parameters["xLightColor"].SetValue(lm.LightColor.ToVector3());
            effect.Parameters["xAmbientLightIntensity"].SetValue(lm.AmbientLightIntensity);
            effect.Parameters["xDirectLightIntensity"].SetValue(lm.DirectLightIntensity);
            effect.Parameters["xDirectLightDirection"].SetValue(lm.DirectLightDirection);
            effect.Parameters["xTexture"].SetValue(texture);
            effect.Parameters["xBumpMap"].SetValue(bumpMap);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
    }
}
