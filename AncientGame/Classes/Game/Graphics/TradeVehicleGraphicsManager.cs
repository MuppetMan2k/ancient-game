﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class TradeVehicleGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexTradeVehicle> vertices;
        private List<int> indices;
        private string techniqueID;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;
        private Texture2D landTexture;
        private Texture2D seaTexture;

        /* -------- Constructors -------- */
        public TradeVehicleGraphicsManager(ContentManager content)
        {
            vertices = new List<VertexTradeVehicle>();
            indices = new List<int>();

            techniqueID = "Standard";

            effect = AssetLoader.LoadEffect(CalibrationManager.GraphicsCalibration.Data.tradeVehicleEffectID, content);
            landTexture = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.tradeVehicleLandTextureID, content);
            seaTexture = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.tradeVehicleSeaTextureID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing()
        {
            vertices.Clear();
            indices.Clear();
        }

        public void DrawTradeVehicles(TradeRoute tr, ViewManager vm)
        {
            GraphicsStyle style = vm.ViewMode.GetTradeVehicleDrawStyle(tr);

            if (!style.ShouldDraw)
                return;
            
            foreach (TradeVehicle tv in tr.VehicleSet.TradeVehicles)
            {
                List<VertexTradeVehicle> vehicleVertices;
                List<int> vehicleIndices;
                tv.GetVerticesAndIndices(GetTradeVehicleLength(tr), GetTradeVehicleWidth(tr), GetTradeVehicleTextureIndex(tr), out vehicleVertices, out vehicleIndices);
                AddIndicesAndVertices(vehicleIndices, vehicleVertices);
            }
        }

        public void EndDrawing(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            if (vertices.Count == 0 || indices.Count == 0)
                return;

            SetBuffers(g);
            RenderBuffers(g, cm, lm);
        }

        /* -------- Private Methods -------- */
        private float GetTradeVehicleLength(TradeRoute tr)
        {
            if (tr is LandTradeRoute)
                return CalibrationManager.GraphicsCalibration.Data.tradeVehicleLandLength;
            if (tr is SeaTradeRoute)
                return CalibrationManager.GraphicsCalibration.Data.tradeVehicleSeaLength;

            return 0f;
        }
        private float GetTradeVehicleWidth(TradeRoute tr)
        {
            if (tr is LandTradeRoute)
                return CalibrationManager.GraphicsCalibration.Data.tradeVehicleLandWidth;
            if (tr is SeaTradeRoute)
                return CalibrationManager.GraphicsCalibration.Data.tradeVehicleSeaWidth;

            return 0f;
        }
        private int GetTradeVehicleTextureIndex(TradeRoute tr)
        {
            if (tr is LandTradeRoute)
                return 1;
            if (tr is SeaTradeRoute)
                return 2;

            return 0;
        }

        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexTradeVehicle> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexTradeVehicle.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[techniqueID];
            effect.Parameters["xView"].SetValue(cm.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(cm.Camera.ProjectionMatrix);
            effect.Parameters["xLightColor"].SetValue(lm.LightColor.ToVector3());
            effect.Parameters["xAmbientLightIntensity"].SetValue(lm.AmbientLightIntensity);
            effect.Parameters["xDirectLightIntensity"].SetValue(lm.DirectLightIntensity);
            effect.Parameters["xDirectLightDirection"].SetValue(lm.DirectLightDirection);
            effect.Parameters["xTexture1"].SetValue(landTexture);
            effect.Parameters["xTexture2"].SetValue(seaTexture);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
    }
}
