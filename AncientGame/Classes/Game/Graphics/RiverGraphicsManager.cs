﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RiverGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexRiver> vertices;
        private List<int> indices;
        private string techniqueID;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;
        private Texture2D texture;
        private Texture2D bumpMap;

        /* -------- Constructors -------- */
        public RiverGraphicsManager(ContentManager content)
        {
            vertices = new List<VertexRiver>();
            indices = new List<int>();

            techniqueID = "Standard";

            string effectID = CalibrationManager.GraphicsCalibration.Data.riverEffectID;
            effect = AssetLoader.LoadEffect(effectID, content);
            string textureID = CalibrationManager.GraphicsCalibration.Data.riverTextureID;
            texture = AssetLoader.LoadTexture(textureID, content);
            string bumpMapID = CalibrationManager.GraphicsCalibration.Data.riverBumpMapID;
            bumpMap = AssetLoader.LoadTexture(bumpMapID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing()
        {
            vertices.Clear();
            indices.Clear();
        }

        public void DrawRiver(River r, RiverMetaStyle ms)
        {
            List<VertexRiver> riverVerts = GeneralUtilities.ShallowCloneList(r.Vertices);

            for (int i = 0; i < riverVerts.Count; i++)
            {
                VertexRiver vert = riverVerts[i];
                vert.bumpMapCoords.Y += ms.BumpMapYOffset;
                riverVerts[i] = vert;
            }

            AddIndicesAndVertices(r.Indices, riverVerts);
        }

        public void EndDrawing(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            if (vertices.Count == 0 || indices.Count == 0)
                return;

            SetBuffers(g);
            RenderBuffers(g, cm, lm);
        }

        /* -------- Private Methods --------- */
        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexRiver> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexRiver.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexRiver>(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData<int>(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[techniqueID];
            effect.Parameters["xView"].SetValue(cm.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(cm.Camera.ProjectionMatrix);
            effect.Parameters["xCameraPosition"].SetValue(cm.Camera.Position);
            effect.Parameters["xLightColor"].SetValue(lm.LightColor.ToVector3());
            effect.Parameters["xAmbientLightIntensity"].SetValue(lm.AmbientLightIntensity);
            effect.Parameters["xDirectLightIntensity"].SetValue(lm.DirectLightIntensity);
            effect.Parameters["xDirectLightDirection"].SetValue(lm.DirectLightDirection);
            effect.Parameters["xSpecularPower"].SetValue(CalibrationManager.GraphicsCalibration.Data.riverSpecularPower);
            effect.Parameters["xSpecularIntensity"].SetValue(CalibrationManager.GraphicsCalibration.Data.riverSpecularIntensity);
            effect.Parameters["xTexture"].SetValue(texture);
            effect.Parameters["xBumpMap"].SetValue(bumpMap);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
    }
}
