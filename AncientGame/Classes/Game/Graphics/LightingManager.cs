﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class LightingManager
    {
        /* -------- Properties -------- */
        public Color LightColor { get; private set; }
        public float AmbientLightIntensity { get; private set; }
        public float DirectLightIntensity { get; private set; }
        public Vector3 DirectLightDirection { get; private set; }

        /* -------- Public Methods -------- */
        public void OnTurnChanged(Date date)
        {
            UpdateLighting(date);
        } // TODO - call this method when turn is iterated
        public void OnDateLoaded(Date date)
        {
            UpdateLighting(date);
        }

        /* -------- Private Methods --------- */
        private void UpdateLighting(Date date)
        {
            eSeason season = date.GetSeason();

            switch (season)
            {
                case eSeason.SPRING:
                    LightColor = CalibrationManager.LightingCalibration.Data.springLightColor;
                    AmbientLightIntensity = CalibrationManager.LightingCalibration.Data.springAmbientLightIntensity;
                    DirectLightIntensity = CalibrationManager.LightingCalibration.Data.springDirectLightIntensity;
                    DirectLightDirection = CalibrationManager.LightingCalibration.Data.springDirectLightDirection;
                    DirectLightDirection.Normalize();
                    break;
                case eSeason.SUMMER:
                    LightColor = CalibrationManager.LightingCalibration.Data.summerLightColor;
                    AmbientLightIntensity = CalibrationManager.LightingCalibration.Data.summerAmbientLightIntensity;
                    DirectLightIntensity = CalibrationManager.LightingCalibration.Data.summerDirectLightIntensity;
                    DirectLightDirection = CalibrationManager.LightingCalibration.Data.summerDirectLightDirection;
                    DirectLightDirection.Normalize();
                    break;
                case eSeason.AUTUMN:
                    LightColor = CalibrationManager.LightingCalibration.Data.autumnLightColor;
                    AmbientLightIntensity = CalibrationManager.LightingCalibration.Data.autumnAmbientLightIntensity;
                    DirectLightIntensity = CalibrationManager.LightingCalibration.Data.autumnDirectLightIntensity;
                    DirectLightDirection = CalibrationManager.LightingCalibration.Data.autumnDirectLightDirection;
                    DirectLightDirection.Normalize();
                    break;
                case eSeason.WINTER:
                    LightColor = CalibrationManager.LightingCalibration.Data.winterLightColor;
                    AmbientLightIntensity = CalibrationManager.LightingCalibration.Data.winterAmbientLightIntensity;
                    DirectLightIntensity = CalibrationManager.LightingCalibration.Data.winterDirectLightIntensity;
                    DirectLightDirection = CalibrationManager.LightingCalibration.Data.winterDirectLightDirection;
                    DirectLightDirection.Normalize();
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(season);
                    LightColor = CalibrationManager.LightingCalibration.Data.springLightColor;
                    AmbientLightIntensity = CalibrationManager.LightingCalibration.Data.springAmbientLightIntensity;
                    DirectLightIntensity = CalibrationManager.LightingCalibration.Data.springDirectLightIntensity;
                    DirectLightDirection = CalibrationManager.LightingCalibration.Data.springDirectLightDirection;
                    DirectLightDirection.Normalize();
                    break;
            }
        }
    }
}
