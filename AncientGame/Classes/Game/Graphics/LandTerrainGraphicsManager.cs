﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandTerrainGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexLandTerrain> vertices;
        private List<int> indices;
        private string techniqueID;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;
        private Texture2D texture1;
        private Texture2D bumpMap1;
        private Texture2D texture2;
        private Texture2D bumpMap2;
        private Texture2D texture3;
        private Texture2D bumpMap3;
        private Texture2D texture4;
        private Texture2D bumpMap4;
        private Texture2D texture5;
        private Texture2D bumpMap5;
        private Texture2D texture6;
        private Texture2D bumpMap6;
        private Texture2D texture7;
        private Texture2D bumpMap7;

        /* -------- Constructors -------- */
        public LandTerrainGraphicsManager(ContentManager content)
        {
            vertices = new List<VertexLandTerrain>();
            indices = new List<int>();

            techniqueID = "Standard";

            effect = AssetLoader.LoadEffect(CalibrationManager.GraphicsCalibration.Data.landTerrainEffectID, content);

            texture1 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainLowGrassLandTextureID, content);
            bumpMap1 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainLowGrassLandBumpMapID, content);
            texture2 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainHillyGrassLandTextureID, content);
            bumpMap2 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainHillyGrassLandBumpMapID, content);
            texture3 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainSteppesTextureID, content);
            bumpMap3 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainSteppesBumpMapID, content);
            texture4 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainForestTextureID, content);
            bumpMap4 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainForestBumpMapID, content);
            texture5 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainDesertTextureID, content);
            bumpMap5 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainDesertBumpMapID, content);
            texture6 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainMountainTextureID, content);
            bumpMap6 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainMountainBumpMapID, content);
            texture7 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainMarshTextureID, content);
            bumpMap7 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.landTerrainMarshBumpMapID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing()
        {
            vertices.Clear();
            indices.Clear();
        }

        public void DrawLandTerritoryTerrain(LandTerritory lt)
        {
            int textureIndex = (int)lt.TerrainType + 1;

            foreach (Area a in lt.Areas)
                DrawLandTerrainArea(a, textureIndex);
        }

        public void EndDrawing(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            SetBuffers(g);
            RenderBuffers(g, cm, lm);
        }

        /* -------- Private Methods --------- */
        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexLandTerrain> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }
        private void DrawLandTerrainArea(Area a, int textureIndex)
        {
            List<VertexLandTerrain> areaVertices = new List<VertexLandTerrain>();

            foreach (Vector2 position in a.MSPositions)
            {
                Vector3 gsPos = SpaceUtilities.ConvertMSToGS(position);
                Vector2 texCoords = GetVertexTexCoord(position);
                Vector2 bumpMapCoords = GetVertexBumpMapCoord(position);
                VertexLandTerrain vert = new VertexLandTerrain(gsPos, texCoords, bumpMapCoords, textureIndex);

                areaVertices.Add(vert);
            }

            AddIndicesAndVertices(a.Indices, areaVertices);
        }

        private Vector2 GetVertexTexCoord(Vector2 position)
        {
            float x = position.X * CalibrationManager.GraphicsCalibration.Data.landTerrainTextureScale;
            float y = position.Y * CalibrationManager.GraphicsCalibration.Data.landTerrainTextureScale;
            return new Vector2(x, y);
        }
        private Vector2 GetVertexBumpMapCoord(Vector2 position)
        {
            float x = position.X * CalibrationManager.GraphicsCalibration.Data.landTerrainBumpMapScale;
            float y = position.Y * CalibrationManager.GraphicsCalibration.Data.landTerrainBumpMapScale;
            return new Vector2(x, y);
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexLandTerrain.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexLandTerrain>(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData<int>(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[techniqueID];
            effect.Parameters["xView"].SetValue(cm.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(cm.Camera.ProjectionMatrix);
            effect.Parameters["xLightColor"].SetValue(lm.LightColor.ToVector3());
            effect.Parameters["xAmbientLightIntensity"].SetValue(lm.AmbientLightIntensity);
            effect.Parameters["xDirectLightIntensity"].SetValue(lm.DirectLightIntensity);
            effect.Parameters["xDirectLightDirection"].SetValue(lm.DirectLightDirection);
            effect.Parameters["xTexture1"].SetValue(texture1);
            effect.Parameters["xBumpMap1"].SetValue(bumpMap1);
            effect.Parameters["xTexture2"].SetValue(texture2);
            effect.Parameters["xBumpMap2"].SetValue(bumpMap2);
            effect.Parameters["xTexture3"].SetValue(texture3);
            effect.Parameters["xBumpMap3"].SetValue(bumpMap3);
            effect.Parameters["xTexture4"].SetValue(texture4);
            effect.Parameters["xBumpMap4"].SetValue(bumpMap4);
            effect.Parameters["xTexture5"].SetValue(texture5);
            effect.Parameters["xBumpMap5"].SetValue(bumpMap5);
            effect.Parameters["xTexture6"].SetValue(texture6);
            effect.Parameters["xBumpMap6"].SetValue(bumpMap6);
            effect.Parameters["xTexture7"].SetValue(texture7);
            effect.Parameters["xBumpMap7"].SetValue(bumpMap7);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
        private Texture2D GetLandTerrainTexture(string landRegionID, ContentManager content)
        {
            string texID = "debug-land-terrain-texture"; // TODO
            return AssetLoader.LoadTexture(texID, content);
        }
        private Texture2D GetLandTerrainBumpMap(string landRegionID, ContentManager content)
        {
            string texID = "debug-land-terrain-bump-map"; // TODO
            return AssetLoader.LoadTexture(texID, content);
        }
    }
}
