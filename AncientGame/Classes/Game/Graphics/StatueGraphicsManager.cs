﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class StatueGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<Statue> statues;
        private List<StatueSettlementStandard> settlementStandards;
        // Constants
        private const float STATUE_DRAG_ALPHA = 0.4f;

        /* -------- Constructors -------- */
        public StatueGraphicsManager()
        {
            statues = new List<Statue>();
            settlementStandards = new List<StatueSettlementStandard>();
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing()
        {
            statues.Clear();
            settlementStandards.Clear();
        }

        public void DrawStatue(Statue s, CameraManager cm, GraphicsComponent g)
        {
            if (s.Position.IsInSettlement)
            {
                StatueSettlementStandard standard = settlementStandards.Find(x => x.Settlement == s.Position.Settlement);
                if (standard != null)
                {
                    standard.Statues.Add(s);
                    return;
                }

                settlementStandards.Add(new StatueSettlementStandard(s, cm.Camera, g));
                return;
            }

            statues.Add(s);
        }

        public void EndDrawing(GraphicsComponent g)
        {
            List<StatueRender> statueRenders = new List<StatueRender>();
            foreach (Statue s in statues)
                statueRenders.Add(new StatueRender(s));
            foreach (StatueSettlementStandard sss in settlementStandards)
                statueRenders.Add(new StatueRender(sss));

            IOrderedEnumerable<StatueRender> orderedStatueRenders = statueRenders.OrderBy(x => x.RectangleBottomY);

            g.SpriteBatch.Begin();
            foreach (StatueRender sr in orderedStatueRenders)
            {
                if (sr.Statue != null)
                    SBDrawStatue(sr.Statue, g);
                if (sr.Standard != null)
                    SBDrawSettlementStandard(sr.Standard, g);
            }
            g.SpriteBatch.End();
        }

        /* -------- Private Methods --------- */
        private void SBDrawStatue(Statue s, GraphicsComponent g)
        {
            float alpha = s.IsBeingDragged ? STATUE_DRAG_ALPHA : 1f;

            int standardSize = Mathf.Round(CalibrationManager.GraphicsCalibration.Data.statueStandardHeightFraction * s.Rectangle.Height);
            int standardOffset = Mathf.Round(CalibrationManager.GraphicsCalibration.Data.statueStandardOffsetFraction * s.Rectangle.Height);
            Rectangle standardRectangle = new Rectangle(s.Rectangle.Center.X - standardSize / 2, s.Rectangle.Top - standardOffset - standardSize, standardSize, standardSize);
            int standardPoleWidth = Mathf.Round(CalibrationManager.UICalibration.Data.standardPoleWidthFraction * standardSize);
            int standardPoleHeight = standardRectangle.Height + standardOffset + s.Rectangle.Height;
            Rectangle standardPoleRect = new Rectangle(standardRectangle.Center.X - standardPoleWidth / 2, standardRectangle.Top, standardPoleWidth, standardPoleHeight);

            if (!s.IsBeingDragged)
            {
                g.SpriteBatch.Draw(s.StandardPoleTex, standardPoleRect, Color.White);
                Standard.DrawStandardGraphics(g, s.OwningFaction, standardRectangle);
            }
            g.SpriteBatch.Draw(s.StatueTex, s.Rectangle, Color.White * alpha);
        }
        private void SBDrawSettlementStandard(StatueSettlementStandard sss, GraphicsComponent g)
        {
            g.SpriteBatch.Draw(sss.Statues[0].StandardPoleTex, sss.PoleRectangle, Color.White);
            Standard.DrawStandardGraphics(g, sss.Faction, sss.Rectangle);
        }
    }
}
