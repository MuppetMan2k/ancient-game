﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class BorderGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexBorder> vertices;
        private List<int> indices;
        private string techniqueID;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;

        /* -------- Constructors -------- */
        public BorderGraphicsManager(ContentManager content)
        {
            vertices = new List<VertexBorder>();
            indices = new List<int>();

            techniqueID = "Standard";

            string effectID = CalibrationManager.GraphicsCalibration.Data.landBorderEffectID;
            effect = AssetLoader.LoadEffect(effectID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing()
        {
            vertices.Clear();
            indices.Clear();
        }

        public void DrawLandTerritoryBorder(LandTerritory lt, ViewManager vm)
        {
            BorderDrawStyle style = vm.ViewMode.GetLandTerritoryBorderDrawStyle(lt);

            if (!style.ShouldDraw)
                return;

            DrawBordersForAreas(lt.Areas, style);
        }
        public void DrawLandRegionBorder(LandRegion lr, ViewManager vm)
        {
            BorderDrawStyle style = vm.ViewMode.GetLandRegionBorderDrawStyle(lr);

            if (!style.ShouldDraw)
                return;

            DrawBordersForAreas(lr.Areas, style);
        }
        public void DrawLandProvinceBorder(LandProvince lp, ViewManager vm)
        {
            BorderDrawStyle style = vm.ViewMode.GetLandProvinceBorderDrawStyle(lp);

            if (!style.ShouldDraw)
                return;

            DrawBordersForAreas(lp.Areas, style);
        }
        public void DrawSeaTerritoryBorder(SeaTerritory st, ViewManager vm)
        {
            BorderDrawStyle style = vm.ViewMode.GetSeaTerritoryBorderDrawStyle(st);

            if (!style.ShouldDraw)
                return;

            DrawBordersForAreas(st.Areas, style);
        }
        public void DrawSeaRegionBorder(SeaRegion sr, ViewManager vm)
        {
            BorderDrawStyle style = vm.ViewMode.GetSeaRegionBorderDrawStyle(sr);

            if (!style.ShouldDraw)
                return;

            DrawBordersForAreas(sr.Areas, style);
        }

        public void EndDrawing(GraphicsComponent g, CameraManager c)
        {
            if (vertices.Count == 0 || indices.Count == 0)
                return;

            SetBuffers(g);
            RenderBuffers(g, c);
        }

        /* -------- Private Methods --------- */
        private void DrawBordersForAreas(List<Area> areas, BorderDrawStyle style)
        {
            SingleBorderDrawStyle singleBorderStyle = style as SingleBorderDrawStyle;
            if (singleBorderStyle != null)
            {
                foreach (Area a in areas)
                    DrawSingleBorder(a, singleBorderStyle);
            }

            DoubleBorderDrawStyle doubleBorderStyle = style as DoubleBorderDrawStyle;
            if (doubleBorderStyle != null)
            {
                foreach (Area a in areas)
                    DrawDoubleBorder(a, doubleBorderStyle);
            }
        }
        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexBorder> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }

        private void DrawSingleBorder(Area a, SingleBorderDrawStyle style)
        {
            List<VertexBorder> areaVerts = new List<VertexBorder>();
            List<int> areaIndices = new List<int>();

            int positionNum = a.MSPositions.Length;
            for (int i = 0; i < positionNum; i++)
            {
                Vector2 position = a.MSPositions[i];
                Vector2 prevPosition = GeneralUtilities.GetFromCircularArray(a.MSPositions, i - 1);
                Vector2 nextPosition = GeneralUtilities.GetFromCircularArray(a.MSPositions, i + 1);

                Vector2 innerPosition = VectorUtilities.GetPositionBisectingAngle(prevPosition, position, nextPosition, eRotationDirection.ANTICLOCKWISE, style.Thickness);

                VertexBorder outerVert = new VertexBorder(SpaceUtilities.ConvertMSToGS(position), style.Color);
                VertexBorder innerVert = new VertexBorder(SpaceUtilities.ConvertMSToGS(innerPosition), style.Color);

                areaVerts.Add(outerVert);
                areaVerts.Add(innerVert);

                int outerInd = 2 * i;
                int innerInd = 2 * i + 1;

                int nextOuterInd;
                int nextInnerInd;
                if (i == positionNum - 1)
                {
                    nextOuterInd = 0;
                    nextInnerInd = 1;
                }
                else
                {
                    nextOuterInd = innerInd + 1;
                    nextInnerInd = nextOuterInd + 1;
                }

                areaIndices.Add(outerInd);
                areaIndices.Add(nextOuterInd);
                areaIndices.Add(innerInd);

                areaIndices.Add(innerInd);
                areaIndices.Add(nextOuterInd);
                areaIndices.Add(nextInnerInd);
            }

            AddIndicesAndVertices(areaIndices, areaVerts);
        }
        private void DrawDoubleBorder(Area a, DoubleBorderDrawStyle style)
        {
            List<VertexBorder> areaVerts = new List<VertexBorder>();
            List<int> areaIndices = new List<int>();

            int positionNum = a.MSPositions.Length;
            for (int i = 0; i < positionNum; i++)
            {
                Vector2 position = a.MSPositions[i];
                Vector2 prevPosition = GeneralUtilities.GetFromCircularArray(a.MSPositions, i - 1);
                Vector2 nextPosition = GeneralUtilities.GetFromCircularArray(a.MSPositions, i + 1);

                Vector2 firstInnerPosition = VectorUtilities.GetPositionBisectingAngle(prevPosition, position, nextPosition, eRotationDirection.ANTICLOCKWISE, style.OuterThickness);
                Vector2 secondInnerPosition = VectorUtilities.GetPositionBisectingAngle(prevPosition, position, nextPosition, eRotationDirection.ANTICLOCKWISE, style.OuterThickness + style.InnerThickness);

                VertexBorder outerOuterVert = new VertexBorder(SpaceUtilities.ConvertMSToGS(position), style.OuterColor);
                VertexBorder outerInnerVert = new VertexBorder(SpaceUtilities.ConvertMSToGS(firstInnerPosition), style.OuterColor);
                VertexBorder innerOuterVert = new VertexBorder(SpaceUtilities.ConvertMSToGS(firstInnerPosition), style.InnerColor);
                VertexBorder innerInnerVert = new VertexBorder(SpaceUtilities.ConvertMSToGS(secondInnerPosition), style.InnerColor);

                areaVerts.Add(outerOuterVert);
                areaVerts.Add(outerInnerVert);
                areaVerts.Add(innerOuterVert);
                areaVerts.Add(innerInnerVert);

                int outerOuterInd = 4 * i;
                int outerInnerInd = 4 * i + 1;
                int innerOuterInd = 4 * i + 2;
                int innerInnerInd = 4 * i + 3;

                int nextOuterOuterInd;
                int nextOuterInnerInd;
                int nextInnerOuterInd;
                int nextInnerInnerInd;
                if (i == positionNum - 1)
                {
                    nextOuterOuterInd = 0;
                    nextOuterInnerInd = 1;
                    nextInnerOuterInd = 2;
                    nextInnerInnerInd = 3;
                }
                else
                {
                    nextOuterOuterInd = innerInnerInd + 1;
                    nextOuterInnerInd = nextOuterOuterInd + 1;
                    nextInnerOuterInd = nextOuterInnerInd + 1;
                    nextInnerInnerInd = nextInnerOuterInd + 1;
                }

                areaIndices.Add(outerOuterInd);
                areaIndices.Add(nextOuterOuterInd);
                areaIndices.Add(outerInnerInd);

                areaIndices.Add(outerInnerInd);
                areaIndices.Add(nextOuterOuterInd);
                areaIndices.Add(nextOuterInnerInd);

                areaIndices.Add(innerOuterInd);
                areaIndices.Add(nextInnerOuterInd);
                areaIndices.Add(innerInnerInd);

                areaIndices.Add(innerInnerInd);
                areaIndices.Add(nextInnerOuterInd);
                areaIndices.Add(nextInnerInnerInd);
            }

            AddIndicesAndVertices(areaIndices, areaVerts);
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexBorder.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexBorder>(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData<int>(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager c)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[techniqueID];
            effect.Parameters["xView"].SetValue(c.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(c.Camera.ProjectionMatrix);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
    }
}
