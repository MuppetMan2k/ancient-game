﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class OverlayGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexOverlay> vertices;
        private List<int> indices;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;

        /* -------- Constructors -------- */
        public OverlayGraphicsManager(ContentManager content)
        {
            vertices = new List<VertexOverlay>();
            indices = new List<int>();

            string effectID = CalibrationManager.GraphicsCalibration.Data.overlayEffectID;
            effect = AssetLoader.LoadEffect(effectID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing()
        {
            vertices.Clear();
            indices.Clear();
        }

        public void DrawLandTerritoryOverlay(LandTerritory lt, ViewManager vm)
        {
            OverlayDrawStyle style = vm.ViewMode.GetLandTerritoryOverlayDrawStyle(lt);

            if (!style.ShouldDraw)
                return;

            DrawOverlayAreas(lt.Areas, style);
        }
        public void DrawLandRegionOverlay(LandRegion lr, ViewManager vm)
        {
            OverlayDrawStyle style = vm.ViewMode.GetLandRegionOverlayDrawStyle(lr);

            if (!style.ShouldDraw)
                return;

            DrawOverlayAreas(lr.Areas, style);
        }
        public void DrawLandProvinceOverlay(LandProvince lp, ViewManager vm)
        {
            OverlayDrawStyle style = vm.ViewMode.GetLandProvinceOverlayDrawStyle(lp);

            if (!style.ShouldDraw)
                return;

            DrawOverlayAreas(lp.Areas, style);
        }
        public void DrawSeaTerritoryOverlay(SeaTerritory st, ViewManager vm)
        {
            OverlayDrawStyle style = vm.ViewMode.GetSeaTerritoryOverlayDrawStyle(st);

            if (!style.ShouldDraw)
                return;

            DrawOverlayAreas(st.Areas, style);
        }
        public void DrawSeaRegionOverlay(SeaRegion sr, ViewManager vm)
        {
            OverlayDrawStyle style = vm.ViewMode.GetSeaRegionOverlayDrawStyle(sr);

            if (!style.ShouldDraw)
                return;

            DrawOverlayAreas(sr.Areas, style);
        }

        public void EndDrawing(GraphicsComponent g, CameraManager c, LightingManager lm, OverlayMetaStyle ms)
        {
            if (vertices.Count == 0 || indices.Count == 0)
                return;

            SetBuffers(g);
            RenderBuffers(g, c, lm, ms);
        }

        /* -------- Private Methods --------- */
        private void DrawOverlayAreas(List<Area> areas, OverlayDrawStyle style)
        {
            foreach (Area a in areas)
            {
                List<VertexOverlay> areaVerts = new List<VertexOverlay>();

                foreach (Vector2 msPos in a.MSPositions)
                {
                    Vector3 gsPos = SpaceUtilities.ConvertMSToGS(msPos);
                    VertexOverlay vert = new VertexOverlay(gsPos, style.Color1, style.Color2);
                    areaVerts.Add(vert);
                }

                AddIndicesAndVertices(a.Indices, areaVerts);
            }
        }
        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexOverlay> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexOverlay.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexOverlay>(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData<int>(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager c, LightingManager lm, OverlayMetaStyle ms)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[ms.TechniqueID];
            effect.Parameters["xView"].SetValue(c.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(c.Camera.ProjectionMatrix);
            effect.Parameters["xBandThickness"].SetValue(CalibrationManager.GraphicsCalibration.Data.overlayDoubleColorBandThickness);
            effect.Parameters["xOverlayAlpha"].SetValue(ms.OverlayAlpha);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
    }
}
