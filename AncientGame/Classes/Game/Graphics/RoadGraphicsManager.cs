﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RoadGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexRoad> vertices;
        private List<int> indices;
        private string techniqueID;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;
        private Texture2D texture1;

        /* -------- Constructors -------- */
        public RoadGraphicsManager(ContentManager content)
        {
            vertices = new List<VertexRoad>();
            indices = new List<int>();

            techniqueID = "Standard";

            effect = AssetLoader.LoadEffect(CalibrationManager.GraphicsCalibration.Data.roadEffectID, content);
            texture1 = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.roadTexture1ID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing()
        {
            vertices.Clear();
            indices.Clear();
        }

        public void DrawRoad(Road r, LandTerritory lt, ViewManager vm)
        {
            GraphicsStyle style = vm.ViewMode.GetRoadDrawStyle(lt);

            if (!style.ShouldDraw)
                return;

            List<VertexRoad> roadVerts = GeneralUtilities.ShallowCloneList(r.Vertices);
            for (int i = 0; i < roadVerts.Count; i++)
            {
                VertexRoad newVert = roadVerts[i];
                newVert.textureIndex = GetRoadTextureIndex(lt);
                roadVerts[i] = newVert;
            }

            AddIndicesAndVertices(r.Indices, roadVerts);
        }

        public void EndDrawing(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            if (vertices.Count == 0 || indices.Count == 0)
                return;

            SetBuffers(g);
            RenderBuffers(g, cm, lm);
        }

        /* -------- Private Methods --------- */
        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexRoad> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }

        private int GetRoadTextureIndex(LandTerritory lt)
        {
            // TODO
            return 0;
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexRoad.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexRoad>(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData<int>(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager cm, LightingManager lm)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[techniqueID];
            effect.Parameters["xView"].SetValue(cm.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(cm.Camera.ProjectionMatrix);
            effect.Parameters["xLightColor"].SetValue(lm.LightColor.ToVector3());
            effect.Parameters["xAmbientLightIntensity"].SetValue(lm.AmbientLightIntensity);
            effect.Parameters["xDirectLightIntensity"].SetValue(lm.DirectLightIntensity);
            effect.Parameters["xDirectLightDirection"].SetValue(lm.DirectLightDirection);
            effect.Parameters["xTexture1"].SetValue(texture1);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
    }
}
