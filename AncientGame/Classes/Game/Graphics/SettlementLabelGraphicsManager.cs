﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class SettlementLabelGraphicsManager
    {
        /* -------- Private Fields -------- */
        // Assets
        private Texture2D pixelTex;
        private Texture2D topBottomEdgeTex;
        private Texture2D leftRightEdgeTex;
        private Texture2D cornerTex;
        private SpriteFont font;
        // Constants
        private const int LABEL_VERT_OFFSET = 20;
        private const int LABEL_TOP_HEIGHT = 21;
        private const int LABEL_BOTTOM_HEIGHT = 5;
        private const int LABEL_INNER_HORIZ_BORDER = 7;
        private const int EDGE_SIZE = 4;
        private const int CORNER_SIZE = 6;

        /* -------- Constructors -------- */
        public SettlementLabelGraphicsManager(ContentManager content)
        {
            pixelTex = AssetLoader.LoadTexture("white-pixel", content);
            topBottomEdgeTex = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.settlementLabelTopBottomEdgeTextureID, content);
            leftRightEdgeTex = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.settlementLabelLeftRightEdgeTextureID, content);
            cornerTex = AssetLoader.LoadTexture(CalibrationManager.GraphicsCalibration.Data.settlementLabelCornerTextureID, content);
            font = AssetLoader.LoadSpriteFont(CalibrationManager.GraphicsCalibration.Data.settlementLabelFontID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing(GraphicsComponent g)
        {
            g.SpriteBatch.Begin();
        }

        public void DrawSettlementLabel(LandTerritory lt, GraphicsComponent g, CameraManager cm, ViewManager vm)
        {
            GraphicsStyle drawStyle = vm.ViewMode.GetSettlementLabelDrawStyle(lt);

            if (!drawStyle.ShouldDraw)
                return;

            Faction faction = lt.Variables.OwningFaction;
            Point settlementPSPos = SpaceUtilities.ConvertMSToPS(lt.Settlement.MSPosition, cm.Camera, g);

            string name = lt.GetNameString();
            Vector2 stringSize = font.MeasureString(name);
            eUITextShade textShade = UIUtilities.GetTextShade(faction.Variables.PrimaryColor);
            Color textColor;
            if (textShade == eUITextShade.LIGHT)
                textColor = CalibrationManager.GraphicsCalibration.Data.settlementLabelLightFontColor;
            else
                textColor = CalibrationManager.GraphicsCalibration.Data.settlementLabelDarkFontColor;
            
            Rectangle labelRect = new Rectangle(settlementPSPos.X - Mathf.Round(stringSize.X / 2f) - LABEL_INNER_HORIZ_BORDER, settlementPSPos.Y + LABEL_VERT_OFFSET, Mathf.Round(stringSize.X) + 2 * LABEL_INNER_HORIZ_BORDER, LABEL_TOP_HEIGHT + LABEL_BOTTOM_HEIGHT);
            Rectangle topRect = new Rectangle(labelRect.Left, labelRect.Top, labelRect.Width, LABEL_TOP_HEIGHT);
            Rectangle bottomRect = new Rectangle(labelRect.Left, labelRect.Top + LABEL_TOP_HEIGHT, labelRect.Width, LABEL_BOTTOM_HEIGHT);

            g.SpriteBatch.Draw(pixelTex, topRect, faction.Variables.PrimaryColor);
            g.SpriteBatch.Draw(pixelTex, bottomRect, faction.Variables.SecondaryColor);

            Vector2 textPSVec = SpaceUtilities.ConvertPSToPSVec(new Point(labelRect.X + LABEL_INNER_HORIZ_BORDER, labelRect.Y + 2));
            g.SpriteBatch.DrawString(font, name, textPSVec, textColor);

            DrawLabelBorder(labelRect, g);
        }

        public void EndDrawing(GraphicsComponent g)
        {
            g.SpriteBatch.End();
        }

        /* -------- Private Methods --------- */
        private void DrawLabelBorder(Rectangle labelRect, GraphicsComponent g)
        {
            Rectangle topEdgeRect = new Rectangle(labelRect.Left, labelRect.Top - EDGE_SIZE, labelRect.Width, EDGE_SIZE);
            Rectangle bottomEdgeRect = new Rectangle(labelRect.Left, labelRect.Bottom, labelRect.Width, EDGE_SIZE);
            Rectangle leftEdgeRect = new Rectangle(labelRect.Left - EDGE_SIZE, labelRect.Top, EDGE_SIZE, labelRect.Height);
            Rectangle rightEdgeRect = new Rectangle(labelRect.Right, labelRect.Top, EDGE_SIZE, labelRect.Height);

            Rectangle topLeftCornerRect = new Rectangle(labelRect.Left - CORNER_SIZE, labelRect.Top - CORNER_SIZE, CORNER_SIZE, CORNER_SIZE);
            Rectangle topRightCornerRect = new Rectangle(labelRect.Right, labelRect.Top - CORNER_SIZE, CORNER_SIZE, CORNER_SIZE);
            Rectangle bottomLeftCornerRect = new Rectangle(labelRect.Left - CORNER_SIZE, labelRect.Bottom, CORNER_SIZE, CORNER_SIZE);
            Rectangle bottomRightCornerRect = new Rectangle(labelRect.Right, labelRect.Bottom, CORNER_SIZE, CORNER_SIZE);

            g.SpriteBatch.Draw(topBottomEdgeTex, topEdgeRect, Color.White);
            g.SpriteBatch.Draw(topBottomEdgeTex, bottomEdgeRect, Color.White);
            g.SpriteBatch.Draw(leftRightEdgeTex, leftEdgeRect, Color.White);
            g.SpriteBatch.Draw(leftRightEdgeTex, rightEdgeRect, Color.White);

            g.SpriteBatch.Draw(cornerTex, topLeftCornerRect, Color.White);
            g.SpriteBatch.Draw(cornerTex, topRightCornerRect, Color.White);
            g.SpriteBatch.Draw(cornerTex, bottomLeftCornerRect, Color.White);
            g.SpriteBatch.Draw(cornerTex, bottomRightCornerRect, Color.White);
        }
    }
}
