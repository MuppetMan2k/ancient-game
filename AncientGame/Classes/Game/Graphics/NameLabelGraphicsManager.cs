﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class NameLabelGraphicsManager
    {
        /* -------- Private Fields -------- */
        private List<VertexPositionTexture> vertices;
        private List<int> indices;
        // Buffers
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        // Assets
        private Effect effect;
        private NameLabelAtlas nameLabelAtlas;

        /* -------- Constructors -------- */
        public NameLabelGraphicsManager(ContentManager content)
        {
            vertices = new List<VertexPositionTexture>();
            indices = new List<int>();

            string effectID = CalibrationManager.GraphicsCalibration.Data.nameLabelEffectID;
            effect = AssetLoader.LoadEffect(effectID, content);

            string nameLabelAtlasID = CalibrationManager.GraphicsCalibration.Data.nameLabelAtlasID;
            nameLabelAtlas = AssetLoader.LoadNameLabelAtlas(nameLabelAtlasID, content);
        }

        /* -------- Public Methods -------- */
        public void BeginDrawing(ContentManager content)
        {
            vertices.Clear();
            indices.Clear();
        }

        public void DrawLandRegionNameLabel(LandRegion lr, ViewManager vm)
        {
            GraphicsStyle style = vm.ViewMode.GetLandRegionNameLabelDrawStyle(lr);

            if (!style.ShouldDraw)
                return;

            DrawNameLabel(lr.NameLabel);
        }
        public void DrawLandProvinceNameLabel(LandProvince lp, ViewManager vm)
        {
            GraphicsStyle style = vm.ViewMode.GetLandProvinceNameLabelDrawStyle(lp);

            if (!style.ShouldDraw)
                return;

            DrawNameLabel(lp.NameLabel);
        }
        public void DrawSeaRegionNameLabel(SeaRegion sr, ViewManager vm)
        {
            GraphicsStyle style = vm.ViewMode.GetSeaRegionNameLabelDrawStyle(sr);

            if (!style.ShouldDraw)
                return;

            DrawNameLabel(sr.NameLabel);
        }

        public void EndDrawing(GraphicsComponent g, CameraManager c)
        {
            if (vertices.Count == 0 || indices.Count == 0)
                return;

            SetBuffers(g);
            RenderBuffers(g, c);
        }

        /* -------- Private Methods --------- */
        private void DrawNameLabel(NameLabel nameLabel)
        {
            Vector2 labelDiff = nameLabel.EndMSPosition - nameLabel.StartMSPosition;
            Vector2 labelDirAlong = labelDiff;
            labelDirAlong.Normalize();
            Vector2 labelDirUp = VectorUtilities.RotateVector(labelDirAlong, 90f);

            float labelLength = labelDiff.Length();
            float textLength = 0f;
            foreach (char c in nameLabel.Text)
                textLength += (float)nameLabelAtlas.GetCharWidth(c);
            float scale = labelLength / textLength;

            List<VertexPositionTexture> labelVerts = new List<VertexPositionTexture>();
            List<int> labelInds = new List<int>();

            Vector2 positionAlongLabel = nameLabel.StartMSPosition;
            int realCharNum = 0;
            for (int i = 0; i < nameLabel.Text.Length; i++)
            {
                char c = nameLabel.Text[i];

                if (char.IsWhiteSpace(c))
                {
                    positionAlongLabel += labelDirAlong * ((float)nameLabelAtlas.GetCharWidth(c) * scale);
                    continue;
                }

                Vector2 topLeft = positionAlongLabel + labelDirUp * ((float)nameLabelAtlas.Data.height * 0.5f * scale);
                Vector2 bottomLeft = positionAlongLabel - labelDirUp * ((float)nameLabelAtlas.Data.height * 0.5f * scale);

                VertexPositionTexture vertTopLeft = new VertexPositionTexture(SpaceUtilities.ConvertMSToGS(topLeft), nameLabelAtlas.GetCharTopLeftTextureCoordinate(c));
                VertexPositionTexture vertBottomLeft = new VertexPositionTexture(SpaceUtilities.ConvertMSToGS(bottomLeft), nameLabelAtlas.GetCharBottomLeftTextureCoordinate(c));

                labelVerts.Add(vertTopLeft);
                labelVerts.Add(vertBottomLeft);

                positionAlongLabel += labelDirAlong * ((float)nameLabelAtlas.GetCharWidth(c) * scale);

                Vector2 topRight = positionAlongLabel + labelDirUp * ((float)nameLabelAtlas.Data.height * 0.5f * scale);
                Vector2 bottomRight = positionAlongLabel - labelDirUp * ((float)nameLabelAtlas.Data.height * 0.5f * scale);

                VertexPositionTexture vertTopRight = new VertexPositionTexture(SpaceUtilities.ConvertMSToGS(topRight), nameLabelAtlas.GetCharTopRightTextureCoordinate(c));
                VertexPositionTexture vertBottomRight = new VertexPositionTexture(SpaceUtilities.ConvertMSToGS(bottomRight), nameLabelAtlas.GetCharBottomRightTextureCoordinate(c));

                labelVerts.Add(vertTopRight);
                labelVerts.Add(vertBottomRight);

                int indTopLeft = realCharNum * 4;
                int indBottomLeft = indTopLeft + 1;
                int indTopRight = indBottomLeft + 1;
                int indBottomRight = indTopRight + 1;

                labelInds.Add(indTopLeft);
                labelInds.Add(indBottomLeft);
                labelInds.Add(indBottomRight);

                labelInds.Add(indBottomRight);
                labelInds.Add(indTopRight);
                labelInds.Add(indTopLeft);

                realCharNum++;
            }

            AddIndicesAndVertices(labelInds, labelVerts);
        }
        private void AddIndicesAndVertices(List<int> indicesToAdd, List<VertexPositionTexture> verticesToAdd)
        {
            int vertexNum = vertices.Count;

            foreach (int i in indicesToAdd)
                indices.Add(i + vertexNum);

            vertices.AddRange(verticesToAdd);
        }

        private void SetBuffers(GraphicsComponent g)
        {
            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            vertexBuffer = new VertexBuffer(g.GraphicsDevice, VertexPositionTexture.VertexDeclaration, vertices.Count, BufferUsage.WriteOnly);

            vertexBuffer.SetData<VertexPositionTexture>(vertices.ToArray());

            if (indexBuffer != null)
                indexBuffer.Dispose();
            indexBuffer = new IndexBuffer(g.GraphicsDevice, IndexElementSize.ThirtyTwoBits, indices.Count, BufferUsage.WriteOnly);

            indexBuffer.SetData<int>(indices.ToArray());
        }
        private void RenderBuffers(GraphicsComponent g, CameraManager c)
        {
            g.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            g.GraphicsDevice.Indices = indexBuffer;

            effect.CurrentTechnique = effect.Techniques[nameLabelAtlas.Data.techniqueID];
            effect.Parameters["xView"].SetValue(c.Camera.ViewMatrix);
            effect.Parameters["xProjection"].SetValue(c.Camera.ProjectionMatrix);
            effect.Parameters["xTexture"].SetValue(nameLabelAtlas.AtlasTexture);

            int triangleNum = indices.Count / 3;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                g.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, vertices.Count, 0, triangleNum);
            }
        }
    }
}
