﻿using System;
using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eLandTerritoryControlLevelRequirement
    {
        NO_REQUIREMENT,
        MILITARY_OWNERSHIP_ONLY,
        MILITARY_OWNERSHIP_OR_HIGHER,
        ANNEXED_OWNERSHIP_ONLY,
        ANNEXED_OWNERSHIP_OR_HIGHER,
        HOMELAND_OWNERSHIP_ONLY
    }

    class BuildingRequirements
    {
        /* -------- Properties -------- */
        public List<Resource> RequiredResources { get; private set; }
        public int RequiredPopulation { get; private set; }
        public string PreceedingBuildingID { get; private set; }
        public eBuildingSlotType BuildingSlotType { get; private set; }
        public Culture RequiredCulture { get; private set; }
        public CultureGroup RequiredCultureGroup { get; private set; }
        public CultureFamily RequiredCultureFamily { get; private set; }
        public MilitaryEra RequiredMilitaryEra { get; private set; }
        public eLandTerritoryControlLevelRequirement RequiredLandTerritoryControlLevel { get; private set; }

        /* -------- Constructors -------- */
        public BuildingRequirements()
        {
            RequiredResources = new List<Resource>();
        }

        /* -------- Public Methods -------- */
        public bool BuildingIsAvailable(BuildingSlot slot, LandTerritory lt)
        {
            Faction owningFaction = lt.Variables.OwningFaction;
            Faction occupyingFaction = lt.Variables.OccupyingFaction;

            // No building in occupied territory
            if (occupyingFaction != null)
                return false;

            if (PreceedingBuildingID != null && (slot.CurrentBuilding == null || slot.CurrentBuilding.ID != PreceedingBuildingID))
                return false;

            if (BuildingSlotType != slot.Type)
                return false;

            if (RequiredCulture != null && owningFaction.Variables.Culture != RequiredCulture)
                return false;

            if (RequiredCultureGroup != null && owningFaction.Variables.Culture.CultureGroup != RequiredCultureGroup)
                return false;

            if (RequiredCultureFamily != null && owningFaction.Variables.Culture.CultureGroup.CultureFamily != RequiredCultureFamily)
                return false;

            if (RequiredMilitaryEra != null && owningFaction.Variables.MilitaryEra != RequiredMilitaryEra)
                return false;

            eLandTerritoryControlLevel controlLevel = owningFaction.Variables.GetLandTerritoryControl(lt).CurrControlLevel;
            switch (RequiredLandTerritoryControlLevel)
            {
                case eLandTerritoryControlLevelRequirement.MILITARY_OWNERSHIP_ONLY:
                    if (controlLevel != eLandTerritoryControlLevel.MILITARY_OWNERSHIP)
                        return false;
                    break;
                case eLandTerritoryControlLevelRequirement.MILITARY_OWNERSHIP_OR_HIGHER:
                    if (controlLevel != eLandTerritoryControlLevel.MILITARY_OWNERSHIP && controlLevel != eLandTerritoryControlLevel.ANNEXED_OWNERSHIP && controlLevel != eLandTerritoryControlLevel.HOMELAND_OWNERSHIP)
                        return false;
                    break;
                case eLandTerritoryControlLevelRequirement.ANNEXED_OWNERSHIP_ONLY:
                    if (controlLevel != eLandTerritoryControlLevel.ANNEXED_OWNERSHIP)
                        return false;
                    break;
                case eLandTerritoryControlLevelRequirement.ANNEXED_OWNERSHIP_OR_HIGHER:
                    if (controlLevel != eLandTerritoryControlLevel.ANNEXED_OWNERSHIP && controlLevel != eLandTerritoryControlLevel.HOMELAND_OWNERSHIP)
                        return false;
                    break;
                case eLandTerritoryControlLevelRequirement.HOMELAND_OWNERSHIP_ONLY:
                    if (controlLevel != eLandTerritoryControlLevel.HOMELAND_OWNERSHIP)
                        return false;
                    break;
                default:
                    break;
            }

            return true;
        }

        public bool ShouldShowRequirementsSectionInTooltip()
        {
            if (RequiredResources.Count > 0)
                return true;

            if (RequiredPopulation > 0)
                return true;

            return false;
        }

        /* -------- Static Methods -------- */
        public static BuildingRequirements CreateFromData(BuildingRequirementsData data, GameManagerContainer gameManagers)
        {
            BuildingRequirements requirements = new BuildingRequirements();

            foreach (string resourceID in data.requiredResourceIDs)
                requirements.RequiredResources.Add(gameManagers.ResourceManager.GetResource(resourceID));
            requirements.PreceedingBuildingID = String.IsNullOrEmpty(data.preceedingBuildingID) ? null : data.preceedingBuildingID;
            requirements.BuildingSlotType = DataUtilities.ParseBuildingSlotType(data.buildingSlotType);
            requirements.RequiredCulture = String.IsNullOrEmpty(data.requiredCultureID) ? null : gameManagers.CultureManagers.CultureManager.GetCulture(data.requiredCultureID);
            requirements.RequiredCultureGroup = String.IsNullOrEmpty(data.requiredCultureGroupID) ? null : gameManagers.CultureManagers.CultureManager.GetCultureGroup(data.requiredCultureGroupID);
            requirements.RequiredCultureFamily = String.IsNullOrEmpty(data.requiredCultureFamilyID) ? null : gameManagers.CultureManagers.CultureManager.GetCultureFamily(data.requiredCultureFamilyID);
            requirements.RequiredMilitaryEra = String.IsNullOrEmpty(data.requiredMilitaryEraID) ? null : gameManagers.MilitaryEraManager.GetMilitaryEra(data.requiredMilitaryEraID);
            requirements.RequiredLandTerritoryControlLevel = DataUtilities.ParseLandTerritoryControlLevelRequirement(data.requiredLandTerritoryControlLevel);

            return requirements;
        }
    }
}
