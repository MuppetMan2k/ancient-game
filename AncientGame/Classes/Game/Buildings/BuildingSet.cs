﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eBuildingSetType
    {
        SETTLEMENT,
        TERRITORY
    }

    class BuildingSet
    {
        /* -------- Properties -------- */
        public eBuildingSetType Type { get; private set; }
        public List<BuildingSlot> BuildingSlots { get; private set; }

        /* -------- Constructors -------- */
        public BuildingSet(eBuildingSetType inType)
        {
            Type = inType;
            BuildingSlots = new List<BuildingSlot>();
        }

        /* -------- Public Methods -------- */
        public BuildingSlot GetBuildingSlot(eBuildingSlotType type)
        {
            foreach (BuildingSlot bs in BuildingSlots)
                if (bs.Type == type)
                    return bs;

            return null;
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            foreach (BuildingSlot slot in BuildingSlots)
            {
                if (slot.CurrentBuilding != null && slot.CurrentBuilding.CanApplyModifiers())
                {
                    slot.CurrentBuilding.ModifierSet.TestIsSatisfied(objectSet);
                    set.Merge(slot.CurrentBuilding.ModifierSet);
                }
            }

            return set;
        }

        public void CheckForBuildingSlotRemovals(int numBuildingSlots)
        {
            if (BuildingSlots.Count <= numBuildingSlots)
                return;

            BuildingSlots.RemoveRange(numBuildingSlots, BuildingSlots.Count - numBuildingSlots);
        }
        public void CheckForBuildingSlotAdditions(int numBuildingSlots)
        {
            if (BuildingSlots.Count >= numBuildingSlots)
                return;

            if (Type == eBuildingSetType.SETTLEMENT)
            {
                if (BuildingSlots.Count == 0)
                    BuildingSlots.Add(new BuildingSlot(eBuildingSlotType.CENTRAL));
                if (BuildingSlots.Count == 1)
                    BuildingSlots.Add(new BuildingSlot(eBuildingSlotType.WALLS));
            }
            
            if (Type == eBuildingSetType.TERRITORY)
            {
                if (BuildingSlots.Count == 0)
                    BuildingSlots.Add(new BuildingSlot(eBuildingSlotType.ROADS));
            }

            eBuildingSlotType newSlotType;
            if (Type == eBuildingSetType.SETTLEMENT)
                newSlotType = eBuildingSlotType.STANDARD_SETTLEMENT;
            else
                newSlotType = eBuildingSlotType.STANDARD_TERRITORY;

            int buildingSlotsToAdd = numBuildingSlots - BuildingSlots.Count;
            for (int i = 0; i < buildingSlotsToAdd; i++)
                BuildingSlots.Add(new BuildingSlot(newSlotType));
        }

        /* -------- Static Methods -------- */
        public static BuildingSet CreateFromData(BuildingSetData data, eBuildingSetType setType, GameManagerContainer gameManagers)
        {
            BuildingSet buildingSet = new BuildingSet(setType);

            foreach (BuildingSlotData slotData in data.buildingSlotDatas)
            {
                BuildingSlot slot = BuildingSlot.CreateFromData(slotData);
                buildingSet.BuildingSlots.Add(slot);
            }
            foreach (BuildingStateData stateData in data.buildingStateDatas)
            {
                if (stateData.buildingSlotIndex >= buildingSet.BuildingSlots.Count)
                    continue;

                Building building = Building.CreateFromStateData(stateData, gameManagers);
                if (building.IsBeingBuilt)
                    buildingSet.BuildingSlots[stateData.buildingSlotIndex].SetDevelopingBuilding(building);
                else
                    buildingSet.BuildingSlots[stateData.buildingSlotIndex].SetCurrentBuilding(building);
            }

            return buildingSet;
        }
    }
}
