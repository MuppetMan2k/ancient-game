﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class BuildingManager
    {
        /* -------- Private Fields -------- */
        private List<BuildingData> buildingDatas;

        /* -------- Constructors -------- */
        public BuildingManager()
        {
            buildingDatas = new List<BuildingData>();
        }

        /* -------- Public Methods -------- */
        public void AddBuildingData(BuildingData buildingData)
        {
            buildingDatas.Add(buildingData);
        }

        public BuildingData GetBuildingData(string id)
        {
            return buildingDatas.Find(x => x.id == id);
        }

        public List<BuildingData> GetAvailableBuildingDatas(BuildingSlot slot, LandTerritory lt, GameManagerContainer gameManagers)
        {
            return buildingDatas.FindAll(x => Building.BuildingIsAvailable(x, gameManagers, slot, lt));
        }
    }
}
