﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eBuildingSlotType
    {
        CENTRAL,
        WALLS,
        SMALL_PORT,
        LARGE_PORT,
        STANDARD_SETTLEMENT,
        ROADS,
        STANDARD_TERRITORY
    }

    class BuildingSlot
    {
        /* -------- Properties -------- */
        public eBuildingSlotType Type { get; private set; }
        public Building CurrentBuilding { get; private set; }
        public Building DevelopingBuilding { get; private set; }

        /* -------- Constructors -------- */
        public BuildingSlot(eBuildingSlotType inType)
        {
            Type = inType;
        }

        /* -------- Public Methods -------- */
        public void SetCurrentBuilding(Building inBuilding)
        {
            CurrentBuilding = inBuilding;
        }
        public void SetDevelopingBuilding(Building inBuilding)
        {
            DevelopingBuilding = inBuilding;
        }

        public bool IsUnderDevelopment()
        {
            if (CurrentBuilding != null && CurrentBuilding.IsBeingRepaired)
                return true;

            if (CurrentBuilding != null && CurrentBuilding.IsBeingDemolished)
                return true;

            if (DevelopingBuilding != null)
                return true;

            return false;
        }

        public void CancelDevelopment()
        {
            if (CurrentBuilding != null && CurrentBuilding.IsBeingDemolished)
            {
                CurrentBuilding.TurnsUntilDemolition = 0;
                return;
            }

            if (CurrentBuilding != null && CurrentBuilding.IsBeingRepaired)
            {
                CurrentBuilding.TurnsUntilRepair = 0;
                return;
            }

            if (DevelopingBuilding != null && DevelopingBuilding.IsBeingBuilt)
            {
                DevelopingBuilding = null;
                return;
            }
        }

        public void StartConstruction(BuildingData data, LandTerritory lt, GameManagerContainer gameManagers)
        {
            if (DevelopingBuilding != null)
                return;

            if (!lt.TryChargeCost(Building.GetCost(data, lt, gameManagers)))
                return;

            DevelopingBuilding = Building.CreateFromData(data, gameManagers);
            DevelopingBuilding.StartConstruction(lt, gameManagers);
        }

        /* -------- Static Methods -------- */
        public static BuildingSlot CreateFromData(BuildingSlotData data)
        {
            eBuildingSlotType slotType = DataUtilities.ParseBuildingSlotType(data.type);
            return new BuildingSlot(slotType);
        }
    }
}
