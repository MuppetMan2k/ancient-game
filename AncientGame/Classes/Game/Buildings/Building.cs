﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eBuildingDamageLevel
    {
        NO_DAMAGE,
        LIGHT_DAMAGE,
        MEDIUM_DAMAGE,
        HEAVY_DAMAGE
    }

    enum eBuildabilityResult
    {
        CAN_BUILD,
        CANNOT_AFFORD_COST,
        REQUIRED_RESOURCES_NOT_PRESENT,
        NOT_ENOUGH_POPULATION
    }

    class Building
    {
        /* -------- Properties -------- */
        // Data
        public string ID { get; private set; }
        public BuildingData Data { get; private set; }
        public ModifierSet ModifierSet { get; private set; }
        // Variables
        public eBuildingDamageLevel DamageLevel { get; private set; }
        public int TurnsUntilCompletion { get; set; }
        public int TurnsUntilDemolition { get; set; }
        public int TurnsUntilRepair { get; set; }
        public bool IsBeingBuilt
        {
            get
            {
                return TurnsUntilCompletion > 0;
            }
        }
        public bool IsBeingRepaired
        {
            get
            {
                return TurnsUntilRepair > 0;
            }
        }
        public bool IsBeingDemolished
        {
            get
            {
                return TurnsUntilDemolition > 0;
            }
        }

        /* -------- Public Methods -------- */
        public bool CanApplyModifiers()
        {
            if (DamageLevel != eBuildingDamageLevel.NO_DAMAGE)
                return false;

            if (IsBeingBuilt)
                return false;

            if (IsBeingRepaired)
                return false;

            if (IsBeingDemolished)
                return false;

            return true;
        }
        
        public void StartConstruction(LandTerritory lt, GameManagerContainer gameManagers)
        {
            TurnsUntilCompletion = GetConstructionTurns(Data, lt, gameManagers);
        }
        public void StartDemolition(LandTerritory lt, GameManagerContainer gameManagers)
        {
            TurnsUntilDemolition = GetDemolishTurns(lt, gameManagers);
        }
        public void StartRepair(LandTerritory lt, GameManagerContainer gameManagers)
        {
            if (!lt.TryChargeCost(GetRepairCost(lt, gameManagers)))
                return;

            TurnsUntilRepair = GetRepairTurns(lt, gameManagers);
        }
        public Cost GetRepairCost(LandTerritory lt, GameManagerContainer gameManagers)
        {
            float multiplier;
            switch (DamageLevel)
            {
                case eBuildingDamageLevel.LIGHT_DAMAGE:
                    multiplier = CalibrationManager.CampaignCalibration.Data.lightDamageRepairCostMultiplier;
                    break;
                case eBuildingDamageLevel.MEDIUM_DAMAGE:
                    multiplier = CalibrationManager.CampaignCalibration.Data.mediumDamageRepairCostMultiplier;
                    break;
                case eBuildingDamageLevel.HEAVY_DAMAGE:
                    multiplier = CalibrationManager.CampaignCalibration.Data.heavyDamageRepairCostMultiplier;
                    break;
                default:
                    multiplier = 0f;
                    break;
            }

            return GetCost(Data, lt, gameManagers).Multiply(multiplier);
        }

        public int GetRepairTurns(LandTerritory lt, GameManagerContainer gameManagers)
        {
            float constructionTime = Data.constructionTime;
            constructionTime = lt.GetModifierSet(gameManagers).Modify(constructionTime, eModifierType.BUILDING_CONSTRUCTION_TIME);
            constructionTime *= (float)CalibrationManager.CampaignCalibration.Data.turnsPerYear;

            float repairMultiplier;
            switch (DamageLevel)
            {
                case eBuildingDamageLevel.LIGHT_DAMAGE:
                    repairMultiplier = CalibrationManager.CampaignCalibration.Data.lightDamageRepairConstructionTimeMultiplier;
                    break;
                case eBuildingDamageLevel.MEDIUM_DAMAGE:
                    repairMultiplier = CalibrationManager.CampaignCalibration.Data.mediumDamageRepairConstructionTimeMultiplier;
                    break;
                case eBuildingDamageLevel.HEAVY_DAMAGE:
                    repairMultiplier = CalibrationManager.CampaignCalibration.Data.heavyDamageRepairConstructionTimeMultiplier;
                    break;
                default:
                    repairMultiplier = 1f;
                    break;
            }
            float repairTime = constructionTime * repairMultiplier;

            return Mathf.ClampLower(Mathf.Round(repairTime), 1);
        }
        public int GetDemolishTurns(LandTerritory lt, GameManagerContainer gameManagers)
        {
            float constructionTime = Data.constructionTime;
            constructionTime = lt.GetModifierSet(gameManagers).Modify(constructionTime, eModifierType.BUILDING_CONSTRUCTION_TIME);
            constructionTime *= (float)CalibrationManager.CampaignCalibration.Data.turnsPerYear;

            float demolishTime = constructionTime * CalibrationManager.CampaignCalibration.Data.demolishConstructionTimeMultiplier;

            return Mathf.ClampLower(Mathf.Round(demolishTime), 1);
        }

        public string GetDamageString()
        {
            switch (DamageLevel)
            {
                case eBuildingDamageLevel.LIGHT_DAMAGE:
                    return Localization.Localize("text_light_damage");
                case eBuildingDamageLevel.MEDIUM_DAMAGE:
                    return Localization.Localize("text_medium_damage");
                case eBuildingDamageLevel.HEAVY_DAMAGE:
                    return Localization.Localize("text_heavy_damage");
                default:
                    return "";
            }
        }

        /* -------- Static Methods -------- */
        public static Building CreateFromData(BuildingData data, GameManagerContainer gameManagers)
        {
            Building building = new Building();

            building.ID = data.id;
            building.ModifierSet = ModifierSet.CreateFromData(data.modifierDatas, gameManagers);
            building.Data = data;

            return building;
        }
        public static Building CreateFromStateData(BuildingStateData data, GameManagerContainer gameManagers)
        {
            BuildingData buildingData = gameManagers.BuildingManager.GetBuildingData(data.buildingID);

            Building building = Building.CreateFromData(buildingData, gameManagers);

            building.DamageLevel = DataUtilities.ParseBuildingDamageLevel(data.damageLevel);
            building.TurnsUntilCompletion = data.turnsUntilCompletion;
            building.TurnsUntilRepair = data.turnsUntilRepair;
            building.TurnsUntilDemolition = data.turnsUntilDemolition;

            return building;
        }

        public static bool BuildingIsAvailable(BuildingData data, GameManagerContainer gameManagers, BuildingSlot slot, LandTerritory lt)
        {
            BuildingRequirements requirements = BuildingRequirements.CreateFromData(data.requirementsData, gameManagers);
            return requirements.BuildingIsAvailable(slot, lt);
        }
        public static eBuildabilityResult GetBuildability(BuildingData data, GameManagerContainer gameManagers, LandTerritory lt)
        {
            Faction owningFaction = lt.Variables.OwningFaction;

            Cost cost = Cost.CreateFromData(data.costData);

            if (!cost.CanAfford(lt))
                return eBuildabilityResult.CANNOT_AFFORD_COST;

            BuildingRequirements requirements = BuildingRequirements.CreateFromData(data.requirementsData, gameManagers);
            foreach (Resource r in requirements.RequiredResources)
                if (lt.Variables.ResourcePresence.GetResourceQuantity(r) <= 0f)
                    return eBuildabilityResult.REQUIRED_RESOURCES_NOT_PRESENT;

            if (lt.Variables.Population.Population < requirements.RequiredPopulation)
                return eBuildabilityResult.NOT_ENOUGH_POPULATION;

            return eBuildabilityResult.CAN_BUILD;
        }

        public static Cost GetCost(BuildingData data, LandTerritory lt, GameManagerContainer gameManagers)
        {
            Cost cost = Cost.CreateFromData(data.costData);

            ModifierSet modifierSet = lt.GetModifierSet(gameManagers);
            cost.SetMoneyCost(Mathf.Round(modifierSet.Modify((float)cost.MoneyCost, eModifierType.BUILDING_MONEY_COST)));
            cost.SetAdminPowerCost(Mathf.Round(modifierSet.Modify((float)cost.AdminPowerCost, eModifierType.BUILDING_ADMIN_POWER_COST)));
            cost.SetManpowerCost(Mathf.Round(modifierSet.Modify((float)cost.ManpowerCost, eModifierType.BUILDING_MANPOWER_COST)));

            return cost;
        }
        public static int GetConstructionTurns(BuildingData data, LandTerritory lt, GameManagerContainer gameManagers)
        {
            float constructionTime = data.constructionTime;
            constructionTime = lt.GetModifierSet(gameManagers).Modify(constructionTime, eModifierType.BUILDING_CONSTRUCTION_TIME);
            constructionTime *= (float)CalibrationManager.CampaignCalibration.Data.turnsPerYear;

            return Mathf.ClampLower(Mathf.Round(constructionTime), 1);
        }

        public static string GetName(BuildingData data)
        {
            return Localization.Localize(data.locID);
        }
        public static string GetHistoricalName(BuildingData data)
        {
            return Localization.Localize(data.historicalNameLocID);
        }
    }
}
