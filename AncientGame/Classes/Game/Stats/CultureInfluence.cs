﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eCultureInfluenceType
    {
        LOCAL_TRADITIONS
    }

    class CultureInfluence
    {
        /* -------- Properties -------- */
        public Culture Culture { get; private set; }
        public float Value { get; private set; }
        public eCultureInfluenceType Type { get; private set; }

        /* -------- Static Methods -------- */
        public static CultureInfluence CreateFromData(CultureInfluenceData data, CultureManager cm)
        {
            CultureInfluence cultureInfluence = new CultureInfluence();

            cultureInfluence.Culture = cm.GetCulture(data.cultureID);
            cultureInfluence.Value = data.value;
            cultureInfluence.Type = DataUtilities.ParseCultureInfluenceType(data.type);

            return cultureInfluence;
        }
    }
}
