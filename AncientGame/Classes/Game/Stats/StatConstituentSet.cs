﻿using System.Collections.Generic;

namespace AncientGame
{
    class StatConstituentSet
    {
        /* -------- Properties -------- */
        public List<StatConstituent> StatConstituents { get; private set; }

        /* -------- Constructors -------- */
        public StatConstituentSet()
        {
            StatConstituents = new List<StatConstituent>();
        }

        /* -------- Public Methods -------- */
        public void AddStatConstituent(StatConstituent newStatConstituent)
        {
            if (newStatConstituent.Value == 0f)
                return;
            
            eStatConstituentBias bias = StatConstituent.GetBias(newStatConstituent.Type);
            StatConstituent preexistingStatConstituent = StatConstituents.Find(x => x.Type == newStatConstituent.Type);

            if (preexistingStatConstituent == null)
            {
                if (newStatConstituent.Value < 0f)
                    return;

                StatConstituents.Add(newStatConstituent);
                return;
            }

            float newValue = preexistingStatConstituent.Value + newStatConstituent.Value;
            if (newValue < 0f)
                return;

            preexistingStatConstituent.SetValue(newValue);
        }
        public float GetTotal()
        {
            float total = 0f;
            foreach (StatConstituent sc in StatConstituents)
            {
                if (StatConstituent.GetBias(sc.Type) == eStatConstituentBias.POSITIVE)
                    total += sc.Value;
                else
                    total -= sc.Value;
            }


            return total;
        }
    }
}
