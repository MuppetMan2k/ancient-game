﻿using DataTypes;

namespace AncientGame
{
    class OrderState
    {
        /* -------- Properties -------- */
        public float Order { get; private set; }
        public StatConstituentSet OrderConstituents { get; private set; }

        /* -------- Constructors -------- */
        public OrderState()
        {
            OrderConstituents = new StatConstituentSet();
        }

        /* -------- Public Methods -------- */
        public TooltipInfo GetTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = UIUtilities.GetBiDirectionalFloatDisplayString(Order) + " " + Localization.Localize("text_order_stat");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetModifierSet(gameManagers));
        }

        public void RecalculateOrder()
        {
            Order = OrderConstituents.GetTotal();
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (Order > 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positiveOrderModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(Order)));
            }
            if (Order < 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.negativeOrderModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(Order)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static OrderState CreateFromData(OrderStateData data)
        {
            OrderState state = new OrderState();

            state.Order = data.order;
            foreach (StatConstituentData scd in data.orderConstituentDatas)
            {
                StatConstituent statConstituent = StatConstituent.CreateFromData(scd);
                state.OrderConstituents.AddStatConstituent(statConstituent);
            }

            return state;
        }
    }
}
