﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class LanguageState
    {
        /* -------- Properties -------- */
        public List<LanguageStateItem> LanguageItems { get; private set; }
        public List<LanguageInfluence> LanguageInfluences { get; private set; }

        /* -------- Constructors -------- */
        public LanguageState()
        {
            LanguageItems = new List<LanguageStateItem>();
            LanguageInfluences = new List<LanguageInfluence>();
        }

        /* -------- Static Methods -------- */
        public static LanguageState CreateFromData(LanguageStateData data, LanguageManager lm)
        {
            LanguageState state = new LanguageState();

            foreach (LanguageStateItemData lsid in data.languageStateItemDatas)
            {
                LanguageStateItem lsi = LanguageStateItem.CreateFromData(lsid, lm);
                state.LanguageItems.Add(lsi);
            }
            foreach (LanguageInfluenceData lid in data.languageInfluenceDatas)
            {
                LanguageInfluence li = LanguageInfluence.CreateFromData(lid, lm);
                state.LanguageInfluences.Add(li);
            }

            return state;
        }
    }
}
