﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class ReligionState
    {
        /* -------- Properties -------- */
        public List<ReligionStateItem> ReligionItems { get; private set; }
        public List<ReligionInfluence> ReligionInfluences { get; private set; }

        /* -------- Constructors -------- */
        public ReligionState()
        {
            ReligionItems = new List<ReligionStateItem>();
            ReligionInfluences = new List<ReligionInfluence>();
        }

        /* -------- Static Methods -------- */
        public static ReligionState CreateFromData(ReligionStateData data, ReligionManager rm)
        {
            ReligionState state = new ReligionState();

            foreach (ReligionStateItemData rsid in data.religionStateItemDatas)
            {
                ReligionStateItem rsi = ReligionStateItem.CreateFromData(rsid, rm);
                state.ReligionItems.Add(rsi);
            }
            foreach (ReligionInfluenceData rid in data.religionInfluenceDatas)
            {
                ReligionInfluence ri = ReligionInfluence.CreateFromData(rid, rm);
                state.ReligionInfluences.Add(ri);
            }

            return state;
        }
    }
}
