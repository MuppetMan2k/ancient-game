﻿using DataTypes;

namespace AncientGame
{
    class FoodStoresState
    {
        /* -------- Properties -------- */
        public int FoodStores { get; private set; }
        public int MaxFoodStores { get; private set; }
        public float FoodStoresChange { get; private set; }
        public StatConstituentSet FoodStoresChangeConstituents { get; private set; }

        /* -------- Constructors -------- */
        public FoodStoresState()
        {
            FoodStoresChangeConstituents = new StatConstituentSet();
        }

        /* -------- Public Methods -------- */
        public TooltipInfo GetFoodStoresChangeTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = UIUtilities.GetBiDirectionalFloatDisplayString(FoodStoresChange) + " " + Localization.Localize("text_food_stores_change");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetFoodStoresChangeModifierSet(gameManagers));
        }

        public void RecalculateFoodStoresChange()
        {
            FoodStoresChange = FoodStoresChangeConstituents.GetTotal();
        }
        public void RecalculateMaxFoodStores(LandTerritory lt, GameManagerContainer gameManagers)
        {
            float maxFoodStoresF = CalibrationManager.CampaignCalibration.Data.baseMaxFoodStores;
            maxFoodStoresF = lt.GetModifierSet(gameManagers).Modify(maxFoodStoresF, eModifierType.MAX_FOOD_STORES);

            MaxFoodStores = Mathf.Round(maxFoodStoresF);
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            set.Merge(GetFoodStoresModiferSet(gameManagers));
            set.Merge(GetFoodStoresChangeModifierSet(gameManagers));

            return set;
        }
        public ModifierSet GetFoodStoresModiferSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (FoodStores > 0)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positiveFoodStoresModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, FoodStores));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }
        public ModifierSet GetFoodStoresChangeModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (FoodStoresChange > 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positiveFoodStoresChangeModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(FoodStoresChange)));
            }
            if (FoodStoresChange < 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.negativeFoodStoresChangeModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(FoodStoresChange)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static FoodStoresState CreateFromData(FoodStoresStateData data)
        {
            FoodStoresState state = new FoodStoresState();

            state.FoodStores = data.foodStores;
            state.MaxFoodStores = data.maxFoodStores;
            state.FoodStoresChange = data.foodStoresChange;
            foreach (StatConstituentData scd in data.foodStoresChangeConstituentDatas)
            {
                StatConstituent statConstituent = StatConstituent.CreateFromData(scd);
                state.FoodStoresChangeConstituents.AddStatConstituent(statConstituent);
            }

            return state;
        }
    }
}
