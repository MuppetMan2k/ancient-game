﻿using DataTypes;

namespace AncientGame
{
    class HappinessState
    {
        /* -------- Properties -------- */
        public float NobilityHappiness { get; private set; }
        public StatConstituentSet NobilityHappinessConstituents { get; private set; }
        public float PlebHappiness { get; private set; }
        public StatConstituentSet PlebHappinessConstituents { get; private set; }

        /* -------- Constructors -------- */
        public HappinessState()
        {
            NobilityHappinessConstituents = new StatConstituentSet();
            PlebHappinessConstituents = new StatConstituentSet();
        }

        /* -------- Public Methods -------- */
        public TooltipInfo GetPlebHappinessTotalTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = UIUtilities.GetBiDirectionalFloatDisplayString(PlebHappiness) + " " + Localization.Localize("text_pleb_happiness_stat");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetPlebHappinessModifierSet(gameManagers));
        }
        public TooltipInfo GetNobilityHappinessTotalTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = UIUtilities.GetBiDirectionalFloatDisplayString(NobilityHappiness) + " " + Localization.Localize("text_nobility_happiness_stat");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetNobilityHappinessModifierSet(gameManagers));
        }

        public void RecalculateNobilityHappiness()
        {
            NobilityHappiness = NobilityHappinessConstituents.GetTotal();
        }
        public void RecalculatePlebHappiness()
        {
            PlebHappiness = PlebHappinessConstituents.GetTotal();
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            set.Merge(GetNobilityHappinessModifierSet(gameManagers));
            set.Merge(GetPlebHappinessModifierSet(gameManagers));

            return set;
        }
        public ModifierSet GetNobilityHappinessModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (NobilityHappiness > 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positiveNobilityHappinessModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(NobilityHappiness)));
            }
            if (NobilityHappiness < 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.negativeNobilityHappinessModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(NobilityHappiness)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }
        public ModifierSet GetPlebHappinessModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (PlebHappiness > 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positivePlebHappinessModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(PlebHappiness)));
            }
            if (PlebHappiness < 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.negativePlebHappinessModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(PlebHappiness)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static HappinessState CreateFromData(HappinessStateData data)
        {
            HappinessState state = new HappinessState();

            state.NobilityHappiness = data.nobilityHappiness;
            foreach (StatConstituentData scd in data.nobilityHappinessConstituentDatas)
            {
                StatConstituent statConstituent = StatConstituent.CreateFromData(scd);
                state.NobilityHappinessConstituents.AddStatConstituent(statConstituent);
            }
            state.PlebHappiness = data.plebHappiness;
            foreach (StatConstituentData scd in data.plebHappinessConstituentDatas)
            {
                StatConstituent statConstituent = StatConstituent.CreateFromData(scd);
                state.PlebHappinessConstituents.AddStatConstituent(statConstituent);
            }

            return state;
        }
    }
}
