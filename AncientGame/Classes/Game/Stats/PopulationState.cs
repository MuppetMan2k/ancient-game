﻿using DataTypes;

namespace AncientGame
{
    class PopulationState
    {
        /* -------- Properties -------- */
        public int Population { get; private set; }
        public float PopulationGrowth { get; private set; }
        public StatConstituentSet PopulationGrowthConstituents { get; private set; }
        public int SlavePopulation { get; private set; }
        public float SlavePopulationPercentage { get; private set; }

        /* -------- Constructors -------- */
        public PopulationState()
        {
            PopulationGrowthConstituents = new StatConstituentSet();
        }

        /* -------- Public Methods -------- */
        public TooltipInfo GetPopulationGrowthTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = UIUtilities.GetBiDirectionalPercentageFloatDisplayString(PopulationGrowth) + " " + Localization.Localize("text_population_stat");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetPopulationGrowthModifierSet(gameManagers));
        }
        public TooltipInfo GetPopulationTooltipInfo(GameManagerContainer gameManagers, LandTerritory lt)
        {
            string staticText = Localization.Localize("text_population_tooltip");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetPopulationModifierSet(gameManagers, lt));
        }
        public TooltipInfo GetSlavePopulationPercentageTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = Localization.Localize("text_slave_population_percentage_tooltip");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetSlavePopulationPercentageModifierSet(gameManagers));
        }

        public void RecalculatePopulationGrowth()
        {
            PopulationGrowth = PopulationGrowthConstituents.GetTotal();
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers, LandTerritory lt)
        {
            ModifierSet set = new ModifierSet();

            set.Merge(GetPopulationGrowthModifierSet(gameManagers));
            set.Merge(GetPopulationModifierSet(gameManagers, lt));
            set.Merge(GetSlavePopulationPercentageModifierSet(gameManagers));

            return set;
        }

        /* -------- Private Methods --------- */
        private ModifierSet GetPopulationGrowthModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (PopulationGrowth > 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positivePopulationGrowthModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(PopulationGrowth)));
            }
            if (PopulationGrowth < 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.negativePopulationGrowthModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(PopulationGrowth)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }
        private ModifierSet GetPopulationModifierSet(GameManagerContainer gameManagers, LandTerritory lt)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.populationModifierDatas)
                set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs((float)Population / 1000f)));

            int overpopulation = Population - lt.Variables.SupportablePopulation;
            if (overpopulation > 0)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.overpopulationModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs((float)overpopulation / 1000f)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }
        private ModifierSet GetSlavePopulationPercentageModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.slavePopulationPercentageModifierDatas)
                set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, SlavePopulationPercentage));
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static PopulationState CreateFromData(PopulationStateData data)
        {
            PopulationState state = new PopulationState();

            state.Population = data.population;
            state.PopulationGrowth = data.populationGrowth;
            foreach (StatConstituentData scd in data.populationGrowthConstituentDatas)
            {
                StatConstituent statConstituent = StatConstituent.CreateFromData(scd);
                state.PopulationGrowthConstituents.AddStatConstituent(statConstituent);
            }
            state.SlavePopulation = data.slavePopulation;
            state.SlavePopulationPercentage = 100f * (float)state.SlavePopulation / ((float)(state.SlavePopulation + state.Population));

            return state;
        }
    }
}
