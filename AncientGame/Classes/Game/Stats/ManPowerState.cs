﻿using DataTypes;

namespace AncientGame
{
    class ManpowerState
    {
        /* -------- Properties -------- */
        public int Manpower { get; private set; }
        public int MaxManpower { get; private set; }
        public float ManpowerChange { get; private set; }
        public StatConstituentSet ManpowerChangeConstituents { get; private set; }

        /* -------- Constructors -------- */
        public ManpowerState()
        {
            ManpowerChangeConstituents = new StatConstituentSet();
        }

        /* -------- Public Methods -------- */
        public TooltipInfo GetTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = UIUtilities.GetBiDirectionalFloatDisplayString(ManpowerChange) + " " + Localization.Localize("text_manpower_change");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetModifierSet(gameManagers));
        }

        public void RecalculateManpowerChange()
        {
            ManpowerChange = ManpowerChangeConstituents.GetTotal();
        }
        public void RecalculateMaxManpower(LandTerritory lt, GameManagerContainer gameManagers)
        {
            float maxManpowerF = CalibrationManager.CampaignCalibration.Data.baseMaxManpower;
            maxManpowerF = lt.GetModifierSet(gameManagers).Modify(maxManpowerF, eModifierType.MAX_MANPOWER);

            MaxManpower = Mathf.Round(maxManpowerF);
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (ManpowerChange > 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positiveManpowerChangeModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(ManpowerChange)));
            }
            if (ManpowerChange < 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.negativeManpowerChangeModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(ManpowerChange)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        public void ChargeManpower(int amount)
        {
            Manpower = Mathf.ClampLower(Manpower - amount, 0);
        }

        /* -------- Static Methods -------- */
        public static ManpowerState CreateFromData(ManpowerStateData data)
        {
            ManpowerState state = new ManpowerState();

            state.Manpower = data.manpower;
            state.MaxManpower = data.maxManPower;
            state.ManpowerChange = data.manpowerChange;
            foreach (StatConstituentData scd in data.manpowerChangeConstituentDatas)
            {
                StatConstituent statConstituent = StatConstituent.CreateFromData(scd);
                state.ManpowerChangeConstituents.AddStatConstituent(statConstituent);
            }

            return state;
        }
    }
}
