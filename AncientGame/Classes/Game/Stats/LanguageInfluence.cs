﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eLanguageInfluenceType
    {
        LOCAL_TRADITIONS
    }

    class LanguageInfluence
    {
        /* -------- Properties -------- */
        public Language Language { get; private set; }
        public float Value { get; private set; }
        public eLanguageInfluenceType Type { get; private set; }

        /* -------- Static Methods -------- */
        public static LanguageInfluence CreateFromData(LanguageInfluenceData data, LanguageManager lm)
        {
            LanguageInfluence languageInfluence = new LanguageInfluence();

            languageInfluence.Language = lm.GetLanguage(data.languageID);
            languageInfluence.Value = data.value;
            languageInfluence.Type = DataUtilities.ParseLanguageInfluenceType(data.type);

            return languageInfluence;
        }
    }
}
