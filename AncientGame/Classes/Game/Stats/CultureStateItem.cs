﻿using DataTypes;

namespace AncientGame
{
    class CultureStateItem
    {
        /* -------- Properties -------- */
        public Culture Culture { get; private set; }
        public float Fraction { get; private set; }

        /* -------- Static Methods -------- */
        public static CultureStateItem CreateFromData(CultureStateItemData data, CultureManager cm)
        {
            CultureStateItem cultureStateItem = new CultureStateItem();

            cultureStateItem.Culture = cm.GetCulture(data.cultureID);
            cultureStateItem.Fraction = data.fraction;

            return cultureStateItem;
        }
    }
}
