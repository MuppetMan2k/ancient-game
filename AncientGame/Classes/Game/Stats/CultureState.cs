﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class CultureState
    {
        /* -------- Properties -------- */
        public List<CultureStateItem> CultureItems { get; private set; }
        public List<CultureInfluence> CultureInfluences { get; private set; }

        /* -------- Constructors -------- */
        public CultureState()
        {
            CultureItems = new List<CultureStateItem>();
            CultureInfluences = new List<CultureInfluence>();
        }

        /* -------- Static Methods -------- */
        public static CultureState CreateFromData(CultureStateData data, CultureManager cm)
        {
            CultureState state = new CultureState();

            foreach (CultureStateItemData csid in data.cultureStateItemDatas)
            {
                CultureStateItem csi = CultureStateItem.CreateFromData(csid, cm);
                state.CultureItems.Add(csi);
            }
            foreach (CultureInfluenceData cid in data.cultureInfluenceDatas)
            {
                CultureInfluence ci = CultureInfluence.CreateFromData(cid, cm);
                state.CultureInfluences.Add(ci);
            }

            return state;
        }
    }
}
