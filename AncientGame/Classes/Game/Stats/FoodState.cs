﻿using DataTypes;

namespace AncientGame
{
    class FoodState
    {
        /* -------- Properties -------- */
        public float Food { get; private set; }
        public StatConstituentSet FoodConstituents { get; private set; }

        /* -------- Constructors -------- */
        public FoodState()
        {
            FoodConstituents = new StatConstituentSet();
        }

        /* -------- Public Methods -------- */
        public TooltipInfo GetTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = UIUtilities.GetBiDirectionalFloatDisplayString(Food) + " " + Localization.Localize("text_food_stat");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetModifierSet(gameManagers));
        }

        public void RecalculateFood()
        {
            Food = FoodConstituents.GetTotal();
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (Food > 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positiveFoodModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(Food)));
            }
            if (Food < 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.negativeFoodModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(Food)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static FoodState CreateFromData(FoodStateData data)
        {
            FoodState state = new FoodState();

            state.Food = data.food;
            foreach (StatConstituentData scd in data.foodConstituentDatas)
            {
                StatConstituent statConstituent = StatConstituent.CreateFromData(scd);
                state.FoodConstituents.AddStatConstituent(statConstituent);
            }

            return state;
        }
    }
}
