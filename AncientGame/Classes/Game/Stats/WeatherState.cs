﻿using DataTypes;

namespace AncientGame
{
    class WeatherState
    {
        /* -------- Properties -------- */
        public float Warmth { get; private set; }
        public StatConstituentSet WarmthConstituents { get; private set; }
        public float Water { get; private set; }
        public StatConstituentSet WaterConstituents { get; private set; }

        /* -------- Constructors -------- */
        public WeatherState()
        {
            WarmthConstituents = new StatConstituentSet();
            WaterConstituents = new StatConstituentSet();
        }

        /* -------- Public Methods -------- */
        public TooltipInfo GetWarmthTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = UIUtilities.GetBiDirectionalFloatDisplayString(Warmth) + " " + Localization.Localize("text_warmth_stat");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetWarmthModifierSet(gameManagers));
        }
        public TooltipInfo GetWaterTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = UIUtilities.GetBiDirectionalFloatDisplayString(Water) + " " + Localization.Localize("text_water_stat");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetWaterModifierSet(gameManagers));
        }

        public void RecalculateWarmth()
        {
            Warmth = WarmthConstituents.GetTotal();
        }
        public void RecalculateWater()
        {
            Water = WaterConstituents.GetTotal();
        }

        public ModifierSet GetWarmthModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (Warmth > 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positiveWarmthModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(Warmth)));
            }
            if (Warmth < 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.negativeWarmthModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(Warmth)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }
        public ModifierSet GetWaterModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (Warmth > 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positiveWaterModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(Water)));
            }
            if (Warmth < 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.negativeWaterModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(Water)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static WeatherState CreateFromData(WeatherStateData data)
        {
            WeatherState state = new WeatherState();

            state.Warmth = data.warmth;
            foreach (StatConstituentData scd in data.warmthConstituentDatas)
            {
                StatConstituent statConstituent = StatConstituent.CreateFromData(scd);
                state.WarmthConstituents.AddStatConstituent(statConstituent);
            }
            state.Water = data.water;
            foreach (StatConstituentData scd in data.waterConstituentDatas)
            {
                StatConstituent statConstituent = StatConstituent.CreateFromData(scd);
                state.WaterConstituents.AddStatConstituent(statConstituent);
            }

            return state;
        }
    }
}
