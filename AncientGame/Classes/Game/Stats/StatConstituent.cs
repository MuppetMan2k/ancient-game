﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eStatConstituentType
    {
        LOCAL_TRADITIONS_POS,
        LOCAL_TRADITIONS_NEG,
        POPULATION_POS,
        POPULATION_NEG,
        OVERPOPULATION_NEG,
        NOBILITY_HAPPINESS_POS,
        NOBILITY_UNHAPPINESS_NEG,
        PLEB_HAPPINESS_POS,
        PLEB_UNHAPPINESS_NEG,
        HEALTH_AND_SANITATION_POS,
        DISEASE_AND_SQUALOR_NEG,
        POPULATION_GROWTH_POS,
        POPULATION_DECLINE_NEG,
        SLAVE_POPULATION_POS,
        SLAVE_POPULATION_NEG,
        FOOD_POS,
        FAMINE_NEG,
        FINANCIAL_INSTABILITY_NEG,
        DROUGHT_NEG,
        FREEZING_NEG,
        VARIED_DIET_POS,
        // Resources
        RESOURCE_AGRICULTURE_POS,
        RESOURCE_FOOD_POS,
        RESOURCE_LIVESTOCK_POS,
        RESOURCE_MINERAL_POS,
        RESOURCE_ANIMAL_POS,
        RESOURCE_LUXURY_POS,
        RESOURCE_WEAPONRY_POS,
        RESOURCE_GENERIC_POS
    }

    enum eStatConstituentBias
    {
        POSITIVE,
        NEGATIVE
    }

    class StatConstituent
    {
        /* -------- Properties -------- */
        public eStatConstituentType Type { get; private set; }
        public float Value { get; private set; }

        /* -------- Constructors -------- */
        public StatConstituent()
        {
            Type = eStatConstituentType.LOCAL_TRADITIONS_POS;
            Value = 0f;
        }
        public StatConstituent(eStatConstituentType inType, float inValue)
        {
            Type = inType;
            Value = inValue;
        }

        /* -------- Public Methods -------- */
        public void SetValue(float inValue)
        {
            Value = inValue;
        }

        public string GetIconTextureID()
        {
            switch (Type)
            {
                case eStatConstituentType.LOCAL_TRADITIONS_POS:
                    return "local-traditions-icon";
                case eStatConstituentType.LOCAL_TRADITIONS_NEG:
                    return "local-traditions-icon";
                case eStatConstituentType.POPULATION_POS:
                    return "population-icon";
                case eStatConstituentType.POPULATION_NEG:
                    return "population-icon";
                case eStatConstituentType.OVERPOPULATION_NEG:
                    return "overpopulation-icon";
                case eStatConstituentType.NOBILITY_HAPPINESS_POS:
                    return "nobility-happiness-icon";
                case eStatConstituentType.NOBILITY_UNHAPPINESS_NEG:
                    return "nobility-unhappiness-icon";
                case eStatConstituentType.PLEB_HAPPINESS_POS:
                    return "pleb-happiness-icon";
                case eStatConstituentType.PLEB_UNHAPPINESS_NEG:
                    return "pleb-unhappiness-icon";
                case eStatConstituentType.HEALTH_AND_SANITATION_POS:
                    return "health-and-sanitation-icon";
                case eStatConstituentType.DISEASE_AND_SQUALOR_NEG:
                    return "disease-and-squalor-icon";
                case eStatConstituentType.POPULATION_GROWTH_POS:
                    return "population-growth-icon";
                case eStatConstituentType.POPULATION_DECLINE_NEG:
                    return "population-decline-icon";
                case eStatConstituentType.SLAVE_POPULATION_POS:
                    return "slave-population-icon";
                case eStatConstituentType.SLAVE_POPULATION_NEG:
                    return "slave-population-icon";
                case eStatConstituentType.FOOD_POS:
                    return "food-icon";
                case eStatConstituentType.FAMINE_NEG:
                    return "famine-icon";
                case eStatConstituentType.FINANCIAL_INSTABILITY_NEG:
                    return "financial-instability-icon";
                case eStatConstituentType.DROUGHT_NEG:
                    return "drought-icon";
                case eStatConstituentType.FREEZING_NEG:
                    return "freezing-icon";
                case eStatConstituentType.VARIED_DIET_POS:
                    return "varied-diet-icon";
                // Resources
                case eStatConstituentType.RESOURCE_AGRICULTURE_POS:
                    return "agriculture-icon";
                case eStatConstituentType.RESOURCE_FOOD_POS:
                    return "food-type-icon";
                case eStatConstituentType.RESOURCE_LIVESTOCK_POS:
                    return "livestock-type-icon";
                case eStatConstituentType.RESOURCE_MINERAL_POS:
                    return "mineral-icon";
                case eStatConstituentType.RESOURCE_ANIMAL_POS:
                    return "animal-icon";
                case eStatConstituentType.RESOURCE_LUXURY_POS:
                    return "luxury-icon";
                case eStatConstituentType.RESOURCE_WEAPONRY_POS:
                    return "weaponry-icon";
                case eStatConstituentType.RESOURCE_GENERIC_POS:
                    return "generic-icon";
                default:
                    Debug.LogUnrecognizedValueWarning(Type);
                    return "";
            }
        }

        /* -------- Static Methods -------- */
        public static StatConstituent CreateFromData(StatConstituentData data)
        {
            StatConstituent statConstituent = new StatConstituent();

            statConstituent.Type = DataUtilities.ParseStatConstituentType(data.constituentType);
            statConstituent.Value = data.constituentValue;

            return statConstituent;
        }

        public static string GetName(eStatConstituentType type)
        {
            switch (type)
            {
                case eStatConstituentType.LOCAL_TRADITIONS_POS:
                    return Localization.Localize("text_stat-constituent_local-traditions-pos");
                case eStatConstituentType.LOCAL_TRADITIONS_NEG:
                    return Localization.Localize("text_stat-constituent_local-traditions-neg");
                case eStatConstituentType.POPULATION_POS:
                    return Localization.Localize("text_stat-constituent_population-pos");
                case eStatConstituentType.POPULATION_NEG:
                    return Localization.Localize("text_stat-constituent_population-neg");
                case eStatConstituentType.OVERPOPULATION_NEG:
                    return Localization.Localize("text_stat-constituent_overpopulation-neg");
                case eStatConstituentType.NOBILITY_HAPPINESS_POS:
                    return Localization.Localize("text_stat-constituent_nobility-happiness-pos");
                case eStatConstituentType.NOBILITY_UNHAPPINESS_NEG:
                    return Localization.Localize("text_stat-constituent_nobility-unhappiness-neg");
                case eStatConstituentType.PLEB_HAPPINESS_POS:
                    return Localization.Localize("text_stat-constituent_pleb-happiness-pos");
                case eStatConstituentType.PLEB_UNHAPPINESS_NEG:
                    return Localization.Localize("text_stat-constituent_pleb-unhappiness-neg");
                case eStatConstituentType.HEALTH_AND_SANITATION_POS:
                    return Localization.Localize("text_stat-constituent_health-and-sanitation-pos");
                case eStatConstituentType.DISEASE_AND_SQUALOR_NEG:
                    return Localization.Localize("text_stat-constituent_disease-and-squalor-neg");
                case eStatConstituentType.POPULATION_GROWTH_POS:
                    return Localization.Localize("text_stat-constituent_population-growth-pos");
                case eStatConstituentType.POPULATION_DECLINE_NEG:
                    return Localization.Localize("text_stat-constituent_population-decline-neg");
                case eStatConstituentType.SLAVE_POPULATION_POS:
                    return Localization.Localize("text_stat-constituent_slave-population-pos");
                case eStatConstituentType.SLAVE_POPULATION_NEG:
                    return Localization.Localize("text_stat-constituent_slave-population-neg");
                case eStatConstituentType.FOOD_POS:
                    return Localization.Localize("text_stat-constituent_food-pos");
                case eStatConstituentType.FAMINE_NEG:
                    return Localization.Localize("text_stat-constituent_famine-neg");
                case eStatConstituentType.FINANCIAL_INSTABILITY_NEG:
                    return Localization.Localize("text_stat-constituent_financial-instability-neg");
                case eStatConstituentType.DROUGHT_NEG:
                    return Localization.Localize("text_stat-constituent_drought-neg");
                case eStatConstituentType.FREEZING_NEG:
                    return Localization.Localize("text_stat-constituent_freezing-neg");
                case eStatConstituentType.VARIED_DIET_POS:
                    return Localization.Localize("text_stat-constituent_varied-diet-pos");
                // Resources
                case eStatConstituentType.RESOURCE_AGRICULTURE_POS:
                    return Localization.Localize("text_stat-constituent_resource-agriculture-pos");
                case eStatConstituentType.RESOURCE_FOOD_POS:
                    return Localization.Localize("text_stat-constituent_resource-food-pos");
                case eStatConstituentType.RESOURCE_LIVESTOCK_POS:
                    return Localization.Localize("text_stat-constituent_resource-livestock-pos");
                case eStatConstituentType.RESOURCE_MINERAL_POS:
                    return Localization.Localize("text_stat-constituent_resource-mineral-pos");
                case eStatConstituentType.RESOURCE_ANIMAL_POS:
                    return Localization.Localize("text_stat-constituent_resource-animal-pos");
                case eStatConstituentType.RESOURCE_LUXURY_POS:
                    return Localization.Localize("text_stat-constituent_resource-luxury-pos");
                case eStatConstituentType.RESOURCE_WEAPONRY_POS:
                    return Localization.Localize("text_stat-constituent_resource-weaponry-pos");
                case eStatConstituentType.RESOURCE_GENERIC_POS:
                    return Localization.Localize("text_stat-constituent_resource-generic-pos");
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return "";
            }
        }
        public static eUIAttitude GetUIAttitude(eStatConstituentType type)
        {
            string typeString = type.ToString();

            if (typeString.EndsWith("_POS"))
                return eUIAttitude.POSITIVE;

            if (typeString.EndsWith("_NEG"))
                return eUIAttitude.NEGATIVE;

            return eUIAttitude.NEUTRAL;
        }
        public static eStatConstituentBias GetBias(eStatConstituentType type)
        {
            string typeString = type.ToString();

            if (typeString.EndsWith("_NEG"))
                return eStatConstituentBias.NEGATIVE;

            return eStatConstituentBias.POSITIVE;
        }
    }
}
