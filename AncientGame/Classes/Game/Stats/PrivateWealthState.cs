﻿using DataTypes;

namespace AncientGame
{
    class PrivateWealthState
    {
        /* -------- Properties -------- */
        public float PrivateWealth { get; private set; }
        public float PrivateWeathChange { get; private set; }
        public StatConstituentSet PrivateWealthChangeConstituents { get; private set; }

        /* -------- Constructors -------- */
        public PrivateWealthState()
        {
            PrivateWealthChangeConstituents = new StatConstituentSet();
        }

        /* -------- Public Methods -------- */
        public TooltipInfo GetPrivateWealthChangeTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = UIUtilities.GetBiDirectionalFloatDisplayString(PrivateWeathChange) + " " + Localization.Localize("text_private_wealth_change");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetPrivateWealthChangeModifierSet(gameManagers));
        }
        public TooltipInfo GetPrivateWealthTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = Localization.Localize("text_private_wealth_tooltip");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetPrivateWealthModifierSet(gameManagers));
        }

        public void RecalculatePrivateWealthChange()
        {
            PrivateWeathChange = PrivateWealthChangeConstituents.GetTotal();
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            set.Merge(GetPrivateWealthChangeModifierSet(gameManagers));
            set.Merge(GetPrivateWealthModifierSet(gameManagers));

            return set;
        }

        /* -------- Private Methods --------- */
        private ModifierSet GetPrivateWealthChangeModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (PrivateWeathChange > 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positivePrivateWealthChangeModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(PrivateWeathChange)));
            }
            if (PrivateWeathChange < 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.negativePrivateWealthChangeModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(PrivateWeathChange)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }
        private ModifierSet GetPrivateWealthModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.privateWealthModifierDatas)
                set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(PrivateWealth)));
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static PrivateWealthState CreateFromData(PrivateWealthStateData data)
        {
            PrivateWealthState state = new PrivateWealthState();

            state.PrivateWealth = data.privateWealth;
            state.PrivateWeathChange = data.privateWealthChange;
            foreach (StatConstituentData scd in data.privateWealthChangeConstituentDatas)
            {
                StatConstituent statConstituent = StatConstituent.CreateFromData(scd);
                state.PrivateWealthChangeConstituents.AddStatConstituent(statConstituent);
            }

            return state;
        }
    }
}
