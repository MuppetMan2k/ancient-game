﻿using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eReligionInfluenceType
    {
        LOCAL_TRADITIONS
    }

    class ReligionInfluence
    {
        /* -------- Properties -------- */
        public Religion Religion { get; private set; }
        public float Value { get; private set; }
        public eReligionInfluenceType Type { get; private set; }

        /* -------- Static Methods -------- */
        public static ReligionInfluence CreateFromData(ReligionInfluenceData data, ReligionManager rm)
        {
            ReligionInfluence religionInfluence = new ReligionInfluence();

            religionInfluence.Religion = rm.GetReligion(data.religionID);
            religionInfluence.Value = data.value;
            religionInfluence.Type = DataUtilities.ParseReligionInfluenceType(data.type);

            return religionInfluence;
        }
    }
}
