﻿namespace AncientGame
{
    class HarvestState
    {
        /* -------- Properties -------- */
        public int HarvestLevel { get; private set; }

        /* -------- Private Fields -------- */
        // Constants
        private const int MAX_VALUE = 10;
        private const int MIN_VALUE = 0;

        /* -------- Public Methods -------- */
        public float GetHarvestLevelFraction()
        {
            return (float)(HarvestLevel - MIN_VALUE) / (float)(MAX_VALUE - MIN_VALUE);
        }
        public int GetNumLevelBelowMax()
        {
            return MAX_VALUE - HarvestLevel;
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            float multiplier = CalibrationManager.CampaignCalibration.Data.resourceProductionBaseHarvestMultiplier;
            multiplier *= Mathf.Pow(CalibrationManager.CampaignCalibration.Data.resourceProductionReducedHarvestMultiplier, GetNumLevelBelowMax());

            if (multiplier != 1f)
            {
                foreach (eResourceType rt in gameManagers.ResourceManager.ResourceTypesAffectedByHarvest)
                {
                    Modifier mod = new Modifier(ModifierTypeSet.Create_ResourceTypeProduction(rt), eModificationType.MULTIPLICATION, ModifierCondition.None, multiplier);
                    // TODO - apply "primary tier resources only" caveat?
                    set.Modifiers.Add(mod);
                }
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static HarvestState CreateFromData(int data)
        {
            HarvestState state = new HarvestState();

            state.HarvestLevel = Mathf.Clamp(data, MIN_VALUE, MAX_VALUE);

            return state;
        }
    }
}
