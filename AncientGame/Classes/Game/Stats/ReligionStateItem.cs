﻿using DataTypes;

namespace AncientGame
{
    class ReligionStateItem
    {
        /* -------- Properties -------- */
        public Religion Religion { get; private set; }
        public float Fraction { get; private set; }

        /* -------- Static Methods -------- */
        public static ReligionStateItem CreateFromData(ReligionStateItemData data, ReligionManager rm)
        {
            ReligionStateItem religionStateItem = new ReligionStateItem();

            religionStateItem.Religion = rm.GetReligion(data.religionID);
            religionStateItem.Fraction = data.fraction;

            return religionStateItem;
        }
    }
}
