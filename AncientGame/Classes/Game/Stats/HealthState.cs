﻿using DataTypes;

namespace AncientGame
{
    class HealthState
    {
        /* -------- Properties -------- */
        public float Health { get; private set; }
        public StatConstituentSet HealthConstituents { get; private set; }

        /* -------- Constructors -------- */
        public HealthState()
        {
            HealthConstituents = new StatConstituentSet();
        }

        /* -------- Public Methods -------- */
        public TooltipInfo GetTooltipInfo(GameManagerContainer gameManagers)
        {
            string staticText = UIUtilities.GetBiDirectionalFloatDisplayString(Health) + " " + Localization.Localize("text_health_stat");
            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, GetModifierSet(gameManagers));
        }

        public void RecalculateHealth()
        {
            Health = HealthConstituents.GetTotal();
        }

        public ModifierSet GetModifierSet(GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();
            ModificationObjectSet objectSet = new ModificationObjectSet();

            if (Health > 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.positiveHealthModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(Health)));
            }
            if (Health < 0f)
            {
                foreach (ModifierData md in CalibrationManager.CampaignCalibration.Data.negativeHealthModifierDatas)
                    set.Modifiers.Add(MultipliedModifier.CreateFromData(md, gameManagers, Mathf.Abs(Health)));
            }
            set.TestIsSatisfied(objectSet);

            return set;
        }

        /* -------- Static Methods -------- */
        public static HealthState CreateFromData(HealthStateData data)
        {
            HealthState state = new HealthState();

            state.Health = data.health;
            foreach (StatConstituentData scd in data.healthConstituentDatas)
            {
                StatConstituent statConstituent = StatConstituent.CreateFromData(scd);
                state.HealthConstituents.AddStatConstituent(statConstituent);
            }

            return state;
        }
    }
}
