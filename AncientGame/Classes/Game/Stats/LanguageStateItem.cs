﻿using DataTypes;

namespace AncientGame
{
    class LanguageStateItem
    {
        /* -------- Properties -------- */
        public Language Language { get; private set; }
        public float Fraction { get; private set; }

        /* -------- Static Methods -------- */
        public static LanguageStateItem CreateFromData(LanguageStateItemData data, LanguageManager lm)
        {
            LanguageStateItem languageStateItem = new LanguageStateItem();

            languageStateItem.Language = lm.GetLanguage(data.languageID);
            languageStateItem.Fraction = data.fraction;

            return languageStateItem;
        }
    }
}
