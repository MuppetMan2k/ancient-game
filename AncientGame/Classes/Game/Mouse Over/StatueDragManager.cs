﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class StatueDragManager
    {
        /* -------- Properties -------- */
        public Statue DraggingStatue { get; private set; }

        /* -------- Private Fields -------- */
        private StatueDragMouseOver currMouseOver;
        private StatueDragMouseOver prevMouseOver;

        /* -------- Public Methods -------- */
        public void Update(GraphicsComponent g, InputComponent i, UIComponent ui, GameManagerContainer gameManagers)
        {
            if (DraggingStatue == null)
            {
                if (ui.Tooltip.Source == eCursorChangeSource.STATUE_DRAGGING)
                    ui.Tooltip.ResetTooltipInfo();
                if (ui.Cursor.Icon.Source == eCursorChangeSource.STATUE_DRAGGING)
                    ui.Cursor.Icon.ResetMode();

                currMouseOver = null;
                prevMouseOver = null;
                return;
            }

            prevMouseOver = currMouseOver;

            if (!gameManagers.ViewManager.ViewMode.StatuesAreSelectable())
                CancelStatueDrag();

            FindGameMouseOver(g, i, gameManagers, gameManagers.CameraManager, gameManagers.ViewManager);
            OnGameMouseOverChosen(ui, gameManagers.BattleManager);

            if (i.MouseButtonIsUp(eMouseButton.LEFT_BUTTON))
            {
                if (currMouseOver == null)
                    CancelStatueDrag();
                else
                    OnStatueReleased(gameManagers);
            }

            if (i.MouseButtonIsPressing(eMouseButton.RIGHT_BUTTON))
                CancelStatueDrag();
        }

        public void DrawStatueDraggingOverlay(StatueDragGraphicsManager gm)
        {
            foreach (StatueMovementOption smo in DraggingStatue.MovementOptions.MovementOptions)
                gm.DrawStatueMovementOption(smo, currMouseOver);
        }

        public void OnStatuePressed(Statue s, BattleManager bm)
        {
            if (bm.ForceIsInvolvedInBattle(s as Force))
                return;

            DraggingStatue = s;
            DraggingStatue.StartDragging();
        }

        public void CancelStatueDrag()
        {
            if (DraggingStatue != null)
            {
                DraggingStatue.StopDragging();
                DraggingStatue = null;
            }
        }

        /* -------- Private Methods --------- */
        private void OnStatueReleased(GameManagerContainer gameManagers)
        {
            if (currMouseOver.LandTerritory != null)
                OnStatueReleasedOnLandTerritory(gameManagers);
            else if (currMouseOver.SeaTerritory != null)
                OnStatueReleasedOnSeaTerritory(gameManagers);
            else if (currMouseOver.Settlement != null)
                OnStatueReleasedOnSettlement(gameManagers);
            else if (currMouseOver.Statue != null)
                OnStatueReleasedOnStatue(gameManagers.BattleManager);

            DraggingStatue.StopDragging();
            DraggingStatue = null;
        }
        private void OnStatueReleasedOnLandTerritory(GameManagerContainer gameManagers)
        {
            LandTerritory landTerritory = currMouseOver.LandTerritory;
            StatueMovementOption smo = DraggingStatue.MovementOptions.GetMovementOptionToLandTerritory(landTerritory);
            if (smo == null || !smo.CanAffordCost)
                return;

            StatuePosition newPosition;

            // Remain in land territory
            if (smo.MovementType == eStatueMovementType.REMAIN_LAND)
            {
                newPosition = new StatuePosition(DraggingStatue.Position.LandTerritory, currMouseOver.MouseMSPosition);
                DraggingStatue.SetPosition(newPosition);
                return;
            }

            newPosition = new StatuePosition(currMouseOver.LandTerritory, currMouseOver.MouseMSPosition);
            DraggingStatue.SetPosition(newPosition);
            DraggingStatue.OnMoveIntoNewLandTerritory(smo, gameManagers);

            // Check for enemy forces for battle initiation
            Force draggingForce = DraggingStatue as Force;
            List<Force> enemyForces = gameManagers.FactionManager.GetForcesInLandTerritory(currMouseOver.LandTerritory);
            enemyForces = enemyForces.FindAll(x => DraggingStatue.OwningFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(x.OwningFaction));
            if (draggingForce != null && enemyForces != null && enemyForces.Count > 0)
            {
                gameManagers.BattleManager.AddForceToBattle(draggingForce, enemyForces, BattleLocation.CreateFromLandTerritory(currMouseOver.LandTerritory));
            }

            // TODO - check for effects of moving into land territory (e.g. attrition, trespassing etc.)
        }
        private void OnStatueReleasedOnSeaTerritory(GameManagerContainer gameManagers)
        {
            SeaTerritory seaTerritory = currMouseOver.SeaTerritory;
            StatueMovementOption smo = DraggingStatue.MovementOptions.GetMovementOptionToSeaTerritory(seaTerritory);
            if (smo == null || !smo.CanAffordCost)
                return;

            StatuePosition newPosition;

            // Remain in sea territory
            if (smo.MovementType == eStatueMovementType.REMAIN_SEA)
            {
                newPosition = new StatuePosition(DraggingStatue.Position.SeaTerritory, currMouseOver.MouseMSPosition);
                DraggingStatue.SetPosition(newPosition);
                return;
            }

            newPosition = new StatuePosition(currMouseOver.SeaTerritory, currMouseOver.MouseMSPosition);
            DraggingStatue.SetPosition(newPosition);
            DraggingStatue.OnMoveIntoNewSeaTerritory(smo, gameManagers);

            // Check for enemy forces for battle initiation
            Force draggingForce = DraggingStatue as Force;
            List<Force> enemyForces = gameManagers.FactionManager.GetForcesInSeaTerritory(currMouseOver.SeaTerritory);
            enemyForces = enemyForces.FindAll(x => DraggingStatue.OwningFaction.Variables.DiplomaticContractSet.HasWarContractWithFaction(x.OwningFaction));
            if (draggingForce != null && enemyForces != null && enemyForces.Count > 0)
            {
                gameManagers.BattleManager.AddForceToBattle(draggingForce, enemyForces, BattleLocation.CreateFromSeaTerritory(currMouseOver.SeaTerritory));
            }

            // TODO - check for effects of moving into sea territory (e.g. deep sea attrition etc.)
        }
        private void OnStatueReleasedOnSettlement(GameManagerContainer gameManagers)
        {
            Settlement settlement = currMouseOver.Settlement;
            StatueMovementOption smo = DraggingStatue.MovementOptions.GetMovementOptionToSettlement(settlement);
            if (smo == null || !smo.CanAffordCost)
                return;

            if (smo.MovementType == eStatueMovementType.GARRISON_LAND || smo.MovementType == eStatueMovementType.GARRISON_SEA)
            {
                StatuePosition newPosition = new StatuePosition(currMouseOver.Settlement);
                DraggingStatue.SetPosition(newPosition);
                DraggingStatue.OnMoveIntoNewSettlement(smo, gameManagers);
		// TODO - maybe merge into force if one exists?
                return;
            }

            // TODO - Initiate battle on settlement
        }
        private void OnStatueReleasedOnStatue(BattleManager bm)
        {
            // Merging two forces
            if (DraggingStatue.OwningFaction == currMouseOver.Statue.OwningFaction && DraggingStatue.Position.EquivalentTo(currMouseOver.Statue.Position) && !bm.ForceIsInvolvedInBattle(currMouseOver.Statue as Force))
            {
                SeaForce draggingSeaForce = DraggingStatue as SeaForce;
                SeaForce mouseOverSeaForce = currMouseOver.Statue as SeaForce;
                if (draggingSeaForce != null && mouseOverSeaForce != null && !(draggingSeaForce is Navy && mouseOverSeaForce is Navy))
                {
                    if (!StatueControlUtility.SeaForcesAreSmallEnoughToMerge(draggingSeaForce, mouseOverSeaForce))
                        return;

                    mouseOverSeaForce.MergeForce(draggingSeaForce);
                    return;
                }

                LandForce draggingLandForce = DraggingStatue as LandForce;
                LandForce mouseOverLandForce = currMouseOver.Statue as LandForce;
                if (draggingLandForce != null && mouseOverLandForce != null && !(draggingLandForce is Army && mouseOverLandForce is Army))
                {
                    if (!StatueControlUtility.LandForcesAreSmallEnoughToMerge(draggingLandForce, mouseOverLandForce))
                        return;

                    mouseOverLandForce.MergeForce(draggingLandForce);
                    return;
                }
            }
        }

        private void FindGameMouseOver(GraphicsComponent g, InputComponent i, GameManagerContainer gameManagers, CameraManager cm, ViewManager vm)
        {
            currMouseOver = null;

            Point mousePSPos = i.GetMousePosition();

            // Statues
            if (vm.ViewMode.StatuesAreSelectable())
            {
                Statue mouseOverStatue = gameManagers.FactionManager.GetPSMouseOverStatue(mousePSPos, vm);
                if (mouseOverStatue != null && !mouseOverStatue.Equals(DraggingStatue))
                {
                    currMouseOver = StatueDragMouseOver.CreateStatueMouseOver(mouseOverStatue);
                    return;
                }
            }

            Vector2 mouseMSPos;
            if (!SpaceUtilities.TryConvertPSToMS(mousePSPos, g, cm, out mouseMSPos))
                return;

            // Settlements
            if (vm.ViewMode.SettlementsAreSelectable())
            {
                Settlement mouseOverSettlement = gameManagers.MapManager.LandTerritoryManager.GetMouseOverSettlement(mouseMSPos);
                if (mouseOverSettlement != null)
                {
                    currMouseOver = StatueDragMouseOver.CreateSettlementMouseOver(mouseOverSettlement);
                    return;
                }
            }

            // Land territories
            if (vm.ViewMode.LandTerritoriesAreSelectable())
            {
                LandTerritory mouseOverLandTerritory = gameManagers.MapManager.LandTerritoryManager.GetMouseOverLandTerritory(mouseMSPos);
                if (mouseOverLandTerritory != null)
                {
                    currMouseOver = StatueDragMouseOver.CreateLandTerritoryMouseOver(mouseOverLandTerritory, mouseMSPos);
                    return;
                }
            }

            // Sea territories
            if (vm.ViewMode.SeaTerritoriesAreSelectable())
            {
                SeaTerritory mouseOverSeaTerritory = gameManagers.MapManager.SeaTerritoryManager.GetMouseOverSeaTerritory(mouseMSPos);
                if (mouseOverSeaTerritory != null)
                {
                    currMouseOver = StatueDragMouseOver.CreateSeaTerritoryMouseOver(mouseOverSeaTerritory, mouseMSPos);
                    return;
                }
            }
        }
        private void OnGameMouseOverChosen(UIComponent ui, BattleManager bm)
        {
            if (currMouseOver == null)
            {
                if (prevMouseOver == null)
                    return;

                ui.Tooltip.ResetTooltipInfo();
                ui.Cursor.Icon.ResetMode(); ;
                return;
            }

            if (prevMouseOver == null || !prevMouseOver.EquivalentTo(currMouseOver))
            {
                TooltipInfo info;
                eCursorIconMode cursorIconMode;
                GetMouseOverTooltipInfoAndCursorIcon(out info, out cursorIconMode, bm);
                if (info != null)
                {
                    ui.SetTooltipInfo(info, eCursorChangeSource.STATUE_DRAGGING);
                    ui.Cursor.Icon.SetMode(cursorIconMode, eCursorChangeSource.STATUE_DRAGGING);
                }
                else if (ui.Tooltip.Source == eCursorChangeSource.STATUE_DRAGGING)
                {
                    ui.Tooltip.ResetTooltipInfo();
                    ui.Cursor.Icon.ResetMode();
                }
            }
        }

        private void GetMouseOverTooltipInfoAndCursorIcon(out TooltipInfo tooltipInfo, out eCursorIconMode cursorIcon, BattleManager bm)
        {
            if (currMouseOver.LandTerritory != null)
            {
                GetMouseOverTooltipInfoAndCursorIconForLandTerritory(out tooltipInfo, out cursorIcon);
                return;
            }

            if (currMouseOver.SeaTerritory != null)
            {
                GetMouseOverTooltipInfoAndCursorIconForSeaTerritory(out tooltipInfo, out cursorIcon);
                return;
            }

            if (currMouseOver.Settlement != null)
            {
                GetMouseOverTooltipInfoAndCursorIconForSettlement(out tooltipInfo, out cursorIcon);
                return;
            }

            if (currMouseOver.Statue != null)
            {
                GetMouseOverTooltipInfoAndCursorIconForStatue(out tooltipInfo, out cursorIcon, bm);
                return;
            }

            tooltipInfo = null;
            cursorIcon = eCursorIconMode.NONE;
        }
        private void GetMouseOverTooltipInfoAndCursorIconForLandTerritory(out TooltipInfo tooltipInfo, out eCursorIconMode cursorIcon)
        {
            StatueMovementOption smo = DraggingStatue.MovementOptions.GetMovementOptionToLandTerritory(currMouseOver.LandTerritory);

            if (smo == null)
            {
                tooltipInfo = null;
                cursorIcon = eCursorIconMode.NONE;
                return;
            }

            string costStringID = (smo.MovementPointCost == 1) ? "text_statue_movement_point" : "text_statue_movement_points";
            string costString = String.Format(Localization.Localize(costStringID), smo.MovementPointCost.ToString("0"));

            if (!smo.CanAffordCost)
            {
                int pointShortfall = smo.MovementPointCost - DraggingStatue.MovementPoints - DraggingStatue.AdditionalMovementPoints;
                int turnsToMakeUpPointShortfall = Mathf.Ceiling((float)pointShortfall / (float)DraggingStatue.MaxMovementPoints);
                turnsToMakeUpPointShortfall = Mathf.ClampLower(turnsToMakeUpPointShortfall, 1);

                string turnsTextID = (turnsToMakeUpPointShortfall == 1) ? "text_statue_turn" : "text_statue_turns";
                string turnsText = String.Format(Localization.Localize(turnsTextID), turnsToMakeUpPointShortfall.ToString("0"));

                string text = Localization.Localize("text_statue_insufficient_points");
                text = String.Format(text, costString, turnsText);

                tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                cursorIcon = eCursorIconMode.NONE;
                return;
            }

            string landTerritoryString = currMouseOver.LandTerritory.GetTerritoryNameString();

            switch (smo.MovementType)
            {
                case eStatueMovementType.FREE_MOVEMENT_LAND:
                    {
                        string text = String.Format(Localization.Localize("text_statue_free-movement-land_to_land-territory"), landTerritoryString, costString);
                        tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                        cursorIcon = eCursorIconMode.LAND_MOVEMENT;
                        return;
                    }
                case eStatueMovementType.TRESPASS_LAND:
                    {
                        string text = String.Format(Localization.Localize("text_statue_trespass-land_to_land-territory"), landTerritoryString, costString);
                        tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                        cursorIcon = eCursorIconMode.LAND_MOVEMENT;
                        return;
                    }
                case eStatueMovementType.ATTACK_LAND:
                    {
                        string text = String.Format(Localization.Localize("text_statue_attack-land_to_land-territory"), landTerritoryString, costString);
                        tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                        cursorIcon = eCursorIconMode.LAND_ATTACK;
                        return;
                    }
                case eStatueMovementType.REPEL_OCCUPIERS:
                    {
                        string text = String.Format(Localization.Localize("text_statue_repel-occupiers_to_land-territory"), landTerritoryString, costString);
                        tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                        cursorIcon = eCursorIconMode.LAND_ATTACK;
                        return;
                    }
                case eStatueMovementType.REMAIN_LAND:
                    {
                        string text = String.Format(Localization.Localize("text_statue_remain-land_in_land-territory"), landTerritoryString);
                        tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                        cursorIcon = eCursorIconMode.LAND_REMAIN;
                        return;
                    }
                default:
                    tooltipInfo = null;
                    cursorIcon = eCursorIconMode.NONE;
                    return;
            }
        }
        private void GetMouseOverTooltipInfoAndCursorIconForSeaTerritory(out TooltipInfo tooltipInfo, out eCursorIconMode cursorIcon)
        {
            StatueMovementOption smo = DraggingStatue.MovementOptions.GetMovementOptionToSeaTerritory(currMouseOver.SeaTerritory);

            if (smo == null)
            {
                tooltipInfo = null;
                cursorIcon = eCursorIconMode.NONE;
                return;
            }

            string costStringID = (smo.MovementPointCost == 1) ? "text_statue_movement_point" : "text_statue_movement_points";
            string costString = String.Format(Localization.Localize(costStringID), smo.MovementPointCost.ToString("0"));

            if (!smo.CanAffordCost)
            {
                int pointShortfall = smo.MovementPointCost - DraggingStatue.MovementPoints - DraggingStatue.AdditionalMovementPoints;
                int turnsToMakeUpPointShortfall = Mathf.Ceiling((float)pointShortfall / (float)DraggingStatue.MaxMovementPoints);
                turnsToMakeUpPointShortfall = Mathf.ClampLower(turnsToMakeUpPointShortfall, 1);

                string turnsTextID = (turnsToMakeUpPointShortfall == 1) ? "text_statue_turn" : "text_statue_turns";
                string turnsText = String.Format(Localization.Localize(turnsTextID), turnsToMakeUpPointShortfall.ToString("0"));

                string text = Localization.Localize("text_statue_insufficient_points");
                text = String.Format(text, costString, turnsText);

                tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                cursorIcon = eCursorIconMode.NONE;
                return;
            }

            if (smo.MovementType == eStatueMovementType.REMAIN_SEA)
            {
                string remainText;
                if (currMouseOver.SeaTerritory is CoastalSeaTerritory)
                    remainText = Localization.Localize("text_statue_remain-sea_in_sea-territory");
                else
                    remainText = Localization.Localize("text_statue_remain-sea_in_coastal-sea-territory");
                tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(remainText);
                cursorIcon = eCursorIconMode.SEA_REMAIN;
                return;
            }

            string tooltipText;
            if (currMouseOver.SeaTerritory is CoastalSeaTerritory)
                tooltipText = String.Format(Localization.Localize("text_statue_free-movement-sea_to_sea-territory"), costString);
            else
                tooltipText = String.Format(Localization.Localize("text_statue_free-movement-sea_to_coastal-sea-territory"), costString);
            tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(tooltipText);
            cursorIcon = eCursorIconMode.SEA_MOVEMENT;
        }
        private void GetMouseOverTooltipInfoAndCursorIconForSettlement(out TooltipInfo tooltipInfo, out eCursorIconMode cursorIcon)
        {
            StatueMovementOption smo = DraggingStatue.MovementOptions.GetMovementOptionToSettlement(currMouseOver.Settlement);

            if (smo == null)
            {
                tooltipInfo = null;
                cursorIcon = eCursorIconMode.NONE;
                return;
            }

            string costStringID = (smo.MovementPointCost == 1) ? "text_statue_movement_point" : "text_statue_movement_points";
            string costString = String.Format(Localization.Localize(costStringID), smo.MovementPointCost.ToString("0"));

            if (!smo.CanAffordCost)
            {
                int pointShortfall = smo.MovementPointCost - DraggingStatue.MovementPoints - DraggingStatue.AdditionalMovementPoints;
                int turnsToMakeUpPointShortfall = Mathf.Ceiling((float)pointShortfall / (float)DraggingStatue.MaxMovementPoints);
                turnsToMakeUpPointShortfall = Mathf.ClampLower(turnsToMakeUpPointShortfall, 1);

                string turnsTextID = (turnsToMakeUpPointShortfall == 1) ? "text_statue_turn" : "text_statue_turns";
                string turnsText = String.Format(Localization.Localize(turnsTextID), turnsToMakeUpPointShortfall.ToString("0"));

                string text = Localization.Localize("text_statue_insufficient_points");
                text = String.Format(text, costString, turnsText);

                tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                cursorIcon = eCursorIconMode.NONE;
                return;
            }

            string settlementString = currMouseOver.Settlement.ParentLandTerritory.GetNameString();

            switch (smo.MovementType)
            {
                case eStatueMovementType.GARRISON_LAND:
                    {
                        string text = String.Format(Localization.Localize("text_statue_garrison-land_to_settlement"), settlementString, costString);
                        tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                        cursorIcon = eCursorIconMode.GARRISON;
                        return;
                    }
                case eStatueMovementType.GARRISON_SEA:
                    {
                        string text = String.Format(Localization.Localize("text_statue_free-movement-sea_to_settlement"), settlementString, costString);
                        tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                        cursorIcon = eCursorIconMode.SEA_MOVEMENT;
                        return;
                    }
                case eStatueMovementType.BESIEGE_LAND:
                    {
                        string text = String.Format(Localization.Localize("text_statue_besiege-land_to_settlement"), settlementString, costString);
                        tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                        cursorIcon = eCursorIconMode.BESIEGE;
                        return;
                    }
                case eStatueMovementType.REPEL_OCCUPIERS:
                    {
                        string text = String.Format(Localization.Localize("text_statue_repel-occupiers_to_settlement"), settlementString, costString);
                        tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                        cursorIcon = eCursorIconMode.BESIEGE;
                        return;
                    }
                default:
                    tooltipInfo = null;
                    cursorIcon = eCursorIconMode.NONE;
                    return;
            }
        }
        private void GetMouseOverTooltipInfoAndCursorIconForStatue(out TooltipInfo tooltipInfo, out eCursorIconMode cursorIcon, BattleManager bm)
        {
            // Merging two forces
            if (DraggingStatue.OwningFaction == currMouseOver.Statue.OwningFaction && DraggingStatue.Position.EquivalentTo(currMouseOver.Statue.Position))
            {
                if (bm.ForceIsInvolvedInBattle(currMouseOver.Statue as Force))
                {
                    tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_statue_engaged_in_battle"));
                    cursorIcon = eCursorIconMode.NONE;
                    return;
                }

                SeaForce draggingSeaForce = DraggingStatue as SeaForce;
                SeaForce mouseOverSeaForce = currMouseOver.Statue as SeaForce;
                if (draggingSeaForce != null && mouseOverSeaForce != null && !(draggingSeaForce is Navy && mouseOverSeaForce is Navy))
                {
                    if (!StatueControlUtility.SeaForcesAreSmallEnoughToMerge(draggingSeaForce, mouseOverSeaForce))
                    {
                        tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_statue_cannot_merge_navy_unit_limit"));
                        cursorIcon = eCursorIconMode.NONE;
                        return;
                    }

                    string text = Localization.Localize("text_statue_merge_to_sea-statue");
                    tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                    cursorIcon = eCursorIconMode.MERGE;
                    return;
                }

                LandForce draggingLandForce = DraggingStatue as LandForce;
                LandForce mouseOverLandForce = currMouseOver.Statue as LandForce;
                if (draggingLandForce != null && mouseOverLandForce != null && !(draggingLandForce is Army && mouseOverLandForce is Army))
                {
                    if (!StatueControlUtility.LandForcesAreSmallEnoughToMerge(draggingLandForce, mouseOverLandForce))
                    {
                        tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_statue_cannot_merge_army_unit_limit"));
                        cursorIcon = eCursorIconMode.NONE;
                        return;
                    }

                    string text = Localization.Localize("text_statue_merge_to_land-statue");
                    tooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(text);
                    cursorIcon = eCursorIconMode.MERGE;
                    return;
                }
            }

            tooltipInfo = null;
            cursorIcon = eCursorIconMode.NONE;
        }
    }
}
