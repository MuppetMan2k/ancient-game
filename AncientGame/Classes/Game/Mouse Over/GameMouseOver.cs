﻿using System.Collections.Generic;

namespace AncientGame
{
    class GameMouseOver
    {
        /* -------- Properties -------- */
        public Statue Statue { get; private set; }
        public List<TradeRoute> TradeRoutes { get; private set; }
        public River River { get; private set; }
        public Lake Lake { get; private set; }
        public SmallIsland SmallIsland { get; private set; }
        public LandTerritory LandTerritory { get; private set; }
        public LandRegion LandRegion { get; private set; }
        public LandProvince LandProvince { get; private set; }
        public SeaTerritory SeaTerritory { get; private set; }
        public SeaRegion SeaRegion { get; private set; }

        /* -------- Public Methods -------- */
        public bool EquivalentTo(GameMouseOver testGameMouseOver)
        {
            if (!Equals(Statue, testGameMouseOver.Statue))
                return false;

            if (TradeRoutes == null && testGameMouseOver.TradeRoutes != null || TradeRoutes != null && testGameMouseOver.TradeRoutes == null)
                return false;

            if (TradeRoutes != null && testGameMouseOver.TradeRoutes != null)
            {
                if (TradeRoutes.Count != testGameMouseOver.TradeRoutes.Count)
                    return false;

                for (int i = 0; i < TradeRoutes.Count; i++)
                    if (!TradeRoutes[i].Equals(testGameMouseOver.TradeRoutes[i]))
                        return false;
            }
            
            if (!Equals(River, testGameMouseOver.River))
                return false;

            if (!Equals(Lake, testGameMouseOver.Lake))
                return false;

            if (!Equals(SmallIsland, testGameMouseOver.SmallIsland))
                return false;

            if (!Equals(LandTerritory, testGameMouseOver.LandTerritory))
                return false;

            if (!Equals(LandRegion, testGameMouseOver.LandRegion))
                return false;

            if (!Equals(LandProvince, testGameMouseOver.LandProvince))
                return false;

            if (!Equals(SeaTerritory, testGameMouseOver.SeaTerritory))
                return false;

            if (!Equals(SeaRegion, testGameMouseOver.SeaRegion))
                return false;

            return true;
        }

        /* -------- Static Methods -------- */
        public static GameMouseOver CreateStatueMouseOver(Statue inStatue)
        {
            GameMouseOver mouseOver = new GameMouseOver();

            mouseOver.Statue = inStatue;

            return mouseOver;
        }
        public static GameMouseOver CreateTradeRouteMouseOver(List<TradeRoute> inTradeRoutes)
        {
            GameMouseOver mouseOver = new GameMouseOver();

            mouseOver.TradeRoutes = inTradeRoutes;

            return mouseOver;
        }
        public static GameMouseOver CreateRiverMouseOver(River inRiver)
        {
            GameMouseOver mouseOver = new GameMouseOver();

            mouseOver.River = inRiver;

            return mouseOver;
        }
        public static GameMouseOver CreateLakeMouseOver(Lake inLake)
        {
            GameMouseOver mouseOver = new GameMouseOver();

            mouseOver.Lake = inLake;

            return mouseOver;
        }
        public static GameMouseOver CreateSmallIslandMouseOver(SmallIsland inSmallIsland)
        {
            GameMouseOver mouseOver = new GameMouseOver();

            mouseOver.SmallIsland = inSmallIsland;

            return mouseOver;
        }
        public static GameMouseOver CreateLandTerritoryMouseOver(LandTerritory inLandTerritory)
        {
            GameMouseOver mouseOver = new GameMouseOver();

            mouseOver.LandTerritory = inLandTerritory;

            return mouseOver;
        }
        public static GameMouseOver CreateLandRegionMouseOver(LandRegion inLandRegion)
        {
            GameMouseOver mouseOver = new GameMouseOver();

            mouseOver.LandRegion = inLandRegion;

            return mouseOver;
        }
        public static GameMouseOver CreateLandProvinceMouseOver(LandProvince inLandProvince)
        {
            GameMouseOver mouseOver = new GameMouseOver();

            mouseOver.LandProvince = inLandProvince;

            return mouseOver;
        }
        public static GameMouseOver CreateSeaTerritoryMouseOver(SeaTerritory inSeaTerritory)
        {
            GameMouseOver mouseOver = new GameMouseOver();

            mouseOver.SeaTerritory = inSeaTerritory;

            return mouseOver;
        }
        public static GameMouseOver CreateSeaRegionMouseOver(SeaRegion inSeaRegion)
        {
            GameMouseOver mouseOver = new GameMouseOver();

            mouseOver.SeaRegion = inSeaRegion;

            return mouseOver;
        }
    }
}
