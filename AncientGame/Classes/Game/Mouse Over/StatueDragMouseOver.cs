﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class StatueDragMouseOver
    {
        /* -------- Properties -------- */
        public LandTerritory LandTerritory { get; private set; }
        public SeaTerritory SeaTerritory { get; private set; }
        public Settlement Settlement { get; private set; }
        public Statue Statue { get; private set; }
        public Vector2 MouseMSPosition { get; private set; }

        /* -------- Public Methods -------- */
        public bool EquivalentTo(StatueDragMouseOver testStatueDragMouseOver)
        {
            if (!Equals(Statue, testStatueDragMouseOver.Statue))
                return false;

            if (!Equals(Settlement, testStatueDragMouseOver.Settlement))
                return false;

            if (!Equals(LandTerritory, testStatueDragMouseOver.LandTerritory))
                return false;

            if (!Equals(SeaTerritory, testStatueDragMouseOver.SeaTerritory))
                return false;

            return true;
        }

        /* -------- Static Methods -------- */
        public static StatueDragMouseOver CreateStatueMouseOver(Statue inStatue)
        {
            StatueDragMouseOver mouseOver = new StatueDragMouseOver();

            mouseOver.Statue = inStatue;

            return mouseOver;
        }
        public static StatueDragMouseOver CreateLandTerritoryMouseOver(LandTerritory inLandTerritory, Vector2 inMouseMSPosition)
        {
            StatueDragMouseOver mouseOver = new StatueDragMouseOver();

            mouseOver.LandTerritory = inLandTerritory;
            mouseOver.MouseMSPosition = inMouseMSPosition;

            return mouseOver;
        }
        public static StatueDragMouseOver CreateSeaTerritoryMouseOver(SeaTerritory inSeaTerritory, Vector2 inMouseMSPosition)
        {
            StatueDragMouseOver mouseOver = new StatueDragMouseOver();

            mouseOver.SeaTerritory = inSeaTerritory;
            mouseOver.MouseMSPosition = inMouseMSPosition;

            return mouseOver;
        }
        public static StatueDragMouseOver CreateSettlementMouseOver(Settlement inSettlement)
        {
            StatueDragMouseOver mouseOver = new StatueDragMouseOver();

            mouseOver.Settlement = inSettlement;

            return mouseOver;
        }
    }
}
