﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class GameMouseOverManager
    {
        /* -------- Private Fields -------- */
        private GameMouseOver currGameMouseOver;
        private GameMouseOver prevGameMouseOver;

        /* -------- Public Methods -------- */
        public void Update(GraphicsComponent g, InputComponent i, UIComponent ui, GameManagerContainer gameManagers, CameraManager cm, ViewManager vm, ContentManager content)
        {
            prevGameMouseOver = currGameMouseOver;

            if (ui.HasDetectedMouseOverUI() || ui.GetMouseOverDetectionFocus() != eUIFocus.NO_FOCUS || gameManagers.StatueDragManager.DraggingStatue != null)
            {
                currGameMouseOver = null;
                if (ui.Tooltip.IsActive && ui.Tooltip.Source == eCursorChangeSource.GAME_ENTITIES)
                    ui.Tooltip.ResetTooltipInfo();
                return;
            }

            FindGameMouseOver(g, i, gameManagers, cm, vm);
            OnGameMouseOverChosen(ui, content);

            if (currGameMouseOver == null)
                return;

            if (i.MouseButtonIsPressing(eMouseButton.LEFT_BUTTON))
                OnPressLeftMouseButton(gameManagers);
            if (i.MouseButtonIsPressing(eMouseButton.RIGHT_BUTTON))
                OnPressRightMouseButton(gameManagers);
        }

        /* -------- Private Methods --------- */
        private void FindGameMouseOver(GraphicsComponent g, InputComponent i, GameManagerContainer gameManagers, CameraManager cm, ViewManager vm)
        {
            currGameMouseOver = null;

            Point mousePSPos = i.GetMousePosition();

            // Statues
            if (vm.ViewMode.StatuesAreSelectable())
            {
                Statue mouseOverStatue = gameManagers.FactionManager.GetPSMouseOverStatue(mousePSPos, vm);
                if (mouseOverStatue != null)
                {
                    currGameMouseOver = GameMouseOver.CreateStatueMouseOver(mouseOverStatue);
                    return;
                }
            }

            Vector2 mouseMSPos;
            if (!SpaceUtilities.TryConvertPSToMS(mousePSPos, g, cm, out mouseMSPos))
                return;

            // Trade routes
            List<TradeRoute> mouseOverTradeRoutes = new List<TradeRoute>();
            if (vm.ViewMode.LandTradeRoutesAreSelectable())
                mouseOverTradeRoutes.AddRange(gameManagers.MapManager.LandTradeRouteManager.GetMouseOverLandTradeRoutes(mouseMSPos));
            if (vm.ViewMode.SeaTradeRoutesAreSelectable())
                mouseOverTradeRoutes.AddRange(gameManagers.MapManager.SeaTradeRouteManager.GetMouseOverSeaTradeRoutes(mouseMSPos));
            if (mouseOverTradeRoutes.Count > 0)
            {
                currGameMouseOver = GameMouseOver.CreateTradeRouteMouseOver(mouseOverTradeRoutes);
                return;
            }

            // Rivers
            if (vm.ViewMode.RiversAreSelectable())
            {
                River mouseOverRiver = gameManagers.MapManager.RiverManager.GetMouseOverRiver(mouseMSPos);
                if (mouseOverRiver != null)
                {
                    currGameMouseOver = GameMouseOver.CreateRiverMouseOver(mouseOverRiver);
                    return;
                }
            }

            // Lakes
            if (vm.ViewMode.LakesAreSelectable())
            {
                Lake mouseOverLake = gameManagers.MapManager.LakeManager.GetMouseOverLake(mouseMSPos);
                if (mouseOverLake != null)
                {
                    currGameMouseOver = GameMouseOver.CreateLakeMouseOver(mouseOverLake);
                    return;
                }
            }

            // Small islands
            if (vm.ViewMode.SmallIslandsAreSelectable())
            {
                SmallIsland mouseOverSmallIsland = gameManagers.MapManager.SmallIslandManager.GetMouseOverSmallIsland(mouseMSPos);
                if (mouseOverSmallIsland != null)
                {
                    currGameMouseOver = GameMouseOver.CreateSmallIslandMouseOver(mouseOverSmallIsland);
                    return;
                }
            }

            // Land territories
            if (vm.ViewMode.LandTerritoriesAreSelectable())
            {
                LandTerritory mouseOverLandTerritory = gameManagers.MapManager.LandTerritoryManager.GetMouseOverLandTerritory(mouseMSPos);
                if (mouseOverLandTerritory != null)
                {
                    currGameMouseOver = GameMouseOver.CreateLandTerritoryMouseOver(mouseOverLandTerritory);
                    return;
                }
            }

            // Land regions
            if (vm.ViewMode.LandRegionsAreSelectable())
            {
                LandRegion mouseOverLandRegion = gameManagers.MapManager.LandRegionManager.GetMouseOverLandRegion(mouseMSPos);
                if (mouseOverLandRegion != null)
                {
                    currGameMouseOver = GameMouseOver.CreateLandRegionMouseOver(mouseOverLandRegion);
                    return;
                }
            }

            // Land provinces
            if (vm.ViewMode.LandProvincesAreSelectable())
            {
                LandProvince mouseOverLandProvince = gameManagers.MapManager.LandProvinceManager.GetMouseOverLandProvince(mouseMSPos);
                if (mouseOverLandProvince != null)
                {
                    currGameMouseOver = GameMouseOver.CreateLandProvinceMouseOver(mouseOverLandProvince);
                    return;
                }
            }

            // Sea territories
            if (vm.ViewMode.SeaTerritoriesAreSelectable())
            {
                SeaTerritory mouseOverSeaTerritory = gameManagers.MapManager.SeaTerritoryManager.GetMouseOverSeaTerritory(mouseMSPos);
                if (mouseOverSeaTerritory != null)
                {
                    currGameMouseOver = GameMouseOver.CreateSeaTerritoryMouseOver(mouseOverSeaTerritory);
                    return;
                }
            }

            // Sea regions
            if (vm.ViewMode.SeaRegionsAreSelectable())
            {
                SeaRegion mouseOverSeaRegion = gameManagers.MapManager.SeaRegionManager.GetMouseOverSeaRegion(mouseMSPos);
                if (mouseOverSeaRegion != null)
                {
                    currGameMouseOver = GameMouseOver.CreateSeaRegionMouseOver(mouseOverSeaRegion);
                    return;
                }
            }
        }
        private void OnGameMouseOverChosen(UIComponent ui, ContentManager content)
        {
            if (currGameMouseOver == null)
            {
                if (prevGameMouseOver == null)
                    return;
                
                ui.Tooltip.ResetTooltipInfo();
                return;
            }

            if (prevGameMouseOver == null || !prevGameMouseOver.EquivalentTo(currGameMouseOver))
            {
                TooltipInfo info = GetMouseOverTooltipInfo();
                if (info != null)
                    ui.SetTooltipInfo(info, eCursorChangeSource.GAME_ENTITIES);
            }
        }
        private TooltipInfo GetMouseOverTooltipInfo()
        {
            if (currGameMouseOver.Statue != null)
                return TooltipInfo.CreateStatueTooltipInfo(currGameMouseOver.Statue);

            if (currGameMouseOver.TradeRoutes != null)
                return TooltipInfo.CreateTradeRoutesTooltipInfo(currGameMouseOver.TradeRoutes);

            if (currGameMouseOver.River != null)
                return TooltipInfo.CreateStaticTextTooltipInfo(currGameMouseOver.River.GetNameString());

            if (currGameMouseOver.Lake != null)
                return TooltipInfo.CreateStaticTextTooltipInfo(currGameMouseOver.Lake.GetNameString());

            if (currGameMouseOver.SmallIsland != null)
                return TooltipInfo.CreateStaticTextTooltipInfo(currGameMouseOver.SmallIsland.GetNameString());

            if (currGameMouseOver.LandTerritory != null)
                return TooltipInfo.CreateLandTerritoryTooltipInfo(currGameMouseOver.LandTerritory);

            if (currGameMouseOver.LandRegion != null)
                return TooltipInfo.CreateStaticTextTooltipInfo(currGameMouseOver.LandRegion.GetNameString());

            if (currGameMouseOver.LandProvince != null)
                return TooltipInfo.CreateStaticTextTooltipInfo(currGameMouseOver.LandProvince.GetNameString());

            if (currGameMouseOver.SeaTerritory != null)
            {
                if (currGameMouseOver.SeaTerritory is CoastalSeaTerritory)
                    return TooltipInfo.CreateStaticTextTooltipInfo("Coastal sea territory"); // TODO
                else
                    return TooltipInfo.CreateStaticTextTooltipInfo("Sea territory"); // TODO
            }
                
            if (currGameMouseOver.SeaRegion != null)
                return TooltipInfo.CreateStaticTextTooltipInfo(currGameMouseOver.SeaRegion.GetNameString());

            return null;
        }

        private void OnPressLeftMouseButton(GameManagerContainer gameManagers)
        {
            if (currGameMouseOver.Statue != null && currGameMouseOver.Statue.CanBeDragged(gameManagers.TurnManager))
            {
                gameManagers.StatueDragManager.OnStatuePressed(currGameMouseOver.Statue, gameManagers.BattleManager);
                return;
            }
        }
        private void OnPressRightMouseButton(GameManagerContainer gameManagers)
        {
            if (currGameMouseOver.Statue != null)
            {
                Force force = currGameMouseOver.Statue as Force;
                if (force != null)
                    gameManagers.WindowManager.CreateWindow_Force(force);
            }
            //if (currGameMouseOver.TradeRoutes != null)
                // TODO
            if (currGameMouseOver.River != null)
                gameManagers.WindowManager.CreateWindow_River(currGameMouseOver.River);
            if (currGameMouseOver.Lake != null)
                gameManagers.WindowManager.CreateWindow_Lake(currGameMouseOver.Lake);
            if (currGameMouseOver.SmallIsland != null)
                gameManagers.WindowManager.CreateWindow_SmallIsland(currGameMouseOver.SmallIsland);
            if (currGameMouseOver.LandTerritory != null)
                gameManagers.WindowManager.CreateWindow_LandTerritory(currGameMouseOver.LandTerritory);
            if (currGameMouseOver.LandRegion != null)
                gameManagers.WindowManager.CreateWindow_LandRegion(currGameMouseOver.LandRegion);
            if (currGameMouseOver.LandProvince != null)
                gameManagers.WindowManager.CreateWindow_LandProvince(currGameMouseOver.LandProvince);
            if (currGameMouseOver.SeaTerritory != null)
            {
                CoastalSeaTerritory coastalSeaTerritory = currGameMouseOver.SeaTerritory as CoastalSeaTerritory;
                if (coastalSeaTerritory != null)
                    gameManagers.WindowManager.CreateWindow_CoastalSeaTerritory(coastalSeaTerritory);
                else
                    gameManagers.WindowManager.CreateWindow_SeaTerritory(currGameMouseOver.SeaTerritory);
            }
            if (currGameMouseOver.SeaRegion != null)
                gameManagers.WindowManager.CreateWindow_SeaRegion(currGameMouseOver.SeaRegion);
        }
    }
}
