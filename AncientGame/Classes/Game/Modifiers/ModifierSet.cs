﻿using System.Collections.Generic;
using DataTypes;

namespace AncientGame
{
    class ModifierSet
    {
        /* -------- Properties -------- */
        public List<Modifier> Modifiers { get; private set; }

        /* -------- Constructors -------- */
        public ModifierSet()
        {
            Modifiers = new List<Modifier>();
        }

        /* -------- Public Methods -------- */
        public void Merge(ModifierSet set)
        {
            Modifiers.AddRange(set.Modifiers);
        }

        public void TestIsSatisfied(ModificationObjectSet objectSet)
        {
            foreach (Modifier m in Modifiers)
                m.Condition.TestIsSatisfied(objectSet);
        }

        // Modify methods
        public float Modify(float value, ModifierTypeSet typeSet)
        {
            List<Modifier> additionModifiers = Modifiers.FindAll(x => (x.ModificationType == eModificationType.ADDITION && x.TypeSet.EquivalentTo(typeSet)));
            List<Modifier> multiplicationModifiers = Modifiers.FindAll(x => (x.ModificationType == eModificationType.MULTIPLICATION && x.TypeSet.EquivalentTo(typeSet)));

            foreach (Modifier m in additionModifiers)
                value += m.Value;

            foreach (Modifier m in multiplicationModifiers)
                value *= m.Value;

            return value;
        }
        public float Modify(float value, eModifierType modifierType)
        {
            ModifierTypeSet modifierTypeSet = ModifierTypeSet.Create(modifierType);
            return Modify(value, modifierTypeSet);
        }
        public float Modify_ResourceQuantity(float value, Resource resource)
        {
            ModifierTypeSet resourceTypeSet = ModifierTypeSet.Create_ResourceProduction(resource);
            ModifierTypeSet resourceTypeTypeSet = ModifierTypeSet.Create_ResourceTypeProduction(resource.Type);

            value = Modify(value, resourceTypeSet);
            value = Modify(value, resourceTypeTypeSet);

            return value;
        }
        public float Modify_ResourceProductionWealthSourceTaxation(float value, eResourceType resourceType)
        {
            eWealthSourceTaxationType wealthSourceTaxationType = WealthSource.GetResourceWealthSourceTaxationTypeForResourceType_Production(resourceType);
            ModifierTypeSet modifierType = ModifierTypeSet.Create_WealthSourceTaxation(wealthSourceTaxationType);
            ModifierTypeSet blanketModifierType = ModifierTypeSet.Create_WealthSourceTaxation(eWealthSourceTaxationType.RESOURCE_ALL_PRODUCTION);

            value = Modify(value, modifierType);
            value = Modify(value, blanketModifierType);

            return value;
        }
        public float Modify_ResourceImportWealthSourceTaxation(float value, eResourceType resourceType)
        {
            eWealthSourceTaxationType wealthSourceTaxationType = WealthSource.GetResourceWealthSourceTaxationTypeForResourceType_Import(resourceType);
            ModifierTypeSet modifierType = ModifierTypeSet.Create_WealthSourceTaxation(wealthSourceTaxationType);
            ModifierTypeSet blanketModifierType = ModifierTypeSet.Create_WealthSourceTaxation(eWealthSourceTaxationType.RESOURCE_ALL_IMPORT);

            value = Modify(value, modifierType);
            value = Modify(value, blanketModifierType);

            return value;
        }
        public float Modify_ResourceThroughportWealthSourceTaxation(float value, eResourceType resourceType)
        {
            eWealthSourceTaxationType wealthSourceTaxationType = WealthSource.GetResourceWealthSourceTaxationTypeForResourceType_Throughport(resourceType);
            ModifierTypeSet modifierType = ModifierTypeSet.Create_WealthSourceTaxation(wealthSourceTaxationType);
            ModifierTypeSet blanketModifierType = ModifierTypeSet.Create_WealthSourceTaxation(eWealthSourceTaxationType.RESOURCE_ALL_THROUGHPORT);

            value = Modify(value, modifierType);
            value = Modify(value, blanketModifierType);

            return value;
        }
        public float Modify_ResourceImportTradeEfficiency(float value, eResourceType resourceType)
        {
            eResourceTradeType resourceTradeType = TradePath.GetResourceTradeTypeForResourceType_Import(resourceType);
            ModifierTypeSet modifierType = ModifierTypeSet.Create_TradeEfficiency(resourceTradeType);
            ModifierTypeSet blanketModifierType = ModifierTypeSet.Create_TradeEfficiency(eResourceTradeType.ALL_IMPORTS);

            value = Modify(value, modifierType);
            value = Modify(value, blanketModifierType);

            return value;
        }
        public float Modify_ResourceThroughportTradeEfficiency(float value, eResourceType resourceType)
        {
            eResourceTradeType resourceTradeType = TradePath.GetResourceTradeTypeForResourceType_Throughport(resourceType);
            ModifierTypeSet modifierType = ModifierTypeSet.Create_TradeEfficiency(resourceTradeType);
            ModifierTypeSet blanketModifierType = ModifierTypeSet.Create_TradeEfficiency(eResourceTradeType.ALL_THROUGHPORTS);

            value = Modify(value, modifierType);
            value = Modify(value, blanketModifierType);

            return value;
        }

        /* -------- Static Methods -------- */
        public static ModifierSet CreateFromData(ModifierData[] datas, GameManagerContainer gameManagers)
        {
            ModifierSet set = new ModifierSet();

            foreach (ModifierData md in datas)
                set.Modifiers.Add(Modifier.CreateFromData(md, gameManagers));

            return set;
        }
    }
}
