﻿using System;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eModifierType
    {
        // Modifier types with further properties
        STATS,
        RESOURCE_PRODUCTION,
        RESOURCE_TYPE_PRODUCTION,
        WEALTH_SOURCE,
        WEALTH_SOURCE_TAXATION,
        INCOME_EFFICIENCY,
        TRADE_EFFICIENCY,
        // Modifier types with no properties
        LAND_TERRITORY_MOVEMENT_POINT_COST,
        SEA_TERRITORY_MOVEMENT_POINT_COST,
        TRADER_MOVEMENT_POINTS,
        ADJACENT_LAND_TERRITORY_MOVEMENT_POINT_COST,
        ADJACENT_SEA_TERRITORY_MOVEMENT_POINT_COST,
        MAX_MANPOWER,
        MAX_FOOD_STORES,
        SETTLEMENT_BUILDING_SLOT_NUMBER,
        TERRITORY_BUILDING_SLOT_NUMBER,
        SUPPORTABLE_POPULATION,
        BUILDING_MONEY_COST,
        BUILDING_ADMIN_POWER_COST,
        BUILDING_MANPOWER_COST,
        BUILDING_CONSTRUCTION_TIME
    }

    class ModifierTypeSet
    {
        /* -------- Properties -------- */
        public eModifierType Type { get; private set; }
        // STATS
        public eStatType StatType { get; private set; }
        public eStatConstituentType StatConstituentType { get; private set; }
        // RESOURCE_PRODUCTION
        public Resource Resource { get; private set; }
        // RESOURCE_TYPE_PRODUCTION
        public eResourceType ResourceType { get; private set; }
        // WEALTH_SOURCE
        public eWealthSourceType WealthSourceType { get; private set; }
        // WEALTH_SOURCE_TAXATION
        public eWealthSourceTaxationType WealthSourceTaxationType { get; private set; }
        // INCOME_EFFICIENCY
        public eIncomeEfficiencySource IncomeEfficiencySource { get; private set; }
        // TRADE_EFFICIENCY
        public eResourceTradeType ResourceTradeType { get; private set; }

        /* -------- Public Methods -------- */
        public bool EquivalentTo(ModifierTypeSet typeSet)
        {
            if (Type != typeSet.Type)
                return false;
            if (StatType != typeSet.StatType)
                return false;
            if (StatConstituentType != typeSet.StatConstituentType)
                return false;
            if (Resource != typeSet.Resource)
                return false;
            if (ResourceType != typeSet.ResourceType)
                return false;
            if (WealthSourceType != typeSet.WealthSourceType)
                return false;
            if (WealthSourceTaxationType != typeSet.WealthSourceTaxationType)
                return false;
            if (IncomeEfficiencySource != typeSet.IncomeEfficiencySource)
                return false;
            if (ResourceTradeType != typeSet.ResourceTradeType)
                return false;

            return true;
        }

        public eUIAttitude GetUIAttitude()
        {
            switch (Type)
            {
                case eModifierType.STATS:
                    return StatConstituent.GetUIAttitude(StatConstituentType);
                case eModifierType.RESOURCE_PRODUCTION:
                    return eUIAttitude.POSITIVE;
                case eModifierType.RESOURCE_TYPE_PRODUCTION:
                    return eUIAttitude.POSITIVE;
                case eModifierType.WEALTH_SOURCE:
                    return eUIAttitude.POSITIVE;
                case eModifierType.WEALTH_SOURCE_TAXATION:
                    return eUIAttitude.POSITIVE;
                case eModifierType.INCOME_EFFICIENCY:
                    return IncomeEfficiency.GetUIAttitude(IncomeEfficiencySource);
                case eModifierType.TRADE_EFFICIENCY:
                    return eUIAttitude.POSITIVE;
                case eModifierType.LAND_TERRITORY_MOVEMENT_POINT_COST:
                    return eUIAttitude.NEGATIVE;
                case eModifierType.SEA_TERRITORY_MOVEMENT_POINT_COST:
                    return eUIAttitude.NEGATIVE;
                case eModifierType.TRADER_MOVEMENT_POINTS:
                    return eUIAttitude.POSITIVE;
                case eModifierType.ADJACENT_LAND_TERRITORY_MOVEMENT_POINT_COST:
                    return eUIAttitude.NEGATIVE;
                case eModifierType.ADJACENT_SEA_TERRITORY_MOVEMENT_POINT_COST:
                    return eUIAttitude.NEGATIVE;
                case eModifierType.MAX_MANPOWER:
                    return eUIAttitude.POSITIVE;
                case eModifierType.MAX_FOOD_STORES:
                    return eUIAttitude.POSITIVE;
                case eModifierType.SETTLEMENT_BUILDING_SLOT_NUMBER:
                    return eUIAttitude.POSITIVE;
                case eModifierType.TERRITORY_BUILDING_SLOT_NUMBER:
                    return eUIAttitude.POSITIVE;
                case eModifierType.SUPPORTABLE_POPULATION:
                    return eUIAttitude.POSITIVE;
                case eModifierType.BUILDING_MONEY_COST:
                    return eUIAttitude.NEGATIVE;
                case eModifierType.BUILDING_ADMIN_POWER_COST:
                    return eUIAttitude.NEGATIVE;
                case eModifierType.BUILDING_MANPOWER_COST:
                    return eUIAttitude.NEGATIVE;
                case eModifierType.BUILDING_CONSTRUCTION_TIME:
                    return eUIAttitude.NEGATIVE;
                default:
                    Debug.LogUnrecognizedValueWarning(Type);
                    return eUIAttitude.NEUTRAL;
            }
        }

        public string GetDescription(eUIAttitude attitude)
        {
            switch (Type)
            {
                case eModifierType.STATS:
                    return GetDescription_Stats(attitude);
                case eModifierType.RESOURCE_PRODUCTION:
                    return GetDescription_ResourceProduction();
                case eModifierType.RESOURCE_TYPE_PRODUCTION:
                    return GetDescription_ResourceTypeProduction();
                case eModifierType.WEALTH_SOURCE:
                    return GetDescription_WealthSource();
                case eModifierType.WEALTH_SOURCE_TAXATION:
                    return GetDescription_WealthSourceTaxation();
                case eModifierType.INCOME_EFFICIENCY:
                    return GetDescription_IncomeEfficiency();
                case eModifierType.TRADE_EFFICIENCY:
                    return GetDescription_TradeEfficiency();
                case eModifierType.LAND_TERRITORY_MOVEMENT_POINT_COST:
                    return Localization.Localize("text_land_territory_movement_point_cost");
                case eModifierType.SEA_TERRITORY_MOVEMENT_POINT_COST:
                    return Localization.Localize("text_sea_territory_movement_point_cost");
                case eModifierType.TRADER_MOVEMENT_POINTS:
                    return Localization.Localize("text_merchant_movement_points");
                case eModifierType.ADJACENT_LAND_TERRITORY_MOVEMENT_POINT_COST:
                    return Localization.Localize("text_adjacent_land_territory_movement_point_cost");
                case eModifierType.ADJACENT_SEA_TERRITORY_MOVEMENT_POINT_COST:
                    return Localization.Localize("text_adjacent_sea_territory_movement_point_cost");
                case eModifierType.MAX_MANPOWER:
                    return Localization.Localize("text_max_manpower");
                case eModifierType.MAX_FOOD_STORES:
                    return Localization.Localize("text_max_food_stores");
                case eModifierType.SETTLEMENT_BUILDING_SLOT_NUMBER:
                    return Localization.Localize("text_settlement_building_slot_modifier");
                case eModifierType.TERRITORY_BUILDING_SLOT_NUMBER:
                    return Localization.Localize("text_territory_building_slot_modifier");
                case eModifierType.SUPPORTABLE_POPULATION:
                    return Localization.Localize("text_supportable_population_modifier");
                case eModifierType.BUILDING_MONEY_COST:
                    return Localization.Localize("text_building_money_cost_modifier");
                case eModifierType.BUILDING_ADMIN_POWER_COST:
                    return Localization.Localize("text_building_admin_power_cost_modifier");
                case eModifierType.BUILDING_MANPOWER_COST:
                    return Localization.Localize("text_building_manpower_cost_modifier");
                case eModifierType.BUILDING_CONSTRUCTION_TIME:
                    return Localization.Localize("text_building_construction_time_cost_modifier");
                default:
                    Debug.LogUnrecognizedValueWarning(Type);
                    return "";
            }
        }

        /* -------- Private Methods --------- */
        private string GetDescription_Stats(eUIAttitude attitude)
        {
            string description = Localization.Localize("text_stat_modifier_due_to");

            string statName;
            eStatConstituentBias statConstituentBias = StatConstituent.GetBias(StatConstituentType);
            switch (StatType)
            {
                case eStatType.PLEB_HAPPINESS:
                    if (statConstituentBias == eStatConstituentBias.NEGATIVE)
                        statName = Localization.Localize("text_stat_modifier_pleb_happiness_neg");
                    else
                        statName = Localization.Localize("text_stat_modifier_pleb_happiness_pos");
                    break;
                case eStatType.NOBILITY_HAPPINESS:
                    if (statConstituentBias == eStatConstituentBias.NEGATIVE)
                        statName = Localization.Localize("text_stat_modifier_nobility_happiness_neg");
                    else
                        statName = Localization.Localize("text_stat_modifier_nobility_happiness_pos");
                    break;
                case eStatType.FOOD:
                    if (statConstituentBias == eStatConstituentBias.NEGATIVE)
                        statName = Localization.Localize("text_stat_modifier_food_neg");
                    else
                        statName = Localization.Localize("text_stat_modifier_food_pos");
                    break;
                case eStatType.HEALTH:
                    if (statConstituentBias == eStatConstituentBias.NEGATIVE)
                        statName = Localization.Localize("text_stat_modifier_health_neg");
                    else
                        statName = Localization.Localize("text_stat_modifier_health_pos");
                    break;
                case eStatType.MANPOWER_CHANGE:
                    if (statConstituentBias == eStatConstituentBias.NEGATIVE)
                        statName = Localization.Localize("text_stat_modifier_manpower_neg");
                    else
                        statName = Localization.Localize("text_stat_modifier_manpower_pos");
                    break;
                case eStatType.FOOD_STORES_CHANGE:
                    if (statConstituentBias == eStatConstituentBias.NEGATIVE)
                        statName = Localization.Localize("text_stat_modifier_food_stores_neg");
                    else
                        statName = Localization.Localize("text_stat_modifier_food_stores_pos");
                    break;
                case eStatType.ORDER:
                    if (statConstituentBias == eStatConstituentBias.NEGATIVE)
                        statName = Localization.Localize("text_stat_modifier_order_neg");
                    else
                        statName = Localization.Localize("text_stat_modifier_order_pos");
                    break;
                case eStatType.POPULATION_GROWTH:
                    if (statConstituentBias == eStatConstituentBias.NEGATIVE)
                        statName = Localization.Localize("text_stat_modifier_population_neg");
                    else
                        statName = Localization.Localize("text_stat_modifier_population_pos");
                    break;
                case eStatType.PRIVATE_WEALTH_CHANGE:
                    if (statConstituentBias == eStatConstituentBias.NEGATIVE)
                        statName = Localization.Localize("text_stat_modifier_private_wealth_neg");
                    else
                        statName = Localization.Localize("text_stat_modifier_private_wealth_pos");
                    break;
                case eStatType.WARMTH:
                    if (statConstituentBias == eStatConstituentBias.NEGATIVE)
                        statName = Localization.Localize("text_stat_modifier_warmth_neg");
                    else
                        statName = Localization.Localize("text_stat_modifier_warmth_pos");
                    break;
                case eStatType.WATER:
                    if (statConstituentBias == eStatConstituentBias.NEGATIVE)
                        statName = Localization.Localize("text_stat_modifier_water_neg");
                    else
                        statName = Localization.Localize("text_stat_modifier_water_pos");
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(StatType);
                    statName = "";
                    break;
            }

            string statConstituentName = StatConstituent.GetName(StatConstituentType);

            description = String.Format(description, statName, statConstituentName);
            return description;
        }
        private string GetDescription_ResourceProduction()
        {
            string description = Localization.Localize("text_resource_production_modifier");
            description = String.Format(description, Resource.GetName());

            return description;
        }
        private string GetDescription_ResourceTypeProduction()
        {
            string description = Localization.Localize("text_resource_production_modifier");
            description = String.Format(description, Resource.GetTypeName(ResourceType));

            return description;
        }
        private string GetDescription_WealthSource()
        {
            string description = Localization.Localize("text_wealth_from");
            description = String.Format(description, WealthSource.GetWealthSourceName(WealthSourceType));

            return description;
        }
        private string GetDescription_WealthSourceTaxation()
        {
            string description = Localization.Localize("text_taxation_on");
            description = String.Format(description, WealthSource.GetWealthSourceTaxationName(WealthSourceTaxationType));

            return description;
        }
        private string GetDescription_IncomeEfficiency()
        {
            string description;
            if (IncomeEfficiency.GetUIAttitude(IncomeEfficiencySource) == eUIAttitude.NEGATIVE)
                description = Localization.Localize("text_income_inefficiency_modifier_due_to");
            else
                description = Localization.Localize("text_income_efficiency_modifier_due_to");

            description = String.Format(description, IncomeEfficiency.GetSourceName(IncomeEfficiencySource));

            return description;
        }
        private string GetDescription_TradeEfficiency()
        {
            string description = Localization.Localize("text_trade_efficiency_modifier");
            string resourceTradeName = TradePath.GetResourceTradeTypeName(ResourceTradeType);
            description = String.Format(description, resourceTradeName);
            return description;
        }

        /* -------- Static Methods -------- */
        public static ModifierTypeSet CreateFromData(string[] args, GameManagerContainer gameManagers)
        {
            ModifierTypeSet typeSet = new ModifierTypeSet();

            string typeString = (args.Length >= 1) ? args[0] : "";
            typeSet.Type = DataUtilities.ParseModifierType(typeString);

            switch (typeSet.Type)
            {
                case eModifierType.STATS:
                    SetDataFromArgs_Stats(typeSet, args);
                    break;
                case eModifierType.RESOURCE_PRODUCTION:
                    SetDataFromArgs_ResourceProduction(typeSet, args, gameManagers.ResourceManager);
                    break;
                case eModifierType.RESOURCE_TYPE_PRODUCTION:
                    SetDataFromArgs_ResourceTypeProduction(typeSet, args);
                    break;
                case eModifierType.WEALTH_SOURCE:
                    SetDataFromArgs_WealthSource(typeSet, args);
                    break;
                case eModifierType.WEALTH_SOURCE_TAXATION:
                    SetDataFromArgs_WealthSourceTaxation(typeSet, args);
                    break;
                case eModifierType.INCOME_EFFICIENCY:
                    SetDataFromArgs_IncomeEfficiency(typeSet, args);
                    break;
                case eModifierType.TRADE_EFFICIENCY:
                    SetDataFromArgs_TradeEfficiency(typeSet, args);
                    break;
                default:
                    break;
            }

            return typeSet;
        }
        private static void SetDataFromArgs_Stats(ModifierTypeSet typeSet, string[] args)
        {
            string statTypeString = (args.Length >= 2) ? args[1] : "";
            typeSet.StatType = DataUtilities.ParseStatType(statTypeString);

            string statConstituentString = (args.Length >= 3) ? args[2] : "";
            typeSet.StatConstituentType = DataUtilities.ParseStatConstituentType(statConstituentString);
        }
        private static void SetDataFromArgs_ResourceProduction(ModifierTypeSet typeSet, string[] args, ResourceManager rm)
        {
            string resourceString = (args.Length >= 2) ? args[1] : "";
            typeSet.Resource = rm.GetResource(resourceString);
        }
        private static void SetDataFromArgs_ResourceTypeProduction(ModifierTypeSet typeSet, string[] args)
        {
            string resourceTypeString = (args.Length >= 2) ? args[1] : "";
            typeSet.ResourceType = DataUtilities.ParseResourceType(resourceTypeString);
        }
        private static void SetDataFromArgs_WealthSource(ModifierTypeSet typeSet, string[] args)
        {
            string wealthSourceTypeString = (args.Length >= 2) ? args[1] : "";
            typeSet.WealthSourceType = DataUtilities.ParseWealthSourceType(wealthSourceTypeString);
        }
        private static void SetDataFromArgs_WealthSourceTaxation(ModifierTypeSet typeSet, string[] args)
        {
            string wealthSourceTypeString = (args.Length >= 2) ? args[1] : "";
            typeSet.WealthSourceTaxationType = DataUtilities.ParseWealthSourceTaxationType(wealthSourceTypeString);
        }
        private static void SetDataFromArgs_IncomeEfficiency(ModifierTypeSet typeSet, string[] args)
        {
            string incomeEfficiencySourceString = (args.Length >= 2) ? args[1] : "";
            typeSet.IncomeEfficiencySource = DataUtilities.ParseIncomeEfficiencySource(incomeEfficiencySourceString);
        }
        private static void SetDataFromArgs_TradeEfficiency(ModifierTypeSet typeSet, string[] args)
        {
            string resourceTradeTypeString = (args.Length >= 2) ? args[1] : "";
            typeSet.ResourceTradeType = DataUtilities.ParseResourceTradeType(resourceTradeTypeString);
        }

        public static ModifierTypeSet Create(eModifierType inType)
        {
            ModifierTypeSet typeSet = new ModifierTypeSet();

            typeSet.Type = inType;

            return typeSet;
        }
        public static ModifierTypeSet Create_Stats(eStatType inStatType, eStatConstituentType inStatConstituentType)
        {
            ModifierTypeSet typeSet = new ModifierTypeSet();

            typeSet.Type = eModifierType.STATS;
            typeSet.StatType = inStatType;
            typeSet.StatConstituentType = inStatConstituentType;

            return typeSet;
        }
        public static ModifierTypeSet Create_ResourceProduction(Resource resource)
        {
            ModifierTypeSet typeSet = new ModifierTypeSet();

            typeSet.Type = eModifierType.RESOURCE_PRODUCTION;
            typeSet.Resource = resource;

            return typeSet;
        }
        public static ModifierTypeSet Create_ResourceTypeProduction(eResourceType resourceType)
        {
            ModifierTypeSet typeSet = new ModifierTypeSet();

            typeSet.Type = eModifierType.RESOURCE_TYPE_PRODUCTION;
            typeSet.ResourceType = resourceType;

            return typeSet;
        }
        public static ModifierTypeSet Create_WealthSourceTaxation(eWealthSourceTaxationType wealthSourceTaxationType)
        {
            ModifierTypeSet typeSet = new ModifierTypeSet();

            typeSet.Type = eModifierType.WEALTH_SOURCE_TAXATION;
            typeSet.WealthSourceTaxationType = wealthSourceTaxationType;

            return typeSet;
        }
        public static ModifierTypeSet Create_IncomeEfficiency(eIncomeEfficiencySource source)
        {
            ModifierTypeSet typeSet = new ModifierTypeSet();

            typeSet.Type = eModifierType.INCOME_EFFICIENCY;
            typeSet.IncomeEfficiencySource = source;

            return typeSet;
        }
        public static ModifierTypeSet Create_TradeEfficiency(eResourceTradeType resourceType)
        {
            ModifierTypeSet typeSet = new ModifierTypeSet();

            typeSet.Type = eModifierType.TRADE_EFFICIENCY;
            typeSet.ResourceTradeType = resourceType;

            return typeSet;
        }
    }
}
