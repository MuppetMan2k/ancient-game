﻿namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eModifierConditionType
    {
        NONE
    }

    class ModifierCondition
    {
        /* -------- Properties -------- */
        public eModifierConditionType Type { get; private set; }
        public bool IsSatisfied { get; private set; }

        /* -------- Public Methods -------- */
        public void TestIsSatisfied(ModificationObjectSet objectSet)
        {
            switch (Type)
            {
                case eModifierConditionType.NONE:
                    IsSatisfied = true;
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(Type);
                    IsSatisfied = false;
                    break;
            }
        }

        public string GetText()
        {
            switch (Type)
            {
                case eModifierConditionType.NONE:
                    return "";
                default:
                    Debug.LogUnrecognizedValueWarning(Type);
                    return "";
            }
        }

        /* -------- Creation Methods -------- */
        public static ModifierCondition CreateFromData(string[] conditionArgs)
        {
            if (conditionArgs.Length == 0)
                return None;

            eModifierConditionType conditionType = DataUtilities.ParseModifierConditionType(conditionArgs[0]);

            switch (conditionType)
            {
                default:
                    break;
            }

            return CreateModifierCondition(conditionType);
        }

        public static ModifierCondition CreateModifierCondition(eModifierConditionType inType)
        {
            return new ModifierCondition()
            {
                Type = inType
            };
        }

        /* -------- Static Instances -------- */
        public static ModifierCondition None = CreateModifierCondition(eModifierConditionType.NONE);
    }
}
