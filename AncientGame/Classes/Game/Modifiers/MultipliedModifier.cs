﻿using DataTypes;

namespace AncientGame
{
    class MultipliedModifier : Modifier
    {
        /* -------- Constructors -------- */
        public MultipliedModifier(ModifierTypeSet inTypeSet, eModificationType inModificationType, ModifierCondition inCaveat, float inValue)
            : base(inTypeSet, inModificationType, inCaveat, inValue)
        {
        }

        /* -------- Static Methods -------- */
        public static MultipliedModifier CreateFromData(ModifierData data, GameManagerContainer gameManagers, float multiplier)
        {
            ModifierTypeSet typeSet = ModifierTypeSet.CreateFromData(data.args, gameManagers);
            eModificationType modificationType = DataUtilities.ParseModificationType(data.modificationType);
            ModifierCondition caveat = ModifierCondition.CreateFromData(data.conditionArgs);

            float value = 0f;
            switch (modificationType)
            {
                case eModificationType.ADDITION:
                    value = data.value * multiplier;
                    break;
                case eModificationType.MULTIPLICATION:
                    float valRelTo1 = data.value - 1f;
                    valRelTo1 *= multiplier;
                    value = Mathf.ClampLower(valRelTo1 + 1f, 0f);
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(modificationType);
                    break;
            }

            return new MultipliedModifier(typeSet, modificationType, caveat, value);
        }
    }
}
