﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eModificationType
    {
        ADDITION,
        MULTIPLICATION
    }

    class Modifier
    {
        /* -------- Properties -------- */
        public ModifierTypeSet TypeSet { get; protected set; }
        public ModifierCondition Condition { get; protected set; }
        public eModificationType ModificationType { get; protected set; }
        public float Value { get; protected set; }
        
        /* -------- Constructors -------- */
        public Modifier(ModifierTypeSet inTypeSet, eModificationType inModificationType, ModifierCondition inCondition, float inValue)
        {
            TypeSet = inTypeSet;
            ModificationType = inModificationType;
            Condition = inCondition;
            Value = inValue;
        }

        /* -------- Public Methods -------- */
        public eUIAttitude GetUIAttitude()
        {
            eUIAttitude typeAttitude = TypeSet.GetUIAttitude();

            if (typeAttitude == eUIAttitude.NEUTRAL)
                return eUIAttitude.NEUTRAL;

            if (ModificationType == eModificationType.ADDITION)
            {
                if (Value > 0f)
                    return typeAttitude;

                if (Value < 0f)
                    return UIUtilities.ReverseUIAttitude(typeAttitude);

                return eUIAttitude.NEUTRAL;
            }

            if (ModificationType == eModificationType.MULTIPLICATION)
            {
                if (Value > 1f)
                    return typeAttitude;

                if (Value < 1f)
                    return UIUtilities.ReverseUIAttitude(typeAttitude);

                return eUIAttitude.NEUTRAL;
            }

            return eUIAttitude.NEUTRAL;
        }

        public List<ColoredTextSegment> GetColoredTextSegments(UIStyle style, Color textColor)
        {
            List<ColoredTextSegment> textSegments = new List<ColoredTextSegment>();

            Color numColor = style.GetUIAttitudeColor(GetUIAttitude());

            textSegments.Add(new ColoredTextSegment(GetValueDisplay(), numColor));

            textSegments.Add(new ColoredTextSegment(TypeSet.GetDescription(GetUIAttitude()), textColor));

            string conditionText = Condition.GetText();
            if (!String.IsNullOrEmpty(conditionText))
                textSegments.Add(new ColoredTextSegment(String.Format(Localization.Localize("text_brackets"), conditionText), textColor));

            return textSegments;
        }

        /* -------- Private Methods --------- */
        private string GetValueDisplay()
        {
            switch (ModificationType)
            {
                case eModificationType.ADDITION:
                    {
                        switch (TypeSet.Type)
                        {
                            case eModifierType.STATS:
                                return UIUtilities.GetFloatDisplayString(Value);
                            case eModifierType.WEALTH_SOURCE_TAXATION:
                                return UIUtilities.GetBiDirectionalPercentageFloatDisplayString(Value);
                            case eModifierType.INCOME_EFFICIENCY:
                                return UIUtilities.GetFloatDisplayString(Value);
                            case eModifierType.SUPPORTABLE_POPULATION:
                                return UIUtilities.GetLargeIntDisplayString(Mathf.Round(Value));
                            case eModifierType.TRADE_EFFICIENCY:
                                return UIUtilities.GetBiDirectionalPercentageFloatDisplayString(Value);
                            case eModifierType.MAX_MANPOWER:
                                return UIUtilities.GetIntDisplayString(Mathf.Round(Value));
                            case eModifierType.MAX_FOOD_STORES:
                                return UIUtilities.GetIntDisplayString(Mathf.Round(Value));
                            default:
                                return UIUtilities.GetBiDirectionalFloatDisplayString(Value);
                        }
                    }
                case eModificationType.MULTIPLICATION:
                    return UIUtilities.GetBiDirectionalPercentageFloatDisplayString((Value - 1f) * 100f);
                default:
                    Debug.LogUnrecognizedValueWarning(ModificationType);
                    return "";
            }
        }

        /* -------- Static Methods -------- */
        public static Modifier CreateFromData(ModifierData data, GameManagerContainer gameManagers)
        {
            ModifierTypeSet typeSet = ModifierTypeSet.CreateFromData(data.args, gameManagers);
            ModifierCondition condition = ModifierCondition.CreateFromData(data.conditionArgs);
            eModificationType modificationType = DataUtilities.ParseModificationType(data.modificationType);
            float value = data.value;

            if (modificationType == eModificationType.MULTIPLICATION)
                value = Mathf.ClampLower(value, 0f);

            return new Modifier(typeSet, modificationType, condition, value);
        }
    }
}
