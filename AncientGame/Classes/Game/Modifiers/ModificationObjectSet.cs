﻿namespace AncientGame
{
    class ModificationObjectSet
    {
        /* -------- Properties -------- */
        public LandTerritory LandTerritory { get; private set; }
        public Character Character { get; private set; }
        public Statue Statue { get; private set; }
        public Faction Faction { get; private set; }

        /* -------- Constructors -------- */
        public ModificationObjectSet(LandTerritory inLandTerritory = null, Character inCharacter = null, Statue inStatue = null, Faction inFaction = null)
        {
            LandTerritory = inLandTerritory;
            Character = inCharacter;
            Statue = inStatue;
            Faction = inFaction;
        }
    }
}
