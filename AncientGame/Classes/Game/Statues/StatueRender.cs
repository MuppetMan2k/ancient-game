﻿namespace AncientGame
{
    class StatueRender
    {
        /* -------- Properties -------- */
        public Statue Statue { get; private set; }
        public StatueSettlementStandard Standard { get; private set; }
        public int RectangleBottomY { get; private set; }

        /* -------- Constructors -------- */
        public StatueRender(Statue inStatue)
        {
            Statue = inStatue;
            Standard = null;
            RectangleBottomY = Statue.Rectangle.Bottom;
        }
        public StatueRender(StatueSettlementStandard inStandard)
        {
            Statue = null;
            Standard = inStandard;
            RectangleBottomY = Standard.Rectangle.Bottom;
        }
    }
}
