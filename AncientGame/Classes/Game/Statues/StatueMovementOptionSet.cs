﻿using System.Collections.Generic;

namespace AncientGame
{
    class StatueMovementOptionSet
    {
        /* -------- Properties -------- */
        public List<StatueMovementOption> MovementOptions { get; private set; }

        /* -------- Constructors -------- */
        public StatueMovementOptionSet()
        {
            MovementOptions = new List<StatueMovementOption>();
        }

        /* -------- Public Methods -------- */
        public StatueMovementOption GetMovementOptionToLandTerritory(LandTerritory lt)
        {
            foreach (StatueMovementOption smo in MovementOptions)
                if (smo.LandTerritory != null && smo.LandTerritory == lt)
                    return smo;

            return null;
        }
        public StatueMovementOption GetMovementOptionToSeaTerritory(SeaTerritory st)
        {
            foreach (StatueMovementOption smo in MovementOptions)
                if (smo.SeaTerritory != null && smo.SeaTerritory == st)
                    return smo;

            return null;
        }
        public StatueMovementOption GetMovementOptionToSettlement(Settlement s)
        {
            foreach (StatueMovementOption smo in MovementOptions)
                if (smo.Settlement != null && smo.Settlement == s)
                    return smo;

            return null;
        }
    }
}
