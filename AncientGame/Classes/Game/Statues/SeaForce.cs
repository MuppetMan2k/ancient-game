﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eSeaForceStance
    {
        NORMAL,
        BLOCKADING,
    }

    class SeaForce : Force
    {
        /* -------- Properties -------- */
        public eSeaForceStance Stance { get; protected set; }

        /* -------- Constructors -------- */
        public SeaForce(StatuePosition inPosition, Faction inOwningFaction, string inTextureID, float inForceRating, int inMovementPoints, int inAdditionalMovementPoints, int inMaxMovementPoints, ContentManager content)
            : base(inPosition, inOwningFaction, inTextureID, inForceRating, inMovementPoints, inAdditionalMovementPoints, inMaxMovementPoints, content)
        {

        }

        /* -------- Public Methods -------- */
        public override bool CanTraverseLand()
        {
            return false;
        }
        public override bool CanTraverseWater()
        {
            return true;
        }

        public void MergeForce(Force force)
        {
            if (force is Navy)
            {
                force.SetPosition(Position);

                foreach (Unit u in Units)
                    force.Units.Add(u);

                force.RecalculateForceRating();

                Destroy();
                return;
            }

            foreach (Unit u in force.Units)
                Units.Add(u);

            RecalculateForceRating();

            SeaForce seaForce = force as SeaForce;
            seaForce.Destroy();
        }

        /* -------- Static Methods -------- */
        public static SeaForce CreateFromData(SeaForceData data, Faction inOwningFaction, StatueStyle style, GameManagerContainer gameManagers, ContentManager content)
        {
            StatuePosition Position = StatuePosition.CreateFromData(data.statuePositionData, gameManagers.MapManager);
            SeaForce seaForce = new SeaForce(Position, inOwningFaction, style.SeaForceTextureID, data.forceRating, data.movementPoints, data.additionalMovementPoints, data.maxMovementPoints, content);

            seaForce.Stance = DataUtilities.ParseSeaForceStance(data.stance);
            foreach (UnitStateData usd in data.unitStateDatas)
            {
                SeaUnitStateData susd = usd as SeaUnitStateData;

                if (susd == null)
                    continue;

                SeaUnit unit = SeaUnit.CreateFromData(susd, gameManagers);

                seaForce.Units.Add(unit);
            }

            return seaForce;
        }
    }
}
