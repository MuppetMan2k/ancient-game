﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    abstract class Statue
    {
        /* -------- Properties -------- */
        public StatuePosition Position { get; private set; }
        public Faction OwningFaction { get; private set; }
        public Rectangle Rectangle { get; private set; }
        public bool IsBeingDragged { get; private set; }
        public int MovementPoints { get; private set; }
        public int AdditionalMovementPoints { get; private set; }
        public int MaxMovementPoints { get; private set; }
        public StatueMovementOptionSet MovementOptions { get; private set; }
        // Assets
        public Texture2D StatueTex { get; private set; }
        public Texture2D StandardPoleTex { get; private set; }

        /* -------- Constructors -------- */
        public Statue(StatuePosition inPosition, Faction inOwningFaction, string inTextureID, int inMovementPoints, int inAdditionalMovementPoints, int inMaxMovementPoints, ContentManager content)
        {
            Position = inPosition;
            OwningFaction = inOwningFaction;
            IsBeingDragged = false;
            MovementPoints = inMovementPoints;
            AdditionalMovementPoints = inAdditionalMovementPoints;
            MaxMovementPoints = inMaxMovementPoints;

            StatueTex = AssetLoader.LoadTexture(inTextureID, content);
            StandardPoleTex = AssetLoader.LoadTexture(CalibrationManager.UICalibration.Data.standardPoleTextureID, content);
        }

        /* -------- Public Methods -------- */
        public void UpdateRectangle(GraphicsComponent g, Camera c)
        {
            if (Position.IsInSettlement)
                return;

            Vector3 baseGSPos = SpaceUtilities.ConvertMSToGS(Position.MSPosition);
            Vector3 topGSPos = SpaceUtilities.ConvertMSToGS(Position.MSPosition);
            topGSPos += c.UpDirection * CalibrationManager.GraphicsCalibration.Data.statueGSHeight;

            Point basePSPos = SpaceUtilities.ConvertGSToPS(baseGSPos, c, g);
            Point topPSPos = SpaceUtilities.ConvertGSToPS(topGSPos, c, g);

            int height = basePSPos.Y - topPSPos.Y;
            int width = Mathf.Round((float)height * (float)StatueTex.Width / (float)StatueTex.Height);

            Rectangle = new Rectangle(basePSPos.X - width / 2, topPSPos.Y, width, height);
        }

        public bool CanBeDragged(TurnManager tm)
        {
            return (OwningFaction.Equals(tm.FactionTurn));
        }
        public void StartDragging()
        {
            IsBeingDragged = true;
        }
        public void StopDragging()
        {
            IsBeingDragged = false;
        }

        public void SetPosition(StatuePosition newPosition)
        {
            Position = newPosition;
        }

        public string GetDescriptiveName()
        {
            string factionAdjective = OwningFaction.GetNameAdjective();

            string forceDescriptiveNameID = "text_statue_land-force_description";
            if (this is Army)
                forceDescriptiveNameID = "text_statue_army_description";
            else if (this is SeaForce)
                forceDescriptiveNameID = "text_statue_sea-force_description";
            else if (this is Navy)
                forceDescriptiveNameID = "text_statue_navy_description";
            string forceDescriptiveName = Localization.Localize(forceDescriptiveNameID);

            return string.Format(forceDescriptiveName, factionAdjective);
        }

        public void RecalculateMovementOptions(GameManagerContainer gameManagers)
        {
            MovementOptions = StatueMovementOptionUtility.CalculateStatueMovementOptions(this, gameManagers);
        }

        public abstract bool CanTraverseLand();
        public abstract bool CanTraverseWater();

        public virtual void OnMoveIntoNewLandTerritory(StatueMovementOption smo, GameManagerContainer gameManagers)
        {
            SubtractMovementPoints(smo.MovementPointCost);
            RecalculateMovementOptions(gameManagers);
        }
        public virtual void OnMoveIntoNewSeaTerritory(StatueMovementOption smo, GameManagerContainer gameManagers)
        {
            SubtractMovementPoints(smo.MovementPointCost);
            RecalculateMovementOptions(gameManagers);
        }
        public virtual void OnMoveIntoNewSettlement(StatueMovementOption smo, GameManagerContainer gameManagers)
        {
            SubtractMovementPoints(smo.MovementPointCost);
            RecalculateMovementOptions(gameManagers);
        }

        /* -------- Private Methods --------- */
        protected void Destroy()
        {
            OwningFaction.Variables.StatueSet.RemoveStatue(this);
        }

        private void SubtractMovementPoints(int pointsToSubtract)
        {
            MovementPoints = Mathf.ClampLower(MovementPoints - pointsToSubtract, 0);
            AdditionalMovementPoints = 0;
        }
    }
}
