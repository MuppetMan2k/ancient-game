﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class Army : LandForce
    {
        /* -------- Constructors -------- */
        public Army(StatuePosition inPosition, Faction inOwningFaction, string inTextureID, float inForceRating, int inMovementPoints, int inAdditionalMovementPoints, int inMaxMovementPoints, ContentManager content)
            : base(inPosition, inOwningFaction, inTextureID, inForceRating, inMovementPoints, inAdditionalMovementPoints, inMaxMovementPoints, content)
        {

        }

        /* -------- Public Methods -------- */
        public int GetMaxUnitNum()
        {
            // TODO
            return 20;
        }

        /* -------- Static Methods -------- */
        public static Army CreateFromData(ArmyData data, Faction inOwningFaction, StatueStyle style, GameManagerContainer gameManagers, ContentManager content)
        {
            StatuePosition Position = StatuePosition.CreateFromData(data.statuePositionData, gameManagers.MapManager);
            Army army = new Army(Position, inOwningFaction, style.ArmyTextureID, data.forceRating, data.movementPoints, data.additionalMovementPoints, data.maxMovementPoints, content);

            army.Stance = DataUtilities.ParseLandForceStance(data.stance);
            foreach (UnitStateData usd in data.unitStateDatas)
            {
                LandUnitStateData lusd = usd as LandUnitStateData;

                if (lusd == null)
                    continue;

                LandUnit unit = LandUnit.CreateFromData(lusd, gameManagers);

                army.Units.Add(unit);
            }

            return army;
        }
    }
}
