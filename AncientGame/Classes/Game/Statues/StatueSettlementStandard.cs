﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class StatueSettlementStandard
    {
        /* -------- Properties -------- */
        public Rectangle Rectangle { get; private set; }
        public Rectangle PoleRectangle { get; private set; }
        public Settlement Settlement { get; private set; }
        public List<Statue> Statues { get; private set; }
        public Faction Faction { get; private set; }

        /* -------- Constructors -------- */
        public StatueSettlementStandard(Statue inStatue, Camera c, GraphicsComponent g)
        {
            Statues = new List<Statue>();
            Statues.Add(inStatue);
            Settlement = inStatue.Position.Settlement;
            Faction = inStatue.OwningFaction;

            CalculateRectangle(c, g);
        }

        /* -------- Private Methods --------- */
        private void CalculateRectangle(Camera c, GraphicsComponent g)
        {
            Point bottom = SpaceUtilities.ConvertMSToPS(Settlement.MSPosition, c, g);
            Vector3 topGS = SpaceUtilities.ConvertMSToGS(Settlement.MSPosition) + c.UpDirection * CalibrationManager.GraphicsCalibration.Data.statueSettlementStandardGSHeight;
            Point top = SpaceUtilities.ConvertGSToPS(topGS, c, g);
            int height = bottom.Y - top.Y;
            int standardSize = Mathf.Round(height * CalibrationManager.GraphicsCalibration.Data.statueSettlementStandardHeightWidthFraction);

            Rectangle = new Rectangle(bottom.X - standardSize / 2, top.Y, standardSize, standardSize);

            int poleWidth = Mathf.Round(CalibrationManager.UICalibration.Data.standardPoleWidthFraction * standardSize);
            PoleRectangle = new Rectangle(bottom.X - poleWidth / 2, top.Y, poleWidth, height);
        }
    }
}
