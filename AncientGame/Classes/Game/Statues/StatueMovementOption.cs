﻿namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eStatueMovementType
    {
        FREE_MOVEMENT_LAND,
        FREE_MOVEMENT_SEA,
        GARRISON_LAND,
        GARRISON_SEA,
        TRESPASS_LAND,
        ATTACK_LAND,
        BESIEGE_LAND,
        ATTACK_SEA,
        REPEL_OCCUPIERS,
        REMAIN_LAND,
        REMAIN_SEA
    }

    class StatueMovementOption
    {
        /* -------- Properties -------- */
        public eStatueMovementType MovementType { get; private set; }
        public LandTerritory LandTerritory { get; private set; }
        public SeaTerritory SeaTerritory { get; private set; }
        public Settlement Settlement { get; private set; }
        public int MovementPointCost { get; private set; }
        public bool CanAffordCost { get; private set; }

        /* -------- Constructors -------- */
        public StatueMovementOption(eStatueMovementType inMovementType, LandTerritory inLandTerritory, SeaTerritory inSeaTerritory, Settlement inSettlement, int inMovementPointCost, bool inCanAffordCost)
        {
            MovementType = inMovementType;
            LandTerritory = inLandTerritory;
            SeaTerritory = inSeaTerritory;
            Settlement = inSettlement;
            MovementPointCost = inMovementPointCost;
            CanAffordCost = inCanAffordCost;
        }

        /* -------- Static Methods -------- */
        public static StatueMovementOption CreateForLandTerritory(eStatueMovementType inMovementType, LandTerritory inLandTerritory, int inMovementPointCost, bool inCanAffordCost)
        {
            return new StatueMovementOption(inMovementType, inLandTerritory, null, null, inMovementPointCost, inCanAffordCost);
        }
        public static StatueMovementOption CreateForSeaTerritory(eStatueMovementType inMovementType, SeaTerritory inSeaTerritory, int inMovementPointCost, bool inCanAffordCost)
        {
            return new StatueMovementOption(inMovementType, null, inSeaTerritory, null, inMovementPointCost, inCanAffordCost);
        }
        public static StatueMovementOption CreateForSettlement(eStatueMovementType inMovementType, Settlement inSettlement, int inMovementPointCost, bool inCanAffordCost)
        {
            return new StatueMovementOption(inMovementType, null, null, inSettlement, inMovementPointCost, inCanAffordCost);
        }
    }
}
