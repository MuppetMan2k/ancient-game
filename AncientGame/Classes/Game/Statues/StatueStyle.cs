﻿using DataTypes;

namespace AncientGame
{
    class StatueStyle
    {
        /* -------- Properties -------- */
        public string LandForceTextureID { get; private set; }
        public string ArmyTextureID { get; private set; }
        public string SeaForceTextureID { get; private set; }
        public string NavyTextureID { get; private set; }

        /* -------- Static Methods -------- */
        public static StatueStyle CreateFromData(StatueStyleData data)
        {
            StatueStyle statueStyle = new StatueStyle();

            statueStyle.LandForceTextureID = data.landForceTextureID;
            statueStyle.ArmyTextureID = data.armyTextureID;
            statueStyle.SeaForceTextureID = data.seaForceTextureID;
            statueStyle.NavyTextureID = data.navyTextureID;

            return statueStyle;
        }
    }
}
