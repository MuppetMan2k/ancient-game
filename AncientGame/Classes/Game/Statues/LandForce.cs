﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eLandForceStance
    {
        NORMAL,
        LAYING_SIEGE,
    }

    class LandForce : Force
    {
        /* -------- Properties -------- */
        public eLandForceStance Stance { get; protected set; }

        /* -------- Constructors -------- */
        public LandForce(StatuePosition inPosition, Faction inOwningFaction, string inTextureID, float inForceRating, int inMovementPoints, int inAdditionalMovementPoints, int inMaxMovementPoints, ContentManager content)
            : base(inPosition, inOwningFaction, inTextureID, inForceRating, inMovementPoints, inAdditionalMovementPoints, inMaxMovementPoints, content)
        {

        }

        /* -------- Public Methods -------- */
        public override bool CanTraverseLand()
        {
            return true;
        }
        public override bool CanTraverseWater()
        {
            return true;
        }
        
        public void MergeForce(Force force)
        {
            if (force is Army)
            {
                force.SetPosition(Position);

                foreach (Unit u in Units)
                    force.Units.Add(u);

                force.RecalculateForceRating();

                Destroy();
                return;
            }

            foreach (Unit u in force.Units)
                Units.Add(u);

            RecalculateForceRating();

            LandForce landForce = force as LandForce;
            landForce.Destroy();
        }

        /* -------- Static Methods -------- */
        public static LandForce CreateFromData(LandForceData data, Faction inOwningFaction, StatueStyle style, GameManagerContainer gameManagers, ContentManager content)
        {
            StatuePosition Position = StatuePosition.CreateFromData(data.statuePositionData, gameManagers.MapManager);
            LandForce landForce = new LandForce(Position, inOwningFaction, style.LandForceTextureID, data.forceRating, data.movementPoints, data.additionalMovementPoints, data.maxMovementPoints, content);

            landForce.Stance = DataUtilities.ParseLandForceStance(data.stance);
            foreach (UnitStateData usd in data.unitStateDatas)
            {
                LandUnitStateData lusd = usd as LandUnitStateData;

                if (lusd == null)
                    continue;

                LandUnit unit = LandUnit.CreateFromData(lusd, gameManagers);

                landForce.Units.Add(unit);
            }

            return landForce;
        }
    }
}
