﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    abstract class Force : Statue
    {
        /* -------- Properties -------- */
        public List<Unit> Units { get; private set; }
        public float ForceRating { get; private set; }

        /* -------- Constructors -------- */
        public Force(StatuePosition inPosition, Faction inOwningFaction, string inTextureID, float inForceRating, int inMovementPoints, int inAdditionalMovementPoints, int inMaxMovementPoints, ContentManager content)
            : base(inPosition, inOwningFaction, inTextureID, inMovementPoints, inAdditionalMovementPoints, inMovementPoints, content)
        {
            Units = new List<Unit>();
        }

        /* -------- Public Methods -------- */
        public void RecalculateForceRating()
        {
            // TODO
        }

        public int GetTotalNumberMen()
        {
            int totalNumberMen = 0;
            foreach (Unit u in Units)
                totalNumberMen += u.NumMen;
            return totalNumberMen;
        }
    }
}
