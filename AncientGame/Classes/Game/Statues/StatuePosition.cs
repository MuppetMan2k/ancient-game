﻿using System;
using Microsoft.Xna.Framework;
using DataTypes;

namespace AncientGame
{
    class StatuePosition
    {
        /* -------- Properties -------- */
        public LandTerritory LandTerritory { get; private set; }
        public bool IsInLandTerritory { get { return (LandTerritory != null); } }
        public SeaTerritory SeaTerritory { get; private set; }
        public bool IsInSeaTerritory { get { return (SeaTerritory != null); } }
        public Settlement Settlement { get; private set; }
        public bool IsInSettlement { get { return (Settlement != null); } }
        public Vector2 MSPosition { get; private set; }

        /* -------- Constructors -------- */
        public StatuePosition(LandTerritory inLandTerritory, Vector2 inMSPosition)
        {
            LandTerritory = inLandTerritory;
            SeaTerritory = null;
            Settlement = null;
            MSPosition = inMSPosition;
        }
        public StatuePosition(SeaTerritory inSeaTerritory, Vector2 inMSPosition)
        {
            LandTerritory = null;
            SeaTerritory = inSeaTerritory;
            Settlement = null;
            MSPosition = inMSPosition;
        }
        public StatuePosition(Settlement inSettlement)
        {
            LandTerritory = null;
            SeaTerritory = null;
            Settlement = inSettlement;
            MSPosition = inSettlement.MSPosition;
        }

        /* -------- Public Methods -------- */
        public bool EquivalentTo(StatuePosition position)
        {
            if (!Equals(LandTerritory, position.LandTerritory))
                return false;

            if (!Equals(SeaTerritory, position.SeaTerritory))
                return false;

            if (!Equals(Settlement, position.Settlement))
                return false;

            return true;
        }

        /* -------- Static Methods -------- */
        public static StatuePosition CreateFromData(StatuePositionData data, MapManager mapManager)
        {
            if (!String.IsNullOrEmpty(data.landTerritoryID))
            {
                LandTerritory landTerritory = mapManager.LandTerritoryManager.GetLandTerritory(data.landTerritoryID);
                return new StatuePosition(landTerritory, data.msPosition);
            }

            if (!String.IsNullOrEmpty(data.seaTerritoryID))
            {
                SeaTerritory seaTerritory = mapManager.SeaTerritoryManager.GetSeaTerritory(data.seaTerritoryID);
                return new StatuePosition(seaTerritory, data.msPosition);
            }

            if (!String.IsNullOrEmpty(data.settlementLandTerritoryID))
            {
                LandTerritory landTerritory = mapManager.LandTerritoryManager.GetLandTerritory(data.settlementLandTerritoryID);
                return new StatuePosition(landTerritory.Settlement);
            }

            return null;
        }
    }
}
