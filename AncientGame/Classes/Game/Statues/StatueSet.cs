﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class StatueSet
    {
        /* -------- Properties -------- */
        public List<Statue> Statues { get; private set; }

        /* -------- Constructors -------- */
        public StatueSet()
        {
            Statues = new List<Statue>();
        }

        /* -------- Public Methods -------- */
        public void Update(GraphicsComponent g, CameraManager cm)
        {
            foreach (Statue s in Statues)
                s.UpdateRectangle(g, cm.Camera);
        }

        public void RemoveStatue(Statue s)
        {
            if (!Statues.Remove(s))
                Debug.LogWarning("Tried to remove statue [{0}] but ut couldn't be found", s.ToString());
        }

        /* -------- Static Methods -------- */
        public static StatueSet CreateFromData(StatueSetData data, Faction inOwningFaction, StatueStyle style, GameManagerContainer gameManagers, ContentManager content)
        {
            StatueSet statueSet = new StatueSet();

            foreach (LandForceData lfd in data.landForceDatas)
            {
                LandForce landForce = LandForce.CreateFromData(lfd, inOwningFaction, style, gameManagers, content);
                statueSet.Statues.Add(landForce);
            }

            foreach (ArmyData ad in data.armyDatas)
            {
                Army army = Army.CreateFromData(ad, inOwningFaction, style, gameManagers, content);
                statueSet.Statues.Add(army);
            }

            foreach (SeaForceData sfd in data.seaForceDatas)
            {
                SeaForce seaForce = SeaForce.CreateFromData(sfd, inOwningFaction, style, gameManagers, content);
                statueSet.Statues.Add(seaForce);
            }

            foreach (NavyData nd in data.navyDatas)
            {
                Navy navy = Navy.CreateFromData(nd, inOwningFaction, style, gameManagers, content);
                statueSet.Statues.Add(navy);
            }

            return statueSet;
        }
    }
}
