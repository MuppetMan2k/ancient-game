﻿using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class Navy : SeaForce
    {
        /* -------- Constructors -------- */
        public Navy(StatuePosition inPosition, Faction inOwningFaction, string inTextureID, float inForceRating, int inMovementPoints, int inAdditionalMovementPoints, int inMaxMovementPoints, ContentManager content)
            : base(inPosition, inOwningFaction, inTextureID, inForceRating, inMovementPoints, inAdditionalMovementPoints, inMaxMovementPoints, content)
        {

        }

        /* -------- Public Methods -------- */
        public int GetMaxUnitNum()
        {
            // TODO
            return 20;
        }

        /* -------- Static Methods -------- */
        public static Navy CreateFromData(NavyData data, Faction inOwningFaction, StatueStyle style, GameManagerContainer gameManagers, ContentManager content)
        {
            StatuePosition Position = StatuePosition.CreateFromData(data.statuePositionData, gameManagers.MapManager);
            Navy navy = new Navy(Position, inOwningFaction, style.SeaForceTextureID, data.forceRating, data.movementPoints, data.additionalMovementPoints, data.maxMovementPoints, content);

            navy.Stance = DataUtilities.ParseSeaForceStance(data.stance);
            foreach (UnitStateData usd in data.unitStateDatas)
            {
                SeaUnitStateData susd = usd as SeaUnitStateData;

                if (susd == null)
                    continue;

                SeaUnit unit = SeaUnit.CreateFromData(susd, gameManagers);

                navy.Units.Add(unit);
            }

            return navy;
        }
    }
}
