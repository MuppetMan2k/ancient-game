﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    class GraphicsComponent : DrawableGameComponent
    {
        /* -------- Properties -------- */
        // Graphics objects
        public SpriteBatch SpriteBatch { get; private set; }
        // Graphics properties
        public int ScreenWidth { get; private set; }
        public int ScreenHeight { get; private set; }
        public float AspectRatio { get { return (float)ScreenWidth / (float)ScreenHeight; } }
        public int HalfScreenWidth { get { return ScreenWidth / 2; } }
        public int HalfScreenHeight { get { return ScreenHeight / 2; } }
        public Rectangle FullScreenRectangle { get { return new Rectangle(0, 0, ScreenWidth, ScreenHeight); } }

        /* -------- Constructors -------- */
        public GraphicsComponent(Game inGame)
            : base(inGame)
        {
            DrawOrder = 1;
        }

        /* -------- Public Methods -------- */
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            base.Draw(gameTime);
        }

        public void SetGraphicsObjects(GraphicsDeviceManager inGraphicsDeviceManager)
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            ScreenWidth = inGraphicsDeviceManager.PreferredBackBufferWidth;
            ScreenHeight = inGraphicsDeviceManager.PreferredBackBufferHeight;
        }

        public void SetRasterizerState()
        {
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;
        }

        /* -------- Static Methods -------- */
        public static Resolution GetDefaultResolution()
        {
            Resolution res = new Resolution();
            res.Width = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            res.Height = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;

            return res;
        }
    }
}
