﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    struct VertexOverlay : IVertexType
    {
        /* -------- Public Fields -------- */
        public Vector3 position;
        public Color color1;
        public Color color2;

        /* -------- Vertex Declaration -------- */
        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
            new VertexElement(sizeof(float) * (0), VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(sizeof(float) * (3 + 1 * 0), VertexElementFormat.Color, VertexElementUsage.Color, 0),
            new VertexElement(sizeof(float) * (3 + 1 * 1), VertexElementFormat.Color, VertexElementUsage.Color, 1)
            );
        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

        /* -------- Constructors -------- */
        public VertexOverlay(Vector3 inPosition, Color inColor1, Color inColor2)
        {
            position = inPosition;
            color1 = inColor1;
            color2 = inColor2;
        }
    }
}
