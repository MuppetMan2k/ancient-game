﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    struct VertexLandTerrain : IVertexType
    {
        /* -------- Public Fields -------- */
        public Vector3 position;
        public Vector2 textureCoords;
        public Vector2 bumpMapCoords;
        public int textureIndex;

        /* -------- Vertex Declaration -------- */
        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
            new VertexElement(sizeof(float) * (0), VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(sizeof(float) * (3 + 2 * 0), VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(sizeof(float) * (3 + 2 * 1), VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 1),
            new VertexElement(sizeof(float) * (3 + 2 * 2), VertexElementFormat.Byte4, VertexElementUsage.TextureCoordinate, 2)
            );
        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

        /* -------- Constructors -------- */
        public VertexLandTerrain(Vector3 inPosition, Vector2 inTextureCoords, Vector2 inBumpMapCoords, int inTextureIndex)
        {
            position = inPosition;
            textureCoords = inTextureCoords;
            bumpMapCoords = inBumpMapCoords;
            textureIndex = inTextureIndex;
        }
    }
}