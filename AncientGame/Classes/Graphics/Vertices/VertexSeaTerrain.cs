﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    struct VertexSeaTerrain : IVertexType
    {
        /* -------- Public Fields -------- */
        public Vector3 position;
        public Vector2 textureCoords;
        public Vector2 bumpMap1Coords;
        public Vector2 bumpMap2Coords;
        public int textureIndex;

        /* -------- Vertex Declaration -------- */
        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
            new VertexElement(sizeof(float) * (0), VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(sizeof(float) * (3 + 2 * 0), VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(sizeof(float) * (3 + 2 * 1), VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 1),
            new VertexElement(sizeof(float) * (3 + 2 * 2), VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 2),
            new VertexElement(sizeof(float) * (3 + 2 * 3), VertexElementFormat.Byte4, VertexElementUsage.TextureCoordinate, 3)
            );
        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

        /* -------- Constructors -------- */
        public VertexSeaTerrain(Vector3 inPosition, Vector2 inTextureCoords, Vector2 inBumpMap1Coords, Vector2 inBumpMap2Coords, int inTextureIndex)
        {
            position = inPosition;
            textureCoords = inTextureCoords;
            bumpMap1Coords = inBumpMap1Coords;
            bumpMap2Coords = inBumpMap2Coords;
            textureIndex = inTextureIndex;
        }
    }
}