﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    struct VertexTradeVehicle : IVertexType
    {
        /* -------- Public Fields -------- */
        public Vector3 position;
        public Vector2 textureCoords;
        public int textureIndex;
        public float alpha;

        /* -------- Vertex Declaration -------- */
        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
            new VertexElement(sizeof(float) * (0), VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(sizeof(float) * (3), VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(sizeof(float) * (3 + 2), VertexElementFormat.Single, VertexElementUsage.TextureCoordinate, 1),
            new VertexElement(sizeof(float) * (3 + 2 + 1), VertexElementFormat.Single, VertexElementUsage.TextureCoordinate, 2)
            );
        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

        /* -------- Constructors -------- */
        public VertexTradeVehicle(Vector3 inPosition, Vector2 inTextureCoords, int inTextureIndex, float inAlpha)
        {
            position = inPosition;
            textureCoords = inTextureCoords;
            textureIndex = inTextureIndex;
            alpha = inAlpha;
        }
    }
}
