﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    struct VertexBorder : IVertexType
    {
        /* -------- Public Fields -------- */
        public Vector3 position;
        public Color color;

        /* -------- Vertex Declaration -------- */
        public static readonly VertexDeclaration VertexDeclaration = new VertexDeclaration(
            new VertexElement(sizeof(float) * (0), VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(sizeof(float) * (3 + 2 * 0), VertexElementFormat.Color, VertexElementUsage.Color, 0)
            );
        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }

        /* -------- Constructors -------- */
        public VertexBorder(Vector3 inPosition, Color inColor)
        {
            position = inPosition;
            color = inColor;
        }
    }
}
