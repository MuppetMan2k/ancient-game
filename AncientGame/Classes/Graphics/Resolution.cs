﻿namespace AncientGame
{
    struct Resolution
    {
        /* -------- Public Fields -------- */
        public int Width;
        public int Height;

        /* -------- Constructors -------- */
        public Resolution(int inWidth, int inHeight)
        {
            Width = inWidth;
            Height = inHeight;
        }

        /* -------- Public Methods -------- */
        public string CreateSettingsText()
        {
            string text = "";

            text += Width.ToString();
            text += "x";
            text += Height.ToString();

            return text;
        }

        public static Resolution CreateResolutionFromSettingsText(string text)
        {
            string[] parts = text.Split('x');

            if (parts.Length != 2)
                return GraphicsComponent.GetDefaultResolution();

            int width;
            int height;
            if (!int.TryParse(parts[0], out width) || !int.TryParse(parts[1], out height))
                return GraphicsComponent.GetDefaultResolution();

            return new Resolution(width, height);
        }
    }
}
