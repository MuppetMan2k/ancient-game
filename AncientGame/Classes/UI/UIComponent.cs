﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class UIComponent : DrawableGameComponent
    {
        /* -------- Properties -------- */
        public Cursor Cursor { get; private set; }
        public Tooltip Tooltip { get; private set; }
        public UIStyle Style { get; private set; }

        /* -------- Private Fields -------- */
        private UIMouseOverDetection mouseOverDetection;
        // UI
        private List<UIEntity> uiEntities;
        private List<UIEntity> uiEntitiesToAdd;
        private List<UIEntity> uiEntitiesToRemove;
        // Sister components
        private GraphicsComponent graphicsComponent;
        private InputComponent inputComponent;
        // Content manager
        private ContentManager contentManager;

        /* -------- Constructors -------- */
        public UIComponent(Game inGame, GraphicsComponent inGraphicsComponent, InputComponent inInputComponent)
            : base(inGame)
        {
            mouseOverDetection = new UIMouseOverDetection();

            graphicsComponent = inGraphicsComponent;
            inputComponent = inInputComponent;

            uiEntities = new List<UIEntity>();
            uiEntitiesToAdd = new List<UIEntity>();
            uiEntitiesToRemove = new List<UIEntity>();

            UpdateOrder = 3;
            DrawOrder = 3;
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTime gameTime)
        {
            GameTimeWrapper gtw = new GameTimeWrapper(gameTime);

            // Update additions and removals
            UpdateUIAddList();
            UpdateUIRemoveList();

            // Update UI entities
            foreach (UIEntity ui in uiEntities)
                if (ui.Active)
                    ui.Update(gtw, graphicsComponent, inputComponent);
            if (Cursor != null)
                Cursor.Update(gtw, graphicsComponent, inputComponent);
            
            // Mouse over detection
            mouseOverDetection.Update(uiEntities, graphicsComponent, inputComponent, this);

            // Update tooltip
            if (Tooltip != null)
                Tooltip.Update(gtw, graphicsComponent, inputComponent);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            graphicsComponent.SpriteBatch.Begin();

            foreach (UIEntity ui in uiEntities)
                if (ui.Active)
                    ui.SBDraw(graphicsComponent);

            if (Cursor != null)
                Cursor.SBDraw(graphicsComponent);

            if (Tooltip != null)
                Tooltip.SBDraw(graphicsComponent);

            graphicsComponent.SpriteBatch.End();

            base.Draw(gameTime);
        }

        public void OnNewFlowStateSet(UIStyle inStyle, ContentManager inContentManager)
        {
            Style = inStyle;
            contentManager = inContentManager;

            uiEntities.Clear();

            Cursor = new Cursor(Style, contentManager);
            Tooltip = new Tooltip(Style, contentManager);
        }

        public void AddUI(UIEntity uiToAdd)
        {
            uiEntitiesToAdd.Add(uiToAdd);
        }
        public void RemoveUI(UIEntity uiToRemove)
        {
            uiEntitiesToRemove.Add(uiToRemove);
        }

        public bool HasDetectedMouseOverUI()
        {
            return (mouseOverDetection.CurrMouseOverUI != null);
        }
        public eUIFocus GetMouseOverDetectionFocus()
        {
            return mouseOverDetection.Focus;
        }

        public void SetTooltipInfo(TooltipInfo inInfo, eCursorChangeSource inSource)
        {
            Tooltip.SetTooltipInfo(inInfo, Style, contentManager, inSource);
        }

        /* -------- Private Methods --------- */
        private void UpdateUIAddList()
        {
            foreach (UIEntity ui in uiEntitiesToAdd)
                uiEntities.Add(ui);

            uiEntitiesToAdd.Clear();
        }
        private void UpdateUIRemoveList()
        {
            foreach (UIEntity ui in uiEntitiesToRemove)
                uiEntities.Remove(ui);

            uiEntitiesToRemove.Clear();
        }
    }
}
