﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class Standard : UIEntity
    {
        /* -------- Private Fields -------- */
        private Faction faction;
        // Constants
        private const float EMBLEM_POS_X_FRACTION = 0.1981f;
        private const float EMBLEM_POS_Y_FRACTION = 0.1491f;
        private const float EMBLEM_SIZE_WIDTH_FRACTION = 0.6038f;

        /* -------- Constructors -------- */
        public Standard(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, TooltipInfo inTooltipInfo, Faction inFaction, bool emblemIsButton, WindowManager inWindowManager)
            : base(inPosition, inAnchor, inSize, inSize, inFocus, inTooltipInfo)
        {
            faction = inFaction;

            int width = Rectangle.Width;

            // Emblem
            Point emblemPos = new Point((int)(EMBLEM_POS_X_FRACTION * (float)width), (int)(EMBLEM_POS_Y_FRACTION * (float)inSize));
            int emblemSize = (int)(EMBLEM_SIZE_WIDTH_FRACTION * (float)width);
            TooltipInfo info = TooltipInfo.CreateStaticTextTooltipInfo(faction.GetDisplayName());
            if (emblemIsButton)
            {
                EmblemButton emblem = new EmblemButton(emblemPos, eUIAnchor.TOP_LEFT, emblemSize, Focus, info, inFaction, inWindowManager);
                SetChild(emblem);
            }
            else
            {
                Emblem emblem = new Emblem(emblemPos, eUIAnchor.TOP_LEFT, emblemSize, Focus, info, inFaction);
                SetChild(emblem);
            }
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(faction.Variables.StandardColorTex, Rectangle, Color.White);
            g.SpriteBatch.Draw(faction.Variables.StandardTintTex, Rectangle, faction.Variables.PrimaryColor);

            base.SBDraw(g);
        }

        /* -------- Static Methods -------- */
        public static void DrawStandardGraphics(GraphicsComponent g, Faction faction, Rectangle rectangle)
        {
            g.SpriteBatch.Draw(faction.Variables.StandardColorTex, rectangle, Color.White);
            g.SpriteBatch.Draw(faction.Variables.StandardTintTex, rectangle, faction.Variables.PrimaryColor);

            int emblemX = rectangle.Left + Mathf.Round(EMBLEM_POS_X_FRACTION * rectangle.Width);
            int emblemY = rectangle.Top + Mathf.Round(EMBLEM_POS_Y_FRACTION * rectangle.Height);
            int emblemSize = Mathf.Round(EMBLEM_SIZE_WIDTH_FRACTION * rectangle.Width);
            Rectangle emblemRect = new Rectangle(emblemX, emblemY, emblemSize, emblemSize);

            g.SpriteBatch.Draw(faction.Variables.EmblemTintTex, emblemRect, faction.Variables.SecondaryColor);
            g.SpriteBatch.Draw(faction.Variables.EmblemColorTex, emblemRect, Color.White);
        }
    }
}
