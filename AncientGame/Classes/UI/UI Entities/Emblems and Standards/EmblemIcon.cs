﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class EmblemIcon : UIEntity
    {
        /* -------- Private Fields -------- */
        private Faction faction;
        // Assets
        private Texture2D circleTex;
        private Texture2D outlineTex;
        private Texture2D tintTex;
        private Texture2D colorTex;
        // Constants
        private const float EMBLEM_SIZE_FRACTION = 0.8f;

        /* -------- Constructors -------- */
        public EmblemIcon(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, TooltipInfo inTooltipInfo, Faction inFaction, ContentManager content)
            : base(inPosition, inAnchor, inSize, inSize, inFocus, inTooltipInfo)
        {
            faction = inFaction;

            circleTex = AssetLoader.LoadTexture("emblem-icon-circle", content);
            outlineTex = AssetLoader.LoadTexture("emblem-icon-outline", content);
            string tintTexID = inFaction.Variables.EmblemID + "-tint";
            tintTex = AssetLoader.LoadTexture(tintTexID, content);
            string colorTexID = inFaction.Variables.EmblemID + "-color";
            colorTex = AssetLoader.LoadTexture(colorTexID, content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(circleTex, Rectangle, faction.Variables.PrimaryColor);
            g.SpriteBatch.Draw(outlineTex, Rectangle, faction.Variables.SecondaryColor);

            int emblemSize = Mathf.Round(EMBLEM_SIZE_FRACTION * (float)Rectangle.Width);
            Rectangle emblemRect = new Rectangle(Rectangle.X + (Rectangle.Width - emblemSize) / 2, Rectangle.Y + (Rectangle.Height - emblemSize) / 2, emblemSize, emblemSize);
            g.SpriteBatch.Draw(tintTex, emblemRect, faction.Variables.SecondaryColor);
            g.SpriteBatch.Draw(colorTex, emblemRect, Color.White);

            base.SBDraw(g);
        }
    }
}
