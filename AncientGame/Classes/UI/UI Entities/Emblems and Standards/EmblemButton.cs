﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class EmblemButton : Button
    {
        /* -------- Private Fields -------- */
        private Faction faction;
        private WindowManager windowManager;

        /* -------- Constructors -------- */
        public EmblemButton(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, TooltipInfo inTooltipInfo, Faction inFaction, WindowManager inWindowManager)
            : base(inPosition, inAnchor, inSize, inSize, inFocus, inTooltipInfo, null, null, null, null)
        {
            faction = inFaction;
            windowManager = inWindowManager;
            rightClickHandler = OpenFactionWindow;
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(faction.Variables.EmblemTintTex, Rectangle, faction.Variables.SecondaryColor);
            g.SpriteBatch.Draw(faction.Variables.EmblemColorTex, Rectangle, Color.White);

            base.SBDraw(g);
        }

        /* -------- Private Methods --------- */
        private void OpenFactionWindow()
        {
            windowManager.CreateWindow_Faction(faction);
        }
    }
}
