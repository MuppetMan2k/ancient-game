﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class Emblem : UIEntity
    {
        /* -------- Private Fields -------- */
        private Faction faction;

        /* -------- Constructors -------- */
        public Emblem(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, TooltipInfo inTooltipInfo, Faction inFaction)
            : base(inPosition, inAnchor, inSize, inSize, inFocus, inTooltipInfo)
        {
            faction = inFaction;
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(faction.Variables.EmblemTintTex, Rectangle, faction.Variables.SecondaryColor);
            g.SpriteBatch.Draw(faction.Variables.EmblemColorTex, Rectangle, Color.White);

            base.SBDraw(g);
        }
    }
}
