﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class Tooltip : UIEntity
    {
        /* -------- Properties -------- */
        public eCursorChangeSource Source { get; private set; }
        public bool IsActive { get { return (tooltipInfo != null); } }

        /* -------- Private Fields -------- */
        private TooltipInfo tooltipInfo;
        private Color bodyColor1;
        private Color bodyColor2;
        private Color bodyColor3;
        // Assets
        private Texture2D pixelTex;
        private Texture2D cornerTex;
        private Texture2D leftRightTex;
        private Texture2D topBottomTex;
        // Constants
        private const int MOUSE_X_OFFSET = 40;
        private const int MOUSE_Y_OFFSET = 40;
        private const int INSIDE_X_BORDER = 9;
        private const int INSIDE_Y_BORDER = 6;
        private const int TEXT_LINE_SPACING = 5;
        private const int TEXT_MAX_LINES = 5;
        private const int MAX_WIDTH = 400;
        private const int CORNER_SIZE = 6;
        private const int EDGE_SIZE = 4;
        private const int LAND_TERRITORY_VERT_SPACING = 8;
        private const int LAND_TERRITORY_EMBLEM_SIZE = 20;
        private const int STANDARD_SPACING = 4;

        /* -------- Constructors -------- */
        public Tooltip(UIStyle style, ContentManager content)
            : base(new Point(), eUIAnchor.TOP_LEFT, 0, 0, eUIFocus.NO_FOCUS, null)
        {
            pixelTex = AssetLoader.LoadTexture("white-pixel", content);
            cornerTex = AssetLoader.LoadTexture("tooltip-corner", content);
            leftRightTex = AssetLoader.LoadTexture("tooltip-left-right", content);
            topBottomTex = AssetLoader.LoadTexture("tooltip-top-bottom", content);
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTimeWrapper gtw, GraphicsComponent g, InputComponent i)
        {
            Point mousePos = i.GetMousePosition();
            int posX = Mathf.ClampUpper(mousePos.X + MOUSE_X_OFFSET, g.ScreenWidth - Rectangle.Width);
            int posY = Mathf.ClampUpper(mousePos.Y + MOUSE_Y_OFFSET, g.ScreenHeight - Rectangle.Height);
            SetPosition(new Point(posX, posY));

            base.Update(gtw, g, i);

            UpdateRectangleSize();
        }

        public override void SBDraw(GraphicsComponent g)
        {
            if (tooltipInfo == null)
                return;

            switch (tooltipInfo.Type)
            {
                case eTooltipType.LAND_TERRITORY:
                    SBDrawForLandTerritory(g);
                    break;
                default:
                    SBDrawPlain(g);
                    break;
            }

            SBDrawEdges(g);
            SBDrawCorners(g);

            base.SBDraw(g);
        }

        public override bool MouseIsOver(InputComponent i)
        {
            return false;
        }

        public override eUIType GetUIType()
        {
            return eUIType.STATIC;
        }

        public void SetTooltipInfo(TooltipInfo info, UIStyle style, ContentManager content, eCursorChangeSource inSource)
        {
            ClearTooltipInfo();

            Source = inSource;
            tooltipInfo = info;
            SetUpForTooltipInfo(style, content);
            UpdateRectangleSize();
        }
        public void ResetTooltipInfo()
        {
            ClearTooltipInfo();
            Source = eCursorChangeSource.NONE;
        }

        /* -------- Private Methods --------- */
        private void ClearTooltipInfo()
        {
            tooltipInfo = null;
            Children.Clear();
        }

        private void UpdateRectangleSize()
        {
            Rectangle childAggregate = GetAggregateChildRectangle();

            Rectangle newRectangle = Rectangle;
            newRectangle.Width = childAggregate.Width + 2 * INSIDE_X_BORDER;
            newRectangle.Height = childAggregate.Height + 2 * INSIDE_Y_BORDER;
            Rectangle = newRectangle;
        }

        private void SetUpForTooltipInfo(UIStyle style, ContentManager content)
        {
            switch (tooltipInfo.Type)
            {
                case eTooltipType.STATIC_TEXT:
                    SetUpForStaticText(style, content);
                    break;
                case eTooltipType.LAND_TERRITORY:
                    SetUpForLandTerritory(style, content);
                    break;
                case eTooltipType.STATUE:
                    SetUpForStatue(style, content);
                    break;
                case eTooltipType.TRADE_ROUTES:
                    SetUpForTradeRoutes(style, content);
                    break;
                case eTooltipType.INT_VAL:
                    SetUpForIntVal(style, content);
                    break;
                case eTooltipType.FLOAT_VAL:
                    SetUpForFloatVal(style, content);
                    break;
                case eTooltipType.MODIFIER_SET:
                    SetUpForModifierSet(style, content);
                    break;
                case eTooltipType.TAX:
                    SetUpForTax(style, content);
                    break;
                case eTooltipType.INCOME_EFFICIENCY_SET:
                    SetUpForIncomeEfficiencySet(style, content);
                    break;
                case eTooltipType.BUILDING:
                    SetUpForBuilding(style, content);
                    break;
                case eTooltipType.BUILDING_DATA:
                    SetUpForBuildingData(style, content);
                    break;
                case eTooltipType.COST:
                    SetUpForCost(style, content);
                    break;
                case eTooltipType.LAND_UNIT_CARD:
                    SetUpForLandUnitCard(style, content);
                    break;
                case eTooltipType.SEA_UNIT_CARD:
                    SetUpForSeaUnitCard(style, content);
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(tooltipInfo.Type);
                    break;
            }
        }
        private void SetUpForStaticText(UIStyle style, ContentManager content)
        {
            bodyColor1 = style.TooltipBodyColor;
            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);
            eUITextShade textShade = UIUtilities.GetTextShade(style.TooltipBodyColor);
            Color textColor = (textShade == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;
            List<ColoredTextSegment> coloredTextSegments = ColoredTextSegment.GetSegmentsForSingleText(tooltipInfo.StaticText, textColor);
            Text text = new Text(point, eUIAnchor.TOP_LEFT, coloredTextSegments, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);

            SetChild(text);
        }
        private void SetUpForLandTerritory(UIStyle style, ContentManager content)
        {
            Faction owningFaction = tooltipInfo.LandTerritory.Variables.OwningFaction;
            Faction occupyingFaction = tooltipInfo.LandTerritory.Variables.OccupyingFaction;
            bodyColor1 = owningFaction.Variables.PrimaryColor;
            bodyColor2 = owningFaction.Variables.SecondaryColor;
            
            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);

            EmblemIcon owningFactionEmblem = new EmblemIcon(point, eUIAnchor.TOP_LEFT, LAND_TERRITORY_EMBLEM_SIZE, eUIFocus.NO_FOCUS, null, owningFaction, content);
            SetChild(owningFactionEmblem);

            string owningFactionName = owningFaction.GetDisplayName();
            eUITextShade textShade1 = UIUtilities.GetTextShade(bodyColor1);
            Color textColor1 = (textShade1 == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;
            List<ColoredTextSegment> coloredTextSegments1 = ColoredTextSegment.GetSegmentsForSingleText(owningFactionName, textColor1);
            Point text1Point = new Point(point.X + LAND_TERRITORY_EMBLEM_SIZE + 4, point.Y);
            Text text1 = new Text(text1Point, eUIAnchor.TOP_LEFT, coloredTextSegments1, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, -1, 1, eUIFocus.NO_FOCUS, null, content);
            SetChild(text1);

            point.Y += text1.Rectangle.Height + LAND_TERRITORY_VERT_SPACING;

            string landTerritoryName = tooltipInfo.LandTerritory.GetNameString();
            eUITextShade textShade2 = UIUtilities.GetTextShade(bodyColor2);
            Color textColor2 = (textShade2 == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;
            List<ColoredTextSegment> coloredTextSegments2 = ColoredTextSegment.GetSegmentsForSingleText(landTerritoryName, textColor2);
            Text text2 = new Text(point, eUIAnchor.TOP_LEFT, coloredTextSegments2, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, -1, 1, eUIFocus.NO_FOCUS, null, content);
            SetChild(text2);

            if (occupyingFaction != null)
            {
                bodyColor3 = occupyingFaction.Variables.PrimaryColor;

                point.Y += text2.Rectangle.Height + LAND_TERRITORY_VERT_SPACING;

                EmblemIcon occupyingFactionEmblem = new EmblemIcon(point, eUIAnchor.TOP_LEFT, LAND_TERRITORY_EMBLEM_SIZE, eUIFocus.NO_FOCUS, null, occupyingFaction, content);
                SetChild(occupyingFactionEmblem);

                string occupyingFactionName = string.Format(Localization.Localize("text_occupied_by"), occupyingFaction.GetDisplayName());
                eUITextShade textShade3 = UIUtilities.GetTextShade(bodyColor3);
                Color textColor3 = (textShade3 == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;
                List<ColoredTextSegment> coloredTextSegments3 = ColoredTextSegment.GetSegmentsForSingleText(occupyingFactionName, textColor3);
                Point text3Point = new Point(point.X + LAND_TERRITORY_EMBLEM_SIZE + 4, point.Y);
                Text text3 = new Text(text3Point, eUIAnchor.TOP_LEFT, coloredTextSegments3, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, -1, 1, eUIFocus.NO_FOCUS, null, content);
                SetChild(text3);
            }
        }
        private void SetUpForStatue(UIStyle style, ContentManager content)
        {
            Statue statue = tooltipInfo.Statue;
            Faction owningFaction = statue.OwningFaction;
            bodyColor1 = owningFaction.Variables.PrimaryColor;

            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);

            EmblemIcon owningFactionEmblem = new EmblemIcon(point, eUIAnchor.TOP_LEFT, LAND_TERRITORY_EMBLEM_SIZE, eUIFocus.NO_FOCUS, null, owningFaction, content);
            SetChild(owningFactionEmblem);

            string statueName = statue.GetDescriptiveName();
            eUITextShade textShade1 = UIUtilities.GetTextShade(bodyColor1);
            Color textColor1 = (textShade1 == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;
            List<ColoredTextSegment> coloredTextSegments1 = ColoredTextSegment.GetSegmentsForSingleText(statueName, textColor1);
            Point text1Point = new Point(point.X + LAND_TERRITORY_EMBLEM_SIZE + 4, point.Y);
            Text text1 = new Text(text1Point, eUIAnchor.TOP_LEFT, coloredTextSegments1, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, -1, 1, eUIFocus.NO_FOCUS, null, content);
            SetChild(text1);
        }
        private void SetUpForTradeRoutes(UIStyle style, ContentManager content)
        {
            // TODO

            bodyColor1 = style.TooltipBodyColor;
            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);
            eUITextShade textShade = UIUtilities.GetTextShade(style.TooltipBodyColor);
            Color textColor = (textShade == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;
            List<ColoredTextSegment> coloredTextSegments = ColoredTextSegment.GetSegmentsForSingleText("Trade routes", textColor);
            Text text = new Text(point, eUIAnchor.TOP_LEFT, coloredTextSegments, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);

            SetChild(text);
        }
        private void SetUpForIntVal(UIStyle style, ContentManager content)
        {
            bodyColor1 = style.TooltipBodyColor;
            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);
            eUITextShade textShade = UIUtilities.GetTextShade(style.TooltipBodyColor);
            Color textColor = (textShade == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;
            List<ColoredTextSegment> coloredTextSegments = new List<ColoredTextSegment>();
            coloredTextSegments.Add(new ColoredTextSegment(tooltipInfo.StaticText, textColor));
            coloredTextSegments.Add(new ColoredTextSegment(tooltipInfo.IntVal.ToString("0"), tooltipInfo.ValColor));
            Text text = new Text(point, eUIAnchor.TOP_LEFT, coloredTextSegments, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);

            SetChild(text);
        }
        private void SetUpForFloatVal(UIStyle style, ContentManager content)
        {
            bodyColor1 = style.TooltipBodyColor;
            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);
            eUITextShade textShade = UIUtilities.GetTextShade(style.TooltipBodyColor);
            Color textColor = (textShade == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;
            List<ColoredTextSegment> coloredTextSegments = new List<ColoredTextSegment>();
            coloredTextSegments.Add(new ColoredTextSegment(tooltipInfo.StaticText, textColor));
            coloredTextSegments.Add(new ColoredTextSegment(tooltipInfo.FloatValString, tooltipInfo.ValColor));
            Text text = new Text(point, eUIAnchor.TOP_LEFT, coloredTextSegments, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);

            SetChild(text);
        }
        private void SetUpForModifierSet(UIStyle style, ContentManager content)
        {
            bodyColor1 = style.TooltipBodyColor;
            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);
            eUITextShade textShade = UIUtilities.GetTextShade(style.TooltipBodyColor);
            Color textColor = (textShade == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;

            if (!String.IsNullOrEmpty(tooltipInfo.StaticText))
            {
                List<ColoredTextSegment> coloredTextSegments = ColoredTextSegment.GetSegmentsForSingleText(tooltipInfo.StaticText, textColor);
                Text staticText = new Text(point, eUIAnchor.TOP_LEFT, coloredTextSegments, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(staticText);
                point.Y += staticText.Rectangle.Height + 2 * TEXT_LINE_SPACING;
            }
            else if (tooltipInfo.StaticTextSegments != null)
            {
                Text staticText = new Text(point, eUIAnchor.TOP_LEFT, tooltipInfo.StaticTextSegments, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(staticText);
                point.Y += staticText.Rectangle.Height + 2 * TEXT_LINE_SPACING;
            }

            if (tooltipInfo.ModifierSet.Modifiers.Count > 0)
            {
                foreach (Modifier mod in tooltipInfo.ModifierSet.Modifiers)
                {
                    Text modText = new Text(point, eUIAnchor.TOP_LEFT, mod.GetColoredTextSegments(style, textColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                    SetChild(modText);
                    point.Y += modText.Rectangle.Height + TEXT_LINE_SPACING;
                }
            }
            else
            {
                Text noModifiersText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_no_modifiers"), textColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(noModifiersText);
            }
        }
        private void SetUpForTax(UIStyle style, ContentManager content)
        {
            bodyColor1 = style.TooltipBodyColor;
            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);
            eUITextShade textShade = UIUtilities.GetTextShade(style.TooltipBodyColor);
            Color textColor = (textShade == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;

            List<ColoredTextSegment> coloredTextSegments = ColoredTextSegment.GetSegmentsForSingleText(tooltipInfo.Tax.GetName(), textColor);
            Text staticText = new Text(point, eUIAnchor.TOP_LEFT, coloredTextSegments, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
            SetChild(staticText);
            point.Y += staticText.Rectangle.Height + 2 * TEXT_LINE_SPACING;

            if (tooltipInfo.Tax.ModifierSet.Modifiers.Count > 0)
            {
                foreach (Modifier mod in tooltipInfo.Tax.ModifierSet.Modifiers)
                {
                    Text modText = new Text(point, eUIAnchor.TOP_LEFT, mod.GetColoredTextSegments(style, textColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                    SetChild(modText);
                    point.Y += modText.Rectangle.Height + TEXT_LINE_SPACING;
                }
            }
            else
            {
                Text noModifiersText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_no_modifiers"), textColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(noModifiersText);
            }
        }
        private void SetUpForIncomeEfficiencySet(UIStyle style, ContentManager content)
        {
            bodyColor1 = style.TooltipBodyColor;
            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);
            eUITextShade textShade = UIUtilities.GetTextShade(style.TooltipBodyColor);
            Color textColor = (textShade == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;

            if (tooltipInfo.IncomeEfficiencySet.Efficiencies.Count > 0)
            {
                IOrderedEnumerable<IncomeEfficiency> orderedEfficiencies = tooltipInfo.IncomeEfficiencySet.Efficiencies.OrderByDescending(x => x.Value);
                foreach (IncomeEfficiency ie in orderedEfficiencies)
                {
                    Color numColor;
                    eUIAttitude attitude = IncomeEfficiency.GetUIAttitude(ie.Source);
                    if (attitude == eUIAttitude.POSITIVE)
                        numColor = style.PositiveColor;
                    else if (attitude == eUIAttitude.NEGATIVE)
                        numColor = style.NegativeColor;
                    else
                        numColor = style.NeutralColor;

                    List<ColoredTextSegment> coloredTextSegments = new List<ColoredTextSegment>();
                    coloredTextSegments.Add(new ColoredTextSegment(UIUtilities.GetPercentageFloatDisplayString(ie.Value), numColor));
                    coloredTextSegments.Add(new ColoredTextSegment(IncomeEfficiency.GetSourceName(ie.Source), textColor));
                    
                    Text text = new Text(point, eUIAnchor.TOP_LEFT, coloredTextSegments, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                    SetChild(text);
                }
            }
            else
            {
                List<ColoredTextSegment> coloredTextSegments = ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_no_income_efficiencies"), textColor);
                Text text = new Text(point, eUIAnchor.TOP_LEFT, coloredTextSegments, style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(text);
            }
        }
        private void SetUpForBuilding(UIStyle style, ContentManager content)
        {
            bodyColor1 = style.TooltipBodyColor;
            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);
            eUITextShade textShade = UIUtilities.GetTextShade(style.TooltipBodyColor);
            Color textColor = (textShade == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;

            Text buildingNameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Building.GetName(tooltipInfo.Building.Data), textColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
            SetChild(buildingNameText);
            point.Y += buildingNameText.Rectangle.Height + TEXT_LINE_SPACING;

            string historicalName = Building.GetHistoricalName(tooltipInfo.Building.Data);
            if (!String.IsNullOrEmpty(historicalName))
            {
                Text buildingHistoricalNameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(historicalName, textColor), style.TooltipAlternateFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(buildingHistoricalNameText);
                point.Y += buildingHistoricalNameText.Rectangle.Height + TEXT_LINE_SPACING;
            }

            if (tooltipInfo.Building.DamageLevel != eBuildingDamageLevel.NO_DAMAGE)
            {
                Text damageText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(tooltipInfo.Building.GetDamageString(), style.NegativeColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(damageText);
                point.Y += damageText.Rectangle.Height + TEXT_LINE_SPACING;
            }

            foreach (Modifier mod in tooltipInfo.Building.ModifierSet.Modifiers)
            {
                Text modText = new Text(point, eUIAnchor.TOP_LEFT, mod.GetColoredTextSegments(style, textColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(modText);
                point.Y += modText.Rectangle.Height + TEXT_LINE_SPACING;
            }
        }
        private void SetUpForBuildingData(UIStyle style, ContentManager content)
        {
            bodyColor1 = style.TooltipBodyColor;
            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);
            eUITextShade textShade = UIUtilities.GetTextShade(style.TooltipBodyColor);
            Color textColor = (textShade == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;

            Text buildingNameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Building.GetName(tooltipInfo.BuildingData), textColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
            SetChild(buildingNameText);
            point.Y += buildingNameText.Rectangle.Height + TEXT_LINE_SPACING;

            string historicalName = Building.GetHistoricalName(tooltipInfo.BuildingData);
            if (!String.IsNullOrEmpty(historicalName))
            {
                Text buildingHistoricalNameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(historicalName, textColor), style.TooltipAlternateFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(buildingHistoricalNameText);
                point.Y += buildingHistoricalNameText.Rectangle.Height + TEXT_LINE_SPACING;
            }

            Cost cost = Cost.CreateFromData(tooltipInfo.BuildingData.costData);

            if (cost.MoneyCost > 0)
            {
                IconText moneyIcon = new IconText(point, eUIAnchor.TOP_LEFT, MAX_WIDTH - 2 * INSIDE_X_BORDER, eUIFocus.NO_FOCUS, null, "money-icon", UIUtilities.GetIntDisplayString(tooltipInfo.Cost.MoneyCost), textColor, style.TooltipFontID, content);
                SetChild(moneyIcon);
                point.Y += moneyIcon.Rectangle.Height + TEXT_LINE_SPACING;
            }

            if (cost.AdminPowerCost > 0)
            {
                IconText adminIcon = new IconText(point, eUIAnchor.TOP_LEFT, MAX_WIDTH - 2 * INSIDE_X_BORDER, eUIFocus.NO_FOCUS, null, "admin-icon", UIUtilities.GetIntDisplayString(tooltipInfo.Cost.AdminPowerCost), textColor, style.TooltipFontID, content);
                SetChild(adminIcon);
                point.Y += adminIcon.Rectangle.Height + TEXT_LINE_SPACING;
            }

            if (cost.ManpowerCost > 0)
            {
                IconText adminIcon = new IconText(point, eUIAnchor.TOP_LEFT, MAX_WIDTH - 2 * INSIDE_X_BORDER, eUIFocus.NO_FOCUS, null, "manpower-icon", UIUtilities.GetIntDisplayString(tooltipInfo.Cost.ManpowerCost), textColor, style.TooltipFontID, content);
                SetChild(adminIcon);
                point.Y += adminIcon.Rectangle.Height + TEXT_LINE_SPACING;
            }

            if (tooltipInfo.BuildingRequirements.ShouldShowRequirementsSectionInTooltip())
            {
                Text requiresText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_resource_requirements"), textColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(requiresText);

                foreach (Resource r in tooltipInfo.BuildingRequirements.RequiredResources)
                {
                    IconText resourceIcon = new IconText(point, eUIAnchor.TOP_LEFT, MAX_WIDTH - 2 * INSIDE_X_BORDER, eUIFocus.NO_FOCUS, null, r.IconTextureID, r.GetName(), textColor, style.TooltipFontID, content);
                    SetChild(resourceIcon);
                }

                if (tooltipInfo.BuildingRequirements.RequiredPopulation > 0)
                {
                    string populationString = UIUtilities.GetLargeIntDisplayString(tooltipInfo.BuildingRequirements.RequiredPopulation);
                    IconText populationIcon = new IconText(point, eUIAnchor.TOP_LEFT, MAX_WIDTH - 2 * INSIDE_X_BORDER, eUIFocus.NO_FOCUS, null, "population-icon", populationString, textColor, style.TooltipFontID, content);
                    SetChild(populationIcon);
                }
            }

            if (tooltipInfo.BuildabilityResult == eBuildabilityResult.CANNOT_AFFORD_COST)
            {
                Text cannotAffordText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_cannot_afford"), style.NegativeColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(cannotAffordText);
            }
            if (tooltipInfo.BuildabilityResult == eBuildabilityResult.REQUIRED_RESOURCES_NOT_PRESENT)
            {
                Text noResourcesText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_no_required_resources"), style.NegativeColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(noResourcesText);
            }
            if (tooltipInfo.BuildabilityResult == eBuildabilityResult.NOT_ENOUGH_POPULATION)
            {
                Text notEnoughPopulationText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_not_enough_population"), style.NegativeColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(notEnoughPopulationText);
            }
        }
        private void SetUpForCost(UIStyle style, ContentManager content)
        {
            bodyColor1 = style.TooltipBodyColor;
            Point point = new Point(INSIDE_X_BORDER, INSIDE_Y_BORDER);
            eUITextShade textShade = UIUtilities.GetTextShade(style.TooltipBodyColor);
            Color textColor = (textShade == eUITextShade.LIGHT) ? style.TooltipTextLightColor : style.TooltipTextDarkColor;

            if (!String.IsNullOrEmpty(tooltipInfo.StaticText))
            {
                Text staticText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(tooltipInfo.StaticText, textColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(staticText);
                point.Y += staticText.Rectangle.Height + TEXT_LINE_SPACING;
            }

            if (!tooltipInfo.BoolVal)
            {
                Text cannotAffordText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_cannot_afford"), style.NegativeColor), style.TooltipFontID, eTextAlignment.LEFT, TEXT_LINE_SPACING, MAX_WIDTH - 2 * INSIDE_X_BORDER, TEXT_MAX_LINES, eUIFocus.NO_FOCUS, null, content);
                SetChild(cannotAffordText);
                point.Y += cannotAffordText.Rectangle.Height + TEXT_LINE_SPACING;
            }

            if (tooltipInfo.Cost.MoneyCost > 0)
            {
                IconText moneyIcon = new IconText(point, eUIAnchor.TOP_LEFT, MAX_WIDTH - 2 * INSIDE_X_BORDER, eUIFocus.NO_FOCUS, null, "money-icon", UIUtilities.GetIntDisplayString(tooltipInfo.Cost.MoneyCost), textColor, style.TooltipFontID, content);
                SetChild(moneyIcon);
                point.Y += moneyIcon.Rectangle.Height + TEXT_LINE_SPACING;
            }

            if (tooltipInfo.Cost.AdminPowerCost > 0)
            {
                IconText adminIcon = new IconText(point, eUIAnchor.TOP_LEFT, MAX_WIDTH - 2 * INSIDE_X_BORDER, eUIFocus.NO_FOCUS, null, "admin-icon", UIUtilities.GetIntDisplayString(tooltipInfo.Cost.AdminPowerCost), textColor, style.TooltipFontID, content);
                SetChild(adminIcon);
                point.Y += adminIcon.Rectangle.Height + TEXT_LINE_SPACING;
            }

            if (tooltipInfo.Cost.ManpowerCost > 0)
            {
                IconText adminIcon = new IconText(point, eUIAnchor.TOP_LEFT, MAX_WIDTH - 2 * INSIDE_X_BORDER, eUIFocus.NO_FOCUS, null, "manpower-icon", UIUtilities.GetIntDisplayString(tooltipInfo.Cost.ManpowerCost), textColor, style.TooltipFontID, content);
                SetChild(adminIcon);
                point.Y += adminIcon.Rectangle.Height + TEXT_LINE_SPACING;
            }
        }
        private void SetUpForLandUnitCard(UIStyle style, ContentManager content)
        {
            // TODO
        }
        private void SetUpForSeaUnitCard(UIStyle style, ContentManager content)
        {
            // TODO
        }

        private void SBDrawCorners(GraphicsComponent g)
        {
            Rectangle topLeft = new Rectangle(Rectangle.Left - CORNER_SIZE, Rectangle.Top - CORNER_SIZE, CORNER_SIZE, CORNER_SIZE);
            g.SpriteBatch.Draw(cornerTex, topLeft, Color.White);
            Rectangle topRight = new Rectangle(Rectangle.Right, Rectangle.Top - CORNER_SIZE, CORNER_SIZE, CORNER_SIZE);
            g.SpriteBatch.Draw(cornerTex, topRight, Color.White);
            Rectangle bottomLeft = new Rectangle(Rectangle.Left - CORNER_SIZE, Rectangle.Bottom, CORNER_SIZE, CORNER_SIZE);
            g.SpriteBatch.Draw(cornerTex, bottomLeft, Color.White);
            Rectangle bottomRight = new Rectangle(Rectangle.Right, Rectangle.Bottom, CORNER_SIZE, CORNER_SIZE);
            g.SpriteBatch.Draw(cornerTex, bottomRight, Color.White);
        }
        private void SBDrawEdges(GraphicsComponent g)
        {
            Rectangle top = new Rectangle(Rectangle.Left, Rectangle.Top - EDGE_SIZE, Rectangle.Width, EDGE_SIZE);
            g.SpriteBatch.Draw(topBottomTex, top, Color.White);
            Rectangle bottom = new Rectangle(Rectangle.Left, Rectangle.Bottom, Rectangle.Width, EDGE_SIZE);
            g.SpriteBatch.Draw(topBottomTex, bottom, Color.White);
            Rectangle left = new Rectangle(Rectangle.Left - EDGE_SIZE, Rectangle.Top, EDGE_SIZE, Rectangle.Height);
            g.SpriteBatch.Draw(leftRightTex, left, Color.White);
            Rectangle right = new Rectangle(Rectangle.Right, Rectangle.Top, EDGE_SIZE, Rectangle.Height);
            g.SpriteBatch.Draw(leftRightTex, right, Color.White);
        }

        private void SBDrawForLandTerritory(GraphicsComponent g)
        {
            int outerBandSize = INSIDE_Y_BORDER + Children[0].Rectangle.Height + LAND_TERRITORY_VERT_SPACING / 2;

            Rectangle top = new Rectangle(Rectangle.X, Rectangle.Y, Rectangle.Width, outerBandSize);
            g.SpriteBatch.Draw(pixelTex, top, bodyColor1);
            
            if (tooltipInfo.LandTerritory.Variables.OccupyingFaction == null)
            {
                Rectangle middle = new Rectangle(Rectangle.X, Rectangle.Y + outerBandSize, Rectangle.Width, Rectangle.Height - outerBandSize);
                g.SpriteBatch.Draw(pixelTex, middle, bodyColor2);
            }
            else
            {
                Rectangle middle = new Rectangle(Rectangle.X, Rectangle.Y + outerBandSize, Rectangle.Width, Rectangle.Height - 2 * outerBandSize);
                g.SpriteBatch.Draw(pixelTex, middle, bodyColor2);

                Rectangle bottom = new Rectangle(Rectangle.X, Rectangle.Bottom - outerBandSize, Rectangle.Width, outerBandSize);
                g.SpriteBatch.Draw(pixelTex, bottom, bodyColor3);
            }
        }
        private void SBDrawPlain(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(pixelTex, Rectangle, bodyColor1);
        }
    }
}
