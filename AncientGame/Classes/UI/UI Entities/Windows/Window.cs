﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eWindowType
    {
        MANAGEMENT,
        INFORMATION,
        MESSAGE
    }

    class Window : UIEntity
    {
        /* -------- Properties -------- */
        public eWindowType Type { get; private set; }

        /* -------- Private Fields -------- */
        private List<Page> pages;
        private WindowManager windowManager;
        // Assets
        private Texture2D backgroundTex;
        private Texture2D tabSeparatorTex;
        private Texture2D vignetteTex;
        // Constants
        private const int TAB_SEPARATOR_HEIGHT = 4;

        /* -------- Constructors -------- */
        public Window(eWindowType inType, WindowManager inWindowManager, UIStyle style, ContentManager content)
            : base(GetWindowPosition(inType), eUIAnchor.TOP_CENTER, GetWindowWidth(inType), GetWindowHeight(inType), eUIFocus.WINDOW, null)
        {
            Type = inType;
            windowManager = inWindowManager;

            pages = new List<Page>();

            backgroundTex = AssetLoader.LoadTexture(style.WindowBackgroundTextureID, content);
            tabSeparatorTex = AssetLoader.LoadTexture(style.WindowTabSeparatorTextureID, content);
            vignetteTex = AssetLoader.LoadTexture("vignette", content);
            
            // Close button
            int closeButtonSize = CalibrationManager.UICalibration.Data.windowCloseButtonSize;
            TooltipInfo info = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_click_to_close"));
            Button closeButton = new Button(new Point(), eUIAnchor.BOTTOM_RIGHT, closeButtonSize, closeButtonSize, eUIFocus.WINDOW, info, style.WindowCloseButtonTextureID, content, CloseWindow, null);
            SetChild(closeButton);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            UIUtilities.SBWrapTextureAcrossRectangle(g, backgroundTex, Rectangle);
            g.SpriteBatch.Draw(vignetteTex, Rectangle, Color.White);

            if (pages.Count > 0)
            {
                Rectangle separatorRect = new Rectangle(Rectangle.Left, Rectangle.Top + CalibrationManager.UICalibration.Data.windowPageTabSize, Rectangle.Width, TAB_SEPARATOR_HEIGHT);
                g.SpriteBatch.Draw(tabSeparatorTex, separatorRect, Color.White);
            }

            base.SBDraw(g);
        }

        public override eUIType GetUIType()
        {
            return eUIType.STATIC;
        }

        public void AddPage(Page inPage, UIStyle style, string pageIconTextureID, string pageLocID, GraphicsComponent g, ContentManager content)
        {
            int tabSize = CalibrationManager.UICalibration.Data.windowPageTabSize;
            int tabX = tabSize * pages.Count;
            PageTab tab = new PageTab(new Point(tabX, 0), tabSize, inPage, this, style.WindowPageTabTextureID, pageIconTextureID, pageLocID, g, content);
            SetChild(tab);

            inPage.SetActive(pages.Count == 0, g);

            pages.Add(inPage);
            SetChild(inPage);
        }

        public void SetPageActive(Page page, GraphicsComponent g)
        {
            foreach (Page p in pages)
                p.SetActive(false, g);

            page.SetActive(true, g);
        }

        /* -------- Private Methods --------- */
        private void CloseWindow()
        {
            windowManager.RemoveWindow(this);
        }

        /* -------- Static Methods --------- */
        private static Point GetWindowPosition(eWindowType inType)
        {
            switch (inType)
            {
                case eWindowType.MANAGEMENT:
                    return CalibrationManager.UICalibration.Data.managementWindowPosition;
                case eWindowType.INFORMATION:
                    return CalibrationManager.UICalibration.Data.informationWindowPosition;
                case eWindowType.MESSAGE:
                    return CalibrationManager.UICalibration.Data.messageWindowPosition;
                default:
                    Debug.LogUnrecognizedValueWarning(inType);
                    return CalibrationManager.UICalibration.Data.managementWindowPosition;
            }
        }
        private static int GetWindowWidth(eWindowType inType)
        {
            switch (inType)
            {
                case eWindowType.MANAGEMENT:
                    return CalibrationManager.UICalibration.Data.managementWindowWidth;
                case eWindowType.INFORMATION:
                    return CalibrationManager.UICalibration.Data.informationWindowWidth;
                case eWindowType.MESSAGE:
                    return CalibrationManager.UICalibration.Data.messageWindowWidth;
                default:
                    Debug.LogUnrecognizedValueWarning(inType);
                    return CalibrationManager.UICalibration.Data.managementWindowWidth;
            }
        }
        private static int GetWindowHeight(eWindowType inType)
        {
            switch (inType)
            {
                case eWindowType.MANAGEMENT:
                    return CalibrationManager.UICalibration.Data.managementWindowHeight;
                case eWindowType.INFORMATION:
                    return CalibrationManager.UICalibration.Data.informationWindowHeight;
                case eWindowType.MESSAGE:
                    return CalibrationManager.UICalibration.Data.messageWindowHeight;
                default:
                    Debug.LogUnrecognizedValueWarning(inType);
                    return CalibrationManager.UICalibration.Data.managementWindowHeight;
            }
        }
    }
}
