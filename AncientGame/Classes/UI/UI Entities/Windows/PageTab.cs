﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class PageTab : Button
    {
        /* -------- Private Fields -------- */
        private Page page;
        private Window window;
        private GraphicsComponent graphicsComponent;
        // Assets
        private Texture2D tabTex;
        private Texture2D iconTex;
        // Constants
        private const float TEXTURE_SIZE_FACTOR = 0.75f;
        private const float INACTIVE_BLACK_ALPHA = 0.4f;

        /* -------- Constructors -------- */
        public PageTab(Point inPosition, int inSize, Page inPage, Window inWindow, string inTabTextureID, string inIconTextureID, string inLocID, GraphicsComponent g, ContentManager content)
            : base(inPosition, eUIAnchor.TOP_LEFT, inSize, inSize, eUIFocus.WINDOW, TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize(inLocID)), null, content, null, null)
        {
            page = inPage;
            window = inWindow;
            graphicsComponent = g;

            leftClickHandler = SetPageActive;

            tabTex = AssetLoader.LoadTexture(inTabTextureID, content);
            iconTex = AssetLoader.LoadTexture(inIconTextureID, content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(tabTex, Rectangle, Color.White);

            Rectangle iconRectangle = new Rectangle(Rectangle.Left + Mathf.Round(0.5f * (1f - TEXTURE_SIZE_FACTOR) * Rectangle.Width), Rectangle.Top + Mathf.Round(0.5f * (1f - TEXTURE_SIZE_FACTOR) * Rectangle.Height), Mathf.Round(TEXTURE_SIZE_FACTOR * Rectangle.Width), Mathf.Round(TEXTURE_SIZE_FACTOR * Rectangle.Height));

            g.SpriteBatch.Draw(iconTex, iconRectangle, Color.White);

            base.SBDraw(g);

            if (!page.Active)
                g.SpriteBatch.Draw(tabTex, Rectangle, Color.Black * INACTIVE_BLACK_ALPHA);
        }

        /* -------- Private Methods --------- */
        private void SetPageActive()
        {
            if (page.Active)
                return;

            window.SetPageActive(page, graphicsComponent);
        }
    }
}
