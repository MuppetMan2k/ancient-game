﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class Page : UIEntity
    {
        /* -------- Constructors -------- */
        public Page(Window window)
            : base(new Point(window.Rectangle.Left, window.Rectangle.Top + CalibrationManager.UICalibration.Data.windowPageTabSize), eUIAnchor.TOP_LEFT, window.Rectangle.Width, window.Rectangle.Height - CalibrationManager.UICalibration.Data.windowPageTabSize, eUIFocus.WINDOW, null)
        {
        }

        /* -------- Public Methods -------- */
        public override eUIType GetUIType()
        {
            return eUIType.STATIC;
        }
    }
}
