﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class WindowDiv : UIEntity
    {
        /* -------- Properties -------- */
        public static int InnerBorder { get { return 8; } }

        /* -------- Private Fields -------- */
        private Color tint;
        // Assets
        private Texture2D pixelTex;

        /* -------- Constructors -------- */
        public WindowDiv(Point inPosition, eUIAnchor inAnchor, int inWidth, int inHeight, eUIFocus inFocus, UIStyle style, ContentManager content)
            : base(inPosition, inAnchor, inWidth, inHeight, inFocus, null)
        {
            pixelTex = AssetLoader.LoadTexture("white-pixel", content);
            tint = style.WindowDivColor;
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(pixelTex, Rectangle, tint);

            base.SBDraw(g);
        }

        public override eUIType GetUIType()
        {
            return eUIType.STATIC;
        }
    }
}
