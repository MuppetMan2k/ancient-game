﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandTerritoryStatisticFoodStoresStateEntry : LandTerritoryStatisticPlainEntry
    {
        /* -------- Private Fields -------- */
        // Reference store
        private UIStyle style;
        private FoodStoresState foodStoresState;
        private GameManagerContainer gameManagers;
        private ContentManager content;

        /* -------- Constructors -------- */
        public LandTerritoryStatisticFoodStoresStateEntry(int inWidth, eUIFocus inFocus, UIStyle inStyle, FoodStoresState inFoodStoresState, GameManagerContainer inGameManagers, GraphicsComponent g, ContentManager inContent)
            : base(inWidth, inFocus)
        {
            style = inStyle;
            foodStoresState = inFoodStoresState;
            gameManagers = inGameManagers;
            content = inContent;

            Refresh(g, false);
        }

        /* -------- Public Methods -------- */
        public override void Refresh(GraphicsComponent g, bool recurse)
        {
            RemoveAllChildren();

            Point point = new Point(THICK_INNER_BORDER, THICK_INNER_BORDER);
            int width = Rectangle.Width;

            string foodStoresString = Localization.Localize("text_food_stores");
            Text foodStoresText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(foodStoresString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, width / 2 - THICK_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            SetChild(foodStoresText);

            point.Y += foodStoresText.Rectangle.Height + SPACING;

            TooltipInfo foodStoresTooltipInfo = TooltipInfo.CreateModifierSetTooltipInfo(Localization.Localize("text_food_stores_tooltip"), foodStoresState.GetFoodStoresModiferSet(gameManagers));
                
            string foodStoresNumString = String.Format(Localization.Localize("text_out_of"), UIUtilities.GetIntDisplayString(foodStoresState.FoodStores), UIUtilities.GetIntDisplayString(foodStoresState.MaxFoodStores));
            IconText manpowerIcon = new IconText(point, eUIAnchor.TOP_LEFT, width / 2, Focus, foodStoresTooltipInfo, "food-stores-icon", foodStoresNumString, style.WindowTextColor, style.WindowFontID, content);
            SetChild(manpowerIcon);

            base.Refresh(g, recurse);
        }
    }
}
