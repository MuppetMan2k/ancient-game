﻿using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandTerritoryThroughportRow : UIEntity
    {
        /* -------- Private Fields -------- */
        // Constants
        private const int EMBLEM_SPACING = 3;

        /* -------- Constructors -------- */
        public LandTerritoryThroughportRow(int inWidth, eUIFocus inFocus, int inNameWidth, int inValueWidth, TradePath tradePath, UIStyle style, ContentManager content)
            : base(new Point(), eUIAnchor.TOP_LEFT, inWidth, ResourceIcon.Size, inFocus, null)
        {
            int resourcesWidth = inWidth - 2 * inNameWidth - inValueWidth;
            int maxNumResourceIcons = resourcesWidth / ResourceIcon.Size;

            Point point = new Point();
            LandTerritory exporterLandTerritory = tradePath.ExportLandTerritory;
            LandTerritory importerLandTerritory = tradePath.ImportLandTerritory;

            // Exporter name
            point = new Point(EMBLEM_SPACING, 0);
            Faction exportFaction = exporterLandTerritory.Variables.OwningFaction;
            TooltipInfo exportEmblemTooltip = TooltipInfo.CreateStaticTextTooltipInfo(exportFaction.GetDisplayName());
            EmblemIcon exportEmblemIcon = new EmblemIcon(point, eUIAnchor.TOP_LEFT, ResourceIcon.Size, eUIFocus.WINDOW, exportEmblemTooltip, exportFaction, content);
            SetChild(exportEmblemIcon);
            point = new Point(exportEmblemIcon.Rectangle.Width + 2 * EMBLEM_SPACING, 0);
            Text exporterLandTerritoryNameText = new Text(point, eUIAnchor.CENTER_LEFT, ColoredTextSegment.GetSegmentsForSingleText(exporterLandTerritory.GetNameString(), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.LEFT, 0, inNameWidth - exportEmblemIcon.Rectangle.Width - 2 * EMBLEM_SPACING, 1, eUIFocus.WINDOW, null, content);
            SetChild(exporterLandTerritoryNameText);

            // Importer name
            point = new Point(inNameWidth + EMBLEM_SPACING, 0);
            Faction importFaction = importerLandTerritory.Variables.OwningFaction;
            TooltipInfo importEmblemTooltip = TooltipInfo.CreateStaticTextTooltipInfo(importFaction.GetDisplayName());
            EmblemIcon importEmblemIcon = new EmblemIcon(point, eUIAnchor.TOP_LEFT, ResourceIcon.Size, eUIFocus.WINDOW, importEmblemTooltip, importFaction, content);
            SetChild(importEmblemIcon);
            point = new Point(inNameWidth + importEmblemIcon.Rectangle.Width + 2 * EMBLEM_SPACING, 0);
            Text importerLandTerritoryNameText = new Text(point, eUIAnchor.CENTER_LEFT, ColoredTextSegment.GetSegmentsForSingleText(importerLandTerritory.GetNameString(), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.LEFT, 0, inNameWidth - importEmblemIcon.Rectangle.Width - 2 * EMBLEM_SPACING, 1, eUIFocus.WINDOW, null, content);
            SetChild(importerLandTerritoryNameText);

            // Resources
            point = new Point(2 * inNameWidth, 0);
            IOrderedEnumerable<SingleResourcePresence> orderedResources = tradePath.TradedResources.OrderByDescending(x => x.Quantity);
            int numResources = Mathf.Min(maxNumResourceIcons, tradePath.TradedResources.Count);
            int numResourcesMade = 0;
            foreach (SingleResourcePresence srp in orderedResources)
            {
                if (numResourcesMade >= numResources)
                    break;

                ResourceIcon resourceIcon = new ResourceIcon(point, eUIAnchor.TOP_LEFT, eUIFocus.WINDOW, srp.Resource, srp.Quantity, eResourceIconMode.PLAIN, style, content);
                SetChild(resourceIcon);
                point.X += ResourceIcon.Size;
                numResourcesMade++;
            }

            // Value
            point = new Point(2 * inNameWidth + resourcesWidth, 0);
            string valueString = UIUtilities.GetFloatDisplayString(tradePath.GetTradedResourcesValue());
            TooltipInfo valueTooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_trade_value"));
            Text valueText = new Text(point, eUIAnchor.CENTER_LEFT, ColoredTextSegment.GetSegmentsForSingleText(valueString, style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, inValueWidth, 1, inFocus, valueTooltipInfo, content);
            SetChild(valueText);
        }
    }
}
