﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class GarrisonLandForcePanel : GarrisonStatuePanel
    {
        /* -------- Properties -------- */
        public LandForce LandForce { get; private set; }

        /* -------- Constructors -------- */
        public GarrisonLandForcePanel(Point inPosition, eUIAnchor inAnchor, int inWidth, eUIFocus inFocus, UIStyle style, ContentManager content, bool addControlButtons, GarrisonForceMergeController inController, LandForce inLandForce)
            : base(inPosition, inAnchor, inWidth, inFocus, style, TooltipInfo.CreateStatueTooltipInfo(inLandForce), content, addControlButtons, inController)
        {
            LandForce = inLandForce;

            string nameString = inLandForce.GetDescriptiveName();
            string numberString = Localization.Localize("text_garrison_force_men_units");
            numberString = string.Format(numberString, LandForce.GetTotalNumberMen(), LandForce.Units.Count);

            Point textPos = new Point(TEXT_OFFSET, TEXT_OFFSET);
            Text nameText = new Text(textPos, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(nameString, style.GarrisonForcePanelTextColor), style.GarrisonForcePanelFontID, eTextAlignment.LEFT, 0, Rectangle.Width, 1, inFocus, null, content);
            SetChild(nameText);
            textPos.Y += nameText.Rectangle.Height + TEXT_OFFSET;
            Text numberText = new Text(textPos, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(nameString, style.GarrisonForcePanelTextColor), style.GarrisonForcePanelFontID, eTextAlignment.LEFT, 0, Rectangle.Width, 1, inFocus, null, content);
            SetChild(numberText);
        }
    }
}
