﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    abstract class GarrisonStatuePanel : UIEntity
    {
        /* -------- Private Fields -------- */
        private GarrisonForceMergeController controller;
        private Texture2D backgroundTex;
        private Texture2D cornerTex;
        private Texture2D topBottomEdgeTex;
        private Texture2D leftRightEdgeTex;
        // Constants
        protected const int PANEL_HEIGHT = 40;
        protected const int BUTTON_SIZE = 32;
        protected const int BUTTON_OFFSET = 6;
        protected const int TEXT_OFFSET = 5;

        /* -------- Constructors -------- */
        public GarrisonStatuePanel(Point inPosition, eUIAnchor inAnchor, int inWidth, eUIFocus inFocus, UIStyle style, TooltipInfo inTooltipInfo, ContentManager content, bool addControlButtons, GarrisonForceMergeController inController)
            : base(inPosition, inAnchor, inWidth, PANEL_HEIGHT, inFocus, inTooltipInfo)
        {
            controller = inController;

            backgroundTex = AssetLoader.LoadTexture(style.GarrisonForcePanelBackgroundTexID, content);
            cornerTex = AssetLoader.LoadTexture(style.GarrisonForcePanelCornerTexID, content);
            topBottomEdgeTex = AssetLoader.LoadTexture(style.GarrisonForcePanelTopBottomEdgeTexID, content);
            leftRightEdgeTex = AssetLoader.LoadTexture(style.GarrisonForcePanelLeftRightEdgeTexID, content);

            if (addControlButtons)
            {
                // Buttons
                Point buttonPos = new Point(BUTTON_OFFSET, 0);
                Button marchOutButton = new Button(buttonPos, eUIAnchor.CENTER_RIGHT, BUTTON_SIZE, BUTTON_SIZE, inFocus, null, "march-out-garrison-button", content, null, null);
                SetChild(marchOutButton);
                buttonPos.X += BUTTON_SIZE + BUTTON_OFFSET;
                Button mergeButton = new Button(buttonPos, eUIAnchor.CENTER_RIGHT, BUTTON_SIZE, BUTTON_SIZE, inFocus, null, "marge-garrison-button", content, null, null);
                SetChild(mergeButton);
            }
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            UIUtilities.SBWrapTextureAcrossRectangle(g, backgroundTex, Rectangle);

            int cornerWidth = cornerTex.Width;
            int cornerHeight = cornerTex.Height;
            int topBottomHeight = topBottomEdgeTex.Height;
            int leftRightWidth = leftRightEdgeTex.Width;
            Rectangle topLeftCornerRect = new Rectangle(Rectangle.Left, Rectangle.Top, cornerWidth, cornerHeight);
            Rectangle topRightCornerRect = new Rectangle(Rectangle.Right - cornerWidth, Rectangle.Top, cornerWidth, cornerHeight);
            Rectangle bottomLeftCornerRect = new Rectangle(Rectangle.Left, Rectangle.Bottom - cornerHeight, cornerWidth, cornerHeight);
            Rectangle bottomRightCornerRect = new Rectangle(Rectangle.Right - cornerWidth, Rectangle.Bottom - cornerHeight, cornerWidth, cornerHeight);
            g.SpriteBatch.Draw(cornerTex, topLeftCornerRect, Color.White);
            g.SpriteBatch.Draw(cornerTex, topRightCornerRect, Color.White);
            g.SpriteBatch.Draw(cornerTex, bottomLeftCornerRect, Color.White);
            g.SpriteBatch.Draw(cornerTex, bottomRightCornerRect, Color.White);
            Rectangle topRect = new Rectangle(Rectangle.Left + cornerWidth, Rectangle.Top, Rectangle.Width - 2 * cornerWidth, topBottomHeight);
            Rectangle bottomRect = new Rectangle(Rectangle.Left + cornerWidth, Rectangle.Bottom - topBottomHeight, Rectangle.Width - 2 * cornerWidth, topBottomHeight);
            Rectangle leftRect = new Rectangle(Rectangle.Left, Rectangle.Top + cornerHeight, leftRightWidth, Rectangle.Height - 2 * cornerHeight);
            Rectangle rightRect = new Rectangle(Rectangle.Right - leftRightWidth, Rectangle.Top + cornerHeight, leftRightWidth, Rectangle.Height - 2 * cornerHeight);
            g.SpriteBatch.Draw(topBottomEdgeTex, topRect, Color.White);
            g.SpriteBatch.Draw(topBottomEdgeTex, bottomRect, Color.White);
            g.SpriteBatch.Draw(leftRightEdgeTex, leftRect, Color.White);
            g.SpriteBatch.Draw(leftRightEdgeTex, rightRect, Color.White);

            base.SBDraw(g);
        }
    }
}
