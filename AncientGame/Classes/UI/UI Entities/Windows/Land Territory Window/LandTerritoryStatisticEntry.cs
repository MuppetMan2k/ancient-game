﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    abstract class LandTerritoryStatisticEntry : UIEntity
    {
        /* -------- Properties -------- */
        public static int Height { get { return 80; } }

        /* -------- Private Fields -------- */
        // Constants
        protected const int INNER_BORDER = 10;
        protected const int SPACING = 8;

        /* -------- Constructors -------- */
        public LandTerritoryStatisticEntry(Point inPosition, eUIAnchor inAnchor, int inWidth, eUIFocus inFocus)
            : base(inPosition, inAnchor, inWidth, Height, inFocus, null)
        {
        }
    }
}
