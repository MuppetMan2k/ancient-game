﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class StatConstituentRack : UIEntity
    {
        /* -------- Private Fields -------- */
        // Assets
        private Texture2D backgroundTex;
        private Texture2D plusTex;
        private Texture2D minusTex;
        // Constants
        private const float VALUE_PER_ICON_MULTIPLIER = 1.1f;

        /* -------- Constructors -------- */
        public StatConstituentRack(Point inPosition, eUIAnchor inAnchor, int inWidth, eUIFocus inFocus, UIStyle style, StatConstituentSet statConstituents, ContentManager content)
            : base(inPosition, inAnchor, inWidth, 2 * StatConstituentIcon.Size, inFocus, null)
        {
            backgroundTex = AssetLoader.LoadTexture(style.LandTerritoryConstituentRackBackgroundTextureID, content);
            plusTex = AssetLoader.LoadTexture(style.LandTerritoryConstituentRackPlusTextureID, content);
            minusTex = AssetLoader.LoadTexture(style.LandTerritoryConstituentRackMinusTextureID, content);

            int numIcons = (inWidth - StatConstituentIcon.Size) / StatConstituentIcon.Size;

            List<StatConstituent> positiveConstituents = statConstituents.StatConstituents.FindAll(x => StatConstituent.GetBias(x.Type) == eStatConstituentBias.POSITIVE);
            List<StatConstituent> negativeConstituents = statConstituents.StatConstituents.FindAll(x => StatConstituent.GetBias(x.Type) == eStatConstituentBias.NEGATIVE);

            float valuePerIcon = Mathf.Max(GetValuePerIcon(numIcons, positiveConstituents), GetValuePerIcon(numIcons, negativeConstituents));

            IOrderedEnumerable<StatConstituent> sortedPositiveConstituents = positiveConstituents.OrderBy(x => x.Value);
            IOrderedEnumerable<StatConstituent> sortedNegativeConstituents = negativeConstituents.OrderByDescending(x => x.Value);

            Point point = new Point(StatConstituentIcon.Size, 0);
            int iconsRemaining = numIcons;
            foreach (StatConstituent sc in sortedPositiveConstituents)
            {
                int numIconsForConstituent = Mathf.Ceiling(sc.Value / valuePerIcon);
                for (int i = 0; i < numIconsForConstituent; i++)
                {
                    StatConstituentIcon icon = new StatConstituentIcon(point, eUIAnchor.TOP_RIGHT, inFocus, style, sc, content);
                    SetChild(icon);

                    point.X += StatConstituentIcon.Size;
                    iconsRemaining--;
                    if (iconsRemaining <= 0)
                        break;
                }

                if (iconsRemaining <= 0)
                    break;
            }
            point = new Point(StatConstituentIcon.Size, StatConstituentIcon.Size);
            iconsRemaining = numIcons;
            foreach (StatConstituent sc in sortedNegativeConstituents)
            {
                int numIconsForConstituent = Mathf.Ceiling(Mathf.Abs(sc.Value) / valuePerIcon);
                for (int i = 0; i < numIconsForConstituent; i++)
                {
                    StatConstituentIcon icon = new StatConstituentIcon(point, eUIAnchor.TOP_RIGHT, inFocus, style, sc, content);
                    SetChild(icon);

                    point.X += StatConstituentIcon.Size;
                    iconsRemaining--;
                    if (iconsRemaining <= 0)
                        break;
                }

                if (iconsRemaining <= 0)
                    break;
            }
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(backgroundTex, Rectangle, Color.White);

            Rectangle plusRect = new Rectangle(Rectangle.Right - StatConstituentIcon.Size, Rectangle.Top, StatConstituentIcon.Size, StatConstituentIcon.Size);
            g.SpriteBatch.Draw(plusTex, plusRect, Color.White);
            Rectangle minusRect = new Rectangle(Rectangle.Right - StatConstituentIcon.Size, Rectangle.Top + StatConstituentIcon.Size, StatConstituentIcon.Size, StatConstituentIcon.Size);
            g.SpriteBatch.Draw(minusTex, minusRect, Color.White);

            base.SBDraw(g);
        }

        /* -------- Private Methods --------- */
        private float GetValuePerIcon(int numIcons, List<StatConstituent> constituents)
        {
            float total = 0f;
            foreach (StatConstituent sc in constituents)
                total += Mathf.Abs(sc.Value);

            float valuePerIcon = total / (float)numIcons;
            valuePerIcon = Mathf.ClampLower(valuePerIcon, CalibrationManager.UICalibration.Data.minValuePerConstituentIcon);
            valuePerIcon *= VALUE_PER_ICON_MULTIPLIER;

            return valuePerIcon;
        }
    }
}
