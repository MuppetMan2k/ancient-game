﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandTerritoryResourceRow : UIEntity
    {
        /* -------- Private Fields -------- */
        private int resourceNameWidth;
        private int columnWidth;
        // Assets
        private Texture2D separatorTex;
        // Constants
        private const float QUANTITY_PER_ICON_MULTIPLIER = 1.1f;
        private const int SEPARATOR_WIDTH = 2;

        /* -------- Constructors -------- */
        public LandTerritoryResourceRow(int inWidth, int inResourceNameWidth, int inColumnWidth, int inMaxNumIcons, eUIFocus inFocus, float quantityPerIcon, Resource resource, ResourceProduction resourceProduction, ResourcePresence resourcePresense, UIStyle style, ContentManager content)
            : base(new Point(), eUIAnchor.TOP_LEFT, inWidth, ResourceIcon.Size, inFocus, null)
        {
            separatorTex = AssetLoader.LoadTexture(style.LandTerritoryResourcesSeparatorTextureID, content);
            resourceNameWidth = inResourceNameWidth;
            columnWidth = inColumnWidth;

            Text resourceNameText = new Text(new Point(), eUIAnchor.CENTER_LEFT, ColoredTextSegment.GetSegmentsForSingleText(resource.GetName(), style.WindowTextColor), style.LandTerritoryResourceNameFontID, eTextAlignment.CENTER, 0, inResourceNameWidth, 1, eUIFocus.WINDOW, null, content);
            SetChild(resourceNameText);

            float productionQuantity = resourceProduction.GetResourceQuantity(resource);
            float consumptionQuantity = resourcePresense.GetResourceQuantity(resource);

            // Assume that a land territory will not simultaneously import and export a resource. TODO - ensure this remains true as further systems are developed.
            float localQuantity;
            float exportQuantity;
            float importQuantity;
            if (productionQuantity == consumptionQuantity)
            {
                localQuantity = productionQuantity;
                exportQuantity = 0f;
                importQuantity = 0f;
            }
            else
            {
                if (productionQuantity > consumptionQuantity)
                {
                    localQuantity = consumptionQuantity;
                    exportQuantity = productionQuantity - consumptionQuantity;
                    importQuantity = 0f;
                }
                else
                {
                    localQuantity = productionQuantity;
                    exportQuantity = 0f;
                    importQuantity = consumptionQuantity - productionQuantity;
                }
            }

            // Production
            Point point = new Point(inResourceNameWidth, 0);
            int numLocalProductionIcons = Mathf.Ceiling(localQuantity / quantityPerIcon);
            int numIconsLeft = inMaxNumIcons;
            for (int i = 0; i < numLocalProductionIcons; i++)
            {
                ResourceIcon icon = new ResourceIcon(point, eUIAnchor.TOP_LEFT, eUIFocus.WINDOW, resource, localQuantity, eResourceIconMode.LOCAL_PRODUCTION_CONSUMPTION, style, content);
                SetChild(icon);
                point.X += ResourceIcon.Size;
                numIconsLeft--;
                if (numIconsLeft <= 0)
                    break;
            }
            if (numIconsLeft > 0)
            {
                int numExportIcons = Mathf.Ceiling(exportQuantity / quantityPerIcon);
                for (int i = 0; i < numExportIcons; i++)
                {
                    ResourceIcon icon = new ResourceIcon(point, eUIAnchor.TOP_LEFT, eUIFocus.WINDOW, resource, exportQuantity, eResourceIconMode.EXPORTED, style, content);
                    SetChild(icon);
                    point.X += ResourceIcon.Size;
                    numIconsLeft--;
                    if (numIconsLeft <= 0)
                        break;
                }
            }

            // Consumption
            point = new Point(inResourceNameWidth + inColumnWidth, 0);
            numIconsLeft = inMaxNumIcons;
            for (int i = 0; i < numLocalProductionIcons; i++)
            {
                ResourceIcon icon = new ResourceIcon(point, eUIAnchor.TOP_LEFT, eUIFocus.WINDOW, resource, localQuantity, eResourceIconMode.LOCAL_PRODUCTION_CONSUMPTION, style, content);
                SetChild(icon);
                point.X += ResourceIcon.Size;
                numIconsLeft--;
                if (numIconsLeft <= 0)
                    break;
            }
            if (numIconsLeft > 0)
            {
                int numImportIcons = Mathf.Ceiling(importQuantity / quantityPerIcon);
                for (int i = 0; i < numImportIcons; i++)
                {
                    ResourceIcon icon = new ResourceIcon(point, eUIAnchor.TOP_LEFT, eUIFocus.WINDOW, resource, importQuantity, eResourceIconMode.IMPORTED, style, content);
                    SetChild(icon);
                    point.X += ResourceIcon.Size;
                    numIconsLeft--;
                    if (numIconsLeft <= 0)
                        break;
                }
            }
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            Rectangle nameSeparatorRect = new Rectangle(Rectangle.Left + resourceNameWidth - SEPARATOR_WIDTH / 2, Rectangle.Top, SEPARATOR_WIDTH, Rectangle.Height);
            Rectangle columnSeparatorRect = new Rectangle(Rectangle.Left + resourceNameWidth + columnWidth - SEPARATOR_WIDTH / 2, Rectangle.Top, SEPARATOR_WIDTH, Rectangle.Height);

            g.SpriteBatch.Draw(separatorTex, nameSeparatorRect, Color.White);
            g.SpriteBatch.Draw(separatorTex, columnSeparatorRect, Color.White);

            base.SBDraw(g);
        }

        /* -------- Static Methods -------- */
        public static float CalculateResourceQuantityPerIcon(ResourceProduction resourceProduction, ResourcePresence resourcePresense, int maxNumIcons)
        {
            float maxQuantity = 0f;
            foreach (SingleResourcePresence srp in resourceProduction.ResourceProductions)
                maxQuantity = Mathf.Max(maxQuantity, srp.Quantity);
            foreach (SingleResourcePresence srp in resourcePresense.ResourcePresences)
                maxQuantity = Mathf.Max(maxQuantity, srp.Quantity);

            float quantityPerIcon = maxQuantity / (float)maxNumIcons;
            quantityPerIcon = Mathf.ClampLower(quantityPerIcon, CalibrationManager.UICalibration.Data.minQuantityPerResourceIcon);

            quantityPerIcon *= QUANTITY_PER_ICON_MULTIPLIER;

            return quantityPerIcon;
        }
    }
}
