﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class StatConstituentIcon : UIEntity
    {
        /* -------- Properties -------- */
        public static int Size { get { return 20; } }

        /* -------- Private Fields -------- */
        // Assets
        private Texture2D backgroundTex;
        private Texture2D iconTex;

        /* -------- Constructors -------- */
        public StatConstituentIcon(Point inPosition, eUIAnchor inAnchor, eUIFocus inFocus, UIStyle style, StatConstituent sc, ContentManager content)
            : base(inPosition, inAnchor, Size, Size, inFocus, GetTooltipInfo(sc, style))
        {
            backgroundTex = AssetLoader.LoadTexture(GetBackgroundTextureID(sc, style), content);
            iconTex = AssetLoader.LoadTexture(sc.GetIconTextureID(), content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(backgroundTex, Rectangle, Color.White);
            g.SpriteBatch.Draw(iconTex, Rectangle, Color.White);

            base.SBDraw(g);
        }

        /* -------- Private Methods --------- */
        private string GetBackgroundTextureID(StatConstituent sc, UIStyle style)
        {
            if (StatConstituent.GetBias(sc.Type) == eStatConstituentBias.POSITIVE)
                return style.LandTerritoryConstituentPositiveBackgroundTextureID;
            else
                return style.LandTerritoryConstituentNegativeBackgroundTextureID;
        }

        /* -------- Static Methods -------- */
        private static TooltipInfo GetTooltipInfo(StatConstituent sc, UIStyle style)
        {
            Color valColor = style.GetUIAttitudeColor(StatConstituent.GetUIAttitude(sc.Type));
            string valString = UIUtilities.GetFloatDisplayString(sc.Value);
            string name = StatConstituent.GetName(sc.Type);

            return TooltipInfo.CreateFloatValTooltipInfo(name, valString, valColor);
        }
    }
}
