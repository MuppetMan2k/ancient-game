﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandTerritoryWealthSourceRow : UIEntity
    {
        /* -------- Private Fields -------- */
        // Constants
        private const float VALUE_PER_ICON_MULTIPLIER = 1.1f;

        /* -------- Constructors -------- */
        public LandTerritoryWealthSourceRow(int inWidth, int inWealthSourceNameWidth, int inMaxNumIcons, eUIFocus inFocus, float valuePerIcon, WealthSource wealthSource, UIStyle style, ContentManager content)
            : base(new Point(), eUIAnchor.TOP_LEFT, inWidth, WealthSourceIcon.Size, inFocus, null)
        {
            Text wealthSourceNameText = new Text(new Point(), eUIAnchor.CENTER_LEFT, ColoredTextSegment.GetSegmentsForSingleText(WealthSource.GetWealthSourceName(wealthSource.Type), style.WindowTextColor), style.LandTerritoryWealthSourceNameFontID, eTextAlignment.CENTER, 0, inWealthSourceNameWidth, 1, eUIFocus.WINDOW, null, content);
            SetChild(wealthSourceNameText);

            Point point = new Point(inWealthSourceNameWidth, 0);
            int numIconsLeft = inMaxNumIcons;

            // Taxed wealth
            if (!UIUtilities.FloatIsApproxZero(wealthSource.TaxedValue))
            {
                int numTaxedIcons = Mathf.Ceiling(wealthSource.TaxedValue / valuePerIcon);
                for (int i = 0; i < numTaxedIcons; i++)
                {
                    WealthSourceIcon icon = new WealthSourceIcon(point, eUIAnchor.TOP_LEFT, eUIFocus.WINDOW, wealthSource, eWealthIconType.TAXED, style, content);
                    SetChild(icon);
                    point.X += WealthSourceIcon.Size;
                    numIconsLeft--;
                    if (numIconsLeft <= 0)
                        break;
                }
            }

            // Untaxed wealth
            if (!UIUtilities.FloatIsApproxZero(wealthSource.UntaxedValue) && numIconsLeft > 0)
            {
                int numUntaxedIcons = Mathf.Ceiling(wealthSource.UntaxedValue / valuePerIcon);
                for (int i = 0; i < numUntaxedIcons; i++)
                {
                    WealthSourceIcon icon = new WealthSourceIcon(point, eUIAnchor.TOP_LEFT, eUIFocus.WINDOW, wealthSource, eWealthIconType.UNTAXED, style, content);
                    SetChild(icon);
                    point.X += WealthSourceIcon.Size;
                    numIconsLeft--;
                    if (numIconsLeft <= 0)
                        break;
                }
            }
        }

        /* -------- Static Methods -------- */
        public static float CalculateWealthSourceValuePerIcon(IncomeSet incomeSet, int maxNumIcons)
        {
            float maxValue = 0f;
            foreach (WealthSource ws in incomeSet.WealthSources)
                maxValue = Mathf.Max(maxValue, ws.Value);

            float valuePerIcon = maxValue / (float)maxNumIcons;
            valuePerIcon = Mathf.ClampLower(valuePerIcon, CalibrationManager.UICalibration.Data.minValuePerWealthSourceIcon);

            valuePerIcon *= VALUE_PER_ICON_MULTIPLIER;

            return valuePerIcon;
        }
    }
}
