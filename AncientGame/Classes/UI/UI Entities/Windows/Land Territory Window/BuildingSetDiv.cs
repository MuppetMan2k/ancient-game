﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class BuildingSetDiv : WindowDiv
    {
        /* -------- Properties -------- */
        public BuildingSlot InspectionBuildingSlot { get; private set; }

        /* -------- Private Fields -------- */
        private bool territoryOwner;
        private int currentBuildingScrollIndex;
        // Reference store
        private BuildingSet buildingSet;
        private LandTerritory landTerritory;
        private GameManagerContainer gameManagers;
        private UIStyle style;
        private GraphicsComponent graphicsComponent;
        private ContentManager content;
        private string title;
        private Window parentWindow;
        // Constants
        private const int BUILDING_PANEL_MAX_SIZE = 80;
        private const int SPACING = 8;

        /* -------- Constructors -------- */
        public BuildingSetDiv(Point inPoint, eUIAnchor inAnchor, int inWidth, int inHeight, eUIFocus inFocus, BuildingSet inBuildingSet, LandTerritory lt, UIStyle inStyle, string inTitle, Window inParentWindow, GameManagerContainer inGameManagers, GraphicsComponent g, ContentManager inContent)
            : base(inPoint, inAnchor, inWidth, inHeight, inFocus, inStyle, inContent)
        {
            buildingSet = inBuildingSet;
            landTerritory = lt;
            gameManagers = inGameManagers;
            style = inStyle;
            graphicsComponent = g;
            content = inContent;
            title = inTitle;
            currentBuildingScrollIndex = 0;
            parentWindow = inParentWindow;

            Refresh(graphicsComponent, false);
        }

        /* -------- Public Methods -------- */
        public override void Refresh(GraphicsComponent g, bool recurse)
        {
            RemoveAllChildren();

            territoryOwner = (landTerritory.Variables.OwningFaction == gameManagers.TurnManager.FactionTurn);

            Point point = new Point(InnerBorder, InnerBorder);
            Text titleText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(title, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, Rectangle.Width - 2 * InnerBorder, 1, Focus, null, content);
            SetChild(titleText);

            if (buildingSet.Type == eBuildingSetType.SETTLEMENT && territoryOwner)
            {
                TooltipInfo manpowerTooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_manpower"));
                IconText manpowerIcon = new IconText(new Point(InnerBorder, InnerBorder), eUIAnchor.TOP_RIGHT, Rectangle.Width - 2 * InnerBorder, Focus, manpowerTooltipInfo, "manpower-icon", UIUtilities.GetIntDisplayString(landTerritory.Variables.Manpower.Manpower), style.WindowTextColor, style.WindowFontID, content);
                SetChild(manpowerIcon);
            }

            point.Y += titleText.Rectangle.Height + SPACING;

            int buildingPanelSize;
            if (territoryOwner)
                buildingPanelSize = Mathf.ClampUpper((Rectangle.Height - titleText.Rectangle.Height * 2 - InnerBorder * 2 - 3 * SPACING) / 2, BUILDING_PANEL_MAX_SIZE);
            else
                buildingPanelSize = Mathf.ClampUpper((Rectangle.Height - titleText.Rectangle.Height - InnerBorder * 2 - SPACING), BUILDING_PANEL_MAX_SIZE);

            if (buildingSet.BuildingSlots.Count == 0)
            {
                point.Y += buildingPanelSize / 2;
                Text noBuildingsText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_no_buildings"), style.WindowTextColor), style.WindowFontID, eTextAlignment.CENTER, 0, Rectangle.Width - 2 * InnerBorder, 1, Focus, null, content);
                SetChild(noBuildingsText);

                base.Refresh(g, recurse);
                return;
            }

            List<UIEntity> buildingSlotPanels = new List<UIEntity>();
            foreach (BuildingSlot bs in buildingSet.BuildingSlots)
                buildingSlotPanels.Add(new BuildingPanel(new Point(), eUIAnchor.TOP_LEFT, buildingPanelSize, Focus, style, bs, eBuildingPanelType.CURRENT_BUILDING, this, content));
            HorizontalScrollPane buildingSlotScrollPane = new HorizontalScrollPane(point, eUIAnchor.TOP_LEFT, Rectangle.Width - 2 * InnerBorder, buildingPanelSize, buildingPanelSize, Focus, buildingSlotPanels, currentBuildingScrollIndex, style, OnCurrentBuildingScroll, graphicsComponent, content);
            SetChild(buildingSlotScrollPane);

            if (!territoryOwner)
            {
                base.Refresh(g, recurse);
                return;
            }

            point.Y += buildingPanelSize + SPACING;

            Text constructionText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_construction"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, Rectangle.Width - 2 * InnerBorder, 1, Focus, null, content);
            SetChild(constructionText);

            point.Y += constructionText.Rectangle.Height + SPACING;

            if (landTerritory.Variables.OccupyingFaction != null)
            {
                point.Y += buildingPanelSize / 2;
                Text noBuildingText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_no_developments_under_occupation"), style.WindowTextColor), style.WindowFontID, eTextAlignment.CENTER, 0, Rectangle.Width - 2 * InnerBorder, 1, Focus, null, content);
                SetChild(noBuildingText);

                base.Refresh(g, recurse);
                return;
            }

            BuildingSlot developiungBuildingSlot = buildingSet.BuildingSlots.Find(x => x.IsUnderDevelopment());
            if (developiungBuildingSlot != null)
                InspectionBuildingSlot = developiungBuildingSlot;
            BuildingPanel inspectionPanel = new BuildingPanel(point, eUIAnchor.TOP_LEFT, buildingPanelSize, Focus, style, InspectionBuildingSlot, eBuildingPanelType.DEVELOPING_BUILDING, this, content);
            SetChild(inspectionPanel);

            if (InspectionBuildingSlot == null)
            {
                base.Refresh(g, recurse);
                return;
            }

            point.X += buildingPanelSize + SPACING;

            if (InspectionBuildingSlot.IsUnderDevelopment())
            {
                CancelBuildingDevelopmentPanel cancelPanel = new CancelBuildingDevelopmentPanel(point, eUIAnchor.TOP_LEFT, buildingPanelSize, Focus, InspectionBuildingSlot, this, style, content);
                SetChild(cancelPanel);

                base.Refresh(g, recurse);
                return;
            }

            if (InspectionBuildingSlot.CurrentBuilding != null)
            {
                DemolishBuildingPanel demolishPanel = new DemolishBuildingPanel(point, eUIAnchor.TOP_LEFT, buildingPanelSize, Focus, this, style, content);
                SetChild(demolishPanel);
                point.X += buildingPanelSize + SPACING;

                if (InspectionBuildingSlot.CurrentBuilding.DamageLevel != eBuildingDamageLevel.NO_DAMAGE)
                {
                    RepairBuildingPanel repairPanel = new RepairBuildingPanel(point, eUIAnchor.TOP_LEFT, buildingPanelSize, Focus, InspectionBuildingSlot, this, landTerritory, gameManagers, style, content);
                    SetChild(repairPanel);

                    base.Refresh(g, recurse);
                    return;
                }
            }

            List<BuildingData> availableBuildingDatas = gameManagers.BuildingManager.GetAvailableBuildingDatas(InspectionBuildingSlot, landTerritory, gameManagers);
            if (availableBuildingDatas.Count > 0)
            {
                List<UIEntity> scrollBuildingDataPanels = new List<UIEntity>();
                foreach (BuildingData bd in availableBuildingDatas)
                    scrollBuildingDataPanels.Add(new BuildingDataPanel(new Point(), eUIAnchor.TOP_LEFT, buildingPanelSize, Focus, bd, this, landTerritory, style, gameManagers, content));
                HorizontalScrollPane availableBuildingScrollPane = new HorizontalScrollPane(point, eUIAnchor.TOP_LEFT, Rectangle.Width - InnerBorder - point.X, buildingPanelSize, buildingPanelSize, Focus, scrollBuildingDataPanels, 0, style, null, graphicsComponent, content);
                SetChild(availableBuildingScrollPane);
            }

            base.Refresh(g, recurse);
        }

        public void OnCurrentBuildingPanelPressed(BuildingSlot buildingSlot)
        {
            if (!territoryOwner)
                return;

            if (InspectionBuildingSlot != null && InspectionBuildingSlot.IsUnderDevelopment())
                return;

            InspectionBuildingSlot = buildingSlot;
            parentWindow.Refresh(graphicsComponent, true);
        }
        public void OnCurrentDevelopmentCancelled()
        {
            if (!territoryOwner)
                return;

            InspectionBuildingSlot.CancelDevelopment();
            parentWindow.Refresh(graphicsComponent, true);
        }
        public void OnCurrentBuildingDemoltionStarted()
        {
            if (!territoryOwner)
                return;

            InspectionBuildingSlot.CurrentBuilding.StartDemolition(landTerritory, gameManagers);
            parentWindow.Refresh(graphicsComponent, true);
        }
        public void OnCurrentBuildingRepairStarted()
        {
            if (!territoryOwner)
                return;

            InspectionBuildingSlot.CurrentBuilding.StartRepair(landTerritory, gameManagers);
            parentWindow.Refresh(graphicsComponent, true);
        }
        public void OnConstructionStarted(BuildingData data)
        {
            if (!territoryOwner)
                return;

            InspectionBuildingSlot.StartConstruction(data, landTerritory, gameManagers);
            parentWindow.Refresh(graphicsComponent, true);
        }

        /* -------- Private Methods --------- */
        private void OnCurrentBuildingScroll(int newScrollIndex)
        {
            currentBuildingScrollIndex = newScrollIndex;
        }
    }
}
