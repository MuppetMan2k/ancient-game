﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandTerritoryStatisticPrivateWealthStateEntry : LandTerritoryStatisticPlainEntry
    {
        /* -------- Constructors -------- */
        public LandTerritoryStatisticPrivateWealthStateEntry(int inWidth, eUIFocus inFocus, UIStyle style, PrivateWealthState privateWealthState, GameManagerContainer gameManagers, ContentManager content)
            : base(inWidth, inFocus)
        {
            Point point = new Point(THICK_INNER_BORDER, THICK_INNER_BORDER);

            string privateWealthString = Localization.Localize("text_private_wealth");
            Text privateWealthText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(privateWealthString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, inWidth / 2 - THICK_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            SetChild(privateWealthText);

            point.Y += privateWealthText.Rectangle.Height + SPACING;

            TooltipInfo privateWealthTooltipInfo = privateWealthState.GetPrivateWealthTooltipInfo(gameManagers);

            string privateWealthNumString = UIUtilities.GetFloatDisplayString(privateWealthState.PrivateWealth);
            IconText privateWealthIcon = new IconText(point, eUIAnchor.TOP_LEFT, inWidth / 2 - point.X, inFocus, privateWealthTooltipInfo, "wealth-icon", privateWealthNumString, style.WindowTextColor, style.WindowFontID, content);
            SetChild(privateWealthIcon);
        }
    }
}
