﻿using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandTerritoryImportRow : UIEntity
    {
        /* -------- Private Fields -------- */
        // Constants
        private const int EMBLEM_SPACING = 3;

        /* -------- Constructors -------- */
        public LandTerritoryImportRow(int inWidth, eUIFocus inFocus, int inNameWidth, int inValueWidth, int inPointsWidth, TradePath tradePath, UIStyle style, ContentManager content)
            : base(new Point(), eUIAnchor.TOP_LEFT, inWidth, ResourceIcon.Size, inFocus, null)
        {
            int resourcesWidth = inWidth - inNameWidth - inPointsWidth - inValueWidth;
            int maxNumResourceIcons = resourcesWidth / ResourceIcon.Size;

            Point point = new Point();
            LandTerritory exporterLandTerritory = tradePath.ExportLandTerritory;

            // Name
            point = new Point(EMBLEM_SPACING, 0);
            Faction exporterFaction = exporterLandTerritory.Variables.OwningFaction;
            TooltipInfo emblemTooltip = TooltipInfo.CreateStaticTextTooltipInfo(exporterFaction.GetDisplayName());
            EmblemIcon emblemIcon = new EmblemIcon(point, eUIAnchor.TOP_LEFT, ResourceIcon.Size, eUIFocus.WINDOW, emblemTooltip, exporterFaction, content);
            SetChild(emblemIcon);
            point = new Point(emblemIcon.Rectangle.Width + 2 * EMBLEM_SPACING, 0);
            Text landTerritoryNameText = new Text(point, eUIAnchor.CENTER_LEFT, ColoredTextSegment.GetSegmentsForSingleText(exporterLandTerritory.GetNameString(), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.LEFT, 0, inNameWidth - emblemIcon.Rectangle.Width - 2 * EMBLEM_SPACING, 1, eUIFocus.WINDOW, null, content);
            SetChild(landTerritoryNameText);

            // Resources
            point = new Point(inNameWidth, 0);
            IOrderedEnumerable<SingleResourcePresence> orderedResources = tradePath.TradedResources.OrderByDescending(x => x.Quantity);
            int numResources = Mathf.Min(maxNumResourceIcons, tradePath.TradedResources.Count);
            int numResourcesMade = 0;
            foreach (SingleResourcePresence srp in orderedResources)
            {
                if (numResourcesMade >= numResources)
                    break;

                ResourceIcon resourceIcon = new ResourceIcon(point, eUIAnchor.TOP_LEFT, eUIFocus.WINDOW, srp.Resource, srp.Quantity, eResourceIconMode.PLAIN, style, content);
                SetChild(resourceIcon);
                point.X += ResourceIcon.Size;
                numResourcesMade++;
            }

            // Value
            point = new Point(inNameWidth + resourcesWidth, 0);
            string valueString = UIUtilities.GetFloatDisplayString(tradePath.GetTradedResourcesValue());
            TooltipInfo valueTooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_trade_value"));
            Text valueText = new Text(point, eUIAnchor.CENTER_LEFT, ColoredTextSegment.GetSegmentsForSingleText(valueString, style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, inValueWidth, 1, inFocus, valueTooltipInfo, content);
            SetChild(valueText);

            // Points
            point = new Point(inNameWidth + resourcesWidth + inValueWidth, 0);
            TooltipInfo pointsTooltip = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_trade_movement_points_tooltip"));
            Text pointsText = new Text(point, eUIAnchor.CENTER_LEFT, ColoredTextSegment.GetSegmentsForSingleText(UIUtilities.GetIntDisplayString(tradePath.MovementPointCost), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, inPointsWidth, 1, eUIFocus.WINDOW, pointsTooltip, content);
            SetChild(pointsText);
        }
    }
}
