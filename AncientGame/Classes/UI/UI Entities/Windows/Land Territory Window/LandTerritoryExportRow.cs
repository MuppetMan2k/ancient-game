﻿using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandTerritoryExportRow : UIEntity
    {
        /* -------- Private Fields -------- */
        // Constants
        private const int EMBLEM_SPACING = 3;

        /* -------- Constructors -------- */
        public LandTerritoryExportRow(int inWidth, eUIFocus inFocus, int inNameWidth, int inValueWidth, TradePath tradePath, UIStyle style, ContentManager content)
            : base(new Point(), eUIAnchor.TOP_LEFT, inWidth, ResourceIcon.Size, inFocus, null)
        {
            int resourcesWidth = inWidth - inNameWidth - inValueWidth;
            int maxNumResourceIcons = resourcesWidth / ResourceIcon.Size;

            Point point = new Point();
            LandTerritory importerLandTerritory = tradePath.ImportLandTerritory;

            // Name
            point = new Point(EMBLEM_SPACING, 0);
            Faction importFaction = importerLandTerritory.Variables.OwningFaction;
            TooltipInfo emblemTooltip = TooltipInfo.CreateStaticTextTooltipInfo(importFaction.GetDisplayName());
            EmblemIcon emblemIcon = new EmblemIcon(point, eUIAnchor.TOP_LEFT, ResourceIcon.Size, eUIFocus.WINDOW, emblemTooltip, importFaction, content);
            SetChild(emblemIcon);
            point = new Point(emblemIcon.Rectangle.Width + 2 * EMBLEM_SPACING, 0);
            Text landTerritoryNameText = new Text(point, eUIAnchor.CENTER_LEFT, ColoredTextSegment.GetSegmentsForSingleText(importerLandTerritory.GetNameString(), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.LEFT, 0, inNameWidth - emblemIcon.Rectangle.Width - 2 * EMBLEM_SPACING, 1, eUIFocus.WINDOW, null, content);
            SetChild(landTerritoryNameText);

            // Resources
            point = new Point(inNameWidth, 0);
            IOrderedEnumerable<SingleResourcePresence> orderedResources = tradePath.TradedResources.OrderByDescending(x => x.Quantity);
            int numResources = Mathf.Min(maxNumResourceIcons, tradePath.TradedResources.Count);
            int numResourcesMade = 0;
            foreach (SingleResourcePresence srp in orderedResources)
            {
                if (numResourcesMade >= numResources)
                    break;

                ResourceIcon resourceIcon = new ResourceIcon(point, eUIAnchor.TOP_LEFT, eUIFocus.WINDOW, srp.Resource, srp.Quantity, eResourceIconMode.PLAIN, style, content);
                SetChild(resourceIcon);
                point.X += ResourceIcon.Size;
                numResourcesMade++;
            }

            // Value
            point = new Point(inNameWidth + resourcesWidth, 0);
            string valueString = UIUtilities.GetFloatDisplayString(tradePath.GetTradedResourcesValue());
            TooltipInfo valueTooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_trade_value"));
            Text valueText = new Text(point, eUIAnchor.CENTER_LEFT, ColoredTextSegment.GetSegmentsForSingleText(valueString, style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, inValueWidth, 1, inFocus, valueTooltipInfo, content);
            SetChild(valueText);
        }
    }
}
