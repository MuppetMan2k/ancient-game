﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandTerritoryStatisticRackEntry : LandTerritoryStatisticEntry
    {
        /* -------- Private Fields -------- */
        // Constants
        private const int STAT_TOTAL_WIDTH = 45;
        private const int STAT_TOTAL_HEIGHT = 20;

        /* -------- Constructors -------- */
        public LandTerritoryStatisticRackEntry(int inWidth, eUIFocus inFocus, UIStyle style, string title, float total, StatConstituentSet statConstituents, bool showAsPercentage, TooltipInfo totalTooltip, ContentManager content)
            : base(new Point(), eUIAnchor.TOP_LEFT, inWidth, inFocus)
        {
            Point point = new Point(INNER_BORDER, INNER_BORDER);

            Text titleText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(title, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, inWidth - 2 * INNER_BORDER, 1, inFocus, null, content);
            SetChild(titleText);

            point.Y += titleText.Rectangle.Height + SPACING;

            int constituentRackWidth = inWidth - INNER_BORDER * 2 - STAT_TOTAL_WIDTH - SPACING;
            StatConstituentRack constituentRack = new StatConstituentRack(point, eUIAnchor.TOP_LEFT, constituentRackWidth, inFocus, style, statConstituents, content);
            SetChild(constituentRack);

            point.X += constituentRackWidth + SPACING;
            point.Y += constituentRack.Rectangle.Height / 2 - STAT_TOTAL_HEIGHT / 2;

            StatTotal statTotal = new StatTotal(point, eUIAnchor.TOP_LEFT, STAT_TOTAL_WIDTH, STAT_TOTAL_HEIGHT, inFocus, style, totalTooltip, total, showAsPercentage, content);
            SetChild(statTotal);
        }
    }
}
