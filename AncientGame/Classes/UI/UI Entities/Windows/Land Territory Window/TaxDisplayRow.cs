﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class TaxDisplayRow : UIEntity
    {
        /* -------- Properties -------- */
        public static int Height { get { return 24; } }

        /* -------- Constructors -------- */
        public TaxDisplayRow(int inWidth, eUIFocus inFocus, Tax inTax, UIStyle style, ContentManager content)
            : base(new Point(), eUIAnchor.TOP_LEFT, inWidth, Height, inFocus, CreateTooltipInfo(inTax))
        {
            Text text = new Text(new Point(), eUIAnchor.CENTER_CENTER, ColoredTextSegment.GetSegmentsForSingleText(inTax.GetName(), style.WindowTextColor), style.WindowFontID, eTextAlignment.CENTER, 0, inWidth, 11, inFocus, CreateTooltipInfo(inTax), content);
            SetChild(text);
        }

        /* -------- Static Methods -------- */
        private static TooltipInfo CreateTooltipInfo(Tax inTax)
        {
            return TooltipInfo.CreateTaxTooltipInfo(inTax);
        }
    }
}
