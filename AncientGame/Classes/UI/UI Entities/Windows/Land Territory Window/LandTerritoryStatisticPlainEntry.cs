﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class LandTerritoryStatisticPlainEntry : LandTerritoryStatisticEntry
    {
        /* -------- Private Fields -------- */
        // Constants
        protected const int THICK_INNER_BORDER = 20;
        protected const int TEXT_HEIGHT = 16;

        /* -------- Constructors -------- */
        public LandTerritoryStatisticPlainEntry(int inWidth, eUIFocus inFocus)
            : base(new Point(), eUIAnchor.TOP_LEFT, inWidth, inFocus)
        {
        }
    }
}
