﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class StatTotal : UIEntity
    {
        /* -------- Private Fields -------- */
        // Assets
        private Texture2D backgroundTex;
        // Constants
        private const float TEXT_MAX_WIDTH_MULTIPLIER = 0.9f;

        /* -------- Constructors -------- */
        public StatTotal(Point inPosition, eUIAnchor inAnchor, int inWidth, int inHeight, eUIFocus inFocus, UIStyle style, TooltipInfo inTooltipInfo, float total, bool showAsPercentage, ContentManager content)
            : base(inPosition, inAnchor, inWidth, inHeight, inFocus, inTooltipInfo)
        {
            backgroundTex = AssetLoader.LoadTexture(style.LandTerritoryStatTotalBackgroundID, content);

            Color textColor;
            if (UIUtilities.FloatIsApproxZero(total))
                textColor = style.NeutralColor;
            else if (total > 0f)
                textColor = style.PositiveColor;
            else
                textColor = style.NegativeColor;
            string totalString = showAsPercentage ? UIUtilities.GetBiDirectionalPercentageFloatDisplayString(total) : UIUtilities.GetBiDirectionalFloatDisplayString(total);
            FittedText totalText = new FittedText(new Point(), eUIAnchor.CENTER_CENTER, ColoredTextSegment.GetSegmentsForSingleText(totalString, textColor), style.LandTerritoryStatTotalFontID, eTextAlignment.CENTER, Mathf.Round(TEXT_MAX_WIDTH_MULTIPLIER * inWidth), inFocus, inTooltipInfo, content);
            SetChild(totalText);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(backgroundTex, Rectangle, Color.White);

            base.SBDraw(g);
        }
    }
}
