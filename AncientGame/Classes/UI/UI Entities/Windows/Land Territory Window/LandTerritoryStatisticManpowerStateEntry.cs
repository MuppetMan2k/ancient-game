﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandTerritoryStatisticManpowerStateEntry : LandTerritoryStatisticPlainEntry
    {
        /* -------- Private Fields -------- */
        // Reference store
        private UIStyle style;
        private ManpowerState manpowerState;
        private ContentManager content;

        /* -------- Constructors -------- */
        public LandTerritoryStatisticManpowerStateEntry(int inWidth, eUIFocus inFocus, UIStyle inStyle, ManpowerState inManpowerState, GraphicsComponent g, ContentManager inContent)
            : base(inWidth, inFocus)
        {
            style = inStyle;
            manpowerState = inManpowerState;
            content = inContent;

            Refresh(g, false);
        }

        /* -------- Public Methods -------- */
        public override void Refresh(GraphicsComponent g, bool recurse)
        {
            RemoveAllChildren();

            Point point = new Point(THICK_INNER_BORDER, THICK_INNER_BORDER);
            int width = Rectangle.Width;

            string manpowerString = Localization.Localize("text_manpower");
            Text manpowerText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(manpowerString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, width / 2 - THICK_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            SetChild(manpowerText);

            point.Y += manpowerText.Rectangle.Height + SPACING;

            TooltipInfo manpowerTooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_manpower_tooltip"));

            string manpowerNumString = String.Format(Localization.Localize("text_out_of"), UIUtilities.GetIntDisplayString(manpowerState.Manpower), UIUtilities.GetIntDisplayString(manpowerState.MaxManpower));
            IconText manpowerIcon = new IconText(point, eUIAnchor.TOP_LEFT, width / 2, Focus, manpowerTooltipInfo, "manpower-icon", manpowerNumString, style.WindowTextColor, style.WindowFontID, content);
            SetChild(manpowerIcon);

            base.Refresh(g, recurse);
        }
    }
}
