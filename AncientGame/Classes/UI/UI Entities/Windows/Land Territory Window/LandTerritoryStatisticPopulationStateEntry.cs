﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandTerritoryStatisticPopulationStateEntry : LandTerritoryStatisticPlainEntry
    {
        /* -------- Constructors -------- */
        public LandTerritoryStatisticPopulationStateEntry(int inWidth, eUIFocus inFocus, UIStyle style, LandTerritory landTerritory, int supportablePopulation, GameManagerContainer gameManagers, ContentManager content)
            : base(inWidth, inFocus)
        {
            PopulationState populationState = landTerritory.Variables.Population;
            Point point = new Point(THICK_INNER_BORDER, THICK_INNER_BORDER);
            int width = Rectangle.Width;

            string populationString = Localization.Localize("text_population");
            Text populationText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(populationString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, width / 2 - THICK_INNER_BORDER, 1, Focus, null, content);
            SetChild(populationText);

            point.Y += populationText.Rectangle.Height + SPACING;

            TooltipInfo populationTooltip = populationState.GetPopulationTooltipInfo(gameManagers, landTerritory);

            Color populationNumColor = (populationState.Population >= supportablePopulation) ? style.NegativeColor : style.WindowTextColor;
            string populationNumString = String.Format(Localization.Localize("text_out_of"), UIUtilities.GetLargeIntDisplayString(populationState.Population), UIUtilities.GetLargeIntDisplayString(supportablePopulation));
            IconText populationIcon = new IconText(point, eUIAnchor.TOP_LEFT, width - point.X, Focus, populationTooltip, "population-icon", populationNumString, populationNumColor, style.WindowFontID, content);
            SetChild(populationIcon);

            point = new Point(width / 2 + THICK_INNER_BORDER, THICK_INNER_BORDER);

            string slavePopulationString = Localization.Localize("text_slave_population");
            Text slavePopulationText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(slavePopulationString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, width - THICK_INNER_BORDER, 1, Focus, null, content);
            SetChild(slavePopulationText);

            point.Y += slavePopulationText.Rectangle.Height + SPACING;

            TooltipInfo slavePopulationTooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_slave_population_tooltip"));

            string slavePopulationNumString = UIUtilities.GetLargeIntDisplayString(populationState.SlavePopulation);

            IconText slavePopulationIcon = new IconText(point, eUIAnchor.TOP_LEFT, width - point.X, Focus, slavePopulationTooltipInfo, "slave-icon", slavePopulationNumString, style.WindowTextColor, style.WindowFontID, content);
            SetChild(slavePopulationIcon);

            point.Y += slavePopulationIcon.Rectangle.Height;

            string slavePopulationPercentageString = UIUtilities.GetPercentageFloatDisplayString(populationState.SlavePopulationPercentage);
            Text slavePopulationPercentageText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(slavePopulationPercentageString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, width - point.X, 1, Focus, populationState.GetSlavePopulationPercentageTooltipInfo(gameManagers), content);
            SetChild(slavePopulationPercentageText);
        }
    }
}
