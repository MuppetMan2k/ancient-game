﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class WindowManager
    {
        /* -------- Private Fields -------- */
        private GraphicsComponent graphicsComponent;
        private UIComponent uiComponent;
        private ContentManager content;
        private List<Window> windows;
        private GameManagerContainer gameManagers;
        // Constants
        private const int WINDOW_INNER_BORDER = 10;
        private const int STANDARD_SMALL_SPACING = 3;
        private const int STANDARD_SPACING = 8;
        private const int STANDARD_TEXT_HEIGHT = 16;
        private const int BOTTOM_SPACE_FOR_CLOSE_BUTTON = 30;

        /* -------- Constructors -------- */
        public WindowManager(GraphicsComponent inGraphicsComponent, UIComponent inUIComponent, GameManagerContainer inGameManagers, ContentManager inContent)
        {
            graphicsComponent = inGraphicsComponent;
            uiComponent = inUIComponent;
            gameManagers = inGameManagers;
            content = inContent;

            windows = new List<Window>();
        }

        /* -------- Public Methods -------- */
        public void CreateWindow_Faction(Faction faction)
        {
            Window window = new Window(eWindowType.MANAGEMENT, this, uiComponent.Style, content);

            window.AddPage(CreatePage_Faction_Overview(faction, window), uiComponent.Style, "overview-icon", "text_faction_overview", graphicsComponent, content);
            window.AddPage(CreatePage_Faction_Government(faction, window), uiComponent.Style, "government-icon", "text_faction_government", graphicsComponent, content);

            AddWindow(window);
            uiComponent.AddUI(window);
        }
        public void CreateWindow_LandTerritory(LandTerritory landTerritory)
        {
            Faction owningFaction = landTerritory.Variables.OwningFaction;
            LandTerritoryControl ltc = owningFaction.Variables.GetLandTerritoryControl(landTerritory);

            Window window = new Window(eWindowType.MANAGEMENT, this, uiComponent.Style, content);

            window.AddPage(CreatePage_LandTerritory_Overview(landTerritory, window), uiComponent.Style, "overview-icon", "text_land-territory_overview", graphicsComponent, content);
            window.AddPage(CreatePage_LandTerritory_Statistics(landTerritory, window), uiComponent.Style, "statistics-icon", "text_land-territory_statistics", graphicsComponent, content);
            window.AddPage(CreatePage_LandTerritory_Resources(landTerritory, window), uiComponent.Style, "resources-icon", "text_land-territory_resources", graphicsComponent, content);
            window.AddPage(CreatePage_LandTerritory_Income(landTerritory, window), uiComponent.Style, "wealth-icon", "text_land-territory_income", graphicsComponent, content);
            window.AddPage(CreatePage_LandTerritory_Trade(landTerritory, window), uiComponent.Style, "trade-icon", "text_land-territory_trade", graphicsComponent, content);
            window.AddPage(CreatePage_LandTerritory_Buildings(landTerritory, window), uiComponent.Style, "building-icon", "text_land-territory_buildings", graphicsComponent, content);
            window.AddPage(CreatePage_LandTerritory_Garrison(landTerritory, window), uiComponent.Style, "garrison-icon", "text_land-territory_garrison", graphicsComponent, content);

            AddWindow(window);
            uiComponent.AddUI(window);
        }
        public void CreateWindow_LandRegion(LandRegion landRegion)
        {
            Window window = new Window(eWindowType.MANAGEMENT, this, uiComponent.Style, content);

            // DEBUG
            window.AddPage(CreatePage_LandRegion_Overview(landRegion, window), uiComponent.Style, "overview-icon", "text_land-region_overview", graphicsComponent, content);

            AddWindow(window);
            uiComponent.AddUI(window);
        }
        public void CreateWindow_LandProvince(LandProvince landProvince)
        {
            Window window = new Window(eWindowType.MANAGEMENT, this, uiComponent.Style, content);

            // DEBUG
            window.AddPage(CreatePage_LandProvince_Overview(landProvince, window), uiComponent.Style, "overview-icon", "text_land-province_overview", graphicsComponent, content);

            AddWindow(window);
            uiComponent.AddUI(window);
        }
        public void CreateWindow_SeaTerritory(SeaTerritory seaTerritory)
        {
            Window window = new Window(eWindowType.MANAGEMENT, this, uiComponent.Style, content);

            // DEBUG
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            string nameString = Localization.Localize("text_sea-territory");
            Text nameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(nameString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, window.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            window.SetChild(nameText);

            AddWindow(window);
            uiComponent.AddUI(window);
        }
        public void CreateWindow_CoastalSeaTerritory(CoastalSeaTerritory coastalSeaTerritory)
        {
            Window window = new Window(eWindowType.MANAGEMENT, this, uiComponent.Style, content);

            // DEBUG
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            string nameString = Localization.Localize("text_coastal_sea-territory");
            Text nameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(nameString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, window.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            window.SetChild(nameText);

            point.Y += 25;

            string adjCoastsString = Localization.Localize("text_coasts_adjacent_label");
            Text adjCoastsText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(adjCoastsString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, window.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            window.SetChild(adjCoastsText);

            foreach (AdjacentLandTerritory alt in coastalSeaTerritory.AdjacentLandTerritories)
            {
                point.Y += 20;
                
                string adjCoastString = "+ " + alt.LandTerritory.GetNameString();
                Text adjCoastText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(adjCoastString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, window.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
                window.SetChild(adjCoastText);
            }

            AddWindow(window);
            uiComponent.AddUI(window);
        }
        public void CreateWindow_SeaRegion(SeaRegion seaRegion)
        {
            Window window = new Window(eWindowType.MANAGEMENT, this, uiComponent.Style, content);

            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            AddWindow(window);
            uiComponent.AddUI(window);
        }
        public void CreateWindow_Force(Force force)
        {
            Window window = new Window(eWindowType.MANAGEMENT, this, uiComponent.Style, content);

            // TODO - create pages

            AddWindow(window);
            uiComponent.AddUI(window);
        }
        public void CreateWindow_River(River river)
        {
            Window window = new Window(eWindowType.INFORMATION, this, uiComponent.Style, content);

            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            string nameString = river.GetNameString();
            Text nameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(nameString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, window.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            window.SetChild(nameText);

            AddWindow(window);
            uiComponent.AddUI(window);
        }
        public void CreateWindow_Lake(Lake lake)
        {
            Window window = new Window(eWindowType.INFORMATION, this, uiComponent.Style, content);

            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            string nameString = lake.GetNameString();
            Text nameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(nameString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, window.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            window.SetChild(nameText);

            AddWindow(window);
            uiComponent.AddUI(window);
        }
        public void CreateWindow_SmallIsland(SmallIsland smallIsland)
        {
            Window window = new Window(eWindowType.INFORMATION, this, uiComponent.Style, content);

            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            string nameString = smallIsland.GetNameString();
            Text nameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(nameString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, window.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            window.SetChild(nameText);

            AddWindow(window);
            uiComponent.AddUI(window);
        }

        public void RemoveWindow(Window window)
        {
            windows.Remove(window);
            uiComponent.RemoveUI(window);
        }

        /* -------- Private Methods --------- */
        private void AddWindow(Window window)
        {
            List<Window> windowsToRemove = new List<Window>();

            foreach (Window w in windows)
                if (NewWindowCausesCurrentWindowToClose(window.Type, w.Type))
                    windowsToRemove.Add(w);

            foreach (Window windowToRemove in windowsToRemove)
                RemoveWindow(windowToRemove);

            windows.Add(window);
        }
        private bool NewWindowCausesCurrentWindowToClose(eWindowType newWindowType, eWindowType currWindowType)
        {
            if (newWindowType == eWindowType.MESSAGE)
                return true;

            return (newWindowType == currWindowType);
        }

        // Faction window
        private Page CreatePage_Faction_Overview(Faction faction, Window window)
        {
            Page page = new Page(window);

            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            int standardHeight = 131;
            Standard standard = new Standard(point, eUIAnchor.TOP_LEFT, standardHeight, eUIFocus.WINDOW, null, faction, false, this);
            page.SetChild(standard);

            int textXOffset = 10;
            Point textPos = new Point(point.X + standard.Rectangle.Width + textXOffset, point.Y);
            Text nameText = new Text(textPos, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(faction.GetDisplayName(), uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - textPos.X - WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(nameText);

            textPos.Y += nameText.Rectangle.Height + STANDARD_SPACING;

            string sizeString = String.Format(Localization.Localize("text_faction-size_label"), faction.GetSizeName());
            Text sizeText = new Text(textPos, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(sizeString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - textPos.X - WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(sizeText);

            textPos.Y += sizeText.Rectangle.Height + STANDARD_SPACING;

            string ratingString = String.Format(Localization.Localize("text_faction-special-rating_label"), faction.GetSpecialRatingName());
            Text ratingText = new Text(textPos, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(ratingString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - textPos.X - WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(ratingText);

            return page;
        }
        private Page CreatePage_Faction_Government(Faction faction, Window window)
        {
            Page page = new Page(window);

            // DEBUG
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            string governmentGroupString = String.Format(Localization.Localize("text_government-group_label"), faction.Variables.Government.GetGroupName());
            Text governmentGroupText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(governmentGroupString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - point.X - WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(governmentGroupText);

            point.Y += 25;

            string governmentTraitsString = Localization.Localize("text_government-traits_name");
            Text governmentTraitsText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(governmentTraitsString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - point.X - WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(governmentTraitsText);

            point.Y += governmentTraitsText.Rectangle.Height + STANDARD_SPACING;

            foreach (GovernmentTrait trait in faction.Variables.Government.Traits)
            {
                string governmentTraitString = "+ " + Localization.Localize("text_government-trait_" + trait.ID + "_name");
                Text governmentTraitText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(governmentTraitString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - point.X - WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
                page.SetChild(governmentTraitText);

                point.Y += governmentTraitText.Rectangle.Height + STANDARD_SPACING;
            }

            return page;
        }
        // Land territory window
        private Page CreatePage_LandTerritory_Overview(LandTerritory lt, Window window)
        {
            Page page = new Page(window);

            UIStyle style = uiComponent.Style;
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);
            
            // Names and parents

            string nameString = lt.GetNameString();
            Text nameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(nameString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(nameText);

            point.Y += nameText.Rectangle.Height + STANDARD_SMALL_SPACING;

            ParentLandRegionText parentLandRegText = new ParentLandRegionText(point, eUIAnchor.TOP_LEFT, style.WindowFontID, style.WindowTextColor, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, eUIFocus.WINDOW, content, lt.ParentLandRegion, this);
            page.SetChild(parentLandRegText);

            point.Y += parentLandRegText.Rectangle.Height + STANDARD_SMALL_SPACING;

            ParentLandProvinceText parentLandProvText = new ParentLandProvinceText(point, eUIAnchor.TOP_LEFT, style.WindowFontID, style.WindowTextColor, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, eUIFocus.WINDOW, content, lt.ParentLandProvince, this);
            page.SetChild(parentLandProvText);

            point.Y += parentLandProvText.Rectangle.Height;

            // Panels

            int panelSize = 80;
            int panelXNum = 4;
            int panelSpacing = (page.Rectangle.Width - panelXNum * panelSize - 2 * WINDOW_INNER_BORDER) / (panelXNum + 1);

            point.Y += STANDARD_SPACING;
            point.X = WINDOW_INNER_BORDER + panelSpacing;

            TerrainTypePanel terrainTypePanel = new TerrainTypePanel(point, eUIAnchor.TOP_LEFT, panelSize, eUIFocus.WINDOW, style.PanelBackgroundTextureID, style.PanelBorderTextureID, lt.TerrainType, lt.GetTerrainTypeModifierSet(gameManagers), content);
            page.SetChild(terrainTypePanel);

            point.X += panelSize + panelSpacing;

            ClimateTypePanel climateTypePanel = new ClimateTypePanel(point, eUIAnchor.TOP_LEFT, panelSize, eUIFocus.WINDOW, style.PanelBackgroundTextureID, style.PanelBorderTextureID, lt.ParentLandProvince.ClimateType, lt.ParentLandProvince.GetClimateModifierSet(gameManagers), content);
            page.SetChild(climateTypePanel);

            point.X += panelSize + panelSpacing;

            FertilityLevelPanel fertilityLevelPanel = new FertilityLevelPanel(point, eUIAnchor.TOP_LEFT, panelSize, eUIFocus.WINDOW, lt.ParentLandRegion.FertilityLevel, style, lt.ParentLandRegion.GetFertilityModifierSet(gameManagers), content);
            page.SetChild(fertilityLevelPanel);

            point.X += panelSize + panelSpacing;

            HarvestLevelPanel harvestLevelPanel = new HarvestLevelPanel(point, eUIAnchor.TOP_LEFT, panelSize, eUIFocus.WINDOW, lt.ParentLandProvince.HarvestState, style, lt.ParentLandProvince.HarvestState.GetModifierSet(gameManagers), content);
            page.SetChild(harvestLevelPanel);

            // Ownership div

            int divPanelSize = 80;
            int divPanelNum = 3;
            int divPanelSpacing = ((page.Rectangle.Width - 2 * WINDOW_INNER_BORDER - 2 * WindowDiv.InnerBorder) - divPanelNum * divPanelSize) / (divPanelNum + 1);

            point.Y += panelSize + STANDARD_SPACING;
            point.X = WINDOW_INNER_BORDER;

            Faction owningFaction = lt.Variables.OwningFaction;
            Faction occupyingFaction = lt.Variables.OccupyingFaction;
            bool isCapitol = (owningFaction.Variables.CapitolLandTerritory == lt);
            LandTerritoryControl ownerControl = owningFaction.GetLandTerritoryControl(lt);
            eLandTerritoryControlLevel ownerControlLevel = owningFaction.GetLandTerritoryControlLevel(lt);
            bool ownershipCanBeUpgraded = (ownerControlLevel == eLandTerritoryControlLevel.MILITARY_OWNERSHIP || ownerControlLevel == eLandTerritoryControlLevel.ANNEXED_OWNERSHIP);

            bool showOwnershipUpgrades = (owningFaction == gameManagers.TurnManager.FactionTurn) && (!isCapitol || ownershipCanBeUpgraded) && (occupyingFaction == null);
            int ownershipDivHeight = showOwnershipUpgrades ? (2 * WindowDiv.InnerBorder + STANDARD_TEXT_HEIGHT + 2 * STANDARD_SPACING + 2 * divPanelSize) : (2 * WindowDiv.InnerBorder + STANDARD_TEXT_HEIGHT + STANDARD_SPACING + divPanelSize);
            WindowDiv ownershipDiv = new WindowDiv(point, eUIAnchor.TOP_LEFT, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, ownershipDivHeight, eUIFocus.WINDOW, style, content);

            Point divPoint = new Point(WindowDiv.InnerBorder, WindowDiv.InnerBorder);

            Text ownershipText = new Text(divPoint, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_ownership"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, ownershipDiv.Rectangle.Width - 2 * WindowDiv.InnerBorder, 1, eUIFocus.WINDOW, null, content);
            ownershipDiv.SetChild(ownershipText);

            divPoint.Y += STANDARD_TEXT_HEIGHT + STANDARD_SPACING;
            divPoint.X += divPanelSpacing;

            Standard ownerStandard = new Standard(divPoint, eUIAnchor.TOP_LEFT, divPanelSize, eUIFocus.WINDOW, null, owningFaction, true, this);
            ownershipDiv.SetChild(ownerStandard);

            divPoint.X += divPanelSize + divPanelSpacing;

            SettlementTypePanel settlementTypePanel = new SettlementTypePanel(divPoint, eUIAnchor.TOP_LEFT, divPanelSize, eUIFocus.WINDOW, isCapitol, uiComponent.Style, lt.GetCapitolModifierSet(gameManagers), content);
            ownershipDiv.SetChild(settlementTypePanel);

            divPoint.X += divPanelSize + divPanelSpacing;

            OwnershipTypePanel ownershipTypePanel = new OwnershipTypePanel(divPoint, eUIAnchor.TOP_LEFT, divPanelSize, eUIFocus.WINDOW, ownerControlLevel, style, lt.GetOwnershipModifierSet(gameManagers), content);
            ownershipDiv.SetChild(ownershipTypePanel);

            if (showOwnershipUpgrades)
            {
                divPoint.Y += divPanelSize + STANDARD_SPACING;
                divPoint.X = WindowDiv.InnerBorder;

                string upgradesString = Localization.Localize("text_upgrades_label");
                Text upgradesText = new Text(divPoint, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(upgradesString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, divPanelSize + 2 * divPanelSpacing, 3, eUIFocus.WINDOW, null, content);
                ownershipDiv.SetChild(upgradesText);

                divPoint.X += divPanelSize + 2 * divPanelSpacing;

                if (!isCapitol)
                {
                    CapitolUpgradePanel capitolUpgradePanel = new CapitolUpgradePanel(divPoint, eUIAnchor.TOP_LEFT, divPanelSize, eUIFocus.WINDOW, style, content);
                    ownershipDiv.SetChild(capitolUpgradePanel);
                }

                divPoint.X += divPanelSize + divPanelSpacing;

                if (ownershipCanBeUpgraded)
                {
                    OwnershipUpgradePanel ownershipUpgradePanel = new OwnershipUpgradePanel(divPoint, eUIAnchor.TOP_LEFT, divPanelSize, eUIFocus.WINDOW, ownerControl, style, content);
                    ownershipDiv.SetChild(ownershipUpgradePanel);
                }
            }

            page.SetChild(ownershipDiv);

            point.Y += ownershipDivHeight + STANDARD_SPACING;

            // Occupation div

            if (occupyingFaction != null)
            {
                bool showOccupationUpgrades = (occupyingFaction == gameManagers.TurnManager.FactionTurn);
                int occupationDivHeight = showOccupationUpgrades ? (2 * WindowDiv.InnerBorder + STANDARD_TEXT_HEIGHT + 2 * STANDARD_SPACING + 2 * divPanelSize) : (2 * WindowDiv.InnerBorder + STANDARD_TEXT_HEIGHT + STANDARD_SPACING + divPanelSize);
                WindowDiv occupationDiv = new WindowDiv(point, eUIAnchor.TOP_LEFT, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, occupationDivHeight, eUIFocus.WINDOW, style, content);

                divPoint = new Point(WindowDiv.InnerBorder, WindowDiv.InnerBorder);

                Text occupationText = new Text(divPoint, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_occupation"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, occupationDiv.Rectangle.Width - 2 * WindowDiv.InnerBorder, 1, eUIFocus.WINDOW, null, content);
                occupationDiv.SetChild(occupationText);

                divPoint.Y += STANDARD_TEXT_HEIGHT + STANDARD_SPACING;

                Standard occupierStandard = new Standard(divPoint, eUIAnchor.TOP_LEFT, divPanelSize, eUIFocus.WINDOW, null, occupyingFaction, true, this);
                occupationDiv.SetChild(occupierStandard);

                divPoint.X += divPanelSize + STANDARD_SPACING;

                string occupiedString = String.Format(Localization.Localize("text_occupied_by"), occupyingFaction.GetDisplayName());
                Text occupiedByText = new Text(divPoint, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(occupiedString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, style.WindowLineSpacing, occupationDiv.Rectangle.Width - WindowDiv.InnerBorder - divPoint.X, 3, eUIFocus.WINDOW, null, content);
                occupationDiv.SetChild(occupiedByText);

                if (showOccupationUpgrades)
                {
                    divPoint.Y += divPanelSize + STANDARD_SPACING;
                    divPoint.X = WindowDiv.InnerBorder;

                    OccupationUpgradePanel occupationUpgradePanel = new OccupationUpgradePanel(divPoint, eUIAnchor.TOP_LEFT, divPanelSize, eUIFocus.WINDOW, lt, occupyingFaction, style, content);
                    occupationDiv.SetChild(occupationUpgradePanel);

                    divPoint.X += divPanelSize + STANDARD_SPACING;

                    string occupationOptionsString = Localization.Localize("text_occupation_options");
                    Text occupationOptionsText = new Text(divPoint, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(occupationOptionsString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, style.WindowLineSpacing, occupationDiv.Rectangle.Width - WindowDiv.InnerBorder - divPoint.X, 3, eUIFocus.WINDOW, null, content);
                    occupationDiv.SetChild(occupationOptionsText);
                }

                page.SetChild(occupationDiv);
            }

            return page;
        }
        private Page CreatePage_LandTerritory_Statistics(LandTerritory lt, Window window)
        {
            Page page = new Page(window);

            UIStyle style = uiComponent.Style;
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            int statWidth = page.Rectangle.Width - 2 * WINDOW_INNER_BORDER;
            List<UIEntity> statEntries = new List<UIEntity>();

            LandTerritoryVariables v = lt.Variables;

            LandTerritoryStatisticRackEntry plebHappiness = new LandTerritoryStatisticRackEntry(statWidth, eUIFocus.WINDOW, style, Localization.Localize("text_pleb_happiness_stat"), v.Happiness.PlebHappiness, v.Happiness.PlebHappinessConstituents, false, v.Happiness.GetPlebHappinessTotalTooltipInfo(gameManagers), content);
            statEntries.Add(plebHappiness);
            LandTerritoryStatisticRackEntry nobilityHappiness = new LandTerritoryStatisticRackEntry(statWidth, eUIFocus.WINDOW, style, Localization.Localize("text_nobility_happiness_stat"), v.Happiness.NobilityHappiness, v.Happiness.NobilityHappinessConstituents, false, v.Happiness.GetNobilityHappinessTotalTooltipInfo(gameManagers), content);
            statEntries.Add(nobilityHappiness);
            LandTerritoryStatisticRackEntry order = new LandTerritoryStatisticRackEntry(statWidth, eUIFocus.WINDOW, style, Localization.Localize("text_order_stat"), v.Order.Order, v.Order.OrderConstituents, false, v.Order.GetTooltipInfo(gameManagers), content);
            statEntries.Add(order);
            LandTerritoryStatisticRackEntry food = new LandTerritoryStatisticRackEntry(statWidth, eUIFocus.WINDOW, style, Localization.Localize("text_food_stat"), v.Food.Food, v.Food.FoodConstituents, false, v.Food.GetTooltipInfo(gameManagers), content);
            statEntries.Add(food);
            LandTerritoryStatisticRackEntry foodStores = new LandTerritoryStatisticRackEntry(statWidth, eUIFocus.WINDOW, style, Localization.Localize("text_food_stores_stat"), v.FoodStores.FoodStoresChange, v.FoodStores.FoodStoresChangeConstituents, false, v.FoodStores.GetFoodStoresChangeTooltipInfo(gameManagers), content);
            statEntries.Add(foodStores);
            LandTerritoryStatisticFoodStoresStateEntry foodStoresState = new LandTerritoryStatisticFoodStoresStateEntry(statWidth, eUIFocus.WINDOW, style, v.FoodStores, gameManagers, graphicsComponent, content);
            statEntries.Add(foodStoresState);
            LandTerritoryStatisticRackEntry health = new LandTerritoryStatisticRackEntry(statWidth, eUIFocus.WINDOW, style, Localization.Localize("text_health_stat"), v.Health.Health, v.Health.HealthConstituents, false, v.Health.GetTooltipInfo(gameManagers), content);
            statEntries.Add(health);
            LandTerritoryStatisticRackEntry population = new LandTerritoryStatisticRackEntry(statWidth, eUIFocus.WINDOW, style, Localization.Localize("text_population_stat"), v.Population.PopulationGrowth, v.Population.PopulationGrowthConstituents, true, v.Population.GetPopulationGrowthTooltipInfo(gameManagers), content);
            statEntries.Add(population);
            LandTerritoryStatisticPopulationStateEntry populationState = new LandTerritoryStatisticPopulationStateEntry(statWidth, eUIFocus.WINDOW, style, lt, lt.Variables.SupportablePopulation, gameManagers, content);
            statEntries.Add(populationState);
            LandTerritoryStatisticRackEntry manpower = new LandTerritoryStatisticRackEntry(statWidth, eUIFocus.WINDOW, style, Localization.Localize("text_manpower_stat"), v.Manpower.ManpowerChange, v.Manpower.ManpowerChangeConstituents, false, v.Manpower.GetTooltipInfo(gameManagers), content);
            statEntries.Add(manpower);
            LandTerritoryStatisticManpowerStateEntry manpowerState = new LandTerritoryStatisticManpowerStateEntry(statWidth, eUIFocus.WINDOW, style, lt.Variables.Manpower, graphicsComponent, content);
            statEntries.Add(manpowerState);
            LandTerritoryStatisticRackEntry wealth = new LandTerritoryStatisticRackEntry(statWidth, eUIFocus.WINDOW, style, Localization.Localize("text_private_wealth_stat"), v.PrivateWealth.PrivateWeathChange, v.PrivateWealth.PrivateWealthChangeConstituents, false, v.PrivateWealth.GetPrivateWealthChangeTooltipInfo(gameManagers), content);
            statEntries.Add(wealth);
            LandTerritoryStatisticPrivateWealthStateEntry wealthState = new LandTerritoryStatisticPrivateWealthStateEntry(statWidth, eUIFocus.WINDOW, style, lt.Variables.PrivateWealth, gameManagers, content);
            statEntries.Add(wealthState);

            if (lt.RequiresWaterStat())
            {
                LandTerritoryStatisticRackEntry water = new LandTerritoryStatisticRackEntry(statWidth, eUIFocus.WINDOW, style, Localization.Localize("text_water_stat"), v.Weather.Water, v.Weather.WaterConstituents, false, v.Weather.GetWaterTooltipInfo(gameManagers), content);
                statEntries.Add(water);
            }
            if (lt.RequiresWarmthStat())
            {
                LandTerritoryStatisticRackEntry warmth = new LandTerritoryStatisticRackEntry(statWidth, eUIFocus.WINDOW, style, Localization.Localize("text_warmth_stat"), v.Weather.Warmth, v.Weather.WarmthConstituents, false, v.Weather.GetWarmthTooltipInfo(gameManagers), content);
                statEntries.Add(warmth);
            }

            VerticalScrollPane scrollPane = new VerticalScrollPane(point, eUIAnchor.TOP_LEFT, page.Rectangle.Height - 2 * WINDOW_INNER_BORDER - BOTTOM_SPACE_FOR_CLOSE_BUTTON, statWidth, LandTerritoryStatisticEntry.Height, eUIFocus.WINDOW, statEntries, 0, style, null, graphicsComponent, content);
            page.SetChild(scrollPane);

            return page;
        }
        private Page CreatePage_LandTerritory_Resources(LandTerritory lt, Window window)
        {
            Page page = new Page(window);

            UIStyle style = uiComponent.Style;
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            int paneWidth = page.Rectangle.Width - 2 * WINDOW_INNER_BORDER;
            
            int resourceNameWidth = 120;
            int columnWidth = (paneWidth - resourceNameWidth) / 2;
            int maxNumIcons = columnWidth / ResourceIcon.Size;

            Text resourceText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_resource"), style.WindowTextColor), style.WindowFontID, eTextAlignment.CENTER, 0, resourceNameWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(resourceText);
            point.X += resourceNameWidth;
            Text productionText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_production"), style.WindowTextColor), style.WindowFontID, eTextAlignment.CENTER, 0, columnWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(productionText);
            point.X += columnWidth;
            Text consumptionText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_consumption"), style.WindowTextColor), style.WindowFontID, eTextAlignment.CENTER, 0, columnWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(consumptionText);

            int paneHeight = page.Rectangle.Height - 2 * WINDOW_INNER_BORDER - BOTTOM_SPACE_FOR_CLOSE_BUTTON - resourceText.Rectangle.Height - STANDARD_SPACING;

            point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER + resourceText.Rectangle.Height + STANDARD_SPACING);

            List<UIEntity> scrollEntities = new List<UIEntity>();

            float quantityPerIcon = LandTerritoryResourceRow.CalculateResourceQuantityPerIcon(lt.Variables.ResourceProduction, lt.Variables.ResourcePresence, maxNumIcons);
            foreach (KeyValuePair<string, Resource> kvp in gameManagers.ResourceManager.GetAllResources())
            {
                LandTerritoryResourceRow row = new LandTerritoryResourceRow(paneWidth, resourceNameWidth, columnWidth, maxNumIcons, eUIFocus.WINDOW, quantityPerIcon, kvp.Value, lt.Variables.ResourceProduction, lt.Variables.ResourcePresence, style, content);
                scrollEntities.Add(row);
            }

            VerticalScrollPane scrollPane = new VerticalScrollPane(point, eUIAnchor.TOP_LEFT, paneHeight, paneWidth, ResourceIcon.Size, eUIFocus.WINDOW, scrollEntities, 0, style, null, graphicsComponent, content);
            page.SetChild(scrollPane);

            return page;
        }
        private Page CreatePage_LandTerritory_Income(LandTerritory lt, Window window)
        {
            Page page = new Page(window);

            UIStyle style = uiComponent.Style;
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            IncomeSet incomeSet = lt.Variables.IncomeSet;

            Text sourcesOfWealthText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_sources_of_wealth"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(sourcesOfWealthText);

            point.Y += sourcesOfWealthText.Rectangle.Height + STANDARD_SMALL_SPACING;

            int bottomDivsHeight = 150;
            int bottomDivsWidth = (page.Rectangle.Width - 2 * WINDOW_INNER_BORDER - STANDARD_SPACING) / 2;

            int wealthSourceScrollPaneHeight = page.Rectangle.Height - point.Y - WINDOW_INNER_BORDER - STANDARD_SPACING - bottomDivsHeight - BOTTOM_SPACE_FOR_CLOSE_BUTTON;
            int wealthSourceRowWidth = page.Rectangle.Width - 2 * WINDOW_INNER_BORDER;
            int wealthSourceRowNameWidth = 160;
            int maxNumIcons = (wealthSourceRowWidth - wealthSourceRowNameWidth) / WealthSourceIcon.Size;
            float valuePerIcon = LandTerritoryWealthSourceRow.CalculateWealthSourceValuePerIcon(lt.Variables.IncomeSet, maxNumIcons);
            List<UIEntity> wealthSourceRows = new List<UIEntity>();
            foreach (WealthSource ws in incomeSet.WealthSources)
                wealthSourceRows.Add(new LandTerritoryWealthSourceRow(wealthSourceRowWidth, wealthSourceRowNameWidth, maxNumIcons, eUIFocus.WINDOW, valuePerIcon, ws, style, content));

            VerticalScrollPane wealthSourceScrollPane = new VerticalScrollPane(point, eUIAnchor.TOP_LEFT, wealthSourceScrollPaneHeight, wealthSourceRowWidth, WealthSourceIcon.Size, eUIFocus.WINDOW, wealthSourceRows, 0, style, null, graphicsComponent, content);
            page.SetChild(wealthSourceScrollPane);

            point.Y += wealthSourceScrollPane.Rectangle.Height + STANDARD_SPACING;

            // Taxes div
            LandProvinceStake lps = lt.Variables.OwningFaction.Variables.GetLandProvinceStake(lt.ParentLandProvince);
            WindowDiv taxesDiv = new WindowDiv(point, eUIAnchor.TOP_LEFT, bottomDivsWidth, bottomDivsHeight, eUIFocus.WINDOW, style, content);
            if (lt.Variables.OccupyingFaction != null)
            {
                Point taxesDivPoint = new Point(0, 0);
                Text noTaxesText = new Text(taxesDivPoint, eUIAnchor.CENTER_CENTER, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_occupation_no_taxes"), style.WindowTextColor), style.WindowFontID, eTextAlignment.CENTER, style.WindowLineSpacing, taxesDiv.Rectangle.Width - 2 * WindowDiv.InnerBorder, 5, eUIFocus.WINDOW, null, content);
                taxesDiv.SetChild(noTaxesText);
            }
            else if (lps != null && lps.Taxes.Taxes.Count > 0)
            {
                Point taxesDivPoint = new Point(WindowDiv.InnerBorder, WindowDiv.InnerBorder);
                Text provincialTaxesText = new Text(taxesDivPoint, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_provincial_taxes"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, taxesDiv.Rectangle.Width - 2 * WindowDiv.InnerBorder, 1, eUIFocus.WINDOW, null, content);
                taxesDiv.SetChild(provincialTaxesText);

                taxesDivPoint.Y += provincialTaxesText.Rectangle.Height + STANDARD_SMALL_SPACING;

                int taxDisplayWidth = taxesDiv.Rectangle.Width - 2 * WindowDiv.InnerBorder;
                int taxDisplayHeight = taxesDiv.Rectangle.Height - WindowDiv.InnerBorder - taxesDivPoint.Y;

                List<UIEntity> scrollEntities = new List<UIEntity>();
                foreach (Tax t in lps.Taxes.Taxes)
                    scrollEntities.Add(new TaxDisplayRow(taxDisplayWidth, eUIFocus.WINDOW, t, style, content));
                VerticalScrollPane taxScrollPane = new VerticalScrollPane(taxesDivPoint, eUIAnchor.TOP_LEFT, taxDisplayHeight, taxDisplayWidth, TaxDisplayRow.Height, eUIFocus.WINDOW, scrollEntities, 0, style, null, graphicsComponent, content);
                taxesDiv.SetChild(taxScrollPane);
            }
            else
            {
                Point taxesDivPoint = new Point(0, 0);
                Text noProvincialTaxesText = new Text(taxesDivPoint, eUIAnchor.CENTER_CENTER, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_no_provincial_taxes"), style.WindowTextColor), style.WindowFontID, eTextAlignment.CENTER, 0, taxesDiv.Rectangle.Width - 2 * WindowDiv.InnerBorder, 1, eUIFocus.WINDOW, null, content);
                taxesDiv.SetChild(noProvincialTaxesText);
            }
            page.SetChild(taxesDiv);

            point.X += taxesDiv.Rectangle.Width + STANDARD_SPACING;

            // Totals div
            WindowDiv totalsDiv = new WindowDiv(point, eUIAnchor.TOP_LEFT, bottomDivsWidth, bottomDivsHeight, eUIFocus.WINDOW, style, content);
            Point totalsDivPoint = new Point(WindowDiv.InnerBorder, WindowDiv.InnerBorder);

            TooltipInfo totalTaxedWealthTooltip = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_total_taxed_wealth_tooltip"));
            Text totalTaxedWealthText = new Text(totalsDivPoint, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_total_taxed_wealth"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, totalsDiv.Rectangle.Width - 2 * WindowDiv.InnerBorder, 1, eUIFocus.WINDOW, totalTaxedWealthTooltip, content);
            totalsDiv.SetChild(totalTaxedWealthText);

            totalsDivPoint.Y += totalTaxedWealthText.Rectangle.Height + STANDARD_SMALL_SPACING;

            IconText totalTaxedWealthIcon = new IconText(totalsDivPoint, eUIAnchor.TOP_LEFT, totalsDiv.Rectangle.Width - WindowDiv.InnerBorder - totalsDivPoint.X, eUIFocus.WINDOW, totalTaxedWealthTooltip, "wealth-icon", UIUtilities.GetFloatDisplayString(incomeSet.TotalTaxedWealth), style.WindowTextColor, style.WindowFontID, content);
            totalsDiv.SetChild(totalTaxedWealthIcon);

            totalsDivPoint.X = WindowDiv.InnerBorder;
            totalsDivPoint.Y = totalTaxedWealthIcon.Position.Y + totalTaxedWealthIcon.Rectangle.Height + STANDARD_SPACING;

            TooltipInfo efficiencyTooltip = TooltipInfo.CreateIncomeEfficiencySetTooltipInfo(lt.Variables.IncomeSet.EfficiencySet);
            Text efficiencyText = new Text(totalsDivPoint, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_income_efficiency"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, totalsDiv.Rectangle.Width - 2 * WindowDiv.InnerBorder, 1, eUIFocus.WINDOW, efficiencyTooltip, content);
            totalsDiv.SetChild(efficiencyText);

            totalsDivPoint.Y += efficiencyText.Rectangle.Height + STANDARD_SMALL_SPACING;

            Text efficiencyNum = new Text(totalsDivPoint, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(UIUtilities.GetPercentageFloatDisplayString(incomeSet.EfficiencySet.TotalEfficiencyPercentage), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, totalsDiv.Rectangle.Width - 2 * WindowDiv.InnerBorder, 1, eUIFocus.WINDOW, efficiencyTooltip, content);
            totalsDiv.SetChild(efficiencyNum);

            totalsDivPoint.Y += efficiencyNum.Rectangle.Height + STANDARD_SPACING;

            TooltipInfo totalIncomeTooltip = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_total_income_tooltip"));
            Text totalIncomeText = new Text(totalsDivPoint, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_total_income"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, totalsDiv.Rectangle.Width - 2 * WindowDiv.InnerBorder, 1, eUIFocus.WINDOW, totalIncomeTooltip, content);
            totalsDiv.SetChild(totalIncomeText);

            totalsDivPoint.Y += totalIncomeText.Rectangle.Height + STANDARD_SMALL_SPACING;

            IconText totalIncomeIcon = new IconText(totalsDivPoint, eUIAnchor.TOP_LEFT, totalsDiv.Rectangle.Width - WindowDiv.InnerBorder - totalsDivPoint.X, eUIFocus.WINDOW, totalIncomeTooltip, "money-icon", UIUtilities.GetIntDisplayString(incomeSet.TotalIncome), style.WindowTextColor, style.WindowFontID, content);
            totalsDiv.SetChild(totalIncomeIcon);

            page.SetChild(totalsDiv);

            return page;
        }
        private Page CreatePage_LandTerritory_Trade(LandTerritory lt, Window window)
        {
            Page page = new Page(window);

            UIStyle style = uiComponent.Style;
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);
            TradeSet tradeSet = lt.Variables.TradeSet;

            TooltipInfo merchantPointsTooltip = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_merchant_movement_points_tooltip"));
            Text merchantPointsText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_merchant_movement_points"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, merchantPointsTooltip, content);
            page.SetChild(merchantPointsText);

            point.Y += merchantPointsText.Rectangle.Height + STANDARD_SMALL_SPACING;

            IconText merchantPointsIcon = new IconText(point, eUIAnchor.TOP_LEFT, page.Rectangle.Width - point.X - WINDOW_INNER_BORDER, eUIFocus.WINDOW, merchantPointsTooltip, "trade-icon", UIUtilities.GetIntDisplayString(lt.GetTraderMovementPoints(gameManagers)), style.WindowTextColor, style.WindowFontID, content);
            page.SetChild(merchantPointsIcon);

            point.Y += merchantPointsIcon.Rectangle.Height + STANDARD_SPACING;

            int paneNameWidth = 120;
            int paneValueWidth = 70;
            int scrollPaneWidth = page.Rectangle.Width - 2 * WINDOW_INNER_BORDER;

            // Imports
            int importsPointsWidth = 70;
            int importsResourcesWidth = scrollPaneWidth - paneNameWidth - importsPointsWidth - paneValueWidth;

            Text importsText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_imports_title"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(importsText);

            point.Y += importsText.Rectangle.Height + STANDARD_SMALL_SPACING;

            Text importsTerritoryText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_land_territory"), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, paneNameWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(importsTerritoryText);
            point.X += paneNameWidth;
            Text importsResourcesText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_resources"), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, importsResourcesWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(importsResourcesText);
            point.X += importsResourcesWidth;
            TooltipInfo valueTooltip = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_trade_value"));
            Text importsValueText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_value"), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, paneValueWidth, 1, eUIFocus.WINDOW, valueTooltip, content);
            page.SetChild(importsValueText);
            point.X += paneValueWidth;
            TooltipInfo importsPointsTooltip = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_merchant_movement_points_tooltip"));
            Text importsPointsText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_movement_points"), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, importsPointsWidth, 1, eUIFocus.WINDOW, importsPointsTooltip, content);
            page.SetChild(importsPointsText);

            point.Y += importsTerritoryText.Rectangle.Height + STANDARD_SMALL_SPACING;
            point.X = WINDOW_INNER_BORDER;

            int scrollPaneHeight = (page.Rectangle.Height - 2 * WINDOW_INNER_BORDER - BOTTOM_SPACE_FOR_CLOSE_BUTTON - 4 * merchantPointsText.Rectangle.Height - 3 * importsTerritoryText.Rectangle.Height - 7 * STANDARD_SMALL_SPACING - 3 * STANDARD_SPACING) / 3;
            
            List<UIEntity> importRows = new List<UIEntity>();
            IOrderedEnumerable<TradePath> orderedImportTradePaths = tradeSet.ImportTradePaths.OrderBy(x => x.MovementPointCost);
            foreach (TradePath tp in orderedImportTradePaths)
                if (tp.TradedResources.Count > 0)
                    importRows.Add(new LandTerritoryImportRow(scrollPaneWidth, eUIFocus.WINDOW, paneNameWidth, paneValueWidth, importsPointsWidth, tp, style, content));
            VerticalScrollPane importsScrollPane = new VerticalScrollPane(point, eUIAnchor.TOP_LEFT, scrollPaneHeight, scrollPaneWidth, ResourceIcon.Size, eUIFocus.WINDOW, importRows, 0, style, null, graphicsComponent, content);
            page.SetChild(importsScrollPane);

            point.Y += importsScrollPane.Rectangle.Height + STANDARD_SPACING;

            // Exports
            int exportsResourcesWidth = scrollPaneWidth - paneNameWidth - paneValueWidth;

            Text exportsText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_exports_title"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(exportsText);

            point.Y += exportsText.Rectangle.Height + STANDARD_SMALL_SPACING;

            Text exportsTerritoryText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_land_territory"), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, paneNameWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(exportsTerritoryText);
            point.X += paneNameWidth;
            Text exportsResourcesText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_resources"), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, exportsResourcesWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(exportsResourcesText);
            point.X += exportsResourcesWidth;
            Text exportsValueText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_value"), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, paneValueWidth, 1, eUIFocus.WINDOW, valueTooltip, content);
            page.SetChild(exportsValueText);

            point.Y += exportsTerritoryText.Rectangle.Height + STANDARD_SMALL_SPACING;
            point.X = WINDOW_INNER_BORDER;

            List<UIEntity> exportRows = new List<UIEntity>();
            IOrderedEnumerable<TradePath> orderedExportTradePaths = tradeSet.ExportTradePaths.OrderBy(x => x.MovementPointCost);
            foreach (TradePath tp in orderedExportTradePaths)
                if (tp.TradedResources.Count > 0)
                    exportRows.Add(new LandTerritoryExportRow(scrollPaneWidth, eUIFocus.WINDOW, paneNameWidth, paneValueWidth, tp, style, content));
            VerticalScrollPane exportsScrollPane = new VerticalScrollPane(point, eUIAnchor.TOP_LEFT, scrollPaneHeight, scrollPaneWidth, ResourceIcon.Size, eUIFocus.WINDOW, exportRows, 0, style, null, graphicsComponent, content);
            page.SetChild(exportsScrollPane);

            point.Y += exportsScrollPane.Rectangle.Height + STANDARD_SPACING;

            // Throughports
            int throughportsResourcesWidth = scrollPaneWidth - 2 * paneNameWidth - paneValueWidth;
            Text throughportsText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_throughports_title"), style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(throughportsText);

            point.Y += throughportsText.Rectangle.Height + STANDARD_SMALL_SPACING;

            Text throughportsExportTerritoryText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_export_land_territory"), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, paneNameWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(throughportsExportTerritoryText);
            point.X += paneNameWidth;
            Text throughportsImportTerritoryText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_import_land_territory"), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, paneNameWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(throughportsImportTerritoryText);
            point.X += paneNameWidth;
            Text throughportsResourcesText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_resources"), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, throughportsResourcesWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(throughportsResourcesText);
            point.X += throughportsResourcesWidth;
            Text throughportsValueText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(Localization.Localize("text_value"), style.WindowTextColor), style.LandTerritoryTradeFontID, eTextAlignment.CENTER, 0, paneValueWidth, 1, eUIFocus.WINDOW, valueTooltip, content);
            page.SetChild(throughportsValueText);

            point.Y += throughportsExportTerritoryText.Rectangle.Height + STANDARD_SMALL_SPACING;
            point.X = WINDOW_INNER_BORDER;

            List<UIEntity> throughportRows = new List<UIEntity>();
            IOrderedEnumerable<TradePath> orderedThroughportTradePaths = tradeSet.ThroughportTradePaths.OrderBy(x => x.MovementPointCost);
            foreach (TradePath tp in orderedThroughportTradePaths)
                if (tp.TradedResources.Count > 0)
                    throughportRows.Add(new LandTerritoryThroughportRow(scrollPaneWidth, eUIFocus.WINDOW, paneNameWidth, paneValueWidth, tp, style, content));
            VerticalScrollPane throughportsScrollPane = new VerticalScrollPane(point, eUIAnchor.TOP_LEFT, scrollPaneHeight, scrollPaneWidth, ResourceIcon.Size, eUIFocus.WINDOW, throughportRows, 0, style, null, graphicsComponent, content);
            page.SetChild(throughportsScrollPane);

            return page;
        }
        private Page CreatePage_LandTerritory_Buildings(LandTerritory lt, Window window)
        {
            Page page = new Page(window);

            UIStyle style = uiComponent.Style;
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            int buildingSetDivHeight = (lt.Variables.OwningFaction == gameManagers.TurnManager.FactionTurn) ? 250 : 150;

            string settlementTitle = Localization.Localize("text_settlement");
            BuildingSetDiv settlementDiv = new BuildingSetDiv(point, eUIAnchor.TOP_LEFT, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, buildingSetDivHeight, eUIFocus.WINDOW, lt.Variables.SettlementBuildingSet, lt, style, settlementTitle, window, gameManagers, graphicsComponent, content);
            page.SetChild(settlementDiv);

            point.Y += settlementDiv.Rectangle.Height + STANDARD_SPACING;

            string territoryTitle = Localization.Localize("text_territory");
            BuildingSetDiv territoryDiv = new BuildingSetDiv(point, eUIAnchor.TOP_LEFT, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, buildingSetDivHeight, eUIFocus.WINDOW, lt.Variables.TerritoryBuildingSet, lt, style, territoryTitle, window, gameManagers, graphicsComponent, content);
            page.SetChild(territoryDiv);

            return page;
        }
        private Page CreatePage_LandTerritory_Garrison(LandTerritory lt, Window window)
        {
            Page page = new Page(window);

            UIStyle style = uiComponent.Style;
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);
            int contentWidth = page.Rectangle.Width - 2 * WINDOW_INNER_BORDER;

            string permanentGarrisonString = Localization.Localize("text_permanent_garrison");
            Text permanentGarrisonText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(permanentGarrisonString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, contentWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(permanentGarrisonText);

            point.Y += permanentGarrisonText.Rectangle.Height + STANDARD_SPACING;

            List<UIEntity> garrisonUnitCards = new List<UIEntity>();
            foreach (Unit unit in lt.Variables.GarrisonUnitSet.Units)
                garrisonUnitCards.Add(UnitCard.CreateUnitCard(unit, Point.Zero, eUIAnchor.TOP_LEFT, style, eUIFocus.WINDOW, content));
            HorizontalScrollPane garrisonScrollPane = new HorizontalScrollPane(point, eUIAnchor.TOP_LEFT, contentWidth, UnitCard.Width, UnitCard.Height, eUIFocus.WINDOW, garrisonUnitCards, 0, style, null, graphicsComponent, content);
            page.SetChild(garrisonScrollPane);

            point.Y += garrisonScrollPane.Rectangle.Height + STANDARD_SPACING;

            List<Statue> garrisonStatues = lt.GetGarrisonedStatues();
            string garrisonForcesString = Localization.Localize((garrisonStatues.Count > 0) ? "text_garrison_forces" : "text_no_garrison_forces");
            Text garrisonForcesText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(garrisonForcesString, style.WindowTextColor), style.WindowFontID, eTextAlignment.LEFT, 0, contentWidth, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(garrisonForcesText);

            // TODO

            return page;
        }
        // Land region window
        private Page CreatePage_LandRegion_Overview(LandRegion lr, Window window)
        {
            Page page = new Page(window);

            // DEBUG
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            string nameString = lr.GetNameString();
            Text nameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(nameString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(nameText);

            return page;
        }
        // Land province window
        private Page CreatePage_LandProvince_Overview(LandProvince lp, Window window)
        {
            Page page = new Page(window);

            // DEBUG
            Point point = new Point(WINDOW_INNER_BORDER, WINDOW_INNER_BORDER);

            string nameString = lp.GetNameString();
            Text nameText = new Text(point, eUIAnchor.TOP_LEFT, ColoredTextSegment.GetSegmentsForSingleText(nameString, uiComponent.Style.WindowTextColor), uiComponent.Style.WindowFontID, eTextAlignment.LEFT, 0, page.Rectangle.Width - 2 * WINDOW_INNER_BORDER, 1, eUIFocus.WINDOW, null, content);
            page.SetChild(nameText);

            return page;
        }
    }
}
