﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eCursorChangeSource
    {
        NONE,
        UI,
        STATUE_DRAGGING,
        GAME_ENTITIES
    }

    class Cursor : UIEntity
    {
        /* -------- Properties -------- */
        public CursorIcon Icon { get; private set; }

        /* -------- Private Fields -------- */
        // Assets
        private Texture2D cursorTex;
        // Constants
        private const int CURSOR_TEXTURE_WIDTH = 40;
        private const int CURSOR_TEXTURE_HEIGHT = 39;

        /* -------- Constructors -------- */
        public Cursor(UIStyle style, ContentManager content)
            : base(new Point(), eUIAnchor.TOP_LEFT, CURSOR_TEXTURE_WIDTH, CURSOR_TEXTURE_HEIGHT, eUIFocus.NO_FOCUS, null)
        {
            Icon = new CursorIcon(style, content);
            SetChild(Icon);

            cursorTex = AssetLoader.LoadTexture(style.CursorTextureId, content);
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTimeWrapper gtw, GraphicsComponent g, InputComponent i)
        {
            SetPosition(i.GetMousePosition());

            base.Update(gtw, g, i);
        }

        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(cursorTex, Rectangle, Color.White);

            base.SBDraw(g);
        }

        public override bool MouseIsOver(InputComponent i)
        {
            return false;
        }

        public override eUIType GetUIType()
        {
            return eUIType.STATIC;
        }
    }
}
