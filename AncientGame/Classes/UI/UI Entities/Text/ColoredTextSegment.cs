﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    class ColoredTextSegment
    {
        /* -------- Properties -------- */
        public string Text { get; private set; }
        public Color Color { get; private set; }

        /* -------- Constructors -------- */
        public ColoredTextSegment(string inText, Color inColor)
        {
            Text = inText;
            Color = inColor;
        }

        /* -------- Static Methods -------- */
        public static List<ColoredTextSegment> GetSegmentsForSingleText(string text, Color color)
        {
            return new List<ColoredTextSegment> { new ColoredTextSegment(text, color) };
        }
    }
}
