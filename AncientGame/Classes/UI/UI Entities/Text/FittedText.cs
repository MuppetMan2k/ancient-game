﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class FittedText : Text
    {
        /* -------- Constructors -------- */
        public FittedText(Point inPosition, eUIAnchor inAnchor, List<ColoredTextSegment> inColoredTextSegments, string inSpriteFontID, eTextAlignment inAlignment, int inMaxWidth, eUIFocus inFocus, TooltipInfo inTooltipInfo, ContentManager content)
            : base(inPosition, inAnchor, inColoredTextSegments, inSpriteFontID, inAlignment, 0, int.MaxValue, 1, inFocus, inTooltipInfo, content)
        {
            int width = Rectangle.Width;
            if (width > inMaxWidth)
            {
                // Text is bigger than max width so 'fit' it
                scale = (float)inMaxWidth / (float)width;
                int newX;
                switch (inAlignment)
                {
                    case eTextAlignment.LEFT:
                        newX = Rectangle.Left;
                        break;
                    case eTextAlignment.CENTER:
                        newX = Rectangle.Center.X - inMaxWidth / 2;
                        break;
                    case eTextAlignment.RIGHT:
                        newX = Rectangle.Right - inMaxWidth;
                        break;
                    default:
                        Debug.LogUnrecognizedValueWarning(inAlignment);
                        newX = 0;
                        break;
                }
                int newY = Rectangle.Y;
                int newHeight = Mathf.Round(scale * (float)Rectangle.Height);
                Rectangle newRect = new Rectangle(newX, newY, inMaxWidth, newHeight);
                Rectangle = newRect;
            }
        }
    }
}
