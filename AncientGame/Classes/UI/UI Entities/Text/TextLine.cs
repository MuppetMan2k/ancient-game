﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    class TextLine
    {
        /* -------- Properties -------- */
        public List<TextWord> Words { get; private set; }
        public int Width { get; private set; }
        public int Offset { get; private set; }

        /* -------- Constructors -------- */
        public TextLine()
        {
            Words = new List<TextWord>();
            Width = 0;
        }

        /* -------- Public Methods -------- */
        public void SBDraw(GraphicsComponent g, SpriteFont spriteFont, Point lineBasePoint, float scale)
        {
            Point lineStartPoint = lineBasePoint;
            lineStartPoint.X += Mathf.Round(Offset * scale);

            foreach (TextWord w in Words)
                w.SBDraw(g, spriteFont, lineStartPoint, scale);
        }

        public void AddWord(TextWord newWord, int spaceSize)
        {
            int wordOffset;
            if (Words.Count == 0)
                wordOffset = 0;
            else
                wordOffset = Width + spaceSize;
            newWord.SetOffset(wordOffset);

            Width = GetLengthWithNewWord(newWord, spaceSize);

            Words.Add(newWord);
        }

        public int GetLengthWithNewWord(TextWord word, int spaceSize)
        {
            if (Words.Count == 0)
                return word.Width;

            return Width + spaceSize + word.Width;
        }

        public void SetOffset(int inOffset)
        {
            Offset = inOffset;
        }
    }
}
