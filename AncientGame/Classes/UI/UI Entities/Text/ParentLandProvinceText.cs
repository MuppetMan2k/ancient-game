﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class ParentLandProvinceText : Text
    {
        /* -------- Private Fields -------- */
        private LandProvince parentLandProvince;
        private WindowManager windowManager;

        /* -------- Constructors -------- */
        public ParentLandProvinceText(Point inPosition, eUIAnchor inAnchor, string inSpriteFontID, Color inColor, int inMaxWidth, eUIFocus inFocus, ContentManager content, LandProvince inParentLandProvince, WindowManager inWindowManager)
            : base(inPosition, inAnchor, GetColoredTextSegments(inColor, inParentLandProvince), inSpriteFontID, eTextAlignment.LEFT, 0, inMaxWidth, 1, inFocus, TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_right_click_further_information")), content)
        {
            parentLandProvince = inParentLandProvince;
            windowManager = inWindowManager;
        }

        /* -------- Public Methods -------- */
        public override void OnMouseRightClick(GraphicsComponent g, InputComponent i)
        {
            windowManager.CreateWindow_LandProvince(parentLandProvince);

            base.OnMouseRightClick(g, i);
        }

        /* -------- Static Methods -------- */
        private static List<ColoredTextSegment> GetColoredTextSegments(Color color, LandProvince parentLandProvince)
        {
            string text = String.Format(Localization.Localize("text_parent_land-province_label"), parentLandProvince.GetNameString());
            return ColoredTextSegment.GetSegmentsForSingleText(text, color);
        }
    }
}
