﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class ParentLandRegionText : Text
    {
        /* -------- Private Fields -------- */
        private LandRegion parentLandRegion;
        private WindowManager windowManager;

        /* -------- Constructors -------- */
        public ParentLandRegionText(Point inPosition, eUIAnchor inAnchor, string inSpriteFontID, Color inColor, int inMaxWidth, eUIFocus inFocus, ContentManager content, LandRegion inParentLandRegion, WindowManager inWindowManager)
            : base(inPosition, inAnchor, GetColoredTextSegments(inColor, inParentLandRegion), inSpriteFontID, eTextAlignment.LEFT, 0, inMaxWidth, 1, inFocus, TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_right_click_further_information")), content)
        {
            parentLandRegion = inParentLandRegion;
            windowManager = inWindowManager;
        }

        /* -------- Public Methods -------- */
        public override void OnMouseRightClick(GraphicsComponent g, InputComponent i)
        {
            windowManager.CreateWindow_LandRegion(parentLandRegion);

            base.OnMouseRightClick(g, i);
        }

        /* -------- Static Methods -------- */
        private static List<ColoredTextSegment> GetColoredTextSegments(Color color, LandRegion parentLandRegion)
        {
            string text = String.Format(Localization.Localize("text_parent_land-region_label"), parentLandRegion.GetNameString());
            return ColoredTextSegment.GetSegmentsForSingleText(text, color);
        }
    }
}
