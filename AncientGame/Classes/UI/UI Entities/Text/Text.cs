﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    // -------- Enumerations --------
    enum eTextAlignment
    {
        LEFT,
        RIGHT,
        CENTER
    }

    class Text : UIEntity
    {
        /* -------- Private Fields -------- */
        private List<ColoredTextSegment> textSegments;
        private List<TextLine> lines;
        private eTextAlignment alignment;
        private int lineSpacing;
        private int fontHeight;
        protected float scale;
        // Reference store
        private int maxWidth;
        private int maxLines;
        // Assets
        private SpriteFont spriteFont;

        /* -------- Constructors -------- */
        public Text(Point inPosition, eUIAnchor inAnchor, List<ColoredTextSegment> inTextSegments, string spriteFontID, eTextAlignment inAlignment, int inLineSpacing, int inMaxWidth, int inMaxLines, eUIFocus inFocus, TooltipInfo inTooltipInfo, ContentManager contentManager)
            : base(inPosition, inAnchor, inMaxWidth, 0, inFocus, inTooltipInfo)
        {
            textSegments = inTextSegments;
            alignment = inAlignment;
            lineSpacing = inLineSpacing;
            lines = new List<TextLine>();
            scale = 1f;

            maxWidth = inMaxWidth;
            maxLines = inMaxLines;

            spriteFont = AssetLoader.LoadSpriteFont(spriteFontID, contentManager);
            fontHeight = CalculateFontHeight();

            SetUpLines(maxWidth, maxLines);
            SetRectangleSize(maxWidth);
            SetLineOffsets();
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                TextLine line = lines[i];
                int lineBasePointX = Rectangle.Left;
                int lineBasePointY = Rectangle.Top + i * (fontHeight + lineSpacing);
                Point lineBasePoint = new Point(lineBasePointX, lineBasePointY);
                line.SBDraw(g, spriteFont, lineBasePoint, scale);
            }

            base.SBDraw(g);
        }

        /* -------- Private Methods --------- */
        private void SetUpLines(int maxWidth, int maxLines)
        {
            List<TextWord> words = TextUtilities.SplitTextSegmentsIntoWords(textSegments, spriteFont);
            lines = TextUtilities.FormWordsIntoLines(words, maxWidth, maxLines, spriteFont);
        }
        private void SetLineOffsets()
        {
            switch (alignment)
            {
                case eTextAlignment.LEFT:
                    foreach (TextLine l in lines)
                        l.SetOffset(0);
                    break;
                case eTextAlignment.CENTER:
                    foreach (TextLine l in lines)
                        l.SetOffset((Rectangle.Width - l.Width) / 2);
                    break;
                case eTextAlignment.RIGHT:
                    foreach (TextLine l in lines)
                        l.SetOffset(Rectangle.Width - l.Width);
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(alignment);
                    break;
            }
        }

        private void SetRectangleSize(int maxWidth)
        {
            int height;
            if (lines.Count == 0)
                height = 0;
            else
                height = lines.Count * fontHeight + (lines.Count - 1) * lineSpacing;
            height = Mathf.Round(height * scale);

            int width;
            if (ShouldUseMaxWidthForWidth() && maxWidth != -1)
                width = maxWidth;
            else
            {
                width = 0;
                foreach (TextLine l in lines)
                    width = Mathf.Max(width, l.Width);
                width = Mathf.Round(width * scale);
            }

            Rectangle newRect = Rectangle;
            newRect.Width = width;
            newRect.Height = height;
            Rectangle = newRect;
        }

        private int CalculateFontHeight()
        {
            return Mathf.Round(spriteFont.MeasureString("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!?.,'").Y); // TODO - improve
        }
        private bool ShouldUseMaxWidthForWidth()
        {
            return (!TextUtilities.TextAlignmentIsEquivalentToUIAnchor(alignment, Anchor));
        }
    }
}
