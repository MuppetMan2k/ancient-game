﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AncientGame
{
    class TextWord
    {
        /* -------- Properties -------- */
        public string Text { get; private set; }
        public Color Color { get; private set; }
        public int Width { get; private set; }
        public int Offset { get; private set; }

        /* -------- Constructors -------- */
        public TextWord(string inText, Color inColor, SpriteFont font)
        {
            Text = inText.Trim();
            Color = inColor;
            Width = Mathf.Round(font.MeasureString(Text).X);
        }

        /* -------- Public Methods -------- */
        public void SBDraw(GraphicsComponent g, SpriteFont spriteFont, Point lineStartPoint, float scale)
        {
            Point wordStartPoint = lineStartPoint;
            wordStartPoint.X += Mathf.Round(Offset * scale);
            Vector2 psPos = SpaceUtilities.ConvertPSToPSVec(wordStartPoint);

            g.SpriteBatch.DrawString(spriteFont, Text, psPos, Color, 0f, new Vector2(), scale, SpriteEffects.None, 0f);
        }

        public void SetOffset(int inOffset)
        {
            Offset = inOffset;
        }
    }
}
