﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    abstract class ScrollPane : UIEntity
    {
        /* -------- Delegates -------- */
        public delegate void OnScrollMethod(int newScrollIndex);

        /* -------- Private Fields -------- */
        protected List<UIEntity> scrollEntities;
        protected int startIndex;
        protected int entitiesPerPage;
        protected bool prevScrollButtonActive;
        protected bool nextScrollButtonActive;
        protected int entityWidth;
        protected int entityHeight;
        private Color borderTint;
        private OnScrollMethod onScrollHandler;
        // Assets
        protected Texture2D backgroundTex;
        protected Texture2D vignetteTex;
        protected Texture2D scrollIconTex;
        private Texture2D pixelTex;
        // Constants
        protected const int SCROLL_BUTTON_SIZE = 16;
        protected const int SCROLL_ICON_SIZE = 12;
        private const int BORDER_THICKNESS = 1;

        /* -------- Constructors -------- */
        public ScrollPane(Point inPosition, eUIAnchor inAnchor, int inWidth, int inHeight, int inEntityWidth, int inEntityHeight, int inEntitiesPerPage, eUIFocus inFocus, List<UIEntity> inScrollEntities, int inStartIndex, UIStyle style, OnScrollMethod inScrollMethod, GraphicsComponent g, ContentManager content)
            : base(inPosition, inAnchor, inWidth, inHeight, inFocus, null)
        {
            onScrollHandler = inScrollMethod;

            scrollEntities = inScrollEntities;
            foreach (UIEntity ui in scrollEntities)
                SetChild(ui);

            startIndex = inStartIndex;
            entityWidth = inEntityWidth;
            entityHeight = inEntityHeight;
            entitiesPerPage = inEntitiesPerPage;

            backgroundTex = AssetLoader.LoadTexture(style.ScrollPaneBackgroundTextureID, content);
            vignetteTex = AssetLoader.LoadTexture("vignette", content);
            pixelTex = AssetLoader.LoadTexture("white-pixel", content);

            borderTint = style.ScrollPaneBorderColor;

            UpdateScrollButtonsActive();
            UpdateScrollEntitesActive(g);
            UpdateScrollEntityPositions(g);
        }

        /* -------- Public Methods -------- */
        public override void Update(GameTimeWrapper gtw, GraphicsComponent g, InputComponent i)
        {
            if (MouseIsOver(i))
            {
                int scrollChange = i.GetMouseScrollChange();
                if (scrollChange > 0 && prevScrollButtonActive)
                    ScrollToPrev(g);
                else if (scrollChange < 0 && nextScrollButtonActive)
                    ScrollToNext(g);
            }

            base.Update(gtw, g, i);
        }

        public override void SBDraw(GraphicsComponent g)
        {
            Rectangle topRect = new Rectangle(Rectangle.Left - BORDER_THICKNESS, Rectangle.Top - BORDER_THICKNESS, Rectangle.Width + 2 * BORDER_THICKNESS, BORDER_THICKNESS);
            Rectangle bottomRect = new Rectangle(Rectangle.Left - BORDER_THICKNESS, Rectangle.Bottom, Rectangle.Width + 2 * BORDER_THICKNESS, BORDER_THICKNESS);
            Rectangle leftRect = new Rectangle(Rectangle.Left - BORDER_THICKNESS, Rectangle.Top, BORDER_THICKNESS, Rectangle.Height);
            Rectangle rightRect = new Rectangle(Rectangle.Right, Rectangle.Top, BORDER_THICKNESS, Rectangle.Height);

            g.SpriteBatch.Draw(pixelTex, topRect, borderTint);
            g.SpriteBatch.Draw(pixelTex, bottomRect, borderTint);
            g.SpriteBatch.Draw(pixelTex, leftRect, borderTint);
            g.SpriteBatch.Draw(pixelTex, rightRect, borderTint);

            base.SBDraw(g);
        }

        /* -------- Private Methods --------- */
        private void UpdateScrollButtonsActive()
        {
            prevScrollButtonActive = (startIndex > 0);

            int lastIndexOnPage = startIndex + entitiesPerPage - 1;
            nextScrollButtonActive = (lastIndexOnPage < scrollEntities.Count - 1);
        }
        private void UpdateScrollEntitesActive(GraphicsComponent g)
        {
            int lastIndexOnPage = startIndex + entitiesPerPage - 1;

            for (int i = 0; i < scrollEntities.Count; i++)
            {
                UIEntity ui = scrollEntities[i];
                bool active = i >= startIndex && i <= lastIndexOnPage;
                ui.SetActive(active, g);
            }
        }
        protected abstract void UpdateScrollEntityPositions(GraphicsComponent g);

        protected void ScrollToPrev(GraphicsComponent g)
        {
            startIndex--;
            UpdateScrollEntitesActive(g);
            UpdateScrollEntityPositions(g);
            UpdateScrollButtonsActive();

            if (onScrollHandler != null)
                onScrollHandler(startIndex);
        }
        protected void ScrollToNext(GraphicsComponent g)
        {
            startIndex++;
            UpdateScrollEntitesActive(g);
            UpdateScrollEntityPositions(g);
            UpdateScrollButtonsActive();

            if (onScrollHandler != null)
                onScrollHandler(startIndex);
        }
    }
}
