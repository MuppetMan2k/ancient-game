﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class HorizontalScrollPane : ScrollPane
    {
        /* -------- Constructors -------- */
        public HorizontalScrollPane(Point inPosition, eUIAnchor inAnchor, int inWidth, int inEntityWidth, int inEntityHeight, eUIFocus inFocus, List<UIEntity> inScrollEntities, int inStartIndex, UIStyle style, ScrollPane.OnScrollMethod inScrollMethod, GraphicsComponent g, ContentManager content)
            : base(inPosition, inAnchor, (inWidth - 2 * SCROLL_BUTTON_SIZE) / inEntityWidth * inEntityWidth + 2 * SCROLL_BUTTON_SIZE, inEntityHeight, inEntityWidth, inEntityHeight, (inWidth - 2 * SCROLL_BUTTON_SIZE) / inEntityWidth, inFocus, inScrollEntities, inStartIndex, style, inScrollMethod, g, content)
        {
            scrollIconTex = AssetLoader.LoadTexture(style.ScrollIconHorizTexID, content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            UIUtilities.SBWrapTextureAcrossRectangle(g, backgroundTex, Rectangle);
            g.SpriteBatch.Draw(vignetteTex, Rectangle, Color.White);

            if (prevScrollButtonActive)
            {
                Rectangle scrollIconRect = new Rectangle(Rectangle.Left + SCROLL_BUTTON_SIZE / 2 - SCROLL_ICON_SIZE / 2, Rectangle.Center.Y - SCROLL_ICON_SIZE / 2, SCROLL_ICON_SIZE, SCROLL_ICON_SIZE);
                g.SpriteBatch.Draw(scrollIconTex, scrollIconRect, Color.White);
            }

            if (nextScrollButtonActive)
            {
                Rectangle scrollIconRect = new Rectangle(Rectangle.Right - SCROLL_BUTTON_SIZE / 2 - SCROLL_ICON_SIZE / 2, Rectangle.Center.Y - SCROLL_ICON_SIZE / 2, SCROLL_ICON_SIZE, SCROLL_ICON_SIZE);
                g.SpriteBatch.Draw(scrollIconTex, scrollIconRect, null, Color.White, 0f, new Vector2(), SpriteEffects.FlipHorizontally, 0f);
            }

            base.SBDraw(g);
        }

        public override void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {
            Point mousePos = i.GetMousePosition();
            int relMouseX = mousePos.X - Rectangle.Left;

            if (relMouseX < SCROLL_BUTTON_SIZE && prevScrollButtonActive)
                ScrollToPrev(g);
            else if (relMouseX > Rectangle.Height - SCROLL_BUTTON_SIZE && nextScrollButtonActive)
                ScrollToNext(g);

            base.OnMouseLeftClick(g, i);
        }

        /* -------- Private Methods --------- */
        protected override void UpdateScrollEntityPositions(GraphicsComponent g)
        {
            int lastIndex = Mathf.Min(startIndex + entitiesPerPage, scrollEntities.Count) - 1;
            for (int i = startIndex; i <= lastIndex; i++)
            {
                int pageIndex = i - startIndex;
                Point newPosition = new Point(SCROLL_BUTTON_SIZE + pageIndex * entityHeight, 0);
                scrollEntities[i].SetPosition(newPosition);
                scrollEntities[i].ForceUpdateRectangle(g);
            }
        }
    }
}
