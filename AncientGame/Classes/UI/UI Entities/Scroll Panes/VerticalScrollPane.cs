﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class VerticalScrollPane : ScrollPane
    {
        /* -------- Constructors -------- */
        public VerticalScrollPane(Point inPosition, eUIAnchor inAnchor, int inHeight, int inEntityWidth, int inEntityHeight, eUIFocus inFocus, List<UIEntity> inScrollEntities, int inScrollIndex, UIStyle style, ScrollPane.OnScrollMethod inScrollMethod, GraphicsComponent g, ContentManager content)
            : base(inPosition, inAnchor, inEntityWidth, (inHeight - 2 * SCROLL_BUTTON_SIZE) / inEntityHeight * inEntityHeight + 2 * SCROLL_BUTTON_SIZE, inEntityWidth, inEntityHeight, (inHeight - 2 * SCROLL_BUTTON_SIZE) / inEntityHeight, inFocus, inScrollEntities, inScrollIndex, style, inScrollMethod, g, content)
        {
            scrollIconTex = AssetLoader.LoadTexture(style.ScrollIconVertTexID, content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            UIUtilities.SBWrapTextureAcrossRectangle(g, backgroundTex, Rectangle);
            g.SpriteBatch.Draw(vignetteTex, Rectangle, Color.White);

            if (prevScrollButtonActive)
            {
                Rectangle scrollIconRect = new Rectangle(Rectangle.Center.X - SCROLL_ICON_SIZE / 2, Rectangle.Top + SCROLL_BUTTON_SIZE / 2 - SCROLL_ICON_SIZE / 2, SCROLL_ICON_SIZE, SCROLL_ICON_SIZE);
                g.SpriteBatch.Draw(scrollIconTex, scrollIconRect, Color.White);
            }

            if (nextScrollButtonActive)
            {
                Rectangle scrollIconRect = new Rectangle(Rectangle.Center.X - SCROLL_ICON_SIZE / 2, Rectangle.Bottom - SCROLL_BUTTON_SIZE / 2 - SCROLL_ICON_SIZE / 2, SCROLL_ICON_SIZE, SCROLL_ICON_SIZE);
                g.SpriteBatch.Draw(scrollIconTex, scrollIconRect, null, Color.White, 0f, new Vector2(), SpriteEffects.FlipVertically, 0f);
            }

            base.SBDraw(g);
        }

        public override void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {
            Point mousePos = i.GetMousePosition();
            int relMouseY = mousePos.Y - Rectangle.Top;

            if (relMouseY < SCROLL_BUTTON_SIZE && prevScrollButtonActive)
                ScrollToPrev(g);
            else if (relMouseY > Rectangle.Height - SCROLL_BUTTON_SIZE && nextScrollButtonActive)
                ScrollToNext(g);

            base.OnMouseLeftClick(g, i);
        }

        /* -------- Private Methods --------- */
        protected override void UpdateScrollEntityPositions(GraphicsComponent g)
        {
            int lastIndex = Mathf.Min(startIndex + entitiesPerPage, scrollEntities.Count) - 1;
            for (int i = startIndex; i <= lastIndex; i++)
            {
                int pageIndex = i - startIndex;
                Point newPosition = new Point(0, SCROLL_BUTTON_SIZE + pageIndex * entityHeight);
                scrollEntities[i].SetPosition(newPosition);
                scrollEntities[i].ForceUpdateRectangle(g);
            }
        }
    }
}
