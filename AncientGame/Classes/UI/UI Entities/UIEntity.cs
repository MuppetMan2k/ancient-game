﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eUIAnchor
    {
        TOP_LEFT,
        TOP_CENTER,
        TOP_RIGHT,
        CENTER_LEFT,
        CENTER_CENTER,
        CENTER_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_CENTER,
        BOTTOM_RIGHT
    }

    enum eUIType
    {
        STATIC,
        INTERACTIVE
    }

    enum eUIFocus
    {
        NO_FOCUS,
        PERMANENT_SCREEN_UI,
        WINDOW,
        ALERT,
        MENU
    }

    abstract class UIEntity
    {
        /* -------- Properties -------- */
        public Point Position { get; private set; }
        public eUIAnchor Anchor { get; private set; }
        public Rectangle Rectangle { get; protected set; }
        public eUIFocus Focus { get; private set; }
        public List<UIEntity> Children { get; private set; }
        public TooltipInfo TooltipInfo { get; private set; }
        public bool Active { get; private set; }
        
        /* -------- Private Fields -------- */
        private UIEntity parent;
        protected bool mouseIsOver;
        private List<UIEntity> childRemovalTempList;

        /* -------- Constructors -------- */
        public UIEntity(Point inPosition, eUIAnchor inAnchor, int inWidth, int inHeight, eUIFocus inFocus, TooltipInfo inTooltipInfo)
        {
            Position = inPosition;
            Anchor = inAnchor;

            parent = null;
            Children = new List<UIEntity>();
            childRemovalTempList = new List<UIEntity>();

            Rectangle = new Rectangle(0, 0, inWidth, inHeight);

            Focus = inFocus;

            TooltipInfo = inTooltipInfo;

            Active = true;
        }

        /* -------- Public Methods -------- */
        public virtual void Update(GameTimeWrapper gtw, GraphicsComponent g, InputComponent i)
        {
            UpdateRectangle(g);

            if (!HasChildren())
                return;

            foreach (UIEntity ui in Children)
                if (ui.Active)
                    ui.Update(gtw, g, i);

            foreach (UIEntity ui in childRemovalTempList)
                Children.Remove(ui);
            childRemovalTempList.Clear();
        }
        public virtual void ForceUpdateRectangle(GraphicsComponent g)
        {
            UpdateRectangle(g);

            if (!HasChildren())
                return;

            foreach (UIEntity ui in Children)
                ui.ForceUpdateRectangle(g);
        }
        public virtual void Refresh(GraphicsComponent g, bool recurse)
        {
            ForceUpdateRectangle(g);

            if (!HasChildren())
                return;

            if (recurse)
                foreach (UIEntity ui in Children)
                    ui.Refresh(g, recurse);
        }

        public virtual void SBDraw(GraphicsComponent g)
        {
            if (!HasChildren())
                return;

            foreach (UIEntity ui in Children)
                if (ui.Active)
                    ui.SBDraw(g);
        }

        public virtual bool MouseIsOver(InputComponent i)
        {
            if (!Active)
                return false;

            return UIUtilities.MouseIsInRectangle(Rectangle, i);
        }
        public virtual void OnMouseOver()
        {
            mouseIsOver = true;
        }
        public virtual void OnMouseOut()
        {
            mouseIsOver = false;
        }
        public virtual void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {

        }
        public virtual void OnMouseRightClick(GraphicsComponent g, InputComponent i)
        {

        }

        public void SetChild(UIEntity child)
        {
            Children.Add(child);
            child.parent = this;
        }
        public bool HasChildren()
        {
            return (Children.Count > 0);
        }
        public void RemoveChild(UIEntity child)
        {
            childRemovalTempList.Add(child);
        }
        public void RemoveAllChildren()
        {
            childRemovalTempList.AddRange(Children);
        }

        public virtual eUIType GetUIType()
        {
            return eUIType.INTERACTIVE;
        }

        public void SetActive(bool inActive, GraphicsComponent g)
        {
            Active = inActive;

            if (Active)
                ForceUpdateRectangle(g);
        }
        public void SetPosition(Point inPosition)
        {
            Position = inPosition;
        }
        public void SetTooltipInfo(TooltipInfo inTooltipInfo)
        {
            TooltipInfo = inTooltipInfo;
        }

        /* -------- Private Methods --------- */
        private void UpdateRectangle(GraphicsComponent g)
        {
            Rectangle newRect = Rectangle;
            Point rectTopLeft = UIUtilities.GetUIRectangleTopLeft(Position, Anchor, Rectangle, parent, g);
            newRect.Location = rectTopLeft;
            Rectangle = newRect;
        }

        protected Rectangle GetAggregateChildRectangle()
        {
            if (Children.Count == 0)
                return new Rectangle();

            Rectangle union = Children[0].Rectangle;
            for (int i = 1; i < Children.Count; i++)
            {
                UIEntity child = Children[i];
                union = Rectangle.Union(union, child.Rectangle);
            }

            return union;
        }
    }
}
