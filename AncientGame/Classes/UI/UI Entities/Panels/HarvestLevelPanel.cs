﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class HarvestLevelPanel : Panel
    {
        /* -------- Constructors -------- */
        public HarvestLevelPanel(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, HarvestState inHarvestState, UIStyle style, ModifierSet modifierSet, ContentManager content)
            : base(inPosition, inAnchor, inSize, inFocus, GetTooltipInfo(inHarvestState, style, modifierSet), style.PanelBackgroundTextureID, style.PanelBorderTextureID, GetPanelTextureID(inHarvestState), content)
        {
        }

        /* -------- Static Methods -------- */
        private static string GetPanelTextureID(HarvestState inHarvestState)
        {
            float harvestFraction = inHarvestState.GetHarvestLevelFraction();
            if (harvestFraction <= 0.33f)
                return "harvest-low-panel";
            else if (harvestFraction <= 0.66f)
                return "harvest-medium-panel";
            else
                return "harvest-high-panel";
        }
        private static TooltipInfo GetTooltipInfo(HarvestState inHarvestState, UIStyle style, ModifierSet modifierSet)
        {
            Color numberColor;
            float harvestFraction = inHarvestState.GetHarvestLevelFraction();
            if (harvestFraction <= 0.33f)
                numberColor = style.NegativeColor;
            else if (harvestFraction <= 0.66f)
                numberColor = style.NeutralColor;
            else
                numberColor = style.PositiveColor;
            List<ColoredTextSegment> staticTextSegments = new List<ColoredTextSegment>();
            staticTextSegments.Add(new ColoredTextSegment(Localization.Localize("text_harvest_level"), style.TooltipTextDefaultColor));
            staticTextSegments.Add(new ColoredTextSegment(UIUtilities.GetIntDisplayString(inHarvestState.HarvestLevel), numberColor));

            return TooltipInfo.CreateModifierSetTooltipInfo(staticTextSegments, modifierSet);
        }
    }
}
