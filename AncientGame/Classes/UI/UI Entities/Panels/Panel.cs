﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class Panel : UIEntity
    {
        /* -------- Private Fields -------- */
        private Texture2D backgroundTex;
        private Texture2D borderTex;
        protected Texture2D panelTex;
        // Constants
        protected const float TIMER_SIZE_FRACTION = 0.6f;

        /* -------- Constructors -------- */
        public Panel(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, TooltipInfo inTooltipInfo, string backgroundTextureID, string borderTextureID, string panelTextureID, ContentManager content)
            : base(inPosition, inAnchor, inSize, inSize, inFocus, inTooltipInfo)
        {
            backgroundTex = AssetLoader.LoadTexture(backgroundTextureID, content);
            borderTex = AssetLoader.LoadTexture(borderTextureID, content);

            if (panelTextureID != null)
                panelTex = AssetLoader.LoadTexture(panelTextureID, content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            UIUtilities.SBWrapTextureAcrossRectangle(g, backgroundTex, Rectangle);
            if (panelTex != null)
                g.SpriteBatch.Draw(panelTex, Rectangle, Color.White);
            g.SpriteBatch.Draw(borderTex, Rectangle, Color.White);

            base.SBDraw(g);
        }
    }
}
