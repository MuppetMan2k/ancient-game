﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class CapitolUpgradePanel : Panel
    {
        /* -------- Constructors -------- */
        public CapitolUpgradePanel(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, UIStyle style, ContentManager content)
            : base(inPosition, inAnchor, inSize, inFocus, GetTooltipInfo(), style.PanelBackgroundTextureID, style.PanelBorderTextureID, GetPanelTextureID(), content)
        {
        }

        /* -------- Public Methods -------- */
        public override void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {
            base.OnMouseLeftClick(g, i);

            // TODO - set as capitol if requirements are met
        }

        /* -------- Static Methods -------- */
        private static string GetPanelTextureID()
        {
            return "debug-capitol-settlement-panel";
        }
        private static TooltipInfo GetTooltipInfo()
        {
            return TooltipInfo.CreateStaticTextTooltipInfo("Click to make capitol settlement. Requirements to upgrade to capitol settlement"); // TODO
        }
    }
}
