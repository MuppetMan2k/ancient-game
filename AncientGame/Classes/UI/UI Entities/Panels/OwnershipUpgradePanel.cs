﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class OwnershipUpgradePanel : Panel
    {
        /* -------- Private Fields -------- */
        LandTerritoryControl control;

        /* -------- Constructors -------- */
        public OwnershipUpgradePanel(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, LandTerritoryControl inControl, UIStyle style, ContentManager content)
            : base(inPosition, inAnchor, inSize, inFocus, GetTooltipInfo(inControl), style.PanelBackgroundTextureID, style.PanelBorderTextureID, GetPanelTextureID(inControl), content)
        {
            control = inControl;

            if (control.DevelopmentRemainingTurns > 0)
            {
                PanelTimer timer = new PanelTimer(new Point(), eUIAnchor.CENTER_CENTER, Mathf.Round(inSize * TIMER_SIZE_FRACTION), inFocus, style, control.DevelopmentRemainingTurns, content);
                SetChild(timer);
            }
        }

        /* -------- Public Methods -------- */
        public override void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {
            base.OnMouseLeftClick(g, i);

            if (control.DevelopmentRemainingTurns > 0)
            {
                // TODO - cancel developing control level
            }
            else
            {
                switch (control.CurrControlLevel)
                {
                    case eLandTerritoryControlLevel.MILITARY_OWNERSHIP:
                        // TODO - start upgrade to annexed ownership if requirements are met
                        break;
                    case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                        // TODO - start upgrade to homeland ownership if requirements are met
                        break;
                    default:
                        break;
                }
            }
        }

        /* -------- Static Methods -------- */
        private static string GetPanelTextureID(LandTerritoryControl inControl)
        {
            if (inControl.DevelopmentRemainingTurns > 0)
            {
                switch (inControl.DevelopingControlLevel)
                {
                    case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                        return "debug-annexed-ownership-panel";
                    case eLandTerritoryControlLevel.HOMELAND_OWNERSHIP:
                        return "debug-homeland-ownership-panel";
                    default:
                        return "";
                }
            }
            else
            {
                switch (inControl.CurrControlLevel)
                {
                    case eLandTerritoryControlLevel.MILITARY_OWNERSHIP:
                        return "debug-annexed-ownership-panel";
                    case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                        return "debug-homeland-ownership-panel";
                    default:
                        return "";
                }
            }
        }
        private static TooltipInfo GetTooltipInfo(LandTerritoryControl inControl)
        {
            if (inControl.DevelopmentRemainingTurns > 0)
            {
                string turnText = (inControl.DevelopmentRemainingTurns == 1) ? Localization.Localize("text_turn") : Localization.Localize("text_turns");
                turnText = String.Format(turnText, inControl.DevelopmentRemainingTurns.ToString("0"));
                string upgradingControlText;
                switch (inControl.DevelopingControlLevel)
                {
                    case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                        upgradingControlText = Localization.Localize("text_annexed_ownership");
                        break;
                    case eLandTerritoryControlLevel.HOMELAND_OWNERSHIP:
                        upgradingControlText = Localization.Localize("text_homeland_ownership");
                        break;
                    default:
                        upgradingControlText = "";
                        break;
                }

                string text = Localization.Localize("text_upgrading_in_click_to_cancel");
                text = String.Format(text, upgradingControlText, turnText);
                return TooltipInfo.CreateStaticTextTooltipInfo(text);
            }
            else
            {
                switch (inControl.CurrControlLevel)
                {
                    case eLandTerritoryControlLevel.MILITARY_OWNERSHIP:
                        {
                            return TooltipInfo.CreateStaticTextTooltipInfo("Click to upgrade to military governed territory. Requirements to upgrade to military governed territory"); // TODO
                        }
                    case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                        {
                            return TooltipInfo.CreateStaticTextTooltipInfo("Click to upgrade to annexed territory. Requirements to upgrade to annexed territory"); // TODO
                        }
                    default:
                        return null;
                }
            }
        }
    }
}
