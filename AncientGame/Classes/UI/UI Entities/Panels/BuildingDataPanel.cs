﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class BuildingDataPanel : Panel
    {
        /* -------- Private Fields -------- */
        private BuildingData data;
        private BuildingSetDiv buildingSetDiv;
        private LandTerritory landTerritory;
        private GameManagerContainer gameManagers;
        private bool buildable;
        // Assets
        private Texture2D pixelTex;
        // Constants
        private const float UNBUILDABLE_MASK_ALPHA = 0.5f;
        private const int ICON_OFFSET = 8;
        private const int RESOURCE_ICON_MAX_OFFSET = 24;

        /* -------- Constructors -------- */
        public BuildingDataPanel(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, BuildingData inData, BuildingSetDiv inBuildingSetDiv, LandTerritory lt, UIStyle style, GameManagerContainer inGameManagers, ContentManager content)
            : base(inPosition, inAnchor, inSize, inFocus, GetTooltipInfo(inData, lt, inGameManagers), style.PanelBackgroundTextureID, style.PanelBorderTextureID, inData.textureID, content)
        {
            data = inData;
            buildingSetDiv = inBuildingSetDiv;
            landTerritory = lt;
            gameManagers = inGameManagers;

            pixelTex = AssetLoader.LoadTexture("white-pixel", content);

            buildable = (Building.GetBuildability(data, gameManagers, landTerritory) == eBuildabilityResult.CAN_BUILD);

            BuildingRequirements requirements = BuildingRequirements.CreateFromData(data.requirementsData, inGameManagers);
            for (int i = 0; i < requirements.RequiredResources.Count; i++)
            {
                Resource r = requirements.RequiredResources[i];

                int xOffset = (requirements.RequiredResources.Count == 1) ? ICON_OFFSET : RESOURCE_ICON_MAX_OFFSET - Mathf.Round((float)(RESOURCE_ICON_MAX_OFFSET - ICON_OFFSET) * (float)i / (float)(requirements.RequiredResources.Count - 1));

                Point point = new Point(xOffset, ICON_OFFSET);
                TooltipInfo resourceTooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(r.GetName());
                Icon resourceIcon = new Icon(point, eUIAnchor.TOP_RIGHT, inFocus, resourceTooltipInfo, r.IconTextureID, content);
                SetChild(resourceIcon);
            }
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            base.SBDraw(g);

            if (!buildable)
                g.SpriteBatch.Draw(pixelTex, Rectangle, Color.Black * UNBUILDABLE_MASK_ALPHA);
        }

        public override void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {
            base.OnMouseLeftClick(g, i);

            if (Building.GetBuildability(data, gameManagers, landTerritory) != eBuildabilityResult.CAN_BUILD)
                return;

            buildingSetDiv.OnConstructionStarted(data);
        }

        /* -------- Static Methods -------- */
        private static TooltipInfo GetTooltipInfo(BuildingData data, LandTerritory lt, GameManagerContainer inGameManagers)
        {
            Cost cost = Cost.CreateFromData(data.costData);
            BuildingRequirements requirements = BuildingRequirements.CreateFromData(data.requirementsData, inGameManagers);
            eBuildabilityResult buildabilityResult = Building.GetBuildability(data, inGameManagers, lt);

            return TooltipInfo.CreateBuildingDataTooltipInfo(data, requirements, cost, buildabilityResult);
        }
    }
}
