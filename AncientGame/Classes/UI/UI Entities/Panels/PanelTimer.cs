﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class PanelTimer : UIEntity
    {
        /* -------- Private Fields -------- */
        private Color color;
        // Assets
        private Texture2D timerTex;

        /* -------- Constructors -------- */
        public PanelTimer(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, UIStyle inStyle, int remainingTurns, ContentManager content)
            : base(inPosition, inAnchor, inSize, inSize, inFocus, null)
        {
            if (remainingTurns == 1)
                color = inStyle.PositiveColor;
            else if (remainingTurns == 2)
                color = inStyle.NeutralColor;
            else
                color = inStyle.NegativeColor;

            timerTex = AssetLoader.LoadTexture("panel-timer", content);

            string numberTooltipString = Localization.Localize((remainingTurns == 1) ? "text_turn_remaining" : "text_turns_remaining");
            numberTooltipString = String.Format(numberTooltipString, UIUtilities.GetIntDisplayString(remainingTurns));
            TooltipInfo numberTooltipInfo = TooltipInfo.CreateStaticTextTooltipInfo(numberTooltipString);
            FittedText fittedText = new FittedText(new Point(), eUIAnchor.CENTER_CENTER, ColoredTextSegment.GetSegmentsForSingleText(remainingTurns.ToString("0"), inStyle.PanelTimerTextColor), inStyle.PanelTimerFontID, eTextAlignment.CENTER, inSize, inFocus, numberTooltipInfo, content);
            SetChild(fittedText);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(timerTex, Rectangle, color);

            base.SBDraw(g);
        }

        public override eUIType GetUIType()
        {
            return eUIType.STATIC;
        }
    }
}
