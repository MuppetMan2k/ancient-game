﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class FertilityLevelPanel : Panel
    {
        /* -------- Constructors -------- */
        public FertilityLevelPanel(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, int inFertilityLevel, UIStyle style, ModifierSet modifierSet, ContentManager content)
            : base(inPosition, inAnchor, inSize, inFocus, GetTooltipInfo(inFertilityLevel, style, modifierSet), style.PanelBackgroundTextureID, style.PanelBorderTextureID, GetPanelTextureID(inFertilityLevel), content)
        {
        }

        /* -------- Static Methods -------- */
        private static string GetPanelTextureID(int inFertilityLevel)
        {
            int maxFertility = 10;

            float fertilityFraction = (float)inFertilityLevel / (float)maxFertility;
            if (fertilityFraction <= 0.33f)
                return "fertility-low-panel";
            else if (fertilityFraction <= 0.66f)
                return "fertility-medium-panel";
            else
                return "fertility-high-panel";
        }
        private static TooltipInfo GetTooltipInfo(int inFertilityLevel, UIStyle style, ModifierSet modifierSet)
        {
            int maxFertility = 10;

            Color numberColor;
            float fertilityFraction = (float)inFertilityLevel / (float)maxFertility;
            if (fertilityFraction <= 0.33f)
                numberColor = style.NegativeColor;
            else if (fertilityFraction <= 0.66f)
                numberColor = style.NeutralColor;
            else
                numberColor = style.PositiveColor;
            List<ColoredTextSegment> staticTextSegments = new List<ColoredTextSegment>();
            staticTextSegments.Add(new ColoredTextSegment(Localization.Localize("text_fertility_level"), style.TooltipTextDefaultColor));
            staticTextSegments.Add(new ColoredTextSegment(UIUtilities.GetIntDisplayString(inFertilityLevel), numberColor));

            return TooltipInfo.CreateModifierSetTooltipInfo(staticTextSegments, modifierSet);
        }
    }
}
