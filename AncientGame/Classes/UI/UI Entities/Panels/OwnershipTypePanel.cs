﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class OwnershipTypePanel : Panel
    {
        /* -------- Constructors -------- */
        public OwnershipTypePanel(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, eLandTerritoryControlLevel inControlLevel, UIStyle style, ModifierSet modifierSet, ContentManager content)
            : base(inPosition, inAnchor, inSize, inFocus, GetTooltipInfo(inControlLevel, modifierSet), style.PanelBackgroundTextureID, style.PanelBorderTextureID, GetPanelTextureID(inControlLevel), content)
        {
        }

        /* -------- Static Methods -------- */
        private static string GetPanelTextureID(eLandTerritoryControlLevel inControlLevel)
        {
            switch (inControlLevel)
            {
                case eLandTerritoryControlLevel.MILITARY_OWNERSHIP:
                    return "debug-military-ownership-panel";
                case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                    return "debug-annexed-ownership-panel";
                case eLandTerritoryControlLevel.HOMELAND_OWNERSHIP:
                    return "debug-homeland-ownership-panel";
                default:
                    return "";
            }
        }
        private static TooltipInfo GetTooltipInfo(eLandTerritoryControlLevel inControlLevel, ModifierSet modifierSet)
        {
            string text = "";
            switch (inControlLevel)
            {
                case eLandTerritoryControlLevel.MILITARY_OWNERSHIP:
                    text = Localization.Localize("text_military_ownership");
                    break;
                case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                    text = Localization.Localize("text_annexed_ownership");
                    break;
                case eLandTerritoryControlLevel.HOMELAND_OWNERSHIP:
                    text = Localization.Localize("text_homeland_ownership");
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inControlLevel);
                    break;
            }

            return TooltipInfo.CreateModifierSetTooltipInfo(text, modifierSet);
        }
    }
}
