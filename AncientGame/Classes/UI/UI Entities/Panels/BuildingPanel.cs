﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eBuildingPanelType
    {
        CURRENT_BUILDING,
        DEVELOPING_BUILDING
    }

    class BuildingPanel : Panel
    {
        /* -------- Private Fields -------- */
        private BuildingSlot buildingSlot;
        private eBuildingPanelType type;
        BuildingSetDiv buildingSetDiv;
        // Constants
        private const int ICON_OFFSET = 8;

        /* -------- Constructors -------- */
        public BuildingPanel(Point inPoint, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, UIStyle inStyle, BuildingSlot inBuildingSlot, eBuildingPanelType panelType, BuildingSetDiv inBuildingSetDiv, ContentManager content)
            : base(inPoint, inAnchor, inSize, inFocus, GetTooltipInfo(inBuildingSlot, panelType), inStyle.PanelBackgroundTextureID, GetPanelBorderTextureID(inStyle, panelType, inBuildingSetDiv, inBuildingSlot), GetTextureID(inBuildingSlot, panelType), content)
        {
            buildingSlot = inBuildingSlot;
            buildingSetDiv = inBuildingSetDiv;
            type = panelType;

            if (buildingSlot == null)
                return;

            Building building = null;
            if (type == eBuildingPanelType.CURRENT_BUILDING)
                building = buildingSlot.CurrentBuilding;
            if (type == eBuildingPanelType.DEVELOPING_BUILDING)
                building = buildingSlot.DevelopingBuilding;

            if (building == null)
                return;

            if (building.DamageLevel != eBuildingDamageLevel.NO_DAMAGE)
            {
                string damageIconTextureID;
                string damageTooltipString;
                switch (building.DamageLevel)
                {
                    case eBuildingDamageLevel.LIGHT_DAMAGE:
                        damageIconTextureID = "light-damage-icon";
                        damageTooltipString = Localization.Localize("text_light_damage");
                        break;
                    case eBuildingDamageLevel.MEDIUM_DAMAGE:
                        damageIconTextureID = "medium-damage-icon";
                        damageTooltipString = Localization.Localize("text_medium_damage");
                        break;
                    case eBuildingDamageLevel.HEAVY_DAMAGE:
                        damageIconTextureID = "heavy-damage-icon";
                        damageTooltipString = Localization.Localize("text_heavy_damage");
                        break;
                    default:
                        Debug.LogUnrecognizedValueWarning(building.DamageLevel);
                        damageIconTextureID = null;
                        damageTooltipString = null;
                        break;
                }
                Icon damageIcon = new Icon(new Point(ICON_OFFSET, ICON_OFFSET), eUIAnchor.TOP_LEFT, inFocus, TooltipInfo.CreateStaticTextTooltipInfo(damageTooltipString), damageIconTextureID, content);
                SetChild(damageIcon);
            }

            int developmentTurnsLeft = Mathf.Max(building.TurnsUntilCompletion, building.TurnsUntilRepair, building.TurnsUntilDemolition);
            if (developmentTurnsLeft > 0)
            {
                int timerSize = Mathf.Round(TIMER_SIZE_FRACTION * (float)Rectangle.Width);
                PanelTimer timer = new PanelTimer(new Point(), eUIAnchor.CENTER_CENTER, timerSize, inFocus, inStyle, developmentTurnsLeft, content);
                SetChild(timer);
            }
        }

        /* -------- Public Methods -------- */
        public override void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {
            base.OnMouseLeftClick(g, i);

            if (type == eBuildingPanelType.DEVELOPING_BUILDING)
                return;

            buildingSetDiv.OnCurrentBuildingPanelPressed(buildingSlot);
        }

        /* -------- Static Methods -------- */
        private static TooltipInfo GetTooltipInfo(BuildingSlot inBuildingSlot, eBuildingPanelType panelType)
        {
            if (panelType == eBuildingPanelType.DEVELOPING_BUILDING && inBuildingSlot == null)
                return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_no_construction_tooltip"));

            if (panelType == eBuildingPanelType.CURRENT_BUILDING)
            {
                if (inBuildingSlot.CurrentBuilding != null)
                    return TooltipInfo.CreateBuildingTooltipInfo(inBuildingSlot.CurrentBuilding);

                switch (inBuildingSlot.Type)
                {
                    case eBuildingSlotType.CENTRAL:
                        return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_central_building_slot"));
                    case eBuildingSlotType.WALLS:
                        return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_walls_building_slot"));
                    case eBuildingSlotType.SMALL_PORT:
                        return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_small_port_building_slot"));
                    case eBuildingSlotType.LARGE_PORT:
                        return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_large_port_building_slot"));
                    case eBuildingSlotType.STANDARD_SETTLEMENT:
                        return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_standard_settlement_building_slot"));
                    case eBuildingSlotType.ROADS:
                        return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_roads_building_slot"));
                    case eBuildingSlotType.STANDARD_TERRITORY:
                        return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_standard_territory_building_slot"));
                    default:
                        Debug.LogUnrecognizedValueWarning(inBuildingSlot.Type);
                        return null;
                }
            }
            else
            {
                if (inBuildingSlot.CurrentBuilding != null && inBuildingSlot.CurrentBuilding.IsBeingRepaired)
                    return TooltipInfo.CreateBuildingTooltipInfo(inBuildingSlot.CurrentBuilding);

                if (inBuildingSlot.DevelopingBuilding != null)
                    return TooltipInfo.CreateBuildingTooltipInfo(inBuildingSlot.DevelopingBuilding);

                return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_no_developing_building")); 
            }
        }
        private static string GetTextureID(BuildingSlot inBuildingSlot, eBuildingPanelType panelType)
        {
            if (panelType == eBuildingPanelType.DEVELOPING_BUILDING && inBuildingSlot == null)
                return null;

            Building building = (panelType == eBuildingPanelType.CURRENT_BUILDING) ? inBuildingSlot.CurrentBuilding : inBuildingSlot.DevelopingBuilding;

            if (building != null)
                return building.Data.textureID;

            switch (inBuildingSlot.Type)
            {
                case eBuildingSlotType.CENTRAL:
                    return "central-building-slot";
                case eBuildingSlotType.WALLS:
                    return "walls-building-slot";
                case eBuildingSlotType.SMALL_PORT:
                    return "small-port-building-slot";
                case eBuildingSlotType.LARGE_PORT:
                    return "large-port-building-slot";
                case eBuildingSlotType.STANDARD_SETTLEMENT:
                    return "standard-settlement-building-slot";
                case eBuildingSlotType.ROADS:
                    return "roads-building-slot";
                case eBuildingSlotType.STANDARD_TERRITORY:
                    return "standard-territory-building-slot";
                default:
                    Debug.LogUnrecognizedValueWarning(inBuildingSlot.Type);
                    return "";
            }
        }
        private static string GetPanelBorderTextureID(UIStyle inStyle, eBuildingPanelType panelType, BuildingSetDiv buildingSetDiv, BuildingSlot inBuildingSlot)
        {
            if (panelType == eBuildingPanelType.DEVELOPING_BUILDING)
                return inStyle.PanelBorderTextureID;

            if (buildingSetDiv.InspectionBuildingSlot != null && buildingSetDiv.InspectionBuildingSlot == inBuildingSlot)
                return inStyle.PanelHighlightedBorderTextureID;

            return inStyle.PanelBorderTextureID;
        }
    }
}
