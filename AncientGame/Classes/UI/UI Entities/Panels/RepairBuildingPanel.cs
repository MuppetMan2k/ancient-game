﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RepairBuildingPanel : Panel
    {
        /* -------- Private Fields -------- */
        private BuildingSlot buildingSlot;
        private BuildingSetDiv buildingSetDiv;
        private LandTerritory landTerritory;
        private GameManagerContainer gameManagers;
        private Cost repairCost;
        private bool available;
        // Assets
        private Texture2D pixelTex;
        private const float UNAVAILABLE_MASK_ALPHA = 0.5f;

        /* -------- Constructors -------- */
        public RepairBuildingPanel(Point inPoint, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, BuildingSlot inBuildingSlot, BuildingSetDiv inBuildingSetDiv, LandTerritory inLandTerritory, GameManagerContainer inGameManagers, UIStyle style, ContentManager content)
            : base(inPoint, inAnchor, inSize, inFocus, GetTooltipInfo(inBuildingSlot, inLandTerritory, inGameManagers), style.PanelBackgroundTextureID, style.PanelBorderTextureID, "repair-building", content)
        {
            buildingSlot = inBuildingSlot;
            buildingSetDiv = inBuildingSetDiv;
            landTerritory = inLandTerritory;
            gameManagers = inGameManagers;
            repairCost = inBuildingSlot.CurrentBuilding.GetRepairCost(landTerritory, gameManagers);

            available = repairCost.CanAfford(landTerritory);

            pixelTex = AssetLoader.LoadTexture("white-pixel", content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            base.SBDraw(g);

            if (!available)
                g.SpriteBatch.Draw(pixelTex, Rectangle, Color.Black * UNAVAILABLE_MASK_ALPHA);
        }

        public override void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {
 	        base.OnMouseLeftClick(g, i);

            Cost cost = buildingSlot.CurrentBuilding.GetRepairCost(landTerritory, gameManagers);
            if (!cost.CanAfford(landTerritory))
                return;

            buildingSetDiv.OnCurrentBuildingRepairStarted();
        }

        /* -------- Static Methods -------- */
        public static TooltipInfo GetTooltipInfo(BuildingSlot inBuildingSlot, LandTerritory inLandTerritory, GameManagerContainer inGameManagers)
        {
            Cost cost = inBuildingSlot.CurrentBuilding.GetRepairCost(inLandTerritory, inGameManagers);
            return TooltipInfo.CreateCostTooltipInfo("text_click_to_repair", cost, cost.CanAfford(inLandTerritory));
        }
    }
}
