﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class ClimateTypePanel : Panel
    {
        /* -------- Constructors -------- */
        public ClimateTypePanel(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, string backgroundTextureID, string borderTextureID, eClimateType inClimateType, ModifierSet modifierSet, ContentManager content)
            : base(inPosition, inAnchor, inSize, inFocus, GetTooltipInfo(inClimateType, modifierSet), backgroundTextureID, borderTextureID, GetPanelTextureID(inClimateType), content)
        {
        }

        /* -------- Static Methods -------- */
        private static string GetPanelTextureID(eClimateType inClimateType)
        {
            switch (inClimateType)
            {
                case eClimateType.BAKED:
                    return "climate-baked-panel";
                case eClimateType.HOT_AND_DRY:
                    return "climate-hot-and-dry-panel";
                case eClimateType.HOT_AND_WET:
                    return "climate-hot-and-wet-panel";
                case eClimateType.WARM_AND_DRY:
                    return "climate-warm-and-dry-panel";
                case eClimateType.WARM_AND_WET:
                    return "climate-warm-and-wet-panel";
                case eClimateType.COOL_AND_DRY:
                    return "climate-cool-and-dry-panel";
                case eClimateType.COOL_AND_WET:
                    return "climate-cool-and-wet-panel";
                case eClimateType.COLD_AND_DRY:
                    return "climate-cold-and-dry-panel";
                case eClimateType.COLD_AND_WET:
                    return "climate-cold-and-wet-panel";
                case eClimateType.FROZEN:
                    return "climate-frozen-panel";
                default:
                    Debug.LogUnrecognizedValueWarning(inClimateType);
                    return null;
            }
        }
        private static TooltipInfo GetTooltipInfo(eClimateType inClimateType, ModifierSet modifierSet)
        {
            string staticText;
            switch (inClimateType)
            {
                case eClimateType.BAKED:
                    staticText = Localization.Localize("text_climate_baked");
                    break;
                case eClimateType.HOT_AND_DRY:
                    staticText = Localization.Localize("text_climate_hot-and-dry");
                    break;
                case eClimateType.HOT_AND_WET:
                    staticText = Localization.Localize("text_climate_hot-and-wet");
                    break;
                case eClimateType.WARM_AND_DRY:
                    staticText = Localization.Localize("text_climate_warm-and-dry");
                    break;
                case eClimateType.WARM_AND_WET:
                    staticText = Localization.Localize("text_climate_warm-and-wet");
                    break;
                case eClimateType.COOL_AND_DRY:
                    staticText = Localization.Localize("text_climate_cool-and-dry");
                    break;
                case eClimateType.COOL_AND_WET:
                    staticText = Localization.Localize("text_climate_cool-and-wet");
                    break;
                case eClimateType.COLD_AND_DRY:
                    staticText = Localization.Localize("text_climate_cold-and-dry");
                    break;
                case eClimateType.COLD_AND_WET:
                    staticText = Localization.Localize("text_climate_cold-and-wet");
                    break;
                case eClimateType.FROZEN:
                    staticText = Localization.Localize("text_climate_frozen");
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inClimateType);
                    staticText = null;
                    break;
            }

            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, modifierSet);
        }
    }
}
