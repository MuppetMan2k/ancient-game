﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class SettlementTypePanel : Panel
    {
        /* -------- Constructors -------- */
        public SettlementTypePanel(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, bool inIsCapitol, UIStyle style, ModifierSet modifierSet, ContentManager content)
            : base(inPosition, inAnchor, inSize, inFocus, GetTooltipInfo(inIsCapitol, modifierSet), style.PanelBackgroundTextureID, style.PanelBorderTextureID, GetPanelTextureID(inIsCapitol), content)
        {
        }

        /* -------- Static Methods -------- */
        private static string GetPanelTextureID(bool inIsCapitol)
        {
            return inIsCapitol ? "debug-capitol-settlement-panel" : "debug-non-capitol-settlement-panel";
        }
        private static TooltipInfo GetTooltipInfo(bool inIsCapitol, ModifierSet modifierSet)
        {
            string text = inIsCapitol ? Localization.Localize("text_capitol_settlement") : Localization.Localize("text_non_capitol_settlement");
            return TooltipInfo.CreateModifierSetTooltipInfo(text, modifierSet);
        }
    }
}
