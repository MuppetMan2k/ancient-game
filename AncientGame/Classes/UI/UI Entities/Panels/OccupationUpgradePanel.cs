﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class OccupationUpgradePanel : Panel
    {
        /* -------- Private Fields -------- */
        LandTerritory landTerritory;
        Faction occupyingFaction;

        /* -------- Constructors -------- */
        public OccupationUpgradePanel(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, LandTerritory inLandTerritory, Faction inOccupyingFaction, UIStyle style, ContentManager content)
            : base(inPosition, inAnchor, inSize, inFocus, GetTooltipInfo(inLandTerritory, inOccupyingFaction), style.PanelBackgroundTextureID, style.PanelBorderTextureID, GetPanelTextureID(inLandTerritory, inOccupyingFaction), content)
        {
            landTerritory = inLandTerritory;
            occupyingFaction = inOccupyingFaction;

            LandTerritoryControl control = occupyingFaction.GetLandTerritoryControl(landTerritory);
            if (control != null && control.DevelopmentRemainingTurns > 0)
            {
                PanelTimer timer = new PanelTimer(new Point(), eUIAnchor.CENTER_CENTER, Mathf.Round(inSize * TIMER_SIZE_FRACTION), inFocus, style, control.DevelopmentRemainingTurns, content);
                SetChild(timer);
            }
        }

        /* -------- Public Methods -------- */
        public override void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {
            base.OnMouseLeftClick(g, i);

            LandTerritoryControl control = occupyingFaction.GetLandTerritoryControl(landTerritory);
            if (control != null && control.DevelopmentRemainingTurns > 0)
            {
                // TODO - cancel developing control level
            }
            else
            {
                eLandTerritoryControlLevel upgradedControlLevel = occupyingFaction.GetLandTerritoryControlUpgradedToFromOccupation(landTerritory);
                // TODO - start upgrade to ownership level
            }
        }

        /* -------- Static Methods -------- */
        private static string GetPanelTextureID(LandTerritory inLandTerritory, Faction inOccupyingFaction)
        {
            LandTerritoryControl control = inOccupyingFaction.GetLandTerritoryControl(inLandTerritory);
            if (control != null && control.DevelopmentRemainingTurns > 0)
            {
                switch (control.DevelopingControlLevel)
                {
                    case eLandTerritoryControlLevel.MILITARY_OWNERSHIP:
                        return "debug-militart-ownership-panel";
                    case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                        return "debug-annexed-ownership-panel";
                    case eLandTerritoryControlLevel.HOMELAND_OWNERSHIP:
                        return "debug-homeland-ownership-panel";
                    default:
                        return "";
                }
            }
            else
            {
                eLandTerritoryControlLevel upgradedControlLevel = inOccupyingFaction.GetLandTerritoryControlUpgradedToFromOccupation(inLandTerritory);
                switch (upgradedControlLevel)
                {
                    case eLandTerritoryControlLevel.MILITARY_OWNERSHIP:
                        return "debug-military-ownership-panel";
                    case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                        return "debug-annexed-ownership-panel";
                    case eLandTerritoryControlLevel.HOMELAND_OWNERSHIP:
                        return "debug-homeland-ownership-panel";
                    default:
                        return "";
                }
            }
        }
        private static TooltipInfo GetTooltipInfo(LandTerritory inLandTerritory, Faction inOccupyingFaction)
        {
            LandTerritoryControl control = inOccupyingFaction.GetLandTerritoryControl(inLandTerritory);
            if (control != null && control.DevelopmentRemainingTurns > 0)
            {
                string turnText = (control.DevelopmentRemainingTurns == 1) ? Localization.Localize("text_turn") : Localization.Localize("text_turns");
                turnText = String.Format(turnText, control.DevelopmentRemainingTurns.ToString("0"));
                string upgradingControlText;
                switch (control.DevelopingControlLevel)
                {
                    case eLandTerritoryControlLevel.MILITARY_OWNERSHIP:
                        upgradingControlText = Localization.Localize("text_military_ownership");
                        break;
                    case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                        upgradingControlText = Localization.Localize("text_annexed_ownership");
                        break;
                    case eLandTerritoryControlLevel.HOMELAND_OWNERSHIP:
                        upgradingControlText = Localization.Localize("text_homeland_ownership");
                        break;
                    default:
                        upgradingControlText = "";
                        break;
                }

                string text = Localization.Localize("text_upgrading_in_click_to_cancel");
                text = String.Format(text, upgradingControlText, turnText);
                return TooltipInfo.CreateStaticTextTooltipInfo(text);
            }
            else
            {
                eLandTerritoryControlLevel upgradedControlLevel = inOccupyingFaction.GetLandTerritoryControlUpgradedToFromOccupation(inLandTerritory);
                switch (upgradedControlLevel)
                {
                    case eLandTerritoryControlLevel.MILITARY_OWNERSHIP:
                        {
                            return TooltipInfo.CreateStaticTextTooltipInfo("Click to upgrade to military governed territory. Requirements to upgrade to military governed territory"); // TODO
                        }
                    case eLandTerritoryControlLevel.ANNEXED_OWNERSHIP:
                        {
                            return TooltipInfo.CreateStaticTextTooltipInfo("Click to upgrade to annexed territory. Requirements to upgrade to annexed territory"); // TODO
                        }
                    case eLandTerritoryControlLevel.HOMELAND_OWNERSHIP:
                        {
                            return TooltipInfo.CreateStaticTextTooltipInfo("Click to upgrade to homeland territory. Requirements to upgrade to homeland territory"); // TODO
                        }
                    default:
                        return null;
                }
            }
        }
    }
}
