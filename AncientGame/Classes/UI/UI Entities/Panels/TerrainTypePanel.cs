﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class TerrainTypePanel : Panel
    {
        /* -------- Constructors -------- */
        public TerrainTypePanel(Point inPosition, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, string backgroundTextureID, string borderTextureID, eTerrainType inTerrainType, ModifierSet modifierSet, ContentManager content)
            : base(inPosition, inAnchor, inSize, inFocus, GetTooltipInfo(inTerrainType, modifierSet), backgroundTextureID, borderTextureID, GetPanelTextureID(inTerrainType), content)
        {
        }

        /* -------- Static Methods -------- */
        private static string GetPanelTextureID(eTerrainType inTerrainType)
        {
            switch (inTerrainType)
            {
                case eTerrainType.LOW_GRASS_LAND:
                    return "terrain-low-grass-land-panel";
                case eTerrainType.HILLY_GRASS_LAND:
                    return "terrain-hilly-grass-land-panel";
                case eTerrainType.FOREST:
                    return "terrain-forest-panel";
                case eTerrainType.DESERT:
                    return "terrain-desert-panel";
                case eTerrainType.MOUNTAIN:
                    return "terrain-mountain-panel";
                case eTerrainType.STEPPES:
                    return "terrain-steppes-panel";
                case eTerrainType.MARSH:
                    return "terrain-marsh-panel";
                default:
                    Debug.LogUnrecognizedValueWarning(inTerrainType);
                    return null;
            }
        }
        private static TooltipInfo GetTooltipInfo(eTerrainType inTerrainType, ModifierSet modifierSet)
        {
            string staticText;
            switch (inTerrainType)
            {
                case eTerrainType.LOW_GRASS_LAND:
                    staticText = Localization.Localize("text_terrain_low-grass-land");
                    break;
                case eTerrainType.HILLY_GRASS_LAND:
                    staticText = Localization.Localize("text_terrain_hilly-grass-land");
                    break;
                case eTerrainType.FOREST:
                    staticText = Localization.Localize("text_terrain_forest");
                    break;
                case eTerrainType.DESERT:
                    staticText = Localization.Localize("text_terrain_desert");
                    break;
                case eTerrainType.MOUNTAIN:
                    staticText = Localization.Localize("text_terrain_mountain");
                    break;
                case eTerrainType.STEPPES:
                    staticText = Localization.Localize("text_terrain_steppes");
                    break;
                case eTerrainType.MARSH:
                    staticText = Localization.Localize("text_terrain_marsh");
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inTerrainType);
                    staticText = null;
                    break;
            }

            return TooltipInfo.CreateModifierSetTooltipInfo(staticText, modifierSet);
        }
    }
}
