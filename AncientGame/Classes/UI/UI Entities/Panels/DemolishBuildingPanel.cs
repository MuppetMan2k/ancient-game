﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class DemolishBuildingPanel : Panel
    {
        /* -------- Private Fields -------- */
        private BuildingSetDiv buildingSetDiv;

        /* -------- Constructors -------- */
        public DemolishBuildingPanel(Point inPoint, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, BuildingSetDiv inBuildingSetDiv, UIStyle style, ContentManager content)
            : base(inPoint, inAnchor, inSize, inFocus, GetTooltipInfo(), style.PanelBackgroundTextureID, style.PanelBorderTextureID, "demolish-building", content)
        {
            buildingSetDiv = inBuildingSetDiv;
        }

        /* -------- Public Methods -------- */
        public override void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {
 	        base.OnMouseLeftClick(g, i);

            buildingSetDiv.OnCurrentBuildingDemoltionStarted();
        }

        /* -------- Static Methods -------- */
        public static TooltipInfo GetTooltipInfo()
        {
            return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_click_to_demolish"));
        }
    }
}
