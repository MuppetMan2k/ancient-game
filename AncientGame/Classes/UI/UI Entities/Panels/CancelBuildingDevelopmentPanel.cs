﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class CancelBuildingDevelopmentPanel : Panel
    {
        /* -------- Private Fields -------- */
        private BuildingSetDiv buildingSetDiv;

        /* -------- Constructors -------- */
        public CancelBuildingDevelopmentPanel(Point inPoint, eUIAnchor inAnchor, int inSize, eUIFocus inFocus, BuildingSlot inBuildingSlot, BuildingSetDiv inBuildingSetDiv, UIStyle style, ContentManager content)
            : base(inPoint, inAnchor, inSize, inFocus, GetTooltipInfo(inBuildingSlot), style.PanelBackgroundTextureID, style.PanelBorderTextureID, "cancel-building-development", content)
        {
            buildingSetDiv = inBuildingSetDiv;
        }

        /* -------- Public Methods -------- */
        public override void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {
            base.OnMouseLeftClick(g, i);

            buildingSetDiv.OnCurrentDevelopmentCancelled();
        }

        /* -------- Static Methods -------- */
        private static TooltipInfo GetTooltipInfo(BuildingSlot inBuildingSlot)
        {
            if (inBuildingSlot.CurrentBuilding != null && inBuildingSlot.CurrentBuilding.IsBeingRepaired)
                return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_click_to_cancel_repair"));

            if (inBuildingSlot.CurrentBuilding != null && inBuildingSlot.CurrentBuilding.IsBeingDemolished)
                return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_click_to_cancel_demolition"));

            if (inBuildingSlot.DevelopingBuilding != null && inBuildingSlot.DevelopingBuilding.IsBeingBuilt)
                return TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_click_to_cancel_construction"));

            return null;
        }
    }
}
