﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class Button : UIEntity
    {
        /* -------- Delegates -------- */
        public delegate void ButtonPressMethod();

        /* -------- Private Fields -------- */
        protected ButtonPressMethod leftClickHandler;
        protected ButtonPressMethod rightClickHandler;
        // Assets
        private Texture2D tex;

        /* -------- Constructors -------- */
        public Button(Point inPosition, eUIAnchor inAnchor, int inWidth, int inHeight, eUIFocus inFocus, TooltipInfo inTooltipInfo, string inTextureId, ContentManager inContentManager, ButtonPressMethod inLeftClickHandler, ButtonPressMethod inRightClickHandler)
            : base(inPosition, inAnchor, inWidth, inHeight, inFocus, inTooltipInfo)
        {
            if (!string.IsNullOrEmpty(inTextureId) && inContentManager != null)
                tex = AssetLoader.LoadTexture(inTextureId, inContentManager);

            leftClickHandler = inLeftClickHandler;
            rightClickHandler = inRightClickHandler;
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            if (tex != null)
                g.SpriteBatch.Draw(tex, Rectangle, Color.White);

            base.SBDraw(g);
        }

        public override void OnMouseLeftClick(GraphicsComponent g, InputComponent i)
        {
            base.OnMouseLeftClick(g, i);

            if (leftClickHandler == null)
                return;

            leftClickHandler();
        }
        public override void OnMouseRightClick(GraphicsComponent g, InputComponent i)
        {
            base.OnMouseRightClick(g, i);

            if (rightClickHandler == null)
                return;

            rightClickHandler();
        }
    }
}
