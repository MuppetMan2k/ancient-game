﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eResourceIconMode
    {
        LOCAL_PRODUCTION_CONSUMPTION,
        EXPORTED,
        IMPORTED,
        PLAIN
    }

    class ResourceIcon : UIEntity
    {
        /* -------- Properties -------- */
        public static int Size { get { return 20; } }

        /* -------- Private Fields -------- */
        // Assets
        private Texture2D backgroundTex;
        private Texture2D iconTex;

        /* -------- Constructors -------- */
        public ResourceIcon(Point inPosition, eUIAnchor inAnchor, eUIFocus inFocus, Resource resource, float quantity, eResourceIconMode inMode, UIStyle style, ContentManager content)
            : base(inPosition, inAnchor, Size, Size, inFocus, CreateTooltipInfo(resource, quantity, inMode))
        {
            switch (inMode)
            {
                case eResourceIconMode.LOCAL_PRODUCTION_CONSUMPTION:
                    backgroundTex = AssetLoader.LoadTexture(style.LandTerritoryResourceIconLocalBackgroundTextureID, content);
                    break;
                case eResourceIconMode.EXPORTED:
                    backgroundTex = AssetLoader.LoadTexture(style.LandTerritoryResourceIconExportBackgroundTextureID, content);
                    break;
                case eResourceIconMode.IMPORTED:
                    backgroundTex = AssetLoader.LoadTexture(style.LandTerritoryResourceIconImportBackgroundTextureID, content);
                    break;
                case eResourceIconMode.PLAIN:
                    backgroundTex = null;
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inMode);
                    break;
            }

            iconTex = AssetLoader.LoadTexture(resource.IconTextureID, content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            if (backgroundTex != null)
                g.SpriteBatch.Draw(backgroundTex, Rectangle, Color.White);
            g.SpriteBatch.Draw(iconTex, Rectangle, Color.White);

            base.SBDraw(g);
        }

        /* -------- Static Methods -------- */
        private static TooltipInfo CreateTooltipInfo(Resource resource, float quantity, eResourceIconMode inMode)
        {
            float totalValue = quantity * resource.Value;
            string resourceString = String.Format(Localization.Localize("text_resource_num"), UIUtilities.GetFloatDisplayString(quantity), resource.GetName(), UIUtilities.GetFloatDisplayString(totalValue));

            string text = "{0}";
            switch (inMode)
            {
                case eResourceIconMode.LOCAL_PRODUCTION_CONSUMPTION:
                    text = Localization.Localize("text_produces_locally_consumes");
                    break;
                case eResourceIconMode.EXPORTED:
                    text = Localization.Localize("text_exports");
                    break;
                case eResourceIconMode.IMPORTED:
                    text = Localization.Localize("text_imports");
                    break;
                case eResourceIconMode.PLAIN:
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inMode);
                    break;
            }

            text = String.Format(text, resourceString);

            return TooltipInfo.CreateStaticTextTooltipInfo(text);
        }
    }
}
