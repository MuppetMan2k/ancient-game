﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class SeaUnitTypeIcon : Icon
    {
        /* -------- Constructors -------- */
        public SeaUnitTypeIcon(eSeaUnitType inType, eSeaUnitSize inSize, Point inPosition, eUIAnchor inAnchor, eUIFocus inFocus, ContentManager content)
            : base(inPosition, inAnchor, inFocus, CreateTooltipInfo(inType, inSize), GetTextureID(inType, inSize), content)
        {
        }

        /* -------- Private Methods -------- */
        private static TooltipInfo CreateTooltipInfo(eSeaUnitType inType, eSeaUnitSize inSize)
        {
            string sizeText = "";
            switch (inSize)
            {
                case eSeaUnitSize.TINY:
                    sizeText = Localization.Localize("text_unit_size_tiny");
                    break;
                case eSeaUnitSize.SMALL:
                    sizeText = Localization.Localize("text_unit_size_small");
                    break;
                case eSeaUnitSize.MEDIUM:
                    sizeText = Localization.Localize("text_unit_size_medium");
                    break;
                case eSeaUnitSize.LARGE:
                    sizeText = Localization.Localize("text_unit_size_large");
                    break;
                case eSeaUnitSize.HUGE:
                    sizeText = Localization.Localize("text_unit_size_huge");
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inSize);
                    break;
            }

            string typeText = "";
            switch (inType)
            {
                case eSeaUnitType.ASSAULT_SHIP:
                    typeText = Localization.Localize("text_unit_type_assault_ship");
                    break;
                case eSeaUnitType.COMMAND_SHIP:
                    typeText = Localization.Localize("text_unit_type_command_ship");
                    break;
                case eSeaUnitType.SIEGE_SHIP:
                    typeText = Localization.Localize("text_unit_type_siege_ship");
                    break;
                case eSeaUnitType.SKIRMISH_SHIP:
                    typeText = Localization.Localize("text_unit_type_skirmish_ship");
                    break;
                case eSeaUnitType.SPECIAL_SHIP:
                    typeText = Localization.Localize("text_unit_type_special_ship");
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inType);
                    break;
            }

            string text = string.Format(typeText, sizeText);
            return TooltipInfo.CreateStaticTextTooltipInfo(text);
        }
        private static string GetTextureID(eSeaUnitType inType, eSeaUnitSize inSize)
        {
            string sizeTexID = "";
            switch (inSize)
            {
                case eSeaUnitSize.TINY:
                    sizeTexID = "tiny";
                    break;
                case eSeaUnitSize.SMALL:
                    sizeTexID = "small";
                    break;
                case eSeaUnitSize.MEDIUM:
                    sizeTexID = "medium";
                    break;
                case eSeaUnitSize.LARGE:
                    sizeTexID = "large";
                    break;
                case eSeaUnitSize.HUGE:
                    sizeTexID = "huge";
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inSize);
                    break;
            }

            string typeTexID = "";
            switch (inType)
            {
                case eSeaUnitType.ASSAULT_SHIP:
                    typeTexID = "assault-ship";
                    break;
                case eSeaUnitType.COMMAND_SHIP:
                    typeTexID = "command-ship";
                    break;
                case eSeaUnitType.SIEGE_SHIP:
                    typeTexID = "siege-ship";
                    break;
                case eSeaUnitType.SKIRMISH_SHIP:
                    typeTexID = "skirmish-ship";
                    break;
                case eSeaUnitType.SPECIAL_SHIP:
                    typeTexID = "special-ship";
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inType);
                    break;
            }

            return sizeTexID + "-" + typeTexID + "-icon";
        }
    }
}
