﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eWealthIconType
    {
        TAXED,
        UNTAXED
    }

    class WealthSourceIcon : UIEntity
    {
        /* -------- Properties -------- */
        public static int Size { get { return 20; } }

        /* -------- Private Fields -------- */
        private eWealthIconType type;
        // Assets
        private Texture2D backgroundTex;
        private Texture2D iconTex;
        // Constants
        private float UNTAXED_ICON_ALPHA = 0.2f;

        /* -------- Constructors -------- */
        public WealthSourceIcon(Point inPosition, eUIAnchor inAnchor, eUIFocus inFocus, WealthSource wealthSource, eWealthIconType inType, UIStyle inStyle, ContentManager content)
            : base(inPosition, inAnchor, Size, Size, inFocus, CreateTooltipInfo(wealthSource, inType))
        {
            type = inType;

            switch (type)
            {
                case eWealthIconType.TAXED:
                    backgroundTex = AssetLoader.LoadTexture(inStyle.LandTerritoryWealthSourceIconTaxedBackgroundTextureID, content);
                    break;
                case eWealthIconType.UNTAXED:
                    backgroundTex = AssetLoader.LoadTexture(inStyle.LandTerritoryWealthSourceIconUntaxedBackgroundTextureID, content);
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    break;
            }

            iconTex = AssetLoader.LoadTexture(wealthSource.GetIconTextureID(), content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(backgroundTex, Rectangle, Color.White);

            Color iconTint = (type == eWealthIconType.TAXED) ? Color.White : Color.White * UNTAXED_ICON_ALPHA;
            g.SpriteBatch.Draw(iconTex, Rectangle, iconTint);

            base.SBDraw(g);
        }

        /* -------- Static Methods -------- */
        private static TooltipInfo CreateTooltipInfo(WealthSource wealthSource, eWealthIconType type)
        {
            switch (type)
            {
                case eWealthIconType.TAXED:
                    {
                        string text = String.Format(Localization.Localize("text_wealth_taxed"), UIUtilities.GetFloatDisplayString(wealthSource.TaxedValue), WealthSource.GetWealthSourceName(wealthSource.Type));
                        return TooltipInfo.CreateStaticTextTooltipInfo(text);
                    }
                case eWealthIconType.UNTAXED:
                    {
                        string text = String.Format(Localization.Localize("text_wealth_untaxed"), UIUtilities.GetFloatDisplayString(wealthSource.UntaxedValue), WealthSource.GetWealthSourceName(wealthSource.Type));
                        return TooltipInfo.CreateStaticTextTooltipInfo(text);
                    }
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return null;
            }
        }
    }
}
