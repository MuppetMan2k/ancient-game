﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class Icon : UIEntity
    {
        /* -------- Properties -------- */
        public static int Size { get { return 16; } }

        /* -------- Private Fields -------- */
        // Assets
        private Texture2D iconTex;

        /* -------- Constructors -------- */
        public Icon(Point inPosition, eUIAnchor inAnchor, eUIFocus inFocus, TooltipInfo inTooltipInfo, string textureID, ContentManager content)
            : base(inPosition, inAnchor, Size, Size, inFocus, inTooltipInfo)
        {
            iconTex = AssetLoader.LoadTexture(textureID, content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(iconTex, Rectangle, Color.White);

            base.SBDraw(g);
        }
    }
}
