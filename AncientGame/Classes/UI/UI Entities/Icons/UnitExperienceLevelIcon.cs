﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class UnitExperienceLevelIcon : Icon
    {
        /* -------- Constructors -------- */
        public UnitExperienceLevelIcon(int inExperienceLevel, Point inPosition, eUIAnchor inAnchor, eUIFocus inFocus, ContentManager content)
            : base(inPosition, inAnchor, inFocus, CreateTooltipInfo(inExperienceLevel), GetTextureID(inExperienceLevel), content)
        {
        }

        /* -------- Private Methods -------- */
        private static TooltipInfo CreateTooltipInfo(int inExperienceLevel)
        {
            string text = string.Format(Localization.Localize("text_unit_experience_level"), inExperienceLevel);
            return TooltipInfo.CreateStaticTextTooltipInfo(text);
        }
        private static string GetTextureID(int inExperienceLevel)
        {
            inExperienceLevel = Mathf.ClampUpper(inExperienceLevel, Unit.GetMaxExperienceLevel());
            return "experience-level-" + inExperienceLevel.ToString() + "-icon";
        }
    }
}
