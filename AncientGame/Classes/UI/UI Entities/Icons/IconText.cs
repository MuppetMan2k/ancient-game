﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class IconText : UIEntity
    {
        /* -------- Private Fields -------- */
        // Constants
        private const int SPACING = 4;

        /* -------- Constructors -------- */
        public IconText(Point inPosition, eUIAnchor inAnchor, int inMaxWidth, eUIFocus inFocus, TooltipInfo inTooltipInfo, string iconTextureID, string textString, Color textColor, string textFontID, ContentManager content)
            : base(inPosition, inAnchor, inMaxWidth, Icon.Size, inFocus, inTooltipInfo)
        {
            Icon icon = new Icon(new Point(), eUIAnchor.TOP_LEFT, inFocus, inTooltipInfo, iconTextureID, content);
            SetChild(icon);

            Text text = new Text(new Point(Icon.Size + SPACING, 0), eUIAnchor.CENTER_LEFT, ColoredTextSegment.GetSegmentsForSingleText(textString, textColor), textFontID, eTextAlignment.LEFT, 0, inMaxWidth - Icon.Size - SPACING, 1, inFocus, inTooltipInfo, content);
            SetChild(text);

            Rectangle updateRectangle = Rectangle;
            updateRectangle.Width = icon.Rectangle.Width + SPACING + text.Rectangle.Width;
            Rectangle = updateRectangle;
        }
    }
}
