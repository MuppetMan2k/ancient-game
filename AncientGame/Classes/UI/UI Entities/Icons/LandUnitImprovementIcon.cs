﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandUnitImprovementIcon : Icon
    {
        /* -------- Constructors -------- */
        public LandUnitImprovementIcon(LandUnitImprovement improvement, Point inPosition, eUIAnchor inAnchor, eUIFocus inFocus, ContentManager content)
            : base(inPosition, inAnchor, inFocus, GetTooltipInfo(improvement), GetTextureID(improvement), content)
        {
        }

        /* -------- Private Methods -------- */
        private static TooltipInfo GetTooltipInfo(LandUnitImprovement improvement)
        {
            string typeText = "";
            switch (improvement.Type)
            {
                case eLandUnitImprovementType.ARMOR:
                    typeText = Localization.Localize("text_unit_improved_armor");
                    break;
                case eLandUnitImprovementType.CAMELS:
                    typeText = Localization.Localize("text_unit_improved_camels");
                    break;
                case eLandUnitImprovementType.ELEPHANTS:
                    typeText = Localization.Localize("text_unit_improved_elephants");
                    break;
                case eLandUnitImprovementType.ENGINES:
                    typeText = Localization.Localize("text_unit_improved_engines");
                    break;
                case eLandUnitImprovementType.HORSES:
                    typeText = Localization.Localize("text_unit_improved_horses");
                    break;
                case eLandUnitImprovementType.MELEE_WEAPONS:
                    typeText = Localization.Localize("text_unit_improved_melee_weapons");
                    break;
                case eLandUnitImprovementType.RANGED_WEAPONS:
                    typeText = Localization.Localize("text_unit_improved_ranged_weapons");
                    break;
                case eLandUnitImprovementType.SHIELDS:
                    typeText = Localization.Localize("text_unit_improved_shields");
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(improvement.Type);
                    break;
            }

            string text = Localization.Localize("text_unit_improvement");
            text = string.Format(text, improvement.Level, typeText);

            return TooltipInfo.CreateStaticTextTooltipInfo(text);
        }
        private static string GetTextureID(LandUnitImprovement improvement)
        {
            string texID = "improved-";
            switch (improvement.Type)
            {
                case eLandUnitImprovementType.ARMOR:
                    texID += "armor";
                    break;
                case eLandUnitImprovementType.CAMELS:
                    texID += "camels";
                    break;
                case eLandUnitImprovementType.ELEPHANTS:
                    texID += "elephants";
                    break;
                case eLandUnitImprovementType.ENGINES:
                    texID += "engines";
                    break;
                case eLandUnitImprovementType.HORSES:
                    texID += "horses";
                    break;
                case eLandUnitImprovementType.MELEE_WEAPONS:
                    texID += "melee-weapons";
                    break;
                case eLandUnitImprovementType.RANGED_WEAPONS:
                    texID += "ranged-weapons";
                    break;
                case eLandUnitImprovementType.SHIELDS:
                    texID += "shields";
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(improvement.Type);
                    break;
            }

            texID += "-level-" + improvement.Level.ToString() + "-icon";
            return texID;
        }
    }
}
