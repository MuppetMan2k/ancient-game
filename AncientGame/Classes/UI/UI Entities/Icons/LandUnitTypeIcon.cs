﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandUnitTypeIcon : Icon
    {
        /* -------- Constructors -------- */
        public LandUnitTypeIcon(eLandUnitType inType, eLandUnitWeight inWeight, Point inPosition, eUIAnchor inAnchor, eUIFocus inFocus, ContentManager content)
            : base(inPosition, inAnchor, inFocus, CreateTooltipInfo(inType, inWeight), GetTextureID(inType, inWeight), content)
        {
        }

        /* -------- Private Methods -------- */
        private static TooltipInfo CreateTooltipInfo(eLandUnitType inType, eLandUnitWeight inWeight)
        {
            string weightText = "";
            switch (inWeight)
            {
                case eLandUnitWeight.SUPER_LIGHT:
                    weightText = Localization.Localize("text_unit_weight_super-light");
                    break;
                case eLandUnitWeight.LIGHT:
                    weightText = Localization.Localize("text_unit_weight_light");
                    break;
                case eLandUnitWeight.MEDIUM:
                    weightText = Localization.Localize("text_unit_weight_medium");
                    break;
                case eLandUnitWeight.HEAVY:
                    weightText = Localization.Localize("text_unit_weight_heavy");
                    break;
                case eLandUnitWeight.SUPER_HEAVY:
                    weightText = Localization.Localize("text_unit_weight_super-heavy");
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inWeight);
                    break;
            }

            string typeText = "";
            switch (inType)
            {
                case eLandUnitType.ARCHER_CAMELS:
                    typeText = Localization.Localize("text_unit_type_archer_camels");
                    break;
                case eLandUnitType.ARCHER_CAVALRY:
                    typeText = Localization.Localize("text_unit_type_archer_cavalry");
                    break;
                case eLandUnitType.ARCHER_INFANTRY:
                    typeText = Localization.Localize("text_unit_type_archer_infantry");
                    break;
                case eLandUnitType.ELEPHANTS:
                    typeText = Localization.Localize("text_unit_type_elephants");
                    break;
                case eLandUnitType.JAVELIN_CAMELS:
                    typeText = Localization.Localize("text_unit_type_javelin_camels");
                    break;
                case eLandUnitType.JAVELIN_CAVALRY:
                    typeText = Localization.Localize("text_unit_type_javelin_cavalry");
                    break;
                case eLandUnitType.JAVELIN_INFANTRY:
                    typeText = Localization.Localize("text_unit_type_javelin_infantry");
                    break;
                case eLandUnitType.MELEE_CAMELS:
                    typeText = Localization.Localize("text_unit_type_melee_camels");
                    break;
                case eLandUnitType.MELEE_CAVALRY:
                    typeText = Localization.Localize("text_unit_type_melee_cavalry");
                    break;
                case eLandUnitType.MELEE_INFANTRY:
                    typeText = Localization.Localize("text_unit_type_melee_infantry");
                    break;
                case eLandUnitType.PIKE_INFANTRY:
                    typeText = Localization.Localize("text_unit_type_pike_infantry");
                    break;
                case eLandUnitType.SHOCK_CAMELS:
                    typeText = Localization.Localize("text_unit_type_shock_camels");
                    break;
                case eLandUnitType.SHOCK_CAVALRY:
                    typeText = Localization.Localize("text_unit_type_shock_cavalry");
                    break;
                case eLandUnitType.SIEGE_ENGINE:
                    typeText = Localization.Localize("text_unit_type_siege_engine");
                    break;
                case eLandUnitType.SLINGER_INFANTRY:
                    typeText = Localization.Localize("text_unit_type_slinger_infantry");
                    break;
                case eLandUnitType.SPEAR_INFANTRY:
                    typeText = Localization.Localize("text_unit_type_spear_infantry");
                    break;
                case eLandUnitType.SPECIAL_INFANTRY:
                    typeText = Localization.Localize("text_unit_type_special_infantry");
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inType);
                    break;
            }

            string text = string.Format(typeText, weightText);
            return TooltipInfo.CreateStaticTextTooltipInfo(text);
        }
        private static string GetTextureID(eLandUnitType inType, eLandUnitWeight inWeight)
        {
            string weightTexID = "";
            switch (inWeight)
            {
                case eLandUnitWeight.SUPER_LIGHT:
                    weightTexID = "super-light";
                    break;
                case eLandUnitWeight.LIGHT:
                    weightTexID = "light";
                    break;
                case eLandUnitWeight.MEDIUM:
                    weightTexID = "medium";
                    break;
                case eLandUnitWeight.HEAVY:
                    weightTexID = "heavy";
                    break;
                case eLandUnitWeight.SUPER_HEAVY:
                    weightTexID = "super-heavy";
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inWeight);
                    break;
            }

            string typeTexID = "";
            switch (inType)
            {
                case eLandUnitType.ARCHER_CAMELS:
                    typeTexID = "archer-camels";
                    break;
                case eLandUnitType.ARCHER_CAVALRY:
                    typeTexID = "archer-cavalry";
                    break;
                case eLandUnitType.ARCHER_INFANTRY:
                    typeTexID = "archer-infantry";
                    break;
                case eLandUnitType.ELEPHANTS:
                    typeTexID = "elephants";
                    break;
                case eLandUnitType.JAVELIN_CAMELS:
                    typeTexID = "javelin-camels";
                    break;
                case eLandUnitType.JAVELIN_CAVALRY:
                    typeTexID = "javelin-cavalry";
                    break;
                case eLandUnitType.JAVELIN_INFANTRY:
                    typeTexID = "javelin-infantry";
                    break;
                case eLandUnitType.MELEE_CAMELS:
                    typeTexID = "melee-camels";
                    break;
                case eLandUnitType.MELEE_CAVALRY:
                    typeTexID = "melee-cavalry";
                    break;
                case eLandUnitType.MELEE_INFANTRY:
                    typeTexID = "melee-infantry";
                    break;
                case eLandUnitType.PIKE_INFANTRY:
                    typeTexID = "pike-infantry";
                    break;
                case eLandUnitType.SHOCK_CAMELS:
                    typeTexID = "shock-camels";
                    break;
                case eLandUnitType.SHOCK_CAVALRY:
                    typeTexID = "shock-cavalry";
                    break;
                case eLandUnitType.SIEGE_ENGINE:
                    typeTexID = "siege-engine";
                    break;
                case eLandUnitType.SLINGER_INFANTRY:
                    typeTexID = "slinger-infantry";
                    break;
                case eLandUnitType.SPEAR_INFANTRY:
                    typeTexID = "spear-infantry";
                    break;
                case eLandUnitType.SPECIAL_INFANTRY:
                    typeTexID = "special-infantry";
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inType);
                    break;
            }

            return weightTexID + "-" + typeTexID + "-icon";
        }
    }
}
