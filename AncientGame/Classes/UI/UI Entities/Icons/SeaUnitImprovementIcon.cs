﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class SeaUnitImprovementIcon : Icon
    {
        /* -------- Constructors -------- */
        public SeaUnitImprovementIcon(SeaUnitImprovement improvement, Point inPosition, eUIAnchor inAnchor, eUIFocus inFocus, ContentManager content)
            : base(inPosition, inAnchor, inFocus, GetTooltipInfo(improvement), GetTextureID(improvement), content)
        {
        }

        /* -------- Private Methods -------- */
        private static TooltipInfo GetTooltipInfo(SeaUnitImprovement improvement)
        {
            string typeText = "";
            switch (improvement.Type)
            {
                case eSeaUnitImprovementType.ARMOR:
                    typeText = Localization.Localize("text_unit_improved_armor");
                    break;
                case eSeaUnitImprovementType.HULLS:
                    typeText = Localization.Localize("text_unit_improved_hulls");
                    break;
                case eSeaUnitImprovementType.OARS:
                    typeText = Localization.Localize("text_unit_improved_oars");
                    break;
                case eSeaUnitImprovementType.ENGINES:
                    typeText = Localization.Localize("text_unit_improved_engines");
                    break;
                case eSeaUnitImprovementType.SAILS:
                    typeText = Localization.Localize("text_unit_improved_sails");
                    break;
                case eSeaUnitImprovementType.MELEE_WEAPONS:
                    typeText = Localization.Localize("text_unit_improved_melee_weapons");
                    break;
                case eSeaUnitImprovementType.RANGED_WEAPONS:
                    typeText = Localization.Localize("text_unit_improved_ranged_weapons");
                    break;
                case eSeaUnitImprovementType.SHIELDS:
                    typeText = Localization.Localize("text_unit_improved_shields");
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(improvement.Type);
                    break;
            }

            string text = Localization.Localize("text_unit_improvement");
            text = string.Format(text, improvement.Level, typeText);

            return TooltipInfo.CreateStaticTextTooltipInfo(text);
        }
        private static string GetTextureID(SeaUnitImprovement improvement)
        {
            string texID = "improved-";
            switch (improvement.Type)
            {
                case eSeaUnitImprovementType.ARMOR:
                    texID += "armor";
                    break;
                case eSeaUnitImprovementType.HULLS:
                    texID += "hulls";
                    break;
                case eSeaUnitImprovementType.OARS:
                    texID += "oars";
                    break;
                case eSeaUnitImprovementType.ENGINES:
                    texID += "engines";
                    break;
                case eSeaUnitImprovementType.SAILS:
                    texID += "sails";
                    break;
                case eSeaUnitImprovementType.MELEE_WEAPONS:
                    texID += "melee-weapons";
                    break;
                case eSeaUnitImprovementType.RANGED_WEAPONS:
                    texID += "ranged-weapons";
                    break;
                case eSeaUnitImprovementType.SHIELDS:
                    texID += "shields";
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(improvement.Type);
                    break;
            }

            texID += "-level-" + improvement.Level.ToString() + "-icon";
            return texID;
        }
    }
}
