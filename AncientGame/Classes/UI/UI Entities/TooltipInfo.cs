﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using DataTypes;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eTooltipType
    {
        STATIC_TEXT,
        LAND_TERRITORY,
        STATUE,
        TRADE_ROUTES,
        INT_VAL,
        FLOAT_VAL,
        MODIFIER_SET,
        TAX,
        INCOME_EFFICIENCY_SET,
        BUILDING,
        BUILDING_DATA,
        COST,
        LAND_UNIT_CARD,
        SEA_UNIT_CARD
    }

    class TooltipInfo
    {
        /* -------- Properties -------- */
        public eTooltipType Type { get; private set; }
        public string StaticText { get; private set; }
        public List<ColoredTextSegment> StaticTextSegments { get; private set; }
        public LandTerritory LandTerritory { get; private set; }
        public Statue Statue { get; private set; }
        public List<TradeRoute> TradeRoutes { get; private set; }
        public int IntVal { get; private set; }
        public string FloatValString { get; private set; }
        public bool BoolVal { get; private set; }
        public Color ValColor { get; private set; }
        public ModifierSet ModifierSet { get; private set; }
        public Tax Tax { get; private set; }
        public IncomeEfficiencySet IncomeEfficiencySet { get; private set; }
        public Building Building { get; private set; }
        public BuildingData BuildingData { get; private set; }
        public BuildingRequirements BuildingRequirements { get; private set; }
        public eBuildabilityResult BuildabilityResult { get; private set; }
        public Cost Cost { get; private set; }
        public LandUnit LandUnit { get; private set; }
        public SeaUnit SeaUnit { get; private set; }

        /* -------- Static Methods -------- */
        public static TooltipInfo CreateStaticTextTooltipInfo(string inText)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.STATIC_TEXT;
            info.StaticText = inText;

            return info;
        }
        public static TooltipInfo CreateLandTerritoryTooltipInfo(LandTerritory lt)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.LAND_TERRITORY;
            info.LandTerritory = lt;

            return info;
        }
        public static TooltipInfo CreateStatueTooltipInfo(Statue s)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.STATUE;
            info.Statue = s;

            return info;
        }
        public static TooltipInfo CreateTradeRoutesTooltipInfo(List<TradeRoute> tradeRoutes)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.TRADE_ROUTES;
            info.TradeRoutes = tradeRoutes;

            return info;
        }
        public static TooltipInfo CreateIntValTooltipInfo(string staticText, int val, Color valColor)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.INT_VAL;
            info.StaticText = staticText;
            info.IntVal = val;
            info.ValColor = valColor;

            return info;
        }
        public static TooltipInfo CreateFloatValTooltipInfo(string staticText, string valString, Color valColor)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.FLOAT_VAL;
            info.StaticText = staticText;
            info.FloatValString = valString;
            info.ValColor = valColor;

            return info;
        }
        public static TooltipInfo CreateModifierSetTooltipInfo(string staticText, ModifierSet modifierSet)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.MODIFIER_SET;
            info.StaticText = staticText;
            info.ModifierSet = modifierSet;

            return info;
        }
        public static TooltipInfo CreateModifierSetTooltipInfo(List<ColoredTextSegment> staticTextSegments, ModifierSet modifierSet)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.MODIFIER_SET;
            info.StaticTextSegments = staticTextSegments;
            info.ModifierSet = modifierSet;

            return info;
        }
        public static TooltipInfo CreateTaxTooltipInfo(Tax tax)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.TAX;
            info.Tax = tax;

            return info;
        }
        public static TooltipInfo CreateIncomeEfficiencySetTooltipInfo(IncomeEfficiencySet incomeEfficiencySet)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.INCOME_EFFICIENCY_SET;
            info.IncomeEfficiencySet = incomeEfficiencySet;

            return info;
        }
        public static TooltipInfo CreateBuildingTooltipInfo(Building building)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.BUILDING;
            info.Building = building;

            return info;
        }
        public static TooltipInfo CreateBuildingDataTooltipInfo(BuildingData buildingData, BuildingRequirements requirements, Cost cost, eBuildabilityResult buildabilityResult)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.BUILDING_DATA;
            info.BuildingData = buildingData;
            info.BuildingRequirements = requirements;
            info.Cost = cost;
            info.BuildabilityResult = buildabilityResult;

            return info;
        }
        public static TooltipInfo CreateCostTooltipInfo(string staticText, Cost cost, bool canAfford)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.COST;
            info.StaticText = staticText;
            info.Cost = cost;
            info.BoolVal = canAfford;

            return info;
        }
        public static TooltipInfo CreateLandUnitCardTooltipInfo(LandUnit landUnit)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.LAND_UNIT_CARD;
            info.LandUnit = landUnit;

            return info;
        }
        public static TooltipInfo CreateSeaUnitCardTooltipInfo(SeaUnit seaUnit)
        {
            TooltipInfo info = new TooltipInfo();

            info.Type = eTooltipType.SEA_UNIT_CARD;
            info.SeaUnit = seaUnit;

            return info;
        }
    }
}
