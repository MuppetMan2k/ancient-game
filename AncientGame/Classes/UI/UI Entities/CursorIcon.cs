﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eCursorIconMode
    {
        NONE,
        LAND_MOVEMENT,
        LAND_ATTACK,
        LAND_REMAIN,
        SEA_MOVEMENT,
        SEA_ATTACK,
        SEA_REMAIN,
        GARRISON,
        BESIEGE,
        MERGE
    }

    class CursorIcon : UIEntity
    {
        /* -------- Properties -------- */
        public eCursorIconMode Mode { get; private set; }
        public eCursorChangeSource Source { get; private set; }

        /* -------- Private Fields -------- */
        // Assets
        private Texture2D landMovementTex;
        private Texture2D landAttackTex;
        private Texture2D landRemainTex;
        private Texture2D seaMovementTex;
        private Texture2D seaAttackTex;
        private Texture2D seaRemainTex;
        private Texture2D garrisonTex;
        private Texture2D besiegeTex;
        private Texture2D mergeTex;
        // Constants
        private const int CURSOR_X_OFFSET = 32;
        private const int CURSOR_Y_OFFSET = 16;
        private const int ICON_SIZE = 16;

        /* -------- Constructors -------- */
        public CursorIcon(UIStyle style, ContentManager content)
            : base(new Point(CURSOR_X_OFFSET, CURSOR_Y_OFFSET), eUIAnchor.TOP_LEFT, ICON_SIZE, ICON_SIZE, eUIFocus.NO_FOCUS, null)
        {
            landMovementTex = AssetLoader.LoadTexture(style.LandMovementTexID, content);
            landAttackTex = AssetLoader.LoadTexture(style.LandAttackTexID, content);
            landRemainTex = AssetLoader.LoadTexture(style.LandRemainTexID, content);
            seaMovementTex = AssetLoader.LoadTexture(style.SeaMovementTexID, content);
            seaAttackTex = AssetLoader.LoadTexture(style.SeaAttackTexID, content);
            seaRemainTex = AssetLoader.LoadTexture(style.SeaRemainTexID, content);
            garrisonTex = AssetLoader.LoadTexture(style.GarrisonTexID, content);
            besiegeTex = AssetLoader.LoadTexture(style.BesiegeTexID, content);
            mergeTex = AssetLoader.LoadTexture(style.MergeTexID, content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            switch (Mode)
            {
                case eCursorIconMode.NONE:
                    break;
                case eCursorIconMode.LAND_MOVEMENT:
                    g.SpriteBatch.Draw(landMovementTex, Rectangle, Color.White);
                    break;
                case eCursorIconMode.LAND_ATTACK:
                    g.SpriteBatch.Draw(landAttackTex, Rectangle, Color.White);
                    break;
                case eCursorIconMode.LAND_REMAIN:
                    g.SpriteBatch.Draw(landRemainTex, Rectangle, Color.White);
                    break;
                case eCursorIconMode.SEA_MOVEMENT:
                    g.SpriteBatch.Draw(seaMovementTex, Rectangle, Color.White);
                    break;
                case eCursorIconMode.SEA_ATTACK:
                    g.SpriteBatch.Draw(seaAttackTex, Rectangle, Color.White);
                    break;
                case eCursorIconMode.SEA_REMAIN:
                    g.SpriteBatch.Draw(seaRemainTex, Rectangle, Color.White);
                    break;
                case eCursorIconMode.GARRISON:
                    g.SpriteBatch.Draw(garrisonTex, Rectangle, Color.White);
                    break;
                case eCursorIconMode.BESIEGE:
                    g.SpriteBatch.Draw(besiegeTex, Rectangle, Color.White);
                    break;
                case eCursorIconMode.MERGE:
                    g.SpriteBatch.Draw(mergeTex, Rectangle, Color.White);
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(Mode);
                    break;
            }

            base.SBDraw(g);
        }

        public override eUIType GetUIType()
        {
            return eUIType.STATIC;
        }

        public void SetMode(eCursorIconMode inMode, eCursorChangeSource inSource)
        {
            Mode = inMode;
            Source = inSource;
        }
        public void ResetMode()
        {
            Mode = eCursorIconMode.NONE;
            Source = eCursorChangeSource.NONE;
        }
    }
}
