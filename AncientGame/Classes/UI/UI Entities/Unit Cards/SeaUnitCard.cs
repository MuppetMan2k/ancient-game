﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class SeaUnitCard : UnitCard
    {
        /* -------- Properties -------- */
        public SeaUnit SeaUnit { get { return Unit as SeaUnit; } }

        /* -------- Private Fields -------- */
        // Constants
        private const int BAR_HEIGHT = 6;
        private const int SECONDARY_BAR_HEIGHT = 3;

        /* -------- Constructors -------- */
        public SeaUnitCard(SeaUnit inSeaUnit, Point inPosition, eUIAnchor inAnchor, UIStyle inStyle, eUIFocus inFocus, ContentManager content)
            : base(inSeaUnit, inPosition, inAnchor, inFocus, inStyle, TooltipInfo.CreateSeaUnitCardTooltipInfo(inSeaUnit), content)
        {
            SeaUnitTypeIcon typeIcon = new SeaUnitTypeIcon(SeaUnit.Type, SeaUnit.Size, new Point(ICON_OFFSET, ICON_OFFSET), eUIAnchor.TOP_LEFT, inFocus, content);
            SetChild(typeIcon);

            Point rightIconPos = new Point(ICON_OFFSET, ICON_OFFSET);

            if (SeaUnit.ExperienceLevel > 0)
            {
                UnitExperienceLevelIcon levelIcon = new UnitExperienceLevelIcon(SeaUnit.ExperienceLevel, rightIconPos, eUIAnchor.TOP_RIGHT, inFocus, content);
                SetChild(levelIcon);
                rightIconPos.Y += levelIcon.Rectangle.Height + ICON_OFFSET;
            }

            if (SeaUnit.Improvements.Count > 0)
            {
                int spaceForImprovementIcons = Rectangle.Height - rightIconPos.Y - 4 * BAR_OFFSET - BAR_HEIGHT - 2 * SECONDARY_BAR_HEIGHT;
                int spaceNeededForImprovementIcons = SeaUnit.Improvements.Count * Icon.Size + (SeaUnit.Improvements.Count - 1) * ICON_OFFSET;
                int squidgeUpAmount = (SeaUnit.Improvements.Count == 1 || spaceForImprovementIcons >= spaceNeededForImprovementIcons) ? 0 : (spaceNeededForImprovementIcons - spaceForImprovementIcons) / (SeaUnit.Improvements.Count - 1);

                foreach (SeaUnitImprovement sui in SeaUnit.Improvements)
                {
                    SeaUnitImprovementIcon improvementIcon = new SeaUnitImprovementIcon(sui, rightIconPos, eUIAnchor.TOP_RIGHT, inFocus, content);
                    SetChild(improvementIcon);
                    rightIconPos.Y += improvementIcon.Rectangle.Height + ICON_OFFSET - squidgeUpAmount;
                }
            }

            Point barPos = new Point(BAR_OFFSET, BAR_OFFSET);

            UnitCardBar menBar = new UnitCardBar(barPos, eUIAnchor.BOTTOM_LEFT, CARD_WIDTH - 2 * BAR_OFFSET, BAR_HEIGHT, eUnitCardBarType.NUMBER_MEN, SeaUnit.NumMen, SeaUnit.SeaUnitData.maxNumMen, inFocus, inStyle, content);
            SetChild(menBar);

            barPos.Y += menBar.Rectangle.Height + BAR_OFFSET;

            UnitCardBar hullBar = new UnitCardBar(barPos, eUIAnchor.BOTTOM_LEFT, CARD_WIDTH - 2 * BAR_OFFSET, SECONDARY_BAR_HEIGHT, eUnitCardBarType.HULL_HP, SeaUnit.HullHP, SeaUnit.SeaUnitData.maxHullHP, inFocus, inStyle, content);
            SetChild(hullBar);

            barPos.Y += hullBar.Rectangle.Height + BAR_OFFSET;

            UnitCardBar riggingBar = new UnitCardBar(barPos, eUIAnchor.BOTTOM_LEFT, CARD_WIDTH - 2 * BAR_OFFSET, SECONDARY_BAR_HEIGHT, eUnitCardBarType.RIGGING_HP, SeaUnit.RiggingHP, SeaUnit.SeaUnitData.maxRiggingHP, inFocus, inStyle, content);
            SetChild(riggingBar);
        }
    }
}
