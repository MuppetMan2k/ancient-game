﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    abstract class UnitCard : UIEntity
    {
        /* -------- Properties -------- */
        public Unit Unit { get; private set; }
        public static int Width { get { return CARD_WIDTH; } }
        public static int Height { get { return CARD_HEIGHT; } }

        /* -------- Private Fields -------- */
        private Texture2D backgroundTex;
        private Texture2D pictureTex;
        // Constants
        protected const int CARD_WIDTH = 80;
        protected const int CARD_HEIGHT = 150;
        private const int PICTURE_WIDTH = 60;
        private const int PICTURE_HEIGHT = 150;
        protected const int ICON_OFFSET = 2;
        protected const int BAR_OFFSET = 3;

        /* -------- Constructors -------- */
        public UnitCard(Unit inUnit, Point inPosition, eUIAnchor inAnchor, eUIFocus inFocus, UIStyle inStyle, TooltipInfo inTooltipInfo, ContentManager content)
            : base(inPosition, inAnchor, CARD_WIDTH, CARD_HEIGHT, inFocus, inTooltipInfo)
        {
            Unit = inUnit;
            backgroundTex = AssetLoader.LoadTexture(inStyle.UnitCardTexID, content);
            pictureTex = AssetLoader.LoadTexture(inUnit.TextureID, content);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            g.SpriteBatch.Draw(backgroundTex, Rectangle, Color.White);

            Rectangle pictureRect = new Rectangle(Rectangle.X + (CARD_WIDTH - PICTURE_WIDTH) / 2, Rectangle.Bottom - PICTURE_HEIGHT, PICTURE_WIDTH, PICTURE_HEIGHT);
            g.SpriteBatch.Draw(pictureTex, pictureRect, Color.White);

            base.SBDraw(g);
        }

        /* -------- Creation Methods -------- */
        public static UnitCard CreateUnitCard(Unit unit, Point inPosition, eUIAnchor inAnchor, UIStyle inStyle, eUIFocus inFocus, ContentManager content)
        {
            LandUnit landUnit = unit as LandUnit;
            if (landUnit != null)
                return new LandUnitCard(landUnit, inPosition, inAnchor, inStyle, inFocus, content);

            SeaUnit seaUnit = unit as SeaUnit;
            if (seaUnit != null)
                return new SeaUnitCard(seaUnit, inPosition, inAnchor, inStyle, inFocus, content);

            return null;
        }
    }
}
