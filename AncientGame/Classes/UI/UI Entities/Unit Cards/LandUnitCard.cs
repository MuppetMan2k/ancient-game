﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class LandUnitCard : UnitCard
    {
        /* -------- Properties -------- */
        public LandUnit LandUnit { get { return Unit as LandUnit; } }

        /* -------- Private Fields -------- */
        // Constants
        private const int BAR_HEIGHT = 6;

        /* -------- Constructors -------- */
        public LandUnitCard(LandUnit inLandUnit, Point inPosition, eUIAnchor inAnchor, UIStyle inStyle, eUIFocus inFocus, ContentManager content)
            : base(inLandUnit, inPosition, inAnchor, inFocus, inStyle, TooltipInfo.CreateLandUnitCardTooltipInfo(inLandUnit), content)
        {
            LandUnitTypeIcon typeIcon = new LandUnitTypeIcon(LandUnit.Type, LandUnit.Weight, new Point(ICON_OFFSET, ICON_OFFSET), eUIAnchor.TOP_LEFT, inFocus, content);
            SetChild(typeIcon);

            Point rightIconPos = new Point(ICON_OFFSET, ICON_OFFSET);

            if (LandUnit.ExperienceLevel > 0)
            {
                UnitExperienceLevelIcon levelIcon = new UnitExperienceLevelIcon(LandUnit.ExperienceLevel, rightIconPos, eUIAnchor.TOP_RIGHT, inFocus, content);
                SetChild(levelIcon);
                rightIconPos.Y += levelIcon.Rectangle.Height + ICON_OFFSET;
            }

            if (LandUnit.Improvements.Count > 0)
            {
                int spaceForImprovementIcons = Rectangle.Height - rightIconPos.Y - 2 * BAR_OFFSET - BAR_HEIGHT;
                int spaceNeededForImprovementIcons = LandUnit.Improvements.Count * Icon.Size + (LandUnit.Improvements.Count - 1) * ICON_OFFSET;
                int squidgeUpAmount = (LandUnit.Improvements.Count == 1 || spaceForImprovementIcons >= spaceNeededForImprovementIcons) ? 0 : (spaceNeededForImprovementIcons - spaceForImprovementIcons) / (LandUnit.Improvements.Count - 1);

                foreach (LandUnitImprovement lui in LandUnit.Improvements)
                {
                    LandUnitImprovementIcon improvementIcon = new LandUnitImprovementIcon(lui, rightIconPos, eUIAnchor.TOP_RIGHT, inFocus, content);
                    SetChild(improvementIcon);
                    rightIconPos.Y += improvementIcon.Rectangle.Height + ICON_OFFSET - squidgeUpAmount;
                }
            }

            UnitCardBar bar = new UnitCardBar(new Point(BAR_OFFSET, BAR_OFFSET), eUIAnchor.BOTTOM_LEFT, CARD_WIDTH - 2 * BAR_OFFSET, BAR_HEIGHT, eUnitCardBarType.NUMBER_MEN, LandUnit.NumMen, LandUnit.LandUnitData.maxNumMen, inFocus, inStyle, content);
            SetChild(bar);
        }
    }
}
