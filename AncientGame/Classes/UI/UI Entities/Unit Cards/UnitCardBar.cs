﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    /* -------- Enumerations -------- */
    enum eUnitCardBarType
    {
        NUMBER_MEN,
        HULL_HP,
        RIGGING_HP
    }

    class UnitCardBar : UIEntity
    {
        /* -------- Private Fields -------- */
        private Texture2D pixelTex;
        private float fillFraction;
        private Color backgroundColor;
        private Color fillColor;

        /* -------- Constructors -------- */
        public UnitCardBar(Point inPosition, eUIAnchor inAnchor, int inWidth, int inHeight, eUnitCardBarType inType, int number, int maxNumber, eUIFocus inFocus, UIStyle inStyle, ContentManager content)
            : base(inPosition, inAnchor, inWidth, inHeight, inFocus, CreateTooltipInfo(inType, number, maxNumber))
        {
            pixelTex = AssetLoader.LoadTexture("white-pixel", content);
            fillFraction = (float)number / maxNumber;

            backgroundColor = inStyle.BarBackgroundColor;
            fillColor = GetFillColor(inType, inStyle);
        }

        /* -------- Public Methods -------- */
        public override void SBDraw(GraphicsComponent g)
        {
            int barFillWidth = Mathf.Round(fillFraction * Rectangle.Width);
            Rectangle barFillRect = new Rectangle(Rectangle.X, Rectangle.Y, barFillWidth, Rectangle.Height);

            g.SpriteBatch.Draw(pixelTex, Rectangle, backgroundColor);
            g.SpriteBatch.Draw(pixelTex, barFillRect, fillColor);

            base.SBDraw(g);
        }

        /* -------- Private Methods -------- */
        private Color GetFillColor(eUnitCardBarType type, UIStyle style)
        {
            switch (type)
            {
                case eUnitCardBarType.NUMBER_MEN:
                    return style.UnitCardMenBarFillColor;
                case eUnitCardBarType.HULL_HP:
                    return style.UnitCardHullBarFillColor;
                case eUnitCardBarType.RIGGING_HP:
                    return style.UnitCardRiggingBarFillColor;
                default:
                    Debug.LogUnrecognizedValueWarning(type);
                    return Color.White;
            }
        }

        private static TooltipInfo CreateTooltipInfo(eUnitCardBarType inType, int number, int maxNumber)
        {
            string text = "";
            switch (inType)
            {
                case eUnitCardBarType.NUMBER_MEN:
                    text = Localization.Localize("text_unit_men");
                    text = string.Format(text, number, maxNumber);
                    break;
                case eUnitCardBarType.HULL_HP:
                    text = Localization.Localize("text_unit_hull");
                    text = string.Format(text, number, maxNumber);
                    break;
                case eUnitCardBarType.RIGGING_HP:
                    text = Localization.Localize("text_unit_rigging");
                    text = string.Format(text, number, maxNumber);
                    break;
                default:
                    Debug.LogUnrecognizedValueWarning(inType);
                    break;
            }

            return TooltipInfo.CreateStaticTextTooltipInfo(text);
        }
    }
}
