﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class MenuUIEntityCreator : UIEntityCreator
    {
        /* -------- Constructors -------- */
        public MenuUIEntityCreator(UIComponent inUIComponent)
            : base(inUIComponent)
        {
        }

        /* -------- Public Methods -------- */
        // DEBUG
        public void CreateDebugMenuUI(MenuFlowState menu, ContentManager content)
        {
            TooltipInfo info = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_start_game"));
            uiComponent.AddUI(new Button(new Point(0, 0), eUIAnchor.CENTER_CENTER, 128, 128, eUIFocus.PERMANENT_SCREEN_UI, info, "debug-start-button", content, menu.DebugGoToGameFlowState, null));
        }
    }
}
