﻿namespace AncientGame
{
    abstract class UIEntityCreator
    {
        /* -------- Private Fields -------- */
        protected UIComponent uiComponent;

        /* -------- Constructors -------- */
        public UIEntityCreator(UIComponent inUIComponent)
        {
            uiComponent = inUIComponent;
        }
    }
}
