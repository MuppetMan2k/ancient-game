﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class GameUIEntityCreator : UIEntityCreator
    {
        /* -------- Private Fields -------- */
        private List<UIEntity> factionUI;
        // Constants
        private const int STANDARD_HEIGHT = 109;

        /* -------- Constructors -------- */
        public GameUIEntityCreator(UIComponent inUIComponent)
            : base(inUIComponent)
        {
            factionUI = new List<UIEntity>();
        }

        /* -------- Public Methods -------- */
        public void SetUpNonFactionUI(ContentManager content)
        {
            TooltipInfo info = TooltipInfo.CreateStaticTextTooltipInfo(Localization.Localize("text_menu"));
            uiComponent.AddUI(new Button(new Point(0, 0), eUIAnchor.TOP_RIGHT, 128, 64, eUIFocus.PERMANENT_SCREEN_UI, info, "debug-menu-button", content, null, null));
        }

        public void RemoveFactionUI()
        {
            foreach (UIEntity ui in factionUI)
                uiComponent.RemoveUI(ui);

            factionUI.Clear();
        }
        public void SetUpFactionUI(GameManagerContainer gameManagers, ContentManager content)
        {
            Standard standard = new Standard(new Point(0, 0), eUIAnchor.TOP_CENTER, STANDARD_HEIGHT, eUIFocus.PERMANENT_SCREEN_UI, null, gameManagers.TurnManager.FactionTurn, true, gameManagers.WindowManager);
            factionUI.Add(standard);
            uiComponent.AddUI(standard);
        }
    }
}
