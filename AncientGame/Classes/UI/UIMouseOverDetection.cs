﻿using System.Collections.Generic;

namespace AncientGame
{
    class UIMouseOverDetection
    {
        /* -------- Properties -------- */
        public UIEntity PrevMouseOverUI { get; private set; }
        public UIEntity CurrMouseOverUI { get; private set; }
        public eUIFocus Focus { get; private set; }

        /* -------- Public Methods -------- */
        public void Update(List<UIEntity> uiEntities, GraphicsComponent g, InputComponent i, UIComponent uiComponent)
        {
            PrevMouseOverUI = CurrMouseOverUI;

            CurrMouseOverUI = null;
            Focus = GetInitialMouseOverFocus(uiEntities);

            foreach (UIEntity ui in uiEntities)
                PerformMouseOverDetectionRecursive(ui, i);

            OnMouseOverUIChosen(uiComponent);

            if (CurrMouseOverUI == null)
                return;

            if (i.MouseButtonIsPressing(eMouseButton.LEFT_BUTTON))
                CurrMouseOverUI.OnMouseLeftClick(g, i);
            else if (i.MouseButtonIsPressing(eMouseButton.RIGHT_BUTTON))
                CurrMouseOverUI.OnMouseRightClick(g, i);
        }

        /* -------- Private Methods --------- */
        private eUIFocus GetInitialMouseOverFocus(List<UIEntity> uiEntities)
        {
            eUIFocus focus = eUIFocus.NO_FOCUS;

            foreach (UIEntity ui in uiEntities)
                if (FocusCausesInitialMinFocus(ui.Focus))
                    focus = ui.Focus;

            return focus;
        }

        private bool FocusCausesInitialMinFocus(eUIFocus focus)
        {
            if (focus == eUIFocus.ALERT)
                return true;

            if (focus == eUIFocus.MENU)
                return true;

            return false;
        }
        private bool UIFocusIsTooLowToConsider(UIEntity ui)
        {
            return ((int)ui.Focus < (int)Focus);
        }
        private bool UIShouldOverwriteCurrMouseOverUI(UIEntity ui)
        {
            if (CurrMouseOverUI == null)
                return true;

            if ((int)ui.GetUIType() >= (int)CurrMouseOverUI.GetUIType())
                return true;

            return false;
        }

        private void PerformMouseOverDetectionRecursive(UIEntity ui, InputComponent i)
        {
            if (!ui.Active)
                return;

            if (UIFocusIsTooLowToConsider(ui))
                return;

            if (ui.MouseIsOver(i) && UIShouldOverwriteCurrMouseOverUI(ui))
                SetNewCurrMouseOverUI(ui);

            foreach (UIEntity child in ui.Children)
                PerformMouseOverDetectionRecursive(child, i);
        }
        private void SetNewCurrMouseOverUI(UIEntity ui)
        {
            CurrMouseOverUI = ui;
            Focus = ui.Focus;
        }

        private void OnMouseOverUIChosen(UIComponent uiComponent)
        {
            if (PrevMouseOverUI == null && CurrMouseOverUI == null)
                return;

            if (PrevMouseOverUI == null && CurrMouseOverUI != null)
            {
                CurrMouseOverUI.OnMouseOver();
                if (CurrMouseOverUI.TooltipInfo != null)
                    uiComponent.SetTooltipInfo(CurrMouseOverUI.TooltipInfo, eCursorChangeSource.UI);
                return;
            }

            if (PrevMouseOverUI != null)
            {
                if (CurrMouseOverUI == null)
                {
                    PrevMouseOverUI.OnMouseOut();
                    uiComponent.Tooltip.ResetTooltipInfo();
                    return;
                }

                if (PrevMouseOverUI.Equals(CurrMouseOverUI))
                    return;

                PrevMouseOverUI.OnMouseOut();
                uiComponent.Tooltip.ResetTooltipInfo();
                CurrMouseOverUI.OnMouseOver();
                if (CurrMouseOverUI.TooltipInfo != null)
                    uiComponent.SetTooltipInfo(CurrMouseOverUI.TooltipInfo, eCursorChangeSource.UI);
            }
        }
    }
}
