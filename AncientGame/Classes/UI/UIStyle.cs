﻿using Microsoft.Xna.Framework;

namespace AncientGame
{
    class UIStyle
    {
        /* -------- Properties -------- */
        // General
        public Color PositiveColor { get; private set; }
        public Color NeutralColor { get; private set; }
        public Color NegativeColor { get; private set; }
        // Cursor
        public string CursorTextureId { get; private set; }
        // Bars
        public Color BarBackgroundColor { get; private set; }
        // Tooltip
        public Color TooltipBodyColor { get; private set; }
        public Color TooltipTextLightColor { get; private set; }
        public Color TooltipTextDarkColor { get; private set; }
        public Color TooltipTextDefaultColor { get; private set; }
        public string TooltipFontID { get; private set; }
        public string TooltipAlternateFontID { get; private set; }
        // Windows
        public string WindowCloseButtonTextureID { get; private set; }
        public string WindowPageTabTextureID { get; private set; }
        public string WindowFontID { get; private set; }
        public Color WindowTextColor { get; private set; }
        public int WindowLineSpacing { get; private set; }
        public string WindowBackgroundTextureID { get; private set; }
        public string WindowTabSeparatorTextureID { get; private set; }
        // Land territory window
        public string LandTerritoryConstituentRackBackgroundTextureID { get; private set; }
        public string LandTerritoryConstituentRackPlusTextureID { get; private set; }
        public string LandTerritoryConstituentRackMinusTextureID { get; private set; }
        public string LandTerritoryConstituentPositiveBackgroundTextureID { get; private set; }
        public string LandTerritoryConstituentNegativeBackgroundTextureID { get; private set; }
        public string LandTerritoryStatTotalBackgroundID { get; private set; }
        public string LandTerritoryStatTotalFontID { get; private set; }
        public string LandTerritoryResourceIconExportBackgroundTextureID { get; private set; }
        public string LandTerritoryResourceIconImportBackgroundTextureID { get; private set; }
        public string LandTerritoryResourceIconLocalBackgroundTextureID { get; private set; }
        public string LandTerritoryResourcesSeparatorTextureID { get; private set; }
        public string LandTerritoryResourceNameFontID { get; private set; }
        public string LandTerritoryWealthSourceIconTaxedBackgroundTextureID { get; private set; }
        public string LandTerritoryWealthSourceIconUntaxedBackgroundTextureID { get; private set; }
        public string LandTerritoryWealthSourceNameFontID { get; private set; }
        public string LandTerritoryTradeFontID { get; private set; }
        // Window divs
        public Color WindowDivColor { get; private set; }
        // Scroll panes
        public string ScrollPaneBackgroundTextureID { get; private set; }
        public string ScrollIconHorizTexID { get; private set; }
        public string ScrollIconVertTexID { get; private set; }
        public Color ScrollPaneBorderColor { get; private set; }
        // Panels
        public string PanelBackgroundTextureID { get; private set; }
        public string PanelBorderTextureID { get; private set; }
        public string PanelHighlightedBorderTextureID { get; private set; }
        public string PanelTimerFontID { get; private set; }
        public Color PanelTimerTextColor { get; private set; }
        // Cursor icons
        public string LandMovementTexID { get; private set; }
        public string LandAttackTexID { get; private set; }
        public string LandRemainTexID { get; private set; }
        public string SeaMovementTexID { get; private set; }
        public string SeaAttackTexID { get; private set; }
        public string SeaRemainTexID { get; private set; }
        public string GarrisonTexID { get; private set; }
        public string BesiegeTexID { get; private set; }
        public string MergeTexID { get; private set; }
        // Unit cards
        public string UnitCardTexID { get; private set; }
        public Color UnitCardMenBarFillColor { get; private set; }
        public Color UnitCardHullBarFillColor { get; private set; }
        public Color UnitCardRiggingBarFillColor { get; private set; }
        // Garrison force panels
        public string GarrisonForcePanelBackgroundTexID { get; private set; }
        public string GarrisonForcePanelCornerTexID { get; private set; }
        public string GarrisonForcePanelTopBottomEdgeTexID { get; private set; }
        public string GarrisonForcePanelLeftRightEdgeTexID { get; private set; }
        public string GarrisonForcePanelFontID { get; private set; }
        public Color GarrisonForcePanelTextColor { get; private set; }

        /* -------- Public Methods -------- */
        public Color GetUIAttitudeColor(eUIAttitude attitude)
        {
            switch (attitude)
            {
                case eUIAttitude.POSITIVE:
                    return PositiveColor;
                case eUIAttitude.NEUTRAL:
                    return NeutralColor;
                case eUIAttitude.NEGATIVE:
                    return NegativeColor;
                default:
                    Debug.LogUnrecognizedValueWarning(attitude);
                    return NeutralColor;
            }
        }

        /* -------- Static Instances -------- */
        public static UIStyle DebugStyle
        {
            get
            {
                return new UIStyle
                {
                    // General
                    PositiveColor = new Color(0x7D, 0xB9, 0x42),
                    NeutralColor = new Color(0xCC, 0xD0, 0x28),
                    NegativeColor = new Color(0xC1, 0x4F, 0x42),
                    // Cursor
                    CursorTextureId = "hand-cursor",
                    // Bars
                    BarBackgroundColor = new Color(0x3C, 0x3C, 0x3C),
                    // Tooltip
                    TooltipBodyColor = new Color(0x10, 0x10, 0x10),
                    TooltipTextLightColor = new Color(0xEE, 0xEE, 0xEE),
                    TooltipTextDarkColor = new Color(0x00, 0x00, 0x00),
                    TooltipTextDefaultColor = new Color(0xEE, 0xEE, 0xEE),
                    TooltipFontID = "times-new-roman-12",
                    TooltipAlternateFontID = "times-new-roman-12-i",
                    // Windows
                    WindowCloseButtonTextureID = "debug-seal-tick",
                    WindowPageTabTextureID = "debug-page-tab",
                    WindowFontID = "times-new-roman-12-b",
                    WindowTextColor = new Color(0xEE, 0xEE, 0xEE),
                    WindowLineSpacing = 3,
                    WindowBackgroundTextureID = "debug-window-background",
                    WindowTabSeparatorTextureID = "debug-tab-separator",
                    // Land territory window
                    LandTerritoryConstituentRackBackgroundTextureID = "stat-constituent-rack-background",
                    LandTerritoryConstituentRackPlusTextureID = "stat-constituent-rack-plus",
                    LandTerritoryConstituentRackMinusTextureID = "stat-constituent-rack-minus",
                    LandTerritoryConstituentPositiveBackgroundTextureID = "stat-constituent-background-positive",
                    LandTerritoryConstituentNegativeBackgroundTextureID = "stat-constituent-background-negative",
                    LandTerritoryStatTotalBackgroundID = "stat-total-background",
                    LandTerritoryStatTotalFontID = "times-new-roman-10-b",
                    LandTerritoryResourceIconExportBackgroundTextureID = "resource-icon-background-export",
                    LandTerritoryResourceIconImportBackgroundTextureID = "resource-icon-background-import",
                    LandTerritoryResourceIconLocalBackgroundTextureID = "resource-icon-background-local",
                    LandTerritoryResourcesSeparatorTextureID = "resources-separator",
                    LandTerritoryResourceNameFontID = "times-new-roman-8-b",
                    LandTerritoryWealthSourceIconTaxedBackgroundTextureID = "wealth-source-icon-background-taxed",
                    LandTerritoryWealthSourceIconUntaxedBackgroundTextureID = "wealth-source-icon-background-untaxed",
                    LandTerritoryWealthSourceNameFontID = "times-new-roman-8-b",
                    LandTerritoryTradeFontID = "times-new-roman-8-b",
                    // Window divs
                    WindowDivColor = new Color(0xFF, 0xFF, 0xFF) * 0.05f,
                    // Scroll panes
                    ScrollPaneBackgroundTextureID = "debug-scroll-pane-background",
                    ScrollIconHorizTexID = "debug-scroll-icon-horiz",
                    ScrollIconVertTexID = "debug-scroll-icon-vert",
                    ScrollPaneBorderColor = new Color(0xD7, 0xB5, 0x40),
                    // Panels
                    PanelBackgroundTextureID = "debug-scroll-pane-background",
                    PanelBorderTextureID = "debug-panel-border",
                    PanelHighlightedBorderTextureID = "debug-panel-highlighted-border",
                    PanelTimerFontID = "times-new-roman-12-b",
                    PanelTimerTextColor = new Color(0xEE, 0xEE, 0xEE),
                    // Cursor icons
                    LandMovementTexID = "debug-move-land-cursor-icon",
                    LandAttackTexID = "debug-attack-cursor-icon",
                    LandRemainTexID = "debug-move-land-cursor-icon",
                    SeaMovementTexID = "debug-move-sea-cursor-icon",
                    SeaAttackTexID = "debug-attack-cursor-icon",
                    SeaRemainTexID = "debug-move-sea-cursor-icon",
                    GarrisonTexID = "debug-garrison-cursor-icon",
                    BesiegeTexID = "debug-attack-cursor-icon",
                    MergeTexID = "debug-garrison-cursor-icon",
                    // Unit cards
                    UnitCardTexID = "debug-unit-card",
                    UnitCardMenBarFillColor = new Color(0x7D, 0xB9, 0x42),
                    UnitCardHullBarFillColor = new Color(0xC1, 0x4F, 0x42),
                    UnitCardRiggingBarFillColor = new Color(0xCC, 0xD0, 0x28),
                    // Garrison force panels
                    GarrisonForcePanelBackgroundTexID = "debug-garrison-force-panel-background",
                    GarrisonForcePanelCornerTexID = "tooltip-corner",
                    GarrisonForcePanelTopBottomEdgeTexID = "tooltip-top-bottom",
                    GarrisonForcePanelLeftRightEdgeTexID = "tooltip-left-right",
                    GarrisonForcePanelFontID = "times-new-roman-12",
                    GarrisonForcePanelTextColor = new Color(0xEE, 0xEE, 0xEE)
                };
            }
        }
    }
}
