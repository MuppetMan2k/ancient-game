﻿namespace AncientGame
{
    abstract class MultiFrameLoadingProcess : LoadingProcess
    {
        /* -------- Constructors -------- */
        public MultiFrameLoadingProcess()
        {
            ContinueProcessing = true;
        }
    }
}
