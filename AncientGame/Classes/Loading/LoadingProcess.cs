﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    abstract class LoadingProcess
    {
        /* -------- Properties -------- */
        public bool ContinueProcessing { get; protected set; }

        /* -------- Constructors -------- */
        public LoadingProcess()
        {
            ContinueProcessing = false;
        }

        /* -------- Public Methods -------- */
        public abstract void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager);
    }
}
