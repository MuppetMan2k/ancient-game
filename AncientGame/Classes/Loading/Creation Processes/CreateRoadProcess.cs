﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateRoadProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private LandTradeRouteData landTradeRouteData;

        /* -------- Constructors -------- */
        public CreateRoadProcess(LandTradeRouteData inLandTradeRouteData)
        {
            landTradeRouteData = inLandTradeRouteData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            LandTerritory originLandTerritory = gameManagers.MapManager.LandTerritoryManager.GetLandTerritory(landTradeRouteData.originLandTerritoryID);
            Road road = Road.CreateFromData(landTradeRouteData);
            originLandTerritory.Roads.Add(road);
        }
    }
}
