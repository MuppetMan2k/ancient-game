﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateLakesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MapData mapData;

        /* -------- Constructors -------- */
        public CreateLakesProcess(MapData inMapData)
        {
            mapData = inMapData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (LakeData data in mapData.lakeDatas)
            {
                Lake lake = Lake.CreateFromData(data);
                gameManagers.MapManager.LakeManager.AddLake(lake);
            }
        }
    }
}
