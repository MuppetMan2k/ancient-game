﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateBuildingsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private BuildingDatabase buildingDatabase;

        /* -------- Constructors -------- */
        public CreateBuildingsProcess(BuildingDatabase inBuildingDatabase)
        {
            buildingDatabase = inBuildingDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (BuildingData data in buildingDatabase.buildingDatas)
                gameManagers.BuildingManager.AddBuildingData(data);
        }
    }
}
