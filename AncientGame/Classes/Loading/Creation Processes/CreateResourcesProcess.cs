﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateResourcesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private ResourceDatabase resourceDatabase;

        /* -------- Constructors -------- */
        public CreateResourcesProcess(ResourceDatabase inResourceDatabase)
        {
            resourceDatabase = inResourceDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (ResourceData data in resourceDatabase.resourceDatas)
            {
                Resource resource = Resource.CreateFromData(data);
                gameManagers.ResourceManager.AddResource(resource);
            }

            foreach (string typeString in resourceDatabase.resourceTypesAffectedByFertility)
            {
                eResourceType type = DataUtilities.ParseResourceType(typeString);
                gameManagers.ResourceManager.ResourceTypesAffectedByFertility.Add(type);
            }

            foreach (string typeString in resourceDatabase.resourceTypesAffectedByHarvest)
            {
                eResourceType type = DataUtilities.ParseResourceType(typeString);
                gameManagers.ResourceManager.ResourceTypesAffectedByHarvest.Add(type);
            }

            foreach (string typeString in resourceDatabase.resourceTypesThatCountAsFood)
            {
                eResourceType type = DataUtilities.ParseResourceType(typeString);
                gameManagers.ResourceManager.ResourceTypesThatCountAsFood.Add(type);
            }
        }
    }
}
