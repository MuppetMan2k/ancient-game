﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateCultureGroupsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private CultureDatabase cultureDatabase;

        /* -------- Constructors -------- */
        public CreateCultureGroupsProcess(CultureDatabase inCultureDatabase)
        {
            cultureDatabase = inCultureDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (CultureGroupData data in cultureDatabase.cultureGroupDatas)
            {
                CultureGroup cultureGroup = CultureGroup.CreateFromData(data, gameManagers);
                gameManagers.CultureManagers.CultureManager.AddCultureGroup(cultureGroup);

                loadingProcesses.Enqueue(new SetCultureGroupCrossReferencesProcess(cultureGroup, data));
            }
        }
    }
}
