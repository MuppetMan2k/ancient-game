﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateFactionsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private FactionDatabase factionDatabase;
        private PlayerInfo playerInfo;

        /* -------- Constructors -------- */
        public CreateFactionsProcess(FactionDatabase inFactionDatabase, PlayerInfo inPlayerInfo)
        {
            factionDatabase = inFactionDatabase;
            playerInfo = inPlayerInfo;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (FactionData data in factionDatabase.factionDatas)
            {
                Faction faction = Faction.CreateFromData(data, playerInfo);
                gameManagers.FactionManager.AddFaction(faction);
            }
        }
    }
}
