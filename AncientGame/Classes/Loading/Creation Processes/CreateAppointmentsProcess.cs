﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateAppointmentsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private AppointmentDatabase appointmentDatabase;

        /* -------- Constructors -------- */
        public CreateAppointmentsProcess(AppointmentDatabase inAppointmentDatabase)
        {
            appointmentDatabase = inAppointmentDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (AppointmentData data in appointmentDatabase.appointmentDatas)
                gameManagers.AppointmentManager.AddAppointmentData(data);
        }
    }
}
