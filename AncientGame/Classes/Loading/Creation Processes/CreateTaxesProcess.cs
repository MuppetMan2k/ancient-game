﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateTaxesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private TaxDatabase taxDatabase;

        /* -------- Constructors -------- */
        public CreateTaxesProcess(TaxDatabase inTaxDatabase)
        {
            taxDatabase = inTaxDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (TaxData data in taxDatabase.taxDatas)
            {
                Tax tax = Tax.CreateFromData(data, gameManagers);
                gameManagers.LandProvinceStakeManagers.TaxManager.AddTax(tax);
            }
        }
    }
}
