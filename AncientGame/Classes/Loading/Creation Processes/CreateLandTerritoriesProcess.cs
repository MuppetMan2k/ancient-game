﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateLandTerritoriesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MapData mapData;

        /* -------- Constructors -------- */
        public CreateLandTerritoriesProcess(MapData inMapData)
        {
            mapData = inMapData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (LandTerritoryData data in mapData.landTerritoryDatas)
            {
                LandTerritory landTerritory = LandTerritory.CreateFromData(data);
                gameManagers.MapManager.LandTerritoryManager.AddLandTerritory(landTerritory);
            }
        }
    }
}
