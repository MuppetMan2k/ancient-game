﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateUnitsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private UnitDatabase unitDatabase;

        /* -------- Constructors -------- */
        public CreateUnitsProcess(UnitDatabase inUnitDatabase)
        {
            unitDatabase = inUnitDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (UnitData data in unitDatabase.unitDatas)
            {
                gameManagers.UnitManager.AddUnit(data);
            }
        }
    }
}
