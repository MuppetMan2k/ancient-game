﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateCulturesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private CultureDatabase cultureDatabase;

        /* -------- Constructors -------- */
        public CreateCulturesProcess(CultureDatabase inCultureDatabase)
        {
            cultureDatabase = inCultureDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (CultureData data in cultureDatabase.cultureDatas)
            {
                Culture culture = Culture.CreateFromData(data, gameManagers);
                gameManagers.CultureManagers.CultureManager.AddCulture(culture);

                loadingProcesses.Enqueue(new SetCultureCrossReferencesProcess(culture, data));
            }
        }
    }
}
