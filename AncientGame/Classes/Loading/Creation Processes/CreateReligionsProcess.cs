﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateReligionsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private ReligionDatabase religionDatabase;

        /* -------- Constructors -------- */
        public CreateReligionsProcess(ReligionDatabase inReligionDatabase)
        {
            religionDatabase = inReligionDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (ReligionData data in religionDatabase.religionDatas)
            {
                Religion religion = Religion.CreateFromData(data, gameManagers);
                gameManagers.CultureManagers.ReligionManager.AddReligion(religion);

                loadingProcesses.Enqueue(new SetReligionCrossReferencesProcess(religion, data));
            }
        }
    }
}
