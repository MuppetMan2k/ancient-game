﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateInstitutionsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private InstitutionDatabase institutionDatabase;

        /* -------- Constructors -------- */
        public CreateInstitutionsProcess(InstitutionDatabase inInstitutionDatabase)
        {
            institutionDatabase = inInstitutionDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (InstitutionData data in institutionDatabase.institutionDatas)
            {
                gameManagers.LandProvinceStakeManagers.InstitutionManager.AddInstitutionData(data);
            }
        }
    }
}
