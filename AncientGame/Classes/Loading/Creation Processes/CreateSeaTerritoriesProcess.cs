﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateSeaTerritoriesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MapData mapData;

        /* -------- Constructors -------- */
        public CreateSeaTerritoriesProcess(MapData inMapData)
        {
            mapData = inMapData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (SeaTerritoryData data in mapData.seaTerritoryDatas)
            {
                SeaTerritory seaTerritory = SeaTerritory.CreateFromData(data);
                gameManagers.MapManager.SeaTerritoryManager.AddSeaTerritory(seaTerritory);

                loadingProcesses.Enqueue(new SetSeaTerritoryCrossReferencesProcess(seaTerritory, data));
            }
        }
    }
}
