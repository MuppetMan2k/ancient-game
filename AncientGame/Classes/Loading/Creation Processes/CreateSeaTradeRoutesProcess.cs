﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateSeaTradeRoutesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MapData mapData;

        /* -------- Constructors -------- */
        public CreateSeaTradeRoutesProcess(MapData inMapData)
        {
            mapData = inMapData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (SeaTradeRouteData data in mapData.seaTradeRouteDatas)
            {
                SeaTradeRoute seaTradeRoute = SeaTradeRoute.CreateFromData(data);
                gameManagers.MapManager.SeaTradeRouteManager.AddSeaTradeRoute(seaTradeRoute);

                loadingProcesses.Enqueue(new SetSeaTradeRouteCrossReferencesProcess(seaTradeRoute, data));
            }
        }
    }
}
