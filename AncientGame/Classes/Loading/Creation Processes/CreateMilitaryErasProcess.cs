﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateMilitaryErasProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MilitaryEraDatabase militaryEraDatabase;

        /* -------- Constructors -------- */
        public CreateMilitaryErasProcess(MilitaryEraDatabase inMilitaryEraDatabase)
        {
            militaryEraDatabase = inMilitaryEraDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (MilitaryEraData med in militaryEraDatabase.militaryEraDatas)
            {
                MilitaryEra militaryEra = MilitaryEra.CreateFromData(med, gameManagers.CultureManagers.CultureManager);
                gameManagers.MilitaryEraManager.AddMilitaryEra(militaryEra);
            }
        }
    }
}
