﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateLandProvincesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MapData mapData;

        /* -------- Constructors -------- */
        public CreateLandProvincesProcess(MapData inMapData)
        {
            mapData = inMapData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (LandProvinceData data in mapData.landProvinceDatas)
            {
                LandProvince landProvince = LandProvince.CreateFromData(data);
                gameManagers.MapManager.LandProvinceManager.AddLandProvince(landProvince);

                loadingProcesses.Enqueue(new SetLandProvinceCrossReferencesProcess(landProvince, data));
            }
        }
    }
}
