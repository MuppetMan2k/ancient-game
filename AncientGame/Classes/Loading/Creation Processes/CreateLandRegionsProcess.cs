﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateLandRegionsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MapData mapData;

        /* -------- Constructors -------- */
        public CreateLandRegionsProcess(MapData inMapData)
        {
            mapData = inMapData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (LandRegionData data in mapData.landRegionDatas)
            {
                LandRegion landRegion = LandRegion.CreateFromData(data);
                gameManagers.MapManager.LandRegionManager.AddLandRegion(landRegion);

                loadingProcesses.Enqueue(new SetLandRegionCrossReferencesProcess(landRegion, data));
            }
        }
    }
}
