﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateGovernmentTraitsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private GovernmentTraitDatabase governmentTraitDatabase;

        /* -------- Constructors -------- */
        public CreateGovernmentTraitsProcess(GovernmentTraitDatabase inGovernmentTraitDatabase)
        {
            governmentTraitDatabase = inGovernmentTraitDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (GovernmentTraitData gtd in governmentTraitDatabase.governmentTraitDatas)
            {
                GovernmentTrait governmentTrait = GovernmentTrait.CreateFromData(gtd, gameManagers);
                gameManagers.GovernmentTraitManager.AddGovernmentTrait(governmentTrait);
            }
        }
    }
}
