﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateLandTradeRoutesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MapData mapData;

        /* -------- Constructors -------- */
        public CreateLandTradeRoutesProcess(MapData inMapData)
        {
            mapData = inMapData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            for (int i = 0; i < mapData.landTradeRouteDatas.Length; i++)
            {
                LandTradeRouteData data1 = mapData.landTradeRouteDatas[i];

                loadingProcesses.Enqueue(new CreateRoadProcess(data1));

                LandTradeRouteData data2 = FindMatchingLandTradeRouteData(mapData.landTradeRouteDatas, i + 1, data1);
                if (data2 == null)
                    continue;

                LandTradeRoute landTradeRoute = LandTradeRoute.CreateFromDatas(data1, data2);
                gameManagers.MapManager.LandTradeRouteManager.AddLandTradeRoute(landTradeRoute);

                loadingProcesses.Enqueue(new SetLandTradeRouteCrossReferencesProcess(landTradeRoute, data1));
            }
        }

        /* -------- Private Methods --------- */
        private LandTradeRouteData FindMatchingLandTradeRouteData(LandTradeRouteData[] datas, int startIndex, LandTradeRouteData dataToMatch)
        {
            for (int i = startIndex; i < datas.Length; i++)
            {
                LandTradeRouteData data = datas[i];

                if (data.originLandTerritoryID != dataToMatch.destinationLandTerritoryID)
                    continue;

                if (data.destinationLandTerritoryID != dataToMatch.originLandTerritoryID)
                    continue;

                return data;
            }

            return null;
        }
    }
}
