﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateCultureFamiliesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private CultureDatabase cultureDatabase;

        /* -------- Constructors -------- */
        public CreateCultureFamiliesProcess(CultureDatabase inCultureDatabase)
        {
            cultureDatabase = inCultureDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (CultureFamilyData data in cultureDatabase.cultureFamilyDatas)
            {
                CultureFamily cultureFamily = CultureFamily.CreateFromData(data, gameManagers);
                gameManagers.CultureManagers.CultureManager.AddCultureFamily(cultureFamily);

                loadingProcesses.Enqueue(new SetCultureFamilyCrossReferencesProcess(cultureFamily, data));
            }
        }
    }
}
