﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateSmallIslandsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MapData mapData;

        /* -------- Constructors -------- */
        public CreateSmallIslandsProcess(MapData inMapData)
        {
            mapData = inMapData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (SmallIslandData data in mapData.smallIslandDatas)
            {
                SmallIsland smallIsland = SmallIsland.CreateFromData(data);
                gameManagers.MapManager.SmallIslandManager.AddSmallIsland(smallIsland);
            }
        }
    }
}
