﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateLanguagesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private LanguageDatabase languageDatabase;

        /* -------- Constructors -------- */
        public CreateLanguagesProcess(LanguageDatabase inLanguageDatabase)
        {
            languageDatabase = inLanguageDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (LanguageData data in languageDatabase.languageDatas)
            {
                Language language = Language.CreateFromData(data, gameManagers);
                gameManagers.CultureManagers.LanguageManager.AddLanguage(language);

                loadingProcesses.Enqueue(new SetLanguageCrossReferencesProcess(language, data));
            }
        }
    }
}
