﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateLawsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private LawDatabase lawDatabase;

        /* -------- Constructors -------- */
        public CreateLawsProcess(LawDatabase inLawDatabase)
        {
            lawDatabase = inLawDatabase;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (LawData data in lawDatabase.lawDatas)
            {
                Law law = Law.CreateFromData(data, gameManagers);
                gameManagers.LawManager.AddLaw(law);
            }
        }
    }
}
