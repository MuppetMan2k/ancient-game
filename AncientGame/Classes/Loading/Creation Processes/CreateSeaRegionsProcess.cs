﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class CreateSeaRegionsProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MapData mapData;

        /* -------- Constructors -------- */
        public CreateSeaRegionsProcess(MapData inMapData)
        {
            mapData = inMapData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (SeaRegionData data in mapData.seaRegionDatas)
            {
                SeaRegion seaRegion = SeaRegion.CreateFromData(data);
                gameManagers.MapManager.SeaRegionManager.AddSeaRegion(seaRegion);

                loadingProcesses.Enqueue(new SetSeaRegionCrossReferencesProcess(seaRegion, data));
            }
        }
    }
}
