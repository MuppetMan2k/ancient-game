﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetLandTerritoryStateProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private LandTerritoryStateData landTerritoryStateData;

        /* -------- Constructors -------- */
        public SetLandTerritoryStateProcess(LandTerritoryStateData inLandTerritoryStateData)
        {
            landTerritoryStateData = inLandTerritoryStateData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            LandTerritory landTerritory = gameManagers.MapManager.LandTerritoryManager.GetLandTerritory(landTerritoryStateData.landTerritoryID);

            landTerritory.Variables.SetState(landTerritoryStateData, gameManagers);
        }
    }
}
