﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetDateProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private DateData dateData;

        /* -------- Constructors -------- */
        public SetDateProcess(DateData inDateData)
        {
            dateData = inDateData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            Date date = Date.CreateFromData(dateData);
            gameManagers.TurnManager.SetDate(date);
            gameManagers.MapManager.OnDateLoaded(date);
        }
    }
}
