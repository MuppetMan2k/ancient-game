﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetMarketStateProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MarketStateData marketStateData;

        /* -------- Constructors -------- */
        public SetMarketStateProcess(MarketStateData inMarketStateData)
        {
            marketStateData = inMarketStateData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (ResourceMarketStateData rmsd in marketStateData.resourceMarketStateDatas)
            {
                Resource resource = gameManagers.ResourceManager.GetResource(rmsd.resourceID);

                if (resource == null)
                    continue;

                ResourceMarketState rms = ResourceMarketState.CreateFromData(rmsd, resource);
                gameManagers.ResourceManager.MarketManager.AddResourceMarketState(rms);
            }

            gameManagers.ResourceManager.MarketManager.EnsureAllResourceMarketStatesExist(gameManagers);
        }
    }
}
