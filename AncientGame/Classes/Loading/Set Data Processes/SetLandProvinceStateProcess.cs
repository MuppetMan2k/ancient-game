﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetLandProvinceStateProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private LandProvinceStateData landProvinceStateData;

        /* -------- Constructors -------- */
        public SetLandProvinceStateProcess(LandProvinceStateData inLandProvinceStateData)
        {
            landProvinceStateData = inLandProvinceStateData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            LandProvince landProvince = gameManagers.MapManager.LandProvinceManager.GetLandProvince(landProvinceStateData.landProvinceID);

            landProvince.SetState(landProvinceStateData);
        }
    }
}
