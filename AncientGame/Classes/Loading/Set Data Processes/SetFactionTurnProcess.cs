﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class SetFactionTurnProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            if (gameInfo.Type == eGameType.NEW_CAMPAIGN)
                gameManagers.TurnManager.SetNewCampaignFactionTurn(gameManagers.FactionManager);

            // TODO
        }
    }
}
