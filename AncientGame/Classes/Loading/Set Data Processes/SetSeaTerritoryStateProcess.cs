﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetSeaTerritoryStateProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private SeaTerritoryStateData data;

        /* -------- Constructors -------- */
        public SetSeaTerritoryStateProcess(SeaTerritoryStateData inData)
        {
            data = inData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            SeaTerritory st = gameManagers.MapManager.SeaTerritoryManager.GetSeaTerritory(data.seaTerritoryID);
            st.Variables.SetState(data, gameManagers.FactionManager);
        }
    }
}
