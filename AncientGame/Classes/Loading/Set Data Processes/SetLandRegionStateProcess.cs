﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetLandRegionStateProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private LandRegionStateData landRegionStateData;

        /* -------- Constructors -------- */
        public SetLandRegionStateProcess(LandRegionStateData inLandRegionStateData)
        {
            landRegionStateData = inLandRegionStateData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            LandRegion landRegion = gameManagers.MapManager.LandRegionManager.GetLandRegion(landRegionStateData.landRegionID);

            landRegion.Variables.SetState(landRegionStateData, gameManagers.CultureManagers);
        }
    }
}
