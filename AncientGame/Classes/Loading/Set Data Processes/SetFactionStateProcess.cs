﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetFactionStateProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        FactionStateData factionStateData;

        /* -------- Constructors -------- */
        public SetFactionStateProcess(FactionStateData inFactionStateData)
        {
            factionStateData = inFactionStateData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            Faction faction = gameManagers.FactionManager.GetFaction(factionStateData.factionID);

            faction.SetState(factionStateData, gameManagers, contentManager);
        }
    }
}
