﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadTaxDatabaseProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            TaxDatabase taxDatabase = AssetLoader.LoadTaxDatabase(contentManager);

            loadingProcesses.Enqueue(new CreateTaxesProcess(taxDatabase));
        }
    }
}
