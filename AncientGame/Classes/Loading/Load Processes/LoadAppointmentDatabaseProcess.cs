﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadAppointmentDatabaseProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            AppointmentDatabase appointmentDatabase = AssetLoader.LoadAppointmentDatabase(contentManager);

            loadingProcesses.Enqueue(new CreateAppointmentsProcess(appointmentDatabase));
        }
    }
}
