﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadFactionDatabaseProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private string factionDatabaseID;
        private PlayerInfo playerInfo;

        /* -------- Constructors -------- */
        public LoadFactionDatabaseProcess(string inFactionDatabaseID, PlayerInfo inPlayerInfo)
        {
            factionDatabaseID = inFactionDatabaseID;
            playerInfo = inPlayerInfo;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            FactionDatabase factionDatabase = AssetLoader.LoadFactionDatabase(factionDatabaseID, contentManager);

            loadingProcesses.Enqueue(new CreateFactionsProcess(factionDatabase, playerInfo));
            loadingProcesses.Enqueue(new LoadSpecificLocalizationProcess(factionDatabase.localizationID));
        }
    }
}
