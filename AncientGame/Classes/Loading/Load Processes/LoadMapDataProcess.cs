﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadMapDataProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private string mapDataID;

        /* -------- Constructors -------- */
        public LoadMapDataProcess(string inMapDataID)
        {
            mapDataID = inMapDataID;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            MapData mapData = AssetLoader.LoadMapData(mapDataID, contentManager);

            loadingProcesses.Enqueue(new LoadSpecificLocalizationProcess(mapData.localizationID));
            loadingProcesses.Enqueue(new CreateLandTerritoriesProcess(mapData));
            loadingProcesses.Enqueue(new CreateLandRegionsProcess(mapData));
            loadingProcesses.Enqueue(new CreateLandProvincesProcess(mapData));
            loadingProcesses.Enqueue(new SetLandTerritoriesCrossReferencesProcess(mapData));
            loadingProcesses.Enqueue(new CreateSeaTerritoriesProcess(mapData));
            loadingProcesses.Enqueue(new CreateSeaRegionsProcess(mapData));
            loadingProcesses.Enqueue(new CreateLakesProcess(mapData));
            loadingProcesses.Enqueue(new CreateSmallIslandsProcess(mapData));
            loadingProcesses.Enqueue(new CreateRiversProcess(mapData));
            loadingProcesses.Enqueue(new CreateLandTradeRoutesProcess(mapData));
            loadingProcesses.Enqueue(new CreateSeaTradeRoutesProcess(mapData));
        }
    }
}
