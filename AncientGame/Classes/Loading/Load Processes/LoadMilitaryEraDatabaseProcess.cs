﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadMilitaryEraDatabaseProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            MilitaryEraDatabase militaryEraDatabase = AssetLoader.LoadMilitaryEraDatabase(contentManager);

            loadingProcesses.Enqueue(new CreateMilitaryErasProcess(militaryEraDatabase));
        }
    }
}
