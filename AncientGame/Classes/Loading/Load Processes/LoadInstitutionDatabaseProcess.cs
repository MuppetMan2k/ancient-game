﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadInstitutionDatabaseProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            InstitutionDatabase cultureDatabase = AssetLoader.LoadInstitutionDatabase(contentManager);

            loadingProcesses.Enqueue(new CreateInstitutionsProcess(cultureDatabase));
        }
    }
}
