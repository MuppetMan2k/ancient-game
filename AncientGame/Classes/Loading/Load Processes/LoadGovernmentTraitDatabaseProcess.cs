﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadGovernmentTraitDatabaseProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            GovernmentTraitDatabase governmentTraitDatabase = AssetLoader.LoadGovernmentTraitDatabase(contentManager);

            loadingProcesses.Enqueue(new CreateGovernmentTraitsProcess(governmentTraitDatabase));
        }
    }
}
