﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadCultureDatabaseProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            CultureDatabase cultureDatabase = AssetLoader.LoadCultureDatabase(contentManager);

            loadingProcesses.Enqueue(new CreateCultureFamiliesProcess(cultureDatabase));
            loadingProcesses.Enqueue(new CreateCultureGroupsProcess(cultureDatabase));
            loadingProcesses.Enqueue(new CreateCulturesProcess(cultureDatabase));
        }
    }
}
