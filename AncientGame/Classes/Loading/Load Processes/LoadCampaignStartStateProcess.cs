﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadCampaignStartStateDataProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private string campaignStartStateDataID;

        /* -------- Constructors -------- */
        public LoadCampaignStartStateDataProcess(string inCampaignStartStateDataID)
        {
            campaignStartStateDataID = inCampaignStartStateDataID;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            CampaignStartStateData campaignStartStateData = AssetLoader.LoadCampaignStartStateData(campaignStartStateDataID, contentManager);

            foreach (FactionStateData fsd in campaignStartStateData.factionStateDatas)
                loadingProcesses.Enqueue(new SetFactionStateProcess(fsd));

            foreach (LandTerritoryStateData ltsd in campaignStartStateData.landTerritoryStateDatas)
                loadingProcesses.Enqueue(new SetLandTerritoryStateProcess(ltsd));

            foreach (LandRegionStateData lrsd in campaignStartStateData.landRegionStateDatas)
                loadingProcesses.Enqueue(new SetLandRegionStateProcess(lrsd));

            foreach (LandProvinceStateData lpsd in campaignStartStateData.landProvinceStateDatas)
                loadingProcesses.Enqueue(new SetLandProvinceStateProcess(lpsd));

            foreach (SeaTerritoryStateData stsd in campaignStartStateData.seaTerritoryStateDatas)
                loadingProcesses.Enqueue(new SetSeaTerritoryStateProcess(stsd));

            loadingProcesses.Enqueue(new SetDateProcess(campaignStartStateData.dateData));
            loadingProcesses.Enqueue(new SetFactionTurnProcess());

            loadingProcesses.Enqueue(new SetMarketStateProcess(campaignStartStateData.marketStateData));
            
            // TODO - Enqueue further processes for implementing and populating with campaignStartStateData
        }
    }
}
