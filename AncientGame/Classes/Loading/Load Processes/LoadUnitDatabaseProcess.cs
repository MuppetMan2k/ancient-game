﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadUnitDatabaseProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private string unitDatabaseID;

        /* -------- Constructors -------- */
        public LoadUnitDatabaseProcess(string inUnitDatabaseID)
        {
            unitDatabaseID = inUnitDatabaseID;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            UnitDatabase unitDatabase = AssetLoader.LoadUnitDatabase(unitDatabaseID, contentManager);

            loadingProcesses.Enqueue(new CreateUnitsProcess(unitDatabase));
            loadingProcesses.Enqueue(new LoadSpecificLocalizationProcess(unitDatabase.localizationID));
        }
    }
}
