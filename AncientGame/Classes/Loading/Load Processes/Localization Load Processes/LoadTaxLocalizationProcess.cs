﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadTaxLocalizationProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            LocalizationListItem[] taxLocalizations;
            taxLocalizations = AssetLoader.LoadLocalization("tax-localization", SettingsManager.Settings.Language, contentManager);
            if (taxLocalizations == null)
                taxLocalizations = AssetLoader.LoadLocalization("tax-localization", Localization.DefaultLanguage, contentManager);
            Localization.AddToSpecificLocalizationDictionary(taxLocalizations);
        }
    }
}
