﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadGovernmentLocalizationProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            LocalizationListItem[] governmentLocalizations;
            governmentLocalizations = AssetLoader.LoadLocalization("government-localization", SettingsManager.Settings.Language, contentManager);
            if (governmentLocalizations == null)
                governmentLocalizations = AssetLoader.LoadLocalization("government-localization", Localization.DefaultLanguage, contentManager);
            Localization.AddToSpecificLocalizationDictionary(governmentLocalizations);
        }
    }
}
