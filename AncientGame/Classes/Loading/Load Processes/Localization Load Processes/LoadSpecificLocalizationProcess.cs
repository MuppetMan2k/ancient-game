﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadSpecificLocalizationProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private string localizationID;

        /* -------- Constructors -------- */
        public LoadSpecificLocalizationProcess(string inLocalizationID)
        {
            localizationID = inLocalizationID;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            LocalizationListItem[] localization = AssetLoader.LoadLocalization(localizationID, SettingsManager.Settings.Language, contentManager);

            if (localization == null)
                localization = AssetLoader.LoadLocalization(localizationID, Localization.DefaultLanguage, contentManager);

            if (localization == null)
                return;

            Localization.AddToSpecificLocalizationDictionary(localization);
        }
    }
}
