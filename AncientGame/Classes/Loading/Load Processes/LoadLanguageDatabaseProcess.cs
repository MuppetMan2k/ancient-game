﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadLanguageDatabaseProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            LanguageDatabase languageDatabase = AssetLoader.LoadLanguageDatabase(contentManager);

            loadingProcesses.Enqueue(new CreateLanguagesProcess(languageDatabase));
        }
    }
}
