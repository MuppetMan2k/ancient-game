﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadResourceDatabaseProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            ResourceDatabase resourceDatabase = AssetLoader.LoadResourceDatabase(contentManager);

            loadingProcesses.Enqueue(new CreateResourcesProcess(resourceDatabase));
        }
    }
}
