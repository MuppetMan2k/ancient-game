﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadReligionDatabaseProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            ReligionDatabase religionDatabase = AssetLoader.LoadReligionDatabase(contentManager);

            loadingProcesses.Enqueue(new CreateReligionsProcess(religionDatabase));
        }
    }
}
