﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadResourceLocalizationProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            LocalizationListItem[] resourceLocalizations;
            resourceLocalizations = AssetLoader.LoadLocalization("resource-localization", SettingsManager.Settings.Language, contentManager);
            if (resourceLocalizations == null)
                resourceLocalizations = AssetLoader.LoadLocalization("resource-localization", Localization.DefaultLanguage, contentManager);
            Localization.AddToSpecificLocalizationDictionary(resourceLocalizations);
        }
    }
}
