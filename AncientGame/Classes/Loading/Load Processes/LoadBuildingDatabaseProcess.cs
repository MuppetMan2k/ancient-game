﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadBuildingDatabaseProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private string buildingDatabaseID;

        /* -------- Constructors -------- */
        public LoadBuildingDatabaseProcess(string inBuildingDatabaseID)
        {
            buildingDatabaseID = inBuildingDatabaseID;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            BuildingDatabase buildingDatabase = AssetLoader.LoadBuildingDatabase(buildingDatabaseID, contentManager);

            loadingProcesses.Enqueue(new CreateBuildingsProcess(buildingDatabase));
            loadingProcesses.Enqueue(new LoadSpecificLocalizationProcess(buildingDatabase.localizationID));
        }
    }
}
