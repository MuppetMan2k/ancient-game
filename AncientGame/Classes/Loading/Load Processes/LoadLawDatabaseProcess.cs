﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class LoadLawDatabaseProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            LawDatabase lawDatabase = AssetLoader.LoadLawDatabase(contentManager);

            loadingProcesses.Enqueue(new CreateLawsProcess(lawDatabase));
        }
    }
}
