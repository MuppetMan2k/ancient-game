﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RecalculateFactionSizesRatingsProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (Faction f in gameManagers.FactionManager.GetAllFactions())
                f.Variables.RecalculateSizeAndSpecialRating(gameManagers.FactionManager);
        }
    }
}
