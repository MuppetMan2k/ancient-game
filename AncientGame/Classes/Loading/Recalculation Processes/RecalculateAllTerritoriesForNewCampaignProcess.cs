﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RecalculateAllTerritoriesForNewCampaignProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            loadingProcesses.Enqueue(new RecalculateLandTerritoryVariablesProcess());
            loadingProcesses.Enqueue(new RecalculateSeaTerritoryMovementPointsProcess());

            // Trade recalculation
            loadingProcesses.Enqueue(new RecalculateTradePathCalculationProcess());
            loadingProcesses.Enqueue(new RecalculatePrimaryResourceProductionProcess());
            loadingProcesses.Enqueue(new RecalculatePrimaryResourceTradeProcess());
            loadingProcesses.Enqueue(new RecalculateSecondaryResourceProductionProcess());
            loadingProcesses.Enqueue(new RecalculateSecondaryResourceTradeProcess());
            loadingProcesses.Enqueue(new RecalculateResourcePresenceProcess());
            loadingProcesses.Enqueue(new RecalculateTradeRouteTrafficProcess(true));

            loadingProcesses.Enqueue(new RecalculateLandTerritoryStatsProcess());
            loadingProcesses.Enqueue(new RecalculateIncomeSetsProcess());
            loadingProcesses.Enqueue(new RecalculateBuildingSetsProcess());
        }
    }
}
