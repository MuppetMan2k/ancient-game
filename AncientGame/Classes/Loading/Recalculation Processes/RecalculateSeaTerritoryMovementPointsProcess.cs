﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RecalculateSeaTerritoryMovementPointsProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (SeaTerritory st in gameManagers.MapManager.SeaTerritoryManager.GetAllSeaTerritories())
                st.RecalculateMovementPointCost(gameManagers);
        }
    }
}
