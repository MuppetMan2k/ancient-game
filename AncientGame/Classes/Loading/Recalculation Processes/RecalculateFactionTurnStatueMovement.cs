﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RecalculateFactionTurnStatueMovement : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (Statue s in gameManagers.TurnManager.FactionTurn.Variables.StatueSet.Statues)
                s.RecalculateMovementOptions(gameManagers);
        }
    }
}
