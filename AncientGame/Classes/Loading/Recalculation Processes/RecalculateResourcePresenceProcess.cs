﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RecalculateResourcePresenceProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (LandTerritory lt in gameManagers.MapManager.LandTerritoryManager.GetAllLandTerritories())
                lt.Variables.RecalculateResourcePresence(gameManagers.ResourceManager);
        }
    }
}
