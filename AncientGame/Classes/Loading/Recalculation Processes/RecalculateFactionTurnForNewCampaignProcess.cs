﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RecalculateFactionTurnForNewCampaignProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            loadingProcesses.Enqueue(new RecalculateFactionTurnStatueMovement());
            loadingProcesses.Enqueue(new RelocateCameraForFactionTurnProcess());
        }
    }
}
