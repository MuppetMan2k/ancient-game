﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RelocateCameraForFactionTurnProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            Faction factionTurn = gameManagers.TurnManager.FactionTurn;

            LandTerritory focusLandTerritory = factionTurn.Variables.CapitolLandTerritory;
            if (focusLandTerritory != null)
            {
                gameManagers.CameraManager.Camera.SnapPosition(focusLandTerritory.Settlement.MSPosition);
                return;
            }

            if (factionTurn.Variables.StatueSet.Statues.Count > 0)
            {
                Vector2 msPos = factionTurn.Variables.StatueSet.Statues[0].Position.MSPosition;
                gameManagers.CameraManager.Camera.SnapPosition(msPos);
                return;
            }
        }
    }
}
