﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RecalculateTradePathCalculationProcess : MultiFrameLoadingProcess
    {
        /* -------- Private Fields -------- */
        private bool calculationStarted;

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            if (!calculationStarted)
            {
                gameManagers.TradeManager.StartTradeCalculation(eTradeCalculationStage.TRADE_PATH_CALCULATION, gameManagers);
                calculationStarted = true;
            }

            gameManagers.TradeManager.Update(gameManagers);

            if (!gameManagers.TradeManager.Active)
                ContinueProcessing = false;
        }
    }
}
