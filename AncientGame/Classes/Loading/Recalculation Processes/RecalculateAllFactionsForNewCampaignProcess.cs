﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RecalculateAllFactionsForNewCampaignProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            loadingProcesses.Enqueue(new RecalculateFactionSizesRatingsProcess());
        }
    }
}
