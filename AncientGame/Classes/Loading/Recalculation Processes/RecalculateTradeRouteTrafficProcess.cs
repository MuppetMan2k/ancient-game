﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RecalculateTradeRouteTrafficProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private bool spawnVehicles;

        /* -------- Constructors -------- */
        public RecalculateTradeRouteTrafficProcess(bool inSpawnVehicles)
        {
            spawnVehicles = inSpawnVehicles;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (LandTradeRoute ltr in gameManagers.MapManager.LandTradeRouteManager.GetAllLandTradeRoutes())
                ltr.RecalculateTraffic(spawnVehicles);
            foreach (SeaTradeRoute str in gameManagers.MapManager.SeaTradeRouteManager.GetAllSeaTradeRoutes())
                str.RecalculateTraffic(spawnVehicles);
        }
    }
}
