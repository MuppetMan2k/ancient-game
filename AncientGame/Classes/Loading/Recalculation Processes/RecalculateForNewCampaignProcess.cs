﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace AncientGame
{
    class RecalculateForNewCampaignProcess : LoadingProcess
    {
        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            loadingProcesses.Enqueue(new RecalculateAllTerritoriesForNewCampaignProcess());
            loadingProcesses.Enqueue(new RecalculateAllFactionsForNewCampaignProcess());
            loadingProcesses.Enqueue(new RecalculateGlobalsForNewCampaignProcess());
            loadingProcesses.Enqueue(new RecalculateFactionTurnForNewCampaignProcess());
        }
    }
}
