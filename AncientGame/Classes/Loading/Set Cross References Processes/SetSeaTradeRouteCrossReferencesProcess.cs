﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetSeaTradeRouteCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private SeaTradeRoute seaTradeRoute;
        private SeaTradeRouteData seaTradeRouteData;

        /* -------- Constructors -------- */
        public SetSeaTradeRouteCrossReferencesProcess(SeaTradeRoute inSeaTradeRoute, SeaTradeRouteData inSeaTradeRouteData)
        {
            seaTradeRoute = inSeaTradeRoute;
            seaTradeRouteData = inSeaTradeRouteData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            seaTradeRoute.SetCrossReferences(seaTradeRouteData, gameManagers.MapManager);
        }
    }
}
