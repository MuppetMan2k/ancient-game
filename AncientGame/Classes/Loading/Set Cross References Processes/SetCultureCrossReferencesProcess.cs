﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetCultureCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private Culture culture;
        private CultureData cultureData;

        /* -------- Constructors -------- */
        public SetCultureCrossReferencesProcess(Culture inCulture, CultureData inCultureData)
        {
            culture = inCulture;
            cultureData = inCultureData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            culture.SetCrossReferences(cultureData, gameManagers);
        }
    }
}
