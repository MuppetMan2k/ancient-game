﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetLandRegionCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private LandRegion landRegion;
        private LandRegionData landRegionData;

        /* -------- Constructors -------- */
        public SetLandRegionCrossReferencesProcess(LandRegion inLandRegion, LandRegionData inLandRegionData)
        {
            landRegion = inLandRegion;
            landRegionData = inLandRegionData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            landRegion.SetCrossReferences(landRegionData, gameManagers.MapManager);
        }
    }
}
