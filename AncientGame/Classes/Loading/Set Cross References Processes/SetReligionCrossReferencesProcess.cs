﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetReligionCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private Religion religion;
        private ReligionData religionData;

        /* -------- Constructors -------- */
        public SetReligionCrossReferencesProcess(Religion inReligion, ReligionData inReligionData)
        {
            religion = inReligion;
            religionData = inReligionData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            religion.SetCrossReferences(religionData, gameManagers);
        }
    }
}
