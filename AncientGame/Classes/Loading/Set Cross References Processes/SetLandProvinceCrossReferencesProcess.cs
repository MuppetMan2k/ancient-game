﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetLandProvinceCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private LandProvince landProvince;
        private LandProvinceData landProvinceData;

        /* -------- Constructors -------- */
        public SetLandProvinceCrossReferencesProcess(LandProvince inLandProvince, LandProvinceData inLandProvinceData)
        {
            landProvince = inLandProvince;
            landProvinceData = inLandProvinceData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            landProvince.SetCrossReferences(landProvinceData, gameManagers.MapManager);
        }
    }
}
