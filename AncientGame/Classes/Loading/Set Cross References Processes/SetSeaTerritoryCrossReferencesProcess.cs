﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetSeaTerritoryCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private SeaTerritory seaTerritory;
        private SeaTerritoryData seaTerritoryData;

        /* -------- Constructors -------- */
        public SetSeaTerritoryCrossReferencesProcess(SeaTerritory inSeaTerritory, SeaTerritoryData inSeaTerritoryData)
        {
            seaTerritory = inSeaTerritory;
            seaTerritoryData = inSeaTerritoryData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            seaTerritory.SetCrossReferences(seaTerritoryData, gameManagers);
        }
    }
}
