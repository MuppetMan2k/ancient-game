﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetCultureFamilyCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private CultureFamily cultureFamily;
        private CultureFamilyData cultureFamilyData;

        /* -------- Constructors -------- */
        public SetCultureFamilyCrossReferencesProcess(CultureFamily inCultureFamily, CultureFamilyData inCultureFamilyData)
        {
            cultureFamily = inCultureFamily;
            cultureFamilyData = inCultureFamilyData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            cultureFamily.SetCrossReferences(cultureFamilyData, gameManagers.AppointmentManager);
        }
    }
}
