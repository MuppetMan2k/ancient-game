﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetLandTradeRouteCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private LandTradeRoute landTradeRoute;
        private LandTradeRouteData landTradeRouteData;

        /* -------- Constructors -------- */
        public SetLandTradeRouteCrossReferencesProcess(LandTradeRoute inLandTradeRoute, LandTradeRouteData inLandTradeRouteData)
        {
            landTradeRoute = inLandTradeRoute;
            landTradeRouteData = inLandTradeRouteData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            landTradeRoute.SetCrossReferences(landTradeRouteData, gameManagers.MapManager);
        }
    }
}
