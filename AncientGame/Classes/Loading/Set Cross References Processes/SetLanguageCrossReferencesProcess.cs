﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetLanguageCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private Language language;
        private LanguageData languageData;

        /* -------- Constructors -------- */
        public SetLanguageCrossReferencesProcess(Language inLanguage, LanguageData inLanguageData)
        {
            language = inLanguage;
            languageData = inLanguageData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            language.SetCrossReferences(languageData, gameManagers.CultureManagers.CultureManager);
        }
    }
}
