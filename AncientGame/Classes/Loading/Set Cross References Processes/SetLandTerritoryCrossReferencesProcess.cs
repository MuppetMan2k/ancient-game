﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetLandTerritoryCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private LandTerritory landTerritory;
        private LandTerritoryData landTerritoryData;

        /* -------- Constructors -------- */
        public SetLandTerritoryCrossReferencesProcess(LandTerritory inLandTerritory, LandTerritoryData inLandTerritoryData)
        {
            landTerritory = inLandTerritory;
            landTerritoryData = inLandTerritoryData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            landTerritory.SetCrossReferences(landTerritoryData, gameManagers);
        }
    }
}
