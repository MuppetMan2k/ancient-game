﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetSeaRegionCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private SeaRegion seaRegion;
        private SeaRegionData seaRegionData;

        /* -------- Constructors -------- */
        public SetSeaRegionCrossReferencesProcess(SeaRegion inSeaRegion, SeaRegionData inSeaRegionData)
        {
            seaRegion = inSeaRegion;
            seaRegionData = inSeaRegionData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            seaRegion.SetCrossReferences(seaRegionData, gameManagers.MapManager);
        }
    }
}
