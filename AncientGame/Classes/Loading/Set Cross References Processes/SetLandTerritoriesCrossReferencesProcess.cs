﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetLandTerritoriesCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private MapData mapData;

        /* -------- Constructors -------- */
        public SetLandTerritoriesCrossReferencesProcess(MapData inMapData)
        {
            mapData = inMapData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            foreach (LandTerritoryData ltd in mapData.landTerritoryDatas)
            {
                LandTerritory lt = gameManagers.MapManager.LandTerritoryManager.GetLandTerritory(ltd.id);

                loadingProcesses.Enqueue(new SetLandTerritoryCrossReferencesProcess(lt, ltd));
            }
        }
    }
}
