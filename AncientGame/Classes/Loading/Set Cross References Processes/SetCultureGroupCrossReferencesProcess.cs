﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using DataTypes;

namespace AncientGame
{
    class SetCultureGroupCrossReferencesProcess : LoadingProcess
    {
        /* -------- Private Fields -------- */
        private CultureGroup cultureGroup;
        private CultureGroupData cultureGroupData;

        /* -------- Constructors -------- */
        public SetCultureGroupCrossReferencesProcess(CultureGroup inCultureGroup, CultureGroupData inCultureGroupData)
        {
            cultureGroup = inCultureGroup;
            cultureGroupData = inCultureGroupData;
        }

        /* -------- Public Methods -------- */
        public override void Process(Queue<LoadingProcess> loadingProcesses, GameManagerContainer gameManagers, GameInfo gameInfo, ContentManager contentManager)
        {
            cultureGroup.SetCrossReferences(cultureGroupData, gameManagers);
        }
    }
}
