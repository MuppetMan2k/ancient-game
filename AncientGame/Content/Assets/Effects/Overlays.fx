float4x4 xView;
float4x4 xProjection;
float xBandThickness;
float xOverlayAlpha;

/* -------- Technique-Agnostic -------- */
struct VertexShaderInput
{
    float4 Position : SV_POSITION0;
	float4 Color1 : COLOR0;
	float4 Color2 : COLOR1;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float2 MSPosition : TEXCOORD2;
	float4 Color1 : COLOR0;
	float4 Color2 : COLOR1;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

	float4 viewPosition = mul(input.Position, xView);
    output.Position = mul(viewPosition, xProjection);
	output.MSPosition = float2(input.Position.x, input.Position.y);
	output.Color1 = input.Color1;
	output.Color2 = input.Color2;

    return output;
}

/* -------- Technique - DoubleColorBands -------- */
float4 PixelShaderFunction_DoubleColorBands(VertexShaderOutput input) : COLOR0
{
	float mod = fmod(input.MSPosition.x - input.MSPosition.y, 2 * xBandThickness);
	float4 overlayColor;
	if (mod > xBandThickness)
		overlayColor = input.Color2;
	else if (mod > 0)
		overlayColor = input.Color1;
	else if (mod > -xBandThickness)
		overlayColor = input.Color2;
	else
		overlayColor = input.Color1;
	float4 outputColor = overlayColor * xOverlayAlpha;

	return outputColor;
}

technique DoubleColorBands
{
    pass Pass1
    {
        VertexShader = compile vs_4_0 VertexShaderFunction();
	PixelShader = compile ps_4_0 PixelShaderFunction_DoubleColorBands();
    }
}
