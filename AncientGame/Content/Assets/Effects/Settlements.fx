float4x4 xView;
float4x4 xProjection;
float3 xLightColor;
float xAmbientLightIntensity;
float xDirectLightIntensity;
float3 xDirectLightDirection;

Texture2D xTexture1;
sampler xTexture1Sampler = sampler_state { texture = < xTexture1 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };

/* -------- Technique-Agnostic -------- */
struct VertexShaderInput
{
	float4 Position : SV_POSITION0;
	float2 TexCoords : TEXCOORD0;
	int TextureIndex : TEXCOORD1;
};

struct VertexShaderOutput
{
	float4 Position : POSITION0;
	float2 TexCoords : TEXCOORD0;
	int TextureIndex : TEXCOORD1;
	float2 MSPosition : TEXCOORD2;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
	VertexShaderOutput output;

	float4 viewPosition = mul(input.Position, xView);
	output.Position = mul(viewPosition, xProjection);
	output.TexCoords = input.TexCoords;
	output.TextureIndex = input.TextureIndex;
	output.MSPosition = float2(input.Position.x, input.Position.y);

	return output;
}

/* -------- Technique - Standard -------- */
float4 PixelShaderFunction_Standard(VertexShaderOutput input) : COLOR0
{
	float4 outputColor = float4(0, 0, 0, 0);
	if (input.TextureIndex < 1.5)
		outputColor = tex2D(xTexture1Sampler, input.TexCoords);
	float3 normal = float3(0, 0, 1);

	// Light color
	outputColor.rgb *= xLightColor.rgb;

	// Direct lighting
	float directLightFactor = clamp(dot(normalize(normal), -xDirectLightDirection), 0, 1);
	float totalLightIntensity = xAmbientLightIntensity + xDirectLightIntensity * directLightFactor;
	outputColor.rgb = lerp(0, outputColor.rgb, totalLightIntensity);

	return outputColor;
}

technique Standard
{
	pass Pass1
	{
		VertexShader = compile vs_4_0 VertexShaderFunction();
		PixelShader = compile ps_4_0 PixelShaderFunction_Standard();
	}
}
