float4x4 xView;
float4x4 xProjection;
float3 xLightColor;
float xAmbientLightIntensity;
float xDirectLightIntensity;
float3 xDirectLightDirection;

/* -------- Technique-Agnostic -------- */
struct VertexShaderInput
{
	float4 Position : SV_POSITION0;
	float4 Color : COLOR0;
};

struct VertexShaderOutput
{
	float4 Position : POSITION0;
	float4 Color : COLOR0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
	VertexShaderOutput output;

	float4 viewPosition = mul(input.Position, xView);
	output.Position = mul(viewPosition, xProjection);
	output.Color = input.Color;

	return output;
}

/* -------- Technique - Standard -------- */
float4 PixelShaderFunction_Standard(VertexShaderOutput input) : COLOR0
{
	float4 outputColor = input.Color;

	float3 normal = float3(0, 0, 1);

	// Light color
	outputColor.rgb *= xLightColor.rgb;

	// Direct lighting
	float directLightFactor = clamp(dot(normalize(normal), -xDirectLightDirection), 0, 1);
	float totalLightIntensity = xAmbientLightIntensity + xDirectLightIntensity * directLightFactor;
	outputColor.rgb = lerp(0, outputColor.rgb, totalLightIntensity);

	return outputColor;
}

technique Standard
{
	pass Pass1
	{
		VertexShader = compile vs_4_0 VertexShaderFunction();
		PixelShader = compile ps_4_0 PixelShaderFunction_Standard();
	}
}
