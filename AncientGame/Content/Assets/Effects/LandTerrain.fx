float4x4 xView;
float4x4 xProjection;
float3 xLightColor;
float xAmbientLightIntensity;
float xDirectLightIntensity;
float3 xDirectLightDirection;

Texture2D xTexture1;
sampler xTextureSampler1 = sampler_state { texture = < xTexture1 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xBumpMap1;
sampler xBumpMapSampler1 = sampler_state { texture = < xBumpMap1 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xTexture2;
sampler xTextureSampler2 = sampler_state { texture = < xTexture2 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xBumpMap2;
sampler xBumpMapSampler2 = sampler_state { texture = < xBumpMap2 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xTexture3;
sampler xTextureSampler3 = sampler_state { texture = < xTexture3 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xBumpMap3;
sampler xBumpMapSampler3 = sampler_state { texture = < xBumpMap3 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xTexture4;
sampler xTextureSampler4 = sampler_state { texture = < xTexture4 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xBumpMap4;
sampler xBumpMapSampler4 = sampler_state { texture = < xBumpMap4 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xTexture5;
sampler xTextureSampler5 = sampler_state { texture = < xTexture5 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xBumpMap5;
sampler xBumpMapSampler5 = sampler_state { texture = < xBumpMap5 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xTexture6;
sampler xTextureSampler6 = sampler_state { texture = < xTexture6 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xBumpMap6;
sampler xBumpMapSampler6 = sampler_state { texture = < xBumpMap6 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xTexture7;
sampler xTextureSampler7 = sampler_state { texture = < xTexture7 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xBumpMap7;
sampler xBumpMapSampler7 = sampler_state { texture = < xBumpMap7 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };

/* -------- Technique-Agnostic -------- */
struct VertexShaderInput
{
	float4 Position : SV_POSITION0;
	float2 TexCoords : TEXCOORD0;
	float2 BumpMapCoords : TEXCOORD1;
	int TextureIndex : TEXCOORD2;
};

struct VertexShaderOutput
{
	float4 Position : POSITION0;
	float2 TexCoords : TEXCOORD0;
	float2 BumpMapCoords : TEXCOORD1;
	int TextureIndex : TEXCOORD2;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
	VertexShaderOutput output;

	float4 viewPosition = mul(input.Position, xView);
	output.Position = mul(viewPosition, xProjection);
	output.TexCoords = input.TexCoords;
	output.BumpMapCoords = input.BumpMapCoords;
	output.TextureIndex = input.TextureIndex;

	return output;
}

/* -------- Technique - Standard -------- */
float4 PixelShaderFunction_Standard(VertexShaderOutput input) : COLOR0
{
	float4 outputColor;
	float4 bumpSample;
	if (input.TextureIndex < 1.5)
	{
		outputColor = tex2D(xTextureSampler1, input.TexCoords);
		bumpSample = tex2D(xBumpMapSampler1, input.TexCoords);
	}
	else if (input.TextureIndex < 2.5)
	{
		outputColor = tex2D(xTextureSampler2, input.TexCoords);
		bumpSample = tex2D(xBumpMapSampler2, input.TexCoords);
	}
	else if (input.TextureIndex < 3.5)
	{
		outputColor = tex2D(xTextureSampler3, input.TexCoords);
		bumpSample = tex2D(xBumpMapSampler3, input.TexCoords);
	}
	else if (input.TextureIndex < 4.5)
	{
		outputColor = tex2D(xTextureSampler4, input.TexCoords);
		bumpSample = tex2D(xBumpMapSampler4, input.TexCoords);
	}
	else if (input.TextureIndex < 5.5)
	{
		outputColor = tex2D(xTextureSampler5, input.TexCoords);
		bumpSample = tex2D(xBumpMapSampler5, input.TexCoords);
	}
	else if (input.TextureIndex < 6.5)
	{
		outputColor = tex2D(xTextureSampler6, input.TexCoords);
		bumpSample = tex2D(xBumpMapSampler6, input.TexCoords);
	}
	else
	{
		outputColor = tex2D(xTextureSampler7, input.TexCoords);
		bumpSample = tex2D(xBumpMapSampler7, input.TexCoords);
	}
	float3 normal = float3(bumpSample.r * 2 - 1, bumpSample.g * 2 - 1, bumpSample.b * 2 - 1);

	// Light color
	outputColor.rgb *= xLightColor.rgb;

	// Direct lighting
	float directLightFactor = clamp(dot(normalize(normal), -xDirectLightDirection), 0, 1);
	float totalLightIntensity = xAmbientLightIntensity + xDirectLightIntensity * directLightFactor;
	outputColor.rgb = lerp(0, outputColor.rgb, totalLightIntensity);

	return outputColor;
}

technique Standard
{
	pass Pass1
	{
		VertexShader = compile vs_4_0 VertexShaderFunction();
		PixelShader = compile ps_4_0 PixelShaderFunction_Standard();
	}
}
