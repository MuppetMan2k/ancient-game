float4x4 xView;
float4x4 xProjection;

Texture2D xTexture;
sampler xTextureSampler = sampler_state { texture = < xTexture >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };

/* -------- Technique-Agnostic -------- */
struct VertexShaderInput
{
    float4 Position : SV_POSITION0;
	float2 TexCoords : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float2 TexCoords : TEXCOORD0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

    float4 viewPosition = mul(input.Position, xView);
    output.Position = mul(viewPosition, xProjection);
	output.TexCoords = input.TexCoords;

    return output;
}

/* -------- Technique - Standard -------- */
float4 PixelShaderFunction_Standard(VertexShaderOutput input) : COLOR0
{
	return tex2D(xTextureSampler, input.TexCoords);
}

technique Standard
{
    pass Pass1
    {
        VertexShader = compile vs_4_0 VertexShaderFunction();
        PixelShader = compile ps_4_0 PixelShaderFunction_Standard();
    }
}
