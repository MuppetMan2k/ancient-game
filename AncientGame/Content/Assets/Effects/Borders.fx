float4x4 xView;
float4x4 xProjection;

/* -------- Technique-Agnostic -------- */
struct VertexShaderInput
{
    float4 Position : SV_POSITION0;
	float4 Color : COLOR0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float4 Color : COLOR0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

	float4 viewPosition = mul(input.Position, xView);
    output.Position = mul(viewPosition, xProjection);
	output.Color = input.Color;

    return output;
}

/* -------- Technique - Standard -------- */
float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	return input.Color;
}

technique Standard
{
    pass Pass1
    {
        VertexShader = compile vs_4_0 VertexShaderFunction();
        PixelShader = compile ps_4_0 PixelShaderFunction();
    }
}
