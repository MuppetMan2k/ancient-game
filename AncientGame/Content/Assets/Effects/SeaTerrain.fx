float4x4 xView;
float4x4 xProjection;
float3 xCameraPosition;
float3 xLightColor;
float xAmbientLightIntensity;
float xDirectLightIntensity;
float3 xDirectLightDirection;
int xSpecularPower;
float xSpecularIntensity;

Texture2D xTexture1;
sampler xTextureSampler1 = sampler_state { texture = < xTexture1 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xTexture2;
sampler xTextureSampler2 = sampler_state { texture = < xTexture2 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xTexture3;
sampler xTextureSampler3 = sampler_state { texture = < xTexture3 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xBumpMap1;
sampler xBumpMap1Sampler = sampler_state { texture = < xBumpMap1 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };
Texture2D xBumpMap2;
sampler xBumpMap2Sampler = sampler_state { texture = < xBumpMap2 >; magfilter = LINEAR; minfilter = LINEAR; mipfilter = LINEAR; AddressU = wrap; AddressV = wrap; };

/* -------- Technique-Agnostic -------- */
struct VertexShaderInput
{
	float4 Position : SV_POSITION0;
	float2 TexCoords : TEXCOORD0;
	float2 BumpMap1Coords : TEXCOORD1;
	float2 BumpMap2Coords : TEXCOORD2;
	int TextureIndex : TEXCOORD3;
};

struct VertexShaderOutput
{
	float4 Position : POSITION0;
	float2 TexCoords : TEXCOORD0;
	float2 BumpMap1Coords : TEXCOORD1;
	float2 BumpMap2Coords : TEXCOORD2;
	int TextureIndex : TEXCOORD3;
	float3 GSPosition : TEXCOORD4;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
	VertexShaderOutput output;

	float4 viewPosition = mul(input.Position, xView);
	output.Position = mul(viewPosition, xProjection);
	output.TexCoords = input.TexCoords;
	output.BumpMap1Coords = input.BumpMap1Coords;
	output.BumpMap2Coords = input.BumpMap2Coords;
	output.TextureIndex = input.TextureIndex;
	output.GSPosition = float3(input.Position.x, input.Position.y, input.Position.z);

	return output;
}

/* -------- Technique - Standard -------- */
float4 PixelShaderFunction_Standard(VertexShaderOutput input) : COLOR0
{
	float4 outputColor;
	if (input.TextureIndex < 1.5)
		outputColor = tex2D(xTextureSampler1, input.TexCoords);
	else if (input.TextureIndex < 2.5)
		outputColor = tex2D(xTextureSampler2, input.TexCoords);
	else
		outputColor = tex2D(xTextureSampler3, input.TexCoords);
	float4 bumpSample1 = tex2D(xBumpMap1Sampler, input.BumpMap1Coords);
	float4 bumpSample2 = tex2D(xBumpMap2Sampler, input.BumpMap2Coords);
	float3 normal1 = float3(bumpSample1.r * 2 - 1, bumpSample1.g * 2 - 1, bumpSample1.b * 2 - 1);
	float3 normal2 = float3(bumpSample2.r * 2 - 1, bumpSample2.g * 2 - 1, bumpSample2.b * 2 - 1);
	float3 normal = normalize(normal1 + normal2);

	// Light color
	outputColor.rgb *= xLightColor.rgb;

	// Direct lighting
	float directLightFactor = clamp(dot(normal, -xDirectLightDirection), 0, 1);
	float totalLightIntensity = xAmbientLightIntensity + xDirectLightIntensity * directLightFactor;
	outputColor.rgb = lerp(0, outputColor.rgb, totalLightIntensity);

	// Specular highlighting
	float3 reflectedLightDirection = normalize(reflect(xDirectLightDirection, normal));
	float3 cameraToPixel = normalize(input.GSPosition - xCameraPosition);
	float specularFactor = clamp(dot(reflectedLightDirection, cameraToPixel), 0, 1);
	specularFactor = pow(specularFactor, xSpecularPower) * xSpecularIntensity;
	outputColor.rgb = lerp(outputColor.rgb, xLightColor.rgb, specularFactor);

	return outputColor;
}

technique Standard
{
	pass Pass1
	{
		VertexShader = compile vs_4_0 VertexShaderFunction();
		PixelShader = compile ps_4_0 PixelShaderFunction_Standard();
	}
}
