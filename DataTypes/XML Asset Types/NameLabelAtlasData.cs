﻿using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class NameLabelAtlasData
    {
        /* -------- Public Fields -------- */
        public string atlasTextureID;
        public string techniqueID;
        public int height;
        public int spaceSize;
        // Letter coordinates
        // a
        public Point aPosition;
        public int aWidth;
        // b
        public Point bPosition;
        public int bWidth;
        // c
        public Point cPosition;
        public int cWidth;
        // d
        public Point dPosition;
        public int dWidth;
        // e
        public Point ePosition;
        public int eWidth;
        // f
        public Point fPosition;
        public int fWidth;
        // g
        public Point gPosition;
        public int gWidth;
        // h
        public Point hPosition;
        public int hWidth;
        // i
        public Point iPosition;
        public int iWidth;
        // j
        public Point jPosition;
        public int jWidth;
        // k
        public Point kPosition;
        public int kWidth;
        // l
        public Point lPosition;
        public int lWidth;
        // m
        public Point mPosition;
        public int mWidth;
        // n
        public Point nPosition;
        public int nWidth;
        // o
        public Point oPosition;
        public int oWidth;
        // p
        public Point pPosition;
        public int pWidth;
        // q
        public Point qPosition;
        public int qWidth;
        // r
        public Point rPosition;
        public int rWidth;
        // s
        public Point sPosition;
        public int sWidth;
        // t
        public Point tPosition;
        public int tWidth;
        // u
        public Point uPosition;
        public int uWidth;
        // v
        public Point vPosition;
        public int vWidth;
        // w
        public Point wPosition;
        public int wWidth;
        // x
        public Point xPosition;
        public int xWidth;
        // y
        public Point yPosition;
        public int yWidth;
        // z
        public Point zPosition;
        public int zWidth;
    }
}
