﻿namespace DataTypes
{
    public class ModifierData
    {
        /* -------- Public Fields -------- */
        public string[] args;
        public string[] conditionArgs;
        public string modificationType;
        public float value;
    }
}
