﻿namespace DataTypes
{
    public class CharacterDetailSetData
    {
        /* -------- Public Fields -------- */
        public string religionID;
        public string cultureID;
        public string languageID;
        public CharacterStatSetData baseStats;
    }
}
