﻿namespace DataTypes
{
    public class AppointmentData
    {
        /* -------- Public Fields -------- */
        public string id;
        public string appointmentType;
        public AppointmentConditionsData appointmentConditionsData;
        public AppointmentCreationConditionsData appointmentCreationConditionsData;
    }
}
