﻿namespace DataTypes
{
    public class CharacterData
    {
        /* -------- Public Fields -------- */
        public int id;
        public string name;
        public string gender;
        public int age;
        public FollowerData[] followerDatas;
        public CharacterTraitData[] characterTraitDatas;
        public string[] completedAppointmentIDs;
        public CharacterDetailSetData characterDetailSetData;
    }
}
