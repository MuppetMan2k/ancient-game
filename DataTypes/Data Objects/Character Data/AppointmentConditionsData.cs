﻿namespace DataTypes
{
    public class AppointmentConditionsData
    {
        /* -------- Public Fields -------- */
        public int minAge;
        public int maxAge;
        public string genderCondition;
        public string[] prerequisiteAppointmentIDs;
    }
}
