﻿namespace DataTypes
{
    public class CharacterStatSetData
    {
        /* -------- Public Fields -------- */
        public float command;
        public float administration;
        public float popularity;
        public float ambition;
        public float piousness;
        public float leadership;
        public float progressiveness;
        public float honor;
    }
}
