﻿namespace DataTypes
{
    public class SeaUnitData : UnitData
    {
        /* -------- Public Fields -------- */
        public int maxHullHP;
        public int maxRiggingHP;
        public string type;
        public string size;
    }
}
