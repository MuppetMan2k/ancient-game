﻿namespace DataTypes
{
    public class UnitStateData
    {
        /* -------- Public Fields -------- */
        public string unitID;
        public int numMen;
        public int experienceLevel;
        public int experiencePoints;
    }
}
