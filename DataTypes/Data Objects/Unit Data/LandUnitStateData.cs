﻿namespace DataTypes
{
    public class LandUnitStateData : UnitStateData
    {
        /* -------- Public Fields -------- */
        public LandUnitImprovementData[] landUnitImprovementDatas;
    }
}
