﻿namespace DataTypes
{
    public class UnitData
    {
        /* -------- Public Fields -------- */
        public string id;
        public string texID;
        public string nameLocID;
        public string histNameLocID;
        public int maxNumMen;
    }
}
