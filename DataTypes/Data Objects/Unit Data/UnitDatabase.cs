﻿namespace DataTypes
{
    public class UnitDatabase
    {
        /* -------- Public Fields -------- */
        public int dummyInt; // Dummy int due to bug in content processor where it isn't set up to handle ints found within field classes
        public UnitData[] unitDatas;
        public string localizationID;
    }
}
