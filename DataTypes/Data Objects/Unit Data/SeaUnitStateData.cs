﻿namespace DataTypes
{
    public class SeaUnitStateData : UnitStateData
    {
        /* -------- Public Fields -------- */
        public int hullHP;
        public int riggingHP;
        public SeaUnitImprovementData[] seaUnitImprovementDatas;
    }
}
