﻿namespace DataTypes
{
    public class CostData
    {
        /* -------- Public Fields -------- */
        public int moneyCost;
        public int adminPowerCost;
        public int manpowerCost;
    }
}
