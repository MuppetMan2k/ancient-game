﻿namespace DataTypes
{
    public class InstitutionData
    {
        /* -------- Public Fields -------- */
        public string id;
        public string factionGroup;
        public ModifierData[] modifierDatas;
    }
}
