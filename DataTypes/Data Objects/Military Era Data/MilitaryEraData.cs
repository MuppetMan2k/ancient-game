﻿namespace DataTypes
{
    public class MilitaryEraData
    {
        /* -------- Public Fields -------- */
        public string id;
        public MilitaryEraConditionsData conditionsData;
        public string associatedCultureID;
        public StatueStyleData statueStyleData;
    }
}
