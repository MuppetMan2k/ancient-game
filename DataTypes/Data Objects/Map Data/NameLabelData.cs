﻿using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class NameLabelData
    {
        /* -------- Public Fields -------- */
        public Vector2 startPosition;
        public Vector2 endPosition;
    }
}
