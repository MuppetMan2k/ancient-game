﻿namespace DataTypes
{
    public class SmallIslandData
    {
        /* -------- Public Fields -------- */
        public string id;
        public AreaData[] areaDatas;
    }
}
