﻿namespace DataTypes
{
    public class LandProvinceData
    {
        /* -------- Public Fields -------- */
        public string id;
        public NameLabelData nameLabelData;
        public AreaData[] areaDatas;
        public string[] landRegionIDs;
        public string climateType;
    }
}
