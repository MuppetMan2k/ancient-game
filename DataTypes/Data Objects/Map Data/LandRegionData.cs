﻿namespace DataTypes
{
    public class LandRegionData
    {
        /* -------- Public Fields -------- */
        public string id;
        public NameLabelData nameLabelData;
        public AreaData[] areaDatas;
        public string[] landTerritoryIDs;
        public int fertilityLevel;
    }
}
