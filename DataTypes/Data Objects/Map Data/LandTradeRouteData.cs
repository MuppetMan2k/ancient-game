﻿namespace DataTypes
{
    public class LandTradeRouteData
    {
        /* -------- Public Fields -------- */
        public LineData lineData;
        public string originLandTerritoryID;
        public string destinationLandTerritoryID;
        public float length;
    }
}
