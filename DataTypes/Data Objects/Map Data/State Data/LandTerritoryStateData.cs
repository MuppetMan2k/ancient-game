﻿namespace DataTypes
{
    public class LandTerritoryStateData
    {
        /* -------- Public Fields -------- */
        public string landTerritoryID;
        public BuildingSetData settlementBuildingSetData;
        public BuildingSetData territoryBuildingSetData;
        public ResourceProductionData resourceProductionData;
        public ResourcePresenceData resourcePresenceData;
        public TradeSetData tradeSetData;
        public IncomeSetData incomeSetData;
        public UnitStateData[] garrisonUnitStateDatas;
        public HappinessStateData happinessStateData;
        public OrderStateData orderStateData;
        public HealthStateData healthStateData;
        public PopulationStateData populationStateData;
        public FoodStateData foodStateData;
        public FoodStoresStateData foodStoresStateData;
        public PrivateWealthStateData privateWealthStateData;
        public ManpowerStateData manpowerStateData;
        public WeatherStateData weatherStateData;
        public float movementPointCost;
        public int supportablePopulation;
    }
}
