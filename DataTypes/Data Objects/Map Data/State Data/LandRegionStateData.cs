﻿namespace DataTypes
{
    public class LandRegionStateData
    {
        /* -------- Public Fields -------- */
        public string landRegionID;
        public CultureStateData cultureStateData;
        public LanguageStateData languageStateData;
        public ReligionStateData religionStateData;
    }
}
