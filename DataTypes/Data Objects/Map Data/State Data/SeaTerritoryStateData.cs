﻿namespace DataTypes
{
    public class SeaTerritoryStateData
    {
        /* -------- Public Fields -------- */
        public string seaTerritoryID;
        public float movementPointCost;
    }
}
