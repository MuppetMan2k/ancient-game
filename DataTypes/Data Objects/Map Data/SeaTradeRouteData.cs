﻿namespace DataTypes
{
    public class SeaTradeRouteData
    {
        /* -------- Public Fields -------- */
        public LineData lineData;
        public string landTerritory1ID;
        public string landTerritory2ID;
        public string[] seaTerritoryPassedThroughIDs;
        public float length;
    }
}
