﻿namespace DataTypes
{
    public class SeaRegionData
    {
        /* -------- Public Fields -------- */
        public string id;
        public AreaData[] areaDatas;
        public string[] seaTerritoryIDs;
        public NameLabelData nameLabelData;
    }
}
