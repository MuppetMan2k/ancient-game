﻿namespace DataTypes
{
    public class MapData
    {
        /* -------- Public Methods -------- */
        public LandTerritoryData[] landTerritoryDatas;
        public LandRegionData[] landRegionDatas;
        public LandProvinceData[] landProvinceDatas;
        public SeaTerritoryData[] seaTerritoryDatas;
        public SeaRegionData[] seaRegionDatas;
        public LakeData[] lakeDatas;
        public SmallIslandData[] smallIslandDatas;
        public RiverData[] riverDatas;
        public LandTradeRouteData[] landTradeRouteDatas;
        public SeaTradeRouteData[] seaTradeRouteDatas;
        public string localizationID;
    }
}
