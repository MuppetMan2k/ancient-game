﻿namespace DataTypes
{
    public class LandTerritoryData
    {
        /* -------- Public Fields -------- */
        public string id;
        public AreaData[] areaDatas;
        public SettlementData settlementData;
        public AdjacentLandTerritoryData[] adjacentLandTerritoryDatas;
        public string adjacentCoastalSeaTerritoryID;
        public bool isSettlementOnly;
        public float size;
        public string terrainType;
        public ResourceProductionData resourceProductionData;
    }
}
