﻿namespace DataTypes
{
    public class SingleResourcePresenceData
    {
        /* -------- Public Fields -------- */
        public string resourceID;
        public int level;
    }
}
