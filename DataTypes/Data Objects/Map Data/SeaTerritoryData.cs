﻿namespace DataTypes
{
    public class SeaTerritoryData
    {
        /* -------- Public Fields -------- */
        public string id;
        public AreaData[] areaDatas;
        public string[] adjacentLandTerritoryIDs;
        public string[] adjacentSeaTerritoryIDs;
        public bool isCoastal;
        public bool isDeep;
    }
}
