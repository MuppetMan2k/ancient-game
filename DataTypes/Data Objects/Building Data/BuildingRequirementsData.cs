﻿namespace DataTypes
{
    public class BuildingRequirementsData
    {
        /* -------- Public Fields -------- */
        public string[] requiredResourceIDs;
        public int requiredPopulation;
        public string preceedingBuildingID;
        public string buildingSlotType;
        public string requiredCultureID;
        public string requiredCultureGroupID;
        public string requiredCultureFamilyID;
        public string requiredMilitaryEraID;
        public string requiredLandTerritoryControlLevel;
    }
}
