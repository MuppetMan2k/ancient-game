﻿namespace DataTypes
{
    public class BuildingData
    {
        /* -------- Public Fields -------- */
        public string id;
        public string locID;
        public string historicalNameLocID;
        public CostData costData;
        public float constructionTime;
        public BuildingRequirementsData requirementsData;
        public ModifierData[] modifierDatas;
        public string textureID;
    }
}
