﻿namespace DataTypes
{
    public class BuildingStateData
    {
        /* -------- Public Fields -------- */
        public int buildingSlotIndex;
        public string buildingID;
        public int turnsUntilCompletion;
        public int turnsUntilRepair;
        public int turnsUntilDemolition;
        public string damageLevel;
    }
}
