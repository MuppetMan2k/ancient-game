﻿namespace DataTypes
{
    public class BuildingSetData
    {
        /* -------- Public Fields -------- */
        public BuildingSlotData[] buildingSlotDatas;
        public BuildingStateData[] buildingStateDatas;
    }
}
