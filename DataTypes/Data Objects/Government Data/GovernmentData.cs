﻿namespace DataTypes
{
    public class GovernmentData
    {
        /* -------- Public Fields -------- */
        public string governmentGroup;
        public string[] governmentTraitIDs;
    }
}
