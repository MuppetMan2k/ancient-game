﻿namespace DataTypes
{
    public class GovernmentTraitData
    {
        /* -------- Public Fields -------- */
        public string id;
        public ModifierData[] modifierDatas;
        public string[] appointmentIDs;
    }
}
