﻿namespace DataTypes
{
    public class TradeSetData
    {
        /* -------- Public Fields -------- */
        public TradePathData[] importTradePathDatas;
        public TradePathData[] exportTradePathDatas;
        public TradePathData[] throughportTradePathDatas;
    }
}
