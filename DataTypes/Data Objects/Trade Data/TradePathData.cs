﻿namespace DataTypes
{
    public class TradePathData
    {
        /* -------- Public Fields -------- */
        public string importLandTerritoryID;
        public string exportLandTerritoryID;
        public string[] throughportLandTerritoryIDs;
        public int movementPointCost;
        public SingleResourcePresenceData[] tradedResourceDatas;
    }
}
