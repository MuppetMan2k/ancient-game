﻿namespace DataTypes
{
    public class ResourceMarketStateData
    {
        /* -------- Public Fields -------- */
        public string resourceID;
        public float totalSupply;
        public float totalDemand;
        public float value;
    }
}
