﻿namespace DataTypes
{
    public class ResourceData
    {
        /* -------- Public Fields -------- */
        public string id;
        public string locID;
        public string iconTextureID;
        public string resourceTier;
        public string resourceType;
        public float baseValue;
        public string[][] requiredResourceIDs;
    }
}
