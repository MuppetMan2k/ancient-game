﻿namespace DataTypes
{
    public class ResourceDatabase
    {
        /* -------- Public Fields -------- */
        public ResourceData[] resourceDatas;
        public string[] resourceTypesAffectedByFertility;
        public string[] resourceTypesAffectedByHarvest;
        public string[] resourceTypesThatCountAsFood;
    }
}
