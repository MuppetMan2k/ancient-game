﻿namespace DataTypes
{
    public class HealthStateData
    {
        /* -------- Public Fields -------- */
        public float health;
        public StatConstituentData[] healthConstituentDatas;
    }
}
