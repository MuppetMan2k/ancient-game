﻿namespace DataTypes
{
    public class FoodStateData
    {
        /* -------- Public Fields -------- */
        public float food;
        public StatConstituentData[] foodConstituentDatas;
    }
}
