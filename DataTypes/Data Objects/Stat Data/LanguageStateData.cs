﻿namespace DataTypes
{
    public class LanguageStateData
    {
        /* -------- Public Fields -------- */
        public LanguageStateItemData[] languageStateItemDatas;
        public LanguageInfluenceData[] languageInfluenceDatas;
    }
}
