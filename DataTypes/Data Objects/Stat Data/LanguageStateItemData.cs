﻿namespace DataTypes
{
    public class LanguageStateItemData
    {
        /* -------- Public Fields -------- */
        public string languageID;
        public float fraction;
    }
}
