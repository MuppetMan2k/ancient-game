﻿namespace DataTypes
{
    public class FoodStoresStateData
    {
        /* -------- Public Fields -------- */
        public int foodStores;
        public int maxFoodStores;
        public float foodStoresChange;
        public StatConstituentData[] foodStoresChangeConstituentDatas;
    }
}
