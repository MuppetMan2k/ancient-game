﻿namespace DataTypes
{
    public class ReligionStateItemData
    {
        /* -------- Public Fields -------- */
        public string religionID;
        public float fraction;
    }
}
