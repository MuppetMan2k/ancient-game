﻿namespace DataTypes
{
    public class ReligionStateData
    {
        /* -------- Public Fields -------- */
        public ReligionStateItemData[] religionStateItemDatas;
        public ReligionInfluenceData[] religionInfluenceDatas;
    }
}
