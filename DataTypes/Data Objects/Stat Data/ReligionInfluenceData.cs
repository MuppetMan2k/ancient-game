﻿namespace DataTypes
{
    public class ReligionInfluenceData
    {
        /* -------- Public Fields -------- */
        public string religionID;
        public float value;
        public string type;
    }
}
