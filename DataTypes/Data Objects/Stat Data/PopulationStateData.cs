﻿namespace DataTypes
{
    public class PopulationStateData
    {
        /* -------- Public Fields -------- */
        public int population;
        public float populationGrowth;
        public StatConstituentData[] populationGrowthConstituentDatas;
        public int slavePopulation;
    }
}
