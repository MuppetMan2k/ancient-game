﻿namespace DataTypes
{
    public class LanguageInfluenceData
    {
        /* -------- Public Fields -------- */
        public string languageID;
        public float value;
        public string type;
    }
}
