﻿namespace DataTypes
{
    public class WeatherStateData
    {
        /* -------- Public Fields -------- */
        public float warmth;
        public StatConstituentData[] warmthConstituentDatas;
        public float water;
        public StatConstituentData[] waterConstituentDatas;
    }
}
