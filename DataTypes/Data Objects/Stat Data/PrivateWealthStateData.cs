﻿namespace DataTypes
{
    public class PrivateWealthStateData
    {
        /* -------- Public Fields -------- */
        public float privateWealth;
        public float privateWealthChange;
        public StatConstituentData[] privateWealthChangeConstituentDatas;
    }
}
