﻿namespace DataTypes
{
    public class HappinessStateData
    {
        /* -------- Public Fields -------- */
        public float nobilityHappiness;
        public StatConstituentData[] nobilityHappinessConstituentDatas;
        public float plebHappiness;
        public StatConstituentData[] plebHappinessConstituentDatas;
    }
}
