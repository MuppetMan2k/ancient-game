﻿namespace DataTypes
{
    public class ManpowerStateData
    {
        /* -------- Public Fields -------- */
        public int manpower;
        public int maxManPower;
        public float manpowerChange;
        public StatConstituentData[] manpowerChangeConstituentDatas;
    }
}
