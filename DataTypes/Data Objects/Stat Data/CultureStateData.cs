﻿namespace DataTypes
{
    public class CultureStateData
    {
        /* -------- Public Fields -------- */
        public CultureStateItemData[] cultureStateItemDatas;
        public CultureInfluenceData[] cultureInfluenceDatas;
    }
}
