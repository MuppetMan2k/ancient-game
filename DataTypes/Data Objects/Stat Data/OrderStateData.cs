﻿namespace DataTypes
{
    public class OrderStateData
    {
        /* -------- Public Fields -------- */
        public float order;
        public StatConstituentData[] orderConstituentDatas;
    }
}
