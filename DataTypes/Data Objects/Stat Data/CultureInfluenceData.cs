﻿namespace DataTypes
{
    public class CultureInfluenceData
    {
        /* -------- Public Fields -------- */
        public string cultureID;
        public float value;
        public string type;
    }
}
