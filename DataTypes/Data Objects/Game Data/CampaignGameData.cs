﻿namespace DataTypes
{
    public class CampaignGameData
    {
        /* -------- Public Fields -------- */
        public string mapDataID;
        public string factionDatabaseID;
        public string unitDatabaseID;
        public string buildingDatabaseID;
        public string campaignStartStateDataID;
    }
}
