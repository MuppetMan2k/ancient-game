﻿namespace DataTypes
{
    public class SaveGameData
    {
        /* -------- Public Fields -------- */
        public string mapDataID;
        public string factionDatabaseID;
        public string unitDatabaseID;
        public string buildingDatabaseID;
        public CampaignSaveData campaignSaveData;
    }
}
