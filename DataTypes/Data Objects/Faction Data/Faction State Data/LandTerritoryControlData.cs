﻿namespace DataTypes
{
    public class LandTerritoryControlData
    {
        /* -------- Public Fields -------- */
        public string landTerritoryID;
        public string currControlLevel;
        public string prevControlLevel;
        public int turnsOfNoControl;
        public string developingControlLevel;
        public int developmentTurnsRemaining;
    }
}
