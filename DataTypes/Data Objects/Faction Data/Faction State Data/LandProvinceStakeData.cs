﻿namespace DataTypes
{
    public class LandProvinceStakeData
    {
        /* -------- Public Fields -------- */
        public string landProvinceID;
        public InstitutionStateData[] institutionStateDatas;
        public string[] taxIDs;
    }
}
