﻿using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class FactionStateData
    {
        /* -------- Public Fields -------- */
        public string factionID;
        public string size;
        public string specialRating;
        public string capitolLandTerritoryID;
        public string emblemID;
        public Color primaryColor;
        public Color secondaryColor;
        public string cultureID;
        public string languageID;
        public string religionID;
        public LandTerritoryControlData[] landTerritoryControlDatas;
        public LandProvinceStakeData[] landProvinceStakeDatas;
        public int stability;
        public int treasury;
        public int treasuryChange;
        public int adminPower;
        public int maxAdminPower;
        public int adminPowerChange;
        public int significance;
        public GovernmentData governmentData;
        public LawSetData lawSetData;
        public DiplomaticContractData[] diplomaticContractDatas;
        public FactionCharacterPoolData factionCharacterPoolData;
        public AppointmentSetData appointmentSetData;
        public StatueSetData statueSetData;
        public string militaryEraID;
    }
}
