﻿namespace DataTypes
{
    public class WealthSourceData
    {
        /* -------- Public Fields -------- */
        public string wealthSourceType;
        public float value;
        public float taxedValue;
    }
}
