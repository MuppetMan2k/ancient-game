﻿namespace DataTypes
{
    public class SingleIncomeEfficiencyData
    {
        /* -------- Public Methods -------- */
        public string incomeEfficiencySource;
        public float value;
    }
}
