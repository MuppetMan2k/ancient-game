﻿namespace DataTypes
{
    public class IncomeSetData
    {
        /* -------- Public Fields -------- */
        public WealthSourceData[] wealthSourceDatas;
        public IncomeEfficiencySetData incomeEfficiencySetData;
    }
}
