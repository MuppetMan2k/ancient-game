﻿namespace DataTypes
{
    public class StatueStyleData
    {
        /* -------- Public Fields -------- */
        public string landForceTextureID;
        public string armyTextureID;
        public string seaForceTextureID;
        public string navyTextureID;
    }
}
