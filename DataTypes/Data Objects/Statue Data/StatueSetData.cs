﻿namespace DataTypes
{
    public class StatueSetData
    {
        /* -------- Public Fields -------- */
        public LandForceData[] landForceDatas;
        public ArmyData[] armyDatas;
        public SeaForceData[] seaForceDatas;
        public NavyData[] navyDatas;
    }
}
