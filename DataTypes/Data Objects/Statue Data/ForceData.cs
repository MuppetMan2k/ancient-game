﻿namespace DataTypes
{
    public class ForceData : StatueData
    {
        /* -------- Public Fields -------- */
        public UnitStateData[] unitStateDatas;
        public float forceRating;
    }
}
