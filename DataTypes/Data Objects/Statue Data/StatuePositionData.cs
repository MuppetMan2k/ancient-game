﻿using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class StatuePositionData
    {
        /* -------- Public Fields -------- */
        public string landTerritoryID;
        public string seaTerritoryID;
        public string settlementLandTerritoryID;
        public Vector2 msPosition;
    }
}
