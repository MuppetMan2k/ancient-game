﻿namespace DataTypes
{
    public class StatueData
    {
        /* -------- Public Fields -------- */
        public StatuePositionData statuePositionData;
        public int movementPoints;
        public int additionalMovementPoints;
        public int maxMovementPoints;
    }
}
