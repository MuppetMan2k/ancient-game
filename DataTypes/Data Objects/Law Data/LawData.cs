﻿namespace DataTypes
{
    public class LawData
    {
        /* -------- Public Fields -------- */
        public string id;
        public string factionGroup;
        public ModifierData[] modifierDatas;
    }
}
