﻿namespace DataTypes
{
    public class CultureData
    {
        /* -------- Public Fields -------- */
        public string id;
        public string cultureGroupID;
        public ModifierData[] modifierDatas;
        public string[] appointmentIDs;
        public StatueStyleData statueStyleData;
    }
}
