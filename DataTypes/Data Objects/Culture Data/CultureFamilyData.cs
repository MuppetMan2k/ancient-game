﻿namespace DataTypes
{
    public class CultureFamilyData
    {
        /* -------- Public Fields -------- */
        public string id;
        public ModifierData[] modifierDatas;
        public string[] appointmentIDs;
    }
}
