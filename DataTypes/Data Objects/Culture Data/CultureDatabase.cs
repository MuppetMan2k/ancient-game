﻿namespace DataTypes
{
    public class CultureDatabase
    {
        /* -------- Public Fields -------- */
        public CultureData[] cultureDatas;
        public CultureGroupData[] cultureGroupDatas;
        public CultureFamilyData[] cultureFamilyDatas;
    }
}
