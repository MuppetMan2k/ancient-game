﻿namespace DataTypes
{
    public class ReligionData
    {
        /* -------- Public Fields -------- */
        public string id;
        public string associatedCultureID;
        public DeityData[] deityDatas;
        public ModifierData[] modifierDatas;
        public string[] appointmentIDs;
    }
}
