﻿namespace DataTypes
{
    public class CultureGroupData
    {
        /* -------- Public Fields -------- */
        public string id;
        public string cultureFamilyID;
        public ModifierData[] modifierDatas;
        public string[] appointmentIDs;
    }
}
