﻿namespace DataTypes
{
    public class LanguageData
    {
        /* -------- Public Fields -------- */
        public string id;
        public string associatedCultureID;
        public ModifierData[] modifierDatas;
    }
}
