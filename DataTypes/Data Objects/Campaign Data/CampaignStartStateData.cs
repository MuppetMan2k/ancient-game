﻿namespace DataTypes
{
    public class CampaignStartStateData
    {
        /* -------- Public Fields -------- */
        public FactionStateData[] factionStateDatas;
        public LandTerritoryStateData[] landTerritoryStateDatas;
        public LandRegionStateData[] landRegionStateDatas;
        public LandProvinceStateData[] landProvinceStateDatas;
        public SeaTerritoryStateData[] seaTerritoryStateDatas;
        public DateData dateData;
        public MarketStateData marketStateData;
    }
}
