﻿namespace DataTypes
{
    public class FactionCalibrationData
    {
        /* -------- Public Fields -------- */
        // Debug Single (required because no other floats in type, but ModifierData uses floats, possible bug in MonoGame that causes load errors otherwise)
        public float debugSingle;
        // Size
        public ModifierData[] factionSizeModifierDatas_TribalState;
        public ModifierData[] factionSizeModifierDatas_CityState;
        public ModifierData[] factionSizeModifierDatas_SmallState;
        public ModifierData[] factionSizeModifierDatas_State;
        public ModifierData[] factionSizeModifierDatas_ProvincialState;
        public ModifierData[] factionSizeModifierDatas_MultiProvincialState;
        // Size thresholds
        public string[] tribalStatePossibleCultureFamilyIDs;
        public int tribalStateMinLandTerritories;
        public int tribalStateMaxLandTerritories;
        public int tribalStateMinLandProvinces;
        public int tribalStateMaxLandProvinces;
        public string[] cityStatePossibleCultureFamilyIDs;
        public int cityStateMinLandTerritories;
        public int cityStateMaxLandTerritories;
        public int cityStateMinLandProvinces;
        public int cityStateMaxLandProvinces;
        public int smallStateMinLandTerritories;
        public int smallStateMaxLandTerritories;
        public int smallStateMinLandProvinces;
        public int smallStateMaxLandProvinces;
        public int stateMinLandTerritories;
        public int stateMaxLandTerritories;
        public int stateMinLandProvinces;
        public int stateMaxLandProvinces;
        public int provincialStateMinLandTerritories;
        public int provincialStateMaxLandTerritories;
        public int provincialStateMinLandProvinces;
        public int provincialStateMaxLandProvinces;
        public int multiProvincialStateMinLandTerritories;
        public int multiProvincialStateMaxLandTerritories;
        public int multiProvincialStateMinLandProvinces;
        public int multiProvincialStateMaxLandProvinces;
        // Special ratings
        public ModifierData[] factionSpecialRatingModifierDatas_Empire;
        public ModifierData[] factionSpecialRatingModifierDatas_Kingdom;
        public ModifierData[] factionSpecialRatingModifierDatas_League;
        public ModifierData[] factionSpecialRatingModifierDatas_Confederation;
        public ModifierData[] factionSpecialRatingModifierDatas_Republic;
        public ModifierData[] factionSpecialRatingModifierDatas_Hegemon;
        public ModifierData[] factionSpecialRatingModifierDatas_Colonies;
        public ModifierData[] factionSpecialRatingModifierDatas_Horde;
        // Special rating thresholds
        public string[] hegemonPossibleSizes;
        public int hegemonMinCityStateClients;
        public string[] republicPossibleSizes;
        public string[] kingdomPossibleSizes;
        public string[] empirePossibleSizes;
        public string[] coloniesPossibleSizes;
        public string[] confederationPossibleSizes;
        public string[] leaguePossibleSizes;
        public string[] hordePossibleCultureFamilyIDs;
        public string[] hordePossibleSizes;
        // Government
        public ModifierData[] governmentGroupModifierDatas_Monarchy;
        public ModifierData[] governmentGroupModifierDatas_Republic;
        public ModifierData[] governmentGroupModifierDatas_Confederation;
        // Stability
        public ModifierData[] factionPositiveStabilityModifierDatas;
        public ModifierData[] factionNegativeStabilityModifierDatas;
    }
}
