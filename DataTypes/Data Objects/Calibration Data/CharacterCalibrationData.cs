﻿namespace DataTypes
{
    public class CharacterCalibrationData
    {
        /* -------- Public Fields -------- */
        // Character relations
        public float characterRelationsAverage;
        public int characterRelationsPower;
    }
}
