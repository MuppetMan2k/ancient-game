﻿using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class LightingCalibrationData
    {
        /* -------- Public Fields -------- */
        // Spring
        public Color springLightColor;
        public float springAmbientLightIntensity;
        public float springDirectLightIntensity;
        public Vector3 springDirectLightDirection;
        // Summer
        public Color summerLightColor;
        public float summerAmbientLightIntensity;
        public float summerDirectLightIntensity;
        public Vector3 summerDirectLightDirection;
        // Autumn
        public Color autumnLightColor;
        public float autumnAmbientLightIntensity;
        public float autumnDirectLightIntensity;
        public Vector3 autumnDirectLightDirection;
        // Winter
        public Color winterLightColor;
        public float winterAmbientLightIntensity;
        public float winterDirectLightIntensity;
        public Vector3 winterDirectLightDirection;
    }
}
