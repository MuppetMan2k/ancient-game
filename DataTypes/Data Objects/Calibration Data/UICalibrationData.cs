﻿using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class UICalibrationData
    {
        /* -------- Public Fields -------- */
        // Window positions and sizes
        public Point managementWindowPosition;
        public int managementWindowWidth;
        public int managementWindowHeight;
        public Point informationWindowPosition;
        public int informationWindowWidth;
        public int informationWindowHeight;
        public Point messageWindowPosition;
        public int messageWindowWidth;
        public int messageWindowHeight;
        // Window objects
        public int windowCloseButtonSize;
        public int windowPageTabSize;
        // Land territory window
        public float minValuePerConstituentIcon;
        public float minQuantityPerResourceIcon;
        public float minValuePerWealthSourceIcon;
        // Standards
        public string standardPoleTextureID;
        public float standardPoleWidthFraction;
    }
}
