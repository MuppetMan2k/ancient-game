﻿namespace DataTypes
{
    public class CameraCalibrationData
    {
        /* -------- Public Fields -------- */
        // Distance
        public float distanceAtMaxZoomFactor;
        public float maxZoomFactorForDistance;
        public float distanceAtMinZoomFactor;
        public float minZoomFactorForDistance;
        // Rise angle
        public float riseAngleAtMaxZoomFactor;
        public float maxZoomFactorForRiseAngle;
        public float riseAngleAtMinZoomFactor;
        public float minZoomFactorForRiseAngle;
        // FOV
        public float verticalFieldOfViewAngle;
        // View modes
        public float zoomFactorForTerritoryLevel;
        public float zoomFactorForRegionLevel;
    }
}
