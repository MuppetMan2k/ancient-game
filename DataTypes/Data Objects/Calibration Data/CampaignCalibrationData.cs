﻿namespace DataTypes
{
    public class CampaignCalibrationData
    {
        /* -------- Public Fields -------- */
        // Turns
        public int turnsPerYear;
        // Seasons
        public ModifierData[] seasonModifierDatas_Spring;
        public ModifierData[] seasonModifierDatas_Summer;
        public ModifierData[] seasonModifierDatas_Autumn;
        public ModifierData[] seasonModifierDatas_Winter;
        // Climate
        public string[] climateTypesRequiringWater;
        public string[] climateTypesRequiringWarmth;
        // Land territories
        public int landTerritoryBaseSupportablePopulation;
        public ModifierData[] landTerritoryTerrainModifierDatas_LowGrassLand;
        public ModifierData[] landTerritoryTerrainModifierDatas_HillyGrassLand;
        public ModifierData[] landTerritoryTerrainModifierDatas_Steppes;
        public ModifierData[] landTerritoryTerrainModifierDatas_Forest;
        public ModifierData[] landTerritoryTerrainModifierDatas_Desert;
        public ModifierData[] landTerritoryTerrainModifierDatas_Mountain;
        public ModifierData[] landTerritoryTerrainModifierDatas_Marsh;
        public ModifierData[] landTerritoryCapitolModifierDatas;
        public ModifierData[] landTerritoryNonCapitolModifierDatas;
        public ModifierData[] landTerritoryOwnershipModifierDatas_MilitaryOwnership;
        public ModifierData[] landTerritoryOwnershipModifierDatas_AnnexedOwnership;
        public ModifierData[] landTerritoryOwnershipModifierDatas_HomelandOwnership;
        // Land provinces
        public ModifierData[] landProvinceClimateModifierDatas_Frozen;
        public ModifierData[] landProvinceClimateModifierDatas_ColdAndWet;
        public ModifierData[] landProvinceClimateModifierDatas_ColdAndDry;
        public ModifierData[] landProvinceClimateModifierDatas_CoolAndWet;
        public ModifierData[] landProvinceClimateModifierDatas_CoolAndDry;
        public ModifierData[] landProvinceClimateModifierDatas_WarmAndWet;
        public ModifierData[] landProvinceClimateModifierDatas_WarmAndDry;
        public ModifierData[] landProvinceClimateModifierDatas_HotAndWet;
        public ModifierData[] landProvinceClimateModifierDatas_HotAndDry;
        public ModifierData[] landProvinceClimateModifierDatas_Baked;
        // Sea territories
        public ModifierData[] seaTerritoryDeepModifierDatas;
        public ModifierData[] seaTerritoryCoastalModifierDatas;
        // Adjacent land territories
        public ModifierData[] adjacentLandTerritoryCrossesRiverModifierDatas;
        // Movement point costs
        public float landTerritoryMovementPointsPerArea;
        public float landTerritoryToSettlementMovementPointCostMultiplier;
        public float seaTerritoryMovementPointsPerArea;
        public float seaTerritoryToSettlementMovementPointCostMultiplier;
        public float settlementToSeaTerritoryMovementPointCostMultiplier;
        public float seaTerritoryToLandTerritoryMovementPointCostMultiplier;
        // Land territory resource production
        public float baseResourceProductionMultiplier;
        public float baseResourceProductionMultiplierPerArea;
        public float baseResourceProductionBaseFertilityMultiplier;
        public float baseResourceProductionReducedFertilityMultiplier;
        public float resourceProductionBaseHarvestMultiplier;
        public float resourceProductionReducedHarvestMultiplier;
        // Land territory stats
        public ModifierData[] positiveNobilityHappinessModifierDatas;
        public ModifierData[] negativeNobilityHappinessModifierDatas;
        public ModifierData[] positivePlebHappinessModifierDatas;
        public ModifierData[] negativePlebHappinessModifierDatas;
        public ModifierData[] positiveOrderModifierDatas;
        public ModifierData[] negativeOrderModifierDatas;
        public ModifierData[] positiveHealthModifierDatas;
        public ModifierData[] negativeHealthModifierDatas;
        public ModifierData[] positivePopulationGrowthModifierDatas;
        public ModifierData[] negativePopulationGrowthModifierDatas;
        public ModifierData[] populationModifierDatas;
        public ModifierData[] overpopulationModifierDatas;
        public ModifierData[] slavePopulationPercentageModifierDatas;
        public ModifierData[] positiveFoodModifierDatas;
        public ModifierData[] negativeFoodModifierDatas;
        public ModifierData[] positivePrivateWealthChangeModifierDatas;
        public ModifierData[] negativePrivateWealthChangeModifierDatas;
        public ModifierData[] privateWealthModifierDatas;
        public ModifierData[] positiveManpowerChangeModifierDatas;
        public ModifierData[] negativeManpowerChangeModifierDatas;
        public int baseMaxManpower;
        public ModifierData[] positiveFoodStoresChangeModifierDatas;
        public ModifierData[] negativeFoodStoresChangeModifierDatas;
        public ModifierData[] positiveFoodStoresModifierDatas;
        public int baseMaxFoodStores;
        public ModifierData[] positiveWaterModifierDatas;
        public ModifierData[] negativeWaterModifierDatas;
        public ModifierData[] positiveWarmthModifierDatas;
        public ModifierData[] negativeWarmthModifierDatas;
        public float foodStatPerUnitFoodResource;
        public int minDifferentFoodTypesForVariedDiet;
        public ModifierData[] variedDietModifierDatas;
        // Buildings
        public int baseNumSettlementStandardBuildingSlots;
        public float numTerritoryBuildingSlotsPerArea;
        public float lightDamageRepairCostMultiplier;
        public float mediumDamageRepairCostMultiplier;
        public float heavyDamageRepairCostMultiplier;
        public float lightDamageRepairConstructionTimeMultiplier;
        public float mediumDamageRepairConstructionTimeMultiplier;
        public float heavyDamageRepairConstructionTimeMultiplier;
        public float demolishConstructionTimeMultiplier;
    }
}
