﻿namespace DataTypes
{
    public class TradeCalibrationData
    {
        /* -------- Public Fields -------- */
        public int baseTraderMovementPoints;
        public float baseResourceDemand;
        public float resourceDemandPer1kPopulation;
        public float baseTradePathEfficiency;
    }
}
