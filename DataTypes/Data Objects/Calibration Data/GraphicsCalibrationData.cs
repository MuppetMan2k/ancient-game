﻿using Microsoft.Xna.Framework;

namespace DataTypes
{
    public class GraphicsCalibrationData
    {
        /* -------- Public Fields -------- */
        // Overlay
        public float factionOwnershipOverlayAlpha;
        public float overlayDoubleColorBandThickness;
        public string overlayEffectID;
        // Land border
        public string landBorderEffectID;
        // Land territory border
        public float landTerritoryBorderThickness;
        public float landTerritoryBorderOuterThickness;
        public float landTerritoryBorderInnerThickness;
        public float landTerritoryBorderAlpha;
        // Land region border
        public float landRegionBorderThickness;
        public Color landRegionBorderColor;
        public float landRegionBorderAlpha;
        // Land province border
        public float landProvinceBorderThickness;
        public Color landProvinceBorderColor;
        public float landProvinceBorderAlpha;
        // Sea territory border
        public float seaTerritoryBorderThickness;
        public Color seaTerritoryBorderColor;
        public float seaTerritoryBorderAlpha;
        // Sea region border
        public float seaRegionBorderThickness;
        public Color seaRegionBorderColor;
        public float seaRegionBorderAlpha;
        // Name Labels
        public string nameLabelEffectID;
        public string nameLabelAtlasID;
        // Land terrain
        public string landTerrainEffectID;
        public string landTerrainLowGrassLandTextureID;
        public string landTerrainLowGrassLandBumpMapID;
        public string landTerrainHillyGrassLandTextureID;
        public string landTerrainHillyGrassLandBumpMapID;
        public string landTerrainSteppesTextureID;
        public string landTerrainSteppesBumpMapID;
        public string landTerrainForestTextureID;
        public string landTerrainForestBumpMapID;
        public string landTerrainDesertTextureID;
        public string landTerrainDesertBumpMapID;
        public string landTerrainMountainTextureID;
        public string landTerrainMountainBumpMapID;
        public string landTerrainMarshTextureID;
        public string landTerrainMarshBumpMapID;
        public float landTerrainTextureScale;
        public float landTerrainBumpMapScale;
        // Sea terrain
        public string seaTerrainEffectID;
        public string seaTerrainTextureID;
        public string seaTerrainCoastalTextureID;
        public string seaTerrainDeepTextureID;
        public float seaTerrainTextureScale;
        public string seaTerrainBumpMap1ID;
        public float seaTerrainBumpMap1Scale;
        public Vector2 seaTerrainBumpMap1Velocity;
        public string seaTerrainBumpMap2ID;
        public float seaTerrainBumpMap2Scale;
        public Vector2 seaTerrainBumpMap2Velocity;
        public int seaTerrainSpecularPower;
        public float seaTerrainSpecularIntensity;
        // Settlements
        public string settlementEffectID;
        public float settlementTextureScale;
        public string settlementTexture1ID;
        // Lakes
        public string lakeEffectID;
        public string lakeTextureID;
        public float lakeTextureScale;
        public int lakeSpecularPower;
        public float lakeSpecularIntensity;
        // Rivers
        public string riverEffectID;
        public float riverWidth;
        public string riverTextureID;
        public float riverTextureScale;
        public string riverBumpMapID;
        public float riverBumpMapScale;
        public float riverBumpMapSpeed;
        public int riverSpecularPower;
        public float riverSpecularIntensity;
        // Small Islands
        public string smallIslandEffectID;
        public string smallIslandTextureID;
        public float smallIslandTextureScale;
        public string smallIslandBumpMapID;
        public float smallIslandBumpMapScale;
        // Roads
        public string roadEffectID;
        public float roadWidth;
        public string roadTexture1ID;
        // Sea trade routes
        public string seaTradeRouteEffectID;
        public Color seaTradeRouteColor;
        public float seaTradeRouteAlpha;
        public float seaTradeRouteWidth;
        // Trade vehicles
        public string tradeVehicleEffectID;
        public string tradeVehicleLandTextureID;
        public string tradeVehicleSeaTextureID;
        public float tradeVehicleLandLength;
        public float tradeVehicleLandWidth;
        public float tradeVehicleSeaLength;
        public float tradeVehicleSeaWidth;
        public float tradeVehicleMaxTraffic;
        public float tradeVehicleMinSpawnTime;
        public float tradeVehicleMaxSpawnTime;
        public float tradeVehicleSpawnTimeError;
        public float tradeVehicleSpeed;
        public float tradeVehicleSpeedError;
        public float tradeVehicleFadeTime;
        // Settlement labels
        public string settlementLabelTopBottomEdgeTextureID;
        public string settlementLabelLeftRightEdgeTextureID;
        public string settlementLabelCornerTextureID;
        public string settlementLabelFontID;
        public Color settlementLabelLightFontColor;
        public Color settlementLabelDarkFontColor;
        // Statues
        public float statueGSHeight;
        public float statueStandardHeightFraction;
        public float statueStandardOffsetFraction;
        public float statueSettlementStandardGSHeight;
        public float statueSettlementStandardHeightWidthFraction;
        // Statue dragging
        public string statueDragEffectID;
        public float statueDragOverlayAlpha;
        public Color statueDragCanMoveOverlayColor;
        public Color statueDragSelectOverlayColor;
        public Color statueDragCannotMoveOverlayColor;
    }
}
