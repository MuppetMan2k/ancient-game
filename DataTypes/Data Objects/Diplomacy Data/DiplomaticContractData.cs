﻿namespace DataTypes
{
    public class DiplomaticContractData
    {
        /* -------- Public Fields -------- */
        public string targetFactionID;
        public string contractType;
        public int turnsRemaining;
        // Specific properties
        public int tributeValue;
    }
}
