﻿namespace DataTypes
{
    public class TaxData
    {
        /* -------- Public Fields -------- */
        public string id;
        public string cultureID;
        public ModifierData[] modifierDatas;
    }
}
